#/bin/bash
PROJ_DIR=..
if [ -z ${PROJ_DIR} ]
  then echo "Need to define PROJ_DIR. Exiting."
  exit
fi

${PROJ_DIR}/Release/PointCuts -i ${PROJ_DIR}/test/bm23.mps -o ${PROJ_DIR}/test --vpc_depth=-2 --timelimit=3600 --log_file=log.csv --use_all_ones_heur=1 --num_cuts_iter_bilinear=1 --use_unit_vectors_heur=0 --use_tight_points_heur=0 --use_tight_rays_heur=0 --num_obj_per_point=-2 --mode_obj_per_point=121 --strengthen=1 --overload_max_cuts=100 --bb_runs=0 --bb_strategy=10776 --prlp_strategy=1 --partial_bb_strategy=04 --bb_use_best_bound=1 --opt_file="$PROJ_DIR/data/ip_opt.csv" --cut_presolve=2 --min_orthogonality=0  --timelimit=900 --cgs=7 $1 $2 $3 $4 $5 $6 $7 $8
