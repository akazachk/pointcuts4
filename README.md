# Cuts from points #
##### Name: README
##### Author: Aleksandr M. Kazachkov, Carnegie Mellon University, akazachk AT cmu DOT edu, http://andrew.cmu.edu/~akazachk/
##### Last edit: 2017-07-11

Implementation of cuts generated from points for my dissertation: V-polyhedral cuts, generalized intersection cuts, and tilted cuts.
When example code below has brackets around the arguments, this indicates the arguments are optional.
Note that the code may yield different results based on the setup in the system being used; e.g., the starting basis chosen by Clp could be affected.


## Contents ##
1. What do I need?
2. How do I compile the code?
3. What is the simplest way to run the code?
4. How do I perform more experiments?
5. How do I get tables?
6. Environment variables
7. Notes
8. Whom do I talk to?


### What do I need? ###

0. Properly set environment variables (see below) and Bcp, BLAS, LAPACK, and Python. Note that all of the necessary packages are included (or rather, a way to install them is included), though you need `gfortran` and a C++ compiler installed. To install, first adjust the file `scripts/configure_scripts/saved_settings/env_vars.sh` and then you can run (following the prompts)

        cd scripts/configure_scripts
        ./config_main.sh

1. BLAS and LAPACK (included in Accelerate framework on Mac). Ensure BLAS and LAPACK are working properly. You will need to use the shared library versions. Note that you need to adjust the environment variable `LD_LIBRARY_PATH` to point to `${PROJ_DIR}/lib` or wherever the files are installed.
2. A version of Bcp from COIN-OR (latest tested is 1.4.3). The code may run differently on different versions. Under `scripts/coin_scripts`, there are config files that have worked on Macs (`mac_*_config.site`) and Linux (`linux_*_config.site`), where * = debug or release.
3. For scripts and compiling tables, Python 2 (maybe will work with Python 3) with `matplotlib` (which in turn requires `six`).


### How do I compile the code? ###

1. Ensure everything from the "What do I need?" is properly set up and works on the machine. 
2. Call [if on LINUX machine; use `makefile_mac` if on Mac]

        cp makefile_linux makefile 

3. Call

        make dir_debug
        make debug

    If you want to use the `Release` version, use

        make dir_release
        make release

    For a description of the arguments that the executable takes, run
        
        ${PROJ_DIR}/{Debug or Release}/PointCuts --help


### What is the simplest way to run the code? ###

0. These scripts require that you first compiled the `Release` version of code, as in the previous section.

1. The following will run the _best_ set of parameters for each instance (where best is in terms of percent integrality gap closed). The `1` indicates that, to speed things up, the code is run in batches as contained in `data/instances/batches`. Note that if you are on a shared computer, this implies that 18 processes will be started and run in parallel.

        ${PROJ_DIR}/scripts/run_scripts/run_experiments.sh ${CUT_TYPE} 1

2. The result is that `results/instances/batches/batchX` for each batch will contain a file `${CUT_TYPE}-best.csv`. To merge these into one file, which will be copied into `results/instances/batches` and `results/instances`, call

        ${PROJ_DIR}/scripts/run_scripts/merge_results.sh ${PROJ_DIR}/results/instances/batches 1 best

3. A table containing the results on the best percent integrality gap closed per instance can be recreated by running

        python ${PROJ_DIR}/scripts/python_scripts/get_table.py 1 best

4. The output of this goes to `results/tables` and will contain `gap-closed.csv` and `gap-closed.tex`. The first is a file that can be opened as a spreadsheet. The second can be put into LaTeX to view as a table.


### How do I perform more experiments? ###

1. There are three things to decide:

    * Which cuts do you want to test? This is passed through the environment variable `CUT_TYPE`.
        * `pha`
        * `vpc`
        * `tilted`
    * Which instances do you want to test on? This is choosing the subdirectory of `data/instances` you want to use, or your own folder of instances.
        * `all`, every instance tested
        * `batches`, can be used to speed up computations by running the instances in parallel; leads to 18 processes being run in parallel
        * `quick`, a subset of the instances that runs quickly and is good for testing purposes
        * choose-your-own-adventure, i.e., wherever your own folder is (you may need to use the full path)
    * What do you want to run? The options are just the best set of parameters per instance, a small test set of parameters per instance, or the full set of parameters used in the paper:
        * `best`
        * `test`, which only works with `pha`
        * `full`, which only works with `pha`

2. The typical approach is to call the following sequence (substitute `test` or `full` instead of `best` depending on your choice)

        ${PROJ_DIR}/scripts/run_scripts/run_experiments.sh ${CUT_TYPE} [instance directory] [0 or 1] best

    After this completes, you create a merged results file (`${CUT_TYPE}-best.csv` or `$pha-test.csv` or `$pha-full.csv`) in `results/instances` using

        ${PROJ_DIR}/scripts/run_scripts/merge_results.sh ${PROJ_DIR}/results/instances/{all or batches or quick} [0 or 1] best

    If you used `full`, the file `${CUT_TYPE}-full.csv` will not go to `results/instances` directly. It will be in the subdirectory of `results/instances/stub` where `stub` is the name of the directory in which the instances were contained, e.g., `all` or `batches` or `quick`. You will need to copy that file manually into `results/instances` in order to create tables from it.

3. The default (if you give no arguments) is running on the instances in `data/instances/quick` (not in batches). Note that if you choose your own folder and it contains batches, you will need to adjust the file `scripts/run_scripts/batch_list.txt`.

4. We first give an example in which one chooses `pha`, `batches`, and `full`. Note that the full set of parameters takes a very long time to run. When the batch argument is specified as `1` and the instance directory is not given, it is by default `data/instances/batches`. This creates three processes per batch, running parameter settings in parallel for the three different hyperplane activation rules.

        ${PROJ_DIR}/scripts/run_scripts/run_experiments.sh pha 1 full

    Once this finishes running, merge the results (which, for this case, will be `pha-full0.csv`, `pha-full1.csv`, and `pha-full2.csv` in each of the batch subfolders) by calling

        ${PROJ_DIR}/scripts/run_scripts/merge_results.sh ${PROJ_DIR}/results/instances/batches 1 full

    Then copy the file `pha-full.csv` into `results/instances`:

        cp ${PROJ_DIR}/results/instances/batches/pha-full.csv ${PROJ_DIR}/results/instances/pha-full.csv

5. As an example of testing the best parameters on all the instances without using batches, call
        
        ${PROJ_DIR}/scripts/run_scripts/run_experiments.sh ${CUT_TYPE} ${PROJ_DIR}/data/instances/all 0 best

     And when this finishes, merge by calling

        ${PROJ_DIR}/scripts/run_scripts/merge_results.sh ${PROJ_DIR}/results/instances/all 0 best


### How do I get tables? ###

0. For the table python scripts to run, you need `PYTHONPATH` to be set correctly and an installation of `matplotlib` and `numpy`. The latter comes with most python source installations, and the former can be compiled through

        ${PROJ_DIR}/scripts/configure_scripts/install_matplotlib.sh

1. Copy the file you want to use for compiling tables into `results/instances`. This is automatically done when calling `merge_results.sh` for `test` and `best` but not for `full`.

2. To compile table data, use the python scripts in `scripts/python_scripts`. For example, call (again, substitute `best` with `test` or `full` as appropriate)

        python ${PROJ_DIR}/scripts/python_scripts/get_table.py 1 best

3. The results are contained in `results/tables`. The tables that end with `short` correspond to the instances for which either SICs or GICs close some of the integrality gap. Note that this means the averages will not match up to what is in Tables 3 and 4 in the paper, because those tables only have the instances in which GICs improve over SICs.

4. To get the tables in the paper:

        cp ${PROJ_DIR}/results/saved_results/pha-full.csv ${PROJ_DIR}/results/instances/pha-full.csv
        python ${PROJ_DIR}/scripts/python_scripts/get_table.py 1 full


### Environment variables ###

These variables should be defined for the environment or locally.

0. Either follow the instructions below, or set the proper values in `scripts/config_environment_variables.sh` and then run

        source ${PROJ_DIR}/scripts/configure_scripts/saved_settings/env_vars.sh

1. Define (the BCP variables will need to be adjusted based on your setup)

        # CUT_TYPE can be vpc_bb or vpc or pha or tilted
        export CUT_TYPE="vpc" 
        export PROJ_DIR="path to pointcuts directory"
        export POINTCUTS_DEBUG="${PROJ_DIR}/Debug/PointCuts"
        export POINTCUTS_RELEASE="${PROJ_DIR}/Release/PointCuts"
        export POINTCUTS="${POINTCUTS_DEBUG}"
        export BCP_DEBUG="${PROJ_DIR}/coin-or/Bcp-1.4.3/buildg"
        export BCP_RELEASE="${PROJ_DIR}/coin-or/Bcp-1.4.3/build"
        export LD_LIBRARY_PATH="${PROJ_DIR}/lib:${LD_LIBRARY_PATH}"
        export PYTHONPATH="${PYTHONPATH}:${PROJ_DIR}/scripts/python_scripts:${PROJ_DIR}/lib64/python:${PROJ_DIR}/lib/python"

2. BLAS, LAPACK: On Mac, you simply need to have the Accelerate framework (invoked by `-framework Accelerate`). On Linux, define (depending on your environment)

        export ENV_LAPACK_LIB="/usr/lib64"
        export ENV_LAPACK_LIB_NAME="liblapack.so.3"
        export ENV_BLAS_LIB="/usr/lib64"
        export ENV_BLAS_LIB_NAME="libblas.so.3"


### Notes ###

The underlying solver used is OsiClp through COIN-OR. We treat slacks differently than does Clp: we assume all slacks/excess variablesare non-negative, whereas Osi is enforcing that all slacks have a +1 coefficient (so rows in equality form become ax+s, where s is non-positive for >= rows, non-negative for <= rows, and = 0 for = rows). We instead have ax + s = b for ax <= b rows and ax - e = b for ax >= b rows, i.e., we add excess variables. Then in the non-basic space, the LP opt is the origin, and only cuts with right-hand side 1 cut it off.

For example, consider any row ax >= b. Suppose the slack, s, in that row is basic. Thus, the tableau becomes s = b' - a'x (where b' and a' are obtained by multiplying by the basis inverse). We want to replace e = -s. Since the coefficient on e should ultimately by +1 in the final tableau, we need to multiply (-e) = b' - a'x by -1 to get e = -b' + a'x. The rays are thus just a' in that row, not -a' as they are for the <= rows.

Similarly, we need to complement non-basic slacks from >= rows. This is treated as we would treat any non-basic variables at their upper-bound in the basis: negate the rays.

### Other useful things ###

You may want to set up `git` in a way that does not ask you for a password each time. Do this:
        
        ssh -v  # ensure ssh is installed (will return version)
        ssh-keygen  # if you haven't set up an ssh key yet; it is highly recommended to use a password
        pbcopy < ~/.ssh/id_rsa.pub  # copies the created key; add this key to the git/bitbucket account
        ssh-add ~/.ssh/id_rsa  # set up ssh-agent

For `Cbc`, install using
        
        svn checkout https://projects.coin-or.org/svn/Cbc/stable/2.9 ./Cbc-2.9

For `gfortran`, see http://hpc.sourceforge.net.

### Whom do I talk to? ###

Aleksandr M. Kazachkov

akazachk AT cmu DOT edu

http://andrew.cmu.edu/~akazachk/

Some of this code was written with Selvaprabu Nadarajah, and some of it is based on code from Francois Margot and Giacomo Nannicini.

Also, apologies to all for the mixed camel case and snake case... I could not make up my mind and it ended up being a mess.
