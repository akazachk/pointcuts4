//============================================================================
// Name        : SIC.cpp
// Author      : Aleksandr M. Kazachkov
// Version     : 0.2014.03.10
// Copyright   : Your copyright notice
// Description : Generates standard intersection cuts
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#include <cmath> // Remember to put std::abs
#include <algorithm> // For max_element, min_element, sort
#include "GlobalConstants.hpp"
#include "Utility.hpp"
#include "Output.hpp"
#include "SIC.hpp"

void genSICs(AdvCuts& structSICs, AdvCuts& NBSICs,
    const PointCutsSolverInterface* const solver, SolutionInfo& solnInfo,
    const bool working_in_subspace) {
  genNBSICs(NBSICs, solver, solnInfo);

#ifdef SHOULD_WRITE_SICS
  if (working_in_subspace) {
    NBSICs.printCuts("SSICs", solver, solnInfo);
  } else {
    NBSICs.printCuts("OSICs", solver, solnInfo);
  }
#endif

//  // Convert cuts to structural space, and print them
  structSICs.resize(solnInfo.numSplits);
  for (int currcut = 0; currcut < (int) NBSICs.size(); currcut++) {
    // NBSICs are in complemented NB space
    AdvCut::convertCutFromJSpaceToStructSpace(structSICs.cuts[currcut],
        solver, solnInfo.nonBasicVarIndex, NBSICs[currcut], true);
  }

  // The generated NBSICs may be proving "infeasibility" if we have both
  // sides of the split empty and no rays intersect either side of the split.
  // This *could* happen when we are working in the subspace
  // (in the full space, this indeed would be a certificate of infeasibility).
  // So, in the subspace, we add each of the SICs, see if it causes infeasibility,
  // and if it does, we no longer consider that split
  checkSICFeasibility(structSICs, solver, solnInfo, working_in_subspace);

  // Remove infeasible cuts
  for (int cut_ind = 0; cut_ind < (int) structSICs.size(); cut_ind++) {
    if (!solnInfo.splitFeasFlag[cut_ind]) {
      NBSICs.cuts.erase(NBSICs.cuts.begin() + cut_ind);
      structSICs.cuts.erase(structSICs.cuts.begin() + cut_ind);
    }
  }

  structSICs.setOsiCuts();
  GlobalVariables::numCutsFromHeur[CutHeuristics::SIC_CUT_GEN] =
      structSICs.sizeCuts();
}

/***********************************************************************/
/**
 * @brief Generate SICs, except only generate at most numCutGenSets splits.
 */
void genNBSICs(AdvCuts& NBSIC, const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo) {
  // Although we may not use all these splits,
  // since some may be lopsided, better to calculate all.
  // Much better to calculate one at a time, decide if it is good,
  // then stop when we have reached our limit...
  int numsplits = solnInfo.fractionalCore.size();

  // Resize NBSIC to account for all possible splits
  NBSIC.resize(numsplits);
  std::vector<double> absSumCoeffs(numsplits);

  // Generate a SIC per each split in the subspace
  for (int split = 0; split < numsplits; split++) {
    int s = solnInfo.fractionalCore[split];
    int s_row = solnInfo.rowOfVar[s]; // Clearly will be basic since it is in the fractional core
    std::string colname = solver->getColName(s);
#ifdef TRACE
    printf("\n## Generating NBSIC for split %d on variable %s. ##\n", s,
        colname.c_str());
#endif

    genNBSIC(NBSIC.cuts[split], absSumCoeffs[split], solver,
        solnInfo.nonBasicOrigVarIndex, solnInfo.nonBasicSlackVarIndex,
        s, s_row);

    // In the previous, we were assuming the LP relaxation is at the origin in the NB space
    // This is not actually true when rays are at their upper-bounds, since their directions are negated
    // Furthermore, they may actually be originating at some point that is not the origin,
    // when they are NB at their upper-bound
    // The subsequent parts complement so that we get the cut in the ``original'' NB space
//    convertCutFromYSpaceToXSpace(NBSIC.cuts[split], solnInfo, solver);

//#ifdef TRACE
//    printf("After complementing and normalization.\n");
//    printf("\tSplit var: %d\n", NBSIC.cuts[split].splitVarIndex);
//    printf("\tSplit var name: %s\n",
//        solver->getColName(NBSIC.cuts[split].splitVarIndex).c_str());
//    printf("\tRHS: %f\n", NBSIC.cuts[split].rhs());
//    for (int i = 0; i < (int) NBSIC.cuts[split].size(); i++) {
//      printf("\tCoeff %d: %f\n", i, NBSIC.cuts[split][i]);
//    }
//    printf("\n");
//#endif
  }
}

/***********************************************************************/
/**
 * @brief Generate a single SIC in the NB space of the form alpha x \ge beta.
 *      Beta is 1 unless there is some complementing.
 */
void genNBSIC(AdvCut& NBSIC, double& absSumCoeffs,
    const PointCutsSolverInterface* const solver,
    const std::vector<int>& nonBasicOrigVarIndex,
    const std::vector<int>& nonBasicSlackVarIndex, const int splitVarIndex,
    const int splitVarRowIndex) {
  int numcols = solver->getNumCols();
  int numrows = solver->getNumRows();
  int numNBOrig = (int) nonBasicOrigVarIndex.size();
  int numNBSlack = (int) nonBasicSlackVarIndex.size();
  int numNB = numNBOrig + numNBSlack;

  double splitVarVal = solver->getColSolution()[splitVarIndex];

  NBSIC.NBSpace = true;
  std::vector<double> coeff(numNB);
  NBSIC.distAlongRay.resize(numNB);
  NBSIC.splitVarIndex = splitVarIndex;
  NBSIC.splitVarName = solver->getColName(splitVarIndex);
  NBSIC.cutHeur = CutHeuristics::SIC_CUT_GEN;
  double rhs = 1.0;

  double rayMoveSize;
  std::vector<int> cstat(numcols), rstat(numrows);

  std::vector<double> basisCol(numrows);
  const char* rowSense = solver->getRowSense();

  // In case something disabled it before
  // Actually we are passing it as const here, so this will not work
  // Just ensure it is not disabled before
//  solver->enableFactorization();

  solver->getBasisStatus(&cstat[0], &rstat[0]);
  double pi0 = std::floor(splitVarVal);
  for (int col = 0; col < numNBOrig; col++) {
    int currvar = nonBasicOrigVarIndex[col];

    solver->getBInvACol(currvar, &basisCol[0]);
    double ray_coeff = basisCol[splitVarRowIndex];

    //If the non-basic variable is at its upper bound, flip the sign of the ray
    if (cstat[currvar] == 2) {
      ray_coeff *= -1;
    }

    //The ray component is the -a_{ij}, where a_{ij} is the entry in the tableau
    if (greaterThanVal(-ray_coeff, 0.0)) {
      rayMoveSize = (pi0 + 1 - splitVarVal) / (-ray_coeff);
      coeff[col] = 1.0 / rayMoveSize;
      NBSIC.distAlongRay[col] = rayMoveSize;
    } else {
      if (lessThanVal(-ray_coeff, 0.0)) {
        rayMoveSize = (splitVarVal - pi0) / ray_coeff;
        coeff[col] = 1.0 / rayMoveSize;
        NBSIC.distAlongRay[col] = rayMoveSize;
      } else {
        coeff[col] = 0.0;
        NBSIC.distAlongRay[col] = solver->getInfinity();
      }

    }
    if (isZero(coeff[col])) {
      coeff[col] = 0.0;
      NBSIC.distAlongRay[col] = solver->getInfinity();
    }
    {
      // TODO Remove!
      // I just want to see what happens if the non-basic variable is free
      // The coefficient *should* be zero... otherwise, we would have two coeffs,
      // one for each direction the ray can go in.
      // In reality, we can force a situation in which it isn't,
      // but this corresponds to a split in which the LP optimum is not being cut by the SIC.
      // We shouldn't consider those splits, since they're hopeless anyway.
      if (isNonBasicFreeVar(solver, currvar) && !isZero(coeff[col])) {
        error_msg(errstr,
            "Found non-basic free variable %d with name %s when calculating NBSIC. Its coefficient %f is not zero!\n",
            currvar, solver->getColName(currvar).c_str(), coeff[col]);
        writeErrorToLog(errstr, GlobalVariables::log_file);
        exit(1);
      }
    }
  }

  for (int var = 0; var < numNBSlack; var++) {
    int currvar = nonBasicSlackVarIndex[var];

    solver->getBInvACol(currvar, &basisCol[0]); // currvar is an index > numcols, so ends up same as getBInvCol(currvar - numcols, &basisCol[0])
    double ray_coeff = basisCol[splitVarRowIndex];

    //The ray component is the -a_{ij}, where a_{ij} is the entry in the tableau
    if (rowSense[currvar - numcols] == 'G') {
      ray_coeff *= -1;
    }
    if (greaterThanVal(-ray_coeff, 0.0)) {
      rayMoveSize = (pi0 + 1 - splitVarVal) / (-ray_coeff); //splitVarValue/basisCol[varBasisRowIndex];
      coeff[numNBOrig + var] = 1.0 / rayMoveSize;
      NBSIC.distAlongRay[numNBOrig + var] = rayMoveSize;
    } else {
      if (lessThanVal(-ray_coeff, 0.0)) {
        rayMoveSize = (splitVarVal - pi0) / ray_coeff; //(splitVarValue - 1)/basisCol[varBasisRowIndex];
        coeff[numNBOrig + var] = 1.0 / rayMoveSize;
        NBSIC.distAlongRay[numNBOrig + var] = rayMoveSize;
      } else {
        coeff[numNBOrig + var] = 0.0;
        NBSIC.distAlongRay[numNBOrig + var] = solver->getInfinity();
      }
    }
    if (isZero(coeff[numNBOrig + var])) {
      coeff[numNBOrig + var] = 0.0;
      NBSIC.distAlongRay[numNBOrig + var] = solver->getInfinity();
    }
  }
#ifdef TRACE
  printf("In the complemented NB space (LP relaxation is origin).\n");
  printf("\tSplit var: %d\n", NBSIC.splitVarIndex);
  printf("\tSplit var name: %s\n",
      solver->getColName(NBSIC.splitVarIndex).c_str());
  printf("\tRHS: %f\n", rhs);
  for (int i = 0; i < (int) coeff.size(); i++) {
    printf("\tCoeff %d: %f\n", i, coeff[i]);
  }
  printf("\n");
#endif

  // Set packed vector
  NBSIC.setOsiRowCut(coeff, rhs);
} /* genNBSIC */

/**
 * Add each SIC and see if the problem is feasible after adding it.
 * In the subspace, infeasibility may sometimes happen
 * (in the full space, it would prove infeasibility of the problem)
 */
void checkSICFeasibility(AdvCuts& structSIC,
    const PointCutsSolverInterface* const solver, SolutionInfo& solnInfo,
    const bool working_in_subspace) {
  printf("\n## Checking SIC Feasibility. ##\n");

  const int numCuts = structSIC.size();

  //LiftGICsSolverInterface* SICLP = new LiftGICsSolverInterface(*solver);
        PointCutsSolverInterface* SICLP = dynamic_cast<PointCutsSolverInterface*>(solver->clone());
  setClpParameters(SICLP);
  SICLP->initialSolve();
  for (int cut_ind = 0; cut_ind < numCuts; cut_ind++) {
    const int split_var = structSIC[cut_ind].splitVarIndex;

    SICLP->applyRowCuts(1, &structSIC.cuts[cut_ind]);
    SICLP->resolve();

    if (!SICLP->isProvenOptimal()) {
#ifdef TRACE
      printf("SICLP not proven optimal after adding SIC %d on var %d.\n", cut_ind, split_var);
#endif
      if (!working_in_subspace) {
        error_msg(errorstring,
            "SIC %d on var %d proves infeasibility of the problem.\n",
            cut_ind, split_var);
        writeErrorToLog(errorstring, GlobalVariables::log_file);
        exit(1);
      }

      solnInfo.infeasSplitVar.push_back(split_var);
      solnInfo.splitFeasFlag[cut_ind] = false;
      structSIC.cuts[cut_ind].postCutObj = solver->getInfinity();
    } else {
#ifdef TRACE
      printf("SICLP proven optimal after adding SIC %d on var %d.\n", cut_ind, split_var);
#endif
      // If the cut has both sides empty, then we should not consider the cut
      // Depending on YESLOP, may also not consider splits with one side empty
      bool lopBool = false;
      // If we are okay using splits in which both sides are infeasible,
      // then we can consider lopBool = true always.
      if (!working_in_subspace || USE_EMPTY) {
        lopBool = true;
      } else if (YESLOP) {
        lopBool = solnInfo.fracCoreFloorFeasFlag[cut_ind]
            || solnInfo.fracCoreCeilFeasFlag[cut_ind];
      } else {
        lopBool = solnInfo.fracCoreFloorFeasFlag[cut_ind]
            && solnInfo.fracCoreCeilFeasFlag[cut_ind];
      }
      if (lopBool
          && ((int) solnInfo.feasSplitVar.size()
              < param.paramVal[NUM_CGS_PARAM_IND])) {
        // And we add it to feasSplit
        solnInfo.feasSplitFracCoreIndex.push_back(cut_ind);
        solnInfo.feasSplitVar.push_back(split_var);
        solnInfo.splitFeasFlag[cut_ind] = true;
      } else {
        solnInfo.infeasSplitVar.push_back(split_var);
        solnInfo.splitFeasFlag[cut_ind] = false;
      }
      structSIC.cuts[cut_ind].postCutObj = SICLP->getObjValue();

#ifdef TRACE
      printf(
          "Cut %d on var %d. LP relaxation optimal value: %f. After cut optimal value: %f.\n",
          cut_ind, split_var, solver->getObjValue(), SICLP->getObjValue());
#endif
    }

    // Don't forget to remove the cut
    std::vector<int> rowsToDelete(1);
    rowsToDelete[0] = solnInfo.numRows;
    SICLP->deleteRows(1, rowsToDelete.data());
  }

  solnInfo.numFeasSplits = solnInfo.feasSplitVar.size();
        
        // Free
        if (SICLP) {
            delete SICLP;
        }
}

///**
// * Generates the points and rays used for SIC generation
// */
//void printSICPointInfo(const LiftGICsSolverInterface* const  solver,
//    const SolutionInfo & solnInfo) {
//  char pointsOutFilename[300];
//  char raysOutFilename[300];
//  snprintf(pointsOutFilename, sizeof(pointsOutFilename) / sizeof(char),
//      "%s-NB_SIC_Points.csv", LiftGICsParam::out_f_name_stub.c_str());
//  snprintf(raysOutFilename, sizeof(raysOutFilename) / sizeof(char),
//      "%s-NB_SIC_Rays.csv", LiftGICsParam::out_f_name_stub.c_str());
//
//  // Empty out the files if they exist
//  FILE *fptr = fopen(pointsOutFilename, "w");
//  fclose(fptr);
//
//  fptr = fopen(raysOutFilename, "w");
//  fclose(fptr);
//
//  // We will want to calculate how good the points we find are
//  // with respect to the objective function (which should be in NB space when appropriate)
//  // Clearly a valid inequality is c^T x \ge c^\T v,
//  // where v is the LP optimum.
//  // Since we will want to be measuring violation, we use the inequality -c^T x \ge -c^T v.
//  AdvCut objCut(false), structObjCut(false);
//  if (SHOULD_CALC_POINT_OBJ_VIOL) {
//    structObjCut.coeff.assign(solver->getObjCoefficients(),
//        solver->getObjCoefficients() + solver->getNumCols());
//    for (int c = 0; c < (int) structObjCut.coeff.size(); c++) {
//      structObjCut.coeff[c] *= -1.0;
//    }
//    structObjCut.rhs = -1.0 * solver->getObjValue();
//
//    // We use the reduced costs of the non-basic variables
//    // (which is exactly the objective in the non-basic space).
//    // Recall that the reduced cost of slack variables is exactly
//    // the negative of the row price (dual variable) for that row.
//    objCut.NBSpace = true;
//    objCut.coeff.resize(solnInfo.numNB);
//    // The rhs of the objective in the NB space will always be 0,
//    // since the optimal point is the origin.
//    objCut.rhs = 0.0;
//
//    for (int var = 0; var < (int) solnInfo.nonBasicOrigVarIndex.size(); var++) {
//      int curr_var = solnInfo.nonBasicOrigVarIndex[var];
//      objCut.coeff[var] = solver->getReducedCost()[curr_var];
//      if (isNonBasicUBVar(solver, curr_var))
//        objCut.coeff[var] *= -1.0;
//    }
//
//    const int numNBOrig = solnInfo.nonBasicSlackVarIndex.size();
//    for (int var = 0; var < numNBOrig; var++) {
//      int curr_var = solnInfo.nonBasicSlackVarIndex[var];
//      objCut.coeff[var + numNBOrig] = -1.0
//          * solver->getRowPrice()[curr_var - solnInfo.numCols];
//      if (isNonBasicUBVar(solver, curr_var))
//        objCut.coeff[var + numNBOrig] *= -1.0;
//    }
//  }
//
//  std::vector<Point> pointStore;
//  std::vector<Ray> rayStore;
//
//  // First we generate the corner polyhedron
//  error_msg(errorstring, "Not yet implemented!");
//  writeErrorToII(errorstring, LiftGICsParam::inst_info_out);
//  exit(1);
//
////  genCornerNB(point, pointFullSpace, ray, rayFullSpace, solver, solver,
////      solnInfo); // TODO
//
//  for (int var = 0; var < (int) solnInfo.fractionalCore; var++) {
//    const int currFracVar = solnInfo.fractionalCore[var];
//
//    intersectCornerWithSplit(currFracVar, point, pointFullSpace, ray,
//        rayFullSpace, solver, solnInfo);
//
//    if (SHOULD_CALC_POINT_OBJ_VIOL) {
//      // First we will calculate the violation from the objective
//      for (int point_ind = 0; point_ind < (int) point.size();
//          point_ind++) {
//        point[point_ind].setObjViolation(objCut);
//        pointFullSpace[point_ind].setObjViolation(structObjCut,
//            solnInfo.numCols);
//      }
//    }
//
//    // Print points and rays generated
//    FILE *fptr = fopen(pointsOutFilename, "a");
//    printPointStore(fptr, currFracVar, solnInfo.numNB, point);
//    fclose(fptr);
//
//    fptr = fopen(raysOutFilename, "a");
//    printRayStore(fptr, currFracVar, solnInfo.numNB, ray);
//    fclose(fptr);
//
//    fptr = fopen(pointsFullOutFilename, "a");
//    printPointStore(fptr, currFracVar, solnInfo.numRows + solnInfo.numCols,
//        pointFullSpace);
//    fclose(fptr);
//
//    fptr = fopen(raysFullOutFilename, "a");
//    printRayStore(fptr, currFracVar, solnInfo.numRows + solnInfo.numCols,
//        rayFullSpace);
//    fclose(fptr);
//
//    // We may have set some of the rays as extreme for this split, so reset all
//    for (int r = 0; r < (int) ray.size(); r++) {
//      ray[r].setExtreme(false);
//      rayFullSpace[r].setExtreme(false);
//    }
//  }
//} /* printSICPointInfo */
//
///**
// * This is a helper function that will allow us to get the points
// * from intersecting the corner relaxation with the split
// */
//void intersectCornerWithSplit(const int splitVar, std::vector<Point> & point,
//    std::vector<Point> & pointFullSpace, std::vector<Ray> & ray,
//    std::vector<Ray> & rayFullSpace, const LiftGICsSolverInterface* const  solver,
//    const SolutionInfo & solnInfo) {
//  // The first point will be the vertex of the corner relaxation
//  int raySize = (int) ray.size();
//  for (int r = 0; r < raySize; r++) {
//    double rayDist;
//    if (!intersectRayWithSplit(splitVar, &rayDist, pointFullSpace[0],
//        rayFullSpace[r], solver, solnInfo)) {
//      ray[r].setExtreme(true);
//      rayFullSpace[r].setExtreme(true);
//    } else {
//      // Add the full point
//      // We don't bother checking if the point already exists; it should not
//      Point intPointFullSpace(solnInfo.numNB);
////      intPointFullSpace.setFullCoorFromPointRay(pointFullSpace[0], // TODO
//          rayFullSpace[r], rayDist);
//      addPoint(pointFullSpace, intPointFullSpace, false);
//
//      // We also add it to the NB space collection of points
//      Point intPointCompNB(solnInfo.numNB);
////      intPointCompNB.setCompNBCoor(intPointFullSpace, solnInfo); // TODO
//      addPoint(point, intPointCompNB, false);
//    }
//  }
//} /* intersectCornerWithSplit */
//
///**
// * This will return the *distance* to the intersection point obtained from intersecting
// * Ray ray originating at Point origPoint with the boundary of the split splitVar
// * @return True if we get a point; false if this is an extreme ray
// */
//bool intersectRayWithSplit(const int splitVar, double * rayDist,
//    const Point & origPoint, const Ray & rayOut,
//    const LiftGICsSolverInterface* const  solver, const SolutionInfo & solnInfo) {
//  *rayDist = solver->getInfinity();
//
//  double ptVal = 0.0, rayDirn = 0.0;
//  ptVal = origPoint[splitVar];
//  if (splitVar <= rayOut.getMaxIndex())
//    rayDirn = rayOut[splitVar];
//
//  if (std::abs(rayDirn) < param.getEPS())
//    return false;
//
//  double UB = std::ceil(ptVal);
//  double LB = std::floor(ptVal);
//
//  // We have x_i + t r_i^j = l_j or g_j
//  // depending on the direction of the ray
//  // (or off to infinity)
//  if (rayDirn > param.getEPS()) {
//    *rayDist = (UB - ptVal) / rayDirn;
//  } else {
//    // Otherwise, the ray direction is negative
//    // (We would have returned before otherwise)
//    *rayDist = (LB - ptVal) / rayDirn;
//  }
//
//  if (*rayDist < solver->getInfinity() - param.getEPS())
//    return true;
//  else
//    return false;
//} /* intersectRayWithSplit */
