/*
 * hyperplaneActivation.hpp
 *
 *  Created on: Jun 12, 2012
 *      Author: selva
 */

#ifndef HYPERPLANEACTIVATION_HPP_
#define HYPERPLANEACTIVATION_HPP_
/**COIN header*/
#include "OsiSolverInterface.hpp"
#include "typedefs.hpp"
#include <vector>
#include "Utility.hpp"
#include "GlobalConstants.hpp"
#include "chooseCutGeneratingSets.hpp"
#include "Output.hpp"
#include "SolutionInfo.hpp"
#include "intersectionPointsAndRays.hpp"

using namespace std;

void PHAOneOneBestAvgDepth(const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo,
    const chooseCutGeneratingSets& chooseSplits, const vector<cut> &NBSIC,
    vector<int> &allRaysCut, vector<vector<int> > &raysToBeCut,
    int numRaysToBeCut, vector<vector<vector<int> > > &raysToBeCutByHplane,
    vector<vector<int> > &strongVertHplaneIndex,
    vector<vector<vector<int> > > &allRaysIntersectedByHplane,
    vector<vector<int> > &hplanesIndexToBeActivated,
    vector<vector<int> > &hplanesRowIndexToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    int &num_hplanes_activated, int max_hplanes_activated,
    FILE* inst_info_out);

void findHplaneToIntersectRayBestAvgDepth(const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo,
    const chooseCutGeneratingSets& chooseSplits, const int rayIndex,
    const int nonBasicColIndex, int &hplaneVarIndexToAct,
    int &hplaneVarRowIndexToAct, int &hplaneVarUpperFlagToAct,
    double &minSlackRatio, const vector<int> &splitsEffByHplane,
    const std::vector<int>& allRayIndices, const std::vector<cut>& NBSIC,
    FILE *inst_info_out);

double fakeActivate(const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo,
    const chooseCutGeneratingSets& chooseSplits, const int split_ind,
    const int ray_ind, const int hplaneVarIndex,
    const int hplaneVarRowIndex, const int hplaneUBFlag, const cut& NBSIC,
    const std::vector<int>& allRayIndices, FILE* inst_info_out);

void PHASecondHplane(std::vector<std::vector<int> >& hplaneVarIndexToActivate,
    std::vector<std::vector<int> >& hplaneVarRowIndexToActivate,
    std::vector<std::vector<int> >& hplaneVarUBFlagToActivate,
    std::vector<int>& allRaysCutActivation2,
    std::vector<std::vector<int> >& raysToBeCutActivation2,
    std::vector<std::vector<std::vector<int> > >& raysToBeCutByHplaneActivation2,
    std::vector<std::vector<std::vector<int> > >& allRaysIntersectedByHplaneActivation2,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    const chooseCutGeneratingSets& chooseSplits,
    CoinPackedMatrix** &interPtsAndRays,
    const std::vector<std::vector<int> >& pointOrRayFlag,
    const std::vector<int>& numPointsOrRays,
    const std::vector<std::vector<ptOrRayInfo*> >& intPtOrRayInfo,
    const std::vector<std::vector<std::vector<int> > >& partitionOfPtsAndRaysByRaysOfC1,
    const std::vector<std::vector<int> >& oldHplaneVarIndexToActivate,
    const std::vector<std::vector<int> >& oldHplaneVarRowIndexToActivate,
    const std::vector<std::vector<int> >& oldHplaneVarUBFlagToActivate,
    const std::vector<std::vector<int> >& oldRaysToBeCut,
    const std::vector<std::vector<std::vector<int> > >& oldRaysCutByHplane,
    const std::vector<std::vector<int> > strongVertHplaneIndex,
    FILE* inst_info_out);

void findHplaneToIntersectRayBestAvgDepth(const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo,
    const chooseCutGeneratingSets& chooseSplits, const int nonBasicColIndex,
    int &hplaneBasicVarIndex, int &hplaneBasicVarRowIndex,
    int &hplaneBasicVarUpperFlag, double &minSlackRatio,
    const vector<int> &splitsEffByHplane,
    const std::vector<int>& allRayIndices, const std::vector<cut>& NBSIC,
    FILE *inst_info_out);

int PHASecondHplaneHelper(std::vector<int>& tmpRaysToBeCut,
    std::vector<int>& tmpRaysCutByHplane,
    std::vector<int>& tmpAllRaysIntersected, const int hplaneVarToAct,
    const int hplaneVarRowToAct, const int hplaneVarUBFlagToAct,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    const int split_ind, const chooseCutGeneratingSets& chooseSplits,
    CoinPackedMatrix** &interPtsAndRays,
    const std::vector<std::vector<int> >& pointOrRayFlag,
    const std::vector<int>& numPointsOrRays,
    const std::vector<std::vector<ptOrRayInfo*> >& intPtOrRayInfo,
    const std::vector<std::vector<std::vector<int> > >& partitionOfPtsAndRaysByRaysOfC1,
    const std::vector<int>& firstHplane,
    const std::vector<int>& splitsEffByHplane,
    const std::vector<std::vector<int> >& oldHplaneVarIndexToActivate,
    const std::vector<std::vector<int> >& oldHplaneVarRowIndexToActivate,
    const std::vector<std::vector<int> >& oldHplaneVarUBFlagToActivate,
    FILE* inst_info_out, const int rayInd = -1);

int numPointsOfRayViolatingHplane(
    std::vector<std::vector<int> >& pointsToRemove, const int rayInd,
    const int hplaneVarIndex, const int hplaneVarRowIndex,
    const int hplaneUBFlag, CoinPackedMatrix**& interPtsAndRays,
    const std::vector<std::vector<ptOrRayInfo*> >& intPtOrRayInfo,
    const std::vector<std::vector<std::vector<int> > >& partitionOfPtsAndRaysByRaysOfC1,
    const chooseCutGeneratingSets& chooseSplits,
    const std::vector<int> & splitsEffByHplane,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo);

void PHAOneOne(const int hplaneHeur, const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo,
    const chooseCutGeneratingSets& chooseSplits, const vector<cut> &NBSIC,
    vector<int> &allRaysCut, vector<vector<int> > &raysToBeCut,
    const int numRaysToBeCut,
    vector<vector<vector<int> > > &raysToBeCutByHplane,
    vector<vector<int> > &strongVertHplaneIndex,
    vector<vector<vector<int> > > &allRaysIntersectedByHplane,
    vector<vector<int> > &hplanesIndexToBeActivated,
    vector<vector<int> > &hplanesRowIndexToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    int &num_hplanes_activated, int max_hplanes_activated,
    FILE* inst_info_out);

void findHplaneToIntersectRay(const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo,
    const chooseCutGeneratingSets& chooseSplits, const int numSplits,
    const int nonBasicColIndex, const vector<double> &basicCol,
    int &hplaneBasicVarIndex, int &hplaneBasicVarRowIndex,
    int &hplaneBasicVarUpperFlag, double &minSlackRatio,
    vector<int> &splitsEffByHplane, FILE *inst_info_out);

void findHplaneMinWeakVert(const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo,
    const chooseCutGeneratingSets& chooseSplits, const int numSplits,
    const int nonBasicColIndex, const int JIndex,
    const vector<double> &basicCol, int &hplaneBasicVarIndex,
    int &hplaneBasicVarRowIndex, int &hplaneBasicVarUpperFlag,
    double &minSlackRatio, vector<int> &splitsEffByHplane,
    vector<vector<int> > &raysToBeCut, vector<vector<bool> > &chosenHplanes,
    vector<vector<int> > &numRaysCutByHplane,
    vector<vector<int> > &activatedUBFlag, FILE *inst_info_out);

void addRaysCutByHplaneBeforeBdS(const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo, const int splitVarIndex,
    const int splitRowIndex, const double pi0, const int hplaneIndex,
    const int hplaneRowIndex, const int hplaneUBFlag,
    const vector<int>& raysToBeCut, vector<int> &raysCutByHplane,
    const int rayIndex, FILE* inst_info_out);

void addRayToHplanesIntersectedBeforeBdS(PointCutsSolverInterface* solver,
    int oldHplane, SolutionInfo solnInfo, int splitVarIndex,
    int splitRowIndex, double pi0, int rayIndex,
    vector<int> &hplanesIndexToBeActivated,
    vector<int> &hplanesRowIndexToBeActivated,
    vector<int> &hplanesToBeActivatedUBFlag,
    vector<vector<int> > &raysToBeCutByHplane, FILE* inst_info_out);

int hasHplaneBeenActivated(const vector<int> &hplanesIndxToBeActivated,
    const vector<int> &hplaneBasicVarUpperFlag, int hplanIndx, int UBFlag);

int isRayCutByHplaneOnSplit(const int splitIndex, const int rayIndex,
    std::vector<int> &hplanesIndexToBeActivated,
    std::vector<int> &hplaneBasicVarUpperFlag, const int tmpHplaneIndex,
    const int UBFlag);

void printActivatedHplaneDetails(FILE* fptr, int numSplits,
    chooseCutGeneratingSets &chooseSplits,
    vector<vector<vector<int> > > &raysToBeCutByHplane,
    vector<vector<vector<int> > > &allRaysCutByHplane,
    vector<vector<int> > &hplanesIndxToBeActivated,
    vector<vector<int> > &hplanesRowIndxToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag);

#endif /* HYPERPLANEACTIVATION_HPP_ */
