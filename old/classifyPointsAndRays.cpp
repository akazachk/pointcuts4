/*
 * classifyPointsAndRays.cpp
 *
 *  Created on: Jul 3, 2012
 *      Author: selva
 */

#include "classifyPointsAndRays.hpp"

//void paritionRaysOfC1(int numSplits, vector<vector<int> > intermPts, vector<
//    vector<int> > intermRays, vector<vector<int> > finalPts, vector<vector<
//    int> > finalRays) {
//
//  for (int s = 0; s < numSplits; s++) {
//
//  }
//}

/**
 * @brief Identifies which rays can be cut.
 *
 * @param interPtObjVal     ::  Max over all splits of distance gone before ray intersects bd of the split.
 *                          ::    -1 if there exists split for which it does not intersect.
 * @param interPtObjIndex   ::  The index of the ray. -1 if there exists a split which the ray is parallel to.
 * @param interPtCutSplit   ::  Which split (index in vector splitVarIndex) yields the value in interPtObjVal.
 *                          ::    -1 if the ray is parallel to some split.
 * @param raysThatMustBeCut ::  Vector of all rays with true if intersects bd of all splits, false o/w.
 */
void identifyInterPtObjs(PointCutsSolverInterface* solver, int numSplits,
    int nNonBasicCols, vector<double> &interPtObjVal,
    vector<int> &interPtObjIndex, vector<int> &interPtCutSplit,
    vector<int> &nonBasicVarIndices, vector<vector<double> > &raysOfC1,
    vector<double> &pi0, vector<int> &splitVarIndex,
    vector<int> &splitRowIndx_t, vector<bool> &raysThatMustBeCut) {

  double ptVal;
  //, ptVal1,
  double deepestVal;
  const double* colSoln = solver.getColSolution();
//  bool ptCut = false;
  int deepestSplitIndx;
  raysThatMustBeCut.resize(nNonBasicCols);
  fill(raysThatMustBeCut.begin(), raysThatMustBeCut.end(), false);
  bool ray = true;

  // For each possible ray that could be cut
  for (int r = 0; r < nNonBasicCols; r++) {
    // deepestVal keeps track of the farthest this ray has to go before intersecting some split bd.
    deepestVal = 0.0;
    deepestSplitIndx = -1;
    ray = false;
    // In every split, check whether the ray intersects bd S
    // If yes, raysThatMustBeCut[r] = true; o/w = false.
    for (int s = 0; s < numSplits; s++) {
      // Let splitRowIndx_t[s] = i.
      // raysOfC1[r][j] = -\bar a_{ir}, so if this is positive,
      // then \bar a_{i0} + \lambda * raysOfC1[r][j] = pi0[s] + 1
      // has a solution with \lambda positive.
      // If raysOfC1[r][j] < 0, then the ray will intersect pi0[s].
      if (raysOfC1[r][splitRowIndx_t[s]] >= param.getEPS()) {
        ptVal = (pi0[s] + 1 - colSoln[splitVarIndex[s]])
            / (raysOfC1[r][splitRowIndx_t[s]]);
      } else {
        if (raysOfC1[r][splitRowIndx_t[s]] <= -param.getEPS()) {
          ptVal = (pi0[s] - colSoln[splitVarIndex[s]])
              / (raysOfC1[r][splitRowIndx_t[s]]);
        } else {
          ray = true;
          break;
        }
      }
      if (ptVal >= deepestVal + param.getEPS()) {
        deepestVal = ptVal;
        deepestSplitIndx = s;
      }
    }
    if (!ray) {
      interPtObjVal.push_back(deepestVal);
      interPtObjIndex.push_back(r);
      interPtCutSplit.push_back(deepestSplitIndx);
      raysThatMustBeCut[r] = true;
    } else {
      interPtObjVal.push_back(-1.0);
      interPtObjIndex.push_back(-1);
      interPtCutSplit.push_back(-1);
      raysThatMustBeCut[r] = false;
    }

  }
}
