/*
 * chooseCutGeneratingSets.cpp
 *
 *  Created on: Jun 12, 2012
 *      Author: selva (modified by Aleksandr M. Kazachkov)
 */

#include "chooseCutGeneratingSets.hpp"

chooseCutGeneratingSets::chooseCutGeneratingSets() {
  // TODO Auto-generated constructor stub
  nEffCutGenSets = 0;
}

chooseCutGeneratingSets::~chooseCutGeneratingSets() {
  // TODO Auto-generated destructor stub
}

void chooseCutGeneratingSets::chooseSplits(const PointCutsSolverInterface* const solver,
    const SolutionInfo &solnInfo, const std::vector<int> &oldRowIndices) {
  nEffCutGenSets = 0;
  int numFeasSplits = solnInfo.feasSplitVar.size();
  pi0.resize(numFeasSplits);
  splitVarIndex.resize(numFeasSplits);
  splitVarRowIndex.resize(numFeasSplits);
  splitVarOldRowIndex.resize(numFeasSplits);
  splitVarValue.resize(numFeasSplits);
  splitVarName.resize(numFeasSplits);
  for (int i = 0; i < numFeasSplits; i++) {
    int currvar = solnInfo.feasSplitVar[i];
    splitVarIndex[nEffCutGenSets] = currvar;
    splitVarRowIndex[nEffCutGenSets] = solnInfo.rowOfVar[currvar];
    splitVarOldRowIndex[nEffCutGenSets] =
        oldRowIndices[splitVarRowIndex[nEffCutGenSets]];
    splitVarValue[nEffCutGenSets] = solver->getColSolution()[currvar];
    std::string currname = solver->getColName(currvar);
    if (currname.empty())
      currname = "unnamed";
    splitVarName[nEffCutGenSets] = currname;
    pi0[nEffCutGenSets] = floor(splitVarValue[nEffCutGenSets]);
    nEffCutGenSets++;
  }
}
