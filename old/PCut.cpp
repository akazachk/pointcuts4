//============================================================================
// Name        : PCut.cpp
// Author      : Aleksandr M. Kazachkov
// Version     : 0.2014.03.18
// Copyright   : Your copyright notice
// Description : Generates point cuts
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#include "PCut.hpp"

/**
 * Generates point cuts based on option provided.
 *   corner - generates only rays corresponding to non-basic vars
 *   cornerNB - does corner in NB space
 *   degencorner - generates all rays from the starting vertex
 *
 *   @return number cuts generated
 */
int genPCuts(const std::string option, AdvCuts & structPCuts,
    const PointCutsSolverInterface* const  solver, const ProblemData & probData,
    const AdvCuts & structSICs, const AdvCuts & NBSICs,
    const bool working_in_subspace) {
  // Decide if we are in the non-basic space, based on option
  bool inNBSpace = false;
  int dim = solver.getNumCols();
  if (option == "cornerNB") {
    inNBSpace = true;
    dim = probData.numNB;
  }

  // Calculate how good the points we find are
  // with respect to the objective function (which should be in NB space when appropriate)
  // Clearly a valid inequality is c^T x \ge c^\T v,
  // where v is the LP optimum.
  AdvCut objCut(false), structObjCut(false);
  if (PTCONST::SHOULD_CALC_POINT_OBJ_VIOL) {
    structObjCut.coeff.assign(solver.getObjCoefficients(),
        solver.getObjCoefficients() + solver.getNumCols());
    structObjCut.rhs = solver.getObjValue();

    // Either we have the objective, or we have the reduced costs
    // of the non-basic variables (which is exactly the objective
    // in the non-basic space).
    // Recall that the reduced cost of slack variables is exactly
    // the negative of the row price (dual variable) for that row.
    if (!inNBSpace) {
      objCut.assign(structObjCut);
    } else {
      objCut.NBSpace = true;
      objCut.coeff.resize(probData.numNB);
      // The rhs of the objective in the NB space will always be 0,
      // since the optimal point is the origin.
      objCut.rhs = 0.0;

      for (int var = 0; var < probData.numNBOrig; var++) {
        int curr_var = probData.nonBasicOrigVarIndex[var];
        objCut.coeff[var] = solver.getReducedCost()[curr_var];
        if (isNonBasicUBVar(solver, curr_var))
          objCut.coeff[var] *= -1.0;
      }

      for (int var = 0; var < probData.numNBSlack; var++) {
        int curr_var = probData.nonBasicSlackVarIndex[var];
        objCut.coeff[var + probData.numNBOrig] = -1.0
            * solver.getRowPrice()[curr_var - probData.numCols];
        if (isNonBasicUBVar(solver, curr_var))
          objCut.coeff[var + probData.numNBOrig] *= -1.0;
      }
    }
//    if (inNBSpace) {
//      AdvCut NBObjCut(inNBSpace);
//      AdvCut::convertStructSpaceCutToJSpace(NBObjCut, solver,
//          probData.rowOfVar, probData.nonBasicOrigVarIndex,
//          probData.nonBasicSlackVarIndex, ObjCut, true);
//      ObjCut.assign(NBObjCut);
//    }
  }

  int num_cuts_generated = 0, num_new_cuts = 0;
  int total_num_points = 0, total_num_rays = 0;

  char pointsOutFilename[300];
  char raysOutFilename[300];
  char pointsFullOutFilename[300];
  char raysFullOutFilename[300];
  char pointRaySummaryOutFilename[300];
  snprintf(pointRaySummaryOutFilename,
      sizeof(pointRaySummaryOutFilename) / sizeof(char),
      "%s-pointRaySummary.csv");
  if (PTCONST::SHOULD_WRITE_POINTS_RAYS) {
    if (inNBSpace) {
      snprintf(pointsOutFilename,
          sizeof(pointsOutFilename) / sizeof(char),
          "%s-NB_Points.csv",
          PointCutsParam::out_f_name_stub.c_str());
      snprintf(raysOutFilename, sizeof(raysOutFilename) / sizeof(char),
          "%s-NB_Rays.csv", PointCutsParam::out_f_name_stub.c_str());
    } else {
      snprintf(pointsOutFilename,
          sizeof(pointsOutFilename) / sizeof(char),
          "%s-Struct_Points.csv",
          PointCutsParam::out_f_name_stub.c_str());
      snprintf(raysOutFilename, sizeof(raysOutFilename) / sizeof(char),
          "%s-Struct_Rays.csv",
          PointCutsParam::out_f_name_stub.c_str());
    }
    snprintf(pointsFullOutFilename,
        sizeof(pointsFullOutFilename) / sizeof(char),
        "%s-Full_Points.csv", PointCutsParam::out_f_name_stub.c_str());
    snprintf(raysFullOutFilename,
        sizeof(raysFullOutFilename) / sizeof(char), "%s-Full_Rays.csv",
        PointCutsParam::out_f_name_stub.c_str());

    // Empty out the files if they exist
    FILE *fptr = fopen(pointsOutFilename, "w");
    fclose(fptr);

    fptr = fopen(raysOutFilename, "w");
    fclose(fptr);

    fptr = fopen(pointsFullOutFilename, "w");
    fclose(fptr);

    fptr = fopen(raysFullOutFilename, "w");
    fclose(fptr);
  }

  // For each variable in the fractional core, we get cuts
  for (int i = 0; i < (int) probData.getFractionalCore().size(); i++) {
    int currFracVar = probData.getFractionalCore()[i];
    double xk = solver.getColSolution()[currFracVar];
    double floorxk = std::floor(xk);
    double ceilxk = std::ceil(xk);

    // We will first check if both sides of the split are feasible
    // If one is infeasible, we simply add the strongest cut available
    // which is xk <= floorxk or xk >= ceilxk (whichever is the feasible side),
    // and we skip the computationally intensive step of generating points and rays.
    //
    // In the full space, assuming feasibility of the original problem,
    // at least one side of the split will be feasible.
    // In the subspace, we may end up with both sides of the split being infeasible.
    // For the time being, we will skip these splits.
    bool floorFeas = true, ceilFeas = true;

    PointCutsSolverInterface tmpSolver(solver);
    setMessageHandler(tmpSolver);

    tmpSolver.setColLower(currFracVar, floorxk);
    tmpSolver.setColUpper(currFracVar, floorxk);
    tmpSolver.initialSolve();

//    {
//      char tmpname[300];
//      snprintf(tmpname, sizeof(tmpname) / sizeof(char),
//          "%s-NBFloor%d.csv", PointCutsParam::out_f_name_stub.c_str(),
//          currFracVar);
//      FILE *tmpOut = fopen(tmpname, "w");
//      printNBSimplexTableauWithNames(tmpOut, tmpSolver);
//      fclose(tmpOut);
//    }

    if (tmpSolver.isProvenPrimalInfeasible()) {
      floorFeas = false;
    } else if (tmpSolver.isProvenDualInfeasible()) {
      // It is then unbounded in this direction?
      // Shouldn't happen.
      error_msg(errstr,
          "Solver is dual infeasible when variable %d with name %s and value %f is fixed to floor %f.\n",
          currFracVar, solver.getColName(currFracVar).c_str(), xk,
          floorxk);
      writeErrorToII(errstr, PointCutsParam::inst_info_out);
      exit(1);
    } else if (!tmpSolver.isProvenOptimal()) {
      // Sometimes solver may not detect that it is primal infeasible?
      // We should check to see when this happens
      error_msg(errstr,
          "Solver is not proven optimal when variable %d with name %s and value %f is fixed to floor %f. Check why this happened!\n",
          currFracVar, solver.getColName(currFracVar).c_str(), xk,
          floorxk);
      writeErrorToII(errstr, PointCutsParam::inst_info_out);
      exit(1);
    }

    tmpSolver.setColLower(currFracVar, ceilxk);
    tmpSolver.setColUpper(currFracVar, ceilxk);
    tmpSolver.initialSolve();

//    {
//      char tmpname[300];
//      snprintf(tmpname, sizeof(tmpname) / sizeof(char), "%s-NBCeil%d.csv",
//          PointCutsParam::out_f_name_stub.c_str(), currFracVar);
//      FILE *tmpOut = fopen(tmpname, "w");
//      printNBSimplexTableauWithNames(tmpOut, tmpSolver);
//      fclose(tmpOut);
//    }

    if (tmpSolver.isProvenPrimalInfeasible()) {
      ceilFeas = false;
    } else if (tmpSolver.isProvenDualInfeasible()) {
      // It is then unbounded in this direction?
      // Shouldn't happen.
      error_msg(errstr,
          "Solver is dual infeasible when variable %d with name %s and value %f is fixed to ceil %f.\n",
          currFracVar, solver.getColName(currFracVar).c_str(), xk,
          ceilxk);
      writeErrorToII(errstr, PointCutsParam::inst_info_out);
      exit(1);
    } else if (!tmpSolver.isProvenOptimal()) {
      // Sometimes solver may not detect that it is primal infeasible?
      // We should check to see when this happens
      error_msg(errstr,
          "Solver is not proven optimal when variable %d with name %s and value %f is fixed to ceil %f. Check why this happened!\n",
          currFracVar, solver.getColName(currFracVar).c_str(), xk,
          ceilxk);
      writeErrorToII(errstr, PointCutsParam::inst_info_out);
      exit(1);
    }

    // If both sides are infeasible, and we are working in the subspace, we simply skip this split.
    // If not in subspace, then the entire problem is then infeasible.
    // For the splits that are infeasible on one side, just add the strongest cut available,
    // which fixes the integer variable to the other side.
    if (!floorFeas && !ceilFeas) {
      if (!working_in_subspace) {
        error_msg(errstr,
            "Solver is primal infeasible when variable %d with name %s and value %f is fixed to floor %f and when it is fixed to ceil %f.\n",
            currFracVar, solver.getColName(currFracVar).c_str(), xk,
            floorxk, ceilxk);
        writeErrorToII(errstr, PointCutsParam::inst_info_out);
        exit(1);
      }
    } else if (!floorFeas || !ceilFeas) {
      bool fixFloor = floorFeas;
      bool added = addOneSidedCut(structPCuts, fixFloor, solver,
          currFracVar);
      if (added) {
#ifdef TRACE
        printf("\n## Generated one sided pcut for split var %d. ##\n",
            currFracVar);
#endif
        num_new_cuts = 1;
      } else {
        num_new_cuts = 0;
      }
    } else {
      // If both sides are feasible, we proceed
      std::vector<Point> point, pointFullSpace;
      std::vector<Ray> ray, rayFullSpace;

      genPCutCeil(option, point, pointFullSpace, ray, rayFullSpace,
          tmpSolver, solver, probData, currFracVar);

      // We need to obtain the floor solver again
      tmpSolver.setColLower(currFracVar, floorxk);
      tmpSolver.setColUpper(currFracVar, floorxk);
      tmpSolver.initialSolve(); // We now know it will be optimal, since we got this far
      genPCutFloor(option, point, pointFullSpace, ray, rayFullSpace,
          tmpSolver, solver, probData, currFracVar);

      total_num_points += point.size();
      total_num_rays += ray.size();

      if (PTCONST::SHOULD_WRITE_POINTS_RAYS) {
        if (PTCONST::SHOULD_CALC_POINT_OBJ_VIOL) {
          // First we will calculate the violation from the objective
          for (int point_ind = 0; point_ind < (int) point.size();
              point_ind++) {
            point[point_ind].setObjViolation(objCut);
            pointFullSpace[point_ind].setObjViolation(structObjCut,
                probData.numCols);
          }
        }

        // Print points and rays generated
//          printSparsePointsAndRays(stdout, point, ray);
        FILE *fptr = fopen(pointsOutFilename, "a");
        printPointStore(fptr, currFracVar, dim, point);
        fclose(fptr);

        fptr = fopen(raysOutFilename, "a");
        printRayStore(fptr, currFracVar, dim, ray);
        fclose(fptr);

        fptr = fopen(pointsFullOutFilename, "a");
        printPointStore(fptr, currFracVar,
            probData.numRows + probData.numCols, pointFullSpace);
        fclose(fptr);

        fptr = fopen(raysFullOutFilename, "a");
        printRayStore(fptr, currFracVar,
            probData.numRows + probData.numCols, rayFullSpace);
        fclose(fptr);
      }

      /***********************************************************************************
       * Generate cuts from points and rays for this split
       ***********************************************************************************/
      if (!inNBSpace) {
        num_new_cuts = genCutsFromPointsRays(structPCuts, solver,
            probData, point, ray, currFracVar);
      } else {
        num_new_cuts = genCutsFromPointsRaysNB(structPCuts, solver,
            probData, point, ray, currFracVar);
      }
    }
    num_cuts_generated += num_new_cuts;
//    {
//      for (int ind = 0; ind < num_new_cuts; ind++)
//        pcuts.insert(pcuts.cuts[pcuts.size() - ind - 1]);
//
//      if (num_new_cuts > 0) {
//        LiftGICsSolverInterface pCutSolver(solver);
//        OsiSolverInterface::ApplyCutsReturnCode code =
//            pCutSolver.applyCuts(pcuts);
//
//        printf("PCuts applied: %d.\n", code.getNumApplied());
//
//        printf("\n## Testing these new cuts on split var %d. ##\n",
//            currFracVar);
//        pCutSolver.initialSolve();
//
//        if (pCutSolver.isProvenPrimalInfeasible()) {
//          error_msg(errstr,
//              "Solver is primal infeasible when adding pcuts for variable %d with name %s and value %f.\n",
//              currFracVar, solver.getColName(currFracVar).c_str(),
//              xk);
//          writeErrorToII(errstr, PointCutsParam::inst_info_out);
//
//          // Print the devious cuts
//          for (int ind = 0; ind < num_new_cuts; ind++) {
//            pcuts.cuts[ind].print();
//          }
//          exit(1);
//        }
//      }
//    }
  }

  structPCuts.setOsiCuts();

  if (PTCONST::SHOULD_WRITE_PCUTS) {
    structPCuts.printCuts("PCuts", solver, probData);
  }

#ifdef TRACE
  printf("\n## Total num points: %d. Total num rays: %d. ##\n",
      total_num_points, total_num_rays);
#endif

  return num_cuts_generated;
}

/**
 * Generates point cuts from floor for col based on option provided.
 *   1. degencorner - generates all rays from the starting vertex
 *   2. corner - generates only rays corresponding to non-basic vars
 *   3.
 */
void genPCutFloor(const std::string option, std::vector<Point>& point,
    std::vector<Point>& pointFullSpace, std::vector<Ray>& ray,
    std::vector<Ray>& rayFullSpace, const PointCutsSolverInterface* const  tmpSolver,
    const PointCutsSolverInterface* const  origSolver,
    const ProblemData & origProbData, const int splitVar) {
#ifdef TRACE
  printf(
      "\n## Generating points and rays from floor of variable %d with name %s.\n",
      splitVar, tmpSolver.getColName(splitVar).c_str());
#endif
  if (option == "corner") {
    genCorner(point, ray, tmpSolver);
  } else if (option == "cornerNB") {
    genCornerNB(point, pointFullSpace, ray, rayFullSpace, tmpSolver,
        origSolver, origProbData);
  } else if (option == "degencorner") {
    genDegenCorner(point, ray, tmpSolver, origProbData);
  } else {
    error_msg(errstr, "Option %s is not recognized.\n", option.c_str());
    writeErrorToII(errstr, PointCutsParam::inst_info_out);
    exit(1);
  }
}

/**
 * Generates point cuts from ceiling for col based on option provided.
 *   1. degencorner - generates all rays from the starting vertex
 *   2. corner - generates only rays corresponding to non-basic vars
 *   3.
 */
void genPCutCeil(const std::string option, std::vector<Point>& point,
    std::vector<Point>& pointFullSpace, std::vector<Ray>& ray,
    std::vector<Ray>& rayFullSpace, const PointCutsSolverInterface* const  tmpSolver,
    const PointCutsSolverInterface* const  origSolver,
    const ProblemData & origProbData, const int splitVar) {
#ifdef TRACE
  printf(
      "\n## Generating points and rays from ceil of variable %d with name %s.\n",
      splitVar, tmpSolver.getColName(splitVar).c_str());
#endif
  if (option == "corner") {
    genCorner(point, ray, tmpSolver);
  } else if (option == "cornerNB") {
    genCornerNB(point, pointFullSpace, ray, rayFullSpace, tmpSolver,
        origSolver, origProbData);
  } else if (option == "degencorner") {
    genDegenCorner(point, ray, tmpSolver, origProbData);
  } else {
    error_msg(errstr, "Option %s is not recognized.\n", option.c_str());
    writeErrorToII(errstr, PointCutsParam::inst_info_out);
    exit(1);
  }
}

/**
 * Get Point and Rays from corner polyhedron defined by current optimum at solver
 * Assumed to be optimal already
 */
void genCorner(std::vector<Point>& point, std::vector<Ray>& ray,
    const PointCutsSolverInterface* const solver) {
  solver.enableFactorization();
  // Ensure solver is optimal
  if (!solver.isProvenOptimal()) {
    error_msg(errstr, "Solver is not proven optimal.\n");
    writeErrorToII(errstr, PointCutsParam::inst_info_out);
    exit(1);
  }

  /***********************************************************************************
   * The first point is simply the first vertex
   ***********************************************************************************/
  addPoint(point, solver.getNumCols(), solver.getColSolution());

  /***********************************************************************************
   * We get the rays corresponding to non-basic variables
   ***********************************************************************************/
  // We could get this information by creating a ProblemData for this solver, but that seems too expensive
  // All we need is the number of rays we will have
  // This is the number of non-basic variables at their upper or lower bounds, including slack variables
  std::vector<int> cstat(solver.getNumCols()), rstat(solver.getNumRows());
  solver.getBasisStatus(&cstat[0], &rstat[0]);
  std::vector<int> varBasicInRow(solver.getNumRows());
  solver.getBasics(&varBasicInRow[0]);
//  const char* rowSense = solver.getRowSense();

  // We also will want to know what row each var is basic in
  // This will be useful to extract the coefficients of the rays quickly
  std::vector<int> basicStructVar;
  std::vector<int> rowOfStructVar(solver.getNumCols(), -1);

  // Gather which are the non-basic variables for this solver
//  int numRays = 0, numNBFree;
  std::vector<int> nonBasicVarIndex, nonBasicFreeIndexInNBVars;
  nonBasicVarIndex.reserve(solver.getNumCols()); // There may be fewer if we have equality constraints
  for (int var = 0; var < solver.getNumCols() + solver.getNumRows(); var++) {
    // If this is a basic structural variable, store its index
    if (var < solver.getNumCols()) {
      if (isBasicCol(solver, var))
        basicStructVar.push_back(var);
    } else { // If this is a row and the basic var is structural, store its row
      int row = var - solver.getNumCols();
      int basicVar = varBasicInRow[row];
      if (basicVar < solver.getNumCols())
        rowOfStructVar[basicVar] = row;
    }

    // If it is a ray we want to capture, add it to the ray index list
    if (isRayVar(solver, var)) {
      if (isNonBasicFreeVar(solver, var)) {
        nonBasicFreeIndexInNBVars.push_back(
            (int) nonBasicVarIndex.size());
      }
      nonBasicVarIndex.push_back(var);
    }
  }
//  numNBFree = (int) nonBasicFreeIndexInNBVars.size();
//  numRays = (int) nonBasicVarIndex.size() + numNBFree; // The NB free vars contribute two rays

  // We now store the packed vector for the corner polyhedron rays.
  // For each non-basic var in nonBasicVarIndex, we get Binv * A.
  // Since we are looking at x_B = \bar a_0 - \bar A_N x_N,
  // the rays should be negative of Binv * A.
  // Exception is when the variable is non-basic at its upper bound.
  // In that case, ray is negated, which is equivalent to taking Binv * A as-is.
  // For free variables, we need both the ray from Binv * A and its negation.
  // For fixed variables, there is no need to get a ray, since we would not be able
  // to increase along that direction anyway.
  // Finally, considering the slack variables, if the row is
  //  ax <= b
  // then Clp stores a slack of the form
  //  ax + s = b, s >= 0
  // in which case we need to again take -Binv * A.
  // If the row is
  //  ax >= b
  // then Clp stores a slack of the form
  //  ax + s = b, s <= 0
  // in which case we already have the negative by using Binv * A.
  // This is because the Clp version is an upper-bounded variable,
  // so we have to negate.
  //
  // For the rows that are >=, if the variable basic in the row is slack,
  // then we have it be a_i - s = b_i, so we move everything to the other side.
  // I.e., we negate the *row* of Binv * A.
  std::vector<int> structRayIndex;
  std::vector<double> structRayElem;
  std::vector<double> currRay(solver.getNumRows());
  for (int j = 0; j < (int) nonBasicVarIndex.size(); j++) {
    int NBVar = nonBasicVarIndex[j];

    // If NBVar is structural, we need to insert it too
    bool toInsert = NBVar < solver.getNumCols();
    double rayMult = 1.0;
    if (isNonBasicUBVar(solver, NBVar))
      rayMult = -1.0;

    // Get the ray corresponding to this NBVar
    solver.getBInvACol(NBVar, &currRay[0]);

    // We now loop through the basic structural variables to get their ray components
    for (int c = 0; c < (int) basicStructVar.size(); c++) {
      int BVar = basicStructVar[c];

      // We should insert (in the appropriate place) the NBVar if it is a structural one
      if (toInsert && NBVar > BVar) {
        structRayIndex.push_back(NBVar);
        structRayElem.push_back(rayMult);
        toInsert = false;
      }

      int rowOfBVar = rowOfStructVar[BVar]; // Should not be -1
      if (rowOfBVar == -1) {
        error_msg(errstr,
            "Var %d (basic struct var index %d) is not basic in any row.\n",
            BVar, c);
        writeErrorToII(errstr, PointCutsParam::inst_info_out);
        exit(1);
      }

      // Default is -Binv * A
      // If NBVar is at ub, we have to negate the ray
      double rayVal = rayMult * -1 * currRay[rowOfBVar];
      if (std::abs(rayVal) > PTCONST::param.getEPS()) {
        structRayIndex.push_back(BVar);
        structRayElem.push_back(rayVal);
      }
    }

    // If we have reached the end of the loop and did not insert the struct NBVar, do so now
    if (toInsert) {
      structRayIndex.push_back(NBVar);
      structRayElem.push_back(rayMult);
    }

    // Now we add the ray
    // The number of non-basic variables is really the number of cols,
    // since we have m + n variables total, counting slacks, and only m basic
    addPackedRay(ray, solver.getNumCols(), structRayIndex, structRayElem);

//    {
//      if (NBVar < solver.getNumCols())
//        printf(
//            "Ray %d on nb var (col) %d with name %s was added? %d.\n",
//            j, NBVar, solver.getColName(NBVar).c_str(), added);
//      else
//        printf(
//            "Ray %d on nb var (row) %d with name %s was added? %d.\n",
//            j, NBVar,
//            solver.getRowName(NBVar - solver.getNumCols()).c_str(),
//            added);
//    }

    // If the ray corresponds to a free variable, we also need to add its negation
    if (isNonBasicFreeVar(solver, NBVar)) {
      for (int i = 0; i < (int) structRayElem.size(); i++) {
        structRayElem[i] *= -1.0;
      }
      addPackedRay(ray, solver.getNumCols(), structRayIndex,
          structRayElem);

//      {
//        if (NBVar < solver.getNumCols())
//          printf(
//              "Negation of ray %d on nb free var (col) %d with name %s was added? %d.\n",
//              j, NBVar, solver.getColName(NBVar).c_str(), added);
//        else
//          printf(
//              "Negation of ray %d on nb free var (row) %d with name %s was added? %d.\n",
//              j, NBVar,
//              solver.getRowName(NBVar - solver.getNumCols()).c_str(),
//              added);
//      }
    }

    // Clear the data structures for use for the next ray
    structRayIndex.clear();
    structRayElem.clear();
  }

//  std::vector<std::vector<double> > raysOfC1; // rays of the corner polyhedron
//  // Save rays of C1, where the coefficients of inv(B) * A
//  // are sometimes negated because we have
//  // \bar x = inv(B) * b - inv(B) * A * x_N
////  int tempIndex = 0;
//
//  raysOfC1.resize(numRays);
//  for (int j = 0; j < numRays; j++) {
//    raysOfC1[j].resize(solver.getNumRows());
//    solver.getBInvACol(nonBasicVarIndex[j], &raysOfC1[j][0]);
//    if (nonBasicVarIndex[j] < solver.getNumCols()) {
//      // We are looking at inv(B)*A, jth col
//      // Call it \bar a^j
//      // Moving it to the right side we get \bar a^0 - \bar a^j
//      // So call ray j r^j := -\bar a^j
//      // (EXCEPT for the ub vars, because for those, we actually
//      // proceed along the negative direction.)
//      if (isNonBasicLBCol(solver, nonBasicVarIndex[j]))
//        for (int k = 0; k < solver.getNumRows(); k++)
//          raysOfC1[j][k] *= -1.0;
//    } else {
//      // For <= slacks, we have to multiply by -1 too.
//      // For >= slacks, clp stores the basis inverse in the entry
//      // we read, which is already the negation of what we'd see in
//      // the tableau, so we do not need to do anything.
//      // This is b/c for ax >= b rows, clp works with ax + s = b, s <= 0,
//      // whereas we tend to imagine ax - s = b, s>= 0.
//      // Thus, since we would need +a_s s, we already have that.
//      if (rowSense[nonBasicVarIndex[j] - solver.getNumCols()] == 'L')
//        for (int k = 0; k < solver.getNumRows(); k++)
//          raysOfC1[j][k] *= -1.0;
//    }
//  }
//
//  // For the rows that are >=, if the variable basic in the row is slack,
//  // then we have it be a_i - s = b_i, so we move everything to the other side.
//  for (int r = 0; r < solver.getNumRows(); r++) {
//    if (varBasicInRow[r] >= solver.getNumCols()) {
//      if (rowSense[varBasicInRow[r] - solver.getNumCols()] == 'G') {
//        for (int j = 0; j < numRays; j++)
//          raysOfC1[j][r] *= -1.0;
//      }
//    }
//  }
//
//  // Now we add the rays to the ray store
//  ray.reserve((int) raysOfC1.size());
//  for (int r = 0; r < (int) raysOfC1.size(); r++) {
//    bool added = addRay(ray, raysOfC1[r]);
//
//    // TODO REMOVE
//    int col = nonBasicVarIndex[r];
//    if (col < solver.getNumCols())
//      printf("Ray %d on nb var (col) %d with name %s was added? %d.\n", r,
//          col, solver.getColName(col).c_str(), added);
//    else
//      printf("Ray %d on nb var (row) %d with name %s was added? %d.\n", r,
//          col, solver.getRowName(col - solver.getNumCols()).c_str(),
//          added);
//  }
}

/**
 * IN NON-BASIC SPACE
 * Get Point and Rays from corner polyhedron defined by current optimum at solver
 * Assumed to be optimal already
 */
void genCornerNB(std::vector<Point>& point, std::vector<Point>& pointFullSpace,
    std::vector<Ray>& ray, std::vector<Ray>& rayFullSpace,
    const PointCutsSolverInterface* const  tmpSolver,
    const PointCutsSolverInterface* const  origSolver,
    const ProblemData & origProbData) {
  tmpSolver.enableFactorization();
  // Ensure solver is optimal
  if (!tmpSolver.isProvenOptimal()) {
    error_msg(errstr, "Solver is not proven optimal.\n");
    writeErrorToII(errstr, PointCutsParam::inst_info_out);
    exit(1);
  }

  /***********************************************************************************
   * Identify the cobasis
   ***********************************************************************************/
  // All we need is the number of rays we will have
  // This is the number of non-basic variables at their upper or lower bounds, including slack variables
  // Also free variables...
  std::vector<int> cstat(tmpSolver.getNumCols()), rstat(
      tmpSolver.getNumRows());
  tmpSolver.getBasisStatus(&cstat[0], &rstat[0]);
  std::vector<int> varBasicInRow(tmpSolver.getNumRows());
  tmpSolver.getBasics(&varBasicInRow[0]);
//  const char* rowSense = solver.getRowSense();

  // We also will want to know what row each *orig* non-basic var is basic in, *if any*
  // This will be useful to extract the coefficients of the rays quickly
//  std::vector<int> basicStructVar;
//  std::vector<int> rowOfStructVar(tmpSolver.getNumCols(), -1);
  // The vector will hold the row that nbvar i is basic in, if any.
  // If none, it will hold -1.
  std::vector<int> rowOfOrigNBVar(origProbData.numNB, -1);

  // Gather which are the non-basic variables for this solver
//  int numRays = 0, numNBFree;
  std::vector<int> nonBasicVarIndex, nonBasicFreeIndexInNBVars;
  nonBasicVarIndex.reserve(tmpSolver.getNumCols()); // There may be fewer if we have equality constraints
  for (int var = 0; var < tmpSolver.getNumCols() + tmpSolver.getNumRows();
      var++) {
    // If this is a row and the basic var *was* non-basic, store its row
    if (var >= tmpSolver.getNumCols()) {
      int row = var - tmpSolver.getNumCols();
      int basicVar = varBasicInRow[row];
      // If this basicVar was non-basic in the basis at v,
      // we need to add this row in the right spot in rowOfOrigNBVar
      int NBIndexOfBasicVar = origProbData.getVarNBIndex(basicVar);
      if (NBIndexOfBasicVar >= 0) {
        rowOfOrigNBVar[NBIndexOfBasicVar] = row;
      }
    }

    // If it is a ray we want to capture, add it to the ray index list
    if (isRayVar(tmpSolver, var)) {
      if (isNonBasicFreeVar(tmpSolver, var)) {
        nonBasicFreeIndexInNBVars.push_back(
            (int) nonBasicVarIndex.size());
      }
      nonBasicVarIndex.push_back(var);
    }
  }

  /***********************************************************************************
   * The first point is simply the first vertex
   ***********************************************************************************/
  // We need to calculate its coefficients in the NB space given in origProbData
  // That is, the NB space defined by v, the solution to the initial LP relaxation
  // *However* we need to work in the complemented NB space, where v is the origin
  const double* structSolution = tmpSolver.getColSolution();

  // The point will be stored wrt origProbData cobasis,
  // but the cobasis of the point is given by nonBasicVarIndex
  Point optPoint(nonBasicVarIndex.size(), nonBasicVarIndex);
  optPoint.setCompNBCoor(structSolution, tmpSolver, origSolver, origProbData);
  bool added = addPoint(point, optPoint);
  if (added) {
    Point optPointFullSpace(nonBasicVarIndex.size(), nonBasicVarIndex);
    optPointFullSpace.setFullCoorWithCompNBSpace(tmpSolver, origSolver,
        origProbData);
    addPoint(pointFullSpace, optPointFullSpace, false);
  }

  /***********************************************************************************
   * We get the rays corresponding to non-basic variables
   ***********************************************************************************/
  // We now store the packed vector for the corner polyhedron rays.
  // For each non-basic var in nonBasicVarIndex, we get Binv * A.
  // Since we are looking at x_B = \bar a_0 - \bar A_N x_N,
  // the rays should be negative of Binv * A.
  // Exception is when the variable is non-basic at its upper bound.
  // In that case, ray is negated, which is equivalent to taking Binv * A as-is.
  // For free variables, we need both the ray from Binv * A and its negation.
  // For fixed variables, there is no need to get a ray, since we would not be able
  // to increase along that direction anyway.
  // Finally, considering the slack variables, if the row is
  //  ax <= b
  // then Clp stores a slack of the form
  //  ax + s = b, s >= 0
  // in which case we need to again take -Binv * A.
  // If the row is
  //  ax >= b
  // then Clp stores a slack of the form
  //  ax + s = b, s <= 0
  // in which case we already have the negative by using Binv * A.
  // This is because the Clp version is an upper-bounded variable,
  // so we have to negate.
  //
  // For the rows that are >=, if the variable basic in the row is slack,
  // then we have it be a_i - s = b_i, so we move everything to the other side.
  // I.e., we negate the *row* of Binv * A.
  std::vector<double> currRay(tmpSolver.getNumRows());

  // We iterate through each of the rays
  for (int j = 0; j < (int) nonBasicVarIndex.size(); j++) {
    int NBVar = nonBasicVarIndex[j]; // This is the current ray

    // Get the ray corresponding to this NBVar
    tmpSolver.getBInvACol(NBVar, &currRay[0]);

    // We want to store this ray in the complemented original NB space
    Ray currRayNB(origProbData.numNB);
    Ray currRayFullSpace(origProbData.numNB);
    currRayNB.setCompNBCoor(currRay.data(), tmpSolver, rowOfOrigNBVar,
        origSolver, origProbData, NBVar);
    added = addRay(ray, currRayNB);
    if (added) {
      currRayFullSpace.setFullCoorWithCompNBSpace(currRay.data(),
          tmpSolver, origSolver, origProbData, NBVar);
      addRay(rayFullSpace, currRayFullSpace, false);

      // We also add this ray to the ray indices for the initial vertex
      point[0].rayIndices.push_back((int) ray.size() - 1);
      pointFullSpace[0].rayIndices.push_back(
          (int) rayFullSpace.size() - 1);
    }

//    {
//      if (NBVar < solver.getNumCols())
//        printf(
//            "Ray %d on nb var (col) %d with name %s was added? %d.\n",
//            j, NBVar, solver.getColName(NBVar).c_str(), added);
//      else
//        printf(
//            "Ray %d on nb var (row) %d with name %s was added? %d.\n",
//            j, NBVar,
//            solver.getRowName(NBVar - solver.getNumCols()).c_str(),
//            added);
//    }

    // If the ray corresponds to a free variable, we also need to add its negation
    if (isNonBasicFreeVar(tmpSolver, NBVar)) {
      currRayNB *= -1.0;
      added = addRay(ray, currRayNB);
      if (added) {
        currRayFullSpace *= -1.0;
        addRay(rayFullSpace, currRayFullSpace, false);

        // We also add this ray to the ray indices for the initial vertex
        point[0].rayIndices.push_back((int) ray.size() - 1);
        pointFullSpace[0].rayIndices.push_back(
            (int) rayFullSpace.size() - 1);
      }

//      {
//        if (NBVar < solver.getNumCols())
//          printf(
//              "Negation of ray %d on nb free var (col) %d with name %s was added? %d.\n",
//              j, NBVar, solver.getColName(NBVar).c_str(), added);
//        else
//          printf(
//              "Negation of ray %d on nb free var (row) %d with name %s was added? %d.\n",
//              j, NBVar,
//              solver.getRowName(NBVar - solver.getNumCols()).c_str(),
//              added);
//      }
    }
  }
}

void genDegenCorner(std::vector<Point>& point, std::vector<Ray>& ray,
    const PointCutsSolverInterface* const solver, const ProblemData& probData) {
  /***********************************************************************************
   * Set up solver with relaxation taking all edges extending from initial vertex
   ***********************************************************************************/
  // We remove all rows in which the slack variable is non-zero.
  PointCutsSolverInterface cornerSolver(solver);
  cornerSolver.enableFactorization();
  // This is to prevent a different basis from being chosen for the subproblem, which sometimes happens.
  // TODO We should check this is really doing what we want
  int* cstat = new int[solver.getNumCols()];
  int* rstat = new int[solver.getNumRows()];
  solver.getBasisStatus(cstat, rstat);
  cornerSolver.setBasisStatus(cstat, rstat);
  delete[] cstat;
  delete[] rstat;
  cornerSolver.disableFactorization();
  if (!cornerSolver.basisIsAvailable())
    cornerSolver.resolve();
  cornerSolver.enableFactorization();

  std::vector<int> rowsToDelete;
  rowsToDelete.reserve(solver.getNumRows());
  const double* rhs = solver.getRightHandSide();
  const double* rowActivity = solver.getRowActivity();
  const double *colLB = solver.getColLower();
  const double *colUB = solver.getColUpper();
  const double *xVal = solver.getColSolution();

  // We delete all constraints that are not active
#ifdef TRACE
  printf("\n## Deleting rows with non-zero slack. ##\n");
  printf("Rows being deleted:\n");
#endif
  for (int r = 0; r < solver.getNumRows(); r++) {
    if (std::abs(rhs[r] - rowActivity[r]) > PTCONST::param.getEPS()) {
      rowsToDelete.push_back(r);
#ifdef TRACE
//      printf(" %d", rowsToDelete[rowsToDelete.size()-1]);
      printf(" %d", rowsToDelete.back());
#endif
    }
  }
  cornerSolver.deleteRows((int) rowsToDelete.size(), rowsToDelete.data());
#ifdef TRACE
  printf("\nNumber rows deleted: %d.\n", (int) rowsToDelete.size());
#endif

  // We must also remove non-tight variable bounds,
  // as they are also non-active constraints
#ifdef TRACE
  printf("\n## Removing non-tight variable bounds. ##\n");
#endif
  for (int c = 0; c < solver.getNumCols(); c++) {
    // If we are not at the lower-bound, delete the lower-bound
    if ((colLB[c] > -1 * solver.getInfinity() + PTCONST::param.getEPS())
        && (std::abs(xVal[c] - colLB[c]) > PTCONST::param.getEPS())) {
      cornerSolver.setColLower(c, -1 * solver.getInfinity());
    }
    // If we are not at the upper-bound, delete the upper-bound
    if ((colUB[c] < solver.getInfinity() - PTCONST::param.getEPS())
        && (std::abs(xVal[c] - colUB[c]) > PTCONST::param.getEPS())) {
      cornerSolver.setColUpper(c, solver.getInfinity());
    }
  }

  // Save the solver
  char filename[300];
  snprintf(filename, sizeof(filename) / sizeof(char), "%s-Corner",
      PointCutsParam::out_f_name_stub.c_str());
#ifdef TRACE
  printf("\n## Writing corner solver to file %s. ##\n", filename);
#endif
  cornerSolver.writeMps(filename, "mps", cornerSolver.getObjSense());

  /***********************************************************************************
   * Get vertices and rays via lrs
   ***********************************************************************************/
  PointCutsParam::timeStats.start_timer(PTCONST::LRS_TIME);
#ifdef TRACE
  printf("\n## Obtaining vertices and rays for cornerSolver via lrs. ##\n");
#endif

//  std::vector<Point> point;
//  std::vector<Ray> ray;
  point.reserve(cornerSolver.getNumCols() * cornerSolver.getNumCols());
  ray.reserve(cornerSolver.getNumCols() * cornerSolver.getNumCols());
  if (!getVerticesFromLrs(point, ray, cornerSolver, probData)) {
    error_msg(errstr,
        "Problem generating vertices and rays from lrs. Exiting.\n");
    writeErrorToII(errstr, PointCutsParam::inst_info_out);
    exit(1);
  }
  PointCutsParam::timeStats.end_timer(PTCONST::LRS_TIME);
}

/**
 * Finds the vertex obtained by proceeding along ray ray[rayOutIndex]
 * emanating from the point point[startPointIndex]
 * until the first hyperplane is intersected,
 * where the index is the index of the ray is within the ray vector,
 * and the index of the point is within the point vector.
 * varOutCobasicIndex is used to get rayOutIndex
 * --- it is the index of the variable being pivoted out
 * of the cobasis at point[startPointIndex].
 *
 * The resulting point is generated,
 * as well as the n-1 (n-2 actually in the face) new rays at this vertex.
 *
 * If no hyperplane intersected, labels this ray as extreme.
 */
void pivotNB(const int startPointIndex, const int varOutCobasicIndex,
    std::vector<Point>& point, std::vector<Point>& pointFullSpace,
    std::vector<Ray>& ray, std::vector<Ray>& rayFullSpace,
    const PointCutsSolverInterface* const tmpSolver,
    const PointCutsSolverInterface* const origSolver,
    const ProblemData& origProbData) {
  // The full point and ray space is the number of structural variables
  // + number of rows, and it is in the complemented NB full space;
  // i.e., we have replaced all x_j, s.t. j \in N(v)
  // (the non-basic variables at the LP opt v)
  // with x_j = l_j + y_j for all j \in N_L(v)
  // and x_j = g_j - y_j for all j \in N_U(v)

  int varIn;
  double rayDist;

  const int rayOutIndex =
      pointFullSpace[startPointIndex].rayIndices[varOutCobasicIndex];

  // Now find a hyperplane
  if (!findHplaneToIntersectRay(varIn, rayDist,
      pointFullSpace[startPointIndex], rayFullSpace[rayOutIndex],
      origSolver, origProbData)) {
    // This ray is actually an extreme ray
    ray[rayOutIndex].setExtreme(true);
    rayFullSpace[rayOutIndex].setExtreme(true);
  } else if (rayDist < -PTCONST::param.getEPS()) {
    error_msg(errstr, "rayDist is negative: %e.\n", rayDist);
    writeErrorToII(errstr, PointCutsParam::inst_info_out);
    exit(1);
  } else if (rayDist < PTCONST::param.getEPS()) {
    // We have intersected a degenerate hyperplane
    // In this case we do not need to add a new point
    // We may need to keep a set of variables non-basic at each point,
    // so that we avoid getting stuck at a degenerate point
  } else {
    // Otherwise we have to find the new point coordinates
    std::vector<int> newCobasis(pointFullSpace[startPointIndex].getCobasis);

    Point newPointFullSpace(
        (int) pointFullSpace[startPointIndex].getCobasisSize());
    newPointFullSpace.setFullCoorFromPointRay(
        pointFullSpace[startPointIndex], rayFullSpace[rayOutIndex],
        rayDist);
    bool added = addPoint(pointFullSpace, newPointFullSpace);
    if (added) {
      // We also add it to the NB space collection of points
      Point newPointCompNB(origProbData.numNB);
      newPointCompNB.setCompNBCoor(newPointFullSpace, origProbData);
      addPoint(point, newPointCompNB, false);
    }

    // We also need to compute the new rays
    // We simply take all the rays from the previous cobasis and adjust them
  }
}
