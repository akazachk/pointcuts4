//============================================================================
// Name        : genSICs.hpp
// Author      : akazachk
// Version     : 0.2013.mm.dd
// Copyright   : Your copyright notice
// Description : 
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#pragma once

#include <cstdio>
#include <cmath>
#include <stdlib.h>
#include <cfloat>
#include <vector>

#include <numeric> // For inner_product
#include <algorithm> // For max_element, min_element
#include "CoinPackedMatrix.hpp"
#include "OsiSolverInterface.hpp"
#include "typedefs.hpp"

#include "GlobalConstants.hpp"
#include "Utility.hpp"
#include "SolutionInfo.hpp"
#include "Output.hpp"
#include "Structs.hpp"

using namespace std;

/***********************************************************************/
/**
 * @brief Generate a single SIC.
 */
void genSIC(cut &NBSIC, vector<double> &distAlongRay, double &absSumCoeffs,
    PointCutsSolverInterface* solver, vector<int> &nonBasicOrigVarIndex,
    vector<int> &nonBasicSlackVarIndex, int splitVarIndex,
    int splitVarRowIndex);

/***********************************************************************/
/**
 * @brief Generate SICs, except only generate at most numCutGenSets splits.
 */
void genSICs(vector<cut> &NBSIC, vector<vector<double> > &distAlongRay,
    vector<cut> &SIC, PointCutsSolverInterface* solver, SolutionInfo &solnInfo,
    int numCutGenSets, string &f_stub, const bool subspace_option,
    FILE* inst_info_out);

/***********************************************************************/
/**
 * @brief Generate SICs in original space (well, works for any space),
 * but restrict the set of splits on which we generate to feasSplits.
 * Note that if the split is feasible in the subspace, then it should
 * also be feasible in the full space, since the trace of the orig SIC in the
 * subspace is the sub SIC.
 */
void genSICsOrig(vector<cut> &NBSIC, vector<vector<double> > &distAlongRay,
    vector<cut> &SIC, PointCutsSolverInterface* solver, SolutionInfo &solnInfo,
    SolutionInfo &solnInfoSub, vector<int> &oldColIndices, string &f_stub,
    FILE* inst_info_out);
