//============================================================================
// Name        : CutMaster.hpp
// Author      : akazachk
// Version     : 0.2014.07.03
// Copyright   : Your copyright notice
// Description : Functions for cuts (SICs and GICs)
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#include "typedefs.hpp"
#include "SolutionInfo.hpp"
#include "AdvCut.hpp"
#include "chooseCutGeneratingSets.hpp"
//#include "Subprob.hpp"

void cutMaster(const PointCutsSolverInterface* const solverOrig,
    const SolutionInfo& solnInfoOrig);

double SICMaster(const PointCutsSolverInterface* const solverMain,
    SolutionInfo& solnInfoMain, AdvCuts& structMSICs, AdvCuts& NBMSICs,
    PointCutsSolverInterface* & MSICSolver);

double GICMaster(int& num_obj_tried, const PointCutsSolverInterface* const solverMain,
    const SolutionInfo& solnInfoMain, const std::vector<int>& oldRowIndices,
    const AdvCuts& structMSICs, const AdvCuts& NBMSICs,
    const PointCutsSolverInterface* const MSICSolver, AdvCuts& structMGICs,
    const AdvCut& obj);
