//============================================================================
// Name        : Compute.cpp
// Author      : akazachk
// Version     : 0.2013.mm.dd
// Copyright   : Your copyright notice
// Description : 
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#include "Compute.hpp"

/********************************************************************************/
/**
 * @brief Computes number of different coefficients and total by which they are different
 */
void computeRayStats(int &numDiffCoeff, double &totaldiff, double &abstotaldiff,
    vector<double> &distAlongRayLifted, vector<double> &distAlongRayOrig) {
  // For computing which rays are the same
  numDiffCoeff = 0;
  totaldiff = 0.0;
  int numcoeffs = distAlongRayLifted.size();

  for (int coeff = 0; coeff < numcoeffs; coeff++) {
    double diff = distAlongRayLifted[coeff] - distAlongRayOrig[coeff];
    if (std::abs(diff) > param.getDIFFEPS()) {
      numDiffCoeff++;
      totaldiff += diff;
      abstotaldiff += std::abs(diff);
    }
  }
}

/********************************************************************************/
/**
 * @brief Count number of sides of the possible splits that are empty
 */
void countEmptySplits(PointCutsSolverInterface* tmpsolver, SolutionInfo &solnInfo,
    std::vector<int> &splitVar, std::string out_f_name_stub) {
  PointCutsSolverInterface solver(tmpsolver);
  int numrows = solver.getNumRows();
  int numcols = solver.getNumCols();
  int numsplits = (int) splitVar.size();

  // Now we go through each split, and force each split to be
  // at its lower bound, and at its upper bound. We count how many
  // of those are feasible.
  solnInfo.num0feas = 0;
  solnInfo.num1feas = 0;
  solnInfo.num2feas = 0;
  solnInfo.numFloorInfeas = 0;
  solnInfo.numCeilInfeas = 0;
  const double* tmp = tmpsolver.getColSolution();
  std::vector<double> colSolution(tmp, tmp + numcols);

  for (int split = 0; split < numsplits; split++) {
    int numvalid = 0;
    int currvar = splitVar[split];
    double currval = colSolution[currvar];
    std::vector<int> cols;
    std::vector<double> vals;
    double rowlb, rowub;

    cols.push_back(currvar);
    vals.push_back(1);
    rowlb = floor(currval);
    rowub = floor(currval);

#ifdef TRACE
    printf("\nFLOOR: Currvar: %d. Currval: %f. Rowlb: %f. Rowub: %f.\n",
        currvar, currval, rowlb, rowub);
#endif

    solver.addRow(1, cols.data(), vals.data(), rowlb, rowub);

    char splitLP[256];
    snprintf(splitLP, sizeof(splitLP) / sizeof(char), "%sTest%dFloor",
        out_f_name_stub.c_str(), currvar);

    //solver.writeLp(splitLP);

    solver.initialSolve();

    if (solver.isProvenOptimal()) {
#ifdef TRACE
      printf("\nFloor of split %d is proven optimal.\n", currvar);
#endif
      numvalid++;
      solnInfo.fracCoreFloorFeasFlag[split] = true;
    } else {
#ifdef TRACE
      printf("\nFloor of split %d is NOT proven optimal.\n", currvar);
#endif
      solnInfo.numFloorInfeas++;
    }

    std::vector<int> rowsToDelete(1);
    rowsToDelete[0] = numrows;
    solver.deleteRows(1, rowsToDelete.data());

    rowlb = ceil(currval);
    rowub = ceil(currval);
#ifdef TRACE
    printf("\nCEIL: Currvar: %d. Currval: %f. Rowlb: %f. Rowub: %f.\n",
        currvar, currval, rowlb, rowub);
#endif

    solver.addRow(1, cols.data(), vals.data(), rowlb, rowub);

    snprintf(splitLP, sizeof(splitLP) / sizeof(char), "%sTest%dCeil",
        out_f_name_stub.c_str(), currvar);

    //solver.writeLp(splitLP);

    solver.initialSolve();

    if (solver.isProvenOptimal()) {
#ifdef TRACE
      printf("\nCeil of split %d is proven optimal.\n", currvar);
#endif
      numvalid++;
      solnInfo.fracCoreCeilFeasFlag[split] = true;
    } else {
#ifdef TRACE
      printf("\nCeil of split %d is NOT proven optimal.\n", currvar);
#endif
      solnInfo.numCeilInfeas++;
    }

    solver.deleteRows(1, rowsToDelete.data());

    if (numvalid == 0)
      solnInfo.num0feas++;
    else if (numvalid == 1)
      solnInfo.num1feas++;
    else
      solnInfo.num2feas++;
  }
#ifdef TRACE
  printf("\n");
#endif
}

/********************************************************************************/
/**
 * @brief Outputs the number of rays to add to a split to have the number
 *  of intersecting rays be at least the minimum number desired.
 */
int numRaysToAdd(int numIntersecting, int numRays) {
  int minNumIntersecting = ceil(MIN_RAYS_RATIO * numRays);
  if (numIntersecting < minNumIntersecting) {
    return minNumIntersecting - numIntersecting;
  }
  return 0;
}

/********************************************************************************/
/**
 * @brief
 *
 * @param indicesToAdd                ::  Variable index to add (original space)
 * @param indicesToAddFromDeletedCols ::  Index within deletedCols
 */
void fixFloor(std::vector<int> &indicesToAdd,
    std::vector<int> &indicesToAddInDeletedCols,
    PointCutsSolverInterface* solver, SolutionInfo &solnInfoOrig,
    int &splitVarIndex, int numToAdd, std::vector<int> &deletedCols,
    std::vector<int> &oldRowIndices, std::vector<int> &oldColIndices,
    PointCutsSolverInterface* subprob, SolutionInfo &solnInfoSub) {
  indicesToAdd.resize(numToAdd, -1);
  indicesToAddInDeletedCols.resize(numToAdd, -1);

  vector<int> densityOfAddedCols(numToAdd, -1);

  std::vector<double> splitRowStructRays(solnInfoOrig.numCols),
      splitRowSlackRays(solnInfoOrig.numRows);
  solver.getBInvARow(oldRowIndices[oldColIndices[splitVarIndex]],
      &splitRowStructRays[0], &splitRowSlackRays[0]);

  // This will be used to stop the process early if we have found the top
  // numToAdd columns to add
  int baseDensityIndex = solnInfoOrig.rayDensitySortIndex[numToAdd + 1];
  int baseDensity = solnInfoOrig.rayDensity[baseDensityIndex];

  // We will continue to add until numAdded = numToAdd
  int numAdded = 0;

  // These will be used to replace the lowest density ray
  // TODO Replace this method by binary search in case numAdded is non-trivial
//  int minRayDensity = 0;
  int minColIndex = -1;
  std::vector<int>::iterator minit;

  // Now let's actually find some rays to add!
  for (int col = 0; col < (int) deletedCols.size(); col++) {
    int currRay = deletedCols[col];

    // Only consider actual rays... not basic or free variables
    if (solnInfoOrig.cstat[currRay] < 2)
      continue;

    int rayDensity = solnInfoOrig.rayDensity[currRay];
    double currRayDirn = splitRowStructRays[currRay];

    // We are looking at inv(B)*A, jth col
    // Call it \bar a^j
    // Moving it to the right side we get \bar a^0 - \bar a^j
    // So call ray j r^j := -\bar a^j
    // (EXCEPT for the ub vars, because for those, we actually
    // proceed along the negative direction.)
    if (solnInfoOrig.cstat[currRay] == 3)
      currRayDirn *= -1.0;

    // If the ray does not hit the floor, we can ignore it
    if (currRayDirn > -param.getEPS())
      continue;

    // If we are here, we know that the variable is
    // (1) non-basic,
    // (2) intersects the floor of the split

    // If we have not added all numToAdd rays, this one goes in the vector
    if (numAdded < numToAdd) {
      indicesToAdd[numAdded] = currRay;
      indicesToAddInDeletedCols[numAdded] = col;
      densityOfAddedCols[numAdded] = rayDensity;

      if (numAdded == 0) {
        minit = densityOfAddedCols.begin();
//        minRayDensity = rayDensity;
        minColIndex = numAdded;
      } else if (*minit > rayDensity) {
        minit = densityOfAddedCols.begin() + numAdded;
//        minRayDensity = rayDensity;
        minColIndex = numAdded;
      }

      numAdded++;
    } else {
      // Otherwise, we have all numToAdd rays
      // so we only add this one if we replace the min distance
      if (*minit < rayDensity) {
        indicesToAdd[minColIndex] = currRay;
        indicesToAddInDeletedCols[minColIndex] = col;
        densityOfAddedCols[minColIndex] = rayDensity;

        // The one we added may not be the new minimum;
        // we have to find the minimum again
        minit = computeMin(densityOfAddedCols); // TODO change this to some binary search
        minColIndex = minit - densityOfAddedCols.begin();
      }
    }

    // If we have added all we need,
    // and we have no hope of finding a set with a larger min density,
    // then let's just stop now.
    if (numAdded == numToAdd && *minit >= baseDensity)
      break;
  }

  // Now that we have the rays we will add back in, we need to
  // (1) Add the relevant columns to subprob
  // (2) Remove them from deletedCols
  // (3) Update feasibility info
  for (int i = 0; i < (int) indicesToAdd.size(); i++) {
    int col = indicesToAdd[i];

    if (col == -1)
      continue;

    vector<int> indices(solnInfoSub.numRows);
    vector<double> vals(solnInfoSub.numRows);
    for (int r = 0; r < (int) oldRowIndices.size(); r++) {
      int currRow = oldRowIndices[r];
      double elem = solver.getMatrixByRow()->getCoefficient(currRow, col);
      if (std::abs(elem) > param.getEPS()) {
        indices.push_back(r);
        vals.push_back(elem);
      }
    }
    subprob.addCol((int) indices.size(), indices.data(), vals.data(),
        solver.getColLower()[col], solver.getColUpper()[col],
        solver.getObjCoefficients()[col], solver.getColName(col));
    indices.clear();
    vals.clear();

    // TODO We have to remove all rows that were deleted...

//    char colName[300];
//    snprintf(colName, sizeof(colName)/sizeof(char), "%s", solver.getColName(col).c_str());

//    CoinShallowPackedVector vec = solver.getMatrixByCol()->getVector(col);

//    subprob.addCol(vec, solver.getColLower()[col], solver.getColUpper()[col],
//        solver.getObjCoefficients()[col], solver.getColName(col));

    // Also don't forget to adjust complementing
    // If this variable was complemented, then we have to change rhs
    // TODO DON'T FORGET ABOUT LB TRANSLATION! WE DON'T DO IT YET
    double colVal = solnInfoOrig.primalSoln[col];
    if (std::abs(colVal) > param.getEPS()) {
      for (int row = 0; row < solnInfoOrig.numRows; row++) {
        double mxEntry = solver.getMatrixByRow()->getCoefficient(row,
            col);
        if (std::abs(mxEntry) > param.getEPS()) {
          double entXval = mxEntry * colVal;
          if (std::abs(entXval) > param.getEPS()) {
            if (subprob.getRowSense()[row] == 'L'
                || subprob.getRowSense()[row] == 'R'
                || subprob.getRowSense()[row] == 'E')
              subprob.setRowUpper(row,
                  subprob.getRowUpper()[row] + entXval);
            else if (subprob.getRowSense()[row] == 'G'
                || subprob.getRowSense()[row] == 'R'
                || subprob.getRowSense()[row] == 'E')
              subprob.setRowLower(row,
                  subprob.getRowLower()[row] + entXval);
          }
        }
      }
    }

    // Now (2), remove the column from deleted cols
    deletedCols.erase(deletedCols.begin() + indicesToAddInDeletedCols[i]);

//    // Finally, (3), update feasibility info
//    std::vector<double> rayDirn(solnInfoOrig.numrows);
//    solver.getBInvACol(col, &rayDirn[0]);
//
//    for (int split = 0; split < (int) chooseSplits.splitVarRowIndex.size();
//        split++) {
//      int currRow = chooseSplits.splitVarOldRowIndex[split];
//
//      // Don't forget to change direction if it is a lower-bound row (since we
//      // move it to the other side and don't switch to going in the negative dir
//      // as we would with an ub var.
//      if (solnInfoOrig.cstat[col] == 3)
//        rayDirn[currRow] *= -1.0;
//
//      if (rayDirn[currRow] < param.getEPS()) {
//        solnInfoSub.fracCoreFloorFeas[split] = true;
//      }
//      if (rayDirn[currRow] > param.getEPS()) {
//        solnInfoSub.fracCoreCeilFeas[split] = true;
//      }
//    }
  }
}

void fixCeil(std::vector<int> &indicesToAdd,
    std::vector<int> &indicesToAddInDeletedCols,
    PointCutsSolverInterface* solver, SolutionInfo &solnInfoOrig,
    int &splitVarIndex, int numToAdd, std::vector<int> &deletedCols,
    std::vector<int> &oldRowIndices, std::vector<int> &oldColIndices,
    PointCutsSolverInterface* subprob, SolutionInfo &solnInfoSub) {
  indicesToAdd.resize(numToAdd, -1);
  indicesToAddInDeletedCols.resize(numToAdd, -1);

  vector<int> densityOfAddedCols(numToAdd, -1);

  std::vector<double> splitRowStructRays(solnInfoOrig.numCols),
      splitRowSlackRays(solnInfoOrig.numRows);
  solver.getBInvARow(oldRowIndices[oldColIndices[splitVarIndex]], //chooseSplits.splitVarOldRowIndex[splitVarIndex],
      &splitRowStructRays[0], &splitRowSlackRays[0]);

  // This will be used to stop the process early if we have found the top
  // numToAdd columns to add
  int baseDensityIndex = solnInfoOrig.rayDensitySortIndex[numToAdd + 1];
  int baseDensity = solnInfoOrig.rayDensity[baseDensityIndex];

  // We will continue to add until numAdded = numToAdd
  int numAdded = 0;

  // These will be used to replace the lowest density ray
  // TODO Replace this method by binary search in case numAdded is non-trivial
//  int minRayDensity = 0;
  int minColIndex = -1;
  std::vector<int>::iterator minit;

  // Now let's actually find some rays to add!
  for (int col = 0; col < (int) deletedCols.size(); col++) {
    int currRay = deletedCols[col];

    // Only consider actual rays... not basic or free variables
    if (solnInfoOrig.cstat[currRay] < 2)
      continue;

    int rayDensity = solnInfoOrig.rayDensity[currRay];
    double currRayDirn = splitRowStructRays[currRay];

    // We are looking at inv(B)*A, jth col
    // Call it \bar a^j
    // Moving it to the right side we get \bar a^0 - \bar a^j
    // So call ray j r^j := -\bar a^j
    // (EXCEPT for the ub vars, because for those, we actually
    // proceed along the negative direction.)
    if (solnInfoOrig.cstat[currRay] == 3)
      currRayDirn *= -1.0;

    // If the ray does not hit the floor, we can ignore it
    if (currRayDirn < param.getEPS())
      continue;

    // If we are here, we know that the variable is
    // (1) non-basic,
    // (2) intersects the floor of the split

    // If we have not added all numToAdd rays, this one goes in the vector
    if (numAdded < numToAdd) {
      indicesToAdd[numAdded] = currRay;
      indicesToAddInDeletedCols[numAdded] = col;
      densityOfAddedCols[numAdded] = rayDensity;

      if (numAdded == 0) {
        minit = densityOfAddedCols.begin();
//        minRayDensity = rayDensity;
        minColIndex = numAdded;
      } else if (*minit > rayDensity) {
        minit = densityOfAddedCols.begin() + numAdded;
//        minRayDensity = rayDensity;
        minColIndex = numAdded;
      }

      numAdded++;
    } else {
      // Otherwise, we have all numToAdd rays
      // so we only add this one if we replace the min distance
      if (*minit < rayDensity) {
        indicesToAdd[minColIndex] = currRay;
        indicesToAddInDeletedCols[minColIndex] = col;
        densityOfAddedCols[minColIndex] = rayDensity;

        // The one we added may not be the new minimum;
        // we have to find the minimum again
        minit = computeMin(densityOfAddedCols); // TODO change this to some binary search
        minColIndex = minit - densityOfAddedCols.begin();
      }
    }

    // If we have added all we need,
    // and we have no hope of finding a set with a larger min density,
    // then let's just stop now.
    if (numAdded == numToAdd && *minit >= baseDensity)
      break;
  }

  // Now that we have the rays we will add back in, we need to
  // (1) Add the relevant columns to subprob
  // (2) Remove them from deletedCols
  // (3) Update feasibility info
  for (int i = 0; i < (int) indicesToAdd.size(); i++) {
    int col = indicesToAdd[i];

    if (col == -1)
      continue;

    vector<int> indices(solnInfoSub.numRows);
    vector<double> vals(solnInfoSub.numRows);
    for (int r = 0; r < (int) oldRowIndices.size(); r++) {
      int currRow = oldRowIndices[r];
      double elem = solver.getMatrixByRow()->getCoefficient(currRow, col);
      if (std::abs(elem) > param.getEPS()) {
        indices.push_back(r);
        vals.push_back(elem);
      }
    }
    subprob.addCol((int) indices.size(), indices.data(), vals.data(),
        solver.getColLower()[col], solver.getColUpper()[col],
        solver.getObjCoefficients()[col], solver.getColName(col));
    indices.clear();
    vals.clear();

//    CoinShallowPackedVector vec = solver.getMatrixByCol()->getVector(col);
    //    char colName[300];
    //    snprintf(colName, sizeof(colName)/sizeof(char), "%s", solver.getColName(col).c_str());
//
//    subprob.addCol(vec, solver.getColLower()[col], solver.getColUpper()[col],
//        solver.getObjCoefficients()[col], solver.getColName(col));

    // Also don't forget to adjust complementing
    // If this variable was complemented, then we have to change rhs
    // TODO DON'T FORGET ABOUT LB TRANSLATION! WE DON'T DO IT YET
    double colVal = solnInfoOrig.primalSoln[col];
    if (std::abs(colVal) > param.getEPS()) {
      for (int row = 0; row < solnInfoOrig.numRows; row++) {
        double mxEntry = solver.getMatrixByRow()->getCoefficient(row,
            col);
        if (std::abs(mxEntry) > param.getEPS()) {
          double entXval = mxEntry * colVal;
          if (std::abs(entXval) > param.getEPS()) {
            if (subprob.getRowSense()[row] == 'L'
                || subprob.getRowSense()[row] == 'R'
                || subprob.getRowSense()[row] == 'E')
              subprob.setRowUpper(row,
                  subprob.getRowUpper()[row] + entXval);
            else if (subprob.getRowSense()[row] == 'G'
                || subprob.getRowSense()[row] == 'R'
                || subprob.getRowSense()[row] == 'E')
              subprob.setRowLower(row,
                  subprob.getRowLower()[row] + entXval);
          }
        }
      }
    }

    // Now (2), remove the column from deleted cols
    deletedCols.erase(deletedCols.begin() + indicesToAddInDeletedCols[i]);

//    // Finally, (3), update feasibility info
//    std::vector<double> rayDirn(solnInfoOrig.numrows);
//    solver.getBInvACol(col, &rayDirn[0]);
//
//    for (int split = 0; split < (int) chooseSplits.splitVarRowIndex.size();
//        split++) {
//      int currRow = chooseSplits.splitVarOldRowIndex[split];
//
//      // Don't forget to change direction if it is a lower-bound row (since we
//      // move it to the other side and don't switch to going in the negative dir
//      // as we would with an ub var.
//      if (solnInfoOrig.cstat[col] == 3)
//        rayDirn[currRow] *= -1.0;
//
//      if (rayDirn[currRow] < param.getEPS()) {
//        solnInfoSub.fracCoreFloorFeas[split] = true;
//      }
//      if (rayDirn[currRow] > param.getEPS()) {
//        solnInfoSub.fracCoreCeilFeas[split] = true;
//      }
//    }
  }
}
