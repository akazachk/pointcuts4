/*
 * generateGICs.hpp
 *
 *  Created on: Jun 21, 2012
 *      Author: selva
 */

#ifndef GENERATEGICS_HPP_
#define GENERATEGICS_HPP_
/**COIN header*/
#include "OsiSolverInterface.hpp"
#include "typedefs.hpp"
#include "GlobalConstants.hpp"
#include "Utility.hpp"
#include "Structs.hpp"
#include "Output.hpp"
#include "chooseCutGeneratingSets.hpp"
#include <list>

#include <vector>

using namespace std;

void findPtEvalOfCut(vector<cut> &cutStore, const double *elmtVals,
    const int *elmtIndices, int nElmts, double &maxFeas,
    FILE* inst_info_out);

void computeInterPtCost(double &cost, const double *ptElmts,
    const int *ptIndices, int nPtElmnts, vector<double> &nonBasicRedCosts,
    double &LPOpt);

void initializeIntPtObjectives(int numSplits, list<ptSortInfo*> &interPtObjFns,
    CoinPackedMatrix** &interPtsAndRays,
    vector<vector<int> > &pointOrRayFlag, std::vector<int>& numPointsOrRays,
    vector<double> &nonBasicRedCosts, double &LPOpt);

void deletePtObjsCutOffByCut(cut &tmpCut, PointCutsSolverInterface* &cutLPSolvers,
    list<ptSortInfo*> &interPtObjFns);

void findFurthestSplit(int numSplits, int splitIndx, const double *LB,
    const double *UB, ptOrRayInfo* intPtOrRayInfo,
    vector<vector<double> > &raysOfC1,
    vector<vector<int> > &hplanesIndxToBeActivated,
    vector<vector<int> > &hplanesRowIndxToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag, vector<double> &a0,
    vector<double> &splitVarValue, vector<int> &splitRowIndx_t,
    vector<int> &splitVarIndex, vector<double> &pi0, int &deepestSplitIndx);

void initializeGICLps(int numSplits, int nNonBasicCols,
    PointCutsSolverInterface* &cutLPSolvers,
    CoinPackedMatrix** &interPtsAndRays,
    vector<vector<int> > &pointOrRayFlag, std::vector<int>& numPointsOrRays,
    double* &constLB, double* &constUB, double* &obj, double* &colLB,
    double* &colUB, FILE* inst_info_out);

void computeVertexCentroid(int numCols, double* &vertFromHAct,
    int* &vertFromHActIndices, int numVert, vector<double> &centroid);

void computeIntersectionPtCentroid(int numCols,
    PointCutsSolverInterface* cutLPSolver, vector<double> &ptCentroid,
    vector<int> &pointOrRayFlag, int numPointsOrRays);

//Generate GICs using convex combination of point centroid and vertices
void generateGICsUsingPts(PointCutsSolverInterface* &cutLPSolvers,
    double** &vertFromHAct, int** &vertFromHActIndices, int* &numVert,
    SolutionInfo &solnInfo, chooseCutGeneratingSets &chooseSplits,
    vector<cut> &cutStore, vector<vector<int> > cutsSpecificToSplits,
    double thIncr, vector<int>& unbnddVertFails,
    vector<int>& nonUnqiueVertFails, vector<int>& vertNotCut,
    CoinPackedMatrix** &interPtsAndRays,
    vector<vector<int> > &pointOrRayFlag, int* &numPointsOrRays,
    FILE *inst_info_out);

void genGICsAvgCutCoeffBestBasis(int splitIndx, int nCutsPerSplit,
    int &nCutsGenerated, cut &tmpCut, PointCutsSolverInterface* solver,
    PointCutsSolverInterface* cutLPSolvers, SolutionInfo &solnInfo,
    chooseCutGeneratingSets &chooseSplits,
    vector<vector<int> > &raysToBeCutByHplane,
    vector<int> &hplanesIndxToBeActivated,
    vector<int> &hplanesToBeActivatedUBFlag,
    vector<int> &hplanesRowIndxToBeActivated, int numSplits,
    int nNonBasicCols, int numCols, vector<cut> &cutStore,
    vector<int> &cutsSpecificToSplits, FILE *inst_info_out);

void genGICsFromStrongPoints(int splitIndx, cut tmpCut, int nNonBasicCols,
    chooseCutGeneratingSets &chooseSplits, int nCutsPerSplit,
    int &nCutsGenerated, PointCutsSolverInterface* cutLPSolvers, int pointIndx,
    vector<vector<int> > &strongPtIndices, double thIncr,
    vector<cut> &cutStore, vector<int> &cutsSpecificToSplits,
    FILE *inst_info_out);

void genGICsAvgCutCoeffStrongBasis(int splitIndx, int nCutsPerSplit,
    int &nCutsGenerated, cut &tmpCut, PointCutsSolverInterface* solver,
    PointCutsSolverInterface* cutLPSolvers, SolutionInfo &solnInfo,
    chooseCutGeneratingSets &chooseSplits,
    vector<vector<int> > &raysToBeCutByHplane, vector<int> &raysToBeCut,
    vector<int> &hplanesIndxToBeActivated,
    vector<int> &hplanesToBeActivatedUBFlag,
    vector<int> &hplanesRowIndxToBeActivated, int numSplits,
    vector<cut> &cutStore, vector<int> &cutsSpecificToSplits,
    vector<int> &strongVertHplaneIndex, FILE* inst_info_out);

void genGICsAvgCutCoeffStrongAdjBasis(int splitIndx, int nTotalCuts,
    int &nCutsGenerated, cut &tmpCut, PointCutsSolverInterface* solver,
    PointCutsSolverInterface* cutLPSolvers, SolutionInfo &solnInfo,
    chooseCutGeneratingSets &chooseSplits,
    vector<vector<int> > &raysToBeCutByHplane, vector<int> &raysToBeCut,
    vector<int> &hplanesIndxToBeActivated,
    vector<int> &hplanesToBeActivatedUBFlag,
    vector<int> &hplanesRowIndxToBeActivated, int numSplits,
    vector<cut> &cutStore, vector<int> &cutsSpecificToSplits,
    vector<int> &strongVertHplaneIndex, FILE *inst_info_out);

void genCutsBasedOnMultCriteria(int nCutsPerSplit,
    PointCutsSolverInterface* solver, PointCutsSolverInterface* &cutLPSolvers,
    SolutionInfo &solnInfo, chooseCutGeneratingSets &chooseSplits,
    vector<vector<vector<int> > > &raysToBeCutByHplane,
    vector<vector<int> > &raysToBeCut,
    vector<vector<int> > &hplanesIndxToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    vector<vector<int> > &hplanesRowIndxToBeActivated, int numSplits,
    int nNonBasicCols, int numCols, vector<cut> &cutStore,
    vector<vector<int> > &cutsSpecificToSplits,
    vector<vector<vector<int> > > &strongPtIndices, double thIncr,
    vector<vector<int> > &strongVertHplaneIndex, FILE *inst_info_out);

int addCutToStore(int splitIndx, int numCols, cut &tmpCut,
    vector<cut> &cutStore, const double* cutCoeff, double viol,
    int cutVertexIndx, double th, chooseCutGeneratingSets &chooseSplits);

void computeCutVertex(int numCols, vector<double> &cutVertex,
    vector<double> &centroid, int vertNZIndex, double vertNZVal, double th);

void generateBestGICAvgCutOptimalBasis(cut &tmpCut,
    PointCutsSolverInterface* &cutLPSolvers, int numSplits, int nNonBasicCols,
    vector<cut> &cutStore, vector<vector<int> > &cutsSpecificToSplits,
    int &numCutsGenerated, chooseCutGeneratingSets &chooseSplits);

void generateGICsUsingInterPointObjs(int &nCutsGenerated, int nTotalCuts,
    cut &tmpCut, PointCutsSolverInterface* &cutLPSolvers, int numSplits,
    int nNonBasicCols, chooseCutGeneratingSets &chooseSplits,
    vector<cut> &cutStore, vector<vector<int> > &cutsSpecificToSplits,
    vector<int> &allRaysCut, vector<double> &interPtObjVal,
    vector<int> &interPtObjIndex, vector<int> &interPtSplit,
    vector<vector<int> > &pointOrRayFlag,
    const std::vector<int>& numPointsOrRays, double thIncr,
    FILE *inst_info_out);

void genCutsBasedOnRankHeur1(int nCutsInBudget, int &nCutsGenerated,
    PointCutsSolverInterface* solver, PointCutsSolverInterface* &cutLPSolvers,
    SolutionInfo &solnInfo, chooseCutGeneratingSets &chooseSplits,
    vector<vector<vector<int> > > &raysToBeCutByHplane,
    vector<int> &allRaysCut, vector<vector<int> > &raysToBeCut,
    vector<vector<int> > &hplanesIndxToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    vector<vector<int> > &hplanesRowIndxToBeActivated,
    vector<cut> &cutStore, vector<vector<int> > &cutsSpecificToSplits,
    vector<vector<vector<int> > > &strongPtIndices, double thIncr,
    vector<vector<int> > &strongVertHplaneIndex,
    vector<double> &interPtObjVal, vector<int> &interPtObjIndex,
    vector<int> &interPtSplit, vector<vector<int> > &pointOrRayFlag,
    const std::vector<int>& numPointsOrRays, FILE *inst_info_out);

void genCutsBasedOnRankHeur2(int nCutsInBudget, int &nCutsGenerated,
    PointCutsSolverInterface* solver, PointCutsSolverInterface* &cutLPSolvers,
    SolutionInfo &solnInfo, chooseCutGeneratingSets &chooseSplits,
    vector<vector<vector<int> > > &raysToBeCutByHplane,
    vector<int> &allRaysCut, vector<vector<int> > &raysToBeCut,
    vector<vector<int> > &hplanesIndxToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    vector<vector<int> > &hplanesRowIndxToBeActivated,
    vector<cut> &cutStore, vector<vector<int> > &cutsSpecificToSplits,
    vector<vector<vector<int> > > &strongPtIndices, double thIncr,
    vector<vector<int> > &strongVertHplaneIndex,
    vector<double> &interPtObjVal, vector<int> &interPtObjIndex,
    vector<int> &interPtSplit, vector<vector<int> > &pointOrRayFlag,
    const std::vector<int>& numPointsOrRays, FILE *inst_info_out);

void genCutsBasedOnRankHeur3(int nCutsInBudget, int &nCutsGenerated,
    PointCutsSolverInterface* solver, PointCutsSolverInterface* &cutLPSolvers,
    SolutionInfo &solnInfo, chooseCutGeneratingSets &chooseSplits,
    vector<vector<vector<int> > > &raysToBeCutByHplane,
    vector<int> &allRaysCut, vector<vector<int> > &raysToBeCut,
    vector<vector<int> > &hplanesIndxToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    vector<vector<int> > &hplanesRowIndxToBeActivated,
    vector<cut> &cutStore, vector<vector<int> > &cutsSpecificToSplits,
    vector<vector<vector<int> > > &strongPtIndices, double thIncr,
    vector<vector<int> > &strongVertHplaneIndex,
    vector<double> &interPtObjVal, vector<int> &interPtObjIndex,
    vector<int> &interPtSplit, vector<vector<int> > &pointOrRayFlag,
    const std::vector<int>& numPointsOrRays, FILE *inst_info_out);

void genCutsBasedOnRankHeur4(int nCutsInBudget, int &nCutsGenerated,
    PointCutsSolverInterface* solver, PointCutsSolverInterface* &cutLPSolvers,
    SolutionInfo &solnInfo, chooseCutGeneratingSets &chooseSplits,
    vector<vector<vector<int> > > &raysToBeCutByHplane,
    vector<int> &allRaysCut, vector<vector<int> > &raysToBeCut,
    vector<vector<int> > &hplanesIndxToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    vector<vector<int> > &hplanesRowIndxToBeActivated,
    vector<cut> &cutStore, vector<vector<int> > &cutsSpecificToSplits,
    vector<vector<vector<int> > > &strongPtIndices, double thIncr,
    vector<vector<int> > &strongVertHplaneIndex,
    vector<double> &interPtObjVal, vector<int> &interPtObjIndex,
    vector<int> &interPtSplit, vector<vector<int> > &pointOrRayFlag,
    const std::vector<int>& numPointsOrRays, FILE *inst_info_out);

void smartGICGeneration1(int nCutsInBudget, int &nCutsGenerated,
    SolutionInfo &solnInfo, chooseCutGeneratingSets &chooseSplits,
    PointCutsSolverInterface* &cutLPSolvers,
    vector<vector<int> > &cutsSpecificToSplits,
    list<ptSortInfo*> &interPtObjFns, vector<cut> &cutStore,
    vector<vector<ptOrRayInfo*> > &intPtOrRayInfo, const double *LB,
    const double *UB, vector<vector<int> > &hplanesIndxToBeActivated,
    vector<vector<int> > &hplanesRowIndxToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    vector<vector<vector<int> > > &strongPtIndices, double thIncr,
    FILE *inst_info_out);

void smartGICGeneration2(int nCutsInBudget, int &nCutsGenerated,
    SolutionInfo &solnInfo, chooseCutGeneratingSets &chooseSplits,
    PointCutsSolverInterface* &cutLPSolvers,
    vector<vector<int> > &cutsSpecificToSplits,
    list<ptSortInfo*> &interPtObjFns, vector<cut> &cutStore,
    vector<vector<ptOrRayInfo*> > &intPtOrRayInfo, const double *LB,
    const double *UB, vector<vector<int> > &hplanesIndxToBeActivated,
    vector<vector<int> > &hplanesRowIndxToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    vector<vector<vector<int> > > &strongPtIndices, double thIncr,
    FILE *inst_info_out);

void smartGICGeneration3(int nCutsInBudget, int &nCutsGenerated,
    SolutionInfo &solnInfo, chooseCutGeneratingSets &chooseSplits,
    PointCutsSolverInterface* &cutLPSolvers,
    vector<vector<int> > &cutsSpecificToSplits,
    list<ptSortInfo*> &interPtObjFns, vector<cut> &cutStore,
    vector<vector<ptOrRayInfo*> > &intPtOrRayInfo,
    vector<vector<int> > &pointOrRayFlag,
    const std::vector<int>& numPointsOrRays, const double *LB,
    const double *UB, vector<vector<int> > &hplanesIndxToBeActivated,
    vector<vector<int> > &hplanesRowIndxToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    vector<vector<vector<int> > > &strongPtIndices, double thIncr,
    const double* objCoeff, FILE *inst_info_out);

void smartGICGeneration4(int &totalObjIntPts, int &ptsNotCut, int &nUnbddPoints,
    int &compUnbddPoints, int nCutsInBudget, int &nCutsGenerated,
    SolutionInfo &solnInfo, chooseCutGeneratingSets &chooseSplits,
    PointCutsSolverInterface* &cutLPSolvers,
    vector<vector<int> > &cutsSpecificToSplits,
    list<ptSortInfo*> &interPtObjFns, vector<cut> &cutStore,
    vector<vector<ptOrRayInfo*> > &intPtOrRayInfo,
    vector<vector<int> > &pointOrRayFlag,
    const std::vector<int>& numPointsOrRays, const double *LB,
    const double *UB, vector<vector<int> > &hplanesIndxToBeActivated,
    vector<vector<int> > &hplanesRowIndxToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    vector<vector<vector<int> > > &strongPtIndices, double thIncr,
    const double* objCoeff, FILE *inst_info_out);

void smartGICGeneration5(int &totalObjIntPts, int &ptsNotCut, int &nUnbddPoints,
    int &compUnbddPoints, int nCutsInBudget, int &nCutsGenerated,
    SolutionInfo &solnInfo, chooseCutGeneratingSets &chooseSplits,
    PointCutsSolverInterface* &cutLPSolvers,
    vector<vector<int> > &cutsSpecificToSplits,
    list<ptSortInfo*> &interPtObjFns, vector<cut> &cutStore,
    vector<vector<ptOrRayInfo*> > &intPtOrRayInfo,
    vector<vector<int> > &pointOrRayFlag,
    const std::vector<int>& numPointsOrRays, const double *LB,
    const double *UB, vector<vector<int> > &hplanesIndxToBeActivated,
    vector<vector<int> > &hplanesRowIndxToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    vector<vector<vector<int> > > &strongPtIndices, double thIncr,
    const double* objCoeff, FILE *inst_info_out);

void generateGICsAvgCutOptimalBasis(cut &tmpCut, int nTotalCuts,
    int &nCutsGenerated, PointCutsSolverInterface* &cutLPSolvers,
    int numSplits, int nNonBasicCols, vector<cut> &cutStore,
    vector<vector<int> > &cutsSpecificToSplits,
    chooseCutGeneratingSets &chooseSplits);

/**********************************************************************************
 * Useful functions
 **********************************************************************************/
/**
 * @brief Removes any cuts that are repeated.
 * Note that the same cut may possibly be generated for different splits,
 * and this is okay, but for the same split, there should be only one of each.
 */
void removeDuplicateCuts(vector<cut> &cutinfo);

/**
 * @brief Checks whether a specific cut exists in the cut store.
 *
 * @param tmpcutinfo  ::  The current set of cuts stored (no dups).
 * @param split       ::  Split for the cut being checked.
 * @param rhs         ::  Rhs of the cut being checked.
 * @param coeffs      ::  Coeffs of the cut being checked.
 *
 * @returns -1 if there does not exist a cut in the cuts passed in which all coeffs
 * of the cut are the same, the rhs is the same, and the split is the same.
 * @returns Index of the cut that is the same if one does exist.
 */
int cutExists(vector<cut> &tmpcutinfo, const int split, const double rhs,
    vector<double> &coeffs);

/**********************************************************************************
 * Printing methods for cuts
 **********************************************************************************/

void printGICs(FILE* fptr, PointCutsSolverInterface* solver,
    chooseCutGeneratingSets &chooseSplits, SolutionInfo &solnInfo,
    vector<cut> &GICCutStore, int* numVert, vector<int>& unbnddVertFails,
    vector<int>& nonUnqiueVertFails);

#endif /* GENERATEGICS_HPP_ */
