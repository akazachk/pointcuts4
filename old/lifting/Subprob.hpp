//============================================================================
// Name        : Subprob.hpp
// Author      : akazachk
// Version     : 0.2013.05.08
// Copyright   : Your copyright notice
// Description : 
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#pragma once

#include <iostream>
#include <fstream>
#include <string>

#include "CoinPackedMatrix.hpp"
#include "OsiSolverInterface.hpp"
#include "typedefs.hpp"
//#include "OsiCpxSolverInterface.hpp"
#include "Output.hpp"
#include "GlobalConstants.hpp"

/***********************************************************************/
/**
 * @brief Generates subproblem, if required
 */
void genSubprobMaster(PointCutsSolverInterface* subprob,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfoOrig,
    std::vector<int>& deletedCols, std::vector<int>& oldRowIndices,
    std::vector<int>& oldColIndices);

/***********************************************************************/
/**
 * @brief Checks whether the variable in column col is one we want to delete.
 *
 * @returns -1 if the variable should be deleted AND complemented.
 * @returns 0 if the variable should not be deleted.
 * @returns 1 if the variable should be deleted but NOT complemented.
 */
int varToDelete(int col, PointCutsSolverInterface* solver, int subspace_option);

/***********************************************************************/
/**
 * Deletes rows that are blank.
 */
void cleanProb(PointCutsSolverInterface* , std::vector<int> &, int, FILE*);

/***********************************************************************/
/**
 * Generates subproblem:
 *  1. We complement all $x$ that at their upper-bound.
 *  2. Retain (1) all fractional $\{0,1\}$ variables,
 *            (2) all positive continuous,
 *            (3) all nonbasic slacks.
 *  3. Remove all other variables.
 */
void genSubProb(PointCutsSolverInterface *solver, std::string out_f_name_stub,
    std::vector<int> &deleteCols, std::vector<int> &oldRowIndices,
    std::vector<int> &oldColIndices, int subspace_option,
    FILE* instances_info_out);

