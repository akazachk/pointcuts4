////============================================================================
//// Name        : Lifter.hpp
//// Author      : akazachk
//// Version     : 0.2013.mm.dd
//// Copyright   : Your copyright notice
//// Description :
////
//// This code is licensed under the terms of the Eclipse Public License (EPL).
////============================================================================
//
//#pragma once
//
//#include <iostream>
//#include <fstream>
//#include <sstream>
//#include <cstdio>
//#include <cmath>
//#include <stdlib.h>
//#include <cfloat>
//#include <vector>
//#include <string>
//
//#include "GlobalConstants.hpp"
//#include "Utility.hpp"
//#include "Output.hpp"
//#include "Subprob.hpp"
////#include "Structs.hpp"
//
//#include "CoinPackedMatrix.hpp"
//#include "OsiSolverInterface.hpp"
//#include "typedefs.hpp"
//
///***********************************************************************/
///**
// * Set up integrated lifting LP in the u, v space.
// */
//void setupLandPUVSpace(LiftGICsSolverInterface* Dk,
//    LiftGICsSolverInterface* subprob, AdvCut &cutinfo, int currvar);
//
///***********************************************************************/
///**
// * Update integrated lifting LP in the u, v space
// * with info for the new split.
// */
//void updateLandPUVSpace(LiftGICsSolverInterface* Dk, AdvCut &cutinfo, int currvar,
//    int oldSplitIndex, double floorxk, double ceilxk, int numSubRows,
//    int numSubCols);
//
///***********************************************************************/
///**
// * Generates lifting in the L&P style.
// */
//void liftingLandPUVSpace(AdvCuts &liftedCut, AdvCuts &orderedLiftedCut,
//    AdvCut &cutinfo, LiftGICsSolverInterface* subprob,
//    LiftGICsSolverInterface* solver, std::vector<int> &deletedCols,
//    std::vector<int> &oldRowIndices, std::vector<int> &oldColIndices,
//    int max_pivots, int &num_pivots, const std::string &out_f_name_stub,
//    const std::string &cutname, LiftGICsSolverInterface* Dk,
//    int subspace_option, FILE *inst_info_out);
//
///***********************************************************************/
///**
// * Lift using the current optimal solution to dualsolver
// */
//void liftUsingCoeffsUVSpace(AdvCuts &liftedCut,
//    AdvCuts &orderedLiftedCut, AdvCut &cutinfo,
//    LiftGICsSolverInterface* subprob, LiftGICsSolverInterface* solver,
//    LiftGICsSolverInterface* Pk, LiftGICsSolverInterface* dualsolver,
//    vector<int> &deletedCols, vector<int> oldRowIndices,
//    std::vector<int> &oldColIndices, int subspace_option,
//    FILE* inst_info_out);
//
///***********************************************************************/
///**
// * Generates the entire L&P program (in the extended formulation space)
// * Num rows:
// *   m + 1 rows for the first side of the disjunction,
// *  + m + 1 rows for the second side of the disjuction,
// *  + n rows for the x = x^1 + x^2 constraints
// *  + 1 row for the lambda1 + lambda2 = 1 constraint
// *  = 2m + n + 3
// * Num cols:
// *    x, x^1, x^2, lambda1, lambda2
// *  = 3n + 2
// */
//void genLandPUVSpace(LiftGICsSolverInterface* liftingSolver, AdvCut &cutinfo,
//    LiftGICsSolverInterface* subsolver);
//
///***********************************************************************/
///**
// * Set up split lifting LPs in the u, v space.
// */
//void setupLandPSplit(LiftGICsSolverInterface* Dku, LiftGICsSolverInterface* Dkv,
//    LiftGICsSolverInterface* subprob, AdvCut &cutinfo, int currvar);
//
///***********************************************************************/
///**
// * Update split lifting LPs in the u, v space
// * with info for the new split.
// */
//void updateLandPSplit(LiftGICsSolverInterface* Dku, LiftGICsSolverInterface* Dkv,
//    AdvCut &cutinfo, int &currvar, int &oldSplitIndex, double &floorxk,
//    double &ceilxk, int &numSubRows, int &numSubCols);
//
///***********************************************************************/
///**
// * Generates lifting in the L&P style, with separate LPs for u and v space
// */
//void liftingLandPSplit(AdvCuts &liftedCut, AdvCuts &orderedLiftedCut,
//    AdvCut &cutinfo, LiftGICsSolverInterface* subprob,
//    LiftGICsSolverInterface* solver, std::vector<int> &deletedCols,
//    std::vector<int> &oldRowIndices, std::vector<int> &oldColIndices,
//    int max_pivots, const std::string &out_f_name_stub,
//    const std::string &cutname, LiftGICsSolverInterface* Dku,
//    LiftGICsSolverInterface* Dkv, int subspace_option, FILE *inst_info_out);
//
///***********************************************************************/
///**
// * Generates the L&P program (in the extended formulation space)
// * for the u variables only
// *
// * We have the problem
// *
// * min \bar\alpha^T x
// * s.t.
// *     Ax \ge b         (u)
// *     -x_k \ge -\pi_0  (u_0)
// *      x \ge \ell      (\lambda)
// *     -x \ge -g        (\mu)
// *
// * Dual is
// *
// * max b^T u - pi_0 u_0 + \ell^T \lambda - g^T \mu
// *     (same as)
// * min -b^T u + pi_0 u_0 - \ell^T \lambda + g^T \mu
// * s.t.
// *     A^T u - e_k u_0 + \lambda - \mu = \bar\alpha
// *     u, u_0 \ge 0 (depends actually on rows)
// *
// */
//void genLandPU(LiftGICsSolverInterface* liftingSolver, AdvCut &cutinfo,
//    LiftGICsSolverInterface* subsolver);
//
///***********************************************************************/
///**
// * Generates the L&P program (in the extended formulation space)
// * for the v variables only
// *
// * We have the problem
// *
// * min \bar\alpha^T x
// * s.t.
// *     Ax \ge b         (u)
// *    x_k \ge ceilxk    (u_0)
// *      x \ge \ell      (\lambda)
// *     -x \ge -g        (\mu)
// *
// * Dual is
// *
// * max b^T v + ceilxk v_0 + \ell^T \lambda - g^T \mu
// *     (same as)
// * min -b^T v - pi_0 v_0 - \ell^T \lambda + g^T \mu
// * s.t.
// *     A^T v + e_k v_0 + \lambda - \mu = \bar\alpha
// *     v, v_0 \ge 0 (depends actually on rows)
// *
// */
//void genLandPV(LiftGICsSolverInterface* liftingSolver, AdvCut &cutinfo,
//    LiftGICsSolverInterface* subsolver);
//
///***********************************************************************/
///**
// * Lift using the current optimal solution to dualsolvers Dku and Dkv
// */
//void liftUsingCoeffsSplit(AdvCuts &liftedCut, AdvCuts &orderedLiftedCut,
//    AdvCut &cutinfo, LiftGICsSolverInterface* subprob,
//    LiftGICsSolverInterface* solver, LiftGICsSolverInterface* Dku,
//    LiftGICsSolverInterface* Dkv, vector<int> &deletedCols,
//    vector<int> oldRowIndices, std::vector<int> &oldColIndices,
//    int subspace_option, FILE* inst_info_out);
//
///***********************************************************************/
///**
// * @brief Lift coefficients from subspace to full space.
// * @param solver        ::  Original problem. Call this (LP).
// * @param deletedCols   ::  Indices of columns deleted from (LP).
// * @param oldRowIndices ::  Row i in subprob is oldRowIndices[i] of (LP).
// * @param DDual         ::  Dual solution to D.
// * @param cutinfo       ::  Info about the AdvCut generated in the subspace.
// * @param liftedCut     ::  Coefficients to be transmitted about lifted AdvCut.
// */
//void liftLandP(AdvCut &liftedCut, LiftGICsSolverInterface* solver,
//    std::vector<int> &deletedCols, std::vector<int> &oldRowIndices,
//    std::vector<double> &DDual, AdvCut &cutinfo, int subspace_option,
//    FILE* inst_info_out);
//
///***********************************************************************/
///**
// * Reorders the lifted AdvCut so that the order of the columns is the same
// * as the order of the columns in the original problem. (This order has
// * changed as a result of creating the subspace and deleting variables.)
// */
//void orderLiftedCut(AdvCut &liftedCut, LiftGICsSolverInterface *solver,
//    LiftGICsSolverInterface *subprob, vector<int> &deletedCols,
//    AdvCut &orderedLiftedCut);
//
///***********************************************************************/
///**
// *
// */
//void getPivots(LiftGICsSolverInterface* solver, SolutionInfo &solnInfo,
//    vector<int> &colIn, vector<int> &colInNBIndex, vector<int> &inStatus,
//    vector<int> &colOut, vector<int> &outStatus, vector<double> &minRatio,
//    int &num_pivots, FILE* inst_info_out);
//
///***********************************************************************/
///**
// * @brief Puts DDual ordered according to rows of the original subprob correctly.
// *
// * Suppose that the subproblem has m rows and n columns
// *  Constraints 0 through m-1 correspond to Ax^0 \ge b \lambda_0
// *  Constraint m is -x_k^0 \ge -\pi_0 \lambda_0
// *  Constraints m+1 through 2m correspond to Ax^1 \ge b \lambda_1
// *  Constraint 2m+1 is x_k^1 \ge (\pi_0 + 1) \lambda_1
// *  Constraint 2m+2 is \lambda_0 + \lambda_1 = 1
// * Then we go into lower-bounds and upper-bounds, but maybe not keep those?
// */
//void orderDualVarsFromCplexOutput(vector<double> &DDual,
//    LiftGICsSolverInterface* subprob, LiftGICsSolverInterface* dualsolver,
//    LiftGICsSolverInterface* Pk);
//
///***********************************************************************/
///**
// * @brief Puts DDual ordered according to rows of the original subprob correctly
// */
//void orderDualVars(vector<double> &DDual, LiftGICsSolverInterface* subprob,
//    LiftGICsSolverInterface* dualsolver, LiftGICsSolverInterface* Pk);
//
/////***********************************************************************/
/////**
//// * Generates lifting in the L&P style.
//// */
////void liftingLandP(AdvCuts &liftedCut, AdvCuts &orderedLiftedCut,
////    AdvCut &cutinfo, LiftGICsSolverInterface* subprob, LiftGICsSolverInterface* solver,
////    std::vector<int> &deletedCols, std::vector<int> &oldRowIndices,
////    int max_pivots, const std::string &out_f_name_stub,
////    const std::string &cutname, char* write_dual_fname, FILE *inst_info_out);
////
/////***********************************************************************/
/////**
//// * Lift using the current optimal solution to dualsolver
//// */
////void liftUsingCoeffs(AdvCuts &liftedCut, AdvCuts &orderedLiftedCut,
////    AdvCut &cutinfo, LiftGICsSolverInterface* subprob, LiftGICsSolverInterface* solver,
////    LiftGICsSolverInterface* Pk, LiftGICsSolverInterface* dualsolver,
////    vector<int> &deletedCols, vector<int> oldRowIndices, FILE* inst_info_out);
////
/////***********************************************************************/
/////**
//// * Generates the entire L&P program (in the extended formulation space)
//// * Num rows:
//// *   m + 1 rows for the first side of the disjunction,
//// *  + m + 1 rows for the second side of the disjuction,
//// *  + n rows for the x = x^1 + x^2 constraints
//// *  + 1 row for the lambda1 + lambda2 = 1 constraint
//// *  = 2m + n + 3
//// * Num cols:
//// *    x, x^1, x^2, lambda1, lambda2
//// *  = 3n + 2
//// */
////void genLandP(LiftGICsSolverInterface* liftingSolver, AdvCut &cutinfo,
////    LiftGICsSolverInterface* subsolver);
//
