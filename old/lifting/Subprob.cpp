//============================================================================
// Name        : Subprob.cpp
// Author      : akazachk
// Version     : 0.2013.05.08
// Copyright   : Your copyright notice
// Description : 
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#include "Subprob.hpp"

/***********************************************************************/
/**
 * @brief Generates subproblem, if required
 */
void genSubprobMaster(PointCutsSolverInterface* subprob,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfoOrig,
    std::vector<int>& deletedCols, std::vector<int>& oldRowIndices,
    std::vector<int>& oldColIndices) {
  // Generate the subproblem
  const std::string out_f_name_stubSub = LiftGICsParam::out_f_name_stub
      + "Sub";
  genSubProb(&subprob, out_f_name_stubSub, deletedCols, oldRowIndices,
      oldColIndices, LiftGICsParam::paramVal[SUBSPACE_PARAM_IND],
      LiftGICsParam::inst_info_out);

  if (SHOULD_WRITE_SOLN) {
    writeSoln(&subprob, out_f_name_stubSub);

    char OptTabName[300];
    snprintf(OptTabName, sizeof(OptTabName) / sizeof(char),
        "%s-Opt_Tableau.csv", out_f_name_stubSub.c_str());
    FILE* OptTabOut = fopen(OptTabName, "w");
    if (OptTabOut != NULL)
      printSimplexTableauWithNames(OptTabOut, subprob);
    else {
      error_msg(errorstring,
          "Could not open file %s to write optimal simplex tableau.\n",
          OptTabName);
      writeErrorToLog(errorstring, LiftGICsParam::inst_info_out);
      exit(1);
    }
    fclose(OptTabOut);

    char NBTabName[300];
    snprintf(NBTabName, sizeof(NBTabName) / sizeof(char),
        "%s-NB_Opt_Tableau.csv", out_f_name_stubSub.c_str());
    FILE* NBTabOut = fopen(NBTabName, "w");
    if (NBTabOut != NULL)
      printNBSimplexTableauWithNames(NBTabOut, subprob);
    else {
      error_msg(errorstring,
          "Could not open file %s to write NB optimal simplex tableau.\n",
          NBTabName);
      std::cerr << errorstring << std::endl;
      writeErrorToLog(errorstring, LiftGICsParam::inst_info_out);
      exit(1);
    }
    fclose(NBTabOut);
  }

  // Save solution because we will use it later
  if (!subprob.isProvenOptimal()) {
    subprob.initialSolve(); // Because in Subprob, we (used to?) only do this if in debug mode
    if (!subprob.isProvenOptimal()) {
      error_msg(errorstring,
          "Subproblem cannot be solved to optimality!\n");
      writeErrorToLog(errorstring, LiftGICsParam::inst_info_out);
      exit(1);
    }
  }

  //  // TODO REMOVE THIS; THIS IS ONLY FOR "ONLY SUBPROBLEM" PHASE
  //  SolutionInfo solnInfoSubTmp(subprob, true);
  //  LiftGICsParam::timeStats.end_timer(GEN_SUB_TIME);
  //
  //  writeSolnInfoToII(prob_name, &solnInfoOrig, instances_info_out);
  //  writeSubSolnInfoToII(prob_name, &solnInfoSubTmp, -1.0, instances_info_out);
  //
  //  fprintf(instances_info_out, "DONE!\n");
  //  fclose(instances_info_out);
  //  printf("\n FINISHED SUBPROBLEM INFO GENERATION \n");
  //
  //  return (1);
}

/***********************************************************************/
/**
 * @brief Checks whether the variable in column col is one we want to delete.
 *
 * @returns -1 if the variable should be deleted AND complemented.
 * @returns 0 if the variable should not be deleted.
 * @returns 1 if the variable should be deleted but NOT complemented.
 */
int varToDelete(int col, PointCutsSolverInterface* solver, int subspace_option) {
  if (col < 0 || col > solver->getNumCols() || subspace_option == 0)
    return 0;
  double currval = solver->getColSolution()[col];
  double currlb = solver->getColLower()[col];
  double currub = solver->getColUpper()[col];

  // Delete the variable if it is at zero (at a bound); no complementing required.
  // TODO Alex: I think this should be only if zero at LB?
  if (isZero(currval, param.getEPS()) && (isZero(currub, param.getEPS()) || isZero(currlb, param.getEPS())))
    return 1;
  // Delete the variable if it is at its ub and its lower-bound is zero,
  // but also COMPLEMENT this variable.
  if (isVal(currval, currub, param.getEPS()) && isZero(currlb, param.getEPS()))
    return -1;

  return 0;
}

/***********************************************************************/
/**
 * Deletes rows that are blank.
 * NOT IMPLEMENTED: Also delete repetitive rows.
 *    This could remove strong cuts.
 */
void cleanProb(PointCutsSolverInterface* solver, std::vector<int> &oldRowIndices,
    int subspace_option, FILE* instances_info_out) {

#ifdef TRACE
  std::cout
  << "\n############### Starting cleanProb routine to clean subproblem. ###############"
  << std::endl;
#endif

  const CoinPackedMatrix *submx = solver.getMatrixByRow();
  const double *mx = submx->getElements();
  const int *mxindex = submx->getIndices();
  const int *mxstarts = submx->getVectorStarts();

  std::vector<int> deleteRows;

  // Either the row is deleted or it is not and its index should be saved
  bool rowdeleted = false;

  for (int row = 0; row < solver.getNumRows(); row++) {
    int numelems = submx->getVectorSize(row);
    int rowstart = mxstarts[row];

    if (subspace_option == 0) {
      rowdeleted = false;
    } else if (numelems == 0) {
#ifdef TRACE
      printf("Row %d is empty! It will be deleted.\n", row);
#endif
      deleteRows.push_back(row);
      rowdeleted = true;
    } /*else if (numelems == 1) {
     int col = mxindex[rowstart];
     double coeff = mx[rowstart];
     char rowsense = solver.getRowSense()[row];
     double rhs = solver.getRightHandSide()[row];
     double lb = solver.getColLower()[col];
     double ub = solver.getColUpper()[col];

     printf(
     "Row %d is of type %c and has one element. It will be deleted if it is redundant (implied by bounds). Rhs in this row is %f and its coeff is %f.\n",
     row, rowsense, rhs, coeff);

     if (std::abs(coeff) < param.getEPS()) {
     continue;
     }

     double newbound = rhs / coeff;
     bool checkupper = (rowsense == 'L' && coeff > param.getEPS())
     || (rowsense == 'G' && coeff < -param.getEPS());
     bool checklower = (rowsense == 'G' && coeff > param.getEPS())
     || (rowsense == 'L' && coeff < -param.getEPS());

     // If it is an assignment std::vector, then this simply sets the value of this var
     if (rowsense == 'E') {
     if (newbound > ub + param.getEPS() || newbound < lb - param.getEPS()) {
     char errorstring[300];
     snprintf(errorstring, sizeof(errorstring) / sizeof(char),
     "Col %d is set in row %d to be equal to %f which violates one of its bounds. Lb: %f. Ub: %f.\n",
     col, row, newbound, lb, ub);
     std::cerr << errorstring << std::endl;
     writeErrorToII(errorstring, instances_info_out);
     exit(1);
     }

     if (std::abs(lb - newbound) > param.getEPS() || std::abs(ub - newbound) > param.getEPS()) {
     // Update bounds
     solver.setColLower(col, newbound);
     solver.setColUpper(col, newbound);
     printf(
     "\tCol %d is set to %f (by changing both upper- and lower-bounds).\n",
     col, newbound);
     } else {
     // If this is an implied row, we delete it
     printf("\tRow %d is indeed redundant, so it is being deleted.\n", row);
     deleteRows.push_back(row);
     }
     }

     // If this is a <= or >= row, then check whether this is implied by the bounds, or
     //  changes the bounds. Update if it is tighter than the current bound.
     if (checkupper) {
     // If we have x <= rhs/coeff < ub, then rhs is a tigher upper bound on x.
     if (newbound < ub - param.getEPS()) {
     if (newbound < lb - param.getEPS()) {
     char errorstring[300];
     snprintf(errorstring, sizeof(errorstring) / sizeof(char),
     "New bound on col %d is %f, lower than the lower bound %f!\n",
     col, newbound, lb);
     std::cerr << errorstring << std::endl;
     writeErrorToII(errorstring, instances_info_out);
     exit(1);
     }
     solver.setColUpper(col, newbound);
     printf(
     "\tUpper bound for col %d updated to %f <= %f (the old bound).\n",
     col, newbound, ub);
     } else {
     // If this is an implied row, we delete it
     printf("\tRow %d is indeed redundant, so it is being deleted.\n", row);
     deleteRows.push_back(row);
     }
     } else if (checklower) {
     // If we have lb > rhs/coeff > x, then rhs is a tigher upper bound on x.
     if (newbound > lb + param.getEPS()) {
     if (newbound > ub + param.getEPS()) {
     char errorstring[300];
     snprintf(errorstring, sizeof(errorstring) / sizeof(char),
     "New bound on col %d is %f, higher than the upper bound %f!\n",
     col, newbound, ub);
     std::cerr << errorstring << std::endl;
     exit(1);
     }
     solver.setColLower(col, newbound);
     printf(
     "\tLower bound for col %d updated to %f >= %f (the old bound).\n",
     col, newbound, lb);
     } else {
     // If this is an implied row, we delete it
     printf("\tRow %d is indeed redundant, so it is being deleted.\n", row);
     deleteRows.push_back(row);
     }
     }
     } */else if (numelems == 1) {
      int col = mxindex[rowstart];
      double coeff = mx[rowstart];
      char rowsense = solver.getRowSense()[row];
      double rhs = solver.getRightHandSide()[row];
      double lb = solver.getColLower()[col];
      double ub = solver.getColUpper()[col];
#ifdef TRACE
      printf(
          "Row %d is of type %c and has one element. It will be deleted if it is redundant (implied by bounds). Rhs in this row is %f and its coeff is %f.\n",
          row, rowsense, rhs, coeff);
#endif

      if (isZero(coeff, param.getEPS())) {
#ifdef TRACE
        printf(
            "Var coefficient in row %d is %f which is approximately 0. It will be deleted.\n",
            row, coeff);
#endif
        deleteRows.push_back(row);
        rowdeleted = false; // Because we are going to the next row; it won't be added to oldRowIndices.
        continue;
      }

      double newbound = rhs / coeff;
      bool checkupper = (rowsense == 'L'
          && greaterThanVal(coeff, 0.0, param.getEPS()))
          || (rowsense == 'G' && lessThanVal(coeff, 0.0, param.getEPS()));
      bool checklower = (rowsense == 'G'
          && greaterThanVal(coeff, 0.0, param.getEPS()))
          || (rowsense == 'L' && lessThanVal(coeff, 0.0, param.getEPS()));

      // If it is an assignment vector, then this simply sets the value of this var
      if (rowsense == 'E') {
        if (greaterThanVal(newbound, ub, param.getEPS())
            || lessThanVal(newbound, lb, param.getEPS())) {
          error_msg(errorstring,
              "Col %d is set in row %d to be equal to %f which violates one of its bounds. Lb: %f. Ub: %f.\n",
              col, row, newbound, lb, ub);
          writeErrorToLog(errorstring, instances_info_out);
          exit(1);
        }

        if (isVal(lb, newbound, param.getEPS()) && isVal(ub, newbound, param.getEPS())) {
          // If this is an implied row, we delete it
#ifdef TRACE
          printf(
              "\tRow %d is indeed redundant, so it is being deleted.\n",
              row);
#endif
          deleteRows.push_back(row);
          rowdeleted = true;
        }
      }

      // If this is a <= or >= row, then check whether this is implied by the bounds, or
      //  changes the bounds. Update if it is tighter than the current bound.
      if (checkupper) {
        // If we have x <= rhs/coeff < ub, then rhs is a tigher upper bound on x.
        if (newbound > ub - param.getEPS()) {
          // If this is an implied row, we delete it
#ifdef TRACE
          printf(
              "\tRow %d is indeed redundant, so it is being deleted.\n",
              row);
#endif
          deleteRows.push_back(row);
          rowdeleted = true;
        }
      } else if (checklower) {
        // If we have lb > rhs/coeff > x, then rhs is a tigher upper bound on x.
        if (newbound < lb + param.getEPS()) {
          // If this is an implied row, we delete it
#ifdef TRACE
          printf(
              "\tRow %d is indeed redundant, so it is being deleted.\n",
              row);
#endif
          deleteRows.push_back(row);
          rowdeleted = true;
        }
      }
    }

    if (!rowdeleted) {
      oldRowIndices.push_back(row);
    }

    rowdeleted = false; // We are moving on to the next row.
  }

  // Delete the found rows
#ifdef TRACE
  std::cout << "\n## Deleting " << deleteRows.size() << " rows. ##" << std::endl;
#endif

  solver.deleteRows(deleteRows.size(), &deleteRows[0]);
} /* cleanProb */

/*************************/
/* GENERATING SUBPROBLEM */
/*************************/

/***********************************************************************/
/**
 * @brief Generates subproblem
 * @param solver        ::  Original problem (LP) solver.
 * @param f_name        ::  Stub name (including directory).
 * @param deleteCols    ::  Vector in which deleted columns are stored.
 * @param oldRowIndices ::  Indices of rows as they are in orig problem.
 * @param out_stream    ::  Out stream.
 *
 *  1. We complement all $x$ that at their upper-bound.
 *  2. Retain (1) all fractional $\{0,1\}$ variables,
 *            (2) all positive continuous,
 *            (3) all non-basic slacks. --- NOT CURRENTLY DONE.
 *  3. Remove all other variables.
 *
 *  TODO: Get this procedure to work with lower-bounds that are not zero.
 *  All that needs to be done is to make sure that this value is accounted for in the rhs and obj.
 */
void genSubProb(PointCutsSolverInterface *solver, std::string out_f_name_stub,
    std::vector<int> &deleteCols, std::vector<int> &oldRowIndices,
    std::vector<int> &oldColIndices, int subspace_option,
    FILE* instances_info_out) {
//  // Check that we actually want to generate a subspace
//  if (subspace_option == 0)
//    return;

#ifdef TRACE
  std::cout
  << "\n############### Starting genSubProb routine to create subproblem. ###############"
  << std::endl;
#endif

  const int numrows = solver->getNumRows();
  const int numcols = solver->getNumCols();
//  const double *row_act = solver->getRowActivity(); // constraint matrix times solution std::vector
  const double *rhs = solver->getRightHandSide();
  const double *obj = solver->getObjCoefficients();

  int *cstat = new int[numcols]; // for basis status (0: free, 1: basic, 2: upper, 3: lower)
  int *rstat = new int[numrows]; // for basis status (0: free, 1: basic, 2: upper, 3: lower)
  solver->getBasisStatus(cstat, rstat); // 0: free  1: basic
                      // 2: upper 3: lower

  int *basis_index = new int[numrows]; // basis_index[i] =
                     //        index of pivot var in row i
                     //        (slack if number >= ncol)
  solver->getBasics(basis_index);

  std::vector<double> soln;

#ifdef TRACE
  for (int col = 0; col < numcols; col++) {
    double tmp = solver->getColSolution()[col];
//    std::cout << "Value of x" << col+1 << " is " << tmp << "." << std::endl;
    if (cstat[col] == 1) {          // only keep basic variables
      soln.push_back(tmp);// we will use this to make sure our subproblem solution is the same as our current one
    }
  }
#endif

  // To create subproblem
//  CoinPackedMatrix submx;
//  submx.copyOf(*(solver->getMatrixByRow())); // Copy A into submx so we can change stuff
//  const double *mx = submx.getElements(); // If we change stuff, we will have to call getElements again.
//  const int *mxindex = submx.getIndices();
//  const int *mxstarts = submx.getVectorStarts();

  const double *mx = solver->getMatrixByRow()->getElements(); // If we change stuff, we will have to call getElements again.
  const int *mxindex = solver->getMatrixByRow()->getIndices();
  const int *mxstarts = solver->getMatrixByRow()->getVectorStarts();

//#ifdef TRACE
//  printf("Printing matrix.\n");
//  for (int i=0; i<submx.getNumElements(); i++)
//    printf("%f ",mx[i]);
//  printf("\n");
//#endif

  const char *rowsense = solver->getRowSense(); // will tell whether <= ('L'), = ('E'), >= ('G') constraint,
                          // ranged constraint ('R'), or free constraint ('N')

  /*
   * Now we complement all variables at upper-bound. That is,
   * given variable $x_j$, then for each row $i$, we complement
   * by subtracting $a_{ij}$ from the rhs $b_i$.
   */

#ifdef TRACE
  std::cout
  << "\n## Complementing all variables at upper-bound whose lower-bound is zero. ##"
  << std::endl;
#endif

// Change std::vector accordingly for each row in which we have a variable to complement
  for (int i = 0; i < numrows; i++) {
// Figure out where the entries of the row are within mx
    int rowstart = mxstarts[i];
    int nextrowstart = rowstart
        + solver->getMatrixByRow()->getVectorSize(i);

//#ifdef TRACE
//    std::cout << "Row: " << i << " with name " << solver->getRowName(i)
//        << ". rowstart: " << rowstart << ". nextrowstart: " << nextrowstart
//        << "." << std::endl;
//#endif

// Sum up total by which rhs needs to be changed
    double sum = 0;

    for (int k = rowstart; k < nextrowstart; k++) {
      int col = mxindex[k];
      double entry = mx[k];
      double val = solver->getColSolution()[col];
//      double entryub = solver->getColUpper()[col];
//      double entrylb = solver->getColLower()[col];

      // If this variable is one of those that needs be complemented:
      if (varToDelete(col, solver, subspace_option) == -1) {
//#ifdef TRACE
//        std::cout << "Complementing entry (" << i << "," << col << ") with name "
//            << solver->getColName(col) << " and coeff " << entry << "." << std::endl;
//#endif
        double entXval = entry * val;
        if (!isZero(val, param.getEPS()) && !isZero(entry, param.getEPS())
            && !isZero(entXval, param.getEPS()))
          sum += entXval; // add its coefficient * value to the sum, and
//        solver->modifyCoefficient(i,col,-1*entry,false); // we don't actually need to do this. these variables will be deleted.
      }
    }

//#ifdef TRACE
//    std::cout << "Sum to change rhs of row " << i << " by: " << sum
//        << ". (We subtract this quantity.)" << std::endl;
//#endif
    if (!isZero(sum, param.getEPS())) { // TODO: If sum is close to zero, will this cause problems?

      // If it is a <= constraint, we use setRowUpper(int elemIndex, double elemValue)
      if (rowsense[i] == 'L') {
//#ifdef TRACE
//        std::cout << "Detected row " << i << " is L for <=. Changing rhs from "
//            << rhs[i] << " to " << rhs[i] - sum << "." << std::endl;
//#endif
        solver->setRowUpper(i, rhs[i] - sum);
      }
      // If it is a >= constraint, we use setRowLower(int elemIndex, double elemValue)
      else if (rowsense[i] == 'G') {
//#ifdef TRACE
//        std::cout << "Detected row " << i << " is G for >=. Changing rhs from "
//            << rhs[i] << " to " << rhs[i] - sum << "." << std::endl;
//#endif
        solver->setRowLower(i, rhs[i] - sum);
      }
      // If it is a ranged constraint, we change upper and lower bounds.
      else if (rowsense[i] == 'R') {
#ifdef TRACE
        std::cout << "Detected row " << i << " is R. Changing rhs from "
        << rhs[i] << " to " << rhs[i] - sum << "." << std::endl;
#endif
        //      solver->setRowUpper(i,rhs[i]-sum);
        solver->setRowBounds(i, rhs[i] - sum, rhs[i] - sum);
        std::cerr << "Detected row " << i << " with name "
            << solver->getRowName(i) << " is R. Changing rhs from "
            << rhs[i] << " to " << rhs[i] - sum << "." << std::endl;
        std::cerr
            << "This is the first time you've encountered a ranged constraint. Make sure it's right, then delete this line and the return below it."
            << std::endl;
        writeErrorToLog(
            "This is the first time you've encountered a ranged constraint. Make sure it's right, then delete this line and the return below it.\n",
            instances_info_out);
        fclose(instances_info_out);
        return;
      }
      // Else, it could be an equality constraint
      else if (rowsense[i] == 'E') {
//#ifdef TRACE
//        std::cout << "Detected row " << i << " is E for =. Changing rhs from "
//            << rhs[i] << " to " << rhs[i] - sum << "." << std::endl;
//#endif
        solver->setRowBounds(i, rhs[i] - sum, rhs[i] - sum);
      }
      // Else, it could be 'N', which seems to mean objective in the language of MPS files
      else if (rowsense[i] == 'N') {
#ifdef TRACE
        std::cout << "*** ERROR: Detected row " << i
        << " is N. NOT CHANGING ANYTHING. Old rhs: " << rhs[i]
        << ". Sum: " << sum << "." << std::endl;
#endif
      } else {
#ifdef TRACE
        std::cout << "*** ERROR: Detected row " << i << " as " << rowsense[i]
        << ", something other than L, G, R, E, or N. Do not know what to do, so skipping it. ";
        std::cout << "Old rhs: " << rhs[i] << ". Sum: " << sum << "."
        << std::endl;
#endif
        std::cerr << "*** ERROR: Detected row " << i << " as "
            << rowsense[i]
            << ", something other than L, G, R, E, or N. Do not know what to do, so skipping it. ";
        std::cerr << "Old rhs: " << rhs[i] << ". Sum: " << sum << "."
            << std::endl;
        std::cerr
            << " FIRST TIME SKIPPING ROW! SEE WHAT HAPPENED! (Then delete this line and the return after it.)"
            << std::endl;
        writeErrorToLog(
            "FIRST TIME SKIPPING ROW! SEE WHAT HAPPENED! (Then delete this line and the return after it.)\n",
            instances_info_out);
        fclose(instances_info_out);
        exit(1);
      }
    }
//    std::cout << "Row: " << i << ". Old rhs: " << rhs[i] << ". New rhs LB: " << solver->getRowLower()[i] << ". New rhs UB: " << solver->getRowUpper()[i] << "." << std::endl;
  }

  /*
   * Now we complement the objective coefficients. Simultaneously, figure out which variables to keep.
   */
//  std::vector<int> deleteCols; // This will hold the indices of the columns (vars) we delete
  std::vector<std::string> deleteColsNames; // This will hold the names of these variables
  std::vector<double> deleteColsVals; // This will hold their value in the optimal solution (wrt to this basis)
  std::vector<double> deleteColsObjCoeffs; // This will hold their objective function coefficients (which we lose)
  std::vector<double> deleteColsLBs; // The lbs of these variables
  std::vector<double> deleteColsUBs; // The ubs of these variables
  double objSum = 0;

#ifdef TRACE
  std::cout << "\n## Deciding which variables to keep/delete. ##" << std::endl;
#endif
  for (int col = 0; col < numcols; col++) {
    // Do we delete the variable?
    double currval = solver->getColSolution()[col];
    double lbcurr = solver->getColLower()[col];
    double ubcurr = solver->getColUpper()[col];
    if (cstat[col] == 0) {
      // Don't do anything here, we will keep these variables.
      if (isVal(currval, lbcurr) || isVal(currval, ubcurr)) {
#ifdef TRACE
        printf(
            "Free variable with index %d and name %s has value %2.3f, which is a bound. LB: %2.3f. UB: %2.3f.\n",
            col, solver->getColName(col).c_str(), currval, lbcurr,
            ubcurr);
#endif
      }
      oldColIndices.push_back(col);
    } else {
      int col_status = varToDelete(col, solver, subspace_option);
      if (!isZero(col_status, param.getEPS())) {
//        if (lbcurr > -1 * solver->getInfinity() + param.getEPS()) {
//          if (ubcurr < solver->getInfinity() - param.getEPS()) {
//            printf(
//                "FOUND VAR TO DELETE: Deleting variable %d with name %s with value %2.3f and objective coefficient %2.3f. LB: %2.3f. UB: %2.3f.\n",
//                col, solver->getColName(col).c_str(), currval, obj[col], lbcurr,
//                ubcurr);
//          } else {
//            printf(
//                "FOUND VAR TO DELETE: Deleting variable %d with name %s with value %2.3f and objective coefficient %2.3f. LB: %2.3f. UB: inf.\n",
//                col, solver->getColName(col).c_str(), currval, obj[col],
//                lbcurr);
//          }
//        } else if (ubcurr < solver->getInfinity() - param.getEPS()) {
//          printf(
//              "FOUND VAR TO DELETE: Deleting variable %d with name %s with value %2.3f and objective coefficient %2.3f. LB: -inf. UB: %2.3f.\n",
//              col, solver->getColName(col).c_str(), currval, obj[col], ubcurr);
//        } else {
//          printf(
//              "*** ERROR: Deleting var %d with name %s with value %2.3f; this has no upper or lower bounds.",
//              col, solver->getColName(col).c_str(), currval);
//        }
        deleteCols.push_back(col);
        deleteColsNames.push_back(solver->getColName(col));
        deleteColsVals.push_back(currval);
        deleteColsObjCoeffs.push_back(obj[col]);
        deleteColsLBs.push_back(lbcurr);
        deleteColsUBs.push_back(ubcurr);
        objSum += obj[col] * currval; // Our objective is going to be different by some amount because of complementing

        // Note that we do not have to do anything to the bounds here because the variables are being deleted.
      } else {
        oldColIndices.push_back(col);
      }
    }
  }

  // Delete rows and columns (vars) that are not necessary for the subproblem
  // solver->deleteRows(2);
  if (deleteCols.size() > 0) {
#ifdef TRACE
    std::cout << "\n## Deleting " << deleteCols.size() << " variable(s). ##"
    << std::endl;
#endif
    solver->deleteCols(deleteCols.size(), &deleteCols[0]);
//    submx.deleteCols(deleteCols.size(), &deleteCols[0]);
  }

  // Replace matrix
//  solver->replaceMatrixOptional(submx);

  cleanProb(*solver, oldRowIndices, subspace_option, instances_info_out);

  // Generate LP and MPS file from this revised problem
  solver->writeLp(out_f_name_stub.c_str(), "lp"); // replaces file if it already exists
  solver->writeMps(out_f_name_stub.c_str(), "mps"); // replaces file if it already exists
#ifdef TRACE
      std::cout << "\n## Subproblem written to " << out_f_name_stub << ".lp. ##"
      << std::endl;
#endif

  // Save delete variables + sum we removed
  writeDeletedVars(deleteCols, deleteColsNames, deleteColsVals,
      deleteColsObjCoeffs, deleteColsLBs, deleteColsUBs, objSum,
      out_f_name_stub);
#ifdef TRACE
  std::cout
  << "\n## Make sure that the solution of the subproblem is the same as that of the original problem. ##"
  << std::endl;
#endif
  double oldObj = solver->getObjValue();
#ifdef TRACE
  std::cout << "Old objective value: " << oldObj << "." << std::endl;
#endif
  solver->initialSolve();
#ifdef TRACE
  std::cout << "New objective value: " << solver->getObjValue()
  << " + sum we removed: " << objSum << " is "
  << solver->getObjValue() + objSum << "." << std::endl;
#endif
  if (!isZero(solver->getObjValue() + objSum - oldObj, param.getEPS())) {
    error_msg(errorstring,
        "While generating subproblem, new objective is not equal to the old objective! New obj: %f. Amount deleted: %f. Old objective: %f.\n",
        solver->getObjValue(), objSum, oldObj);
    writeErrorToLog(errorstring, instances_info_out);
    exit(1);
  }
//  assert((solver->getObjValue() + objSum - oldObj) < param.getEPS());
//  writeSoln(solver, f_name.substr(0,f_name.find_last_of("."))+"-resolved");
//  std::cout << "Difference between old solution and new solution, variable-by-variable:" << std::endl;
//  std::cout << "\t";
//  for (int col=0; col<solver->getNumCols(); col++) {
//    printf("Value of %s is %f. %-10sOriginally: %f.\n", solver->getColName(col), solver->getColSolution()[col], "\t", soln[col]);
//    std::cout << soln[col] - solver->getColSolution()[col] << " "; // THIS COULD THROW ERROR? soln[col] ...?
//    if (col>0 && (col%15)==14)
//      std::cout << "\\" << std::endl;
//      std::cout << "\t";
//  }
//  std::cout << std::endl;
//  std::cout << "If one of the above entries is nonzero, something went wrong." << std::endl;

  delete[] cstat;
  delete[] rstat;
  delete[] basis_index;
} /* genSubProb */
