//============================================================================
// Name        : SIC.hpp
// Author      : Aleksandr M. Kazachkov
// Version     : 0.2014.03.10
// Copyright   : Your copyright notice
// Description : Generates standard intersection cuts
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#pragma once

#include "SolutionInfo.hpp"

#include "typedefs.hpp"
#include "AdvCut.hpp"
#include "Point.hpp"
#include "Ray.hpp"

void genSICs(AdvCuts& structSICs, AdvCuts& NBSICs,
    const PointCutsSolverInterface* const solver, SolutionInfo& solnInfo,
    const bool working_in_subspace);

void genNBSICs(AdvCuts& NBSIC, const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo);

/***********************************************************************/
/**
 * @brief Generate a single SIC in the NB space of the form alpha x \ge beta.
 *      Beta is 1 unless there is some complementing.
 */
void genNBSIC(AdvCut& NBSIC, double& absSumCoeffs,
    const PointCutsSolverInterface* const solver,
    const std::vector<int>& nonBasicOrigVarIndex,
    const std::vector<int>& nonBasicSlackVarIndex, const int splitVarIndex,
    const int splitVarRowIndex);
/**
 * Add each SIC and see if the problem is feasible after adding it.
 * In the subspace, infeasibility may sometimes happen
 * (in the full space, it would prove infeasibility of the problem)
 */
void checkSICFeasibility(AdvCuts& structSIC,
    const PointCutsSolverInterface* const solver, SolutionInfo& solnInfo,
    const bool working_in_subspace);

///**
// * Generates the points and rays used for SIC generation
// */
//void printSICPointInfo(const LiftGICsSolverInterface* const  solver,
//    const SolutionInfo & solnInfo);
//
///**
// * This is a helper function that will allow us to get the points
// * from intersecting the corner relaxation with the split
// */
//void intersectCornerWithSplit(const int splitVar, std::vector<Point> & point,
//    std::vector<Point> & pointFullSpace, std::vector<Ray> & ray,
//    std::vector<Ray> & rayFullSpace, const LiftGICsSolverInterface* const  solver,
//    const SolutionInfo & solnInfo);
//
///**
// * This will return the *distance* to the intersection point obtained from intersecting
// * Ray ray originating at Point origPoint with the boundary of the split splitVar
// * @return True if we get a point; false if this is an extreme ray
// */
//bool intersectRayWithSplit(const int splitVar, double * rayDist,
//    const Point & origPoint, const Ray & rayOut,
//    const LiftGICsSolverInterface* const  solver, const SolutionInfo & solnInfo);
