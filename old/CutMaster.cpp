//============================================================================
// Name        : CutMaster.cpp
// Author      : akazachk
// Version     : 0.2014.07.03
// Copyright   : Your copyright notice
// Description : Functions for cuts (SICs and GICs)
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#include "GlobalConstants.hpp"
#include "Utility.hpp"
#include "SIC.hpp"
#include "Output.hpp"
#include "CutHelper.hpp"
#include "CutMaster.hpp"
#include "../cuts/CglPHA/PHA.hpp"

void cutMaster(const PointCutsSolverInterface* const solverOrig,
    const SolutionInfo& solnInfoOrig) {
  //LiftGICsSolverInterface* solverMain = new LiftGICsSolverInterface(*solverOrig);
  PointCutsSolverInterface* solverMain = dynamic_cast<PointCutsSolverInterface*>(solverOrig->clone());
  setClpParameters(solverMain);
  copySolverBasis(solverMain, solverOrig);

  // deletedCols stores indices of columns deleted
  // oldRowIndices stores original indices of rows
  //  (they may have changed due to removed rows)
  // oldColIndices stores original indices of vars
  std::vector<int> deletedCols, oldRowIndices, oldColIndices;

  /***********************************************************************************
   * Generate subproblem if necessary
   ***********************************************************************************/
  if (param.paramVal[ParamIndices::SUBSPACE_PARAM_IND]) {
    GlobalVariables::timeStats.start_timer(GEN_SUB_TIME);
#ifdef TRACE
    printf("\n## Generating subproblem. ##\n");
#endif

//    genSubprobMaster(solverMain, solverOrig, solnInfoOrig, deletedCols,
//        oldRowIndices, oldColIndices);
    error_msg(errorstring, "Cannot work in subspace yet.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);

    GlobalVariables::timeStats.end_timer(GEN_SUB_TIME);
  } else {
    oldRowIndices.resize(solnInfoOrig.numRows);
    oldColIndices.resize(solnInfoOrig.numCols);
    for (int row = 0; row < solnInfoOrig.numRows; row++) {
      oldRowIndices[row] = row;
    }
    for (int col = 0; col < solnInfoOrig.numCols; col++) {
      oldColIndices[col] = col;
    }
  }

  SolutionInfo solnInfoMain(solverMain,
      param.paramVal[NUM_CGS_PARAM_IND],
      param.paramVal[ParamIndices::SUBSPACE_PARAM_IND], false); // We'll be using the ray info, so we should not use the brief SolutionInfo

#ifdef SHOULD_WRITE_BRIEF_SOLN
  solnInfoMain.printBriefSolutionInfo(solverMain,
      GlobalVariables::out_f_name_stub + "Main");
#endif

  if (solnInfoOrig.numSplits == 0 || solnInfoMain.numSplits == 0) {
    error_msg(errorstring,
        "numsplitsOrig: %d. solnInfoMain.numSplits: %d. Neither of these should be zero.\n",
        solnInfoOrig.numSplits, solnInfoMain.numSplits);
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  if (param.paramVal[ParamIndices::SUBSPACE_PARAM_IND]) {
#ifdef TRACE
    printf("Num0: %d. Num1: %d. Num2: %d.\n", solnInfoMain.num0feas,
        solnInfoMain.num1feas, solnInfoMain.num2feas);
    printf("Num floor infeas: %d. Num ceil infeas: %d.\n",
        solnInfoMain.numFloorInfeas, solnInfoMain.numCeilInfeas);
#endif

    // If all the splits have zero feasible sides, then there is no point of continuing on
    if (solnInfoMain.numSplits == solnInfoMain.num0feas) {
      error_msg(errorstring,
          "All possible splits are empty on both sides.\n");
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }
  }

  /***********************************************************************************
   * Finalize parameters
   ***********************************************************************************/
#ifdef TRACE
  printf("\n## Finalizing parameters. ##\n");
#endif

  // Ensure parameters given to the problem are feasible
  if (param.paramVal[NUM_RAYS_CUT_PARAM_IND] == 0) {
    param.paramVal[NUM_RAYS_CUT_PARAM_IND] = solnInfoMain.numNB;
  }

  if (param.paramVal[NUM_RAYS_CUT_PARAM_IND] > solnInfoMain.numNB) {
    warning_msg(warnstring,
        "The maximum number of rays that can be cut is %d. The value specified of %d is too high. Changing to the maximum number of rays.\n",
        solnInfoMain.numNB,
        param.paramVal[NUM_RAYS_CUT_PARAM_IND]);
    param.paramVal[NUM_RAYS_CUT_PARAM_IND] = solnInfoMain.numNB;
  }

  // If number of rays to be cut is STILL 0, there's a problem.
  // Note that there might be some strange things happening if
  // a variable is restricted to be integer, but it has a
  // bound that is fractional. We should clean the problem maybe
  // to ensure that we don't have non-basic integer-restricted
  // variables that are fractional.
  if (param.paramVal[NUM_RAYS_CUT_PARAM_IND] == 0) {
    error_msg(errorstring,
        "The number of non-basic columns is %d. There are no rays to cut.\n",
        solnInfoMain.numNB);
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  if (param.paramVal[NUM_CGS_PARAM_IND] == 0) {
    param.paramVal[NUM_CGS_PARAM_IND] =
        solnInfoMain.numSplits;
  }

  if (param.paramVal[NUM_CGS_PARAM_IND]
      > solnInfoMain.numSplits) {
    warning_msg(warnstring,
        "Number of requested cut generating sets is %d, but the maximum number we can choose is %d. Reducing the requested number of splits.\n",
        param.paramVal[NUM_CGS_PARAM_IND],
        solnInfoMain.numSplits);
    param.paramVal[NUM_CGS_PARAM_IND] =
        solnInfoMain.numSplits;
  }

  /***********************************************************************************
   * Save objective
   ***********************************************************************************/
  // In order to calculate how good the points we find are
  // with respect to the objective function (which should be in NB space when appropriate)
  // Clearly a valid inequality is c^T x \ge c^\T v,
  // where v is the LP optimum, since we are minimizing
  AdvCut NBObj(false), structObj(false);
  structObj.setOsiRowCut(solverMain->getNumCols(),
      solverMain->getObjCoefficients(), solverMain->getObjValue());

  // Either we have the objective, or we have the reduced costs
  // of the non-basic variables (which is exactly the objective
  // in the non-basic space).
  // Recall that the reduced cost of slack variables is exactly
  // the negative of the row price (dual variable) for that row.
//  if (!inNBSpace) {
//    objCut.assign(structObjCut);
//  } else {
  NBObj.NBSpace = true;
  NBObj.num_coeff = solnInfoMain.numNB;

  // TODO Replace this with simply taking solnInfoMain.nonBasicReducedCost (when that issue is fixed)
  std::vector<double> coeff(solnInfoMain.numNB);
  for (int var = 0; var < solnInfoMain.numNBOrig; var++) {
    int curr_var = solnInfoMain.nonBasicOrigVarIndex[var];
    coeff[var] = solverMain->getReducedCost()[curr_var];
    if (isNonBasicUBVar(solverMain, curr_var))
      coeff[var] *= -1.0;
  }

  for (int var = 0; var < solnInfoMain.numNBSlack; var++) {
    int curr_var = solnInfoMain.nonBasicSlackVarIndex[var];
    coeff[var + solnInfoMain.numNBOrig] = -1.0
        * solverMain->getRowPrice()[curr_var - solnInfoMain.numCols];
    if (isNonBasicUBVar(solverMain, curr_var))
      coeff[var + solnInfoMain.numNBOrig] *= -1.0;
  }
  // The rhs of the objective in the NB space will always be 0,
  // since the optimal point is the origin.
  NBObj.setOsiRowCut(coeff, 0.0);
//  }

  /***********************************************************************************
   * Generate SICs
   ***********************************************************************************/
  AdvCuts structMSICs(false), NBMSICs(true);
  PointCutsSolverInterface* MSICSolver = dynamic_cast<PointCutsSolverInterface*>(solverMain->clone());
  setClpParameters(MSICSolver);

  const double postMSICObj = SICMaster(solverMain, solnInfoMain, structMSICs,
      NBMSICs, MSICSolver);
  // Ensure MSICSolver is optimal
  if ((MSICSolver == NULL) || !MSICSolver->isProvenOptimal()) {
    error_msg(errorstring,
        "MSIC solver is null or not optimal. Unless we are in the subspace, this should not happen.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  /***********************************************************************************
   * Generate GICs
   ***********************************************************************************/
  AdvCuts structMGICs(false);
//  AdvCuts NBMGICs(true);
//  structMGICs.setCuts(structMSICs);
//  NBMGICs.setCuts(NBMSICs);
  int num_obj_tried = 0;
  const double postMGICObj = GICMaster(num_obj_tried, solverMain, solnInfoMain,
      oldRowIndices, structMSICs, NBMSICs, MSICSolver, structMGICs,
      NBObj);

  /***********************************************************************************
   *  Print optimal objective values of all solvers after cuts added
   ***********************************************************************************/
  compareCuts(num_obj_tried, solverMain, postMSICObj, postMGICObj, structMSICs, structMGICs);

  /***********************************************************************************
   * Clear things
   ***********************************************************************************/
#ifdef TRACE
  printf("\n## Finished with cut master. ##\n");
#endif
  if (solverMain) {
    delete solverMain;
  }
  if (MSICSolver) {
    delete MSICSolver;
  }
}

double SICMaster(const PointCutsSolverInterface* const solverMain,
    SolutionInfo& solnInfoMain, AdvCuts& structMSICs, AdvCuts& NBMSICs,
    PointCutsSolverInterface* & MSICSolver) {
#ifdef TRACE
  printf("\n## Generating SICs for the main problem (subproblem if working in subspace). ##\n");
#endif

  GlobalVariables::timeStats.start_timer(GEN_MSICS_TIME);
  // Only feasible ones are returned
  genSICs(structMSICs, NBMSICs, solverMain, solnInfoMain,
      param.paramVal[ParamIndices::SUBSPACE_PARAM_IND]);
  const int numMSICs = structMSICs.sizeCuts();

#ifdef TRACE
  printf("***** MSICs generated: %d.\n", numMSICs);
  printf("\n## Printing MSICs in struct space. ##\n");
  structMSICs.printCuts();
#endif

  GlobalVariables::timeStats.end_timer(GEN_MSICS_TIME);

#ifdef TRACE
  printf("\n## Writing MSICs to file. ##\n");
#endif

#ifdef SHOULD_WRITE_SICS
  if (param.paramVal[ParamIndices::SUBSPACE_PARAM_IND])
  structMSICs.printCuts("SSICs", solverMain, solnInfoMain);
  else
  structMSICs.printCuts("OSICs", solverMain, solnInfoMain);
#endif

  // Check parameters
  if (numMSICs == 0) {
    error_msg(errorstring, "Number of subspace splits is 0.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  if (solnInfoMain.numFeasSplits == 0) {
    error_msg(errorstring, "Number of *feasible* subspace splits is 0.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  if (!param.paramVal[ParamIndices::SUBSPACE_PARAM_IND]
      && param.paramVal[NUM_CGS_PARAM_IND]
          != solnInfoMain.numFeasSplits) {
    error_msg(errorstring,
        "Number of *feasible* subspace splits is not equal to the number of splits, and this is not in subspace, so should not happen unless problem is infeasible.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  if (param.paramVal[NUM_CGS_PARAM_IND]
      > solnInfoMain.numFeasSplits) {
    warning_msg(warnstring,
        "Number of requested cut generating sets is %d, but the maximum number we can choose is %d. Reducing the requested number of splits.\n",
        param.paramVal[NUM_CGS_PARAM_IND],
        solnInfoMain.numFeasSplits);
    param.paramVal[NUM_CGS_PARAM_IND] =
        solnInfoMain.numFeasSplits;
  }

  // We moved the SolnInfo and SubSolnInfo here because params are only set now.
  writeParamInfoToLog(GlobalVariables::log_file);
  writeSolnInfoToLog(GlobalVariables::prob_name, solnInfoMain, -1.0,
      GlobalVariables::log_file); // TODO Fix valgrind error here (due to inst_info_out)
  if (param.paramVal[ParamIndices::SUBSPACE_PARAM_IND]) {
    writeSubSolnInfoToLog(GlobalVariables::prob_name, &solnInfoMain, -1.0,
        GlobalVariables::log_file);
  }

  // Count intersecting rays for the chosen splits
  solnInfoMain.countIntersectingRays(solnInfoMain.feasSplitVar);

#ifdef SHOULD_WRITE_SPLIT_FEAS_INFO
  printSplitInfo(solverMain, solnInfoMain);
#endif

  /***********************************************************************************
   * Apply MSICs
   ***********************************************************************************/
  OsiSolverInterface::ApplyCutsReturnCode code;
  if (numMSICs > 0) {
    GlobalVariables::timeStats.start_timer(APPLY_MSICS_TIME);
#ifdef TRACE
    printf("\n## Applying MSICs. ##\n");
#endif
    code = MSICSolver->applyCuts(structMSICs);

#ifdef TRACE
    printf("MSICs applied: %d (of the %d possible).\n",
        code.getNumApplied(), numMSICs);
#endif

    MSICSolver->initialSolve();

    // At this point it is useful to check for integer feasibility quickly
    // If we are in the subspace, then this is not necessary
    if (!param.paramVal[ParamIndices::SUBSPACE_PARAM_IND]) {
      if (!MSICSolver->isProvenOptimal()) {
        error_msg(errstr,
            "Not in subspace and solver not optimal after adding MSICs, so we have an integer infeasible problem.\n");
        writeErrorToLog(errstr, GlobalVariables::log_file);
        exit(1);
      }
    }

    GlobalVariables::timeStats.end_timer(APPLY_MSICS_TIME);

    if (MSICSolver->isProvenOptimal()) {
      return MSICSolver->getObjValue();
    }
  } else {
    // There are no SICs!
    error_msg(errstr, "There are no MSICs!\n");
    writeErrorToLog(errstr, GlobalVariables::log_file);
    exit(1);
  }

  return solverMain->getInfinity();
}

double GICMaster(int& num_obj_tried, const PointCutsSolverInterface* const solverMain,
    const SolutionInfo& solnInfoMain, const std::vector<int>& oldRowIndices,
    const AdvCuts& structMSICs, const AdvCuts& NBMSICs,
    const PointCutsSolverInterface* const MSICSolver, AdvCuts& structMGICs,
    const AdvCut& obj) {
#ifdef TRACE
  printf("\n## Setting up chooseSplits. ##\n");
#endif
  // Set up cut-generating sets (this is simply feasSplitVars from solnInfoMain)
  chooseCutGeneratingSets chooseSplits;
  chooseSplits.chooseSplits(solverMain, solnInfoMain, oldRowIndices);
//      LiftGICsParam::paramVal[NUM_CUT_GEN_SETS_PARAM_IND],
  //, LiftGICsParam::inst_info_out);

  printf(
      "\n## Instance info:\n== Splits chosen on the following variables:\n");
  for (int s = 0; s < chooseSplits.nEffCutGenSets; s++)
    printf(
        ":::: Var name: %s. Var value: %2.3f. Var index: %d. Row index for var: %d.\n",
        chooseSplits.splitVarName[s].c_str(),
        chooseSplits.splitVarValue[s], chooseSplits.splitVarIndex[s],
        chooseSplits.splitVarRowIndex[s]);
  printf("====\n");

  printf("== Number rays to be cut set to %d. ==\n",
      param.paramVal[NUM_RAYS_CUT_PARAM_IND]);
  printf("== Number effective cut-generating sets is %d. ==\n",
      chooseSplits.nEffCutGenSets);
  printf("== Number rounds is %d. ==\n",
      param.paramVal[NUM_CUTS_PER_CGS_PARAM_IND]);

  // Start PHA procedure
  printf("\n## Starting PHA procedure. ##\n");

  // These will be the vertices generated along the way
  std::vector<Vertex> vertexStore;
  std::vector<Ray> rayStore;
  // These will be the activated hyperplanes
  std::vector<Hplane> hplaneStore;

  // Set up Vertex and Ray stores
  vertexStore.resize(1); // Initially just the origin
  vertexStore[0].rayIndices.resize(solnInfoMain.numNB);
  vertexStore[0].rayActiveFlag.resize(solnInfoMain.numNB, true);
  rayStore.resize(solnInfoMain.numNB); // Initially the rays of C1
  for (int i = 0; i < solnInfoMain.numNB; i++) {
    const int rayIndices[1] = { i };
    const double rayValues[1] = { 1.0 };
    rayStore[i].setVector(1, rayIndices, rayValues);
//    rayStore[i].partitionOfPtsAndRaysByRaysOfC1.resize(numSplits);
    rayStore[i].cutFlag.resize(chooseSplits.nEffCutGenSets, false);
//    rayStore[i].addedPointFlag.resize(chooseSplits.nEffCutGenSets, false);
//    rayStore[i].firstHplaneOverall = -1;
//    rayStore[i].firstVertexOverall = -1;

    // This ray emanates from the original vertex
    vertexStore[0].rayIndices[i] = i;
  }

  // The constraint matrices for each split
  std::vector<int> num_SIC_points(chooseSplits.nEffCutGenSets, 0),
      num_SIC_final_points(chooseSplits.nEffCutGenSets, 0), num_SIC_rays(
          chooseSplits.nEffCutGenSets, 0);
  std::vector<IntersectionInfo> interPtsAndRays(chooseSplits.nEffCutGenSets);
  for (int s = 0; s < chooseSplits.nEffCutGenSets; s++) {
    // Reverse ordering so that the packed matrix is row ordered
    // and set number of columns
    interPtsAndRays[s].reverseOrdering();
    interPtsAndRays[s].setDimensions(0, solnInfoMain.numNB);

    // This adds all the original intersection points
    addInterPtsFromUncutRaysOfC1(interPtsAndRays[s], solverMain,
        solnInfoMain, chooseSplits, s, rayStore, vertexStore);

    // Gather information about number of final initial points
    const int numIntPointsOrRays = interPtsAndRays[s].getNumRows(); // Should be == numNB
    for (int row_ind = 0; row_ind < numIntPointsOrRays; row_ind++) {
      // If it's a point get it, o/w count the ray
      if (isZero(interPtsAndRays[s].RHS[row_ind])) {
        num_SIC_rays[s]++;
      } else {
        num_SIC_points[s]++;
        if (pointInP(solverMain, solnInfoMain, interPtsAndRays[s], s,
            row_ind)) {
          num_SIC_final_points[s]++;
        }
      }
    }
  }

  if (param.paramVal[NUM_EXTRA_ACT_ROUNDS_PARAM_IND]) {
    PHAMaster(param.paramVal[NUM_RAYS_CUT_PARAM_IND],
        interPtsAndRays, solverMain, solnInfoMain, chooseSplits,
        NBMSICs, vertexStore, rayStore, hplaneStore);
  }

  // Analyze resulting point and ray collection
#ifdef TRACE
  printf("\n## Analyzing final point collection. ##\n");
#endif
  std::vector<int> num_rays_parallel_to_split;
  std::vector<double> minSICDepth, maxSICDepth, avgSICDepth;
  std::vector<double> minObjDepth, maxObjDepth, avgObjDepth;
  analyzePointCollection("MGICs", solverMain, solnInfoMain, chooseSplits,
      hplaneStore, NBMSICs, obj, interPtsAndRays,
      num_rays_parallel_to_split, minSICDepth, maxSICDepth, avgSICDepth,
      minObjDepth, maxObjDepth, avgObjDepth);

#ifdef SHOULD_WRITE_INTPOINTS
  printf("\n## Save information about the points and rays. ##\n");
  char filename[300];
  snprintf(filename, sizeof(filename) / sizeof(char),
      "%s-PointsAndRays.csv", GlobalVariables::out_f_name_stub.c_str());
  FILE *ptsRaysOut = fopen(filename, "w");
  printInterPtsAndRaysInJSpace(ptsRaysOut, solnInfoMain, chooseSplits,
      interPtsAndRays, hplaneStore, num_SIC_points,
      num_SIC_final_points, num_SIC_rays);
  fclose(ptsRaysOut);
#endif

  // Write GIC point info to II
  int totalNumPoints = 0, totalNumFinalPoints = 0, totalNumRays = 0;
  std::vector<int> num_points_per_split(chooseSplits.nEffCutGenSets);
  std::vector<int> num_final_points_per_split(chooseSplits.nEffCutGenSets);
  std::vector<int> num_rays_per_split(chooseSplits.nEffCutGenSets);
  for (int s = 0; s < chooseSplits.nEffCutGenSets; s++) {
    num_points_per_split[s] = interPtsAndRays[s].numPoints;
    num_final_points_per_split[s] = interPtsAndRays[s].numFinalPoints;
    num_rays_per_split[s] = interPtsAndRays[s].numRays;
    totalNumPoints += num_points_per_split[s];
    totalNumFinalPoints += num_final_points_per_split[s];
    totalNumRays += num_rays_per_split[s];
  }

  const double avg_num_SIC_final_points = computeAverage(
      num_SIC_final_points);
  const double avg_num_par_rays = computeAverage(num_rays_parallel_to_split);
  writeGICPointInfoToLog(param.paramVal[PHA_ACT_OPTION_PARAM_IND],
      param.paramVal[NEXT_HPLANE_FINAL_PARAM_IND],
      param.paramVal[NUM_EXTRA_ACT_ROUNDS_PARAM_IND],
      solnInfoMain.numNB, avg_num_par_rays, avg_num_SIC_final_points,
      param.paramVal[NUM_RAYS_CUT_PARAM_IND],
      param.paramVal[NUM_CGS_PARAM_IND],
      param.paramVal[NUM_CUTS_PER_CGS_PARAM_IND],
      num_points_per_split, num_final_points_per_split,
      num_rays_per_split, totalNumPoints, totalNumFinalPoints,
      totalNumRays, avgSICDepth, minSICDepth, maxSICDepth, avgObjDepth,
      minObjDepth, maxObjDepth, GlobalVariables::log_file);

  /***********************************************************************************
   * Generate GICs
   ***********************************************************************************/
  if (NO_GEN_CUTS) {
    /***********************************************************************************
     * Wrap up
     ***********************************************************************************/
    return solverMain->getInfinity();
  }

  printf("\n## Generating GICs ##\n");
  GlobalVariables::timeStats.start_timer(GEN_MPHA_TIME);
  std::vector<int> num_gics_per_split(chooseSplits.nEffCutGenSets, 0);
  int num_total_gics = 0;

  // Set up solvers with points and rays found
  std::vector<PointCutsSolverInterface*> cutSolver(chooseSplits.nEffCutGenSets);
  // This is in the * complemented * non-basic space,
  // so the origin is the solution to the LP relaxation
  // Moreover, every cut will have right-hand side 1,
  // since we want to separate the origin
  const double beta = 1.0;
  for (int s = 0; s < chooseSplits.nEffCutGenSets; s++) {
                cutSolver[s] = new PointCutsSolverInterface;
    setClpParameters(cutSolver[s]);
//    cutSolver[s]->getModelPtr()->setMaximumSeconds(CUT_SOLVER_MAX_SECONDS);
    cutSolver[s]->getModelPtr()->setMaximumIterations(
        param.getCUTSOLVER_MAX_ITER());
    cutSolver[s]->setHintParam(OsiDoPresolveInInitial,
        param.paramVal[CUT_PRESOLVE_PARAM_IND]);
    cutSolver[s]->setHintParam(OsiDoPresolveInResolve,
        param.paramVal[CUT_PRESOLVE_PARAM_IND]);

    setupCutSolverNB(cutSolver[s],
        (int) solnInfoMain.nonBasicVarIndex.size(), interPtsAndRays[s],
        beta);
  }

  if (param.paramVal[USE_SPLIT_SHARE_PARAM_IND] == 1) {
    // Try split sharing idea
    printf("\n## Try split sharing idea. ##\n");
    splitSharingCutGeneration(cutSolver, structMGICs, num_gics_per_split,
        num_total_gics, num_obj_tried, interPtsAndRays, solverMain, solnInfoMain,
        chooseSplits, rayStore, vertexStore, hplaneStore, structMSICs);
  }

//  LiftGICsSolverInterface tmpMGICSolver(solverMain);
//  setMessageHandler(tmpMGICSolver);
//  OsiSolverInterface::ApplyCutsReturnCode tmpcode;
//  AdvCuts tmpGICs;
  for (int s = 0; s < chooseSplits.nEffCutGenSets; s++) {
#ifdef TRACE
    const int tmp_num_gics = genCutsFromPointsRaysNB(cutSolver[s],
        structMGICs, num_gics_per_split[s], num_total_gics, num_obj_tried,
        solverMain, solnInfoMain, interPtsAndRays[s], vertexStore,
        chooseSplits.splitVarIndex[s], s, structMSICs, NBMSICs,
        MSICSolver);
    printf("\n## Number cuts generated for split %d: %d. ##\n",
        chooseSplits.splitVarIndex[s], tmp_num_gics);
#else
    genCutsFromPointsRaysNB(cutSolver[s], structMGICs,
        num_gics_per_split[s], num_total_gics, num_obj_tried, solverMain, solnInfoMain,
        interPtsAndRays[s], vertexStore, chooseSplits.splitVarIndex[s],
        s, structMSICs, NBMSICs, MSICSolver);
#endif
    cutSolver[s]->setHintParam(OsiDoPresolveInInitial,
        param.paramVal[CUT_PRESOLVE_PARAM_IND]);
    cutSolver[s]->setHintParam(OsiDoPresolveInResolve,
        param.paramVal[CUT_PRESOLVE_PARAM_IND]);
//    {
//    if (structMGICs.size() > 0) {
//      tmpGICs = structMGICs;
//      tmpGICs.setOsiCuts();
//      tmpMGICSolver = solverMain;
//      LiftGICsParam::timeStats.start_timer(APPLY_MGICS_TIME);
//#ifdef TRACE
//      printf("\n## Applying MGICs. ##\n");
//#endif
//
//      tmpcode = tmpMGICSolver->applyCuts(tmpGICs);
//
//#ifdef TRACE
//      printf("MGICs applied: %d (of the %d possible).\n",
//          tmpcode.getNumApplied(), structMGICs.size());
//#endif
//
//      tmpMGICSolver->initialSolve();
//
//      // At this point it is useful to check for integer feasibility quickly
//      // If we are in the subspace, then this is not necessary
//      if (!LiftGICsParam::paramVal[ParamIndices::SUBSPACE_PARAM_IND]) {
//        if (!tmpMGICSolver->isProvenOptimal()) {
//          error_msg(errstr,
//              "Not in subspace and solver not optimal after adding MGICs, so we have an integer infeasible problem.\n");
//          writeErrorToII(errstr, LiftGICsParam::inst_info_out);
//          exit(1);
//        }
//      }
//
//      LiftGICsParam::timeStats.end_timer(APPLY_MGICS_TIME);
//    }
//    }
  }

  if (param.paramVal[ParamIndices::USE_SPLIT_SHARE_PARAM_IND] == 2) {
//    // Set up solvers with points and rays found
//    std::vector<LiftGICsSolverInterface*> cutSolverTmp(
//        chooseSplits.nEffCutGenSets);
//    // This is in the * complemented * non-basic space,
//    // so the origin is the solution to the LP relaxation
//    // Moreover, every cut will have right-hand side 1,
//    // since we want to separate the origin
//    const double beta = 1.0;
//    for (int s = 0; s < chooseSplits.nEffCutGenSets; s++) {
//            cutSolverTmp[s] = new LiftGICsSolverInterface;
//      setMessageHandler(cutSolverTmp[s]);
//      //    cutSolver[s]->getModelPtr()->setMaximumSeconds(CUT_SOLVER_MAX_SECONDS);
//      cutSolverTmp[s].getModelPtr()->setMaximumIterations(
//          param.getCUT_SOLVER_MAX_ITER());
//      cutSolverTmp[s].setHintParam(OsiDoPresolveInInitial, true);
//      cutSolverTmp[s].setHintParam(OsiDoPresolveInResolve, true);
//
//      setupCutSolverNB(cutSolverTmp[s],
//          (int) solnInfoMain.nonBasicVarIndex.size(),
//          interPtsAndRays[s], beta);
//    }
//    // Try split sharing idea
//    printf("\n## Try split sharing idea. ##\n");
//    splitSharingCutGeneration(cutSolverTmp, structMGICs, num_gics_per_split,
//        num_total_gics, interPtsAndRays, solverMain, solnInfoMain,
//        chooseSplits, rayStore, vertexStore, hplaneStore, structMSICs);

    // Try split sharing idea
    printf("\n## Try split sharing idea. ##\n");
    splitSharingCutGeneration(cutSolver, structMGICs, num_gics_per_split,
        num_total_gics, num_obj_tried, interPtsAndRays, solverMain, solnInfoMain,
        chooseSplits, rayStore, vertexStore, hplaneStore, structMSICs);
  }
  structMGICs.setOsiCuts();

  for (int s = 0; s < chooseSplits.nEffCutGenSets; s++) {
                delete cutSolver[s];
        }
  GlobalVariables::timeStats.end_timer(GEN_MPHA_TIME);

  const int numMGICs = structMGICs.sizeCuts();

  /***********************************************************************************
   * Lift GICs (if necessary)
   ***********************************************************************************/

  /***********************************************************************************
   * Apply MGICs
   ***********************************************************************************/
#ifdef TRACE
  printf("\n## Writing MGICs to file. ##\n");
#endif

#ifdef SHOULD_WRITE_GICS
  if (param.paramVal[ParamIndices::SUBSPACE_PARAM_IND]) {
    structMGICs.printCuts("SGICs", solverMain, solnInfoMain);
  } else {
    structMGICs.printCuts("GICs", solverMain, solnInfoMain);
  }
#endif

  OsiSolverInterface::ApplyCutsReturnCode code;
  PointCutsSolverInterface* MGICSolver;
  if (numMGICs > 0) {
    GlobalVariables::timeStats.start_timer(APPLY_MPHA_TIME);
#ifdef TRACE
    printf("\n## Applying MGICs. ##\n");
#endif
    //MGICSolver = new LiftGICsSolverInterface(*solverMain);
    MGICSolver = dynamic_cast<PointCutsSolverInterface*>(solverMain->clone());
    setClpParameters(MGICSolver);

    code = MGICSolver->applyCuts(structMGICs);

#ifdef TRACE
    printf("MGICs applied: %d (of the %d possible).\n",
        code.getNumApplied(), numMGICs);
#endif

    MGICSolver->initialSolve();

    // At this point it is useful to check for integer feasibility quickly
    // If we are in the subspace, then this is not necessary
    if (!param.paramVal[ParamIndices::SUBSPACE_PARAM_IND]) {
      if (!MGICSolver->isProvenOptimal()) {
        error_msg(errstr,
            "Not in subspace and solver not optimal after adding MGICs, so we have an integer infeasible problem.\n");
        if (param.getEXIT_ON_INFEAS()) {
          writeErrorToLog(errstr, GlobalVariables::log_file);
          exit(1);
        }
      }
    }

    GlobalVariables::timeStats.end_timer(APPLY_MPHA_TIME);
  }
//  else {
//    // There are no GICs!
//    error_msg(errstr, "There are no MGICs!\n");
//    writeErrorToII(errstr, LiftGICsParam::inst_info_out);
//    exit(1);
//  }

  if (numMGICs > 0 && (MGICSolver != NULL) && MGICSolver->isProvenOptimal()) {
    const double mgic_opt = MGICSolver->getObjValue();
                delete MGICSolver;
                return mgic_opt; 
  } else {
    return solverMain->getInfinity();
  }
}
