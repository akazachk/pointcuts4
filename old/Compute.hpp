//============================================================================
// Name        : Compute.hpp
// Author      : akazachk
// Version     : 0.2013.mm.dd
// Copyright   : Your copyright notice
// Description : 
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#pragma once

#include <vector>
#include <cmath>
#include <algorithm> // For max_element, min_element
#include <string>
#include <cstdio>

#include "typedefs.hpp"
//#include "OsiCpxSolverInterface.hpp"

#include "GlobalConstants.hpp"
#include "Output.hpp"
#include "SolutionInfo.hpp"
#include "chooseCutGeneratingSets.hpp"
#include "Utility.hpp"

/********************************************************************************/
/**
 *
 */
void computeRayStats(int &numDiffCoeff, double &totaldiff, double &abstotaldiff,
    vector<double> &distAlongRayLifted, vector<double> &distAlongRayOrig);

/********************************************************************************/
/**
 * Count number of sides of the possible splits that are empty
 */
void countEmptySplits(PointCutsSolverInterface* solver, SolutionInfo &solnInfo,
    std::vector<int> &splitVar, std::string out_f_name_stub);

/********************************************************************************/
/**
 * @brief
 *
 * @param indicesToAdd                ::  Variable index to add (original space)
 * @param indicesToAddFromDeletedCols ::  Index within deletedCols
 */
void fixFloor(std::vector<int> &indicesToAdd,
    std::vector<int> &indicesToAddInDeletedCols,
    PointCutsSolverInterface* solver, SolutionInfo &solnInfoOrig,
    int &splitVarIndex, int numToAdd, std::vector<int> &deletedCols,
    std::vector<int> &oldRowIndices, std::vector<int> &oldColIndices,
    PointCutsSolverInterface* subprob, SolutionInfo &solnInfoSub);

void fixCeil(std::vector<int> &indicesToAdd,
    std::vector<int> &indicesToAddInDeletedCols,
    PointCutsSolverInterface* solver, SolutionInfo &solnInfoOrig,
    int &splitVarIndex, int numToAdd, std::vector<int> &deletedCols,
    std::vector<int> &oldRowIndices, std::vector<int> &oldColIndices,
    PointCutsSolverInterface* subprob, SolutionInfo &solnInfoSub);
