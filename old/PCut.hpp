//============================================================================
// Name        : PCut.hpp
// Author      : Aleksandr M. Kazachkov
// Version     : 0.2014.03.18
// Copyright   : Your copyright notice
// Description : Generates point cuts
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#pragma once

#include "GlobalConstants.hpp"
#include "SolutionInfo.hpp"
#include "Utility.hpp"
#include "Output.hpp"
#include "CutHelper.hpp"
//#include "LrsHelper.hpp"
#include "AdvCut.hpp"
#include "Point.hpp"
#include "Ray.hpp"

#include "typedefs.hpp"

#include <cmath> // Remember to put std::abs
#include <algorithm> // For max_element, min_element, sort

/**
 * Generates point cuts based on option provided.
 *   corner - generates only rays corresponding to non-basic vars
 *   cornerNB - does corner in NB space
 *   degencorner - generates all rays from the starting vertex
 *
 *   @return number cuts generated
 */
int genPCuts(const std::string option, AdvCuts & structPCuts,
    const PointCutsSolverInterface* const  solver, const SolutionInfo & probData,
    const AdvCuts & structSICs, const AdvCuts & NBSICs,
    const bool working_in_subspace);

void genPCutFloor(const std::string option, std::vector<Point>& point,
    std::vector<Point>& pointFullSpace, std::vector<Ray>& ray,
    std::vector<Ray>& rayFullSpace, const PointCutsSolverInterface* const  tmpSolver,
    const PointCutsSolverInterface* const  origSolver,
    const SolutionInfo & origProbData, const int splitVar);

void genPCutCeil(const std::string option, std::vector<Point>& point,
    std::vector<Point>& pointFullSpace, std::vector<Ray>& ray,
    std::vector<Ray>& rayFullSpace, const PointCutsSolverInterface* const  tmpSolver,
    const PointCutsSolverInterface* const  origSolver,
    const SolutionInfo & origProbData, const int splitVar);

void genCorner(std::vector<Point>& point, std::vector<Ray>& ray,
    const PointCutsSolverInterface* const solver);

/**
 * IN NON-BASIC SPACE
 * Get Point and Rays from corner polyhedron defined by current optimum at solver
 * Assumed to be optimal already
 */
void genCornerNB(std::vector<Point>& point, std::vector<Point>& pointFullSpace,
    std::vector<Ray>& ray, std::vector<Ray>& rayFullSpace,
    const PointCutsSolverInterface* const  tmpSolver,
    const PointCutsSolverInterface* const  origSolver,
    const SolutionInfo & origProbData);

void genDegenCorner(std::vector<Point>& point, std::vector<Ray>& ray,
    const PointCutsSolverInterface* const solver, const SolutionInfo& probData);

void getExtremeRays();

void getCornerRays();
