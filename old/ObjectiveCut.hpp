//============================================================================
// Name        : ObjectiveCut.hpp
// Author      : Aleksandr M. Kazachkov
// Version     : 0.2014.03.10
// Copyright   : Your copyright notice
// Description : Generates simple objective cuts
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#pragma once

#include "GlobalConstants.hpp"
#include "SolutionInfo.hpp"
#include "CutHelper.hpp"

#include "typedefs.hpp"
#include "OsiRowCut.hpp"
#include "OsiCuts.hpp"
#include <cmath> // Remember to put std::abs

//OsiCuts genAllObjCuts(const LiftGICsSolverInterface* const solver,
//    const SolutionInfo& probData);
void genAllObjCuts(OsiCuts& cuts, const PointCutsSolverInterface* const solver,
    const SolutionInfo& probData, const bool working_in_subspace);

//OsiRowCut genObjCut(const LiftGICsSolverInterface* const solver,
//    const SolutionInfo& probData, int col);
bool genObjCut(OsiRowCut& cut, const PointCutsSolverInterface* const solver,
    const SolutionInfo& probData, int currFracVar, const bool working_in_subspace);
