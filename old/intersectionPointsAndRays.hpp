/*
 * intersectionPointsAndRays.hpp
 *
 *  Created on: Jun 18, 2012
 *      Author: selva
 */

#ifndef INTERSECTIONPOINTSANDRAYS_HPP_
#define INTERSECTIONPOINTSANDRAYS_HPP_
#include "OsiSolverInterface.hpp"
#include "typedefs.hpp"
#include <vector>
#include "GlobalConstants.hpp"
#include "chooseCutGeneratingSets.hpp"
#include "Structs.hpp"
#include "SolutionInfo.hpp"
#include "Utility.hpp"

using namespace std;

void addRankOneInterPtsAndRays(const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo,
    const chooseCutGeneratingSets& chooseSplits,
    const vector<vector<vector<int> > >& raysToBeCutByHplane,
    const vector<vector<vector<int> > >& allRaysCutByHplane,
    const vector<vector<int> >& hplanesIndxToBeActivated,
    const vector<vector<int> >& hplanesRowIndxToBeActivated,
    const vector<vector<int> >& hplanesToBeActivatedUBFlag,
    CoinPackedMatrix**& interPtsAndRays,
    std::vector<std::vector<int> >& pointOrRayFlag,
    std::vector<int>& numPointsOrRays,
    std::vector<std::vector<ptOrRayInfo*> >& intPtOrRayInfo,
    std::vector<std::vector<int> >& raysToBeCut,
    std::vector<std::vector<std::vector<int> > >& partitionOfPtsAndRaysByRaysOfC1,
    std::vector<cut>& NBSIC, std::vector<int>& totalNumPointsRemoved,
    std::vector<int>& totalNumPointsAdded,
    std::vector<double>& avgRemovedDepth,
    std::vector<double>& avgAddedDepth, FILE* inst_info_out);

void CreateInterPtsAndRays(PointCutsSolverInterface* solver,
    SolutionInfo &solnInfo, chooseCutGeneratingSets &splitCont,
    vector<vector<vector<int> > > &raysToBeCutByHplane,
    vector<vector<vector<int> > >& allRaysCutByHplane,
    vector<vector<int> > &hplanesIndxToBeActivated,
    vector<vector<int> > &hplanesRowIndxToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    CoinPackedMatrix**& interPtsAndRays,
    vector<vector<int> > &pointOrRayFlag, std::vector<int>& numPointsOrRays,
    vector<vector<ptOrRayInfo*> > &intPtOrRayInfo,
    vector<vector<int> > &raysToBeCut,
    vector<vector<vector<int> > > &partitionOfPtsAndRaysByRaysOfC1,
    std::vector<cut>& NBSIC, std::vector<int>& totalNumPointsRemoved,
    std::vector<int>& totalNumPointsAdded,
    std::vector<double>& avgRemovedDepth,
    std::vector<double>& avgAddedDepth, FILE* inst_info_out);

void activateHplane(int& totalNumPointsRemoved, double& avgRemovedDepth,
    int& totalNumPointsAdded, double& avgAddedDepth,
    CoinPackedMatrix*& interPtsAndRays, std::vector<int>& pointOrRayFlag,
    int &numPointsOrRays, std::vector<ptOrRayInfo*>& intPtOrRayInfo,
    const chooseCutGeneratingSets& chooseSplits, const int splitIndex,
    const int hplaneIndexInSplit, const int hplaneVarIndex,
    const int hplaneVarRowIndex, const int hplaneUBFlag,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    const std::vector<int>& raysToBeCutByHplane,
    std::vector<std::vector<int> >& partitionOfPtsAndRaysByRaysOfC1,
    const std::vector<int>& allRaysCutByHplane, const cut& NBSIC,
    const double norm, FILE* inst_info_out, const bool SECOND_ACT = false);

void addInterPtsFromUncutRaysOfC1(const char* rowSense,
    CoinPackedMatrix &interPtsAndRays,
    const vector<int> &nonBasicVarIndices, vector<int> &pointOrRayFlag,
    int &numPointsOrRays, vector<ptOrRayInfo*> &intPtOrRayInfo,
    const int splitRowIndex, const int numCols, const int numNonBasicCols,
    const vector<vector<double> > &raysOfC1, const vector<int> &raysToBeCut,
    vector<int> &tempElmt, vector<double> &tempPt, const double splitValue,
    const double pi0, vector<vector<int> > &partitionOfPtsAndRaysByRaysOfC1,
    FILE* inst_info_out);

int computeIntersectionPointsWithSplit(const PointCutsSolverInterface* const solver,
    const int splitRowIndx, const int hplaneIndexInSplit,
    const int hplaneVarIndx, const int hplaneRowIndx, const int hplanUBFlag,
    const double splitValue, const double pi0, const int rayIndx,
    const int rayVarIndx, vector<int> &newRayColIndex,
    vector<vector<int> > &partitionOfPtsAndRaysByRaysOfC1,
    const vector<vector<double> > &raysOfC1, const vector<double> &a0,
    vector<int> &tempElmt, vector<double> &tempPt,
    CoinPackedMatrix &interPtsAndRays, vector<int> &pointOrRayFlag,
    int &numPointsOrRays, vector<ptOrRayInfo*> &intPtOrRayInfo,
    const cut& NBSIC, const double norm, double& sumAddedDepth,
    FILE* inst_info_out);

int removePtsAndRaysCutByHplane(const PointCutsSolverInterface* const solver,
    const SolutionInfo &solnInfo, vector<bool> &raysCutFlag,
    const int hplaneRowIndx, const int hplaneVarIndx,
    const int hplaneUBFlag, vector<int>& delIndices,
    CoinPackedMatrix &interPtsAndRays, vector<int> &pointOrRayFlag,
    int &numPointsOrRays, vector<ptOrRayInfo*> &intPtOrRayInfo,
    const cut& NBSIC, const double norm, double& sumRemovedDepth,
    std::vector<std::vector<int> >& partitionOfPtsAndRaysByRaysOfC1,
    FILE* inst_info_out);

bool duplicateRay(const CoinPackedMatrix& interPtsAndRays,
    const vector<int>& pointOrRayFlag, const int numElmtsToAdd,
    const vector<int>& rayIndexToAdd, const vector<double>& rayValueToAdd,
    FILE* inst_info_out);

void computeVertFromHplaneAct(PointCutsSolverInterface* solver, vector<double> &a0,
    int numSplits, int numCols, double** &vertFromHAct,
    int** &vertFromHActIndices, vector<vector<double> > &raysOfC1,
    int* &numVert, vector<vector<vector<int> > > &raysToBeCutByHplane,
    vector<vector<int> > &hplanesIndxToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    vector<vector<int> > &hplanesRowIndxToBeActivated, FILE* inst_info_out);

void prtVertFromHplaneAct(FILE* fptr, int numSplits, vector<int> &splitVarIndx,
    vector<int> &splitVarRowIndx, double** &vertFromHAct,
    int** &vertFromHActIndices, int* &numVert,
    vector<vector<vector<int> > > &raysToBeCutByHplane,
    vector<int> &nonBasicVarIndices,
    vector<vector<int> > &hplanesRowIndxToBeActivated);

void printInterPtsAndRaysInJSpace(FILE* fptr, SolutionInfo &solnInfo,
    chooseCutGeneratingSets &chooseSplits,
    CoinPackedMatrix**& interPtsAndRays,
    vector<vector<int> > &pointOrRayFlag, std::vector<int>& numPointsOrRays,
    vector<vector<ptOrRayInfo*> > &intPtOrRayInfo);

void storeStrongPtIndices(int numSplits, int nNonBasicCols,
    vector<vector<ptOrRayInfo*> > &intPtOrRayInfo,
    vector<vector<vector<int> > > &strongPtIndices,
    vector<vector<int> > &strongVertHplaneIndex,
    vector<vector<int> > &raysToBeCut, vector<int> &nStrongPts);

#endif /* INTERSECTIONPOINTSANDRAYS_HPP_ */
