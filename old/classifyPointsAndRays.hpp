/*
 * classifyPointsAndRays.hpp
 *
 *  Created on: Jul 3, 2012
 *      Author: selva
 */

#ifndef CLASSIFYPOINTSANDRAYS_HPP_
#define CLASSIFYPOINTSANDRAYS_HPP_
#include "OsiSolverInterface.hpp"
#include "typedefs.hpp"
#include "GlobalConstants.hpp"
#include "Structs.hpp"
#include "chooseCutGeneratingSets.hpp"

#include <vector>

void identifyInterPtObjs(PointCutsSolverInterface* solver, int numSplits,
    int nNonBasicCols, vector<double> &interPtObjVal,
    vector<int> &interPtObjIndex, vector<int> &interPtCutSplit,
    vector<int> &nonBasicVarIndices, vector<vector<double> > &raysOfC1,
    vector<double> &pi0, vector<int> &splitVarIndex,
    vector<int> &splitRowIndx_t, vector<bool> &raysThatMustBeCut);

#endif /* CLASSIFYPOINTSANDRAYS_HPP_ */
