/*
 * hyperplaneActivation.cpp
 *
 *  Created on: Jun 12, 2012
 *      Author: selva
 */

#include "hyperplaneActivation.hpp"

void PHAOneOneBestAvgDepth(const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo,
    const chooseCutGeneratingSets& chooseSplits, const vector<cut> &NBSIC,
    vector<int> &allRaysCut, vector<vector<int> > &raysToBeCut,
    int numRaysToBeCut, vector<vector<vector<int> > > &raysToBeCutByHplane,
    vector<vector<int> > &strongVertHplaneIndex,
    vector<vector<vector<int> > > &allRaysIntersectedByHplane,
    vector<vector<int> > &hplanesIndexToBeActivated,
    vector<vector<int> > &hplanesRowIndexToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    int &num_hplanes_activated, int max_hplanes_activated,
    FILE* inst_info_out) {
  const int numSplits = chooseSplits.nEffCutGenSets;
  const int numRaysOfC1 = solnInfo.raysOfC1.size();

  //Choose rays to be cut based on weakest average cost coefficients among Gomory cuts over the chosen splits
  vector<double> avgCoeffAcrossCuts(numRaysOfC1, 0.0);
  vector<int> sortIndex(numRaysOfC1);
  for (int j = 0; j < numRaysOfC1; j++) {
    sortIndex[j] = j;
    for (int s = 0; s < numSplits; s++) {
      avgCoeffAcrossCuts[j] += NBSIC[s].compNB_coeff[j];
    }
    avgCoeffAcrossCuts[j] /= (double) numSplits;
  }

  //Sort average cut coefficients in descending order. Larger cut coefficients in the J-space correspond to weaker cuts
  sort(sortIndex.begin(), sortIndex.end(),
      index_cmp_dsc<double*>(&avgCoeffAcrossCuts[0]));

  //  solver.enableFactorization();
//  const char* rowSense = solver.getRowSense();

// Resize arrays and put default values
  double minSlackRatio;
  int nonBasicColIndex;
  int hplaneVarIndexToAct, hplaneVarRowIndexToAct, hplaneVarUpperFlagToAct;
  int oldHplane, tmpSplitIndex;
  int tmpSize, tmpSize1;
//  int tmpSize2,tmpSize3;
  // Set of splits that the hyperplane activation affects;
  // i.e., split s is here if the ray intersects the
  // chosen hplane *before* the ray intersects the bd of split s.
  vector<int> splitsEffByHplane(1, -1);
  vector<bool> raysInterHplane(numRaysOfC1);
  // Set of hyperplanes activated. Indices are [split][hplane].
  vector<vector<bool> > chosenHplanes(numSplits);
  vector<int> AllRayIndices(numRaysOfC1);

  hplanesIndexToBeActivated.resize(numSplits);
  hplanesRowIndexToBeActivated.resize(numSplits);
  hplanesToBeActivatedUBFlag.resize(numSplits);
  raysToBeCutByHplane.resize(numSplits);
  strongVertHplaneIndex.resize(numSplits);
  allRaysIntersectedByHplane.resize(numSplits);
  raysToBeCut.resize(numSplits);

  vector<vector<int> > numRaysCutByHplane(numSplits);
  vector<vector<int> > activatedHplaneUBVect(numSplits);

  for (int s = 0; s < numSplits; s++) {
    chosenHplanes[s].resize(solnInfo.numCols + solnInfo.numRows, false);
    numRaysCutByHplane[s].resize(solnInfo.numCols + solnInfo.numRows, 0);
    activatedHplaneUBVect[s].resize(solnInfo.numCols, -1);
  }

  for (int j = 0; j < numRaysOfC1; j++)
    AllRayIndices[j] = j;

  for (int split_ind = 0; split_ind < numSplits; split_ind++) {
    // For each "ray to be cut," we choose the hyperplane that has best average depth for points created for that split
    splitsEffByHplane[0] = split_ind;
    for (int j = 0; j < numRaysToBeCut; j++) {
      nonBasicColIndex = solnInfo.nonBasicVarIndex[sortIndex[j]];

#ifdef TRACE
      string nb_var_name;
      if (nonBasicColIndex < solnInfo.numCols) {
        nb_var_name = solver.getColName(nonBasicColIndex);
      } else {
        nb_var_name = solver.getRowName(nonBasicColIndex - solnInfo.numCols);
      }
      printf(
          "Split %d, choosing hplane for ray (index %d, variable %d, and name %s).\n",
          split_ind, sortIndex[j], nonBasicColIndex, nb_var_name.c_str());
#endif

      // Find hyperplane that intersects ray and yields highest avg depth of resulting points.
      // It returns -1 if no hyperplane is intersected by the ray before the bdS for all chosen splits
      findHplaneToIntersectRayBestAvgDepth(solver, solnInfo, chooseSplits,
          sortIndex[j], nonBasicColIndex, hplaneVarIndexToAct,
          hplaneVarRowIndexToAct, hplaneVarUpperFlagToAct,
          minSlackRatio, splitsEffByHplane, AllRayIndices, NBSIC,
          inst_info_out);

      // If we did find a hyperplane
      if (hplaneVarIndexToAct != -1) {
        // If the hyperplane activated affects at least one split, then this ray is cut for some split.
        // (I.e., if ||splitsEffByHplane|| = 0, then for all splits,
        // the ray intersects the boundary of the split before it intersects
        // the hplane chosen.)
        if (splitsEffByHplane.size() > 0)
          allRaysCut.push_back(sortIndex[j]);

        //Only iterate over splits where the hyperplane intersects the ray before the bdS
        for (unsigned s1 = 0; s1 < splitsEffByHplane.size(); s1++) {

          tmpSplitIndex = splitsEffByHplane[s1];

          chosenHplanes[tmpSplitIndex][hplaneVarIndexToAct] = true;
          if (hplaneVarIndexToAct < solnInfo.numCols) // If variable is structural
            activatedHplaneUBVect[tmpSplitIndex][hplaneVarIndexToAct] =
                hplaneVarUpperFlagToAct;

          //Check if activated hyperplane is new or has been previously activated
          oldHplane = hasHplaneBeenActivated(
              hplanesIndexToBeActivated[tmpSplitIndex],
              hplanesToBeActivatedUBFlag[tmpSplitIndex],
              hplaneVarIndexToAct, hplaneVarUpperFlagToAct);

          if (oldHplane == -1) {
            //The activated hyperplane is new
            hplanesIndexToBeActivated[tmpSplitIndex].push_back(
                hplaneVarIndexToAct);
            hplanesRowIndexToBeActivated[tmpSplitIndex].push_back(
                hplaneVarRowIndexToAct);
            hplanesToBeActivatedUBFlag[tmpSplitIndex].push_back(
                hplaneVarUpperFlagToAct);

            //This stores only those rays of C1 that have to be cut by the new hplane for validity of the procedure
            tmpSize = raysToBeCutByHplane[tmpSplitIndex].size();
            raysToBeCutByHplane[tmpSplitIndex].resize(tmpSize + 1);
            raysToBeCutByHplane[tmpSplitIndex][tmpSize].push_back(
                sortIndex[j]); // Clearly this ray, r^j, is cut

            //Store index in hplanesIndexToBeActivated that stores the hyperplanes that cuts this ray first
            strongVertHplaneIndex[tmpSplitIndex].push_back(tmpSize); // tmpSize is index of hplane for this split
            // recall: raysToBeCutByHplane is [split][hplane][ray]
            // should be same as hplanesIndexToBeAct.size() - 1 (since we've already added it)

            //This stores all the rays of C1 cut by the new hyperplanes
            tmpSize1 =
                allRaysIntersectedByHplane[tmpSplitIndex].size();
            allRaysIntersectedByHplane[tmpSplitIndex].resize(
                tmpSize1 + 1);

            //Find all rays cut by hyperplane before bdS and store it in allRaysCutByHplane
            addRaysCutByHplaneBeforeBdS(solver, solnInfo,
                chooseSplits.splitVarIndex[tmpSplitIndex],
                chooseSplits.splitVarRowIndex[tmpSplitIndex],
                chooseSplits.pi0[tmpSplitIndex],
                hplaneVarIndexToAct, hplaneVarRowIndexToAct,
                hplaneVarUpperFlagToAct, AllRayIndices,
                allRaysIntersectedByHplane[tmpSplitIndex][tmpSize1],
                sortIndex[j], inst_info_out);

            numRaysCutByHplane[tmpSplitIndex][hplaneVarIndexToAct] =
                raysToBeCutByHplane[tmpSplitIndex][tmpSize].size();

          } else {
            raysToBeCutByHplane[tmpSplitIndex][oldHplane].push_back(
                sortIndex[j]);
            //Store index in hplanesIndexToBeActivated that stores the hyperplanes that cuts this ray first
            strongVertHplaneIndex[tmpSplitIndex].push_back(
                oldHplane);

            numRaysCutByHplane[tmpSplitIndex][hplaneVarIndexToAct] =
                raysToBeCutByHplane[tmpSplitIndex][oldHplane].size();
          }
          raysToBeCut[tmpSplitIndex].push_back(sortIndex[j]);
        }
      }
    }
  }
}

void findHplaneToIntersectRayBestAvgDepth(const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo,
    const chooseCutGeneratingSets& chooseSplits, const int rayIndex,
    const int nonBasicColIndex, int &hplaneVarIndexToAct,
    int &hplaneVarRowIndexToAct, int &hplaneVarUpperFlagToAct,
    double &minSlackRatio, const vector<int> &splitsEffByHplane,
    const std::vector<int>& allRayIndices, const std::vector<cut>& NBSIC,
    FILE *inst_info_out) {
  // Initialize values, in case no hplane is found at all
  hplaneVarIndexToAct = -1;
  hplaneVarRowIndexToAct = -1;
  hplaneVarUpperFlagToAct = -2;

  // For each of the splits we care about (splitsEffByHplane)
  // look at the average depth of the points that would be created if we activated each hplane
  double maxAverageDepth = -1.0;

  // Look at each structural hyperplane
  // For each that is intersected before bd S, fake activate it
  // Similarly for each basic structural variable, for each bound
  // Finally, check the bound of the ray itself
  const int numBasicSlacks = solnInfo.basicSlackVarIndex.size();
  const int numBasicOrig = solnInfo.basicOrigVarIndex.size()
      * ACTIVATE_BASIC_BOUNDS;
  const bool structNBRayFlag = nonBasicColIndex < solver.getNumCols();
  const int numHplanesToCheck = numBasicSlacks + numBasicOrig
      + 1 * structNBRayFlag * ACTIVATE_NB_BOUNDS; // when nb variable is not structural, we don't activate it
  const int numSplitsToCheck = splitsEffByHplane.size();

  for (int h = 0; h < numHplanesToCheck; h++) {
    int hplaneVar, hplaneRow, numBoundsToCheck;
    bool NBVar = false;
    double currValue;

    if (h < numBasicSlacks) {
      hplaneVar = solnInfo.basicSlackVarIndex[h];
      hplaneRow = solnInfo.basicSlackVarRowIndex[h];
      currValue = solnInfo.a0[hplaneRow];
      numBoundsToCheck = 1;
    } else if (h < numBasicSlacks + numBasicOrig) {
      hplaneVar = solnInfo.basicOrigVarIndex[h - numBasicSlacks];
      hplaneRow = solnInfo.basicOrigVarRowIndex[h - numBasicSlacks];
      currValue = solnInfo.a0[hplaneRow];
      numBoundsToCheck = 2;
    } else {
      hplaneVar = nonBasicColIndex;
      hplaneRow = -1;
      currValue = 0.0;
      numBoundsToCheck = 1;
      NBVar = true;
    }

    if ((hplaneRow != -1) && (solver.getRowSense()[hplaneRow] == 'E')) {
      continue; // Do not activate equality rows
    }

    for (int b = 0; b < numBoundsToCheck; b++) {
      bool hplaneAffectsASplit = false;
      double avgDepthForThisHplane = 0.0;

      double bound = 0.0;
      int UBFlag = b;
      // The only case we do not hold to this is when activating the nb var bound
      // In the comp nb space, we are activating an upper bound, but originally, it may be different
      if (NBVar) {
        // When it was at lower-bound, now it will be at upper-bound
        if (solnInfo.cstat[hplaneVar] == 3) {
          UBFlag = 1;
        }
        bound = solver.getColUpper()[hplaneVar]
            - solver.getColLower()[hplaneVar];
      } else if (b == 0) {
        if (hplaneVar < solver.getNumCols()) {
          bound = solver.getColLower()[hplaneVar];
        } else {
          bound = 0.0;
          UBFlag = -1;
        }
      } else if (b == 1) {
        bound = solver.getColUpper()[hplaneVar];
      }

      // This bound is actually at infinity; we'll never intersect it
      if (std::abs(bound) > solver.getInfinity() - param.getEPS())
        continue;

      double rayGrad;

      if (NBVar) {
        rayGrad = 1.0;
      } else {
        rayGrad = solnInfo.raysOfC1[rayIndex][hplaneRow];
      }

      if (std::abs(rayGrad) <= param.getRAYEPS()) {
        // This ray is parallel to the hplane
        continue;
      }

      const bool rayIntersectsLB = (UBFlag <= 0) && (rayGrad <= -param.getRAYEPS());
      const bool rayIntersectsUB = (UBFlag == 1) && (rayGrad >= param.getRAYEPS());

      if (!rayIntersectsLB && !rayIntersectsUB) {
        // The ray does not intersect the correct bound of the hplane
        continue;
      }

      // Else, the ray intersects this hplane
//      tmpAllRaysIntersected.push_back(ray_ind);
      // How far to go?
      const double distToHplane = (bound - currValue) / rayGrad;

      for (int s = 0; s < numSplitsToCheck; s++) {
        const int split_ind = splitsEffByHplane[s];

        const double splitValue = chooseSplits.splitVarValue[split_ind];
        const int splitRow = chooseSplits.splitVarRowIndex[split_ind];
        const double raySplitDirn =
            solnInfo.raysOfC1[rayIndex][splitRow];

        double distToSplit;
        if (std::abs(raySplitDirn) <= param.getRAYEPS()) {
          distToSplit = solver.getInfinity();
          if (!CUT_PARALLEL_RAYS) {
            continue; // Ignore rays parallel to the split
          }
        } else if (raySplitDirn < -param.getRAYEPS()) {
          // Intersects floor of split
          distToSplit = (std::floor(splitValue) - splitValue)
              / raySplitDirn;
        } else {
          // Intersects ceil of split
          distToSplit = (std::ceil(splitValue) - splitValue)
              / raySplitDirn;
        }

        if (distToHplane <= distToSplit - param.getEPS()) {
          // The hplane is intersected before bdS
          hplaneAffectsASplit = true;
          avgDepthForThisHplane += fakeActivate(solver, solnInfo,
              chooseSplits, split_ind, rayIndex, hplaneVar,
              hplaneRow, UBFlag, NBSIC[split_ind], allRayIndices,
              inst_info_out);
        }
      }
      avgDepthForThisHplane /= numSplitsToCheck;

      // If the avg depth is better than what we had before, use this hplane instead
      if (hplaneAffectsASplit
          && (avgDepthForThisHplane > maxAverageDepth)) {
        maxAverageDepth = avgDepthForThisHplane;
        hplaneVarIndexToAct = hplaneVar;
        hplaneVarRowIndexToAct = hplaneRow;
        hplaneVarUpperFlagToAct = UBFlag;
        minSlackRatio = distToHplane;
      }
    }
  }

}

/**
 * @return Average depth of points that would be created by activating this hplane
 */
double fakeActivate(const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo,
    const chooseCutGeneratingSets& chooseSplits, const int split_ind,
    const int ray_ind, const int hplaneVarIndex,
    const int hplaneVarRowIndex, const int hplaneUBFlag, const cut& NBSIC,
    const std::vector<int>& allRayIndices, FILE* inst_info_out) {
  int totalNumPointsRemoved = 0, totalNumPointsAdded = 0, numPointsOrRays = 0;
  double avgRemovedDepth = 0.0, avgAddedDepth = 0.0;
  std::vector<int> pointOrRayFlag;
  std::vector<ptOrRayInfo*> intPtOrRayInfo;
  const std::vector<int> raysToBeCutByHplane(1, ray_ind);
  std::vector<int> allRaysIntersectedByHplane;
  std::vector<std::vector<int> > partitionOfPtsAndRaysByRaysOfC1;
  partitionOfPtsAndRaysByRaysOfC1.resize(solnInfo.numNB);

  CoinPackedMatrix* interPtsAndRays = new CoinPackedMatrix;
  (*interPtsAndRays).reverseOrdering();
  (*interPtsAndRays).setDimensions(0, solnInfo.numNB);

  // Calculate totalSOS for the SIC for this split
  double totalSOS = 0.0;
  for (int k = 0; k < (int) NBSIC.compNB_coeff.size(); k++) {
    totalSOS += NBSIC.compNB_coeff[k] * NBSIC.compNB_coeff[k];
  }
  const double norm = std::sqrt(totalSOS);

  // Get all rays cut by this hplane
  addRaysCutByHplaneBeforeBdS(solver, solnInfo,
      chooseSplits.splitVarIndex[split_ind],
      chooseSplits.splitVarRowIndex[split_ind],
      chooseSplits.pi0[split_ind], hplaneVarIndex, hplaneVarRowIndex,
      hplaneUBFlag, allRayIndices, allRaysIntersectedByHplane, ray_ind,
      inst_info_out);

  // Now that we have that, we can do the activate hplane
  activateHplane(totalNumPointsRemoved, avgRemovedDepth, totalNumPointsAdded,
      avgAddedDepth, interPtsAndRays, pointOrRayFlag, numPointsOrRays,
      intPtOrRayInfo, chooseSplits, split_ind, -100, hplaneVarIndex,
      hplaneVarRowIndex, hplaneUBFlag, solver, solnInfo,
      raysToBeCutByHplane, partitionOfPtsAndRaysByRaysOfC1,
      allRaysIntersectedByHplane, NBSIC, norm, inst_info_out);

  return avgAddedDepth;

  // Use calculatePointStats to get statistics about these points
  // For each point generated for this split
  // Measure efficacy, and determine whether it is final
//  std::vector<double> normed_viol;
//  double min_viol = -1., max_viol = -1., sum = 0.0, sumSquares = 0.0, avg =
//      0.0;
//  int num_rays = 0, num_final_points = 0;
//  for (int pt_ind = 0; pt_ind < (int) intPtOrRayInfo.size(); pt_ind++) {
//    // If it's a point get it, o/w count the ray
//    if (intPtOrRayInfo[pt_ind]->ptFlag == 0) {
//      num_rays++;
//    } else {
//      calculatePointStats(num_final_points, normed_viol, min_viol,
//          max_viol, sum, sumSquares, split_ind, pt_ind, solver,
//          solnInfo, intPtOrRayInfo, interPtsAndRays, NBSIC, totalSOS,
//          inst_info_out);
//    }
//  }

  // Measure min, max, average, and std_dev
//  avg = sum / (int) normed_viol.size();
//  double std_dev = std::sqrt(sumSquares / (int) normed_viol.size() - avg * avg);
//  return avg;
}

void PHASecondHplane(std::vector<std::vector<int> >& hplaneVarIndexToActivate,
    std::vector<std::vector<int> >& hplaneVarRowIndexToActivate,
    std::vector<std::vector<int> >& hplaneVarUBFlagToActivate,
    std::vector<int>& allRaysCutActivation2,
    std::vector<std::vector<int> >& raysToBeCutActivation2,
    std::vector<std::vector<std::vector<int> > >& raysToBeCutByHplaneActivation2,
    std::vector<std::vector<std::vector<int> > >& allRaysIntersectedByHplaneActivation2,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    const chooseCutGeneratingSets& chooseSplits,
    CoinPackedMatrix** &interPtsAndRays,
    const std::vector<std::vector<int> >& pointOrRayFlag,
    const std::vector<int>& numPointsOrRays,
    const std::vector<std::vector<ptOrRayInfo*> >& intPtOrRayInfo,
    const std::vector<std::vector<std::vector<int> > >& partitionOfPtsAndRaysByRaysOfC1,
    const std::vector<std::vector<int> >& oldHplaneVarIndexToActivate,
    const std::vector<std::vector<int> >& oldHplaneVarRowIndexToActivate,
    const std::vector<std::vector<int> >& oldHplaneVarUBFlagToActivate,
    const std::vector<std::vector<int> >& oldRaysToBeCut,
    const std::vector<std::vector<std::vector<int> > >& oldRaysCutByHplane,
    const std::vector<std::vector<int> > strongVertHplaneIndex,
    FILE* inst_info_out) {
  const int numSplits = chooseSplits.nEffCutGenSets;
  const int numRaysOfC1 = solnInfo.raysOfC1.size();
  const char* rowSense = solver.getRowSense();
  std::vector<int> maxPointsRemoved(numSplits, -1);
  hplaneVarIndexToActivate.resize(numSplits);
  hplaneVarRowIndexToActivate.resize(numSplits);
  hplaneVarUBFlagToActivate.resize(numSplits);
  raysToBeCutActivation2.resize(numSplits);
  raysToBeCutByHplaneActivation2.resize(numSplits);
  allRaysIntersectedByHplaneActivation2.resize(numSplits);

  std::vector<int> firstHplane(numRaysOfC1, -1);

  std::vector<int> splitsEffByHplane(1, -1);

  for (int split_ind = 0; split_ind < numSplits; split_ind++) {
    hplaneVarIndexToActivate[split_ind].resize(1, -1);
    hplaneVarRowIndexToActivate[split_ind].resize(1, -1);
    hplaneVarUBFlagToActivate[split_ind].resize(1, -2);
    raysToBeCutByHplaneActivation2[split_ind].resize(1);
    allRaysIntersectedByHplaneActivation2[split_ind].resize(1);

    std::vector<int> tmpRaysToBeCut; // rays that are cut by this hplane
    std::vector<int> tmpRaysCutByHplane;
    std::vector<int> tmpAllRaysIntersected;

    // For the split, what were the rays previously cut for this split?
    firstHplane.resize(numRaysOfC1, -1);
    for (int r = 0; r < (int) oldRaysToBeCut[split_ind].size(); r++) {
      const int currRay = oldRaysToBeCut[split_ind][r];

      // Hplane that first intersected this ray
      firstHplane[currRay] = strongVertHplaneIndex[split_ind][r];
    }

    splitsEffByHplane[0] = split_ind;

    // For each split, we look at the possible hplanes, and choose one that cuts off the most points
    for (int h = 0; h < (int) solnInfo.basicSlackVarIndex.size(); h++) {
      // This is the index of the row corresponding to the slack variable
      const int currVar = solnInfo.basicSlackVarIndex[h];
      const int rowIndex = solnInfo.basicSlackVarRowIndex[h];

      // Equality constraints correspond to slack variables that are basic but have value zero
      // They clearly have zero distance from the optimal solution
      if (rowSense[rowIndex] == 'E') {
        continue;
      }

      const int numPointsCutByHplane = PHASecondHplaneHelper(
          tmpRaysToBeCut, tmpRaysCutByHplane, tmpAllRaysIntersected,
          currVar, rowIndex, -1, solver, solnInfo, split_ind,
          chooseSplits, interPtsAndRays, pointOrRayFlag,
          numPointsOrRays, intPtOrRayInfo,
          partitionOfPtsAndRaysByRaysOfC1, firstHplane,
          splitsEffByHplane, oldHplaneVarIndexToActivate,
          oldHplaneVarRowIndexToActivate,
          oldHplaneVarUBFlagToActivate, inst_info_out);

      if (numPointsCutByHplane > maxPointsRemoved[split_ind]) {
        maxPointsRemoved[split_ind] = numPointsCutByHplane;
        hplaneVarIndexToActivate[split_ind][0] = currVar;
        hplaneVarRowIndexToActivate[split_ind][0] = rowIndex;
        hplaneVarUBFlagToActivate[split_ind][0] = -1;
        raysToBeCutActivation2[split_ind] = tmpRaysToBeCut;
        raysToBeCutByHplaneActivation2[split_ind][0] =
            tmpRaysCutByHplane;
        allRaysIntersectedByHplaneActivation2[split_ind][0] =
            tmpAllRaysIntersected;
      }
    }

    if (SECOND_ACTIVATE_BASIC_BOUNDS) {
      // For each structural basic variable, see if one of its bound does better
      for (int v = 0; v < (int) solnInfo.basicOrigVarIndex.size(); v++) {
        // This is the index of the row corresponding to the slack variable
        const int currVar = solnInfo.basicOrigVarIndex[v];
        const int rowIndex = solnInfo.basicOrigVarRowIndex[v];

        // Equality constraints do not lead to interesting hyperplanes
        if (rowSense[rowIndex] == 'E') {
          continue;
        }

        const std::vector<int> splitsEffByHplane(1, split_ind);

        // Check lower bound of this variable
        const double LB = solver.getColLower()[currVar];
        if (LB > -1 * solver.getInfinity() + param.getEPS()) {
          const int numPointsCutByHplane = PHASecondHplaneHelper(
              tmpRaysToBeCut, tmpRaysCutByHplane,
              tmpAllRaysIntersected, currVar, rowIndex, 0, solver,
              solnInfo, split_ind, chooseSplits, interPtsAndRays,
              pointOrRayFlag, numPointsOrRays, intPtOrRayInfo,
              partitionOfPtsAndRaysByRaysOfC1, firstHplane,
              splitsEffByHplane, oldHplaneVarIndexToActivate,
              oldHplaneVarRowIndexToActivate,
              oldHplaneVarUBFlagToActivate, inst_info_out);

          if (numPointsCutByHplane > maxPointsRemoved[split_ind]) {
            maxPointsRemoved[split_ind] = numPointsCutByHplane;
            hplaneVarIndexToActivate[split_ind][0] = currVar;
            hplaneVarRowIndexToActivate[split_ind][0] = rowIndex;
            hplaneVarUBFlagToActivate[split_ind][0] = 0;
            raysToBeCutActivation2[split_ind] = tmpRaysToBeCut;
            raysToBeCutByHplaneActivation2[split_ind][0] =
                tmpRaysCutByHplane;
            allRaysIntersectedByHplaneActivation2[split_ind][0] =
                tmpAllRaysIntersected;
          }
        }

        // Check upper bound of this variable
        const double UB = solver.getColUpper()[currVar];
        if (UB < solver.getInfinity() - param.getEPS()) {
          const int numPointsCutByHplane = PHASecondHplaneHelper(
              tmpRaysToBeCut, tmpRaysCutByHplane,
              tmpAllRaysIntersected, currVar, rowIndex, 1, solver,
              solnInfo, split_ind, chooseSplits, interPtsAndRays,
              pointOrRayFlag, numPointsOrRays, intPtOrRayInfo,
              partitionOfPtsAndRaysByRaysOfC1, firstHplane,
              splitsEffByHplane, oldHplaneVarIndexToActivate,
              oldHplaneVarRowIndexToActivate,
              oldHplaneVarUBFlagToActivate, inst_info_out);

          if (numPointsCutByHplane > maxPointsRemoved[split_ind]) {
            maxPointsRemoved[split_ind] = numPointsCutByHplane;
            hplaneVarIndexToActivate[split_ind][0] = currVar;
            hplaneVarRowIndexToActivate[split_ind][0] = rowIndex;
            hplaneVarUBFlagToActivate[split_ind][0] = 1;
            raysToBeCutActivation2[split_ind] = tmpRaysToBeCut;
            raysToBeCutByHplaneActivation2[split_ind][0] =
                tmpRaysCutByHplane;
            allRaysIntersectedByHplaneActivation2[split_ind][0] =
                tmpAllRaysIntersected;
          }
        }
      }
    }

    if (SECOND_ACTIVATE_NB_BOUNDS) {
      // Check if one of the bounds of the non-basic columns does better
      for (int ray_ind = 0;
          ray_ind < (int) solnInfo.nonBasicOrigVarIndex.size();
          ray_ind++) {
        const int currVar = solnInfo.nonBasicOrigVarIndex[ray_ind];

        // Check that both bounds exist
        const double LB = solver.getColLower()[currVar];
        const double UB = solver.getColUpper()[currVar];

        if ((LB > -1 * solver.getInfinity() + param.getEPS())
            && (UB < solver.getInfinity() - param.getEPS())) {
          int UBFlag = 0;
          if (solnInfo.cstat[currVar] == 3) // It was at LB, now will be at UB
            UBFlag = 1;

          const int numPointsCutByHplane = PHASecondHplaneHelper(
              tmpRaysToBeCut, tmpRaysCutByHplane,
              tmpAllRaysIntersected, currVar, -1, UBFlag, solver,
              solnInfo, split_ind, chooseSplits, interPtsAndRays,
              pointOrRayFlag, numPointsOrRays, intPtOrRayInfo,
              partitionOfPtsAndRaysByRaysOfC1, firstHplane,
              splitsEffByHplane, oldHplaneVarIndexToActivate,
              oldHplaneVarRowIndexToActivate,
              oldHplaneVarUBFlagToActivate, inst_info_out,
              ray_ind);

          if (numPointsCutByHplane > maxPointsRemoved[split_ind]) {
            maxPointsRemoved[split_ind] = numPointsCutByHplane;
            hplaneVarIndexToActivate[split_ind][0] = currVar;
            hplaneVarRowIndexToActivate[split_ind][0] = -1;
            hplaneVarUBFlagToActivate[split_ind][0] = UBFlag;
            raysToBeCutActivation2[split_ind] = tmpRaysToBeCut;
            raysToBeCutByHplaneActivation2[split_ind][0] =
                tmpRaysCutByHplane;
            allRaysIntersectedByHplaneActivation2[split_ind][0] =
                tmpAllRaysIntersected;
          }
        }
      }
    }

#ifdef TRACE
    printf("Second hplane split %d removes %d points. Hplane var index: %d. Hplane row index: %d. Hplane UB flag: %d.\n",
        split_ind, maxPointsRemoved[split_ind],hplaneVarIndexToActivate[split_ind][0], hplaneVarRowIndexToActivate[split_ind][0], hplaneVarUBFlagToActivate[split_ind][0]);
#endif
  }

  // Now update allRaysCut
  std::vector<bool> allRaysCutFlag(numRaysOfC1, false);
  for (int s = 0; s < numSplits; s++) {
    for (int r = 0; r < (int) raysToBeCutActivation2[s].size(); r++) {
      if (!allRaysCutFlag[raysToBeCutActivation2[s][r]]) {
        allRaysCutFlag[raysToBeCutActivation2[s][r]] = true;
        allRaysCutActivation2.push_back(raysToBeCutActivation2[s][r]);
      }
    }
  }
}

/**
 * Finds number of points that would be removed by activating this hplane on this split
 */
int PHASecondHplaneHelper(std::vector<int>& tmpRaysToBeCut,
    std::vector<int>& tmpRaysCutByHplane,
    std::vector<int>& tmpAllRaysIntersected, const int hplaneVarToAct,
    const int hplaneVarRowToAct, const int hplaneVarUBFlagToAct,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    const int split_ind, const chooseCutGeneratingSets& chooseSplits,
    CoinPackedMatrix** &interPtsAndRays,
    const std::vector<std::vector<int> >& pointOrRayFlag,
    const std::vector<int>& numPointsOrRays,
    const std::vector<std::vector<ptOrRayInfo*> >& intPtOrRayInfo,
    const std::vector<std::vector<std::vector<int> > >& partitionOfPtsAndRaysByRaysOfC1,
    const std::vector<int>& firstHplane,
    const std::vector<int>& splitsEffByHplane,
    const std::vector<std::vector<int> >& oldHplaneVarIndexToActivate,
    const std::vector<std::vector<int> >& oldHplaneVarRowIndexToActivate,
    const std::vector<std::vector<int> >& oldHplaneVarUBFlagToActivate,
    FILE* inst_info_out, const int rayInd) {
  // Clear data structures
  tmpRaysToBeCut.clear();
  tmpRaysCutByHplane.clear();
  tmpAllRaysIntersected.clear();

  const int numRaysOfC1 = solnInfo.raysOfC1.size();
  const int split_row = chooseSplits.splitVarRowIndex[split_ind];
  int numPointsCutByHplane = 0;

  if (hplaneVarRowToAct == -1) {
    // The only ray that intersects this nb bound is the ray itself
    const double currValue = 0.0; // We assume all nb variables are at zero
    // Note: both of the bounds are finite, since we are activating it
    const double bound = solver.getColUpper()[hplaneVarToAct]
        - solver.getColLower()[hplaneVarToAct];
    const double rayGrad = 1.0;
    // How far to go?
    const double distToHplane = (bound - currValue) / rayGrad;

    // For each split, check whether the hplane is intersected before the split boundary
    const double raySplitDirn = solnInfo.raysOfC1[rayInd][split_row];
    const double splitValue = solnInfo.a0[split_row];
    double distToSplit;

    if (raySplitDirn > param.getRAYEPS()) {
      // ray hits pi0 + 1
      distToSplit = (std::ceil(splitValue) - splitValue) / raySplitDirn;
    } else if (raySplitDirn < -param.getRAYEPS()) {
      // ray hits pi0
      distToSplit = (std::floor(splitValue) - splitValue) / raySplitDirn;
    } else {
      if (distToHplane <= solver.getInfinity() - param.getEPS()) {
        tmpAllRaysIntersected.push_back(rayInd);
      }
      return 0; // Do not consider rays parallel to the split
    }

    // Check that the hplane is intersected before the split boundary
    if (distToHplane <= distToSplit - param.getEPS()) {
      tmpAllRaysIntersected.push_back(rayInd);

      // We don't continue if this ray already is cut by this hplane
      if (firstHplane[rayInd] != -1) {
        const int old_h_ind = firstHplane[rayInd];
        const int old_hvar =
            oldHplaneVarIndexToActivate[split_ind][old_h_ind];
        const int old_hvar_ubflag =
            oldHplaneVarUBFlagToActivate[split_ind][old_h_ind];
        if ((old_hvar == hplaneVarToAct)
            && (old_hvar_ubflag == hplaneVarUBFlagToAct)) {
          return 0;
        }
      }
      tmpRaysToBeCut.push_back(rayInd);
      tmpRaysCutByHplane.push_back(rayInd);
      std::vector<std::vector<int> > tmpPointsToRemove;
      int numPointsOfRayCutByHplane = numPointsOfRayViolatingHplane(
          tmpPointsToRemove, rayInd, hplaneVarToAct,
          hplaneVarRowToAct, hplaneVarUBFlagToAct, interPtsAndRays,
          intPtOrRayInfo, partitionOfPtsAndRaysByRaysOfC1,
          chooseSplits, splitsEffByHplane, solver, solnInfo);
      numPointsCutByHplane += numPointsOfRayCutByHplane;
    }

    return numPointsCutByHplane;
  }

  double bound = 0.0;
  if (hplaneVarUBFlagToAct == 0) {
    bound = solver.getColLower()[hplaneVarToAct];
  } else if (hplaneVarUBFlagToAct == 1) {
    bound = solver.getColUpper()[hplaneVarToAct];
  }

  if (std::abs(bound) > solver.getInfinity() - param.getEPS())
    return 0;

  // For each ray, find distance to this hyperplane
  const double currValue = solnInfo.a0[hplaneVarRowToAct];
  for (int ray_ind = 0; ray_ind < numRaysOfC1; ray_ind++) {
    const double rayGrad = solnInfo.raysOfC1[ray_ind][hplaneVarRowToAct];

    if (std::abs(rayGrad) <= param.getRAYEPS()) {
      // This ray is parallel to the hplane
      continue;
    }

    const bool rayIntersectsLB = (hplaneVarUBFlagToAct <= 0)
        && (rayGrad <= -param.getRAYEPS());
    const bool rayIntersectsUB = (hplaneVarUBFlagToAct == 1)
        && (rayGrad >= param.getRAYEPS());

    if (!rayIntersectsLB && !rayIntersectsUB) {
      // The ray does not intersect the correct bound of the hplane
      continue;
    }

    // How far to go?
    const double distToHplane = (bound - currValue) / rayGrad;

    // For each split, check whether the hplane is intersected before the split boundary
    const double raySplitDirn = solnInfo.raysOfC1[ray_ind][split_row];
    const double splitValue = solnInfo.a0[split_row];
    double distToSplit;

    if (raySplitDirn >= param.getRAYEPS()) {
      // ray hits pi0 + 1
      distToSplit = (std::ceil(splitValue) - splitValue) / raySplitDirn;
    } else if (raySplitDirn <= -param.getRAYEPS()) {
      // ray hits pi0
      distToSplit = (std::floor(splitValue) - splitValue) / raySplitDirn;
    } else {
      if (distToHplane <= solver.getInfinity() - param.getEPS()) {
        tmpAllRaysIntersected.push_back(ray_ind);
      }
      continue; // Do not consider rays parallel to the split
    }

    // Check that the hplane is intersected before the split boundary
    if (distToHplane <= distToSplit - param.getEPS()) {
      // The ray intersects this hplane before bd S
      tmpAllRaysIntersected.push_back(ray_ind);

      // We don't continue if this ray already is cut by this hplane
      if (firstHplane[ray_ind] != -1) {
        const int old_h_ind = firstHplane[ray_ind];
        const int old_hvar =
            oldHplaneVarIndexToActivate[split_ind][old_h_ind];
        const int old_hvar_ubflag =
            oldHplaneVarUBFlagToActivate[split_ind][old_h_ind];
        if ((old_hvar == hplaneVarToAct)
            && (old_hvar_ubflag == hplaneVarUBFlagToAct)) {
          continue;
        }
      }
      tmpRaysToBeCut.push_back(ray_ind);
      tmpRaysCutByHplane.push_back(ray_ind);
      std::vector<std::vector<int> > tmpPointsToRemove;
      int numPointsOfRayCutByHplane = numPointsOfRayViolatingHplane(
          tmpPointsToRemove, ray_ind, hplaneVarToAct,
          hplaneVarRowToAct, hplaneVarUBFlagToAct, interPtsAndRays,
          intPtOrRayInfo, partitionOfPtsAndRaysByRaysOfC1,
          chooseSplits, splitsEffByHplane, solver, solnInfo);
      numPointsCutByHplane += numPointsOfRayCutByHplane;
    }
  }
  return numPointsCutByHplane;
}

/**
 * Find previous splits for which this hplane has been activated
 */
void findPrevSplitsEffByHplane(std::vector<int>& prevSplitsEffByHplane,
    const int hplaneVarIndexToAct, const int hplaneVarUBFlagToAct,
    const std::vector<std::vector<int> >& oldActHplaneVarIndex,
    const std::vector<std::vector<int> >& oldActHplaneUBFlag) {
  for (int split_ind = 0; split_ind < (int) oldActHplaneVarIndex.size();
      split_ind++) {
    prevSplitsEffByHplane[split_ind] = hasHplaneBeenActivated(
        oldActHplaneVarIndex[split_ind], oldActHplaneUBFlag[split_ind],
        hplaneVarIndexToAct, hplaneVarUBFlagToAct);
  }
}

/**
 * Finds points created from rayInd that are cut by this hyperplane
 */
int numPointsOfRayViolatingHplane(
    std::vector<std::vector<int> >& pointsToRemove, const int rayInd,
    const int hplaneVarIndex, const int hplaneVarRowIndex,
    const int hplaneUBFlag, CoinPackedMatrix**& interPtsAndRays,
    const std::vector<std::vector<ptOrRayInfo*> >& intPtOrRayInfo,
    const std::vector<std::vector<std::vector<int> > >& partitionOfPtsAndRaysByRaysOfC1,
    const chooseCutGeneratingSets& chooseSplits,
    const std::vector<int> & splitsEffByHplane,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo) {
  pointsToRemove.clear();
  pointsToRemove.resize(chooseSplits.nEffCutGenSets);
  int numViolPoints = 0;
  for (int s_ind = 0; s_ind < (int) splitsEffByHplane.size(); s_ind++) {
    const int s = splitsEffByHplane[s_ind];
    const int numPointsAndRaysByRay =
        partitionOfPtsAndRaysByRaysOfC1[s][rayInd].size();
    for (int ind = 0; ind < numPointsAndRaysByRay; ind++) {
      const int curr_ptray_ind =
          partitionOfPtsAndRaysByRaysOfC1[s][rayInd][ind];

      if (intPtOrRayInfo[s][curr_ptray_ind]->ptFlag == true) {
        const CoinShallowPackedVector tmpVector =
            (*interPtsAndRays[s]).getVector(curr_ptray_ind);
        const int numElmts = tmpVector.getNumElements();
        const int* elmtIndices = tmpVector.getIndices();
        const double* elmtVals = tmpVector.getElements();

        const bool NBFlag = hplaneVarRowIndex < 0;

        // The NBFlag prevents us from using a -1 row index
        const double xVal = !NBFlag
            * solnInfo.a0[!NBFlag * hplaneVarRowIndex];
        double NBRowActivity = 0.0;
        for (int k = 0; k < numElmts; k++) {
          if (!NBFlag) {
            NBRowActivity +=
                solnInfo.raysOfC1[elmtIndices[k]][hplaneVarRowIndex]
                    * elmtVals[k];
          } else if (elmtIndices[k] == hplaneVarIndex) {
            NBRowActivity += elmtVals[k];
          }
        }
        const double newVal = xVal + NBRowActivity;
        double colLB, colUB;

        if (hplaneVarIndex < solver.getNumCols()) {
          if (!NBFlag) {
            colLB = solver.getColLower()[hplaneVarIndex];
            colUB = solver.getColUpper()[hplaneVarIndex];
          } else {
            colLB = 0.0;
            colUB = solver.getColUpper()[hplaneVarIndex]
                - solver.getColLower()[hplaneVarIndex];
          }
        } else {
          colLB = 0.0;
          colUB = solver.getInfinity();
        }

        if (newVal < colLB - param.getEPS() || newVal > colUB + param.getEPS()) {
          numViolPoints++;
          pointsToRemove[s].push_back(curr_ptray_ind);
        }

      }
    }
  }
  return numViolPoints;
}

///**Choose hyperplanes to avoid weak vertices*/
//void cutRaysWithStrongHplanes() {
//  //Choose rays to be cut based on weakest average cost coefficients among Gomory cuts over the chosen splits
//  vector<double> avgCoeffAcrossCuts(solnInfo.numNonBasicCols, 0.0);
//  vector<int> sortIndex(solnInfo.numNonBasicCols);
//  for (int j = 0; j < solnInfo.numNonBasicCols; j++) {
//    sortIndex[j] = j;
//    for (int s = 0; s < numSplits; s++) {
//      avgCoeffAcrossCuts[j] += stdCutGen.stdIntersectionCuts[s].coeff[j];
//    }
//    avgCoeffAcrossCuts[j] /= (double) numSplits;
//  }
//
//  //Sort average cut coefficients in descending order. Larger cut coefficients in the J-space correspond to weaker cuts
//  sort(sortIndex.begin(), sortIndex.end(), index_cmp<double*> (
//      &avgCoeffAcrossCuts[0]));
//
//}

/**
 * This method returns a set of hyperplanes identified by the sets
 * vector<vector<int> > &hplanesIndexToBeActivated,
 * vector<vector<int> > &hplanesRowIndexToBeActivated,
 * vector<vector<int> > &hplanesToBeActivatedUBFlag
 * For each hyperplane it also populates the vector vector<vector< raysToBeCutByHplanevector<int> > >
 * which contains the rays that need to be intersected when activating a hyperplane
 *
 * 1. There are |N| rays to be cut. Cut as many as specified by numRaysToCut.
 *    Do this in order of those that have the largest average coefficient in terms of the Gomory cuts.
 *    Larger coefficients in J-space correspond to weaker cuts (since you go not as far along that ray).
 * 2. For each ray to be cut, figure out which hyperplanes can cut it, and take the one that cuts it first.
 * 3. If a hplane to cut the ray is found, see if it's been activated.
 *      a. If no, then for each split in which the hplane cuts the ray before the ray hits the bd of the split,
 *
 *
 * @param hplaneHeur                    ::  Heuristic used to activate hyperplanes
 * @param solver                        ::  Solver for original problem
 * @param solnInfo.a0                           ::  Solution to the LP relaxation
 * @param solnInfo.raysOfC1                     ::  Rays of the original simplicial cone
 * @param numSplits                     ::  Number of splits
 * @param solnInfo.numcols                        ::  Number of columns in the LP relaxation
 * @param solnInfo.numNonBasicCols                  ::  Number of non-basic columns
 * @param solnInfo.numrows                        ::  Number of rows
 * @param stdCutGen                     ::  Standard intersection cut generator
 * @param allRaysCut                    ::  OUTPUT: The set of rays whose chosen hplane cuts the
 *                                                  ray before bd S for at least one split.
 * @param raysToBeCut                   ::  OUTPUT: [split][ray index], stores all rays to be cut by at least one hplane in this split
 * @param nRaysToBeCut                  ::
 * @param raysToBeCutByHplane           ::  OUTPUT: [split][num hplane cutting a ray in this split][index of ray cut by hplane for this split]
 * @param strongVertHplaneIndex         ::  OUTPUT: [split][index of the hplane]
 *                                                  So [0][3] = 2 says that the fourth (since indexing starts at 0) hplane intersecting some
 *                                                  ray before the ray hits the boundary of the first split is the second hplane that was activated.
 * @param hplanesIndexToBeActivated     ::  OUTPUT: Index of hyperplane to be activated.
 * @param hplanesRowIndexToBeActivated    ::  OUTPUT: Row index of the hyperplane to be activated. -1 if hyperplane was not structural.
 * @param hplanesToBeActivatedUBFlag    ::  OUTPUT: -1 (not a bound; i.e. it's a slack var?), 0 (a LB), or 1 (an UB).
 * @param nonBasicVarIndices            ::  Variable indices of the non-basic columns
 * @param cstat                         ::  0 free, 1 basic, 2 upper, 3 lower
 * @param chooseSplits.pi0                            ::  Floor of the LP relaxation solution for the variable of x_k, for each split k
 * @param chooseSplits.splitVarRowIndex               ::  Row in which the variable is basic for each split
 * @param splitVarIndex                 ::  Index of the variable used for each split
 * @param numBasicSlacks                ::  Number of basic slack variables
 * @param solnInfo.basicSlackVarIndex           ::  Index for each basic slack
 * @param solnInfo.basicSlackVarRowIndex          ::  Row in which each basic slack is basic
 * @param numBasicOrigVars              ::  Number of basic original variables
 * @param solnInfo.basicOrigVarIndex              ::  Variable index of each basic columns
 * @param solnInfo.basicOrigVarRowIndex         ::  Row in which each basic variable is basic
 * @param inst_info_out                 ::  For outputting errors
 */
void PHAOneOne(const int hplaneHeur, const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo,
    const chooseCutGeneratingSets& chooseSplits, const vector<cut> &NBSIC,
    vector<int> &allRaysCut, vector<vector<int> > &raysToBeCut,
    const int numRaysToBeCut,
    vector<vector<vector<int> > > &raysToBeCutByHplane,
    vector<vector<int> > &strongVertHplaneIndex,
    vector<vector<vector<int> > > &allRaysIntersectedByHplane,
    vector<vector<int> > &hplanesIndexToBeActivated,
    vector<vector<int> > &hplanesRowIndexToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    int &num_hplanes_activated, int max_hplanes_activated,
    FILE* inst_info_out) {
  int numSplits = chooseSplits.nEffCutGenSets;

  //Choose rays to be cut based on weakest average cost coefficients among Gomory cuts over the chosen splits
  vector<double> avgCoeffAcrossCuts(solnInfo.numNB, 0.0);
  vector<int> sortIndex(solnInfo.numNB);
  for (int j = 0; j < solnInfo.numNB; j++) {
    sortIndex[j] = j;
    for (int s = 0; s < numSplits; s++) {
      avgCoeffAcrossCuts[j] += NBSIC[s].compNB_coeff[j];
    }
    avgCoeffAcrossCuts[j] /= (double) numSplits;
  }

  //Sort average cut coefficients in descending order. Larger cut coefficients in the J-space correspond to weaker cuts
  sort(sortIndex.begin(), sortIndex.end(),
      index_cmp_dsc<double*>(&avgCoeffAcrossCuts[0]));

  //  solver.enableFactorization();
  const char* rowSense = solver.getRowSense();

  // Resize arrays and put default values
  double minSlackRatio;
  int nonBasicColIndex;
  int hplaneBasicVarIndex, hplaneBasicVarRowIndex, hplaneBasicVarUpperFlag;
  int oldHplane, tmpSplitIndex;
  int tmpSize, tmpSize1;
//  int tmpSize2,tmpSize3;
  // Set of splits that the hyperplane activation affects;
  // i.e., split s is here if the ray intersects the
  // chosen hplane *before* the ray intersects the bd of split s.
  vector<int> splitsEffByHplane;
  vector<bool> raysInterHplane(solnInfo.numNB);
  // Set of hyperplanes activated. Indices are [split][hplane].
  vector<vector<bool> > chosenHplanes(numSplits);
  vector<int> AllRayIndices(solnInfo.numNB);

  hplanesIndexToBeActivated.resize(numSplits);
  hplanesRowIndexToBeActivated.resize(numSplits);
  hplanesToBeActivatedUBFlag.resize(numSplits);
  raysToBeCutByHplane.resize(numSplits);
  strongVertHplaneIndex.resize(numSplits);
  allRaysIntersectedByHplane.resize(numSplits);
  raysToBeCut.resize(numSplits);

  vector<vector<int> > numRaysCutByHplane(numSplits);
  vector<vector<int> > activatedHplaneUBVect(numSplits);

  for (int s = 0; s < numSplits; s++) {
    chosenHplanes[s].resize(solnInfo.numCols + solnInfo.numRows, false);
    numRaysCutByHplane[s].resize(solnInfo.numCols + solnInfo.numRows, 0);
    activatedHplaneUBVect[s].resize(solnInfo.numCols, -1);
  }

  for (int j = 0; j < solnInfo.numNB; j++)
    AllRayIndices[j] = j;

  // For each "ray to be cut," see which splits are affected by the hplane activation
  for (int j = 0; j < numRaysToBeCut; j++) {
    splitsEffByHplane.clear();
    nonBasicColIndex = solnInfo.nonBasicVarIndex[sortIndex[j]];

#ifdef TRACE
    if (nonBasicColIndex < solnInfo.numCols) {
      printf(
          "\nIn idRaysToBeCutByHplanes: Choosing hplane for ray (index %d and name %s).\n",
          nonBasicColIndex,
          solver.getColName(nonBasicColIndex).c_str());

    } else {

      printf(
          "\nIn idRaysToBeCutByHplanes: Choosing hplane for ray (index %d and name %s).\n",
          nonBasicColIndex,
          solver.getRowName(nonBasicColIndex - solnInfo.numCols).c_str());
    }
#endif

    //Find hyperplane that intersects ray. Currently this finds the hyperlane intersecting first ray.
    //It returns -1 if no hyperplane is intersected by the ray before the bdS for all chosen splits
    if (hplaneHeur == 1) {
      findHplaneToIntersectRay(solver, solnInfo, chooseSplits, numSplits,
          nonBasicColIndex, solnInfo.raysOfC1[sortIndex[j]],
          hplaneBasicVarIndex, hplaneBasicVarRowIndex,
          hplaneBasicVarUpperFlag, minSlackRatio, splitsEffByHplane,
          inst_info_out);
    }

    if (hplaneHeur == 2) {
      findHplaneMinWeakVert(solver, solnInfo, chooseSplits, numSplits,
          nonBasicColIndex, sortIndex[j],
          solnInfo.raysOfC1[sortIndex[j]], hplaneBasicVarIndex,
          hplaneBasicVarRowIndex, hplaneBasicVarUpperFlag,
          minSlackRatio, splitsEffByHplane, raysToBeCut,
          chosenHplanes, numRaysCutByHplane, activatedHplaneUBVect,
          inst_info_out);
    }

    // If the basic variable index of the hplane is a slack variable, and it's an equality constraint, ERROR!
    if (hplaneBasicVarIndex >= solnInfo.numCols) {
      if (rowSense[hplaneBasicVarIndex - solnInfo.numCols] == 'E') {
        error_msg(errstr,
            "Equality constraint chosen. Row index = %d, Col index = %d\n",
            hplaneBasicVarRowIndex, nonBasicColIndex);
        writeErrorToLog(errstr, inst_info_out);
        exit(1);
      }
    }
#ifdef TRACE
    cout << "\tOrig index of ray in array: " << sortIndex[j]
    << "\tNon-basic col index: " << nonBasicColIndex
    << "\tValue of avg coeff: " << avgCoeffAcrossCuts[sortIndex[j]]
    << "\n";
    cout << "\tHplane basic var index: " << hplaneBasicVarIndex
    << "\tHplane basic var row index: " << hplaneBasicVarRowIndex
    << "\tHplane basic var upper flag: " << hplaneBasicVarUpperFlag
    << "\n";
    cout << "\tMin slack ratio: " << minSlackRatio
    << "\tSplits eff by hplane: " << (int) splitsEffByHplane.size()
    << "\n";
#endif
    if (hplaneBasicVarIndex == -1) {
#ifdef TRACE
      printf("~~~~~~~~~~~~~~~~~ BASIC VAR INDEX IS -1.\n");
#endif
    }

    // If we did find a hyperplane
    if (hplaneBasicVarIndex != -1) {
      // If the hyperplane activated affects at least one split, then this ray is cut for some split.
      // (I.e., if ||splitsEffByHplane|| = 0, then for all splits,
      // the ray intersects the boundary of the split before it intersects
      // the hplane chosen.)
      if (splitsEffByHplane.size() > 0)
        allRaysCut.push_back(sortIndex[j]);

      //Only iterate over splits where the hyperplane intersects the ray before the bdS
      for (unsigned s1 = 0; s1 < splitsEffByHplane.size(); s1++) {

        tmpSplitIndex = splitsEffByHplane[s1];

        chosenHplanes[tmpSplitIndex][hplaneBasicVarIndex] = true;
        if (hplaneBasicVarIndex < solnInfo.numCols) // If variable is structural
          activatedHplaneUBVect[tmpSplitIndex][hplaneBasicVarIndex] =
              hplaneBasicVarUpperFlag;

        //Check if activated hyperplane is new or has been previously activated
        oldHplane = hasHplaneBeenActivated(
            hplanesIndexToBeActivated[tmpSplitIndex],
            hplanesToBeActivatedUBFlag[tmpSplitIndex],
            hplaneBasicVarIndex, hplaneBasicVarUpperFlag);

        if (oldHplane == -1) {
          //The activated hyperplane is new
          hplanesIndexToBeActivated[tmpSplitIndex].push_back(
              hplaneBasicVarIndex);
          hplanesRowIndexToBeActivated[tmpSplitIndex].push_back(
              hplaneBasicVarRowIndex);
          hplanesToBeActivatedUBFlag[tmpSplitIndex].push_back(
              hplaneBasicVarUpperFlag);

          //This stores only those rays of C1 that have to be cut by the new hplane for validity of the procedure
          tmpSize = raysToBeCutByHplane[tmpSplitIndex].size();
          raysToBeCutByHplane[tmpSplitIndex].resize(tmpSize + 1);
          raysToBeCutByHplane[tmpSplitIndex][tmpSize].push_back(
              sortIndex[j]); // Clearly this ray, r^j, is cut

          //Store index in hplanesIndexToBeActivated that stores the hyperplanes that cuts this ray first
          strongVertHplaneIndex[tmpSplitIndex].push_back(tmpSize); // tmpSize is index of hplane for this split
          // recall: raysToBeCutByHplane is [split][hplane][ray]
          // should be same as hplanesIndexToBeAct.size() - 1 (since we've already added it)

          //This stores all the rays of C1 cut by the new hyperplanes
          tmpSize1 = allRaysIntersectedByHplane[tmpSplitIndex].size();
          allRaysIntersectedByHplane[tmpSplitIndex].resize(
              tmpSize1 + 1);

          //Find all rays cut by hyperplane before bdS and store it in allRaysCutByHplane
          addRaysCutByHplaneBeforeBdS(solver, solnInfo,
              chooseSplits.splitVarIndex[tmpSplitIndex],
              chooseSplits.splitVarRowIndex[tmpSplitIndex],
              chooseSplits.pi0[tmpSplitIndex],
              hplaneBasicVarIndex, hplaneBasicVarRowIndex,
              hplaneBasicVarUpperFlag, AllRayIndices,
              allRaysIntersectedByHplane[tmpSplitIndex][tmpSize1],
              sortIndex[j], inst_info_out);

//          //TODO UNDERSTAND THIS SECTION!
//          //Add all rays cut but by this new hyperplane that were processed previously
//          // So now raysInterHplane has true in index j if ray j intersects the hplane chosen for this split
//          fill(raysInterHplane.begin(), raysInterHplane.end(), false);
//          tmpSize2 =
//              allRaysCutByHplane[tmpSplitIndex][tmpSize1].size();
//          for (int k = 0; k < tmpSize2; k++)
//            raysInterHplane[allRaysCutByHplane[tmpSplitIndex][tmpSize1][k]] =
//                true;
//
//          tmpSize3 = raysToBeCut[tmpSplitIndex].size(); // raysToBeCut stores all rays to be cut by at least one hplane in this split
//          // For all rays that have previously been added as cut
//          for (int k = 0; k < tmpSize3; k++) {
//            int currRayIndex = raysToBeCut[tmpSplitIndex][k];
//            // If this ray that has previously been cut is also cut for this split,
//            // add its index to raysToBeCutByHplane
//            if (raysInterHplane[currRayIndex] == true) {
//              raysToBeCutByHplane[tmpSplitIndex][tmpSize].push_back(
//                  currRayIndex);
//            }
//          }
          numRaysCutByHplane[tmpSplitIndex][hplaneBasicVarIndex] =
              raysToBeCutByHplane[tmpSplitIndex][tmpSize].size();

        } else {
          raysToBeCutByHplane[tmpSplitIndex][oldHplane].push_back(
              sortIndex[j]);
          //Store index in hplanesIndexToBeActivated that stores the hyperplanes that cuts this ray first
          strongVertHplaneIndex[tmpSplitIndex].push_back(oldHplane);
//
//          //Check hplanes from oldHplane + 1 till the end in the sequence to check if they cut this ray.
//          //If yes, add the ray to hyperplane's ray set
//          addRayToHplanesIntersectedBeforeBdS(solver, oldHplane,
//              solnInfo, chooseSplits.splitVarIndex[tmpSplitIndex],
//              chooseSplits.splitVarRowIndex[tmpSplitIndex],
//              chooseSplits.pi0[tmpSplitIndex], sortIndex[j],
//              hplanesIndexToBeActivated[tmpSplitIndex],
//              hplanesRowIndexToBeActivated[tmpSplitIndex],
//              hplanesToBeActivatedUBFlag[tmpSplitIndex],
//              raysToBeCutByHplane[tmpSplitIndex], inst_info_out);
          numRaysCutByHplane[tmpSplitIndex][hplaneBasicVarIndex] =
              raysToBeCutByHplane[tmpSplitIndex][oldHplane].size();
        }
        raysToBeCut[tmpSplitIndex].push_back(sortIndex[j]);
      }
    }
  }
}

/**
 * @brief Finds the first hyperplane to intersect a ray. Returns -1 if no hyperplane
 * intersects the ray before bdS over all chosen splits.
 *
 * The procedure is:
 *  1. Compute (1/\alpha_j) for each split and the given ray j. This is the multiplier such that
 *      \bar a_{i0} - (1/\alpha_j) \bar a_{ij} = chooseSplits.pi0 or chooseSplits.pi0 + 1.
 *  2. For each original constraint, see if ray j intersects it. Store the first one we intersect.
 *      **  If none are intersected by the ray, then the program is unbounded in this direction
 *          except for possibly bounds on just the variables!
 *  3. For each bound on a basic original variable, check if the bound is actually intersected
 *      prior to the hyperplane found before. If it is, this is the new hyperplane first intersected.
 *  4. Finally, check if maybe the ray actually first intersects a bound on the non-basic var
 *      that corresponds to this ray.
 *
 * @param solver                  ::  Solver from which we are generating cut
 * @param solnInfo.numcols              ::  Initial number of variables
 * @param cstat                   ::  0 free, 1 basic, 2 upper, 3 lower
 * @param numSplits               ::  Number effective cut-generating sets
 * @param nonBasicColIndex          ::  Index of the ray we are currently checking
 * @param basicCol                ::  Column \bar a^j corresponding to the ray
 * @param solnInfo.a0                     ::  Solution vector ordered by original rows
 * @param chooseSplits.pi0                      ::  Floor of solnInfo.a0 for the split variables
 * @param chooseSplits.splitVarRowIndex         ::  Row in which the split var is basic
 * @param splitVarIndex           ::  Index of the split variable
 * @param hplaneBasicVarIndex     ::  OUTPUT: Index of basic var in constr chosen
 * @param hplaneBasicVarRowIndex    ::  OUTPUT: Row index of constraint chosen
 * @param hplanBasicVarUpperFlag  ::  OUTPUT: -1 (not a bound), 0 (a LB), or 1 (an UB)
 * @param minSlackRatio           ::  OUTPUT: How far along the ray we go before intersecting ray
 * @param numBasicSlacks          ::  Number of basic slack variables
 * @param solnInfo.basicSlackVarIndex     ::  Indices of basic slack variables
 * @param solnInfo.basicSlackVarRowIndex    ::  Row in which the slack var is basic
 * @param numBasicOrigVars        ::  Number of basic original variables
 * @param solnInfo.basicOrigVarRowIndex   ::  Row in which basic orig var is basic
 * @param solnInfo.basicOrigVarIndex        ::  Indices of basic orig variables
 * @param splitsEffByHplane       ::  OUTPUT: Set of splits that the hyperplane activation affects;
 *                                ::          i.e., split s is here if the ray intersects the
 *                                ::          chosen hplane *before* the ray intersects the bd of split s.
 */
void findHplaneToIntersectRay(const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo,
    const chooseCutGeneratingSets& chooseSplits, const int numSplits,
    const int nonBasicColIndex, const vector<double> &basicCol,
    int &hplaneBasicVarIndex, int &hplaneBasicVarRowIndex,
    int &hplaneBasicVarUpperFlag, double &minSlackRatio,
    vector<int> &splitsEffByHplane, FILE *inst_info_out) {
  int numBasicSlacks = solnInfo.basicSlackVarIndex.size();
  int numBasicOrigVars = solnInfo.basicOrigVarIndex.size();
  const char* rowSense = solver.getRowSense();

  //  FILE* fptr = fopen("debugHplanes.csv","w");

  // Compute the distance of ray to intersect each split and also store the max and min distance
  // These two variables can be used to decide quickly if the hyperplane does not intersect any of the splits
  vector<double> splitSlackRatio(numSplits);
  double maxDistToSplit = -param.getINFINITY();
  //  cout << "nonBasicColIndex = " << nonBasicColIndex << "\n";

  // For each split, suppose split variable s is basic
  // in row i. Let j be the index of a non-basic col.
  // Then basicCol[chooseSplits.splitVarRowIndex[s]] = solnInfo.raysOfC1[j][i] = -\bar a_{ij},
  // adjusted as appropriate for non-basic cols at ub.
  //
  // If solnInfo.raysOfC1[j][i] > 0, then we will intersect pi_0 + 1.
  // If solnInfo.raysOfC1[j][i] < 0, then we will intersect pi_0.
  // If solnInfo.raysOfC1[j][i] = 0, then we will not intersect anything.
  // Note that \bar a_{ij} = basicCol[splitRowIndex_t[s]],
  // and \bar a_{i0} = solnInfo.primalSoln[chooseSplits.splitVarIndex[s]].
  // THIS IS HOW WE SET A0, BUT IN GENERAL, \bar a_{i0} \ne solnInfo.primalSoln[...]
  for (int s = 0; s < numSplits; s++) {
    // If intersecting pi_0 + 1, and currently we are at \bar a_{i0},
    // then (1/\alpha_j) = (pi_0 + 1 - \bar a_{i0})/(solnInfo.raysOfC1[j][i]).
    if (basicCol[chooseSplits.splitVarRowIndex[s]] >= param.getRAYEPS()) {
      splitSlackRatio[s] = (chooseSplits.pi0[s] + 1
          - solnInfo.primalSoln[chooseSplits.splitVarIndex[s]])
          / (basicCol[chooseSplits.splitVarRowIndex[s]]);
    } else {
      // Else, if we intersect pi_0, then
      //  (1/\alpha_j) = (pi_0 - \bar a_{i0})/(solnInfo.raysOfC1[j][i])
      if (basicCol[chooseSplits.splitVarRowIndex[s]] <= -param.getRAYEPS()) {
        splitSlackRatio[s] = (chooseSplits.pi0[s]
            - solnInfo.primalSoln[chooseSplits.splitVarIndex[s]])
            / (basicCol[chooseSplits.splitVarRowIndex[s]]);
      } else {
        // Else, ray does not intersect boundary of S,
        // so we put splitSlackRatio[s] = (1/\alpha_j) = \infty.
        // This means the coefficient \alpha_j will be 0 for this ray.
        splitSlackRatio[s] = param.getINFINITY();
      }
    }

    // Find maximum distance of any ray to the split.
    // Note that rays that do not intersect the split have distance infinity,
    // and this will be factored in here.
    if (splitSlackRatio[s] >= maxDistToSplit + param.getEPS()) {
      maxDistToSplit = splitSlackRatio[s];
    }
  }
  //  cout << "maxDistToSplit = " << maxDistToSplit << "\n";
  // What do these do?
  double maxViolGrad = 0.0, currSlackRatio = 0.0, rayGrad = 0.0;
  int constRowIndex = 0;

  //Initialize the hyperplane indices to -1. If the method terminates with -1,
  //this means that no hyperplane intersected the ray before the boundary of a split
  hplaneBasicVarIndex = -1;
  hplaneBasicVarRowIndex = -1;
  hplaneBasicVarUpperFlag = -2;

  //This is there to ensure that the chosen hyperplane intersects at least one ray before the boundary of S
  minSlackRatio = maxDistToSplit;
#ifdef TRACE
  printf("In findHplaneToIntersectRay: Initial minSR: %2.7lf.\n",
      minSlackRatio);
#endif

  //Check structural hyperplanes, that is hyperplanes corresponding to basic slack variables
  // (basic slack variable implies vertex is not on that hyperplane, unless it's basic at 0).
  // What we are checking is if the hyperplanes that
  // are not tight at the current solution intersect any rays.
  for (int h = 0; h < numBasicSlacks; h++) {
    // This is the index of the row corresponding to the slack variable. Let it be r.
    constRowIndex = solnInfo.basicSlackVarIndex[h] - solnInfo.numCols;

    //Equality constraints correspond to slack variables that are basic but have value zero
    //They clearly have zero distance from the optimal solution
    //Do we activate them?
    if (rowSense[constRowIndex] == 'E') {
      /*
       rayGrad = fabs(basicCol[solnInfo.basicSlackVarRowIndex[h]]);
       if (rayGrad >= param.getEPS()) {
       currSlackRatio = 0.0;
       if (currSlackRatio <= minSlackRatio - param.getEPS()) {
       minSlackRatio = currSlackRatio;
       maxViolGrad = rayGrad;
       hplaneBasicVarIndex = solnInfo.basicSlackVarIndex[h];
       hplaneBasicVarRowIndex = solnInfo.basicSlackVarRowIndex[h];
       hplaneBasicVarUpperFlag = -1;
       } else {
       if (currSlackRatio <= minSlackRatio + param.getEPS()
       && currSlackRatio >= minSlackRatio - param.getEPS()
       && rayGrad >= maxViolGrad + param.getEPS()) {
       minSlackRatio = currSlackRatio;
       maxViolGrad = rayGrad;
       hplaneBasicVarIndex = solnInfo.basicSlackVarIndex[h];
       hplaneBasicVarRowIndex = solnInfo.basicSlackVarRowIndex[h];
       hplaneBasicVarUpperFlag = -1;
       }
       }
       assert(currSlackRatio >= -param.getEPS());
       }
       */
    } else {
      //      hplaneDirn = -1.0;
      //      if (rowSense[constRowIndex] == 'G')
      //        hplaneDirn = 1.0;

      // If slack variable is basic in row s, and we are currently
      //  looking at ray j, then rayGrad = solnInfo.raysOfC1[j][s].
      rayGrad = basicCol[solnInfo.basicSlackVarRowIndex[h]];

      // If we go along the direction of ray j, do we ever hit
      // the hyperplane corresponding to constraint s?
      // This happens when the slack variable goes to zero. Thus,
      // \bar a_{s0} is the current solution, so we need to decrease
      // it to zero, which can only happen if rayGrad < 0. This obviously
      // assumes that the slack variables are all nonnegative (so that
      // an excess variable would be incorporated as a slack with a negative
      // one as its coefficient).
      if (rayGrad <= -param.getRAYEPS()) {
        // This does the same as the later assert, but gives more info.
        // Except this will return true more often than the later assert.
        // TODO: Can these slightly negative slacks cause issues?
        if (solnInfo.a0[constRowIndex] < -param.getEPS()) {
#ifdef TRACE
          printf(
              "*** ERROR: Slack variable %d basic in row %d has value %2.3f, which is negative.\n",
              h, solnInfo.basicSlackVarRowIndex[h],
              solnInfo.a0[solnInfo.basicSlackVarRowIndex[h]]);
#endif
//          writeErrorToII(
//              "*** ERROR: Negative slack found in findHplaneToIntersectRay.",
//              inst_info_out);
//          exit(1);
        }

        // TODO From Alex: Is this right?? Seems using two different rows possibly.
        // Solve \bar a_{r0} + currSR * solnInfo.raysOfC1[j][s] = 0.
        currSlackRatio = solnInfo.a0[constRowIndex]
            / (-basicCol[solnInfo.basicSlackVarRowIndex[h]]);

        // TODO From Alex; This is only because the above seems fishy.
        if (constRowIndex != solnInfo.basicSlackVarRowIndex[h]) {
          error_msg(errstr,
              "h: %d. constRowIndex: %d. solnInfo.basicSlackVarRowIndex[h]: %d.\n",
              h, constRowIndex, solnInfo.basicSlackVarRowIndex[h]);
          writeErrorToLog(errstr, inst_info_out);
          exit(1);
        }

        //        if (currSlackRatio <= -param.getEPS())
        //          printf("%d,%d,%f\n", solnInfo.basicSlackVarIndex[h],
        //              solnInfo.basicSlackVarRowIndex[h], currSlackRatio);
//        assert(currSlackRatio >= -param.getEPS());// This makes sure the slack variable was nonnegative to start with.
        if (currSlackRatio < -param.getEPS()) {
          char errorstring[300];
          snprintf(errorstring, sizeof(errorstring) / sizeof(char),
              "*** ERROR: In findHplaneToIntersectRay: currSR should not be negative, ever. It is %f.\n"
                  "\tconstRowIndex: %d\n"
                  "\ta0: %f\n"
                  "\tbasicSlackVarRowIndex: %d\n"
                  "\t-basicCol: %f\n"
                  "\trayGrad: %f.\n", currSlackRatio,
              constRowIndex, solnInfo.a0[constRowIndex],
              solnInfo.basicSlackVarRowIndex[h],
              -basicCol[solnInfo.basicSlackVarRowIndex[h]],
              rayGrad);
          cerr << errorstring << endl;
          writeErrorToLog(errorstring, inst_info_out);
          exit(1);
        }

#ifdef TRACE
        printf(
            "In findHplaneToIntersectRay: Hplane corresponding to row %d with name %s has SR: %2.7lf. Current min: %2.7lf.\n",
            constRowIndex, solver.getRowName(constRowIndex).c_str(),
            currSlackRatio, minSlackRatio);
#endif

        // If currSlackRatio < minSlackRatio, then we do not have to go as far
        // along ray j before intersecting the hyperplane. So then this becomes
        // the new hplane first intersected by this ray.
        if (currSlackRatio <= minSlackRatio - param.getEPS()) {
          minSlackRatio = currSlackRatio;
          hplaneBasicVarIndex = solnInfo.basicSlackVarIndex[h];
          hplaneBasicVarRowIndex = solnInfo.basicSlackVarRowIndex[h];
          hplaneBasicVarUpperFlag = -1;
          maxViolGrad = rayGrad;
        } else {
          // Otherwise, if the currSR is the same as the minSR, but the rayGrad
          // is simply a higher number (so that the effect of moving delta in
          // this direction is more than moving delta in the previous direction),
          // we also make this the new hyperplane corresopnding to this ray.
          if (currSlackRatio >= minSlackRatio - param.getEPS()
              && currSlackRatio <= minSlackRatio + param.getEPS()
              && rayGrad >= maxViolGrad + param.getEPS()) {
            minSlackRatio = currSlackRatio;
            hplaneBasicVarIndex = solnInfo.basicSlackVarIndex[h];
            hplaneBasicVarRowIndex =
                solnInfo.basicSlackVarRowIndex[h];
            hplaneBasicVarUpperFlag = -1;
            maxViolGrad = rayGrad;

          }
        }
      }
    }
  }

  const double* UB = solver.getColUpper();
  const double* LB = solver.getColLower();
  if (ACTIVATE_BASIC_BOUNDS) {
    // For each structural basic variable, see if the ray attains one of its bounds
    // before it intersects the hyperplane found before.
    for (int v = 0; v < (int) numBasicOrigVars; v++) {
      // If we are working with ray j and variable v is basic in row i
      // then rayGrad = \bar a_{ij}
      rayGrad = basicCol[solnInfo.basicOrigVarRowIndex[v]];
#ifdef TRACE
      printf("\n raysOfC1[j][k] for basic var %d, basic in row %d, is %f.\n",
          v, solnInfo.basicOrigVarRowIndex[v], rayGrad);
#endif

      // If rayGrad is positive (so that moving along ray j increases
      // the value of variable v) and v is bounded from above
      if (rayGrad >= param.getRAYEPS()
          && UB[solnInfo.basicOrigVarIndex[v]] <= param.getINFINITY()) {
        // How far until we hit the UB?
        // Solve \bar a_{i0} + currSR * rayGrad = UB.
        // currSR = (UB - \bar a_{ij}) / rayGrad.
        // If rayGrad >= 0, then the above quantity is non-negative.
        currSlackRatio = (UB[solnInfo.basicOrigVarIndex[v]]
            - solnInfo.primalSoln[solnInfo.basicOrigVarIndex[v]])
            / (basicCol[solnInfo.basicOrigVarRowIndex[v]]);
        if (currSlackRatio < -param.getEPS()) {
          char errorstring[300];
          snprintf(errorstring, sizeof(errorstring) / sizeof(char),
              "*** ERROR: In findHplaneToIntersectRay: currSR should not be negative, ever. It is %f.\n"
                  "\tv: %d\n"
                  "\tbasicOrigVarIndex[v]: %d\n"
                  "\tUB[solnInfo.basicOrigVarIndex[v]]: %f\n"
                  "\tprimalSoln[solnInfo.basicOrigVarIndex[v]]): %f\n"
                  "\tbasicOrigVarRowIndex: %d\n"
                  "\t-basicCol: %f\n"
                  "\trayGrad: %f.\n", currSlackRatio, v,
              solnInfo.basicOrigVarIndex[v],
              UB[solnInfo.basicOrigVarIndex[v]],
              solnInfo.primalSoln[solnInfo.basicOrigVarIndex[v]],
              solnInfo.basicOrigVarRowIndex[v],
              -basicCol[solnInfo.basicOrigVarRowIndex[v]],
              rayGrad);
          cerr << errorstring << endl;
          writeErrorToLog(errorstring, inst_info_out);
          exit(1);
        }

#ifdef TRACE
        printf(
            "In findHplaneToIntersectRay: Hplane corresponding to ub of var %d with name %s has SR: %2.7lf. Current min: %2.7lf.\n",
            solnInfo.basicOrigVarIndex[v],
            solver.getColName(solnInfo.basicOrigVarIndex[v]).c_str(),
            currSlackRatio, minSlackRatio);
#endif

        // If currSR improves current minSR, then we should actually activate
        // the hyperplane in which v is basic in.
        // In this case, we set the UB flag to be 1.
        if (currSlackRatio <= minSlackRatio - param.getEPS()) {
          minSlackRatio = currSlackRatio;
          hplaneBasicVarIndex = solnInfo.basicOrigVarIndex[v];
          hplaneBasicVarRowIndex = solnInfo.basicOrigVarRowIndex[v];
          hplaneBasicVarUpperFlag = 1;
          maxViolGrad = rayGrad;
        } else {
          // Similarly to before, if currSR is same as minSR,
          // but the gradient is sharper, use this one instead.
          if (currSlackRatio >= minSlackRatio - param.getEPS()
              && currSlackRatio <= minSlackRatio + param.getEPS()
              && rayGrad >= maxViolGrad + param.getEPS()) {
            minSlackRatio = currSlackRatio;
            hplaneBasicVarIndex = solnInfo.basicOrigVarIndex[v];
            hplaneBasicVarRowIndex =
                solnInfo.basicOrigVarRowIndex[v];
            hplaneBasicVarUpperFlag = 1;
            maxViolGrad = rayGrad;
          }
        }
      } else {
        // Otherwise, if rayGrad is negative and variable v has a lower-bound
        if (rayGrad <= -param.getRAYEPS()
            && LB[solnInfo.basicOrigVarIndex[v]] >= -param.getINFINITY()) {
          // \bar a_{i0} + currSR * rayGrad = LB
          currSlackRatio =
              (LB[solnInfo.basicOrigVarIndex[v]]
                  - solnInfo.primalSoln[solnInfo.basicOrigVarIndex[v]])
                  / (basicCol[solnInfo.basicOrigVarRowIndex[v]]);
          if (currSlackRatio < -param.getEPS()) {
            char errorstring[300];
            snprintf(errorstring,
                sizeof(errorstring) / sizeof(char),
                "*** ERROR: In findHplaneToIntersectRay: currSR should not be negative, ever. It is %f.\n"
                    "\tv: %d\n"
                    "\tbasicOrigVarIndex[v]: %d\n"
                    "\tLB[solnInfo.basicOrigVarIndex[v]]: %f\n"
                    "\tprimalSoln[solnInfo.basicOrigVarIndex[v]]): %f\n"
                    "\tbasicOrigVarRowIndex: %d\n"
                    "\t-basicCol: %f\n"
                    "\trayGrad: %f.\n", currSlackRatio, v,
                solnInfo.basicOrigVarIndex[v],
                LB[solnInfo.basicOrigVarIndex[v]],
                solnInfo.primalSoln[solnInfo.basicOrigVarIndex[v]],
                solnInfo.basicOrigVarRowIndex[v],
                -basicCol[solnInfo.basicOrigVarRowIndex[v]],
                rayGrad);
            cerr << errorstring << endl;
            writeErrorToLog(errorstring, inst_info_out);
            exit(1);
          }

//        printf("\tv: %d.\n", v);
//        printf("\trayGrad: %f.\n", rayGrad);
//        printf("\tbasicOrigVarIndex: %d.\n", solnInfo.basicOrigVarIndex[v]);
//        printf("\tLB: %f.\n", LB[solnInfo.basicOrigVarIndex[v]]);
//        printf("\tvar val: %f.\n",
//            solnInfo.primalSoln[solnInfo.basicOrigVarIndex[v]]);
//        printf("\traysOfC1 value in this row: %f.\n",
//            basicCol[solnInfo.basicOrigVarRowIndex[v]]);

#ifdef TRACE
          printf(
              "In findHplaneToIntersectRay: Hplane corresponding to lb of var %d with name %s has SR: %2.7lf. Current min: %2.7lf.\n",
              solnInfo.basicOrigVarIndex[v],
              solver.getColName(solnInfo.basicOrigVarIndex[v]).c_str(),
              currSlackRatio, minSlackRatio);
#endif

          // Again, if this LB intersected first, make it the first hplane.
          if (currSlackRatio <= minSlackRatio - param.getEPS()) {
            minSlackRatio = currSlackRatio;
            hplaneBasicVarIndex = solnInfo.basicOrigVarIndex[v];
            hplaneBasicVarRowIndex =
                solnInfo.basicOrigVarRowIndex[v];
            hplaneBasicVarUpperFlag = 0;
            maxViolGrad = rayGrad;
          } else {
            // Again, if it's intersected at the same time but has a higher
            // gradient, make it the one.
            if (currSlackRatio >= minSlackRatio - param.getEPS()
                && currSlackRatio <= minSlackRatio + param.getEPS()
                && rayGrad >= maxViolGrad + param.getEPS()) {
              minSlackRatio = currSlackRatio;
              hplaneBasicVarIndex = solnInfo.basicOrigVarIndex[v];
              hplaneBasicVarRowIndex =
                  solnInfo.basicOrigVarRowIndex[v];
              hplaneBasicVarUpperFlag = 0;
              maxViolGrad = rayGrad;
            }
          }
        }
      }
    }
  }

  if (ACTIVATE_NB_BOUNDS) {
    // Check if the ray intersects its other bound first
    if (nonBasicColIndex < solnInfo.numCols) {
      // Clearly, this can happen only for the variables
      // that have both bounds (cannot happen for slacks)
      if ((UB[nonBasicColIndex] <= param.getINFINITY())
          && (LB[nonBasicColIndex] >= -param.getINFINITY())) {
        const double distToOtherBound = UB[nonBasicColIndex]
            - LB[nonBasicColIndex];
        if (minSlackRatio >= distToOtherBound - param.getEPS()) {
          if ((minSlackRatio >= distToOtherBound + param.getEPS())
              || (maxViolGrad <= 1.0 - param.getEPS())) {
#ifdef TRACE
            printf(
                "In findHplaneToIntersectRay: Hplane corresponding to bound of ray is chosen.\n");
#endif
            minSlackRatio = distToOtherBound;
            hplaneBasicVarIndex = nonBasicColIndex;
            hplaneBasicVarRowIndex = -1;
            if (solnInfo.cstat[nonBasicColIndex] == 2)
              hplaneBasicVarUpperFlag = 0;
            else
              hplaneBasicVarUpperFlag = 1;
            maxViolGrad = 1.0;
          }
        }
      }
    }
  }

  //Store the splits affected by the hyperplane activation
  for (int s = 0; s < numSplits; s++) {
    // That is, if minSR <= splitSR[s], then the intersection
    // of the ray with the hyperplane happens before the ray
    // intersects the boundary of split s.
    if (!CUT_PARALLEL_RAYS
        && std::abs(splitSlackRatio[s]) >= param.getINFINITY() - param.getEPS())
      continue;
    if (minSlackRatio <= splitSlackRatio[s] - param.getEPS()) {
      splitsEffByHplane.push_back(s);
    }
  }
//  assert(minSlackRatio >= -param.getEPS());
  if (minSlackRatio < -param.getEPS()) {
    error_msg(errorstring,
        "In findHplaneToIntersectRay: minSR should not be negative, ever. It is %f.\n",
        minSlackRatio);
    writeErrorToLog(errorstring, inst_info_out);
    exit(1);
  }

}

/**
 * @brief Finds the first hyperplane to intersect a ray, activating those that cut the fewest of the
 * rays already cut. Returns -1 if no hyperplane intersects the  * ray before the bdS over all chosen splits.
 *
 * Briefly, this finds the first hyperplane to intersect a ray, activating those that cut the fewest rays of
 * those that have already been cut. The rationale for this is that every time a
 * new hyperplane cuts a previously intersected ray, this may create potentially shallower intersection
 * points. Vertices created from rays that are intersected more than once are named “weak vertices”.
 * The idea is to account for the number of weak vertices created when activating the new hyperplane.
 * That is, if hyperplane heuristic 1 chooses the hyperplane that intersects a ray first, in hyperplane
 * heuristic 2, we realize this may end up creating many shallow points emanating from weak vertices
 * on previously intersected ray directions, so we try to minimize the number of such weak vertices
 * created. The procedure is:
 *
 *  1. Compute (1/αj ) for each split and the given ray j. This is the multiplier such that
 *      \bar a_{i0} - (1/\alpha_j) \bar a_{ij} = chooseSplits.pi0 or chooseSplits.pi0 + 1.
 *  2. Compute the maximum number of rays that have already been cut (by previously activated
 *      hyperplanes) before they reach the boundary of split s, where the maximum is taken over all
 *      splits being considered. Call this quantity C.
 *  3. For each original constraint, see if ray j intersects it. Store the first one we intersect. However,
 *      we only consider those constraints that create fewer than C weak vertices (on rays that have
 *      already been cut by previously activated hyperplanes) in each split. If no original constraints
 *      are intersected by the ray, then the program is unbounded in this direction, other than bounds
 *      found in the next step.
 *  4. For each bound on a basic original variable, check if the bound is actually intersected prior to
 *      the hyperplane found before. If it is, this is the new hyperplane first intersected. Again, we
 *      only consider those bounds that themselves cut fewer than C rays (of those rays that have
 *      already been cut) for each split.
 *  5. Check if one of the bounds of the ray is intersected first.
 *
 * @param solver                  ::  Solver from which we are generating cut
 * @param solnInfo.numcols              ::  Initial number of variables
 * @param cstat                   ::  0 free, 1 basic, 2 upper, 3 lower
 * @param numSplits               ::  Number effective cut-generating sets
 * @param nonBasicColIndex          ::  Variable index of the ray we are currently checking
 * @param JIndex                  ::  Index in the array of non-basic columns of this ray
 * @param basicCol                ::  Column \bar a^j corresponding to the ray
 * @param solnInfo.a0                     ::  Solution vector ordered by original rows
 * @param chooseSplits.pi0                      ::  Floor of solnInfo.a0 for the split variables
 * @param chooseSplits.splitVarRowIndex         ::  Row in which the split var is basic
 * @param chooseSplits.splitVarIndex            ::  Index of the split variable
 * @param hplaneBasicVarIndex     ::  OUTPUT: Index of basic var in constr chosen
 * @param hplaneBasicVarRowIndex    ::  OUTPUT: Row index of constraint chosen
 * @param hplaneBasicVarUpperFlag ::  OUTPUT: -1 (not a bound), 0 (a LB), or 1 (an UB)
 * @param minSlackRatio           ::  OUTPUT: How far along the ray we go before intersecting ray
 * @param numBasicSlacks          ::  Number of basic slack variables
 * @param solnInfo.basicSlackVarIndex     ::  Indices of basic slack variables
 * @param solnInfo.basicSlackVarRowIndex    ::  Row in which the slack var is basic
 * @param numBasicOrigVars        ::  Number of basic original variables
 * @param solnInfo.basicOrigVarRowIndex   ::  Row in which basic orig var is basic
 * @param solnInfo.basicOrigVarIndex        ::  Indices of basic orig variables
 * @param nonBasicVarIndices      ::  Indices of the non-basic variables
 * @param splitsEffByHplane       ::  OUTPUT: Set of splits that the hyperplane activation affects;
 *                                ::          i.e., split s is here if the ray intersects the
 *                                ::          chosen hplane *before* the ray intersects the bd of split s.
 * @param raysToBeCut             ::  [split][ray index], stores all rays to be cut by at least one hplane in this split
 * @param rasyOfC1                ::  Rays of the original simplicial cone
 * @param chosenHplanes           ::  For each split, which hplanes were activated
 * @param numRaysCutByHplane      ::  For each hyperplane, how many of the possible rays are cut
 * @param activatedUBFlag         ::
 * findHplaneMinWeakVert(solver, solnInfo, chooseSplits, numSplits,
 nonBasicColIndex, sortIndex[j], solnInfo.raysOfC1[sortIndex[j]],
 hplaneBasicVarIndex, hplaneBasicVarRowIndex, hplaneBasicVarUpperFlag,
 minSlackRatio, splitsEffByHplane, raysToBeCut, chosenHplanes,
 numRaysCutByHplane, activatedHplaneUBVect)
 */
void findHplaneMinWeakVert(const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo,
    const chooseCutGeneratingSets& chooseSplits, const int numSplits,
    const int nonBasicColIndex, const int JIndex,
    const vector<double> &basicCol, int &hplaneBasicVarIndex,
    int &hplaneBasicVarRowIndex, int &hplaneBasicVarUpperFlag,
    double &minSlackRatio, vector<int> &splitsEffByHplane,
    vector<vector<int> > &raysToBeCut, vector<vector<bool> > &chosenHplanes,
    vector<vector<int> > &numRaysCutByHplane,
    vector<vector<int> > &activatedUBFlag, FILE *inst_info_out) {
  int numBasicSlacks = solnInfo.basicSlackVarIndex.size();
  int numBasicOrigVars = solnInfo.basicOrigVarIndex.size();
  const double* UB = solver.getColUpper();
  const double* LB = solver.getColLower();
  const char* rowSense = solver.getRowSense();

  //Compute the distance of ray to intersect each split and also store the max and min distance
  //These two variables can be used to decide quickly if the hyperplane does not intersect any of the splits
  vector<double> splitSlackRatio(numSplits);
  double maxDistToSplit = -param.getINFINITY();
  int maxNumRays = -1;// This keeps the maximum number of rays that are cut by the hyperplane?

  // For each split, suppose split variable s is basic
  // in row i. Let j be the index of a non-basic col.
  // Then basicCol[chooseSplits.splitVarRowIndex[s]] = solnInfo.raysOfC1[j][i] = -\bar a_{ij},
  // adjusted as appropriate for non-basic cols at ub.
  //
  // If solnInfo.raysOfC1[j][i] > 0, then we will intersect pi_0 + 1.
  // If solnInfo.raysOfC1[j][i] < 0, then we will intersect pi_0.
  // If solnInfo.raysOfC1[j][i] = 0, then we will not intersect anything.
  // Note that \bar a_{ij} = basicCol[splitRowIndex_t[s]],
  // and \bar a_{i0} = solnInfo.primalSoln[chooseSplits.splitVarIndex[s]].
  for (int s = 0; s < numSplits; s++) {
    // Finds the maximum number of rays cut by hyperplanes over all splits
    if ((int) raysToBeCut[s].size() > maxNumRays)
      maxNumRays = raysToBeCut[s].size();

    // If intersecting pi_0 + 1, and currently we are at \bar a_{i0},
    // then (1/\alpha_j) = (pi_0 + 1 - \bar a_{i0})/(solnInfo.raysOfC1[j][i]).
    if (basicCol[chooseSplits.splitVarRowIndex[s]] >= param.getRAYEPS()) {
      splitSlackRatio[s] = (chooseSplits.pi0[s] + 1
          - solnInfo.primalSoln[chooseSplits.splitVarIndex[s]])
          / (basicCol[chooseSplits.splitVarRowIndex[s]]);
    } else {
      // Else, if we intersect pi_0, then
      //  (1/\alpha_j) = (pi_0 - \bar a_{i0})/(solnInfo.raysOfC1[j][i])
      if (basicCol[chooseSplits.splitVarRowIndex[s]] <= -param.getRAYEPS()) {
        splitSlackRatio[s] = (chooseSplits.pi0[s]
            - solnInfo.primalSoln[chooseSplits.splitVarIndex[s]])
            / (basicCol[chooseSplits.splitVarRowIndex[s]]);
      } else {
        // Else, ray does not intersect boundary of S,
        // so we put splitSlackRatio[s] = (1/\alpha_j) = \infty.
        // This means the coefficient \alpha_j will be 0 for this ray.
        splitSlackRatio[s] = param.getINFINITY();
#ifdef TRACE
        printf("Error: Ray does not intersect the bdS\n");
#endif
        //        exit(1);
      }
    }

    // Find maximum distance of any ray to the split.
    // Note that rays that do not intersect the split have distance infinity,
    // and this will be factored in here.
    if (splitSlackRatio[s] >= maxDistToSplit + param.getEPS()) {
      maxDistToSplit = splitSlackRatio[s];
    }
  }

  double maxViolGrad = 0.0, currSlackRatio = 0.0, rayGrad = 0.0;
  int constRowIndex = 0;

  //Initialize the hyperplane indices to -1. If the method terminates with -1,
  //this means that no hyperplane intersected the ray before the boundary of a split
  hplaneBasicVarIndex = -1;
  hplaneBasicVarRowIndex = -1;
  hplaneBasicVarUpperFlag = -2;
  vector<int> raysCutByHplane;
  //This is there to ensure that the chosen hyperplane intersects at least one ray before the boundary of S
  minSlackRatio = maxDistToSplit;

  // The minimum number of rays intersected is the current largest number of rays cut over all splits
  int minRaysIntersected = maxNumRays;

  //Check structural hyperplanes, that is hyperplanes corresponding to basic slack variables
  // (basic slack variable implies vertex is not on that hyperplane, unless it's basic at 0).
  // What we are checking is if the hyperplanes that
  // are not tight at the current solution intersect any rays.
  for (int h = 0; h < numBasicSlacks; h++) {
    // This is the index of the row corresponding to the slack variable. Let it be r.
    constRowIndex = solnInfo.basicSlackVarIndex[h] - solnInfo.numCols;

    //Equality constraints correspond to slack variables that are basic but have value zero
    //They clearly have zero distance from the optimal solution
    //Do we activate them?
    if (rowSense[constRowIndex] == 'E') {
    } else {
      //      hplaneDirn = -1.0;
      //      if (rowSense[constRowIndex] == 'G')
      //        hplaneDirn = 1.0;

      // If slack variable is basic in row s, and we are currently
      //  looking at ray j, then rayGrad = solnInfo.raysOfC1[j][s].
      rayGrad = basicCol[solnInfo.basicSlackVarRowIndex[h]];

      // If we go along the direction of ray j, do we ever hit
      // the hyperplane corresponding to constraint s?
      // This happens when the slack variable goes to zero. Thus,
      // \bar a_{s0} is the current solution, so we need to decrease
      // it to zero, which can only happen if rayGrad < 0. This obviously
      // assumes that the slack variables are all nonnegative (so that
      // an excess variable would be incorporated as a slack with a negative
      // one as its coefficient).
      if (rayGrad <= -param.getRAYEPS()) {
        // This does the same as the later assert, but gives more info.
        // Except this will return true more often than the later assert.
        // TODO: Can these slightly negative slacks cause issues?
        if (solnInfo.a0[constRowIndex] < -param.getEPS()) {
#ifdef TRACE
          printf(
              "*** ERROR: Slack variable %d basic in row %d has value %2.3f, which is negative.\n",
              h, solnInfo.basicSlackVarRowIndex[h],
              solnInfo.a0[solnInfo.basicSlackVarRowIndex[h]]);
#endif
        }

        // TODO From Alex: Is this right?? Seems using two different rows possibly.
        // Solve \bar a_{r0} + currSR * solnInfo.raysOfC1[j][s] = 0.
        currSlackRatio = solnInfo.a0[constRowIndex]
            / (-basicCol[solnInfo.basicSlackVarRowIndex[h]]);

        if (currSlackRatio < -param.getEPS()) {
          error_msg(errorstring,
              "In findHplaneMinWeakVert: currSR should not be negative, ever. It is %f.\n" "\tconstRowIndex: %d\n" "\ta0: %f\n" "\tbasicSlackVarRowIndex: %d\n" "\t-basicCol: %f\n" "\trayGrad: %f.\n",
              currSlackRatio, constRowIndex,
              solnInfo.a0[constRowIndex],
              solnInfo.basicSlackVarRowIndex[h],
              -basicCol[solnInfo.basicSlackVarRowIndex[h]],
              rayGrad);
          writeErrorToLog(errorstring, inst_info_out);
          exit(1);
        }

        // We find the most number of rays, of those cut previously per split, that are
        // again cut by the hyperplane
        maxNumRays = -1;
        for (int s = 0; s < numSplits; s++) {
          raysCutByHplane.clear();

          // For this hyperplane, in this split, what are the rays that are cut
          // before hitting the boundary of the split?
          // This is of those rays that were already previously cut.
          addRaysCutByHplaneBeforeBdS(solver, solnInfo,
              chooseSplits.splitVarIndex[s],
              chooseSplits.splitVarRowIndex[s],
              chooseSplits.pi0[s], solnInfo.basicSlackVarIndex[h],
              solnInfo.basicSlackVarRowIndex[h], -1,
              raysToBeCut[s], raysCutByHplane, JIndex,
              inst_info_out);

          // If this hyperplane was chosen ...
          if (chosenHplanes[s][solnInfo.basicSlackVarIndex[h]]) {
            // ... Let t be the number of rays cut by the hplane
            // If t - numRaysCuByHplane[split][var] > maxNumRays, reset that quantity
            if ((int) (raysCutByHplane.size()
                - numRaysCutByHplane[s][solnInfo.basicSlackVarIndex[h]])
                > maxNumRays)
              maxNumRays =
                  raysCutByHplane.size()
                      - numRaysCutByHplane[s][solnInfo.basicSlackVarIndex[h]];
          } else {
            // Otherwise, this hyperplane was not chosen, and only if
            // the number of rays cut by the hyperplane is more than he current max, reset
            if ((int) raysCutByHplane.size() > maxNumRays)
              maxNumRays = raysCutByHplane.size();
          }
        }

        // If we intersect fewer than minRaysIntersected
        // (which checks what?) TODO?
        // and the currSR is smaller than the maxDistToSplit (i.e.,
        // we do not have to go as far to reach the hplane),
        // then update to be this hplane.
        if ((maxNumRays < minRaysIntersected)
            && (currSlackRatio <= maxDistToSplit - param.getEPS())) {
          minSlackRatio = currSlackRatio;
          hplaneBasicVarIndex = solnInfo.basicSlackVarIndex[h];
          hplaneBasicVarRowIndex = solnInfo.basicSlackVarRowIndex[h];
          hplaneBasicVarUpperFlag = -1;
          maxViolGrad = rayGrad;
        }

        if (maxNumRays == minRaysIntersected) {
          // Same as above, but also if same number of rays, not just fewer
          if (currSlackRatio <= minSlackRatio - param.getEPS()) {
            minSlackRatio = currSlackRatio;
            hplaneBasicVarIndex = solnInfo.basicSlackVarIndex[h];
            hplaneBasicVarRowIndex =
                solnInfo.basicSlackVarRowIndex[h];
            hplaneBasicVarUpperFlag = -1;
            maxViolGrad = rayGrad;
          } else {
            // Else, if the hplane chosen before and the current one are the same
            // length away along the ray, but the gradient is higher, use that one.
            if (currSlackRatio >= minSlackRatio - param.getEPS()
                && currSlackRatio <= minSlackRatio + param.getEPS()
                && rayGrad >= maxViolGrad + param.getEPS()) {
              minSlackRatio = currSlackRatio;
              hplaneBasicVarIndex =
                  solnInfo.basicSlackVarIndex[h];
              hplaneBasicVarRowIndex =
                  solnInfo.basicSlackVarRowIndex[h];
              hplaneBasicVarUpperFlag = -1;
              maxViolGrad = rayGrad;

            }
          }
        }
      }

    }
  }

  for (int v = 0; v < (int) numBasicOrigVars; v++) {
    rayGrad = basicCol[solnInfo.basicOrigVarRowIndex[v]];
    if (rayGrad >= param.getRAYEPS() && UB[solnInfo.basicOrigVarIndex[v]] <= param.getINFINITY()) {
      currSlackRatio = (UB[solnInfo.basicOrigVarIndex[v]]
          - solnInfo.primalSoln[solnInfo.basicOrigVarIndex[v]])
          / (basicCol[solnInfo.basicOrigVarRowIndex[v]]);
      if (currSlackRatio < -param.getEPS()) {
        error_msg(errstr,
            "currSR should not be negative, ever. It is %f.\n" "\tv: %d\n" "\tbasicOrigVarIndex[v]: %d\n" "\tUB[solnInfo.basicOrigVarIndex[v]]: %f\n" "\tprimalSoln[solnInfo.basicOrigVarIndex[v]]): %f\n" "\tbasicOrigVarRowIndex: %d\n" "\t-basicCol: %f\n" "\trayGrad: %f.\n",
            currSlackRatio, v, solnInfo.basicOrigVarIndex[v],
            UB[solnInfo.basicOrigVarIndex[v]],
            solnInfo.primalSoln[solnInfo.basicOrigVarIndex[v]],
            solnInfo.basicOrigVarRowIndex[v],
            -basicCol[solnInfo.basicOrigVarRowIndex[v]], rayGrad);
        writeErrorToLog(errstr, inst_info_out);
        exit(1);
      }
      maxNumRays = -1;
      for (int s = 0; s < numSplits; s++) {
        raysCutByHplane.clear();
        addRaysCutByHplaneBeforeBdS(solver, solnInfo,
            chooseSplits.splitVarIndex[s],
            chooseSplits.splitVarRowIndex[s], chooseSplits.pi0[s],
            solnInfo.basicOrigVarIndex[v],
            solnInfo.basicOrigVarRowIndex[v], 1, raysToBeCut[s],
            raysCutByHplane, JIndex, inst_info_out);
        if (chosenHplanes[s][solnInfo.basicOrigVarIndex[v]]
            && activatedUBFlag[s][solnInfo.basicOrigVarIndex[v]]
                == 1) {
          if ((int) (raysCutByHplane.size()
              - numRaysCutByHplane[s][solnInfo.basicOrigVarIndex[v]])
              > maxNumRays)
            maxNumRays =
                raysCutByHplane.size()
                    - numRaysCutByHplane[s][solnInfo.basicOrigVarIndex[v]];
        } else {
          if ((int) raysCutByHplane.size() > maxNumRays)
            maxNumRays = raysCutByHplane.size();
        }
      }

      if ((maxNumRays < minRaysIntersected)
          && (currSlackRatio <= maxDistToSplit - param.getEPS())) {

        minSlackRatio = currSlackRatio;
        hplaneBasicVarIndex = solnInfo.basicOrigVarIndex[v];
        hplaneBasicVarRowIndex = solnInfo.basicOrigVarRowIndex[v];
        hplaneBasicVarUpperFlag = 1;
        maxViolGrad = rayGrad;
      }

      if (maxNumRays == minRaysIntersected) {
//        assert(currSlackRatio >= -param.getEPS());
        if (currSlackRatio <= minSlackRatio - param.getEPS()) {
          minSlackRatio = currSlackRatio;
          hplaneBasicVarIndex = solnInfo.basicOrigVarIndex[v];
          hplaneBasicVarRowIndex = solnInfo.basicOrigVarRowIndex[v];
          hplaneBasicVarUpperFlag = 1;
          maxViolGrad = rayGrad;
        } else {
          if (currSlackRatio >= minSlackRatio - param.getEPS()
              && currSlackRatio <= minSlackRatio + param.getEPS()
              && rayGrad >= maxViolGrad + param.getEPS()) {
            minSlackRatio = currSlackRatio;
            hplaneBasicVarIndex = solnInfo.basicOrigVarIndex[v];
            hplaneBasicVarRowIndex =
                solnInfo.basicOrigVarRowIndex[v];
            hplaneBasicVarUpperFlag = 1;
            maxViolGrad = rayGrad;
          }
        }
      }
    } else {
      if (rayGrad <= -param.getRAYEPS()
          && LB[solnInfo.basicOrigVarIndex[v]] >= -param.getINFINITY()) {
        currSlackRatio = (LB[solnInfo.basicOrigVarIndex[v]]
            - solnInfo.primalSoln[solnInfo.basicOrigVarIndex[v]])
            / (basicCol[solnInfo.basicOrigVarRowIndex[v]]);
        maxNumRays = -1;
        for (int s = 0; s < numSplits; s++) {
          raysCutByHplane.clear();
          addRaysCutByHplaneBeforeBdS(solver, solnInfo,
              chooseSplits.splitVarIndex[s],
              chooseSplits.splitVarRowIndex[s],
              chooseSplits.pi0[s], solnInfo.basicOrigVarIndex[v],
              solnInfo.basicOrigVarRowIndex[v], 0, raysToBeCut[s],
              raysCutByHplane, JIndex, inst_info_out);
          if (chosenHplanes[s][solnInfo.basicOrigVarIndex[v]]
              && activatedUBFlag[s][solnInfo.basicOrigVarIndex[v]]
                  == 0) {
            if ((int) (raysCutByHplane.size()
                - numRaysCutByHplane[s][solnInfo.basicOrigVarIndex[v]])
                > maxNumRays)
              maxNumRays =
                  raysCutByHplane.size()
                      - numRaysCutByHplane[s][solnInfo.basicOrigVarIndex[v]];
          } else {
            if ((int) raysCutByHplane.size() > maxNumRays)
              maxNumRays = raysCutByHplane.size();
          }
        }

        if ((maxNumRays < minRaysIntersected)
            && (currSlackRatio <= maxDistToSplit - param.getEPS())) {
          minSlackRatio = currSlackRatio;
          hplaneBasicVarIndex = solnInfo.basicOrigVarIndex[v];
          hplaneBasicVarRowIndex = solnInfo.basicOrigVarRowIndex[v];
          hplaneBasicVarUpperFlag = 0;
          maxViolGrad = rayGrad;
        }

        if (maxNumRays == minRaysIntersected) {
//          assert(currSlackRatio >= -param.getEPS());
          if (currSlackRatio < -param.getEPS()) {
            error_msg(errstr,
                "currSR should not be negative, ever. It is %f.\n" "\tv: %d\n" "\tbasicOrigVarIndex[v]: %d\n" "\tLB[solnInfo.basicOrigVarIndex[v]]: %f\n" "\tprimalSoln[solnInfo.basicOrigVarIndex[v]]): %f\n" "\tbasicOrigVarRowIndex: %d\n" "\t-basicCol: %f\n" "\trayGrad: %f.\n",
                currSlackRatio, v,
                solnInfo.basicOrigVarIndex[v],
                LB[solnInfo.basicOrigVarIndex[v]],
                solnInfo.primalSoln[solnInfo.basicOrigVarIndex[v]],
                solnInfo.basicOrigVarRowIndex[v],
                -basicCol[solnInfo.basicOrigVarRowIndex[v]],
                rayGrad);
            writeErrorToLog(errstr, inst_info_out);
            exit(1);
          }
          if (currSlackRatio <= minSlackRatio - param.getEPS()) {
            minSlackRatio = currSlackRatio;
            hplaneBasicVarIndex = solnInfo.basicOrigVarIndex[v];
            hplaneBasicVarRowIndex =
                solnInfo.basicOrigVarRowIndex[v];
            hplaneBasicVarUpperFlag = 0;
            maxViolGrad = rayGrad;
          } else {
            if (currSlackRatio >= minSlackRatio - param.getEPS()
                && currSlackRatio <= minSlackRatio + param.getEPS()
                && rayGrad >= maxViolGrad + param.getEPS()) {
              minSlackRatio = currSlackRatio;
              hplaneBasicVarIndex = solnInfo.basicOrigVarIndex[v];
              hplaneBasicVarRowIndex =
                  solnInfo.basicOrigVarRowIndex[v];
              hplaneBasicVarUpperFlag = 0;
              maxViolGrad = rayGrad;
            }
          }
        }
      }
    }
  }

  //Check if one of the bounds of the non-basic column intersects the ray first
  if (nonBasicColIndex < solnInfo.numCols) {
    if (minSlackRatio >= 1.0 - param.getEPS()) {
      if ((UB[nonBasicColIndex] <= param.getINFINITY())
          && (LB[nonBasicColIndex] >= -param.getINFINITY())) {
        if ((minSlackRatio >= 1.0 + param.getEPS()) || maxViolGrad <= 1.0 - param.getEPS()) {
          minSlackRatio = 1.0;
          hplaneBasicVarIndex = nonBasicColIndex;
          hplaneBasicVarRowIndex = -1;
          if (solnInfo.cstat[nonBasicColIndex] == 2)
            hplaneBasicVarUpperFlag = 0;
          else
            hplaneBasicVarUpperFlag = 1;
          maxViolGrad = 1.0;
        }
      }
    }
  }
  //  fclose(fptr);

  //Store the splits affected by the hyperplane activation
  for (int s = 0; s < numSplits; s++) {
    if (minSlackRatio <= splitSlackRatio[s] - param.getEPS())
      splitsEffByHplane.push_back(s);
  }

  if (minSlackRatio < -param.getEPS()) {
    error_msg(errstr,
        "*** ERROR: In findHplaneMinWeakVert: minSR should not be negative, ever. It is %f.\n",
        minSlackRatio);
    writeErrorToLog(errstr, inst_info_out);
    exit(1);
  }
} /* */

/**
 * @brief This method checks which rays in raysToBeCut are intersected by the hyperplane hplaneIndex before the bdS,
 * and adds them to raysCutByHplane
 *
 * @param solver              ::
 * @param solnInfo.numcols              ::
 * @param chooseSplits.splitVarIndex        ::
 * @param splitRowIndex       ::
 * @param chooseSplits.pi0                  ::
 * @param hplaneIndex         ::  Index of the basic var corresponding to the hplane chosen
 * @param hplaneRowIndex        ::  Row of the constraint chosen for the hplane
 * @param hplaneUBFlag        ::  1 if u.b. constraint, 0 if l.b. constraint, -1 otherwise
 * @param nonBasicVarIndices  ::
 * @param raysToBeCut         ::  Indices of all rays (0 to numNonbasicCols-1).
 * @param raysCutByHplane     ::  OUTPUT: Rays that are actually cut by the chosen hplane for this split.
 * @param solnInfo.raysOfC1           ::
 * @param solnInfo.a0                 ::
 * @param rayIndex              ::
 *
 addRaysCutByHplaneBeforeBdS(solver, solnInfo,
 chooseSplits.splitVarIndex[tmpSplitIndex],
 chooseSplits.splitVarRowIndex[tmpSplitIndex],
 chooseSplits.pi0[tmpSplitIndex], hplaneBasicVarIndex,
 hplaneBasicVarRowIndex, hplaneBasicVarUpperFlag, AllRayIndices,
 allRaysCutByHplane[tmpSplitIndex][tmpSize1], sortIndex[j]);
 */
void addRaysCutByHplaneBeforeBdS(const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo, const int splitVarIndex,
    const int splitRowIndex, const double pi0, const int hplaneIndex,
    const int hplaneRowIndex, const int hplaneUBFlag,
    const vector<int>& raysToBeCut, vector<int> &raysCutByHplane,
    const int rayIndex, FILE* inst_info_out) {

//  int nonBasicColIndex;
  double rayGrad, splitSlackRatio, hplaneRatio = 0.0;
  const double* UB = solver.getColUpper();
  const double* LB = solver.getColLower();
  const char* rowSense = solver.getRowSense();

  string hplaneName = "";
  if (hplaneIndex < solnInfo.numCols)
    hplaneName = solver.getColName(hplaneIndex);
  else
    hplaneName = solver.getRowName(hplaneIndex - solnInfo.numCols);

  // If there exists a hyperplane that intersects the ray before bd S for this split
  if (hplaneRowIndex != -1) {
    for (unsigned j = 0; j < raysToBeCut.size(); j++) {
//      nonBasicColIndex = nonBasicVarIndices[raysToBeCut[j]];
      rayGrad = solnInfo.raysOfC1[raysToBeCut[j]][splitRowIndex];
      //Compute slack ratio
      if (rayGrad >= param.getEPS()) {
        splitSlackRatio = (pi0 + 1 - solnInfo.primalSoln[splitVarIndex])
            / (rayGrad);
      } else {
        if (rayGrad <= -param.getEPS()) {
          splitSlackRatio = (pi0 - solnInfo.primalSoln[splitVarIndex])
              / (rayGrad);
        } else {
          splitSlackRatio = param.getINFINITY();
        }
      }
      if (splitSlackRatio < -param.getEPS()) {
        error_msg(errstr,
            "splitSlackRatio should not be negative, ever. It is %f.\n",
            splitSlackRatio);
        writeErrorToLog(errstr, inst_info_out);
        exit(1);
      }

      string rayName = "";
      if (solnInfo.nonBasicVarIndex[raysToBeCut[j]] < solnInfo.numCols)
        rayName = solver.getColName(
            solnInfo.nonBasicVarIndex[raysToBeCut[j]]);
      else
        rayName = solver.getRowName(
            solnInfo.nonBasicVarIndex[raysToBeCut[j]]
                - solnInfo.numCols);

      //Compute hplane ratio
      if (hplaneIndex < solnInfo.numCols) {
        rayGrad = solnInfo.raysOfC1[raysToBeCut[j]][hplaneRowIndex];
        // If we have a positive gradient and this is an ub
        if (rayGrad >= param.getEPS() && hplaneUBFlag == 1) {
          hplaneRatio =
              (UB[hplaneIndex] - solnInfo.primalSoln[hplaneIndex])
                  / (solnInfo.raysOfC1[raysToBeCut[j]][hplaneRowIndex]);
          // If we intersect this ub prior to reaching bd S, add it as a ray cut by the hplane
          if (hplaneRatio <= splitSlackRatio - param.getEPS()) {
//#ifdef TRACE
//            printf(
//                "In addRaysCutByHplaneBeforeBdS: ** FOUND: ray cut by hplane corresponding to struct var. Ray %d (non-basic var index %d with name %s) with splitSR %lf is cut by hplane %d (name %s) with ratio %lf.\n",
//                raysToBeCut[j],
//                solnInfo.nonBasicVarIndex[raysToBeCut[j]],
//                rayName.c_str(), splitSlackRatio, hplaneIndex,
//                hplaneName.c_str(), hplaneRatio);
//#endif
            raysCutByHplane.push_back(raysToBeCut[j]);
          }
        } else {
          // Otherwise, if the gradient is negative and the hplane is a lower bound
          if (rayGrad <= -param.getEPS() && hplaneUBFlag == 0) {
            hplaneRatio =
                (LB[hplaneIndex]
                    - solnInfo.primalSoln[hplaneIndex])
                    / (solnInfo.raysOfC1[raysToBeCut[j]][hplaneRowIndex]);
            // and we intersect the bound prior to intersecting the bd S, add it as a ray cut by the hplane
            if (hplaneRatio <= splitSlackRatio - param.getEPS()) {
//#ifdef TRACE
//              printf(
//                  "In addRaysCutByHplaneBeforeBdS: ** FOUND: ray cut by hplane corresponding to struct var. Ray %d (non-basic var index %d with name %s) with splitSR %lf is cut by hplane %d (name %s) with ratio %lf.\n",
//                  raysToBeCut[j],
//                  solnInfo.nonBasicVarIndex[raysToBeCut[j]],
//                  rayName.c_str(), splitSlackRatio,
//                  hplaneIndex, hplaneName.c_str(),
//                  hplaneRatio);
//#endif
              raysCutByHplane.push_back(raysToBeCut[j]);
            }
          }
        }

      } else { // Otherwise, this is a slack variable
        if (rowSense[hplaneRowIndex] == 'E') { // We should never be activating an equality hyperplane!
          error_msg(errstr,
              "Should never be activating an equality hyperplane!\n");
          writeErrorToLog(errstr, inst_info_out);
          exit(1);
//          rayGrad = fabs(
//              solnInfo.raysOfC1[raysToBeCut[j]][hplaneRowIndex]);
//          if (rayGrad >= param.getEPS()) {
//#ifdef TRACE
//            printf(
//                "In addRaysCutByHplaneBeforeBdS: ** FOUND: ray cut by hplane corresponding to slack var for equality constraint. Ray %d (non-basic var index %d, corresponding to constraint %d with name %s) with splitSR %lf is cut by hplane %d (name %s).\n",
//                raysToBeCut[j],
//                solnInfo.nonBasicVarIndex[raysToBeCut[j]],
//                solnInfo.nonBasicVarIndex[raysToBeCut[j]]
//                - solnInfo.numcols, rayName.c_str(),
//                splitSlackRatio, hplaneIndex,
//                hplaneName.c_str());
//#endif
//            raysCutByHplane.push_back(raysToBeCut[j]);
//          }
        } else {
          rayGrad = solnInfo.raysOfC1[raysToBeCut[j]][hplaneRowIndex];
          if (rayGrad <= -param.getEPS()) {
            hplaneRatio =
                solnInfo.a0[hplaneRowIndex]
                    / (-solnInfo.raysOfC1[raysToBeCut[j]][hplaneRowIndex]);
            if (hplaneRatio <= splitSlackRatio - param.getEPS()) {
//#ifdef TRACE
//              printf(
//                  "In addRaysCutByHplaneBeforeBdS: ** FOUND: ray cut by hplane corresponding to slack var. Ray %d (non-basic var index %d, corresponding to constraint %d with name %s) with splitSR %lf is cut by hplane %d (name %s) with ratio %lf.\n",
//                  raysToBeCut[j],
//                  solnInfo.nonBasicVarIndex[raysToBeCut[j]],
//                  solnInfo.nonBasicVarIndex[raysToBeCut[j]]
//                  - solnInfo.numcols, rayName.c_str(),
//                  splitSlackRatio, hplaneIndex,
//                  hplaneName.c_str(), hplaneRatio);
//#endif
              raysCutByHplane.push_back(raysToBeCut[j]);
            }
          }
        }
      }
//      assert(hplaneRatio >= -param.getEPS());
      if (hplaneRatio < -param.getEPS()) {
        error_msg(errstr,
            "*** ERROR: In addRaysCutByHplaneBeforeBdS: hplaneRatio should not be negative, ever. It is %f.\n",
            hplaneRatio);
        writeErrorToLog(errstr, inst_info_out);
        exit(1);
      }
    }
  } else {
    //This is because the hyperplane is the bounding constraint of a non-basic variable
    //Since it was chosen, the ray must intersect this bounding constraints before the bdS
//#ifdef TRACE
//    printf(
//        "In addRaysCutByHplaneBeforeBdS: ** FOUND: ray cut by hplane corresponding to bounding constraint. Ray %d (non-basic var index %d) is cut by hplane %d (name %s).\n",
//        rayIndex, solnInfo.nonBasicVarIndex[rayIndex], hplaneIndex,
//        hplaneName.c_str());
//#endif
    raysCutByHplane.push_back(rayIndex);
  }
}

//This method checks which hyperplanes from oldHplane + 1 onwards are intersected by rayIndex before the bdS
void addRayToHplanesIntersectedBeforeBdS(PointCutsSolverInterface* solver,
    int oldHplane, SolutionInfo solnInfo, int splitVarIndex,
    int splitRowIndex, double pi0, int rayIndex,
    vector<int> &hplanesIndexToBeActivated,
    vector<int> &hplanesRowIndexToBeActivated,
    vector<int> &hplanesToBeActivatedUBFlag,
    vector<vector<int> > &raysToBeCutByHplane, FILE* inst_info_out) {

  const double* UB = solver.getColUpper();
  const double* LB = solver.getColLower();
  const char* rowSense = solver.getRowSense();
  double splitSlackRatio, hplaneRatio = 0.0;
  double rayGrad = solnInfo.raysOfC1[rayIndex][splitRowIndex];
  int hplaneIndex, hplaneRowIndex;
  //Compute slack ratio
  if (rayGrad >= param.getEPS()) {
    splitSlackRatio = (pi0 + 1 - solnInfo.primalSoln[splitVarIndex])
        / (rayGrad);
  } else {
    if (rayGrad <= -param.getEPS()) {
      splitSlackRatio = (pi0 - solnInfo.primalSoln[splitVarIndex])
          / (rayGrad);
    } else {
      splitSlackRatio = param.getINFINITY();
#ifdef TRACE
      printf(
          "Error in addRayToHplanesIntersectedBeforeBdS: Ray does not intersect the bdS for split variable %d.\n",
          splitVarIndex);
#endif
      //        exit(1);
    }
  }

  for (unsigned h = oldHplane + 1; h < hplanesIndexToBeActivated.size();
      h++) {
    hplaneIndex = hplanesIndexToBeActivated[h];
    hplaneRowIndex = hplanesRowIndexToBeActivated[h];
    //This check is to avoid checking hyperplanes corresponding to bounding constraints of a nonbasic ray
    //These hyperplanes only intersect the nonbasic ray of the variable defining them
    if (hplaneRowIndex != -1) {
      //Compute hplane ratio
      if (hplaneIndex < solnInfo.numCols) {
        rayGrad = solnInfo.raysOfC1[rayIndex][hplaneRowIndex];
        if (rayGrad >= param.getEPS() && hplanesToBeActivatedUBFlag[h] == 1) {
          hplaneRatio = (UB[hplaneIndex]
              - solnInfo.primalSoln[hplaneIndex])
              / (solnInfo.raysOfC1[rayIndex][hplaneRowIndex]);
          if (hplaneRatio <= splitSlackRatio - param.getEPS())
            raysToBeCutByHplane[h].push_back(rayIndex);
        } else {
          if (rayGrad <= -param.getEPS() && hplanesToBeActivatedUBFlag[h] == 0) {
            hplaneRatio = (LB[hplaneIndex]
                - solnInfo.primalSoln[hplaneIndex])
                / (solnInfo.raysOfC1[rayIndex][hplaneRowIndex]);
            if (hplaneRatio <= splitSlackRatio - param.getEPS())
              raysToBeCutByHplane[h].push_back(rayIndex);
          }
        }

      } else {
        if (rowSense[hplaneRowIndex] == 'E') {
          rayGrad = fabs(solnInfo.raysOfC1[rayIndex][hplaneRowIndex]);
          if (rayGrad >= param.getEPS())
            raysToBeCutByHplane[h].push_back(rayIndex);
        } else {
          //          hplaneDirn = -1.0;
          //          if (rowSense[hplaneRowIndex] == 'G')
          //            hplaneDirn = 1.0;
          //          rayGrad = hplaneDirn * solnInfo.raysOfC1[rayIndex][hplaneRowIndex];
          rayGrad = solnInfo.raysOfC1[rayIndex][hplaneRowIndex];
          if (rayGrad <= -param.getEPS()) {
            hplaneRatio =
                solnInfo.a0[hplaneRowIndex]
                    / (-solnInfo.raysOfC1[rayIndex][hplaneRowIndex]);
            if (hplaneRatio <= splitSlackRatio - param.getEPS())
              raysToBeCutByHplane[h].push_back(rayIndex);
          }
        }
      }
//      assert(hplaneRatio >= -param.getEPS());
      if (hplaneRatio < -param.getEPS()) {
        char errorstring[300];
        snprintf(errorstring, sizeof(errorstring) / sizeof(char),
            "*** ERROR: In addRayToHplanesIntersectedBeforeBdS: hplaneRatio should not be negative, ever. It is %f.\n",
            hplaneRatio);
        cerr << errorstring << endl;
        writeErrorToLog(errorstring, inst_info_out);
        exit(1);
      }
    }
  }

//  assert(splitSlackRatio >= -param.getEPS());
  if (splitSlackRatio < -param.getEPS()) {
    char errorstring[300];
    snprintf(errorstring, sizeof(errorstring) / sizeof(char),
        "*** ERROR: In addRayToHplanesIntersectedBeforeBdS: hplaneRatio should not be negative, ever. It is %f.\n",
        splitSlackRatio);
    cerr << errorstring << endl;
    writeErrorToLog(errorstring, inst_info_out);
    exit(1);
  }
}

//Method checks if a hyperplane hplanIndex is in hplanesIndexToBeActivated. Returns the index if yes and -1 otherwise
int hasHplaneBeenActivated(const vector<int> &hplanesIndexToBeActivated,
    const vector<int> &hplaneBasicVarUpperFlag, int hplanIndex,
    int UBFlag) {
  for (int i = 0; i < (int) hplanesIndexToBeActivated.size(); i++) {
    if (hplanIndex == hplanesIndexToBeActivated[i]
        && UBFlag == hplaneBasicVarUpperFlag[i])
      return i;
  }
  return -1;
}

void printActivatedHplaneDetails(FILE* fptr, int numSplits,
    chooseCutGeneratingSets &chooseSplits,
    vector<vector<vector<int> > > &raysToBeCutByHplane,
    vector<vector<vector<int> > > &allRaysCutByHplane,
    vector<vector<int> > &hplanesIndexToBeActivated,
    vector<vector<int> > &hplanesRowIndexToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag) {
  fprintf(fptr,
      "This file contains details on the partially activated hyperplanes. UB flag: -1 = hplane not a bound. 0 = lower-bound. 1 = upper-bound.\n\n");
  fprintf(fptr,
      "(UBlag = -1 implies the hyperplane is structural and not a variable bound)\n\n");
  fprintf(fptr,
      "SplitVarIndex,No,hplaneVarIndex,hplaneRowIndex,UBFlag,RaysToBeCut...\n");
  for (int s = 0; s < numSplits; s++) {
    for (unsigned h = 0; h < hplanesIndexToBeActivated[s].size(); h++) {
      fprintf(fptr, "%d,%d,%d,%d,%d,", chooseSplits.splitVarIndex[s], h,
          hplanesIndexToBeActivated[s][h],
          hplanesRowIndexToBeActivated[s][h],
          hplanesToBeActivatedUBFlag[s][h]);
      for (unsigned r = 0; r < raysToBeCutByHplane[s][h].size(); r++) {
        fprintf(fptr, "%d,", raysToBeCutByHplane[s][h][r]);
      }
      fprintf(fptr, "\n");
    }
  }

  fprintf(fptr, "\nAll rays to be cut\n");
  for (int s = 0; s < numSplits; s++) {
    for (unsigned h = 0; h < hplanesIndexToBeActivated[s].size(); h++) {
      fprintf(fptr, "%d,%d,%d,%d,%d,", chooseSplits.splitVarIndex[s], h,
          hplanesIndexToBeActivated[s][h],
          hplanesRowIndexToBeActivated[s][h],
          hplanesToBeActivatedUBFlag[s][h]);
      for (unsigned r = 0; r < allRaysCutByHplane[s][h].size(); r++) {
        fprintf(fptr, "%d,", allRaysCutByHplane[s][h][r]);
      }
      fprintf(fptr, "\n");
    }
  }

}
