/*
 * cutComparator.hpp
 *
 *  Created on: Jun 22, 2012
 *      Author: selva
 */

#ifndef CUTCOMPARATOR_HPP_
#define CUTCOMPARATOR_HPP_
/**COIN header*/
#include "OsiSolverInterface.hpp"
#include "typedefs.hpp"
#include "GlobalConstants.hpp"
#include "Structs.hpp"
#include "chooseCutGeneratingSets.hpp"
#include "Output.hpp"

#include <vector>

using namespace std;

void compareCutCoefficients(FILE* fptr, string &instName, int hplaneHeur,
    int cutSelHeur, int numSplits, int nRaysToBeCut, int nNonBasicCols,
    vector<int> &splitVarIndx, vector<cut> &GICCutStore,
    vector<cut> &stdIntersectionCuts, PointCutsSolverInterface *cutLpSolvers);

void compareCutRootNodeBoundsAllCutsSub(char* filename, const char* instName,
    int hplaneHeur, int cutSelHeur, int nRaysToBeCut,
    PointCutsSolverInterface* tmpsolver, SolutionInfo &solnInfo,
    chooseCutGeneratingSets &chooseSplits, vector<cut> &SIC,
    vector<cut> &cutStore, vector<vector<int> > &hplanesIndxToBeActivated,
    double &SICBound, double &SICBasisCondNumber, double &GICBound,
    double &GICBasisCondNumber, double &SICAndGICBound,
    double &SICAndGICBasisCondNumber, string lp_name_stub,
    FILE *inst_info_out);

void compareCutRootNodeBoundsAllCutsLifted(char* filename, const char* instName,
    int hplaneHeur, int cutSelHeur, int nRaysToBeCut,
    PointCutsSolverInterface* tmpsolver, SolutionInfo &solnInfo,
    chooseCutGeneratingSets &chooseSplits, vector<cut> &SIC,
    vector<cut> &cutStore, vector<vector<int> > &hplanesIndxToBeActivated,
    double &SICBound, double &SICBasisCondNumber, double &GICBound,
    double &GICBasisCondNumber, double &SICAndGICBound,
    double &SICAndGICBasisCondNumber, string lp_name_stub,
    FILE *inst_info_out);

/**
 * This compares LSICs to OSICs, using all LSICs for each split
 */
void compare_LSIC_to_OSIC_Root_All(const char* instName,
    PointCutsSolverInterface* tmpsolver, SolutionInfo &solnInfo,
    chooseCutGeneratingSets &chooseSplits, vector<cut> &OSIC,
    vector<vector<cut> > &cutStore, double &OSICBound,
    double &OSICBasisCondNumber, double &LSICBound,
    double &LSICBasisCondNumber, double &OSICAndLSICBound,
    double &OSICAndLSICBasisCondNumber, string lp_name_stub,
    FILE *inst_info_out);

/**
 * This compares LSICs to OSICs, only using the *first* LSIC for each split
 */
void compare_LSIC_to_OSIC_Root_Initial(const char* instName,
    PointCutsSolverInterface* tmpsolver, SolutionInfo &solnInfo,
    chooseCutGeneratingSets &chooseSplits, vector<cut> &OSIC,
    vector<vector<cut> > &cutStore, double &OSICBound,
    double &OSICBasisCondNumber, double &LSICBound,
    double &LSICBasisCondNumber, double &OSICAndLSICBound,
    double &OSICAndLSICBasisCondNumber, string lp_name_stub,
    FILE *inst_info_out);

void compareCutRootNodeBoundsRounds(int numRounds, FILE* cutSum,
    string &instName, int hplaneHeur, int cutSelHeur, int nRaysToBeCut,
    PointCutsSolverInterface* solver, SolutionInfo &solnInfo,
    chooseCutGeneratingSets &chooseSplits, vector<cut> &stdIntersectionCuts,
    vector<cut> &cutStore, vector<vector<int> > hplanesIndxToBeActivated,
    string lp_name_stub, FILE *inst_info_out);

/**
 * We already have the bound from just LSICs and OSICs,
 * so we need to compute:
 * 1. Bound from only adding LGICs
 * 2. Bound from LGICs+OSICs
 * 3. Bound from LGICs+LSICs
 */
void compare_LGIC_to_LSIC_and_OSIC_Root_All(const char* instName,
    PointCutsSolverInterface* tmpsolver, SolutionInfo &solnInfo,
    chooseCutGeneratingSets &chooseSplits, vector<cut> &OSIC,
    vector<vector<cut> > &LSIC, vector<vector<cut> > &LGIC,
    double &LGICBound, double &LGICBasisCondNumber,
    double &LGICAndOSICBound, double &LGICAndOSICBasisCondNumber,
    double &LGICAndLSICBound, double &LGICAndLSICBasisCondNumber,
    string lp_name_stub, FILE *inst_info_out);

void compare_LGIC_to_InitLSIC_and_OSIC_Root_All(const char* instName,
    PointCutsSolverInterface* tmpsolver, SolutionInfo &solnInfo,
    chooseCutGeneratingSets &chooseSplits, vector<cut> &OSIC,
    vector<vector<cut> > &LSIC, vector<vector<cut> > &LGIC,
    double &LGICBound, double &LGICBasisCondNumber,
    double &LGICAndOSICBound, double &LGICAndOSICBasisCondNumber,
    double &LGICAndLSICBound, double &LGICAndLSICBasisCondNumber,
    string lp_name_stub, FILE *inst_info_out);

void compare_LGIC_to_LSIC_and_OSIC_Root_Initial(const char* instName,
    PointCutsSolverInterface* tmpsolver, SolutionInfo &solnInfo,
    chooseCutGeneratingSets &chooseSplits, vector<cut> &OSIC,
    vector<vector<cut> > &LSIC, vector<vector<cut> > &LGIC,
    double &LGICBoundInit, double &LGICBasisCondNumberInit,
    double &LGICAndOSICBoundInit, double &LGICAndOSICBasisCondNumberInit,
    double &LGICAndLSICBoundInit, double &LGICAndLSICBasisCondNumberInit,
    string lp_name_stub, FILE *inst_info_out);
#endif /* CUTCOMPARATOR_HPP_ */
