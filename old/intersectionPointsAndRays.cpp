/*
 * intersectionPointsAndRays.cpp
 *
 *  Created on: Jun 18, 2012
 *      Author: selva
 */
#include "intersectionPointsAndRays.hpp"

void addRankOneInterPtsAndRays(const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo,
    const chooseCutGeneratingSets& chooseSplits,
    const vector<vector<vector<int> > >& raysToBeCutByHplane,
    const vector<vector<vector<int> > >& allRaysCutByHplane,
    const vector<vector<int> >& hplanesIndxToBeActivated,
    const vector<vector<int> >& hplanesRowIndxToBeActivated,
    const vector<vector<int> >& hplanesToBeActivatedUBFlag,
    CoinPackedMatrix**& interPtsAndRays,
    std::vector<std::vector<int> >& pointOrRayFlag,
    std::vector<int>& numPointsOrRays,
    std::vector<std::vector<ptOrRayInfo*> >& intPtOrRayInfo,
    std::vector<std::vector<int> >& raysToBeCut,
    std::vector<std::vector<std::vector<int> > >& partitionOfPtsAndRaysByRaysOfC1,
    std::vector<cut>& NBSIC, std::vector<int>& totalNumPointsRemoved,
    std::vector<int>& totalNumPointsAdded,
    std::vector<double>& avgRemovedDepth,
    std::vector<double>& avgAddedDepth, FILE* inst_info_out) {
  int numSplits = chooseSplits.nEffCutGenSets;
  //Initialize temp containers points and rays
  vector<double> tempPt(2, 0.0);
  vector<int> tempElmt(2, 0), newRayColIndices;
  vector<int> delIndices;
  const char* rowSense = solver.getRowSense();
  numPointsOrRays.resize(chooseSplits.nEffCutGenSets, 0);
  pointOrRayFlag.resize(chooseSplits.nEffCutGenSets);

  // Set up the depth calculations
  totalNumPointsRemoved.resize(numSplits, 0);
  totalNumPointsAdded.resize(numSplits, 0);
  avgRemovedDepth.resize(numSplits, 0.0);
  avgAddedDepth.resize(numSplits, 0.0);

  for (int s = 0; s < numSplits; s++) {
    // Calculate totalSOS for the SIC for this split
    double totalSOS = 0.0;
    for (int k = 0; k < (int) NBSIC[s].compNB_coeff.size(); k++) {
      totalSOS += NBSIC[s].compNB_coeff[k] * NBSIC[s].compNB_coeff[k];
    }
    const double norm = std::sqrt(totalSOS);

    const int numHplanesActivatedForSplit =
        hplanesIndxToBeActivated[s].size();
    for (int h = 0; h < numHplanesActivatedForSplit; h++) {
#ifdef TRACE
      printf(
          "\nIn addRankOneInterPtsAndRays: Split %d, hplane var %d, hplane row index %d.\n",
          s, hplanesIndxToBeActivated[s][h], hplanesRowIndxToBeActivated[s][h]);
#endif
      activateHplane(totalNumPointsRemoved[s], avgRemovedDepth[s],
          totalNumPointsAdded[s], avgAddedDepth[s],
          interPtsAndRays[s], pointOrRayFlag[s], numPointsOrRays[s],
          intPtOrRayInfo[s], chooseSplits, s, h,
          hplanesIndxToBeActivated[s][h],
          hplanesRowIndxToBeActivated[s][h],
          hplanesToBeActivatedUBFlag[s][h], solver, solnInfo,
          raysToBeCutByHplane[s][h],
          partitionOfPtsAndRaysByRaysOfC1[s],
          allRaysCutByHplane[s][h], NBSIC[s], norm, inst_info_out,
          true);
    }

    // Average average depth
    if (numHplanesActivatedForSplit > 0) {
      avgRemovedDepth[s] /= numHplanesActivatedForSplit;
      avgAddedDepth[s] /= numHplanesActivatedForSplit;
    }

#ifdef TRACE
    printf(
        "\nIn addRankOneInterPtsAndRays: Adding intersection points from uncut rays of C1. This may add a few points and/or rays.\n");
#endif
    addInterPtsFromUncutRaysOfC1(rowSense, *interPtsAndRays[s],
        solnInfo.nonBasicVarIndex, pointOrRayFlag[s],
        numPointsOrRays[s], intPtOrRayInfo[s],
        chooseSplits.splitVarRowIndex[s], solnInfo.numCols,
        solnInfo.numNB, solnInfo.raysOfC1, raysToBeCut[s],
        tempElmt, tempPt, chooseSplits.splitVarValue[s],
        chooseSplits.pi0[s], partitionOfPtsAndRaysByRaysOfC1[s],
        inst_info_out);
  }

//  const int numSplitsEffByHplane = splitsEffByHplane.size();
//  std::vector<int> numPointsRemoved(numSplitsEffByHplane);
//  std::vector<double> avgRemovedDepth(numSplitsEffByHplane);
//  std::vector<double> avgAddedDepth(numSplitsEffByHplane);
//  for (int s_ind = 0; s_ind < numSplitsEffByHplane; s_ind++) {
//    const int s = splitsEffByHplane[s_ind];
//    // Calculate totalSOS for the SIC for this split
//    double totalSOS = 0.0;
//    for (int k = 0; k < (int) NBSIC[s].compNB_coeff.size(); k++) {
//      totalSOS += NBSIC[s].compNB_coeff[k] * NBSIC[s].compNB_coeff[k];
//    }
//    const double norm = std::sqrt(totalSOS);
//    activateHplane(numPointsRemoved[s], avgRemovedDepth[s],
//        avgAddedDepth[s], interPtsAndRays[s], pointOrRayFlag[s],
//        numPointsOrRays[s], intPtOrRayInfo[s], chooseSplits, s,
//        hplaneVarIndexToActivate, hplaneVarRowIndexToActivate,
//        hplaneVarUBFlagToActivate, solver, solnInfo,
//        raysToBeCutByHplane[s][h], partitionOfPtsAndRaysByRaysOfC1[s],
//        allRaysCutByHplane[s][h], NBSIC[s], norm, inst_info_out);
//  }
}

void CreateInterPtsAndRays(PointCutsSolverInterface* solver,
    SolutionInfo &solnInfo, chooseCutGeneratingSets &splitCont,
    vector<vector<vector<int> > > &raysToBeCutByHplane,
    vector<vector<vector<int> > >& allRaysCutByHplane,
    vector<vector<int> > &hplanesIndxToBeActivated,
    vector<vector<int> > &hplanesRowIndxToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    CoinPackedMatrix**& interPtsAndRays,
    vector<vector<int> > &pointOrRayFlag, std::vector<int>& numPointsOrRays,
    vector<vector<ptOrRayInfo*> > &intPtOrRayInfo,
    vector<vector<int> > &raysToBeCut,
    vector<vector<vector<int> > > &partitionOfPtsAndRaysByRaysOfC1,
    std::vector<cut>& NBSIC, std::vector<int>& totalNumPointsRemoved,
    std::vector<int>& totalNumPointsAdded,
    std::vector<double>& avgRemovedDepth,
    std::vector<double>& avgAddedDepth, FILE* inst_info_out) {

  const int numSplits = splitCont.nEffCutGenSets;
  //Initialize temp containers points and rays
  vector<double> tempPt(2, 0.0);
  vector<int> tempElmt(2, 0), newRayColIndices;
  vector<int> delIndices;
  const char* rowSense = solver.getRowSense();
  numPointsOrRays.resize(numSplits, 0);
  //  for (int s = 0; s < numSplits; s++) cout << "numPointsOrRays = " << numPointsOrRays[s] << "\n";
  pointOrRayFlag.resize(numSplits);

  // Set up the depth calculations
  totalNumPointsRemoved.resize(numSplits, 0);
  totalNumPointsAdded.resize(numSplits, 0);
  avgRemovedDepth.resize(numSplits, 0.0);
  avgAddedDepth.resize(numSplits, 0.0);

  for (int s = 0; s < numSplits; s++) {
    //Reverse ordering so that the coinpacked matrix is row ordered
    //and set number of columns
    (*interPtsAndRays[s]).reverseOrdering();
    (*interPtsAndRays[s]).setDimensions(0, solnInfo.numNB);

    // Calculate totalSOS for the SIC for this split
    double totalSOS = 0.0;
    for (int k = 0; k < (int) NBSIC[s].compNB_coeff.size(); k++) {
      totalSOS += NBSIC[s].compNB_coeff[k] * NBSIC[s].compNB_coeff[k];
    }
    const double norm = std::sqrt(totalSOS);

    const int numHplanesActivatedForSplit =
        hplanesIndxToBeActivated[s].size();
    for (int h = 0; h < numHplanesActivatedForSplit; h++) {
#ifdef TRACE
      printf(
          "\nIn CreateInterPtsAndRays: Looking at hplane to be activated with row index %d.\n",
          hplanesRowIndxToBeActivated[s][h]);
#endif
      activateHplane(totalNumPointsRemoved[s], avgRemovedDepth[s],
          totalNumPointsAdded[s], avgAddedDepth[s],
          interPtsAndRays[s], pointOrRayFlag[s], numPointsOrRays[s],
          intPtOrRayInfo[s], splitCont, s, h,
          hplanesIndxToBeActivated[s][h],
          hplanesRowIndxToBeActivated[s][h],
          hplanesToBeActivatedUBFlag[s][h], solver, solnInfo,
          raysToBeCutByHplane[s][h],
          partitionOfPtsAndRaysByRaysOfC1[s],
          allRaysCutByHplane[s][h], NBSIC[s], norm, inst_info_out);
    }

    // Average average depth
    if (numHplanesActivatedForSplit > 0) {
      avgRemovedDepth[s] /= numHplanesActivatedForSplit;
      avgAddedDepth[s] /= numHplanesActivatedForSplit;
    }

#ifdef TRACE
    printf(
        "\nIn CreateInterPtsAndRays: Adding intersection points from uncut rays of C1. This may add a few points and/or rays.\n");
#endif
    addInterPtsFromUncutRaysOfC1(rowSense, *interPtsAndRays[s],
        solnInfo.nonBasicVarIndex, pointOrRayFlag[s],
        numPointsOrRays[s], intPtOrRayInfo[s],
        splitCont.splitVarRowIndex[s], solnInfo.numCols,
        solnInfo.numNB, solnInfo.raysOfC1, raysToBeCut[s],
        tempElmt, tempPt, splitCont.splitVarValue[s], splitCont.pi0[s],
        partitionOfPtsAndRaysByRaysOfC1[s], inst_info_out);
  }
}

void activateHplane(int& totalNumPointsRemoved, double& avgRemovedDepth,
    int& totalNumPointsAdded, double& avgAddedDepth,
    CoinPackedMatrix*& interPtsAndRays, std::vector<int>& pointOrRayFlag,
    int &numPointsOrRays, std::vector<ptOrRayInfo*>& intPtOrRayInfo,
    const chooseCutGeneratingSets& chooseSplits, const int splitIndex,
    const int hplaneIndexInSplit, const int hplaneVarIndex,
    const int hplaneVarRowIndex, const int hplaneUBFlag,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    const std::vector<int>& raysToBeCutByHplane,
    std::vector<std::vector<int> >& partitionOfPtsAndRaysByRaysOfC1,
    const std::vector<int>& allRaysCutByHplane, const cut& NBSIC,
    const double norm, FILE* inst_info_out, const bool SECOND_ACT) {
  // We we want to track the average depth of the removed intersection points
  double sumRemovedDepth = 0.0, sumAddedDepth = 0.0;
  int numNewPointsRemoved = 0, numNewPointsAdded = 0;

  std::vector<double> tempPt(2, 0.0);
  std::vector<int> tempElmt(2, 0);
  std::vector<int> delIndices, newRayColIndices;

  //Mark rays that will be cut by a hyperplane
  vector<bool> raysCutFlag(solnInfo.numNB, false);
  // In PHA 1.1., there should only be one ray cut by the hplane
  // (actually, no, all the rays for which the first hplane is this one)
  for (unsigned j = 0; j < raysToBeCutByHplane.size(); j++) {
    raysCutFlag[raysToBeCutByHplane[j]] = true;
    // Removed depth is zero for these points, since they are from C1
    numNewPointsRemoved++;
  }

  //Remove all intersection points cut off by this hyperplane
  numNewPointsRemoved += removePtsAndRaysCutByHplane(solver, solnInfo,
      raysCutFlag, hplaneVarRowIndex, hplaneVarIndex, hplaneUBFlag,
      delIndices, *interPtsAndRays, pointOrRayFlag, numPointsOrRays,
      intPtOrRayInfo, NBSIC, norm, sumRemovedDepth,
      partitionOfPtsAndRaysByRaysOfC1, inst_info_out);

  // Calculate average depth of the removed points
//      cout << "sumRemoved" << s << ": " << sumRemovedDepth << endl;
  if (numNewPointsRemoved > 0) {
    avgRemovedDepth += sumRemovedDepth / numNewPointsRemoved;
    totalNumPointsRemoved += numNewPointsRemoved;
  }

  //For each hyperplane, create the set J \setminus K
  storeNewRayNBIndices(solnInfo.numNB, newRayColIndices,
      allRaysCutByHplane, raysCutFlag);

  //For ray intersected by hyperplane H generate new rays corresponding to each element in J \setminus K
  //This is of course available in closed form from the tableau
  for (unsigned j = 0; j < raysToBeCutByHplane.size(); j++) {
    numNewPointsAdded += computeIntersectionPointsWithSplit(solver,
        chooseSplits.splitVarRowIndex[splitIndex], hplaneIndexInSplit,
        hplaneVarIndex, hplaneVarRowIndex, hplaneUBFlag,
        chooseSplits.splitVarValue[splitIndex],
        chooseSplits.pi0[splitIndex], raysToBeCutByHplane[j],
        solnInfo.nonBasicVarIndex[raysToBeCutByHplane[j]],
        newRayColIndices, partitionOfPtsAndRaysByRaysOfC1,
        solnInfo.raysOfC1, solnInfo.a0, tempElmt, tempPt,
        *interPtsAndRays, pointOrRayFlag, numPointsOrRays,
        intPtOrRayInfo, NBSIC, norm, sumAddedDepth, inst_info_out);
  }
  if (numNewPointsAdded > 0) {
    avgAddedDepth += sumAddedDepth / numNewPointsAdded;
    totalNumPointsAdded += numNewPointsAdded;
  }

  // Ensure the average removed depth < average added depth
  if (!SECOND_ACT
      && (sumAddedDepth / numNewPointsAdded
          - sumRemovedDepth / numNewPointsRemoved) < -param.getEPS()) {
    error_msg(errstr,
        "Average added depth less than average removed depth:\n" "\tsumAddedDepth: %f, numAddedPoints: %d\n" "\tsumRemovedDepth: %f, numRemovedPoints: %d\n" "\tavgAddedDepth: %f, avgRemovedDepth: %f\n",
        sumAddedDepth, numNewPointsAdded, sumRemovedDepth,
        numNewPointsRemoved, sumAddedDepth / numNewPointsAdded,
        sumRemovedDepth / numNewPointsRemoved);
    writeErrorToII(errstr, inst_info_out);
    exit(1);
  }
}

/**
 * @param intPtOrRayInfo :: [split][ray]
 * @param strongVertHplaneIndex :: [split][ray] -- gives index of first hplane intersected
 * @param raysToBeCut :: [split][ray]
 */
void storeStrongPtIndices(int numSplits, int numNonBasicCols,
    vector<vector<ptOrRayInfo*> > &intPtOrRayInfo,
    vector<vector<vector<int> > > &strongPtIndices,
    vector<vector<int> > &strongVertHplaneIndex,
    vector<vector<int> > &raysToBeCut, vector<int> &nStrongPts) {

  vector<int> Hplane(numNonBasicCols, -1);
  int colIndx;
  strongPtIndices.resize(numSplits);
  for (unsigned s = 0; s < intPtOrRayInfo.size(); s++) {
    nStrongPts[s] = 0;
    fill(Hplane.begin(), Hplane.end(), -1);
    for (unsigned r = 0; r < strongVertHplaneIndex[s].size(); r++)
      Hplane[raysToBeCut[s][r]] = strongVertHplaneIndex[s][r];
    strongPtIndices[s].resize(numNonBasicCols);
    for (unsigned p = 0; p < intPtOrRayInfo[s].size(); p++) {
      colIndx = (*intPtOrRayInfo[s][p]).cutRayColIndx;
      if (colIndx != -1) {
        if (Hplane[colIndx] == (*intPtOrRayInfo[s][p]).hplaneIndx) {
          strongPtIndices[s][colIndx].push_back(p);
          nStrongPts[s]++;
        }
      }
    }
  }
}

int removePtsAndRaysCutByHplane(const PointCutsSolverInterface* const solver,
    const SolutionInfo &solnInfo, vector<bool> &raysCutFlag,
    const int hplaneRowIndx, const int hplaneVarIndx,
    const int hplaneUBFlag, vector<int>& delIndices,
    CoinPackedMatrix &interPtsAndRays, vector<int> &pointOrRayFlag,
    int &numPointsOrRays, vector<ptOrRayInfo*> &intPtOrRayInfo,
    const cut& NBSIC, const double norm, double& sumRemovedDepth,
    std::vector<std::vector<int> >& partitionOfPtsAndRaysByRaysOfC1,
    FILE* inst_info_out) {
  delIndices.clear();

  const double* UB = solver.getColUpper();
  const double* LB = solver.getColLower();

  // We only remove points that correspond to those rays that were cut previously by this hplane
//  for (int ray_ind = 0; ray_ind < (int) raysCutFlag.size(); ray_ind++) {
//    if (raysCutFlag[ray_ind] == true) {
//      // If this ray was cut, we check all points and rays generated from cutting it
//      std::vector<int> currRayDelIndices;
//      for (int ind = 0;
//          ind < (int) partitionOfPtsAndRaysByRaysOfC1[ray_ind].size();
//          ind++) {
//        const int currpt_row_ind =
//            partitionOfPtsAndRaysByRaysOfC1[ray_ind][ind];
  for (int row_ind = 0; row_ind < interPtsAndRays.getNumRows();
      row_ind++) {
    const int ray_ind =
        (intPtOrRayInfo[row_ind]->cutRayColIndx >= 0) ?
            intPtOrRayInfo[row_ind]->cutRayColIndx :
            intPtOrRayInfo[row_ind]->newRayColIndx;
    if (!raysCutFlag[ray_ind]) {
      continue;
    }

    double SIC_activity = 0.0;
    //This if condition ensures that intersection points are removed only
    //if they emanate from a vertex of a ray that is cut
    // In PHA 1.1, this should never be true, since we never cut a ray twice

    //      if ((*intPtOrRayInfo[r]).cutRayColIndx >= 0) { // Uncomment this if we want to remove all points violating hplane
    const CoinShallowPackedVector tmpVector = interPtsAndRays.getVector(
        row_ind);
    const int numElmts = tmpVector.getNumElements();
    const int* elmtIndices = tmpVector.getIndices();
    const double* elmtVals = tmpVector.getElements();
    double activity = 0.0;
    for (int e = 0; e < numElmts; e++) {
      // Either hplaneRowIndx != -1, in which case we activated a basic var
      // or it is -1, and we activated an upper or lower bound of a nb var
      if (hplaneRowIndx != -1) {
        activity += elmtVals[e]
            * solnInfo.raysOfC1[elmtIndices[e]][hplaneRowIndx];
      } else if (elmtIndices[e]
          == solnInfo.mapStructVarIndexToNBVarIndex[hplaneVarIndx]) {
        activity = elmtVals[e];
      }
      SIC_activity += NBSIC.compNB_coeff[elmtIndices[e]] * elmtVals[e];
    }

    // Note that if hplaneRowIndx == -1, then we activated a bound of a nb var,
    // and it can't be a bound of a slack variable, since they are at zero in the cobasis,
    // and that is their only bound.
    if (hplaneVarIndx >= solnInfo.numCols) {
      // Recall that all slack variables are non-negative in this
      // setting, since they were complemented in SolutionInfo
      if ((pointOrRayFlag[row_ind] == 1)
          && (solnInfo.a0[hplaneRowIndx] + activity <= -param.getEPS())) {
        delIndices.push_back(row_ind);
//        currRayDelIndices.push_back(ind);
      } else if ((pointOrRayFlag[row_ind] == 0)
          && (activity <= -param.getEPS())) {
        delIndices.push_back(row_ind);
//        currRayDelIndices.push_back(ind);
      }
    } else {
      // If this was a basic variable, no bounds were changed
      // Otherwise things may have shifted
      double lb, ub, newValue;
      if (hplaneRowIndx != -1) {
        lb = LB[hplaneVarIndx];
        ub = UB[hplaneVarIndx];
        newValue = solnInfo.a0[hplaneRowIndx] + activity;
      } else {
        lb = 0.0;
        ub = UB[hplaneVarIndx] - LB[hplaneVarIndx];
        newValue = activity;
      }
      if (hplaneUBFlag == 1) {
        if ((pointOrRayFlag[row_ind] == 1)
            && (newValue >= ub + param.getEPS())) {
          delIndices.push_back(row_ind);
//          currRayDelIndices.push_back(ind);
        } else if ((pointOrRayFlag[row_ind] == 0)
            && (activity >= param.getEPS())) {
          delIndices.push_back(row_ind);
//          currRayDelIndices.push_back(ind);
        }
      } else {
        if ((pointOrRayFlag[row_ind] == 1)
            && (newValue <= lb - param.getEPS())) {
          delIndices.push_back(row_ind);
//          currRayDelIndices.push_back(ind);
        } else if ((pointOrRayFlag[row_ind] == 0)
            && (activity <= -param.getEPS())) {
          delIndices.push_back(row_ind);
//          currRayDelIndices.push_back(ind);
        }
      }
    }

    // If we are deleting this point, then count it in deleted depth calculations
    if (pointOrRayFlag[row_ind] == 1 && delIndices.size() > 0
        && delIndices[delIndices.size() - 1] == row_ind) {
      double curr_normed_viol = (SIC_activity - NBSIC.compNB_RHS) / norm;
      if (std::abs(curr_normed_viol) < param.getEPS()) {
        curr_normed_viol = +0.0;
      }
      sumRemovedDepth += curr_normed_viol;
      //        cout << "sumRemoved" << ": " << sumRemovedDepth << endl;

      if (curr_normed_viol < -param.getEPS()) {
        error_msg(errstr,
            "Depth %f is negative! Point cannot be above the SIC.\n",
            curr_normed_viol);
        writeErrorToII(errstr, inst_info_out);
        exit(1);
      }
    }
  }

//  delElmtsInPartitionVector(partitionOfPtsAndRaysByRaysOfC1[ray_ind],
//      currRayDelIndices);
//    }
//  }

  // Sort deleted indices
  sort(delIndices.begin(), delIndices.end());

  interPtsAndRays.deleteRows(delIndices.size(), &delIndices[0]);
  delElmtsInIntVector(pointOrRayFlag, delIndices);
  delElmtsInptOrRayInfoVector(intPtOrRayInfo, delIndices);
  numPointsOrRays -= delIndices.size();
//  cout << "# points deleted: " << delIndices.size() << endl;
  return delIndices.size();
}

void addInterPtsFromUncutRaysOfC1(const char* rowSense,
    CoinPackedMatrix &interPtsAndRays,
    const vector<int> &nonBasicVarIndices, vector<int> &pointOrRayFlag,
    int &numPointsOrRays, vector<ptOrRayInfo*> &intPtOrRayInfo,
    const int splitRowIndex, const int numCols, const int numNonBasicCols,
    const vector<vector<double> > &raysOfC1, const vector<int> &raysToBeCut,
    vector<int> &tempElmt, vector<double> &tempPt, const double splitValue,
    const double pi0, vector<vector<int> > &partitionOfPtsAndRaysByRaysOfC1,
    FILE* inst_info_out) {
  vector<bool> raysCutFlag(nonBasicVarIndices.size(), false);
  for (unsigned r = 0; r < raysToBeCut.size(); r++)
    raysCutFlag[raysToBeCut[r]] = true;

  ptOrRayInfo* tmpPtOrRayInfo;
  bool zeroRay;
  for (int j = 0; j < numNonBasicCols; j++) {
    if (raysCutFlag[j] == false) {

      zeroRay = false;
      if (nonBasicVarIndices[j] >= numCols)
        if (rowSense[nonBasicVarIndices[j] - numCols] == 'E')
          zeroRay = true;

      if (!zeroRay) {
        if (std::abs(raysOfC1[j][splitRowIndex]) <= param.getRAYEPS()) {
          tempElmt[0] = j;
          tempPt[0] = 1.0;

          // Make sure this is not a duplicate
          if (!CHECK_FOR_DUP_RAYS
              || !duplicateRay(interPtsAndRays, pointOrRayFlag, 1,
                  tempElmt, tempPt, inst_info_out)) {
            interPtsAndRays.appendRow(1, &tempElmt[0], &tempPt[0]);
            pointOrRayFlag.push_back(0);

            tmpPtOrRayInfo = new ptOrRayInfo;
            (*tmpPtOrRayInfo).cutRayColIndx = -1;
            (*tmpPtOrRayInfo).newRayColIndx = j;
            (*tmpPtOrRayInfo).hplaneRowIndx = -1;
            (*tmpPtOrRayInfo).hplaneIndx = -1;
            (*tmpPtOrRayInfo).ptFlag = false;
            intPtOrRayInfo.push_back(tmpPtOrRayInfo);

            //Add generated ray to respective partition
            partitionOfPtsAndRaysByRaysOfC1[j].push_back(
                intPtOrRayInfo.size() - 1);

            numPointsOrRays++;
          }
        } else {
          tempElmt[0] = j;
          if (raysOfC1[j][splitRowIndex] >= param.getRAYEPS())
            tempPt[0] = (pi0 + 1 - splitValue)
                / raysOfC1[j][splitRowIndex];
          else
            tempPt[0] = (pi0 - splitValue)
                / raysOfC1[j][splitRowIndex];
          interPtsAndRays.appendRow(1, &tempElmt[0], &tempPt[0]);
          pointOrRayFlag.push_back(1);

          tmpPtOrRayInfo = new ptOrRayInfo;
          (*tmpPtOrRayInfo).cutRayColIndx = -1;
          (*tmpPtOrRayInfo).newRayColIndx = j;
          (*tmpPtOrRayInfo).hplaneRowIndx = -1;
          (*tmpPtOrRayInfo).hplaneIndx = -1;
          (*tmpPtOrRayInfo).ptFlag = true;
          intPtOrRayInfo.push_back(tmpPtOrRayInfo);
          numPointsOrRays++;

          //Add generated point to respective partition
          partitionOfPtsAndRaysByRaysOfC1[j].push_back(
              intPtOrRayInfo.size() - 1);
        }
      }
    }
  }
}

int computeIntersectionPointsWithSplit(const PointCutsSolverInterface* const solver,
    const int splitRowIndx, const int hplaneIndexInSplit,
    const int hplaneVarIndx, const int hplaneRowIndx, const int hplanUBFlag,
    const double splitValue, const double pi0, const int rayIndx,
    const int rayVarIndx, vector<int> &newRayColIndex,
    vector<vector<int> > &partitionOfPtsAndRaysByRaysOfC1,
    const vector<vector<double> > &raysOfC1, const vector<double> &a0,
    vector<int> &tempElmt, vector<double> &tempPt,
    CoinPackedMatrix &interPtsAndRays, vector<int> &pointOrRayFlag,
    int &numPointsOrRays, vector<ptOrRayInfo*> &intPtOrRayInfo,
    const cut& NBSIC, const double norm, double& sumAddedDepth,
    FILE* inst_info_out) {
  int numAddedPoints = 0;
  // To save some space in this method
  bool toAdd = false;
  int cutRayIndexToAdd = rayIndx;
  int newRayIndexToAdd = -1;
  int hplaneIndexToAdd = hplaneIndexInSplit; // Index in activated hplanes for that split
  int hplaneRowIndexToAdd = hplaneRowIndx;
  bool ptFlagToAdd = false;
  int numElmtsToAdd = 0;

  double rayDirnInJSpace = 0.0, theta = 0.0, hplaneRHS = 0.0;
  ptOrRayInfo* tmpPtOrRayInfo;
  if (hplaneRowIndx != -1) {
    const double* UB = solver.getColUpper();
    const double* LB = solver.getColLower();
    tempElmt[0] = rayIndx;
    for (unsigned j = 0; j < newRayColIndex.size(); j++) {
      // - ( a_{kq} - a_{kj} * a_{hq} / a_{hj} )
      // What is computed below is actually the negative of above
      rayDirnInJSpace = -raysOfC1[newRayColIndex[j]][splitRowIndx]
          - (-raysOfC1[rayIndx][splitRowIndx]
              * (raysOfC1[newRayColIndex[j]][hplaneRowIndx]
                  / raysOfC1[rayIndx][hplaneRowIndx]));

      if (std::abs(rayDirnInJSpace) < param.getRAYEPS()) {
        //Add coordinates of ray to interPtsAndRays
        tempPt[0] = -(raysOfC1[newRayColIndex[j]][hplaneRowIndx]
            / raysOfC1[rayIndx][hplaneRowIndx]);
        tempElmt[1] = newRayColIndex[j];
        tempPt[1] = 1.0;

        if (std::abs(tempPt[0]) < param.getEPS())
          tempPt[0] = 0.0;
        if (std::abs(tempPt[1]) < param.getEPS())
          tempPt[1] = 0.0;

        numElmtsToAdd = 2;
        toAdd = !CHECK_FOR_DUP_RAYS
            || !duplicateRay(interPtsAndRays, pointOrRayFlag,
                numElmtsToAdd, tempElmt, tempPt, inst_info_out);
        ptFlagToAdd = false;
        newRayIndexToAdd = newRayColIndex[j];
      } else {
        // We update the new value of x_k
        // NewSplitValue:
        //   splitValue + r_k^j * (bound_h - x*_h) / r_h^j
        // bound_h is the bound intersected of the activated hplane
        if (hplanUBFlag == 1) {
          hplaneRHS = UB[hplaneVarIndx] - a0[hplaneRowIndx];
        } else {
          if (hplaneVarIndx < solver.getNumCols()) {
            hplaneRHS = LB[hplaneVarIndx] - a0[hplaneRowIndx];
          } else {
            // for a slack, bound_h = 0 always
            hplaneRHS = -a0[hplaneRowIndx];
          }
        }
        const double newSplitValue = splitValue
            + raysOfC1[rayIndx][splitRowIndx] * hplaneRHS
                / raysOfC1[rayIndx][hplaneRowIndx];

        // From this new point how far do we go until we intersect the split?
        if (rayDirnInJSpace >= param.getRAYEPS()) {
          theta = (newSplitValue - pi0) / rayDirnInJSpace;
        } else if (rayDirnInJSpace <= -param.getRAYEPS()) {
          theta = (newSplitValue - (pi0 + 1)) / rayDirnInJSpace;
        }

        // Thus, we go theta along the direction of the new ray
        tempPt[0] = (hplaneRHS
            - theta * raysOfC1[newRayColIndex[j]][hplaneRowIndx])
            / raysOfC1[rayIndx][hplaneRowIndx];
        tempElmt[1] = newRayColIndex[j];
        tempPt[1] = theta;

        //Assert that the intersection point coordinates are non-negative
#ifdef TRACE
        if (tempPt[0] <= -param.getEPS() || tempPt[1] <= -param.getEPS()) {
          printf(
              "tmpPoint0 = %lf, tempPoint1 = %lf, splitRowIndx = %d, hplaneRowIndx = %d, hplaneUBFlag = %d, rayIndx = %d, newRayIndx = %d\n",
              tempPt[0], tempPt[1], splitRowIndx, hplaneRowIndx,
              hplanUBFlag, rayIndx, newRayColIndex[j]);
          printf(
              "akq = %lf, akl = %lf, ahq = %lf, ahl = %lf, rayDirn = %lf, ratio = %lf\n",
              -raysOfC1[rayIndx][splitRowIndx],
              -raysOfC1[newRayColIndex[j]][splitRowIndx],
              -raysOfC1[rayIndx][hplaneRowIndx],
              -raysOfC1[newRayColIndex[j]][hplaneRowIndx],
              rayDirnInJSpace,
              (raysOfC1[newRayColIndex[j]][hplaneRowIndx]
                  / raysOfC1[rayIndx][hplaneRowIndx]));
        }
#endif
        if (tempPt[0] < -param.getEPS()) {
          error_msg(errorstring,
              "tempPt[0] should be positive. It is %e. splitRowIndx = %d, hplaneRowIndx = %d, hplaneUBFlag = %d, rayIndx = %d, newRayIndx = %d.\n",
              tempPt[0], splitRowIndx, hplaneRowIndx, hplanUBFlag,
              rayIndx, newRayColIndex[j]);
          writeErrorToII(errorstring, inst_info_out);
          exit(1);
        }
        if (tempPt[1] < -param.getEPS()) {
          error_msg(errorstring,
              "tempPt[1] should be positive. It is %e. splitRowIndx = %d, hplaneRowIndx = %d, hplaneUBFlag = %d, rayIndx = %d, newRayIndx = %d.\n",
              tempPt[1], splitRowIndx, hplaneRowIndx, hplanUBFlag,
              rayIndx, newRayColIndex[j]);
          writeErrorToII(errorstring, inst_info_out);
          exit(1);
        }

        if (std::abs(tempPt[0]) < param.getEPS())
          tempPt[0] = 0.0;
        if (std::abs(tempPt[1]) < param.getEPS())
          tempPt[1] = 0.0;

        toAdd = true;
        numElmtsToAdd = 2;
        ptFlagToAdd = true;
        newRayIndexToAdd = newRayColIndex[j];
      }

      // Add point or ray if found
      // Also calculate its SIC normalized violation
      if (toAdd) {
        //Add coordinates of point / ray to interPtsAndRays
        interPtsAndRays.appendRow(numElmtsToAdd, &tempElmt[0],
            &tempPt[0]);
        pointOrRayFlag.push_back(ptFlagToAdd);

        //Add information about generated points to intPtOrRayInfo
        tmpPtOrRayInfo = new ptOrRayInfo;
        (*tmpPtOrRayInfo).cutRayColIndx = cutRayIndexToAdd;
        (*tmpPtOrRayInfo).newRayColIndx = newRayIndexToAdd;
        (*tmpPtOrRayInfo).hplaneRowIndx = hplaneRowIndexToAdd;
        (*tmpPtOrRayInfo).hplaneIndx = hplaneIndexToAdd;
        (*tmpPtOrRayInfo).ptFlag = ptFlagToAdd;
        intPtOrRayInfo.push_back(tmpPtOrRayInfo);

        //Add generated point to respective partition
        partitionOfPtsAndRaysByRaysOfC1[rayIndx].push_back(
            intPtOrRayInfo.size() - 1);

        numPointsOrRays++;
        toAdd = false;

        // Calculate depth of added point
        if (ptFlagToAdd) {
          double SIC_activity = 0.0;
          for (int e = 0; e < numElmtsToAdd; e++) {
            SIC_activity += NBSIC.compNB_coeff[tempElmt[e]]
                * tempPt[e];
          }
          double curr_normed_viol = (SIC_activity - NBSIC.compNB_RHS)
              / norm;
          if (std::abs(curr_normed_viol) < param.getEPS()) {
            curr_normed_viol = +0.0;
          }
          sumAddedDepth += curr_normed_viol;
          numAddedPoints++;

          if (curr_normed_viol < -param.getEPS()) {
            error_msg(errstr,
                "Depth %f is negative! Point cannot be above the SIC.\n",
                curr_normed_viol);
            writeErrorToII(errstr, inst_info_out);
            exit(1);
          }
        }

        //#ifdef TRACE
        //    printf(
        //        "\tIn computeIntersectionPointsWithSplit: Found new point or ray:\n\t\tnumPointsOrRays: %d.\n\t\tcutRayColIndx: %d.\n\t\tnewRayColIndx: %d.\n\t\thplaneIndx: %d.\n\t\thplaneRowIndx: %d.\n",
        //        numPointsOrRays, (*tmpPtOrRayInfo).cutRayColIndx,
        //        (*tmpPtOrRayInfo).newRayColIndx,
        //        (*tmpPtOrRayInfo).hplaneIndx,
        //        (*tmpPtOrRayInfo).hplaneRowIndx);
        //#endif
      }
    }
  } else { // We are here when hplaneRowIndx == -1, activating bound on nb variable (which *must* be structural in this case)
    const double* UB = solver.getColUpper();
    const double* LB = solver.getColLower();
    for (unsigned j = 0; j < newRayColIndex.size(); j++) {
      rayDirnInJSpace = -raysOfC1[newRayColIndex[j]][splitRowIndx];
      if (std::abs(rayDirnInJSpace) <= param.getRAYEPS()) {
        //Add coordinates of ray to interPtsAndRays
        tempElmt[0] = newRayColIndex[j];
        tempPt[0] = 1.0;

        numElmtsToAdd = 1;
        toAdd = !CHECK_FOR_DUP_RAYS
            || !duplicateRay(interPtsAndRays, pointOrRayFlag,
                numElmtsToAdd, tempElmt, tempPt, inst_info_out);
        ptFlagToAdd = false;
        newRayIndexToAdd = newRayColIndex[j];
      } else {
        if (rayDirnInJSpace >= param.getRAYEPS()) {
          theta = (splitValue - pi0
              - (-raysOfC1[rayIndx][splitRowIndx])
                  * (UB[rayVarIndx] - LB[rayVarIndx]))
              / rayDirnInJSpace;
        } else if (rayDirnInJSpace <= -param.getRAYEPS()) {
          theta = (splitValue - (pi0 + 1)
              - (-raysOfC1[rayIndx][splitRowIndx])
                  * (UB[rayVarIndx] - LB[rayVarIndx]))
              / rayDirnInJSpace;
        }
        tempElmt[0] = rayIndx;
        tempPt[0] = (UB[rayVarIndx] - LB[rayVarIndx]);
        tempElmt[1] = newRayColIndex[j];
        tempPt[1] = theta;

        if (std::abs(tempPt[0]) < param.getEPS())
          tempPt[0] = 0.0;
        if (std::abs(tempPt[1]) < param.getEPS())
          tempPt[1] = 0.0;

        toAdd = true;
        numElmtsToAdd = 2;
        ptFlagToAdd = true;
        newRayIndexToAdd = newRayColIndex[j];
      }

      // Add point or ray if found
      // Also calculate its SIC normalized violation
      if (toAdd) {
        //Add coordinates of point / ray to interPtsAndRays
        interPtsAndRays.appendRow(numElmtsToAdd, &tempElmt[0],
            &tempPt[0]);
        pointOrRayFlag.push_back(ptFlagToAdd);

        //Add information about generated points to intPtOrRayInfo
        tmpPtOrRayInfo = new ptOrRayInfo;
        (*tmpPtOrRayInfo).cutRayColIndx = cutRayIndexToAdd;
        (*tmpPtOrRayInfo).newRayColIndx = newRayIndexToAdd;
        (*tmpPtOrRayInfo).hplaneRowIndx = hplaneRowIndexToAdd;
        (*tmpPtOrRayInfo).hplaneIndx = hplaneIndexToAdd;
        (*tmpPtOrRayInfo).ptFlag = ptFlagToAdd;
        intPtOrRayInfo.push_back(tmpPtOrRayInfo);

        //Add generated point to respective partition
        partitionOfPtsAndRaysByRaysOfC1[rayIndx].push_back(
            intPtOrRayInfo.size() - 1);

        numPointsOrRays++;
        toAdd = false;

        // Calculate depth of added point
        if (ptFlagToAdd) {
          double SIC_activity = 0.0;
          for (int e = 0; e < numElmtsToAdd; e++) {
            SIC_activity += NBSIC.compNB_coeff[tempElmt[e]]
                * tempPt[e];
          }
          double curr_normed_viol = (SIC_activity - NBSIC.compNB_RHS)
              / norm;
          if (std::abs(curr_normed_viol) < param.getEPS()) {
            curr_normed_viol = +0.0;
          }
          sumAddedDepth += curr_normed_viol;
          numAddedPoints++;

          if (curr_normed_viol < -param.getEPS()) {
            error_msg(errstr,
                "Depth %f is negative! Point cannot be above the SIC.\n",
                curr_normed_viol);
            writeErrorToII(errstr, inst_info_out);
            exit(1);
          }
        }

        //#ifdef TRACE
        //    printf(
        //        "\tIn computeIntersectionPointsWithSplit: Found new point or ray:\n\t\tnumPointsOrRays: %d.\n\t\tcutRayColIndx: %d.\n\t\tnewRayColIndx: %d.\n\t\thplaneIndx: %d.\n\t\thplaneRowIndx: %d.\n",
        //        numPointsOrRays, (*tmpPtOrRayInfo).cutRayColIndx,
        //        (*tmpPtOrRayInfo).newRayColIndx,
        //        (*tmpPtOrRayInfo).hplaneIndx,
        //        (*tmpPtOrRayInfo).hplaneRowIndx);
        //#endif
      }
    }
  }

  return numAddedPoints;
}

bool duplicateRay(const CoinPackedMatrix& interPtsAndRays,
    const vector<int>& pointOrRayFlag, const int numElmtsToAdd,
    const vector<int>& rayIndexToAdd, const vector<double>& rayValueToAdd,
    FILE* inst_info_out) {
  const int numPtsAndRays = interPtsAndRays.getNumRows();
  for (int r = 0; r < numPtsAndRays; r++) {
    // Make sure it is a ray
    if (pointOrRayFlag[r] == 0) {
      const CoinShallowPackedVector vec = interPtsAndRays.getVector(r);
      const int numElmts = vec.getNumElements();
      const int* vecIndex = vec.getIndices();
      const double* vecElmt = vec.getElements();

      if (numElmts != numElmtsToAdd) {
        if (numElmtsToAdd == 1) {
          if (numElmts == 2) {
            if (std::abs(vecElmt[0]) < param.getEPS()) {
              bool dup = true;
              // These two rays are different if their non-basic indices are different,
              // or if the sign of the rays are different
              if (vecIndex[1] != rayIndexToAdd[0]) {
                dup = false;
              } else if ((std::abs(rayValueToAdd[0]) < param.getEPS()
                  && std::abs(vecElmt[1]) >= param.getEPS())
                  || (std::abs(rayValueToAdd[0]) >= param.getEPS()
                      && std::abs(vecElmt[1]) < param.getEPS())) {
                dup = false;
              } else if (rayValueToAdd[0] >= param.getEPS()
                  && vecElmt[1] <= -param.getEPS()) {
                dup = false;
              } else if (rayValueToAdd[0] <= -param.getEPS()
                  && vecElmt[1] >= param.getEPS()) {
                dup = false;
              }
              if (dup)
                return true;
            }
          }
        }
      } else {
        bool dup = true;
        for (int el = 0; el < numElmts; el++) {
          bool bothZero = std::abs(vecElmt[el]) < param.getEPS()
              && std::abs(rayValueToAdd[el]) < param.getEPS();
          // These two rays are different if their non-basic indices are different,
          // or if the sign of the rays are different
          if (!bothZero && (vecIndex[el] != rayIndexToAdd[el])) {
            dup = false;
            break;
          } else if ((std::abs(rayValueToAdd[el]) < param.getEPS()
              && std::abs(vecElmt[el]) >= param.getEPS())
              || (std::abs(rayValueToAdd[el]) >= param.getEPS()
                  && std::abs(vecElmt[el]) < param.getEPS())) {
            dup = false;
            break;
          } else if (rayValueToAdd[el] >= param.getEPS() && vecElmt[el] <= -param.getEPS()) {
            dup = false;
            break;
          } else if (rayValueToAdd[el] <= -param.getEPS() && vecElmt[el] >= param.getEPS()) {
            dup = false;
            break;
          }
        }
        if (dup)
          return true;
      }
    }
  }
  return false;
}

void computeVertFromHplaneAct(PointCutsSolverInterface* solver, vector<double> &a0,
    int numSplits, int numCols, double** &vertFromHAct,
    int** &vertFromHActIndices, vector<vector<double> > &raysOfC1,
    int* &numVert, vector<vector<vector<int> > > &raysToBeCutByHplane,
    vector<vector<int> > &hplanesIndxToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    vector<vector<int> > &hplanesRowIndxToBeActivated,
    FILE* inst_info_out) {

  int count;
  double hplaneRHS = 0.0;
  const double* UB = solver.getColUpper();
  const double* LB = solver.getColLower();
  for (int s = 0; s < numSplits; s++) {
    numVert[s] = 0;
    for (unsigned h = 0; h < raysToBeCutByHplane[s].size(); h++) {
      numVert[s] += raysToBeCutByHplane[s][h].size();
    }
    vertFromHAct[s] = new double[numVert[s]];
    vertFromHActIndices[s] = new int[numVert[s]];
    count = 0;
    for (unsigned h = 0; h < raysToBeCutByHplane[s].size(); h++) {
      if (hplanesRowIndxToBeActivated[s][h] != -1) {
        for (unsigned r = 0; r < raysToBeCutByHplane[s][h].size();
            r++) {
          if (hplanesToBeActivatedUBFlag[s][h] == 1)
            hplaneRHS = a0[hplanesRowIndxToBeActivated[s][h]]
                - UB[hplanesIndxToBeActivated[s][h]];
          else
            hplaneRHS = fabs(a0[hplanesRowIndxToBeActivated[s][h]]);
          vertFromHAct[s][count] =
              hplaneRHS
                  / (-raysOfC1[raysToBeCutByHplane[s][h][r]][hplanesRowIndxToBeActivated[s][h]]);
          vertFromHActIndices[s][count] =
              raysToBeCutByHplane[s][h][r];

          if (vertFromHAct[s][count] <= -param.getEPS()) {

#ifdef TRACE
            printf(
                "hRow = %d,hUB = %d, rayIndx = %d,hRHS = %lf, ahq = %lf\n",
                hplanesRowIndxToBeActivated[s][h],
                hplanesToBeActivatedUBFlag[s][h],
                raysToBeCutByHplane[s][h][r], hplaneRHS,
                -raysOfC1[raysToBeCutByHplane[s][h][r]][hplanesRowIndxToBeActivated[s][h]]);
#endif
          }
          if (vertFromHAct[s][count] < -param.getEPS()) {
            char errorstring[300];
            snprintf(errorstring,
                sizeof(errorstring) / sizeof(char),
                "*** ERROR: In computeVertFromHplaneAct: vertFromHAct[%d][%d] should be positive. It is %e.\n",
                s, count, vertFromHAct[s][count]);
            cerr << errorstring << endl;
            writeErrorToII(errorstring, inst_info_out);
            exit(1);
          }
//          assert(vertFromHAct[s][count] >= -param.getEPS());
          count++;
        }
      } else {
        vertFromHAct[s][count] = UB[hplanesIndxToBeActivated[s][h]]
            - LB[hplanesIndxToBeActivated[s][h]];
        vertFromHActIndices[s][count] = raysToBeCutByHplane[s][h][0];
        if (raysToBeCutByHplane[s][h].size() != 1) {
          char errorstring[300];
          snprintf(errorstring, sizeof(errorstring) / sizeof(char),
              "*** ERROR: In computeVertFromHplaneAct: size of raysToBeCutByHplane[%d][%d] should be 1. It is %d.\n",
              s, h, (int) raysToBeCutByHplane[s][h].size());
          cerr << errorstring << endl;
          writeErrorToII(errorstring, inst_info_out);
          exit(1);
        }
//        assert(raysToBeCutByHplane[s][h].size() == 1);
        count++;

      }
    }
  }

}

void prtVertFromHplaneAct(FILE* fptr, int numSplits, vector<int> &splitVarIndx,
    vector<int> &splitVarRowIndex, double** &vertFromHAct,
    int** &vertFromHActIndices, int* &numVert,
    vector<vector<vector<int> > > &raysToBeCutByHplane,
    vector<int> &nonBasicVarIndices,
    vector<vector<int> > &hplanesRowIndxToBeActivated) {
  int count = 0;
  fprintf(fptr,
      "These are vertices created from intersecting the rays with activated hyperplanes.\n");
  fprintf(fptr,
      "splitVarIndx,splitRowIndx,hplaneRowIndx,rayIndx,vertIndx,vertVal\n");
  for (int s = 0; s < numSplits; s++) {
    count = 0;
    for (unsigned h = 0; h < raysToBeCutByHplane[s].size(); h++) {
      for (unsigned r = 0; r < raysToBeCutByHplane[s][h].size(); r++) {
        fprintf(fptr, "%d,%d,%d,%d,%d,%lf\n", splitVarIndx[s],
            splitVarRowIndex[s], hplanesRowIndxToBeActivated[s][h],
            nonBasicVarIndices[raysToBeCutByHplane[s][h][r]],
            vertFromHActIndices[s][count], vertFromHAct[s][count]);
        count++;
      }
    }

  }

}

void printInterPtsAndRaysInJSpace(FILE* fptr, SolutionInfo &solnInfo,
    chooseCutGeneratingSets &chooseSplits,
    CoinPackedMatrix**& interPtsAndRays,
    vector<vector<int> > &pointOrRayFlag, std::vector<int>& numPointsOrRays,
    vector<vector<ptOrRayInfo*> > &intPtOrRayInfo) {
  int numSplits = chooseSplits.nEffCutGenSets;

  fprintf(fptr,
      "Contains information about the intersection points and rays created from partial hyperplane activation\n\n");

  fprintf(fptr, "splitIndx,numPoints,numRays,Total\n");
  int numPoints, numRays;
  for (int s = 0; s < numSplits; s++) {
    numPoints = 0;
    numRays = 0;
    for (int r = 0; r < numPointsOrRays[s]; r++) {
      if (pointOrRayFlag[s][r] == 1)
        numPoints++;
      else
        numRays++;
    }
    fprintf(fptr, "%d,%d,%d,%d\n", chooseSplits.splitVarIndex[s], numPoints,
        numRays, numPoints + numRays);
  }
  fprintf(fptr, "\n\n");

  int numElmts;
  //, count;
  fprintf(fptr,
      "splitIndx,No,cutRayNBColIndx,cutRayVarIndx,hplaneRowIndx,newRayNBColIndx,newRayVarIndx,PtFlag,indx1,val1,indx2,val2\n");
  //  fprintf(fptr, ",,,");
  //  for (int j = 0; j < numCols; j++)
  //    fprintf(fptr, "%d,", j);
  //  fprintf(fptr, "\n");
  vector<bool> flag(solnInfo.numNB);
  vector<int> index(solnInfo.numNB);

  for (int s = 0; s < numSplits; s++) {
    for (int r = 0; r < numPointsOrRays[s]; r++) {
      if ((*intPtOrRayInfo[s][r]).cutRayColIndx != -1)
        fprintf(fptr, "%d,%d,%d,%d,%d,%d,%d,%d,",
            chooseSplits.splitVarIndex[s], r,
            (*intPtOrRayInfo[s][r]).cutRayColIndx,
            solnInfo.nonBasicVarIndex[(*intPtOrRayInfo[s][r]).cutRayColIndx],
            (*intPtOrRayInfo[s][r]).hplaneRowIndx,
            (*intPtOrRayInfo[s][r]).newRayColIndx,
            solnInfo.nonBasicVarIndex[(*intPtOrRayInfo[s][r]).newRayColIndx],
            pointOrRayFlag[s][r]);
      else {
        fprintf(fptr, "%d,%d,%d,-1,%d,%d,%d,%d,",
            chooseSplits.splitVarIndex[s], r,
            (*intPtOrRayInfo[s][r]).cutRayColIndx,
            (*intPtOrRayInfo[s][r]).hplaneRowIndx,
            (*intPtOrRayInfo[s][r]).newRayColIndx,
            solnInfo.nonBasicVarIndex[(*intPtOrRayInfo[s][r]).newRayColIndx],
            pointOrRayFlag[s][r]);
      }

      const CoinShallowPackedVector tmpVector =
          (*interPtsAndRays[s]).getVector(r);
      numElmts = tmpVector.getNumElements();
      const int* elmtIndices = tmpVector.getIndices();
      const double* elmtVals = tmpVector.getElements();
      //
      //      fill(flag.begin(), flag.end(), false);
      //      for (int k = 0; k < numElmts; k++) {
      //        flag[elmtIndices[k]] = true;
      //        index[elmtIndices[k]] = k;
      //      }
      //
      //      count = 0;
      //      for (int j = 0; j < solnInfo.numNonBasicCols; j++) {
      //        //        if(flag[j] == false) fprintf(fptr, "0.0,");
      //        //        else{
      //        //          fprintf(fptr, "%lf,", elmtVals[count]);
      //        //          count++;
      //        //        }
      //        if (flag[j] == true) {
      //          fprintf(fptr, "%d,%lf,", elmtIndices[index[j]],
      //              elmtVals[index[j]]);
      //          count++;
      //        }
      //
      //      }
      for (int k = 0; k < numElmts; k++) {
        fprintf(fptr, "%d,%lf,", elmtIndices[k], elmtVals[k]);
      }
      fprintf(fptr, "\n");
    }
  }

}
