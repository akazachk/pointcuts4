/*
 * chooseCutGeneratingSets.hpp
 *
 *  Created on: Jun 12, 2012
 *      Author: selva
 */

//#ifndef CHOOSECUTGENERATINGSETS_HPP_
//#define CHOOSECUTGENERATINGSETS_HPP_
#pragma once

#include <vector>
#include <string>
#include "SolutionInfo.hpp"
#include "typedefs.hpp"

class chooseCutGeneratingSets {
public:
  int nEffCutGenSets;
  std::vector<int> splitVarRowIndex, splitVarIndex, splitVarOldRowIndex;
  std::vector<double> splitVarValue, pi0;
  std::vector<std::string> splitVarName;
  chooseCutGeneratingSets();
  virtual ~chooseCutGeneratingSets();
  void chooseSplits(const PointCutsSolverInterface* const solver,
      const SolutionInfo &solnInfo,
      const std::vector<int> &oldRowIndices);
};

//#endif /* CHOOSECUTGENERATINGSETS_HPP_ */
