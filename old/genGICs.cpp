/*
 * generateGICs.cpp
 *
 *  Created on: Jun 21, 2012
 *      Author: selva
 */

#include "genGICs.hpp"

void initializeIntPtObjectives(int numSplits, list<ptSortInfo*> &interPtObjFns,
    CoinPackedMatrix** &interPtsAndRays,
    vector<vector<int> > &pointOrRayFlag, std::vector<int>& numPointsOrRays,
    vector<double> &nonBasicRedCosts, double &LPOpt) {

  int tmpNElmts;
  ptSortInfo *tmpPtSortInfo;
  for (int s = 0; s < numSplits; s++) {
    for (int p = 0; p < numPointsOrRays[s]; p++) {
      if (pointOrRayFlag[s][p] == 1) {
        tmpPtSortInfo = new ptSortInfo();
        const CoinShallowPackedVector tempRow =
            (*interPtsAndRays[s]).getVector(p);
        const double *tmpElmts = tempRow.getElements();
        const int *tmpIndices = tempRow.getIndices();
        tmpNElmts = tempRow.getNumElements();
        computeInterPtCost((*tmpPtSortInfo).cost, tmpElmts, tmpIndices,
            tmpNElmts, nonBasicRedCosts, LPOpt);
        (*tmpPtSortInfo).splitIndx = s;
        (*tmpPtSortInfo).ptIndex = p;
        interPtObjFns.push_back(tmpPtSortInfo);
      }
    }
  }
}

void computeInterPtCost(double &cost, const double *ptElmts,
    const int *ptIndices, int nPtElmnts, vector<double> &nonBasicRedCosts,
    double &LPOpt) {

  cost = LPOpt;
  for (int n = 0; n < nPtElmnts; n++) {
    cost += ptElmts[n] * nonBasicRedCosts[ptIndices[n]];
  }

}

void deletePtObjsCutOffByCut(cut &tmpCut, PointCutsSolverInterface* &cutLPSolvers,
    list<ptSortInfo*> &interPtObjFns) {

  int tmpNElmnts;
  double LHS = 0.0;
  list<ptSortInfo*>::iterator it = interPtObjFns.begin();
  while (it != interPtObjFns.end()) {
    const CoinPackedMatrix *tmpMatrix =
        cutLPSolvers[(**it).splitIndx].getMatrixByRow();
    const CoinShallowPackedVector tmpRow = (*tmpMatrix).getVector(
        (**it).ptIndex);
    const double* tmpRowElmts = tmpRow.getElements();
    const int* tmpRowIndices = tmpRow.getIndices();
    tmpNElmnts = tmpRow.getNumElements();
    LHS = 0.0;
    for (int n = 0; n < tmpNElmnts; n++) {
      LHS += tmpCut.coeff[tmpRowIndices[n]] * tmpRowElmts[n];
    }
    if (LHS <= tmpCut.RHS + param.getEPS()) {
      it = interPtObjFns.erase(it);
    } else
      it++;
  }
}

void findFurthestSplit(int numSplits, int splitIndx, const double *LB,
    const double *UB, ptOrRayInfo* intPtOrRayInfo,
    vector<vector<double> > &raysOfC1,
    vector<vector<int> > &hplanesIndxToBeActivated,
    vector<vector<int> > &hplanesRowIndxToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag, vector<double> &a0,
    vector<double> &splitVarValue, vector<int> &splitRowIndx_t,
    vector<int> &splitVarIndex, vector<double> &pi0,
    int &deepestSplitIndx) {

  int cutRayColIndx = (*intPtOrRayInfo).cutRayColIndx;
  int newRayColIndx = (*intPtOrRayInfo).newRayColIndx;

  double ptVal = 0.0;
  deepestSplitIndx = -1;
  if (cutRayColIndx == -1) {
    double deepestVal = 0.0;
    //    int deepestSplitIndx = 0;
    for (int s = 0; s < numSplits; s++) {
      if (raysOfC1[newRayColIndx][splitRowIndx_t[s]] >= param.getEPS()) {
        ptVal = (pi0[s] + 1 - splitVarValue[s])
            / (raysOfC1[newRayColIndx][splitRowIndx_t[s]]);
      } else {
        if (raysOfC1[newRayColIndx][splitRowIndx_t[s]] <= -param.getEPS()) {
          ptVal = (pi0[s] - splitVarValue[s])
              / (raysOfC1[newRayColIndx][splitRowIndx_t[s]]);
        } else {
          deepestSplitIndx = s;
          break;
        }
      }
      if (ptVal >= deepestVal + param.getEPS()) {
        deepestVal = ptVal;
        deepestSplitIndx = s;
      }
    }
  } else {
    int hplaneIndx = (*intPtOrRayInfo).hplaneIndx;
    int hplaneRowIndx = hplanesRowIndxToBeActivated[splitIndx][hplaneIndx];
    double rayDirnInJSpace = 0.0, hplaneRHS = 0.0;
    double deepestVal = 0.0, theta = 0.0;
    //    int deepestSplitIndx = 0;
    int hplanUBFlag = hplanesToBeActivatedUBFlag[splitIndx][hplaneIndx];
    int hplaneVarIndx = hplanesIndxToBeActivated[splitIndx][hplaneIndx];

    //    cout << "splitIndx = " << splitIndx << "\n";
    //    cout << "hplaneVarIndx = "
    //        << hplaneVarIndx << "\n";
    //    cout << "hplaneRowIndx = " << hplaneRowIndx << "\n";
    //    cout << "cutRayColIndx = " << cutRayColIndx << "\n";
    //    cout << "newRayColIndx = " << newRayColIndx << "\n";
    if (hplaneRowIndx != -1) {
      for (int s = 0; s < numSplits; s++) {
        rayDirnInJSpace =
            -raysOfC1[newRayColIndx][splitRowIndx_t[s]]
                - (-raysOfC1[cutRayColIndx][splitRowIndx_t[s]]
                    * (raysOfC1[newRayColIndx][hplaneRowIndx]
                        / raysOfC1[cutRayColIndx][hplaneRowIndx]));
        if (rayDirnInJSpace <= param.getEPS() && rayDirnInJSpace >= -param.getEPS()) {
          deepestSplitIndx = s;
          break;
        } else {
          if (hplanUBFlag == 1) {
            hplaneRHS = a0[hplaneRowIndx] - UB[hplaneVarIndx];
          } else {
            hplaneRHS = a0[hplaneRowIndx];
          }
          if (rayDirnInJSpace >= param.getEPS())
            theta =
                (splitVarValue[s] - pi0[s]
                    - (-raysOfC1[cutRayColIndx][splitRowIndx_t[s]])
                        * (hplaneRHS
                            / (-raysOfC1[cutRayColIndx][hplaneRowIndx])))
                    / rayDirnInJSpace;

          if (rayDirnInJSpace <= -param.getEPS())
            theta =
                (splitVarValue[s] - (pi0[s] + 1)
                    - (-raysOfC1[cutRayColIndx][splitRowIndx_t[s]])
                        * (hplaneRHS
                            / (-raysOfC1[cutRayColIndx][hplaneRowIndx])))
                    / rayDirnInJSpace;
          if (theta >= deepestVal + param.getEPS()) {
            deepestVal = theta;
            deepestSplitIndx = s;
          }
        }
      }
    } else {
      for (int s = 0; s < numSplits; s++) {
        rayDirnInJSpace = -raysOfC1[newRayColIndx][splitRowIndx_t[s]];
        if (rayDirnInJSpace <= param.getEPS() && rayDirnInJSpace >= -param.getEPS()) {
          deepestSplitIndx = s;
          break;
        } else {
          if (rayDirnInJSpace >= param.getEPS())
            theta =
                (splitVarValue[s] - pi0[s]
                    - (-raysOfC1[cutRayColIndx][splitRowIndx_t[s]])
                        * (UB[hplaneVarIndx]
                            - LB[hplaneVarIndx]))
                    / rayDirnInJSpace;

          if (rayDirnInJSpace <= -param.getEPS())
            theta =
                (splitVarValue[s] - (pi0[s] + 1)
                    - (-raysOfC1[cutRayColIndx][splitRowIndx_t[s]])
                        * (UB[hplaneVarIndx]
                            - LB[hplaneVarIndx]))
                    / rayDirnInJSpace;
          if (theta >= deepestVal + param.getEPS()) {
            deepestVal = theta;
            deepestSplitIndx = s;
          }

        }
      }
    }
  }
}

void initializeGICLps(int numSplits, int nNonBasicCols,
    PointCutsSolverInterface* &cutLPSolvers,
    CoinPackedMatrix** &interPtsAndRays,
    vector<vector<int> > &pointOrRayFlag, std::vector<int>& numPointsOrRays,
    double* &constLB, double* &constUB, double* &obj, double* &colLB,
    double* &colUB, FILE* inst_info_out) {
  cutLPSolvers = new PointCutsSolverInterface[numSplits];

  for (int s = 0; s < numSplits; s++) {
    constLB = new double[numPointsOrRays[s]];
    constUB = new double[numPointsOrRays[s]];
    obj = new double[nNonBasicCols];
    colLB = new double[nNonBasicCols];
    colUB = new double[nNonBasicCols];
    //    CoinPackedMatrix* tempMtx = new CoinPackedMatrix(*interPtsAndRays[s]);

    for (int p = 0; p < numPointsOrRays[s]; p++) {
      // If p is a point, we add the constraint that \alpha^T p \ge 1; for ray \ge 0.
      if (pointOrRayFlag[s][p] == 1)
        constLB[p] = 1.0;
      else
        constLB[p] = 0.0;
      constUB[p] = COIN_DBL_MAX;
    }
    for (int c = 0; c < nNonBasicCols; c++) {
      colLB[c] = -COIN_DBL_MAX;
      colUB[c] = COIN_DBL_MAX;
      obj[c] = 0.0;
    }
    cutLPSolvers[s].assignProblem(interPtsAndRays[s], colLB, colUB, obj,
        constLB, constUB);
    //    cutLPSolvers[s].setLogLevel(0);

    if (cutLPSolvers[s].getNumCols() != nNonBasicCols) {
      char errorstring[300];
      snprintf(errorstring, sizeof(errorstring) / sizeof(char),
          "*** ERROR: In initializeGICLPs: Number of columns of cutLPSolver %d should be equal to the number of non-basic columns. Instead, cutLPSolvers[s].getNumCols() is %d, and numNonBasicCols is %d.\n",
          s, cutLPSolvers[s].getNumCols(), nNonBasicCols);
      cerr << errorstring << endl;
      writeErrorToLog(errorstring, inst_info_out);
      exit(1);
    }

#ifndef TRACE
    cutLPSolvers[s].messageHandler()->setLogLevel(0);
    cutLPSolvers[s].getModelPtr()->messageHandler()->setLogLevel(0);
#endif
  }
}

//Generate GICs using convex combination of point centroid and vertices
void generateGICsUsingPts(PointCutsSolverInterface* &cutLPSolvers,
    double** &vertFromHAct, int** &vertFromHActIndices, int* &numVert,
    SolutionInfo &solnInfo, chooseCutGeneratingSets &chooseSplits,
    vector<cut> &cutStore, vector<vector<int> > cutsSpecificToSplits,
    double thIncr, vector<int>& unbnddVertFails,
    vector<int>& nonUnqiueVertFails, vector<int>& vertNotCut,
    CoinPackedMatrix** &interPtsAndRays,
    vector<vector<int> > &pointOrRayFlag, int* &numPointsOrRays,
    FILE *inst_info_out) {
  int numSplits = chooseSplits.nEffCutGenSets;
  vector<double> centroid(solnInfo.numNB);
  vector<double> cutVertex(solnInfo.numNB, 0.0);
  vector<vector<bool> > cutSuccess(numSplits);
  cut tmpCut;
  tmpCut.coeff.resize(solnInfo.numNB);
  double th;
  bool unBndd = false, vertCut = true;
  int numElmts;
  double newObjVal = 0.0;

  //  FILE* fp = fopen("cutsObj.txt", "w");
  for (int s = 0; s < numSplits; s++) {
    for (int j = 0; j < solnInfo.numNB; j++)
      cutLPSolvers[s].setObjCoeff(j, 1.0);
    //    if (s == 1)
    //      cutLPSolvers[s].writeLp(fp);
    cutLPSolvers[s].initialSolve();
    const double* Soln = cutLPSolvers[s].getColSolution();
    if (cutLPSolvers[s].isProvenOptimal()) {
      if (addCutToStore(s, solnInfo.numNB, tmpCut, cutStore,
          Soln, cutLPSolvers[s].getObjValue(), -1, -1.0,
          chooseSplits)) {
        cutsSpecificToSplits[s].push_back(cutStore.size() - 1);
      }
    } else {
#ifdef TRACE
      printf("average cut coefficient is unbounded!!!, split Index: %d\n",
          s);
#endif
      //      exit(1);
    }
    for (int j = 0; j < solnInfo.numNB; j++)
      cutLPSolvers[s].setObjCoeff(j, 0.0);

    //    cout << "*******" << (*tmpMatrix).getNumCols() << "\t"
    //        << numPointsOrRays[s] << "\n";
    for (int p = 0; p < numPointsOrRays[s]; p++) {
      cutSuccess[s].resize(numPointsOrRays[s], false);
      if (pointOrRayFlag[s][p] == 1) {
        vertCut = true;
        //        fprintf(fp, "****p = %d\n,", p);

        //get intersection point
        unBndd = true;
        th = thIncr;
        while (!cutSuccess[s][p] && th <= 1.0 - param.getEPS() && vertCut) {
          //        while (th <= 1.0 - param.getEPS() && vertCut) {
          unBndd = true;
          const CoinPackedMatrix* tmpMatrix =
              cutLPSolvers[s].getMatrixByRow();
          const CoinShallowPackedVector tmpVector =
              (*tmpMatrix).getVector(p);
          numElmts = tmpVector.getNumElements();
          const int* elmtIndices = tmpVector.getIndices();
          const double* elmtVals = tmpVector.getElements();
          for (int i = 0; i < numElmts; i++) {
            newObjVal = th * elmtVals[i];
            cutLPSolvers[s].setObjCoeff(elmtIndices[i], newObjVal);
          }
          //          if (s == 1) {
          //            fprintf(fp, "*********th = %lf\n\n", th);
          //            cutLPSolvers[s].writeLp(fp);
          //          }
          //cutLPSolvers[s].resolve();
          cutLPSolvers[s].initialSolve();
          if (cutLPSolvers[s].isProvenPrimalInfeasible()) {
#ifdef TRACE
            printf("*** ERROR: Cut LP is infeasible\n");
#endif
            writeErrorToLog(
                "*** ERROR: Cut LP is infeasible in generateGICsUsingPts.\n",
                inst_info_out);
            exit(1);
          }
          if (cutLPSolvers[s].isProvenOptimal()) {
            unBndd = false;
            if (cutLPSolvers[s].getObjValue() <= 1.0 - param.getEPS()) {
              const double* Soln =
                  cutLPSolvers[s].getColSolution();
              //              fprintf(fp, "th = %lf\n,", th);
              //              for (int c = 0; c < nNonBasicCols; c++)
              //                fprintf(fp, "%lf,", Soln[c]);
              //              fprintf(fp, "\n");
              if (addCutToStore(s, solnInfo.numNB,
                  tmpCut, cutStore, Soln,
                  cutLPSolvers[s].getObjValue(), p, th,
                  chooseSplits)) {
                cutSuccess[s][p] = true;
                cutsSpecificToSplits[s].push_back(
                    cutStore.size() - 1);
              }
            } else
              vertCut = false;
          }
          th += thIncr;
        }
        const CoinPackedMatrix* tmpMatrix =
            cutLPSolvers[s].getMatrixByRow();
        const CoinShallowPackedVector tmpVector =
            (*tmpMatrix).getVector(p);
        numElmts = tmpVector.getNumElements();
        const int* elmtIndices = tmpVector.getIndices();
        for (int i = 0; i < numElmts; i++) {
          newObjVal = 0.0;
          cutLPSolvers[s].setObjCoeff(elmtIndices[i], newObjVal);
        }
        if (unBndd)
          unbnddVertFails[s]++;
        else {
          if (!vertCut)
            vertNotCut[s]++;
          else if (!cutSuccess[s][p])
            nonUnqiueVertFails[s]++;
        }

      }
    }
  }
  //  fclose(fp);
}

void genCutsBasedOnMultCriteria(int nCutsPerSplit,
    PointCutsSolverInterface* solver, PointCutsSolverInterface* &cutLPSolvers,
    SolutionInfo &solnInfo, chooseCutGeneratingSets &chooseSplits,
    vector<vector<vector<int> > > &raysToBeCutByHplane,
    vector<vector<int> > &raysToBeCut,
    vector<vector<int> > &hplanesIndxToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    vector<vector<int> > &hplanesRowIndxToBeActivated, int numSplits,
    int nNonBasicCols, int numCols, vector<cut> &cutStore,
    vector<vector<int> > &cutsSpecificToSplits,
    vector<vector<vector<int> > > &strongPtIndices, double thIncr,
    vector<vector<int> > &strongVertHplaneIndex, FILE* inst_info_out) {

  int nCutsGenerated = 0;
  cut tmpCut;
  tmpCut.coeff.resize(nNonBasicCols);
  int maxPoints;
  //Generate cuts based on average cut coefficient
  for (int s = 0; s < numSplits; s++) {
    nCutsGenerated = 0;
    //    genGICsAvgCutCoeffBestBasis(s, nCutsPerSplit, nCutsGenerated, tmpCut,
    //        solver, cutLPSolvers[s], a0, raysOfC1, raysToBeCutByHplane[s],
    //        hplanesIndxToBeActivated[s], hplanesToBeActivatedUBFlag[s],
    //        hplanesRowIndxToBeActivated[s], numSplits, nNonBasicCols,
    //        numCols, cutStore[s]);
    genGICsAvgCutCoeffStrongBasis(s, nCutsPerSplit, nCutsGenerated, tmpCut,
        solver, cutLPSolvers[s], solnInfo, chooseSplits,
        raysToBeCutByHplane[s], raysToBeCut[s],
        hplanesIndxToBeActivated[s], hplanesToBeActivatedUBFlag[s],
        hplanesRowIndxToBeActivated[s], numSplits, cutStore,
        cutsSpecificToSplits[s], strongVertHplaneIndex[s],
        inst_info_out);

    //Generate cuts based on vertices
    for (int j = 0; j < nNonBasicCols; j++)
      cutLPSolvers[s].setObjCoeff(j, 0.0);

    if (nCutsGenerated < nCutsPerSplit) {
      maxPoints = 0;
      for (unsigned r = 0; r < strongPtIndices[s].size(); r++)
        if ((int) strongPtIndices[s][r].size() > maxPoints)
          maxPoints = strongPtIndices[s][r].size();
      for (int p = 0; p < maxPoints; p++) {
        if (nCutsGenerated < nCutsPerSplit)
          genGICsFromStrongPoints(s, tmpCut, nNonBasicCols,
              chooseSplits, nCutsPerSplit, nCutsGenerated,
              cutLPSolvers[s], p, strongPtIndices[s], thIncr,
              cutStore, cutsSpecificToSplits[s], inst_info_out);
        else
          break;
      }
    }

  }
}

void genGICsAvgCutCoeffBestBasis(int splitIndx, int nCutsPerSplit,
    int &nCutsGenerated, cut &tmpCut, PointCutsSolverInterface* solver,
    PointCutsSolverInterface* cutLPSolvers, SolutionInfo &solnInfo,
    chooseCutGeneratingSets &chooseSplits,
    vector<vector<int> > &raysToBeCutByHplane,
    vector<int> &hplanesIndxToBeActivated,
    vector<int> &hplanesToBeActivatedUBFlag,
    vector<int> &hplanesRowIndxToBeActivated, int numSplits,
    int nNonBasicCols, int numCols, vector<cut> &cutStore,
    vector<int> &cutsSpecificToSplits, FILE *inst_info_out) {

  const double* UB = solver.getColUpper();
  double newCoord, hplaneRHS;

  for (int j = 0; j < nNonBasicCols; j++)
    cutLPSolvers.setObjCoeff(j, 1.0);
  cutLPSolvers.initialSolve();
  const double* Soln = cutLPSolvers.getColSolution();
  if (cutLPSolvers.isProvenOptimal()) {
    if (addCutToStore(splitIndx, nNonBasicCols, tmpCut, cutStore, Soln,
        cutLPSolvers.getObjValue(), -1, -1.0, chooseSplits)) {
      nCutsGenerated++;
      cutsSpecificToSplits.push_back(cutStore.size() - 1);
    }
  }
  for (unsigned h = 0; h < raysToBeCutByHplane.size(); h++) {
    if (nCutsGenerated < nCutsPerSplit) {
      for (unsigned r = 0; r < 1; r++) {
        if (hplanesToBeActivatedUBFlag[h] == 1)
          hplaneRHS = solnInfo.a0[hplanesRowIndxToBeActivated[h]]
              - UB[hplanesIndxToBeActivated[h]];
        else
          hplaneRHS = solnInfo.a0[hplanesRowIndxToBeActivated[h]];
        newCoord =
            hplaneRHS
                / (-solnInfo.raysOfC1[raysToBeCutByHplane[h][r]][hplanesRowIndxToBeActivated[h]]);
        for (int j = 0; j < raysToBeCutByHplane[h][r]; j++) {
          newCoord -=
              solnInfo.raysOfC1[j][hplanesRowIndxToBeActivated[h]]
                  / solnInfo.raysOfC1[raysToBeCutByHplane[h][r]][hplanesRowIndxToBeActivated[h]];
        }
        for (int j = raysToBeCutByHplane[h][r] + 1; j < nNonBasicCols;
            j++) {
          newCoord -=
              solnInfo.raysOfC1[j][hplanesRowIndxToBeActivated[h]]
                  / solnInfo.raysOfC1[raysToBeCutByHplane[h][r]][hplanesRowIndxToBeActivated[h]];
        }
        newCoord -=
            (1.0
                / (-solnInfo.raysOfC1[raysToBeCutByHplane[h][r]][hplanesRowIndxToBeActivated[h]]));

        //        assert(newCoord >= -param.getEPS());
        cutLPSolvers.setObjCoeff(raysToBeCutByHplane[h][r], newCoord);
        cutLPSolvers.initialSolve();

        if (cutLPSolvers.isProvenOptimal()) {
          const double* Soln = cutLPSolvers.getColSolution();
          if (addCutToStore(splitIndx, nNonBasicCols, tmpCut,
              cutStore, Soln, cutLPSolvers.getObjValue(), -1,
              -1.0, chooseSplits)) {
            nCutsGenerated++;
            cutsSpecificToSplits.push_back(cutStore.size() - 1);
          }

        } else {
          if (cutLPSolvers.isProvenPrimalInfeasible()) {
#ifdef TRACE
            printf(
                "*** ERROR: Problem is infeasible, change objective\n");
#endif
            writeErrorToLog(
                "*** ERROR: Problem is infeasible, change objective in genGICsAvgCutCoeffBestBasis.\n",
                inst_info_out);
            exit(1);
          }
        }

        cutLPSolvers.setObjCoeff(raysToBeCutByHplane[h][r], 1.0);
      }
    } else
      break;
  }
}

void genGICsAvgCutCoeffStrongBasis(int splitIndx, int nCutsPerSplit,
    int &nCutsGenerated, cut &tmpCut, PointCutsSolverInterface* solver,
    PointCutsSolverInterface* cutLPSolvers, SolutionInfo &solnInfo,
    chooseCutGeneratingSets &chooseSplits,
    vector<vector<int> > &raysToBeCutByHplane, vector<int> &raysToBeCut,
    vector<int> &hplanesIndxToBeActivated,
    vector<int> &hplanesToBeActivatedUBFlag,
    vector<int> &hplanesRowIndxToBeActivated, int numSplits,
    vector<cut> &cutStore, vector<int> &cutsSpecificToSplits,
    vector<int> &strongVertHplaneIndex, FILE* inst_info_out) {

  const double* UB = solver.getColUpper();
  double newCoord, hplaneRHS;
  //  vector<double> &objCoeff(nNonBasicCols);
  if (nCutsGenerated < nCutsPerSplit) {
    for (int j = 0; j < solnInfo.numNB; j++)
      cutLPSolvers.setObjCoeff(j, 1.0);
    cutLPSolvers.initialSolve();
    if (cutLPSolvers.isProvenOptimal()) {
      const double* Soln = cutLPSolvers.getColSolution();
      if (addCutToStore(splitIndx, solnInfo.numNB, tmpCut,
          cutStore, Soln, cutLPSolvers.getObjValue(), -1, -1.0,
          chooseSplits)) {
        nCutsGenerated++;
        cutsSpecificToSplits.push_back(cutStore.size() - 1);
      }
    }
  }

  for (unsigned r = 0; r < raysToBeCut.size(); r++) {
    if (nCutsGenerated < nCutsPerSplit) {
      int h = strongVertHplaneIndex[r];
      if (hplanesRowIndxToBeActivated[h] != -1) {
        if (nCutsGenerated < nCutsPerSplit) {
          if (hplanesToBeActivatedUBFlag[h] == 1)
            hplaneRHS = solnInfo.a0[hplanesRowIndxToBeActivated[h]]
                - UB[hplanesIndxToBeActivated[h]];
          else
            hplaneRHS = solnInfo.a0[hplanesRowIndxToBeActivated[h]];
          newCoord =
              hplaneRHS
                  / (-solnInfo.raysOfC1[raysToBeCut[r]][hplanesRowIndxToBeActivated[h]]);
          for (int j = 0; j < raysToBeCut[r]; j++) {
            newCoord -=
                solnInfo.raysOfC1[j][hplanesRowIndxToBeActivated[h]]
                    / solnInfo.raysOfC1[raysToBeCut[r]][hplanesRowIndxToBeActivated[h]];
          }
          for (int j = raysToBeCut[r] + 1;
              j < solnInfo.numNB; j++) {
            newCoord -=
                solnInfo.raysOfC1[j][hplanesRowIndxToBeActivated[h]]
                    / solnInfo.raysOfC1[raysToBeCut[r]][hplanesRowIndxToBeActivated[h]];
          }
          newCoord -=
              (1.0
                  / (-solnInfo.raysOfC1[raysToBeCut[r]][hplanesRowIndxToBeActivated[h]]));

          //        assert(newCoord >= -param.getEPS());
          //      cout << "newCoord = " << newCoord << "\n";
          cutLPSolvers.setObjCoeff(raysToBeCut[r], newCoord);
          cutLPSolvers.initialSolve();

          if (cutLPSolvers.isProvenOptimal()) {
            const double* Soln = cutLPSolvers.getColSolution();
            if (addCutToStore(splitIndx, solnInfo.numNB,
                tmpCut, cutStore, Soln,
                cutLPSolvers.getObjValue(), -1, -1.0,
                chooseSplits)) {
              nCutsGenerated++;
              cutsSpecificToSplits.push_back(cutStore.size() - 1);
            }

          } else {
            char errorstring[300];
            if (cutLPSolvers.isProvenDualInfeasible()) {
              snprintf(errorstring,
                  sizeof(errorstring) / sizeof(char),
                  "In genGICsAvgCutCoeffStrongBasis: *** ERROR: Problem is proven dual infeasible after changed objective.\n");
              cerr << errorstring << endl;
            } else if (cutLPSolvers.isProvenPrimalInfeasible()) {
              snprintf(errorstring,
                  sizeof(errorstring) / sizeof(char),
                  "In genGICsAvgCutCoeffStrongBasis: *** ERROR: Problem is proven primal infeasible after changed objective.\n");
              cerr << errorstring << endl;
//              cutLPSolvers.writeLp("cutLp-changeobj", "lp");
              writeErrorToLog(errorstring, inst_info_out);
              exit(1);
            } else {
              snprintf(errorstring,
                  sizeof(errorstring) / sizeof(char),
                  "In genGICsAvgCutCoeffStrongBasis: *** ERROR: Problem is infeasible (but not proven primal or dual infeasible) after changed objective.\n");
              cerr << errorstring << endl;
              writeErrorToLog(errorstring, inst_info_out);
            }
          }

          cutLPSolvers.setObjCoeff(raysToBeCut[r], 1.0);
        } else
          break;
      }
    }
  }

}

void genGICsFromStrongPoints(int splitIndx, cut tmpCut, int nNonBasicCols,
    chooseCutGeneratingSets &chooseSplits, int nCutsPerSplit,
    int &nCutsGenerated, PointCutsSolverInterface* cutLPSolvers, int pointIndx,
    vector<vector<int> > &strongPtIndices, double thIncr,
    vector<cut> &cutStore, vector<int> &cutsSpecificToSplits,
    FILE *inst_info_out) {

  double newObjVal;
  int numElmts;
  for (unsigned r = 0; r < strongPtIndices.size(); r++) {
    if (nCutsGenerated < nCutsPerSplit) {
      if (pointIndx < (int) strongPtIndices[r].size()) {
        const CoinPackedMatrix* tmpMatrix =
            cutLPSolvers.getMatrixByRow();
        const CoinShallowPackedVector tmpVector =
            (*tmpMatrix).getVector(strongPtIndices[r][pointIndx]);
        numElmts = tmpVector.getNumElements();
        const int* elmtIndices = tmpVector.getIndices();
        const double* elmtVals = tmpVector.getElements();
        cutLPSolvers.enableSimplexInterface(true);
        for (int i = 0; i < numElmts; i++) {
          newObjVal = thIncr * elmtVals[i];
          cutLPSolvers.setObjCoeff(elmtIndices[i], newObjVal);
        }
        cutLPSolvers.disableSimplexInterface();

        cutLPSolvers.resolve();
        if (cutLPSolvers.isProvenPrimalInfeasible()) {
#ifdef TRACE
          printf("*** ERROR: Cut LP is infeasible\n");
#endif
          writeErrorToLog(
              "*** ERROR: Cut LP is infeasible in genGICsFromStrongPoints.\n",
              inst_info_out);
          exit(1);
        }

        if (cutLPSolvers.isProvenDualInfeasible()) {
#ifdef TRACE
          printf("*** ERROR: This LP cannot be unbounded\n");
#endif
          writeErrorToLog(
              "*** ERROR: This LP cannot be unbounded in genGICsFromStrongPoints.\n",
              inst_info_out);
          exit(1);
        }
        if (cutLPSolvers.isProvenOptimal()) {
          if (cutLPSolvers.getObjValue() <= 1.0 - param.getEPS()) {
            const double* Soln = cutLPSolvers.getColSolution();
            if (addCutToStore(splitIndx, nNonBasicCols, tmpCut,
                cutStore, Soln, cutLPSolvers.getObjValue(),
                strongPtIndices[r][pointIndx], thIncr,
                chooseSplits)) {
              nCutsGenerated++;
              cutsSpecificToSplits.push_back(cutStore.size() - 1);
            }

          }

        }
        const CoinPackedMatrix* tmpMatrix1 =
            cutLPSolvers.getMatrixByRow();
        const CoinShallowPackedVector tmpVector1 =
            (*tmpMatrix1).getVector(strongPtIndices[r][pointIndx]);
        numElmts = tmpVector1.getNumElements();
        const int* elmtIndices1 = tmpVector1.getIndices();
        cutLPSolvers.enableSimplexInterface(true);
        for (int i = 0; i < numElmts; i++) {
          cutLPSolvers.setObjCoeff(elmtIndices1[i], 0.0);
        }
        cutLPSolvers.disableSimplexInterface();
      }
    } else
      break;
  }
}

void computeCutVertex(int numCols, vector<double> &cutVertex,
    vector<double> &centroid, int vertNZIndex, double vertNZVal,
    double th) {

  for (int j = 0; j < vertNZIndex; j++)
    cutVertex[j] = th * centroid[j];
  cutVertex[vertNZIndex] = th * centroid[vertNZIndex] + (1 - th) * vertNZVal;
  for (int j = vertNZIndex + 1; j < numCols; j++)
    cutVertex[j] = th * centroid[j];

}

//Add cut to cut store if unique
int addCutToStore(int splitIndex, int numCols, cut &tmpCut,
    vector<cut> &cutStore, const double* cutCoeff, double viol,
    int cutVertexIndx, double th, chooseCutGeneratingSets &chooseSplits) {
  int diff = 1;
//  int dupCutIndx = -1;

  for (int c = 0; c < (int) cutStore.size(); c++) {
    diff = 0;
    for (int k = 0; k < numCols; k++) {
      if (fabs(cutStore[c].coeff[k] - cutCoeff[k]) >= param.getEPS() * 1000) {
        diff = 1;
        break;
      }
    }
    if (diff == 0) {
//      dupCutIndx = c;
      break;
    }
  }
  if (diff == 1) {
    tmpCut.eucl = 0.0;
    tmpCut.cutGenVertexIndex = cutVertexIndx;
    tmpCut.viol = viol;
    tmpCut.RHS = 1.0;
    tmpCut.th = th;
    tmpCut.splitIndex = splitIndex;
    tmpCut.splitVarIndex = chooseSplits.splitVarIndex[splitIndex];
    for (int k = 0; k < numCols; k++)
      tmpCut.coeff[k] = cutCoeff[k];
    cutStore.push_back(tmpCut);
  }
  return diff;
}

//Computes centroid of vertices in vertFromHAct
void computeVertexCentroid(int numCols, double* &vertFromHAct,
    int* &vertFromHActIndices, int numVert, vector<double> &centroid) {

  for (int p = 0; p < numVert; p++)
    centroid[p] = 0.0;

  for (int p = 0; p < numVert; p++) {
    centroid[vertFromHActIndices[p]] += vertFromHAct[p];
  }
  for (int j = 0; j < numCols; j++)
    centroid[j] /= (double) numVert;
}

//Computes centroid of intersection points
void computeIntersectionPtCentroid(int numCols,
    PointCutsSolverInterface* cutLPSolver, vector<double> &ptCentroid,
    vector<int> &pointOrRayFlag, int numPointsOrRays) {
  int numPoints, numElmts;

  numPoints = 0;
  for (int p = 0; p < numPointsOrRays; p++) {
    if (pointOrRayFlag[p] == 1) {
      numPoints++;
      const CoinPackedMatrix* tmpMatrix = cutLPSolver.getMatrixByRow();
      const CoinShallowPackedVector tmpVector = (*tmpMatrix).getVector(p);
      numElmts = tmpVector.getNumElements();
      const int* elmtIndices = tmpVector.getIndices();
      const double* elmtVals = tmpVector.getElements();
      for (int j = 0; j < numElmts; j++) {
        ptCentroid[elmtIndices[j]] += elmtVals[j];
      }
    }
  }
  for (int j = 0; j < numCols; j++)
    ptCentroid[j] /= numPoints;

}

void findPtEvalOfCut(vector<cut> &cutStore, const double *elmtVals,
    const int *elmtIndices, int nElmts, double &maxFeas,
    FILE* inst_info_out) {
  maxFeas = 0.0;
  double LHS = 0.0;
  for (unsigned c = 0; c < cutStore.size(); c++) {
    LHS = 0.0;
    //    cout << "c = " << c << "\n";
    for (int i = 0; i < nElmts; i++)
      LHS += elmtVals[i] * cutStore[c].coeff[elmtIndices[i]];
    //    if (LHS <= 1.0 - param.getEPS())
    //      cout << "LHS = " << LHS << "\n";

    if (LHS < 1.0 - param.getEPS()) {
      char errorstring[300];
      snprintf(errorstring, sizeof(errorstring) / sizeof(char),
          "*** ERROR: In findPtEvalOfCut: LHS of cut %d should be at least 1.0 - param.getEPS(). It is %f.\n",
          c, LHS);
      cerr << errorstring << endl;
      writeErrorToLog(errorstring, inst_info_out);
      exit(1);
    }

    if (LHS >= maxFeas + param.getEPS())
      maxFeas = LHS;
  }
}

/**********************************************************************************
 * Cut generating methods that are not specific for each cut
 **********************************************************************************/
void genCutsBasedOnRankHeur1(int nCutsInBudget, int &nCutsGenerated,
    PointCutsSolverInterface* solver, PointCutsSolverInterface* &cutLPSolvers,
    SolutionInfo &solnInfo, chooseCutGeneratingSets &chooseSplits,
    vector<vector<vector<int> > > &raysToBeCutByHplane,
    vector<int> &allRaysCut, vector<vector<int> > &raysToBeCut,
    vector<vector<int> > &hplanesIndxToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    vector<vector<int> > &hplanesRowIndxToBeActivated,
    vector<cut> &cutStore, vector<vector<int> > &cutsSpecificToSplits,
    vector<vector<vector<int> > > &strongPtIndices, double thIncr,
    vector<vector<int> > &strongVertHplaneIndex,
    vector<double> &interPtObjVal, vector<int> &interPtObjIndex,
    vector<int> &interPtSplit, vector<vector<int> > &pointOrRayFlag,
    const std::vector<int>& numPointsOrRays, FILE *inst_info_out) {

  int numSplits = chooseSplits.nEffCutGenSets;
  nCutsGenerated = 0;
  cut tmpCut;
  tmpCut.coeff.resize(solnInfo.numNB);

  //Generate strongest cut in the direction of the optimal LP solution
  generateBestGICAvgCutOptimalBasis(tmpCut, cutLPSolvers, numSplits,
      solnInfo.numNB, cutStore, cutsSpecificToSplits,
      nCutsGenerated, chooseSplits);
#ifdef TRACE
  printf(
      "\t## Num cuts generated after generateBestGICAvgCutOptimalBasis: %d.\n\n",
      nCutsGenerated);
#endif

  //Generate cuts using C1 intersection points and centroid point objective functions
  generateGICsUsingInterPointObjs(nCutsGenerated, nCutsInBudget, tmpCut,
      cutLPSolvers, numSplits, solnInfo.numNB, chooseSplits,
      cutStore, cutsSpecificToSplits, allRaysCut, interPtObjVal,
      interPtObjIndex, interPtSplit, pointOrRayFlag, numPointsOrRays,
      thIncr, inst_info_out);
#ifdef TRACE
  printf(
      "\t## Num cuts generated after generateGICsUsingInterPointObjs: %d.\n\n",
      nCutsGenerated);
#endif

  //Generate cuts using adjacent basis
  for (int s = 0; s < numSplits; s++) {
    genGICsAvgCutCoeffStrongBasis(s, nCutsInBudget, nCutsGenerated, tmpCut,
        solver, cutLPSolvers[s], solnInfo, chooseSplits,
        raysToBeCutByHplane[s], raysToBeCut[s],
        hplanesIndxToBeActivated[s], hplanesToBeActivatedUBFlag[s],
        hplanesRowIndxToBeActivated[s], numSplits, cutStore,
        cutsSpecificToSplits[s], strongVertHplaneIndex[s],
        inst_info_out);
#ifdef TRACE
    printf(
        "\t## Num cuts generated after genGICsAvgCutCoeffStrongBasis for split %d: %d.\n\n",
        s, nCutsGenerated);
#endif
  }

  //Generate cuts using intersection points
  int maxPoints;
  for (int s = 0; s < numSplits; s++) {
    for (int j = 0; j < solnInfo.numNB; j++)
      cutLPSolvers[s].setObjCoeff(j, 0.0);
    if (nCutsGenerated < nCutsInBudget) {
      maxPoints = 0;
      for (unsigned r = 0; r < strongPtIndices[s].size(); r++)
        if ((int) strongPtIndices[s][r].size() > maxPoints)
          maxPoints = strongPtIndices[s][r].size();
      for (int p = 0; p < maxPoints; p++) {
        if (nCutsGenerated < nCutsInBudget) {
          genGICsFromStrongPoints(s, tmpCut, solnInfo.numNB,
              chooseSplits, nCutsInBudget, nCutsGenerated,
              cutLPSolvers[s], p, strongPtIndices[s], thIncr,
              cutStore, cutsSpecificToSplits[s], inst_info_out);
#ifdef TRACE
          printf(
              "\t## Num cuts generated after genGICsFromStrongPoints for split %d and point %d: %d.\n\n",
              s, p, nCutsGenerated);
#endif
        } else
          break;
      }
    }
  }

}

void genCutsBasedOnRankHeur2(int nCutsInBudget, int &nCutsGenerated,
    PointCutsSolverInterface* solver, PointCutsSolverInterface* &cutLPSolvers,
    SolutionInfo &solnInfo, chooseCutGeneratingSets &chooseSplits,
    vector<vector<vector<int> > > &raysToBeCutByHplane,
    vector<int> &allRaysCut, vector<vector<int> > &raysToBeCut,
    vector<vector<int> > &hplanesIndxToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    vector<vector<int> > &hplanesRowIndxToBeActivated,
    vector<cut> &cutStore, vector<vector<int> > &cutsSpecificToSplits,
    vector<vector<vector<int> > > &strongPtIndices, double thIncr,
    vector<vector<int> > &strongVertHplaneIndex,
    vector<double> &interPtObjVal, vector<int> &interPtObjIndex,
    vector<int> &interPtSplit, vector<vector<int> > &pointOrRayFlag,
    const std::vector<int>& numPointsOrRays, FILE *inst_info_out) {
  int numSplits = chooseSplits.nEffCutGenSets;
  nCutsGenerated = 0;
  cut tmpCut;
  tmpCut.coeff.resize(solnInfo.numNB);

  //Generate strongest cut in the direction of the optimal LP solution
  generateGICsAvgCutOptimalBasis(tmpCut, nCutsInBudget, nCutsGenerated,
      cutLPSolvers, numSplits, solnInfo.numNB, cutStore,
      cutsSpecificToSplits, chooseSplits);
  //
  //Generate cuts using C1 intersection points and centroid point objective functions
  generateGICsUsingInterPointObjs(nCutsGenerated, nCutsInBudget, tmpCut,
      cutLPSolvers, numSplits, solnInfo.numNB, chooseSplits,
      cutStore, cutsSpecificToSplits, allRaysCut, interPtObjVal,
      interPtObjIndex, interPtSplit, pointOrRayFlag, numPointsOrRays,
      thIncr, inst_info_out);

  //Generate cuts using adjacent basis
  for (int s = 0; s < numSplits; s++) {
    genGICsAvgCutCoeffStrongBasis(s, nCutsInBudget, nCutsGenerated, tmpCut,
        solver, cutLPSolvers[s], solnInfo, chooseSplits,
        raysToBeCutByHplane[s], raysToBeCut[s],
        hplanesIndxToBeActivated[s], hplanesToBeActivatedUBFlag[s],
        hplanesRowIndxToBeActivated[s], numSplits, cutStore,
        cutsSpecificToSplits[s], strongVertHplaneIndex[s],
        inst_info_out);
  }

  //Generate cuts using intersection points
  int maxPoints;
  for (int s = 0; s < numSplits; s++) {
    for (int j = 0; j < solnInfo.numNB; j++)
      cutLPSolvers[s].setObjCoeff(j, 0.0);
    if (nCutsGenerated < nCutsInBudget) {
      maxPoints = 0;
      for (unsigned r = 0; r < strongPtIndices[s].size(); r++)
        if ((int) strongPtIndices[s][r].size() > maxPoints)
          maxPoints = strongPtIndices[s][r].size();
      for (int p = 0; p < maxPoints; p++) {
        if (nCutsGenerated < nCutsInBudget)
          genGICsFromStrongPoints(s, tmpCut, solnInfo.numNB,
              chooseSplits, nCutsInBudget, nCutsGenerated,
              cutLPSolvers[s], p, strongPtIndices[s], thIncr,
              cutStore, cutsSpecificToSplits[s], inst_info_out);
        else
          break;
      }
    }
  }

}

void genCutsBasedOnRankHeur3(int nCutsInBudget, int &nCutsGenerated,
    PointCutsSolverInterface* solver, PointCutsSolverInterface* &cutLPSolvers,
    SolutionInfo &solnInfo, chooseCutGeneratingSets &chooseSplits,
    vector<vector<vector<int> > > &raysToBeCutByHplane,
    vector<int> &allRaysCut, vector<vector<int> > &raysToBeCut,
    vector<vector<int> > &hplanesIndxToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    vector<vector<int> > &hplanesRowIndxToBeActivated,
    vector<cut> &cutStore, vector<vector<int> > &cutsSpecificToSplits,
    vector<vector<vector<int> > > &strongPtIndices, double thIncr,
    vector<vector<int> > &strongVertHplaneIndex,
    vector<double> &interPtObjVal, vector<int> &interPtObjIndex,
    vector<int> &interPtSplit, vector<vector<int> > &pointOrRayFlag,
    const std::vector<int>& numPointsOrRays, FILE *inst_info_out) {
  int numSplits = chooseSplits.nEffCutGenSets;
  nCutsGenerated = 0;
  cut tmpCut;
  tmpCut.coeff.resize(solnInfo.numNB);

  //Generate strongest cuts in the direction of the optimal LP solution
  generateGICsAvgCutOptimalBasis(tmpCut, nCutsInBudget, nCutsGenerated,
      cutLPSolvers, numSplits, solnInfo.numNB, cutStore,
      cutsSpecificToSplits, chooseSplits);

  //Generate cuts using adjacent basis
  for (int s = 0; s < numSplits; s++) {
    genGICsAvgCutCoeffStrongBasis(s, nCutsInBudget, nCutsGenerated, tmpCut,
        solver, cutLPSolvers[s], solnInfo, chooseSplits,
        raysToBeCutByHplane[s], raysToBeCut[s],
        hplanesIndxToBeActivated[s], hplanesToBeActivatedUBFlag[s],
        hplanesRowIndxToBeActivated[s], numSplits, cutStore,
        cutsSpecificToSplits[s], strongVertHplaneIndex[s],
        inst_info_out);
  }

  //  //Generate cuts using C1 intersection points and centroid point objective functions
  generateGICsUsingInterPointObjs(nCutsGenerated, nCutsInBudget, tmpCut,
      cutLPSolvers, numSplits, solnInfo.numNB, chooseSplits,
      cutStore, cutsSpecificToSplits, allRaysCut, interPtObjVal,
      interPtObjIndex, interPtSplit, pointOrRayFlag, numPointsOrRays,
      thIncr, inst_info_out);

  //Generate cuts using intersection points
  int maxPoints;
  for (int s = 0; s < numSplits; s++) {
    for (int j = 0; j < solnInfo.numNB; j++)
      cutLPSolvers[s].setObjCoeff(j, 0.0);
    if (nCutsGenerated < nCutsInBudget) {
      maxPoints = 0;
      for (unsigned r = 0; r < strongPtIndices[s].size(); r++)
        if ((int) strongPtIndices[s][r].size() > maxPoints)
          maxPoints = strongPtIndices[s][r].size();
      for (int p = 0; p < maxPoints; p++) {
        if (nCutsGenerated < nCutsInBudget)
          genGICsFromStrongPoints(s, tmpCut, solnInfo.numNB,
              chooseSplits, nCutsInBudget, nCutsGenerated,
              cutLPSolvers[s], p, strongPtIndices[s], thIncr,
              cutStore, cutsSpecificToSplits[s], inst_info_out);
        else
          break;
      }
    }
  }

}

void genCutsBasedOnRankHeur4(int nCutsInBudget, int &nCutsGenerated,
    PointCutsSolverInterface* solver, PointCutsSolverInterface* &cutLPSolvers,
    SolutionInfo &solnInfo, chooseCutGeneratingSets &chooseSplits,
    vector<vector<vector<int> > > &raysToBeCutByHplane,
    vector<int> &allRaysCut, vector<vector<int> > &raysToBeCut,
    vector<vector<int> > &hplanesIndxToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    vector<vector<int> > &hplanesRowIndxToBeActivated,
    vector<cut> &cutStore, vector<vector<int> > &cutsSpecificToSplits,
    vector<vector<vector<int> > > &strongPtIndices, double thIncr,
    vector<vector<int> > &strongVertHplaneIndex,
    vector<double> &interPtObjVal, vector<int> &interPtObjIndex,
    vector<int> &interPtSplit, vector<vector<int> > &pointOrRayFlag,
    const std::vector<int>& numPointsOrRays, FILE *inst_info_out) {

  int numSplits = chooseSplits.nEffCutGenSets;
  nCutsGenerated = 0;
  cut tmpCut;
  tmpCut.coeff.resize(solnInfo.numNB);

  //Generate cuts using intersection points
  int maxPoints;
  for (int s = 0; s < numSplits; s++) {
    for (int j = 0; j < solnInfo.numNB; j++)
      cutLPSolvers[s].setObjCoeff(j, 0.0);
    if (nCutsGenerated < nCutsInBudget) {
      maxPoints = 0;
      for (unsigned r = 0; r < strongPtIndices[s].size(); r++)
        if ((int) strongPtIndices[s][r].size() > maxPoints)
          maxPoints = strongPtIndices[s][r].size();
      for (int p = 0; p < maxPoints; p++) {
        if (nCutsGenerated < nCutsInBudget)
          genGICsFromStrongPoints(s, tmpCut, solnInfo.numNB,
              chooseSplits, nCutsInBudget, nCutsGenerated,
              cutLPSolvers[s], p, strongPtIndices[s], thIncr,
              cutStore, cutsSpecificToSplits[s], inst_info_out);
        else
          break;
      }
    }
  }

  //Generate strongest cuts in the direction of the optimal LP solution
  generateGICsAvgCutOptimalBasis(tmpCut, nCutsInBudget, nCutsGenerated,
      cutLPSolvers, numSplits, solnInfo.numNB, cutStore,
      cutsSpecificToSplits, chooseSplits);

  //Generate cuts using adjacent basis
  for (int s = 0; s < numSplits; s++) {
    genGICsAvgCutCoeffStrongBasis(s, nCutsInBudget, nCutsGenerated, tmpCut,
        solver, cutLPSolvers[s], solnInfo, chooseSplits,
        raysToBeCutByHplane[s], raysToBeCut[s],
        hplanesIndxToBeActivated[s], hplanesToBeActivatedUBFlag[s],
        hplanesRowIndxToBeActivated[s], numSplits, cutStore,
        cutsSpecificToSplits[s], strongVertHplaneIndex[s],
        inst_info_out);
  }

  //Generate cuts using C1 intersection points and centroid point objective functions
  generateGICsUsingInterPointObjs(nCutsGenerated, nCutsInBudget, tmpCut,
      cutLPSolvers, numSplits, solnInfo.numNB, chooseSplits,
      cutStore, cutsSpecificToSplits, allRaysCut, interPtObjVal,
      interPtObjIndex, interPtSplit, pointOrRayFlag, numPointsOrRays,
      thIncr, inst_info_out);

}

void smartGICGeneration1(int nCutsInBudget, int &nCutsGenerated,
    SolutionInfo &solnInfo, chooseCutGeneratingSets &chooseSplits,
    PointCutsSolverInterface* &cutLPSolvers,
    vector<vector<int> > &cutsSpecificToSplits,
    list<ptSortInfo*> &interPtObjFns, vector<cut> &cutStore,
    vector<vector<ptOrRayInfo*> > &intPtOrRayInfo, const double *LB,
    const double *UB, vector<vector<int> > &hplanesIndxToBeActivated,
    vector<vector<int> > &hplanesRowIndxToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    vector<vector<vector<int> > > &strongPtIndices, double thIncr,
    FILE *inst_info_out) {

  int numSplits = chooseSplits.nEffCutGenSets;
  nCutsGenerated = 0;
  cut tmpCut;
  tmpCut.coeff.resize(solnInfo.numNB);
  tmpCut.RHS = 1.0;

  //reduced cost based cut
  vector<double> Obj(solnInfo.numNB);

  //  for (int j = 0; j < solnInfo.numNonBasicCols; j++)
  //    Obj[j] = fabs(nonBasicRedCosts[j]);
  for (int j = 0; j < solnInfo.numNB; j++)
    //    Obj[j] = 1.0;
    Obj[j] = fabs(solnInfo.nonBasicReducedCost[j]);

  for (int s = 0; s < numSplits; s++) {
    if (nCutsGenerated >= nCutsInBudget)
      break;
    cutLPSolvers[s].enableSimplexInterface(true);
//    cutLPSolvers[s].setObjectiveAndRefresh(&Obj[0]);
    cutLPSolvers[s].setObjective(&Obj[0]); // Alex added
    cutLPSolvers[s].disableSimplexInterface();
    cutLPSolvers[s].resolve();
    const double* Soln = cutLPSolvers[s].getColSolution();
    if (cutLPSolvers[s].isProvenOptimal()) {
      if (addCutToStore(s, solnInfo.numNB, tmpCut, cutStore,
          Soln, cutLPSolvers[s].getObjValue(), -1, -1.0,
          chooseSplits)) {
        cutsSpecificToSplits[s].push_back(cutStore.size() - 1);
        deletePtObjsCutOffByCut(tmpCut, cutLPSolvers, interPtObjFns);
        nCutsGenerated++;
      }
    }
  }

  //Start iterative cut generation
  list<ptSortInfo*>::iterator it = interPtObjFns.begin();
  int splitIndx, pointIndx;
  int deepestSplitIndx;
  for (int j = 0; j < solnInfo.numNB; j++)
    Obj[j] = 0.0;
  int numElmts;
  double maxFeas = 0.0;
  while (nCutsGenerated < nCutsInBudget && interPtObjFns.size() > 0) {
    it = interPtObjFns.begin();
    splitIndx = (**it).splitIndx;
    pointIndx = (**it).ptIndex;

    findFurthestSplit(numSplits, splitIndx, LB, UB,
        intPtOrRayInfo[splitIndx][pointIndx], solnInfo.raysOfC1,
        hplanesIndxToBeActivated, hplanesRowIndxToBeActivated,
        hplanesToBeActivatedUBFlag, solnInfo.a0,
        chooseSplits.splitVarValue, chooseSplits.splitVarRowIndex,
        chooseSplits.splitVarIndex, chooseSplits.pi0, deepestSplitIndx);

    if (deepestSplitIndx == -1) {
      char errorstring[300];
      snprintf(errorstring, sizeof(errorstring) / sizeof(char),
          "*** ERROR: In smartGICGeneration1: deepestSplitIndx should not be -1 for splitIndx %d and pointIndx %d.\n",
          splitIndx, pointIndx);
      cerr << errorstring << endl;
      writeErrorToLog(errorstring, inst_info_out);
      exit(1);
    }

    const CoinPackedMatrix* tmpMatrix =
        cutLPSolvers[splitIndx].getMatrixByRow();
    const CoinShallowPackedVector tmpVector = (*tmpMatrix).getVector(
        pointIndx);
    numElmts = tmpVector.getNumElements();
    const int* elmtIndices = tmpVector.getIndices();
    const double* elmtVals = tmpVector.getElements();
    findPtEvalOfCut(cutStore, elmtVals, elmtIndices, numElmts, maxFeas,
        inst_info_out);

    cutLPSolvers[deepestSplitIndx].enableSimplexInterface(true);
    for (int i = 0; i < numElmts; i++)
      Obj[elmtIndices[i]] = elmtVals[i] / maxFeas;
//    cutLPSolvers[deepestSplitIndx].setObjectiveAndRefresh(&Obj[0]);
    cutLPSolvers[deepestSplitIndx].setObjective(&Obj[0]); // Alex added
    for (int i = 0; i < numElmts; i++)
      Obj[elmtIndices[i]] = 0.0;
    cutLPSolvers[deepestSplitIndx].disableSimplexInterface();
    cutLPSolvers[deepestSplitIndx].resolve();
    const double* Soln = cutLPSolvers[deepestSplitIndx].getColSolution();
    if (cutLPSolvers[deepestSplitIndx].isProvenOptimal()) {
      if (cutLPSolvers[deepestSplitIndx].getObjValue() >= 1.0 - param.getEPS())
        interPtObjFns.erase(it);
      else {
        if (addCutToStore(deepestSplitIndx, solnInfo.numNB,
            tmpCut, cutStore, Soln,
            cutLPSolvers[deepestSplitIndx].getObjValue(), pointIndx,
            -1.0, chooseSplits)) {
          cutsSpecificToSplits[deepestSplitIndx].push_back(
              cutStore.size() - 1);
          deletePtObjsCutOffByCut(tmpCut, cutLPSolvers,
              interPtObjFns);
          nCutsGenerated++;
        } else
          interPtObjFns.erase(it);
      }
    } else {
#ifdef TRACE
      cout << "cut generating LP unbounded in smart GIC\n";
#endif
      writeErrorToLog(
          "*** ERROR: Cut generating LP unbounded in smartGICGeneration1.\n",
          inst_info_out);
      exit(1);
    }

  }

  //Generate cuts using intersection points
  int maxPoints;
  for (int s = 0; s < numSplits; s++) {
    for (int j = 0; j < solnInfo.numNB; j++)
      cutLPSolvers[s].setObjCoeff(j, 0.0);
    if (nCutsGenerated < nCutsInBudget) {
      maxPoints = 0;
      for (unsigned r = 0; r < strongPtIndices[s].size(); r++)
        if ((int) strongPtIndices[s][r].size() > maxPoints)
          maxPoints = strongPtIndices[s][r].size();
      for (int p = 0; p < maxPoints; p++) {
        if (nCutsGenerated < nCutsInBudget)
          genGICsFromStrongPoints(s, tmpCut, solnInfo.numNB,
              chooseSplits, nCutsInBudget, nCutsGenerated,
              cutLPSolvers[s], p, strongPtIndices[s], thIncr,
              cutStore, cutsSpecificToSplits[s], inst_info_out);
        else
          break;
      }
    }
  }

}

void smartGICGeneration2(int nCutsInBudget, int &nCutsGenerated,
    SolutionInfo &solnInfo, chooseCutGeneratingSets &chooseSplits,
    PointCutsSolverInterface* &cutLPSolvers,
    vector<vector<int> > &cutsSpecificToSplits,
    list<ptSortInfo*> &interPtObjFns, vector<cut> &cutStore,
    vector<vector<ptOrRayInfo*> > &intPtOrRayInfo, const double *LB,
    const double *UB, vector<vector<int> > &hplanesIndxToBeActivated,
    vector<vector<int> > &hplanesRowIndxToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    vector<vector<vector<int> > > &strongPtIndices, double thIncr,
    FILE *inst_info_out) {

  int numSplits = chooseSplits.nEffCutGenSets;
  nCutsGenerated = 0;
  cut tmpCut;
  tmpCut.coeff.resize(solnInfo.numNB);
  tmpCut.RHS = 1.0;

  //Avg cut coefficient
  vector<double> Obj(solnInfo.numNB);

  //  for (int j = 0; j < solnInfo.numNonBasicCols; j++)
  //    Obj[j] = fabs(nonBasicRedCosts[j]);
  for (int j = 0; j < solnInfo.numNB; j++)
    Obj[j] = 1.0;
  //    Obj[j] = fabs(nonBasicRedCosts[j]);

  for (int s = 0; s < numSplits; s++) {
    if (nCutsGenerated >= nCutsInBudget)
      break;
    cutLPSolvers[s].enableSimplexInterface(true);
//    cutLPSolvers[s].setObjectiveAndRefresh(&Obj[0]);
    cutLPSolvers[s].setObjective(&Obj[0]); // Alex added
    cutLPSolvers[s].disableSimplexInterface();
    cutLPSolvers[s].resolve();
    const double* Soln = cutLPSolvers[s].getColSolution();
    if (cutLPSolvers[s].isProvenOptimal()) {
      if (addCutToStore(s, solnInfo.numNB, tmpCut, cutStore,
          Soln, cutLPSolvers[s].getObjValue(), -1, -1.0,
          chooseSplits)) {
        cutsSpecificToSplits[s].push_back(cutStore.size() - 1);
        deletePtObjsCutOffByCut(tmpCut, cutLPSolvers, interPtObjFns);
        nCutsGenerated++;
      }
    }
  }

  //Start iterative cut generation
  list<ptSortInfo*>::iterator it = interPtObjFns.begin();
  int splitIndx, pointIndx;
  int deepestSplitIndx;
  for (int j = 0; j < solnInfo.numNB; j++)
    Obj[j] = 0.0;
  int numElmts;
  double maxFeas = 0.0;
  while (nCutsGenerated < nCutsInBudget && interPtObjFns.size() > 0) {
    it = interPtObjFns.begin();
    splitIndx = (**it).splitIndx;
    pointIndx = (**it).ptIndex;

    findFurthestSplit(numSplits, splitIndx, LB, UB,
        intPtOrRayInfo[splitIndx][pointIndx], solnInfo.raysOfC1,
        hplanesIndxToBeActivated, hplanesRowIndxToBeActivated,
        hplanesToBeActivatedUBFlag, solnInfo.a0,
        chooseSplits.splitVarValue, chooseSplits.splitVarRowIndex,
        chooseSplits.splitVarIndex, chooseSplits.pi0, deepestSplitIndx);
    if (deepestSplitIndx == -1) {
      char errorstring[300];
      snprintf(errorstring, sizeof(errorstring) / sizeof(char),
          "*** ERROR: In smartGICGeneration2: deepestSplitIndx should not be -1 for splitIndx %d and pointIndx %d.\n",
          splitIndx, pointIndx);
      cerr << errorstring << endl;
      writeErrorToLog(errorstring, inst_info_out);
      exit(1);
    }

    const CoinPackedMatrix* tmpMatrix =
        cutLPSolvers[splitIndx].getMatrixByRow();
    const CoinShallowPackedVector tmpVector = (*tmpMatrix).getVector(
        pointIndx);
    numElmts = tmpVector.getNumElements();
    const int* elmtIndices = tmpVector.getIndices();
    const double* elmtVals = tmpVector.getElements();
    findPtEvalOfCut(cutStore, elmtVals, elmtIndices, numElmts, maxFeas,
        inst_info_out);

    cutLPSolvers[deepestSplitIndx].enableSimplexInterface(true);
    for (int i = 0; i < numElmts; i++)
      Obj[elmtIndices[i]] = elmtVals[i] / maxFeas;
//    cutLPSolvers[deepestSplitIndx].setObjectiveAndRefresh(&Obj[0]);
    cutLPSolvers[deepestSplitIndx].setObjective(&Obj[0]); // Alex added
    for (int i = 0; i < numElmts; i++)
      Obj[elmtIndices[i]] = 0.0;
    cutLPSolvers[deepestSplitIndx].disableSimplexInterface();
    cutLPSolvers[deepestSplitIndx].resolve();
    const double* Soln = cutLPSolvers[deepestSplitIndx].getColSolution();
    if (cutLPSolvers[deepestSplitIndx].isProvenOptimal()) {
      if (cutLPSolvers[deepestSplitIndx].getObjValue() >= 1.0 - param.getEPS())
        interPtObjFns.erase(it);
      else {
        if (addCutToStore(deepestSplitIndx, solnInfo.numNB,
            tmpCut, cutStore, Soln,
            cutLPSolvers[deepestSplitIndx].getObjValue(), pointIndx,
            -1.0, chooseSplits)) {
          cutsSpecificToSplits[deepestSplitIndx].push_back(
              cutStore.size() - 1);
          deletePtObjsCutOffByCut(tmpCut, cutLPSolvers,
              interPtObjFns);
          nCutsGenerated++;
        } else
          interPtObjFns.erase(it);
      }
    } else {
#ifdef TRACE
      cout
      << "*** ERROR: Cut generating LP unbounded in smartGICGeneration2.\n"
      << endl;
#endif
      writeErrorToLog(
          "*** ERROR: Cut generating LP unbounded in smartGICGeneration2.\n",
          inst_info_out);
      exit(1);
    }

  }

  //Generate cuts using intersection points
  int maxPoints;
  for (int s = 0; s < numSplits; s++) {
    for (int j = 0; j < solnInfo.numNB; j++)
      cutLPSolvers[s].setObjCoeff(j, 0.0);
    if (nCutsGenerated < nCutsInBudget) {
      maxPoints = 0;
      for (unsigned r = 0; r < strongPtIndices[s].size(); r++)
        if ((int) strongPtIndices[s][r].size() > maxPoints)
          maxPoints = strongPtIndices[s][r].size();
      for (int p = 0; p < maxPoints; p++) {
        if (nCutsGenerated < nCutsInBudget)
          genGICsFromStrongPoints(s, tmpCut, solnInfo.numNB,
              chooseSplits, nCutsInBudget, nCutsGenerated,
              cutLPSolvers[s], p, strongPtIndices[s], thIncr,
              cutStore, cutsSpecificToSplits[s], inst_info_out);
        else
          break;
      }
    }
  }

}

void smartGICGeneration3(int nCutsInBudget, int &nCutsGenerated,
    SolutionInfo &solnInfo, chooseCutGeneratingSets &chooseSplits,
    PointCutsSolverInterface* &cutLPSolvers,
    vector<vector<int> > &cutsSpecificToSplits,
    list<ptSortInfo*> &interPtObjFns, vector<cut> &cutStore,
    vector<vector<ptOrRayInfo*> > &intPtOrRayInfo,
    vector<vector<int> > &pointOrRayFlag,
    const std::vector<int>& numPointsOrRays, const double *LB,
    const double *UB, vector<vector<int> > &hplanesIndxToBeActivated,
    vector<vector<int> > &hplanesRowIndxToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    vector<vector<vector<int> > > &strongPtIndices, double thIncr,
    const double* objCoeff, FILE *inst_info_out) {

  int numSplits = chooseSplits.nEffCutGenSets;
  nCutsGenerated = 0;
  cut tmpCut;
  tmpCut.coeff.resize(solnInfo.numNB);
  tmpCut.RHS = 1.0;
  double th = 0.1;
  //reduced cost based cut
  vector<double> Obj(solnInfo.numNB);

  //  for (int j = 0; j < solnInfo.numNonBasicCols; j++)
  //    Obj[j] = fabs(nonBasicRedCosts[j]);
  for (int j = 0; j < solnInfo.numNB; j++)
    Obj[j] = 1.0;

  vector<double> splitCost(numSplits);
  vector<int> sortIndex(numSplits);
  vector<vector<double> > ptCentroid(numSplits);
  for (int s = 0; s < numSplits; s++) {
    ptCentroid[s].resize(solnInfo.numNB);
    computeIntersectionPtCentroid(solnInfo.numNB, cutLPSolvers[s],
        ptCentroid[s], pointOrRayFlag[s], numPointsOrRays[s]);
  }

  for (int s = 0; s < numSplits; s++) {
    sortIndex[s] = s;
    splitCost[s] = objCoeff[chooseSplits.splitVarIndex[s]];
  }
  sort(sortIndex.begin(), sortIndex.end(), index_cmp_dsc<double*>(&splitCost[0]));
  //    Obj[j] = fabs(nonBasicRedCosts[j]);
  int splitIndex;
  for (int s = 0; s < numSplits; s++) {
    if (nCutsGenerated >= nCutsInBudget)
      break;
    splitIndex = sortIndex[s];
    cutLPSolvers[splitIndex].enableSimplexInterface(true);
//    cutLPSolvers[splitIndex].setObjectiveAndRefresh(&Obj[0]);
    cutLPSolvers[splitIndex].setObjective(&Obj[0]); // Alex added
    cutLPSolvers[splitIndex].disableSimplexInterface();
    cutLPSolvers[splitIndex].resolve();
    const double* Soln = cutLPSolvers[splitIndex].getColSolution();
    if (cutLPSolvers[splitIndex].isProvenOptimal()) {
      if (addCutToStore(splitIndex, solnInfo.numNB, tmpCut,
          cutStore, Soln, cutLPSolvers[splitIndex].getObjValue(), -1,
          -1.0, chooseSplits)) {
        cutsSpecificToSplits[splitIndex].push_back(cutStore.size() - 1);
        deletePtObjsCutOffByCut(tmpCut, cutLPSolvers, interPtObjFns);
        nCutsGenerated++;
        break;
      }
    }
  }

  //Start iterative cut generation
  list<ptSortInfo*>::iterator it = interPtObjFns.begin();
  int splitIndx, pointIndx;
  int deepestSplitIndx;
  for (int j = 0; j < solnInfo.numNB; j++)
    Obj[j] = 0.0;
  int numElmts;
  double maxFeas = 0.0;
  while (nCutsGenerated < nCutsInBudget && interPtObjFns.size() > 0) {
    it = interPtObjFns.begin();
    splitIndx = (**it).splitIndx;
    pointIndx = (**it).ptIndex;

    findFurthestSplit(numSplits, splitIndx, LB, UB,
        intPtOrRayInfo[splitIndx][pointIndx], solnInfo.raysOfC1,
        hplanesIndxToBeActivated, hplanesRowIndxToBeActivated,
        hplanesToBeActivatedUBFlag, solnInfo.a0,
        chooseSplits.splitVarValue, chooseSplits.splitVarRowIndex,
        chooseSplits.splitVarIndex, chooseSplits.pi0, deepestSplitIndx);
    if (deepestSplitIndx == -1) {
      char errorstring[300];
      snprintf(errorstring, sizeof(errorstring) / sizeof(char),
          "*** ERROR: In smartGICGeneration3: deepestSplitIndx should not be -1 for splitIndx %d and pointIndx %d.\n",
          splitIndx, pointIndx);
      cerr << errorstring << endl;
      writeErrorToLog(errorstring, inst_info_out);
      exit(1);
    }

    const CoinPackedMatrix* tmpMatrix =
        cutLPSolvers[splitIndx].getMatrixByRow();
    const CoinShallowPackedVector tmpVector = (*tmpMatrix).getVector(
        pointIndx);
    numElmts = tmpVector.getNumElements();
    const int* elmtIndices = tmpVector.getIndices();
    const double* elmtVals = tmpVector.getElements();
    findPtEvalOfCut(cutStore, elmtVals, elmtIndices, numElmts, maxFeas,
        inst_info_out);

    cutLPSolvers[deepestSplitIndx].enableSimplexInterface(true);
    for (int i = 0; i < numElmts; i++)
      Obj[elmtIndices[i]] = elmtVals[i];
//    cutLPSolvers[deepestSplitIndx].setObjectiveAndRefresh(&Obj[0]);
    cutLPSolvers[deepestSplitIndx].setObjective(&Obj[0]); // Alex added
    for (int i = 0; i < numElmts; i++)
      Obj[elmtIndices[i]] = 0.0;
    cutLPSolvers[deepestSplitIndx].disableSimplexInterface();
    cutLPSolvers[deepestSplitIndx].resolve();
    const double* Soln = cutLPSolvers[deepestSplitIndx].getColSolution();
    if (cutLPSolvers[deepestSplitIndx].isProvenOptimal()) {
      if (cutLPSolvers[deepestSplitIndx].getObjValue() >= 1.0 - param.getEPS()) {
        interPtObjFns.erase(it);
      } else {
        if (addCutToStore(deepestSplitIndx, solnInfo.numNB,
            tmpCut, cutStore, Soln,
            cutLPSolvers[deepestSplitIndx].getObjValue(), pointIndx,
            -1.0, chooseSplits)) {
          cutsSpecificToSplits[deepestSplitIndx].push_back(
              cutStore.size() - 1);
          deletePtObjsCutOffByCut(tmpCut, cutLPSolvers,
              interPtObjFns);
          nCutsGenerated++;
        } else
          interPtObjFns.erase(it);
      }
    } else {
      //LP is unbounded, move objective closer to point centroid
      cutLPSolvers[deepestSplitIndx].enableSimplexInterface(true);

      for (int j = 0; j < elmtIndices[0]; j++)
        Obj[j] = th * ptCentroid[deepestSplitIndx][j];
      Obj[elmtIndices[0]] = (1 - th) * elmtVals[0]
          + th * ptCentroid[deepestSplitIndx][elmtIndices[0]];
      for (int i = 1; i < numElmts; i++) {
        for (int j = elmtIndices[i - 1] + 1; j < elmtIndices[i]; j++) {
          Obj[j] = th * ptCentroid[deepestSplitIndx][j];
        }
        Obj[elmtIndices[i]] = (1 - th) * elmtVals[i]
            + th * ptCentroid[deepestSplitIndx][elmtIndices[i]];
      }
      for (int j = elmtVals[numElmts - 1] + 1;
          j < solnInfo.numNB; j++)
        Obj[j] = th * ptCentroid[deepestSplitIndx][j];

//      cutLPSolvers[deepestSplitIndx].setObjectiveAndRefresh(&Obj[0]);
      cutLPSolvers[deepestSplitIndx].setObjective(&Obj[0]); // Alex added
      for (int i = 0; i < numElmts; i++)
        Obj[elmtIndices[i]] = 0.0;
      cutLPSolvers[deepestSplitIndx].disableSimplexInterface();
      cutLPSolvers[deepestSplitIndx].resolve();
      const double* Soln =
          cutLPSolvers[deepestSplitIndx].getColSolution();
      if (cutLPSolvers[deepestSplitIndx].isProvenOptimal()) {
        if (cutLPSolvers[deepestSplitIndx].getObjValue() >= 1.0 - param.getEPS()) {
          interPtObjFns.erase(it);
        } else {
          if (addCutToStore(deepestSplitIndx,
              solnInfo.numNB, tmpCut, cutStore, Soln,
              cutLPSolvers[deepestSplitIndx].getObjValue(),
              pointIndx, -1.0, chooseSplits)) {
            cutsSpecificToSplits[deepestSplitIndx].push_back(
                cutStore.size() - 1);
            deletePtObjsCutOffByCut(tmpCut, cutLPSolvers,
                interPtObjFns);
            nCutsGenerated++;
          } else
            interPtObjFns.erase(it);
        }
      } else {
        interPtObjFns.erase(it);
      }
    }

  }

  //Generate cuts using intersection points
  int maxPoints;
  for (int s = 0; s < numSplits; s++) {
    for (int j = 0; j < solnInfo.numNB; j++)
      cutLPSolvers[s].setObjCoeff(j, 0.0);
    if (nCutsGenerated < nCutsInBudget) {
      maxPoints = 0;
      for (unsigned r = 0; r < strongPtIndices[s].size(); r++)
        if ((int) strongPtIndices[s][r].size() > maxPoints)
          maxPoints = strongPtIndices[s][r].size();
      for (int p = 0; p < maxPoints; p++) {
        if (nCutsGenerated < nCutsInBudget)
          genGICsFromStrongPoints(s, tmpCut, solnInfo.numNB,
              chooseSplits, nCutsInBudget, nCutsGenerated,
              cutLPSolvers[s], p, strongPtIndices[s], thIncr,
              cutStore, cutsSpecificToSplits[s], inst_info_out);
        else
          break;
      }
    }
  }

}

void smartGICGeneration4(int &totalObjIntPts, int &ptsNotCut, int &nUnbddPoints,
    int &compUnbddPoints, int nCutsInBudget, int &nCutsGenerated,
    SolutionInfo &solnInfo, chooseCutGeneratingSets &chooseSplits,
    PointCutsSolverInterface* &cutLPSolvers,
    vector<vector<int> > &cutsSpecificToSplits,
    list<ptSortInfo*> &interPtObjFns, vector<cut> &cutStore,
    vector<vector<ptOrRayInfo*> > &intPtOrRayInfo,
    vector<vector<int> > &pointOrRayFlag,
    const std::vector<int>& numPointsOrRays, const double *LB,
    const double *UB, vector<vector<int> > &hplanesIndxToBeActivated,
    vector<vector<int> > &hplanesRowIndxToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    vector<vector<vector<int> > > &strongPtIndices, double thIncr,
    const double* objCoeff, FILE* inst_info_out) {

  int numSplits = chooseSplits.nEffCutGenSets;
  nCutsGenerated = 0;
  cut tmpCut;
  tmpCut.coeff.resize(solnInfo.numNB);
  tmpCut.RHS = 1.0;
  double th = 0.1;
  //reduced cost based cut
  vector<double> Obj(solnInfo.numNB);
  totalObjIntPts = 0;
  nUnbddPoints = 0;
  compUnbddPoints = 0;
  ptsNotCut = 0;
  //  for (int j = 0; j < solnInfo.numNonBasicCols; j++)
  //    Obj[j] = fabs(nonBasicRedCosts[j]);
  for (int j = 0; j < solnInfo.numNB; j++)
    Obj[j] = 1.0;

  vector<double> splitCost(numSplits);
  vector<int> sortIndex(numSplits);
  vector<vector<double> > ptCentroid(numSplits);
  for (int s = 0; s < numSplits; s++) {
    ptCentroid[s].resize(solnInfo.numNB);
    computeIntersectionPtCentroid(solnInfo.numNB, cutLPSolvers[s],
        ptCentroid[s], pointOrRayFlag[s], numPointsOrRays[s]);
  }

  for (int s = 0; s < numSplits; s++) {
    sortIndex[s] = s;
    splitCost[s] = objCoeff[chooseSplits.splitVarIndex[s]];
  }
  sort(sortIndex.begin(), sortIndex.end(), index_cmp_dsc<double*>(&splitCost[0]));

  totalObjIntPts = interPtObjFns.size();
  //    Obj[j] = fabs(nonBasicRedCosts[j]);
  int splitIndex;
  int currSortIndex = 0;
  for (int s = 0; s < numSplits; s++) {
    currSortIndex = s;
    if (nCutsGenerated >= nCutsInBudget)
      break;
    splitIndex = sortIndex[s];
    cutLPSolvers[splitIndex].enableSimplexInterface(true);
//    cutLPSolvers[splitIndex].setObjectiveAndRefresh(&Obj[0]);
    cutLPSolvers[splitIndex].setObjective(&Obj[0]); // Alex added
    cutLPSolvers[splitIndex].disableSimplexInterface();
    cutLPSolvers[splitIndex].resolve();
    const double* Soln = cutLPSolvers[splitIndex].getColSolution();
    if (cutLPSolvers[splitIndex].isProvenOptimal()) {
      if (addCutToStore(splitIndex, solnInfo.numNB, tmpCut,
          cutStore, Soln, cutLPSolvers[splitIndex].getObjValue(), -1,
          -1.0, chooseSplits)) {
        cutsSpecificToSplits[splitIndex].push_back(cutStore.size() - 1);
        deletePtObjsCutOffByCut(tmpCut, cutLPSolvers, interPtObjFns);
        nCutsGenerated++;
        break;
      }
    }
  }

  //Start iterative cut generation
  list<ptSortInfo*>::iterator it = interPtObjFns.begin();
  int splitIndx, pointIndx;
  int deepestSplitIndx;
  for (int j = 0; j < solnInfo.numNB; j++)
    Obj[j] = 0.0;
  int numElmts;
  double maxFeas = 0.0;
  while (nCutsGenerated < nCutsInBudget && interPtObjFns.size() > 0) {
#ifdef TRACE
    cout << "*************Looping****************\n";
#endif
    it = interPtObjFns.begin();
    splitIndx = (**it).splitIndx;
    pointIndx = (**it).ptIndex;

    findFurthestSplit(numSplits, splitIndx, LB, UB,
        intPtOrRayInfo[splitIndx][pointIndx], solnInfo.raysOfC1,
        hplanesIndxToBeActivated, hplanesRowIndxToBeActivated,
        hplanesToBeActivatedUBFlag, solnInfo.a0,
        chooseSplits.splitVarValue, chooseSplits.splitVarRowIndex,
        chooseSplits.splitVarIndex, chooseSplits.pi0, deepestSplitIndx);
    if (deepestSplitIndx == -1) {
      char errorstring[300];
      snprintf(errorstring, sizeof(errorstring) / sizeof(char),
          "*** ERROR: In smartGICGeneration4: deepestSplitIndx should not be -1 for splitIndx %d and pointIndx %d.\n",
          splitIndx, pointIndx);
      cerr << errorstring << endl;
      writeErrorToLog(errorstring, inst_info_out);
      exit(1);
    }

    const CoinPackedMatrix* tmpMatrix =
        cutLPSolvers[splitIndx].getMatrixByRow();
    const CoinShallowPackedVector tmpVector = (*tmpMatrix).getVector(
        pointIndx);
    numElmts = tmpVector.getNumElements();
    const int* elmtIndices = tmpVector.getIndices();
    const double* elmtVals = tmpVector.getElements();
    findPtEvalOfCut(cutStore, elmtVals, elmtIndices, numElmts, maxFeas,
        inst_info_out);

    cutLPSolvers[deepestSplitIndx].enableSimplexInterface(true);
    for (int i = 0; i < numElmts; i++)
      Obj[elmtIndices[i]] = elmtVals[i];
//    cutLPSolvers[deepestSplitIndx].setObjectiveAndRefresh(&Obj[0]);
    cutLPSolvers[deepestSplitIndx].setObjective(&Obj[0]); // Alex added
    for (int i = 0; i < numElmts; i++)
      Obj[elmtIndices[i]] = 0.0;
    cutLPSolvers[deepestSplitIndx].disableSimplexInterface();
    cutLPSolvers[deepestSplitIndx].resolve();
    const double* Soln = cutLPSolvers[deepestSplitIndx].getColSolution();
    if (cutLPSolvers[deepestSplitIndx].isProvenOptimal()) {
      if (cutLPSolvers[deepestSplitIndx].getObjValue() >= 1.0 - param.getEPS()) {
        //        it = interPtObjFns.begin();
        ptsNotCut++;
        interPtObjFns.erase(it);
      } else {
        if (addCutToStore(deepestSplitIndx, solnInfo.numNB,
            tmpCut, cutStore, Soln,
            cutLPSolvers[deepestSplitIndx].getObjValue(), pointIndx,
            -1.0, chooseSplits)) {
          cutsSpecificToSplits[deepestSplitIndx].push_back(
              cutStore.size() - 1);
          deletePtObjsCutOffByCut(tmpCut, cutLPSolvers,
              interPtObjFns);
          nCutsGenerated++;
        } else
          interPtObjFns.erase(it);
      }
    } else {
      nUnbddPoints++;
      //LP is unbounded, move objective closer to point centroid
      const CoinPackedMatrix* tmpMatrix =
          cutLPSolvers[splitIndx].getMatrixByRow();
      const CoinShallowPackedVector tmpVector = (*tmpMatrix).getVector(
          pointIndx);
      numElmts = tmpVector.getNumElements();
      const int* elmtIndices = tmpVector.getIndices();
      const double* elmtVals = tmpVector.getElements();
      cutLPSolvers[deepestSplitIndx].enableSimplexInterface(true);

      for (int j = 0; j < elmtIndices[0]; j++)
        Obj[j] = th * ptCentroid[deepestSplitIndx][j];
      Obj[elmtIndices[0]] = (1 - th) * elmtVals[0]
          + th * ptCentroid[deepestSplitIndx][elmtIndices[0]];
      for (int i = 1; i < numElmts; i++) {
        for (int j = elmtIndices[i - 1] + 1; j < elmtIndices[i]; j++) {
          Obj[j] = th * ptCentroid[deepestSplitIndx][j];
        }
        Obj[elmtIndices[i]] = (1 - th) * elmtVals[i]
            + th * ptCentroid[deepestSplitIndx][elmtIndices[i]];
      }
      for (int j = elmtVals[numElmts - 1] + 1;
          j < solnInfo.numNB; j++)
        Obj[j] = th * ptCentroid[deepestSplitIndx][j];

//      cutLPSolvers[deepestSplitIndx].setObjectiveAndRefresh(&Obj[0]);
      cutLPSolvers[deepestSplitIndx].setObjective(&Obj[0]); // Alex added
      for (int i = 0; i < numElmts; i++)
        Obj[elmtIndices[i]] = 0.0;
      cutLPSolvers[deepestSplitIndx].disableSimplexInterface();
      cutLPSolvers[deepestSplitIndx].resolve();
      const double* Soln =
          cutLPSolvers[deepestSplitIndx].getColSolution();
      if (cutLPSolvers[deepestSplitIndx].isProvenOptimal()) {
        if (cutLPSolvers[deepestSplitIndx].getObjValue() >= 1.0 - param.getEPS()) {
          //        it = interPtObjFns.begin();
          ptsNotCut++;
          interPtObjFns.erase(it);
        } else {
          if (addCutToStore(deepestSplitIndx,
              solnInfo.numNB, tmpCut, cutStore, Soln,
              cutLPSolvers[deepestSplitIndx].getObjValue(),
              pointIndx, -1.0, chooseSplits)) {
            cutsSpecificToSplits[deepestSplitIndx].push_back(
                cutStore.size() - 1);
            deletePtObjsCutOffByCut(tmpCut, cutLPSolvers,
                interPtObjFns);
            nCutsGenerated++;
          } else
            interPtObjFns.erase(it);
        }
      } else {
        compUnbddPoints++;
        interPtObjFns.erase(it);
      }
    }

  }

  if (nCutsGenerated < nCutsInBudget) {
    for (int j = 0; j < solnInfo.numNB; j++) {
      //    Obj[j] = 1.0;
//      Obj[j] = fabs(solnInfo.nonBasicReducedCost[j]); // TODO Which one to use? nonBasicRedCost or redCost?
      Obj[j] = fabs(solnInfo.reducedCost[j]);
    }
    for (int s = currSortIndex + 1; s < numSplits; s++) {
      if (nCutsGenerated >= nCutsInBudget)
        break;
      splitIndex = sortIndex[s];
      cutLPSolvers[splitIndex].enableSimplexInterface(true);
//      cutLPSolvers[splitIndex].setObjectiveAndRefresh(&Obj[0]);
      cutLPSolvers[splitIndex].setObjective(&Obj[0]); // Alex added
      cutLPSolvers[splitIndex].disableSimplexInterface();
      cutLPSolvers[splitIndex].resolve();
      const double* Soln = cutLPSolvers[splitIndex].getColSolution();
      if (cutLPSolvers[splitIndex].isProvenOptimal()) {
        if (addCutToStore(splitIndex, solnInfo.numNB, tmpCut,
            cutStore, Soln, cutLPSolvers[splitIndex].getObjValue(),
            -1, -1.0, chooseSplits)) {
          cutsSpecificToSplits[splitIndex].push_back(
              cutStore.size() - 1);
          nCutsGenerated++;
        }
      }
    }
  }

  if (nCutsGenerated < nCutsInBudget) {
    for (int j = 0; j < solnInfo.numNB; j++)
      Obj[j] = 1.0;
//      Obj[j] = fabs(nonBasicRedCosts[j]);
    for (int s = currSortIndex + 1; s < numSplits; s++) {
      if (nCutsGenerated >= nCutsInBudget)
        break;
      splitIndex = sortIndex[s];
      cutLPSolvers[splitIndex].enableSimplexInterface(true);
//      cutLPSolvers[splitIndex].setObjectiveAndRefresh(&Obj[0]);
      cutLPSolvers[splitIndex].setObjective(&Obj[0]); // Alex added
      cutLPSolvers[splitIndex].disableSimplexInterface();
      cutLPSolvers[splitIndex].resolve();
      const double* Soln = cutLPSolvers[splitIndex].getColSolution();
      if (cutLPSolvers[splitIndex].isProvenOptimal()) {
        if (addCutToStore(splitIndex, solnInfo.numNB, tmpCut,
            cutStore, Soln, cutLPSolvers[splitIndex].getObjValue(),
            -1, -1.0, chooseSplits)) {
          cutsSpecificToSplits[splitIndex].push_back(
              cutStore.size() - 1);
          nCutsGenerated++;
        }
      }
    }
  }

}

void smartGICGeneration5(int &totalObjIntPts, int &ptsNotCut, int &nUnbddPoints,
    int &compUnbddPoints, int nCutsInBudget, int &nCutsGenerated,
    SolutionInfo &solnInfo, chooseCutGeneratingSets &chooseSplits,
    PointCutsSolverInterface* &cutLPSolvers,
    vector<vector<int> > &cutsSpecificToSplits,
    list<ptSortInfo*> &interPtObjFns, vector<cut> &cutStore,
    vector<vector<ptOrRayInfo*> > &intPtOrRayInfo,
    vector<vector<int> > &pointOrRayFlag,
    const std::vector<int>& numPointsOrRays, const double *LB,
    const double *UB, vector<vector<int> > &hplanesIndxToBeActivated,
    vector<vector<int> > &hplanesRowIndxToBeActivated,
    vector<vector<int> > &hplanesToBeActivatedUBFlag,
    vector<vector<vector<int> > > &strongPtIndices, double thIncr,
    const double* objCoeff, FILE* inst_info_out) {

  int numSplits = chooseSplits.nEffCutGenSets;
  nCutsGenerated = 0;
  cut tmpCut;
  tmpCut.coeff.resize(solnInfo.numNB);
  tmpCut.RHS = 1.0;
  double th = 0.1;
  //reduced cost based cut
  vector<double> Obj(solnInfo.numNB);
  totalObjIntPts = 0;
  nUnbddPoints = 0;
  compUnbddPoints = 0;
  ptsNotCut = 0;
  //  for (int j = 0; j < solnInfo.numNonBasicCols; j++)
  //    Obj[j] = fabs(nonBasicRedCosts[j]);
  for (int j = 0; j < solnInfo.numNB; j++)
    Obj[j] = 1.0;

  vector<double> splitCost(numSplits);
  vector<int> sortIndex(numSplits);
  vector<vector<double> > ptCentroid(numSplits);
  for (int s = 0; s < numSplits; s++) {
    ptCentroid[s].resize(solnInfo.numNB);
    computeIntersectionPtCentroid(solnInfo.numNB, cutLPSolvers[s],
        ptCentroid[s], pointOrRayFlag[s], numPointsOrRays[s]);
  }

  for (int s = 0; s < numSplits; s++) {
    sortIndex[s] = s;
    splitCost[s] = objCoeff[chooseSplits.splitVarIndex[s]];
  }
  sort(sortIndex.begin(), sortIndex.end(), index_cmp_dsc<double*>(&splitCost[0]));

  totalObjIntPts = interPtObjFns.size();
  //    Obj[j] = fabs(nonBasicRedCosts[j]);
  int splitIndex;
  int currSortIndex = 0;
  for (int s = 0; s < numSplits; s++) {
    currSortIndex = s;
    if (nCutsGenerated >= nCutsInBudget)
      break;
    splitIndex = sortIndex[s];
    cutLPSolvers[splitIndex].enableSimplexInterface(true);
//    cutLPSolvers[splitIndex].setObjectiveAndRefresh(&Obj[0]);
    cutLPSolvers[splitIndex].setObjective(&Obj[0]); // Alex added
    cutLPSolvers[splitIndex].disableSimplexInterface();
    cutLPSolvers[splitIndex].resolve();
    const double* Soln = cutLPSolvers[splitIndex].getColSolution();
    if (cutLPSolvers[splitIndex].isProvenOptimal()) {
      if (addCutToStore(splitIndex, solnInfo.numNB, tmpCut,
          cutStore, Soln, cutLPSolvers[splitIndex].getObjValue(), -1,
          -1.0, chooseSplits)) {
        cutsSpecificToSplits[splitIndex].push_back(cutStore.size() - 1);
        deletePtObjsCutOffByCut(tmpCut, cutLPSolvers, interPtObjFns);
        nCutsGenerated++;
        break;
      }
    }
  }

  //Start iterative cut generation
  list<ptSortInfo*>::iterator it = interPtObjFns.begin();
  int splitIndx, pointIndx;
  int deepestSplitIndx;
  for (int j = 0; j < solnInfo.numNB; j++)
    Obj[j] = 0.0;
  int numElmts;
  double maxFeas = 0.0;
  while (nCutsGenerated < nCutsInBudget && interPtObjFns.size() > 0) {
    it = interPtObjFns.begin();
    splitIndx = (**it).splitIndx;
    pointIndx = (**it).ptIndex;

    findFurthestSplit(numSplits, splitIndx, LB, UB,
        intPtOrRayInfo[splitIndx][pointIndx], solnInfo.raysOfC1,
        hplanesIndxToBeActivated, hplanesRowIndxToBeActivated,
        hplanesToBeActivatedUBFlag, solnInfo.a0,
        chooseSplits.splitVarValue, chooseSplits.splitVarRowIndex,
        chooseSplits.splitVarIndex, chooseSplits.pi0, deepestSplitIndx);
    if (deepestSplitIndx == -1) {
      char errorstring[300];
      snprintf(errorstring, sizeof(errorstring) / sizeof(char),
          "*** ERROR: In smartGICGeneration5: deepestSplitIndx should not be -1 for splitIndx %d and pointIndx %d.\n",
          splitIndx, pointIndx);
      cerr << errorstring << endl;
      writeErrorToLog(errorstring, inst_info_out);
      exit(1);
    }

    const CoinPackedMatrix* tmpMatrix =
        cutLPSolvers[splitIndx].getMatrixByRow();
    const CoinShallowPackedVector tmpVector = (*tmpMatrix).getVector(
        pointIndx);
    numElmts = tmpVector.getNumElements();
    const int* elmtIndices = tmpVector.getIndices();
    const double* elmtVals = tmpVector.getElements();
    findPtEvalOfCut(cutStore, elmtVals, elmtIndices, numElmts, maxFeas,
        inst_info_out);

    cutLPSolvers[deepestSplitIndx].enableSimplexInterface(true);
    for (int i = 0; i < numElmts; i++)
      Obj[elmtIndices[i]] = elmtVals[i];
//    cutLPSolvers[deepestSplitIndx].setObjectiveAndRefresh(&Obj[0]);
    cutLPSolvers[deepestSplitIndx].setObjective(&Obj[0]); // Alex added
    for (int i = 0; i < numElmts; i++)
      Obj[elmtIndices[i]] = 0.0;
    cutLPSolvers[deepestSplitIndx].disableSimplexInterface();
    cutLPSolvers[deepestSplitIndx].resolve();
    const double* Soln = cutLPSolvers[deepestSplitIndx].getColSolution();
    if (cutLPSolvers[deepestSplitIndx].isProvenOptimal()) {
      if (cutLPSolvers[deepestSplitIndx].getObjValue() >= 1.0 - param.getEPS()) {
        //        it = interPtObjFns.begin();
        ptsNotCut++;
        interPtObjFns.erase(it);
      } else {
        if (addCutToStore(deepestSplitIndx, solnInfo.numNB,
            tmpCut, cutStore, Soln,
            cutLPSolvers[deepestSplitIndx].getObjValue(), pointIndx,
            -1.0, chooseSplits)) {
          cutsSpecificToSplits[deepestSplitIndx].push_back(
              cutStore.size() - 1);
          deletePtObjsCutOffByCut(tmpCut, cutLPSolvers,
              interPtObjFns);
          nCutsGenerated++;
        } else
          interPtObjFns.erase(it);
      }
    } else {
      nUnbddPoints++;
      //LP is unbounded, move objective closer to point centroid
      const CoinPackedMatrix* tmpMatrix =
          cutLPSolvers[splitIndx].getMatrixByRow();
      const CoinShallowPackedVector tmpVector = (*tmpMatrix).getVector(
          pointIndx);
      numElmts = tmpVector.getNumElements();
      const int* elmtIndices = tmpVector.getIndices();
      const double* elmtVals = tmpVector.getElements();
      cutLPSolvers[deepestSplitIndx].enableSimplexInterface(true);

      for (int j = 0; j < elmtIndices[0]; j++)
        Obj[j] = th * ptCentroid[deepestSplitIndx][j];
      Obj[elmtIndices[0]] = (1 - th) * elmtVals[0]
          + th * ptCentroid[deepestSplitIndx][elmtIndices[0]];
      for (int i = 1; i < numElmts; i++) {
        for (int j = elmtIndices[i - 1] + 1; j < elmtIndices[i]; j++) {
          Obj[j] = th * ptCentroid[deepestSplitIndx][j];
        }
        Obj[elmtIndices[i]] = (1 - th) * elmtVals[i]
            + th * ptCentroid[deepestSplitIndx][elmtIndices[i]];
      }
      for (int j = elmtVals[numElmts - 1] + 1;
          j < solnInfo.numNB; j++)
        Obj[j] = th * ptCentroid[deepestSplitIndx][j];

//      cutLPSolvers[deepestSplitIndx].setObjectiveAndRefresh(&Obj[0]);
      cutLPSolvers[deepestSplitIndx].setObjective(&Obj[0]); // Alex added
      for (int i = 0; i < numElmts; i++)
        Obj[elmtIndices[i]] = 0.0;
      cutLPSolvers[deepestSplitIndx].disableSimplexInterface();
      cutLPSolvers[deepestSplitIndx].resolve();
      const double* Soln =
          cutLPSolvers[deepestSplitIndx].getColSolution();
      if (cutLPSolvers[deepestSplitIndx].isProvenOptimal()) {
        if (cutLPSolvers[deepestSplitIndx].getObjValue() >= 1.0 - param.getEPS()) {
          //        it = interPtObjFns.begin();
          ptsNotCut++;
          interPtObjFns.erase(it);
        } else {
          if (addCutToStore(deepestSplitIndx,
              solnInfo.numNB, tmpCut, cutStore, Soln,
              cutLPSolvers[deepestSplitIndx].getObjValue(),
              pointIndx, -1.0, chooseSplits)) {
            cutsSpecificToSplits[deepestSplitIndx].push_back(
                cutStore.size() - 1);
            deletePtObjsCutOffByCut(tmpCut, cutLPSolvers,
                interPtObjFns);
            nCutsGenerated++;
          } else
            interPtObjFns.erase(it);
        }
      } else {
        compUnbddPoints++;
        interPtObjFns.erase(it);
      }
    }

  }

  if (nCutsGenerated < nCutsInBudget) {
    for (int j = 0; j < solnInfo.numNB; j++)
      Obj[j] = 1.0;
//      Obj[j] = fabs(nonBasicRedCosts[j]);
    for (int s = currSortIndex + 1; s < numSplits; s++) {
      if (nCutsGenerated >= nCutsInBudget)
        break;
      splitIndex = sortIndex[s];
      cutLPSolvers[splitIndex].enableSimplexInterface(true);
//      cutLPSolvers[splitIndex].setObjectiveAndRefresh(&Obj[0]);
      cutLPSolvers[splitIndex].setObjective(&Obj[0]); // Alex added
      cutLPSolvers[splitIndex].disableSimplexInterface();
      cutLPSolvers[splitIndex].resolve();
      const double* Soln = cutLPSolvers[splitIndex].getColSolution();
      if (cutLPSolvers[splitIndex].isProvenOptimal()) {
        if (addCutToStore(splitIndex, solnInfo.numNB, tmpCut,
            cutStore, Soln, cutLPSolvers[splitIndex].getObjValue(),
            -1, -1.0, chooseSplits)) {
          cutsSpecificToSplits[splitIndex].push_back(
              cutStore.size() - 1);
          nCutsGenerated++;
        }
      }
    }
  }

  if (nCutsGenerated < nCutsInBudget) {
    for (int j = 0; j < solnInfo.numNB; j++)
      //    Obj[j] = 1.0;
      Obj[j] = fabs(solnInfo.nonBasicReducedCost[j]);
    for (int s = currSortIndex + 1; s < numSplits; s++) {
      if (nCutsGenerated >= nCutsInBudget)
        break;
      splitIndex = sortIndex[s];
      cutLPSolvers[splitIndex].enableSimplexInterface(true);
//      cutLPSolvers[splitIndex].setObjectiveAndRefresh(&Obj[0]);
      cutLPSolvers[splitIndex].setObjective(&Obj[0]); // Alex added
      cutLPSolvers[splitIndex].disableSimplexInterface();
      cutLPSolvers[splitIndex].resolve();
      const double* Soln = cutLPSolvers[splitIndex].getColSolution();
      if (cutLPSolvers[splitIndex].isProvenOptimal()) {
        if (addCutToStore(splitIndex, solnInfo.numNB, tmpCut,
            cutStore, Soln, cutLPSolvers[splitIndex].getObjValue(),
            -1, -1.0, chooseSplits)) {
          cutsSpecificToSplits[splitIndex].push_back(
              cutStore.size() - 1);
          nCutsGenerated++;
        }
      }
    }
  }
}

void generateGICsUsingInterPointObjs(int &nCutsGenerated, int nTotalCuts,
    cut &tmpCut, PointCutsSolverInterface* &cutLPSolvers, int numSplits,
    int nNonBasicCols, chooseCutGeneratingSets &chooseSplits,
    vector<cut> &cutStore, vector<vector<int> > &cutsSpecificToSplits,
    vector<int> &allRaysCut, vector<double> &interPtObjVal,
    vector<int> &interPtObjIndex, vector<int> &interPtSplit,
    vector<vector<int> > &pointOrRayFlag,
    const std::vector<int>& numPointsOrRays, double thIncr,
    FILE *inst_info_out) {

  if (nCutsGenerated < nTotalCuts) {
    vector<vector<double> > ptCentroid(numSplits);
    bool cutSuccess = true;
    bool vertCut = true;
    double th = 0.0;
    for (int s = 0; s < numSplits; s++) {
      ptCentroid[s].resize(nNonBasicCols);
#ifdef TRACE
      printf(
          "In generateGICsUsingInterPointObjs: Computing centroid of intersection points.\n");
#endif
      computeIntersectionPtCentroid(nNonBasicCols, cutLPSolvers[s],
          ptCentroid[s], pointOrRayFlag[s], numPointsOrRays[s]);
#ifdef TRACE
      printf(
          "In generateGICsUsingInterPointObjs: Finished computing centroid of intersection points.\n");
#endif
    }
    int rayIndx = 0;
    for (unsigned r = 0; r < allRaysCut.size(); r++) {
      rayIndx = allRaysCut[r];
#ifdef TRACE
      printf(
          "In generateGICsUsingInterPointObjs: For rayIndx %d, the split index which yields distance ray goes before intersecting the boundary of farthest split is %d. This is -1 if the ray is parallel to one of the splits being considered.\n",
          rayIndx, interPtSplit[rayIndx]);
#endif
      if (interPtSplit[rayIndx] != -1) {
        if (nCutsGenerated < nTotalCuts) {
          cutSuccess = false;
          for (int j = 0; j < nNonBasicCols; j++)
            cutLPSolvers[interPtSplit[rayIndx]].setObjCoeff(j, 0.0);
          cutLPSolvers[interPtSplit[rayIndx]].setObjCoeff(rayIndx,
              interPtObjVal[rayIndx]);

          cutLPSolvers[interPtSplit[rayIndx]].initialSolve();
          const double * Soln =
              cutLPSolvers[interPtSplit[rayIndx]].getColSolution();
          if (cutLPSolvers[interPtSplit[rayIndx]].isProvenOptimal()) {
#ifdef TRACE
            printf(
                "In generateGICsUsingInterPointObjs: LP using obj coeff %lf for ray %d is proven optimal; cut being added.\n",
                interPtObjVal[rayIndx], rayIndx);
#endif
            if (addCutToStore(interPtSplit[rayIndx], nNonBasicCols,
                tmpCut, cutStore, Soln,
                cutLPSolvers[interPtSplit[rayIndx]].getObjValue(),
                -1, -1.0, chooseSplits)) {
              cutSuccess = true;
              nCutsGenerated++;
              cutsSpecificToSplits[interPtSplit[rayIndx]].push_back(
                  cutStore.size() - 1);
            } else {
#ifdef TRACE
              printf(
                  "In generateGICsUsingInterPointObjs: *** ERROR: Cut not added because already existed.\n");
#endif
            }
          } else {
#ifdef TRACE
            printf(
                "In generateGICsUsingInterPointObjs:  LP using obj coeff %lf for ray %d is NOT proven optimal, so changing objective.\n",
                interPtObjVal[rayIndx], rayIndx);
            printf("Proven primal infeasible? %d.\n",
                cutLPSolvers[interPtSplit[rayIndx]].isProvenPrimalInfeasible());
            printf("Proven dual infeasible? %d.\n",
                cutLPSolvers[interPtSplit[rayIndx]].isProvenDualInfeasible());
#endif

            th = thIncr;
            vertCut = true;
            while (!cutSuccess && th <= 1.0 - param.getEPS() && vertCut) {
              if (nCutsGenerated < nTotalCuts) {
                for (int i = 0; i < rayIndx; i++) {
                  cutLPSolvers[interPtSplit[rayIndx]].setObjCoeff(
                      i,
                      th
                          * ptCentroid[interPtSplit[rayIndx]][i]);
                }
                cutLPSolvers[interPtSplit[rayIndx]].setObjCoeff(
                    rayIndx,
                    th
                        * ptCentroid[interPtSplit[rayIndx]][rayIndx]
                        + (1 - th)
                            * interPtObjVal[rayIndx]);
                for (int i = rayIndx + 1; i < nNonBasicCols;
                    i++) {
                  cutLPSolvers[interPtSplit[rayIndx]].setObjCoeff(
                      i,
                      th
                          * ptCentroid[interPtSplit[rayIndx]][i]);
                }

                cutLPSolvers[interPtSplit[rayIndx]].initialSolve();
                if (cutLPSolvers[interPtSplit[rayIndx]].isProvenPrimalInfeasible()) {
#ifdef TRACE
                  printf("*** ERROR: Cut LP is infeasible\n");
#endif
                  writeErrorToLog(
                      "*** ERROR: Cut LP is infeasible in generateGICsUsingInterPointObjs.\n",
                      inst_info_out);
                  exit(1);
                }
                if (cutLPSolvers[interPtSplit[rayIndx]].isProvenOptimal()) {
#ifdef TRACE
                  printf(
                      "In generateGICsUsingInterPointObjs: LP using some complicated obj coeffs for ray %d is proven optimal; cut being added.\n",
                      rayIndx);
#endif
                  if (cutLPSolvers[interPtSplit[rayIndx]].getObjValue() <= 1.0 - param.getEPS()) {
                    const
                    double * Soln =
                        cutLPSolvers[interPtSplit[rayIndx]].getColSolution();
                    if (addCutToStore(interPtSplit[rayIndx],
                        nNonBasicCols, tmpCut, cutStore,
                        Soln,
                        cutLPSolvers[interPtSplit[rayIndx]].getObjValue(),
                        -2, th, chooseSplits)) {
                      cutSuccess = true;
                      cutsSpecificToSplits[interPtSplit[rayIndx]].push_back(
                          cutStore.size() - 1);
                      nCutsGenerated++;
                    } else {
#ifdef TRACE
                      printf(
                          "In generateGICsUsingInterPointObjs: *** ERROR: Cut not added because already existed.\n");
#endif
                    }
                  } else {
#ifdef TRACE
                    printf(
                        "In generateGICsUsingInterPointObjs: LP using some complicated obj coeffs for ray %d is still NOT proven optimal. Abandoning.\n",
                        rayIndx);
#endif
                    vertCut = false;
                  }
                }
                th += thIncr;
              } else
                break;
            }
          }
        } else
          break;
      }
    }
  }
}

void generateBestGICAvgCutOptimalBasis(cut &tmpCut,
    PointCutsSolverInterface* &cutLPSolvers, int numSplits, int nNonBasicCols,
    vector<cut> &cutStore, vector<vector<int> > &cutsSpecificToSplits,
    int &numCutsGenerated, chooseCutGeneratingSets &chooseSplits) {
#ifdef TRACE
  printf("Num splits: %d.\n", numSplits);
#endif
  //  vector<double> avgCutCoeff(numSplits,0.0);
  double avgCutCoeff, bestAvgCoeff = param.getINFINITY(), bestViol;
  vector<double> cutCoeff(nNonBasicCols);
  int bestSplitIndx = -1;
  for (int s = 0; s < numSplits; s++) {
    for (int j = 0; j < nNonBasicCols; j++)
      cutLPSolvers[s].setObjCoeff(j, 1.0);
    cutLPSolvers[s].initialSolve();
    const double* Soln = cutLPSolvers[s].getColSolution();
    if (cutLPSolvers[s].isProvenOptimal()) {
      avgCutCoeff = 0.0;
      for (int j = 0; j < nNonBasicCols; j++)
        avgCutCoeff += Soln[j];
      avgCutCoeff /= nNonBasicCols;
      if (avgCutCoeff <= bestAvgCoeff - param.getEPS()) {
        bestSplitIndx = s;
        bestViol = cutLPSolvers[s].getObjValue();
        for (int j = 0; j < nNonBasicCols; j++)
          cutCoeff[j] = Soln[j];
      }
    }
//    printf("SAVING LP FILE.\n");
//    char tmp[50];
//    ///home/akazachk/Dropbox/Research/test-instances/liftingtest/n3div36/
//    sprintf(tmp, "cutLP-split%d-cut1", s);
//    printf("CUT LP NAME IS %s\n", tmp);
//    printf("Making lp.\n");
//    cutLPSolvers[s].writeLp(tmp, "lp");
  }

  if (bestSplitIndx != -1) {
#ifdef TRACE
    printf(
        "In generateBestGICAvgCutOptimalBasis: Cut store size before addition: %d.\n",
        (int) cutStore.size());
#endif
    int cutAdded = addCutToStore(bestSplitIndx, nNonBasicCols, tmpCut,
        cutStore, &cutCoeff[0], bestViol, -1, -1.0, chooseSplits);
#ifdef TRACE
    printf(
        "In generateBestGICAvgCutOptimalBasis: addCutToStore returned %d.\n",
        cutAdded);
    printf(
        "In generateBestGICAvgCutOptimalBasis: Cut store size after addition: %d.\n",
        (int) cutStore.size());
    printf(
        "Adding cuts specific to splits for split index %d. The cut added is %d.\n",
        bestSplitIndx, (int) cutStore.size() - 1);
#endif
    if (cutAdded == 1) {
      cutsSpecificToSplits[bestSplitIndx].push_back(cutStore.size() - 1);
      numCutsGenerated++;
    }
  }

}

void generateGICsAvgCutOptimalBasis(cut &tmpCut, int nTotalCuts,
    int &nCutsGenerated, PointCutsSolverInterface* &cutLPSolvers,
    int numSplits, int nNonBasicCols, vector<cut> &cutStore,
    vector<vector<int> > &cutsSpecificToSplits,
    chooseCutGeneratingSets &chooseSplits) {

  //  vector<double> avgCutCoeff(numSplits,0.0);
  for (int s = 0; s < numSplits; s++) {
    if (nCutsGenerated < nTotalCuts) {
      for (int j = 0; j < nNonBasicCols; j++)
        cutLPSolvers[s].setObjCoeff(j, 1.0);
      cutLPSolvers[s].initialSolve();
      const double* Soln = cutLPSolvers[s].getColSolution();
      if (cutLPSolvers[s].isProvenOptimal()) {
        if (addCutToStore(s, nNonBasicCols, tmpCut, cutStore, Soln,
            cutLPSolvers[s].getObjValue(), -1, -1.0,
            chooseSplits)) {
          cutsSpecificToSplits[s].push_back(cutStore.size() - 1);
          nCutsGenerated++;
        }
      }
    }
  }

}

void genGICsAvgCutCoeffStrongAdjBasis(int splitIndx, int nTotalCuts,
    int &nCutsGenerated, cut &tmpCut, PointCutsSolverInterface* solver,
    PointCutsSolverInterface* cutLPSolvers, SolutionInfo &solnInfo,
    chooseCutGeneratingSets &chooseSplits,
    vector<vector<int> > &raysToBeCutByHplane, vector<int> &raysToBeCut,
    vector<int> &hplanesIndxToBeActivated,
    vector<int> &hplanesToBeActivatedUBFlag,
    vector<int> &hplanesRowIndxToBeActivated, int numSplits,
    vector<cut> &cutStore, vector<int> &cutsSpecificToSplits,
    vector<int> &strongVertHplaneIndex, FILE *inst_info_out) {

  const double* UB = solver.getColUpper();
  double newCoord, hplaneRHS;
  for (int j = 0; j < solnInfo.numNB; j++)
    cutLPSolvers.setObjCoeff(j, 1.0);
  for (unsigned r = 0; r < raysToBeCut.size(); r++) {
    int h = strongVertHplaneIndex[r];
    if (hplanesRowIndxToBeActivated[h] != -1) {
      if (nCutsGenerated < nTotalCuts) {
        if (hplanesToBeActivatedUBFlag[h] == 1)
          hplaneRHS = solnInfo.a0[hplanesRowIndxToBeActivated[h]]
              - UB[hplanesIndxToBeActivated[h]];
        else
          hplaneRHS = solnInfo.a0[hplanesRowIndxToBeActivated[h]];
        newCoord =
            hplaneRHS
                / (-solnInfo.raysOfC1[raysToBeCut[r]][hplanesRowIndxToBeActivated[h]]);
        for (int j = 0; j < raysToBeCut[r]; j++) {
          newCoord -=
              solnInfo.raysOfC1[j][hplanesRowIndxToBeActivated[h]]
                  / solnInfo.raysOfC1[raysToBeCut[r]][hplanesRowIndxToBeActivated[h]];
        }
        for (int j = raysToBeCut[r] + 1; j < solnInfo.numNB;
            j++) {
          newCoord -=
              solnInfo.raysOfC1[j][hplanesRowIndxToBeActivated[h]]
                  / solnInfo.raysOfC1[raysToBeCut[r]][hplanesRowIndxToBeActivated[h]];
        }
        newCoord -=
            (1.0
                / (-solnInfo.raysOfC1[raysToBeCut[r]][hplanesRowIndxToBeActivated[h]]));

        //        assert(newCoord >= -param.getEPS());
        //      cout << "newCoord = " << newCoord << "\n";
        cutLPSolvers.setObjCoeff(raysToBeCut[r], newCoord);
        cutLPSolvers.resolve();

        if (cutLPSolvers.isProvenOptimal()) {
          const double* Soln = cutLPSolvers.getColSolution();
          if (addCutToStore(splitIndx, solnInfo.numNB,
              tmpCut, cutStore, Soln, cutLPSolvers.getObjValue(),
              -1, -1.0, chooseSplits)) {
            nCutsGenerated++;
            cutsSpecificToSplits.push_back(cutStore.size() - 1);
          }

        } else {
          if (cutLPSolvers.isProvenPrimalInfeasible()) {
#ifdef TRACE
            printf(
                "*** ERROR: Problem is infeasible, change objective\n");
#endif
            writeErrorToLog(
                "*** ERROR: Problem is infeasible, change objective in genGICsAvgCutCoeffStrongAdjBasis.\n",
                inst_info_out);
            exit(1);
          }
        }

        cutLPSolvers.setObjCoeff(raysToBeCut[r], 1.0);
      } else
        break;
    }
  }

}

/**********************************************************************************
 * Useful functions
 **********************************************************************************/
/**
 * @brief Removes any cuts that are repeated.
 * Note that the same cut may possibly be generated for different splits,
 * and this is okay, but for the same split, there should be only one of each.
 */
void removeDuplicateCuts(vector<cut> &cutinfo) {
  vector<cut> tmpcutinfo;

  for (int cut = 0; cut < (int) cutinfo.size(); cut++) {
#ifdef TRACE
    printf("Comparing cut %d.\n", cut);
#endif
    int index = cutExists(tmpcutinfo, cutinfo[cut].splitIndex,
        cutinfo[cut].RHS, cutinfo[cut].coeff);
    if (index == -1) {
      tmpcutinfo.push_back(cutinfo[cut]);
    }
  }

  cutinfo = tmpcutinfo;

}

/**
 * @brief Checks whether a specific cut exists in the cut store.
 *
 * @param tmpcutinfo  ::  The current set of cuts stored (no dups).
 * @param split       ::  Split for the cut being checked.
 * @param rhs         ::  Rhs of the cut being checked.
 * @param coeffs      ::  Coeffs of the cut being checked.
 *
 * @returns -1 if there does not exist a cut in the cuts passed in which all coeffs
 * of the cut are the same, the rhs is the same, and the split is the same.
 * @returns Index of the cut that is the same if one does exist.
 */
int cutExists(vector<cut> &tmpcutinfo, const int split, const double rhs,
    vector<double> &coeffs) {
  int index = -1;

  for (int cut = 0; cut < (int) tmpcutinfo.size(); cut++) {
// Ensure we are looking at a cut for the same split
    int currsplit = tmpcutinfo[cut].splitIndex;
    if (currsplit != split)
      continue;

// Ensure the rhs of both cuts is the same
    double currRHS = tmpcutinfo[cut].RHS;
    if (std::abs(rhs - currRHS) > param.getEPS())
      continue;

// Now check that all coeffs are the same
    int c;
    for (c = 0; c < (int) tmpcutinfo[cut].coeff.size(); c++) {
      double currcoeff = tmpcutinfo[cut].coeff[c];
      if (std::abs(currcoeff - coeffs[c]) > param.getEPS()) {
        break;
      }
    }
    if (c == (int) tmpcutinfo[cut].coeff.size()) {
#ifdef TRACE
      printf("\tFound cut in the previous set of cuts at index %d.\n",
          cut);
#endif
      index = cut;
      break;
    }
  }

  return index;
}

/**********************************************************************************
 * Printing methods for cuts
 **********************************************************************************/

void printGICs(FILE* fptr, PointCutsSolverInterface* solver,
    chooseCutGeneratingSets &chooseSplits, SolutionInfo &solnInfo,
    vector<cut> &GICCutStore, int* numVert, vector<int>& unbnddVertFails,
    vector<int>& nonUnqiueVertFails) {

  fprintf(fptr, "Print out GIC cut generation information and cuts\n");
  fprintf(fptr, "splitVarIndx,numCutVert,unbddVertFails,nonUniqVerfails\n");
  for (int s = 0; s < chooseSplits.nEffCutGenSets; s++) {
    fprintf(fptr, "%d,%d,%d,%d\n", chooseSplits.splitVarIndex[s],
        numVert[s], unbnddVertFails[s], nonUnqiueVertFails[s]);
  }
  fprintf(fptr, "\n\n");

  fprintf(fptr, ",,,,,coord,\n");

  fprintf(fptr, ",,,,,");
  for (int c = 0; c < solnInfo.numNB; c++)
    fprintf(fptr, "%d,", c);
  fprintf(fptr, "\n");

  fprintf(fptr, "splitVarIndx,cutNo.,vertIndx,th,RHS,");
  for (int c = 0; c < solnInfo.numNB; c++) {
    if (solnInfo.nonBasicVarIndex[c] < solnInfo.numCols)
      fprintf(fptr, "%s,",
          solver.getColName(solnInfo.nonBasicVarIndex[c]).c_str());
    else
      fprintf(fptr, "%s,",
          solver.getRowName(
              solnInfo.nonBasicVarIndex[c] - solnInfo.numCols).c_str());
  }
  fprintf(fptr, "\n");

  //  for (int s = 0; s < numSplits; s++) {
  for (unsigned c = 0; c < GICCutStore.size(); c++) {
    fprintf(fptr, "%d,%d,%d,%lf,%lf,", GICCutStore[c].splitVarIndex, c,
        GICCutStore[c].cutGenVertexIndex, GICCutStore[c].th,
        GICCutStore[c].RHS);
    for (int j = 0; j < solnInfo.numNB; j++)
      fprintf(fptr, "%lf,", GICCutStore[c].coeff[j]);
    fprintf(fptr, "\n");
  }
  //  }
}
