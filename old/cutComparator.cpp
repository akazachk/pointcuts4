/*
 * cutComparator.cpp
 *
 *  Created on: Jun 22, 2012
 *      Author: selva
 */

#include "cutComparator.hpp"

void compareCutCoefficients(FILE* fptr, string &instName, int hplaneHeur,
    int cutSelHeur, int numSplits, int nRaysToBeCut, int nNonBasicCols,
    vector<int> &splitVarIndx, vector<cut> &GICCutStore,
    vector<cut> &stdIntersectionCuts, PointCutsSolverInterface *cutLpSolvers) {
  vector<double> avgCoeff(numSplits, 0.0);
  for (int s = 0; s < numSplits; s++) {
    for (int j = 0; j < nNonBasicCols; j++)
      avgCoeff[s] += stdIntersectionCuts[s].coeff[j];
    avgCoeff[s] /= (double) nNonBasicCols;
  }

  //  fprintf(
  //      fptr,
  //      "splitVarIndx,cutNo.,stdIntAvgCoeff,GICAvgCoeff,GICBetter, GICEqual, GICWorse\n");
  double GICAvgCoeff;
  int GICBetter, GICEqual, GICWorse;
  //  for (int s = 0; s < numSplits; s++) {
  fprintf(fptr,
      "instName, hplaneHeur, cutSelHeur, numSplits, nRaysToBeCut, splitVarIndx[GICCutStore[c].splitIndx], c, avgCoeff[GICCutStore[c].splitIndx], GICAvgCoeff, GICBetter, GICEqual, GICWorse\n");
  for (unsigned c = 0; c < GICCutStore.size(); c++) {
#ifdef TRACE
    printf(
        "##################################################### c = %d #####################################################\n",
        c);
#endif
    GICBetter = 0;
    GICEqual = 0;
    GICWorse = 0;
    GICAvgCoeff = 0.0;
    for (int j = 0; j < nNonBasicCols; j++)
      GICAvgCoeff += GICCutStore[c].coeff[j];
    GICAvgCoeff /= (double) nNonBasicCols;
    for (int j = 0; j < nNonBasicCols; j++) {
//      printf("Here0.1: GICCutStore[c].coeff[j] is %f. \n", GICCutStore[c].coeff[j]);
//      printf("Split: %d\n", GICCutStore[c].splitIndx);
//      printf("stdIntersectionCuts[GICCutStore[c].splitIndx].coeff.size(): %d\n", (int) stdIntersectionCuts[GICCutStore[c].splitIndx].coeff.size());
//      printf("Here0.2: stdIntersectionCuts[GICCutStore[c].splitIndx].coeff[j] is %f. \n", stdIntersectionCuts[GICCutStore[c].splitIndx].coeff[j]);
      if (GICCutStore[c].coeff[j]
          <= stdIntersectionCuts[GICCutStore[c].splitIndex].coeff[j]
              - param.getEPS()) {
        GICBetter++;
//        printf("Here1-j=%d.\n",j);
      } else {
        if (GICCutStore[c].coeff[j]
            >= stdIntersectionCuts[GICCutStore[c].splitIndex].coeff[j]
                + param.getEPS()) {
          GICWorse++;
//          printf("Here2-j=%d.\n",j);
        } else
          GICEqual++;
      }
    }

    fprintf(fptr, "%s,%d,%d,%d,%d,%d,%d,%lf,%lf,%d,%d,%d\n",
        instName.c_str(), hplaneHeur, cutSelHeur, numSplits,
        nRaysToBeCut, splitVarIndx[GICCutStore[c].splitIndex], c,
        avgCoeff[GICCutStore[c].splitIndex], GICAvgCoeff, GICBetter,
        GICEqual, GICWorse);
  }

}

void compareCutRootNodeBoundsAllCutsSub(char* filename, const char* instName,
    int hplaneHeur, int cutSelHeur, int nRaysToBeCut,
    PointCutsSolverInterface* tmpsolver, SolutionInfo &solnInfo,
    chooseCutGeneratingSets &chooseSplits, vector<cut> &SIC,
    vector<cut> &cutStore, vector<vector<int> > &hplanesIndxToBeActivated,
    double &SICBound, double &SICBasisCondNumber, double &GICBound,
    double &GICBasisCondNumber, double &SICAndGICBound,
    double &SICAndGICBasisCondNumber, string lp_name_stub,
    FILE *inst_info_out) {

  PointCutsSolverInterface solver(tmpsolver);
  solver.setHintParam(OsiDoPresolveInInitial, 1);
  solver.setHintParam(OsiDoPresolveInResolve, 1);
  solver.initialSolve();

  vector<int> constColIndx(solnInfo.numCols);
  for (int i = 0; i < solnInfo.numCols; i++)
    constColIndx[i] = i;

  //Add intersection cuts and evaluate bound and if you get the fractional closure
#ifdef TRACE
  printf(
      "\n## Solving problem with all standard intersection cuts added. ##\n");
#endif
  for (int i = 0; i < chooseSplits.nEffCutGenSets; i++)
    solver.addRow(solnInfo.numCols, &constColIndx[0],
        &SIC[solnInfo.feasSplit[i]].coeff[0],
        SIC[solnInfo.feasSplit[i]].RHS, solver.getInfinity());
  solver.initialSolve();
  SICBasisCondNumber = compute_condition_number_norm2(&solver);
  if (solver.isProvenOptimal()) {
    SICBound = solver.getObjValue();
  } else {
    SICBound = solver.getInfinity();
  }

  if (WRITE_LP_COMPARE_FILE) {
    solver.writeLp((lp_name_stub + "AllSICs").c_str());
    SolutionInfo solnInfoTmp(solver, true);
    solnInfoTmp.printBriefSolutionInfo(solver, lp_name_stub + "AllSICs");
  }

  // Delete SIC rows added
  vector<int> delRows0(chooseSplits.nEffCutGenSets);
  for (int i = 0; i < chooseSplits.nEffCutGenSets; i++)
    delRows0[i] = solnInfo.numRows + i;
  solver.deleteRows(chooseSplits.nEffCutGenSets, &delRows0[0]);

#ifdef TRACE
  printf("\nSolving to get initial solution back.\n");
#endif
  solver.initialSolve();

  //Add all cuts from all splits (required for split closure experiments)
  int totalNumCuts = (int) cutStore.size();
  for (int j = 0; j < (int) cutStore.size(); j++) {
    solver.addRow(solnInfo.numCols, &constColIndx[0], &cutStore[j].coeff[0],
        cutStore[j].RHS, solver.getInfinity());
  }

#ifdef TRACE
  printf(
      "\n## Solving problem with all generalized intersection cuts added. ##\n");
#endif
  solver.initialSolve();
  GICBasisCondNumber = compute_condition_number_norm2(&solver);
  if (solver.isProvenOptimal()) {
    GICBound = solver.getObjValue();
  } else {
    GICBound = solver.getInfinity();
  }

  if (WRITE_LP_COMPARE_FILE) {
    solver.writeLp((lp_name_stub + "AllGICs").c_str());
    //writeSoln(&solver, lp_name_stub);
    SolutionInfo solnInfoTmp(solver, true);
    solnInfoTmp.printBriefSolutionInfo(solver, lp_name_stub + "AllGICs");
  }

  // Delete GIC rows added
  vector<int> delRowsAllCuts(totalNumCuts);
  for (int c = 0; c < totalNumCuts; c++)
    delRowsAllCuts[c] = solnInfo.numRows + c;
  if (totalNumCuts > 0)
    solver.deleteRows(totalNumCuts, &delRowsAllCuts[0]);

#ifdef TRACE
  printf("\nSolving to get initial solution back.\n");
#endif

  solver.initialSolve();

  // Evaluate bound after adding all SICs *and* GICs
#ifdef TRACE
  printf("\n## Solving problem with all SICs and GICs added. ##\n");
#endif
  for (int i = 0; i < chooseSplits.nEffCutGenSets; i++)
    solver.addRow(solnInfo.numCols, &constColIndx[0],
        &SIC[solnInfo.feasSplit[i]].coeff[0],
        SIC[solnInfo.feasSplit[i]].RHS, solver.getInfinity());
  for (int j = 0; j < (int) cutStore.size(); j++) {
    solver.addRow(solnInfo.numCols, &constColIndx[0], &cutStore[j].coeff[0],
        cutStore[j].RHS, solver.getInfinity());
  }
  solver.initialSolve();
  SICAndGICBasisCondNumber = compute_condition_number_norm2(&solver);
  if (solver.isProvenOptimal()) {
    SICAndGICBound = solver.getObjValue();
  } else {
    SICAndGICBound = solver.getInfinity();
  }

  if (WRITE_LP_COMPARE_FILE) {
    solver.writeLp((lp_name_stub + "AllSICsAndGICs").c_str());
    SolutionInfo solnInfoTmp(solver, true);
    solnInfoTmp.printBriefSolutionInfo(solver,
        lp_name_stub + "AllSICsAndGICs");
  }
}

void compareCutRootNodeBoundsAllCutsLifted(char* filename, const char* instName,
    int hplaneHeur, int cutSelHeur, int nRaysToBeCut,
    PointCutsSolverInterface* tmpsolver, SolutionInfo &solnInfo,
    chooseCutGeneratingSets &chooseSplits, vector<cut> &SIC,
    vector<cut> &cutStore, vector<vector<int> > &hplanesIndxToBeActivated,
    double &SICBound, double &SICBasisCondNumber, double &GICBound,
    double &GICBasisCondNumber, double &SICAndGICBound,
    double &SICAndGICBasisCondNumber, string lp_name_stub,
    FILE *inst_info_out) {

  PointCutsSolverInterface solver(tmpsolver);
  solver.setHintParam(OsiDoPresolveInInitial, 1);
  solver.setHintParam(OsiDoPresolveInResolve, 1);
  solver.initialSolve();

  vector<int> constColIndx(solnInfo.numCols);
  for (int i = 0; i < solnInfo.numCols; i++)
    constColIndx[i] = i;

  //Add intersection cuts and evaluate bound and if you get the fractional closure
#ifdef TRACE
  printf(
      "\n## Solving problem with all standard intersection cuts added. ##\n");
#endif
  for (int i = 0; i < chooseSplits.nEffCutGenSets; i++)
    solver.addRow(solnInfo.numCols, &constColIndx[0], &SIC[i].coeff[0],
        SIC[i].RHS, solver.getInfinity());
  solver.initialSolve();
  SICBasisCondNumber = compute_condition_number_norm2(&solver);
  if (solver.isProvenOptimal()) {
    SICBound = solver.getObjValue();
  } else {
    SICBound = solver.getInfinity();
  }

  if (WRITE_LP_COMPARE_FILE) {
    solver.writeLp((lp_name_stub + "AllSICs").c_str());
    SolutionInfo solnInfoTmp(solver, true);
    solnInfoTmp.printBriefSolutionInfo(solver, lp_name_stub + "AllSICs");
  }

  // Delete SIC rows added
  vector<int> delRows0(chooseSplits.nEffCutGenSets);
  for (int i = 0; i < chooseSplits.nEffCutGenSets; i++)
    delRows0[i] = solnInfo.numRows + i;
  solver.deleteRows(chooseSplits.nEffCutGenSets, &delRows0[0]);

#ifdef TRACE
  printf("\nSolving to get initial solution back.\n");
#endif
  solver.initialSolve();

  //Add all cuts from all splits (required for split closure experiments)
  int totalNumCuts = (int) cutStore.size();
  for (int j = 0; j < (int) cutStore.size(); j++) {
    solver.addRow(solnInfo.numCols, &constColIndx[0], &cutStore[j].coeff[0],
        cutStore[j].RHS, solver.getInfinity());
  }

#ifdef TRACE
  printf(
      "\n## Solving problem with all generalized intersection cuts added. ##\n");
#endif
  solver.initialSolve();
  GICBasisCondNumber = compute_condition_number_norm2(&solver);
  if (solver.isProvenOptimal()) {
    GICBound = solver.getObjValue();
  } else {
    GICBound = solver.getInfinity();
  }

  if (WRITE_LP_COMPARE_FILE) {
    solver.writeLp((lp_name_stub + "AllGICs").c_str());
    SolutionInfo solnInfoTmp(solver, true);
    solnInfoTmp.printBriefSolutionInfo(solver, lp_name_stub + "AllGICs");
  }

  // Delete GIC rows added
  vector<int> delRowsAllCuts(totalNumCuts);
  for (int c = 0; c < totalNumCuts; c++)
    delRowsAllCuts[c] = solnInfo.numRows + c;
  if (totalNumCuts > 0)
    solver.deleteRows(totalNumCuts, &delRowsAllCuts[0]);

#ifdef TRACE
  printf("\nSolving to get initial solution back.\n");
#endif
  solver.initialSolve();

  // Evaluate bound after adding all SICs *and* GICs
#ifdef TRACE
  printf("\n## Solving problem with all SICs and GICs added. ##\n");
#endif
  for (int i = 0; i < chooseSplits.nEffCutGenSets; i++)
    solver.addRow(solnInfo.numCols, &constColIndx[0], &SIC[i].coeff[0],
        SIC[i].RHS, solver.getInfinity());
  for (int j = 0; j < (int) cutStore.size(); j++) {
    solver.addRow(solnInfo.numCols, &constColIndx[0], &cutStore[j].coeff[0],
        cutStore[j].RHS, solver.getInfinity());
  }
  solver.initialSolve();
  SICAndGICBasisCondNumber = compute_condition_number_norm2(&solver);
  if (solver.isProvenOptimal()) {
    SICAndGICBound = solver.getObjValue();
  } else {
    SICAndGICBound = solver.getInfinity();
  }

  if (WRITE_LP_COMPARE_FILE) {
    solver.writeLp((lp_name_stub + "AllSICsAndGICs").c_str());
    SolutionInfo solnInfoTmp(solver, true);
    solnInfoTmp.printBriefSolutionInfo(solver,
        lp_name_stub + "AllSICsAndGICs");
  }

}

/**
 * This compares LSICs to OSICs, using all LSICs for each split
 */
void compare_LSIC_to_OSIC_Root_All(const char* instName,
    PointCutsSolverInterface* tmpsolver, SolutionInfo &solnInfo,
    chooseCutGeneratingSets &chooseSplits, vector<cut> &OSIC,
    vector<vector<cut> > &cutStore, double &OSICBound,
    double &OSICBasisCondNumber, double &LSICBound,
    double &LSICBasisCondNumber, double &OSICAndLSICBound,
    double &OSICAndLSICBasisCondNumber, string lp_name_stub,
    FILE *inst_info_out) {

  PointCutsSolverInterface solver(tmpsolver);
  solver.setHintParam(OsiDoPresolveInInitial, 1);
  solver.setHintParam(OsiDoPresolveInResolve, 1);
  solver.initialSolve();

  vector<int> constColIndx(solnInfo.numCols);
  for (int i = 0; i < solnInfo.numCols; i++)
    constColIndx[i] = i;

  //Add intersection cuts and evaluate bound and if you get the fractional closure
#ifdef TRACE
  printf(
      "\n## Solving problem with all original standard intersection cuts added. ##\n");
#endif
  for (int i = 0; i < chooseSplits.nEffCutGenSets; i++)
    solver.addRow(solnInfo.numCols, &constColIndx[0], &OSIC[i].coeff[0],
        OSIC[i].RHS, solver.getInfinity());
  solver.initialSolve();
  OSICBasisCondNumber = compute_condition_number_norm2(&solver);
  if (solver.isProvenOptimal()) {
    OSICBound = solver.getObjValue();
  } else {
    OSICBound = solver.getInfinity();
  }

  if (WRITE_LP_COMPARE_FILE) {
    solver.writeLp((lp_name_stub + "AllOSICs").c_str());
    SolutionInfo solnInfoTmp(solver, true);
    solnInfoTmp.printBriefSolutionInfo(solver, lp_name_stub + "AllOSICs");
  }

  // Delete SIC rows added
  vector<int> delRows0(chooseSplits.nEffCutGenSets);
  for (int i = 0; i < chooseSplits.nEffCutGenSets; i++)
    delRows0[i] = solnInfo.numRows + i;
  solver.deleteRows(chooseSplits.nEffCutGenSets, &delRows0[0]);

#ifdef TRACE
  printf("\nSolving to get initial solution back.\n");
#endif
  solver.initialSolve();

  //Add all cuts from all splits (required for split closure experiments)
  int totalNumCuts = 0;
  for (int j = 0; j < (int) cutStore.size(); j++) {
    for (int currcut = 0; currcut < (int) cutStore[j].size(); currcut++) {
      totalNumCuts++;
      solver.addRow(solnInfo.numCols, &constColIndx[0],
          &cutStore[j][currcut].coeff[0], cutStore[j][currcut].RHS,
          solver.getInfinity());
    }
  }

#ifdef TRACE
  printf(
      "\n## Solving problem with all lifted standard intersection cuts added. ##\n");
#endif
  solver.initialSolve();
  LSICBasisCondNumber = compute_condition_number_norm2(&solver);
  if (solver.isProvenOptimal()) {
    LSICBound = solver.getObjValue();
  } else {
    LSICBound = solver.getInfinity();
  }

  if (WRITE_LP_COMPARE_FILE) {
    solver.writeLp((lp_name_stub + "AllLSICs").c_str());
    SolutionInfo solnInfoTmp(solver, true);
    solnInfoTmp.printBriefSolutionInfo(solver, lp_name_stub + "AllLSICs");
  }

  // Delete LSIC rows added
  vector<int> delRowsAllCuts(totalNumCuts);
  for (int c = 0; c < totalNumCuts; c++)
    delRowsAllCuts[c] = solnInfo.numRows + c;
  if (totalNumCuts > 0)
    solver.deleteRows(totalNumCuts, &delRowsAllCuts[0]);

#ifdef TRACE
  printf("\nSolving to get initial solution back.\n");
#endif
  solver.initialSolve();

  // Evaluate bound after adding all OSICs *and* initial LSICs
#ifdef TRACE
  printf("\n## Solving problem with all OSICs and all LSICs added. ##\n");
#endif
  for (int i = 0; i < chooseSplits.nEffCutGenSets; i++)
    solver.addRow(solnInfo.numCols, &constColIndx[0], &OSIC[i].coeff[0],
        OSIC[i].RHS, solver.getInfinity());
  for (int j = 0; j < (int) cutStore.size(); j++) {
    for (int currcut = 0; currcut < (int) cutStore[j].size(); currcut++) {
      solver.addRow(solnInfo.numCols, &constColIndx[0],
          &cutStore[j][currcut].coeff[0], cutStore[j][currcut].RHS,
          solver.getInfinity());
    }
  }
  solver.initialSolve();
  OSICAndLSICBasisCondNumber = compute_condition_number_norm2(&solver);
  if (solver.isProvenOptimal()) {
    OSICAndLSICBound = solver.getObjValue();
  } else {
    OSICAndLSICBound = solver.getInfinity();
  }

  if (WRITE_LP_COMPARE_FILE) {
    solver.writeLp((lp_name_stub + "AllOSICsAndAllLSICs").c_str());
    SolutionInfo solnInfoTmp(solver, true);
    solnInfoTmp.printBriefSolutionInfo(solver,
        lp_name_stub + "AllOSICsAndAllLSICs");
  }

}

/**
 * This compares LSICs to OSICs, only using the *first* LSIC for each split
 */
void compare_LSIC_to_OSIC_Root_Initial(const char* instName,
    PointCutsSolverInterface* tmpsolver, SolutionInfo &solnInfo,
    chooseCutGeneratingSets &chooseSplits, vector<cut> &OSIC,
    vector<vector<cut> > &cutStore, double &OSICBound,
    double &OSICBasisCondNumber, double &LSICBound,
    double &LSICBasisCondNumber, double &OSICAndLSICBound,
    double &OSICAndLSICBasisCondNumber, string lp_name_stub,
    FILE *inst_info_out) {

  PointCutsSolverInterface solver(tmpsolver);
  solver.setHintParam(OsiDoPresolveInInitial, 1);
  solver.setHintParam(OsiDoPresolveInResolve, 1);
  solver.initialSolve();

  vector<int> constColIndx(solnInfo.numCols);
  for (int i = 0; i < solnInfo.numCols; i++)
    constColIndx[i] = i;

  //Add intersection cuts and evaluate bound and if you get the fractional closure
#ifdef TRACE
  printf(
      "\n## Solving problem with all original standard intersection cuts added. ##\n");
#endif
  for (int i = 0; i < chooseSplits.nEffCutGenSets; i++)
    solver.addRow(solnInfo.numCols, &constColIndx[0], &OSIC[i].coeff[0],
        OSIC[i].RHS, solver.getInfinity());
  solver.initialSolve();
  OSICBasisCondNumber = compute_condition_number_norm2(&solver);
  if (solver.isProvenOptimal()) {
    OSICBound = solver.getObjValue();
  } else {
    OSICBound = solver.getInfinity();
  }

  if (WRITE_LP_COMPARE_FILE) {
    solver.writeLp((lp_name_stub + "AllOSICs").c_str());
    SolutionInfo solnInfoTmp(solver, true);
    solnInfoTmp.printBriefSolutionInfo(solver, lp_name_stub + "AllOSICs");
  }

  // Delete SIC rows added
  vector<int> delRows0(chooseSplits.nEffCutGenSets);
  for (int i = 0; i < chooseSplits.nEffCutGenSets; i++)
    delRows0[i] = solnInfo.numRows + i;
  solver.deleteRows(chooseSplits.nEffCutGenSets, &delRows0[0]);

#ifdef TRACE
  printf("\nSolving to get initial solution back.\n");
#endif
  solver.initialSolve();

  //Add all cuts from all splits (required for split closure experiments)
  int totalNumCuts = (int) cutStore.size();
  for (int j = 0; j < (int) cutStore.size(); j++) {
    solver.addRow(solnInfo.numCols, &constColIndx[0],
        &cutStore[j][0].coeff[0], cutStore[j][0].RHS,
        solver.getInfinity());
  }

#ifdef TRACE
  printf(
      "\n## Solving problem with initial lifted standard intersection cuts added. ##\n");
#endif
  solver.initialSolve();
  LSICBasisCondNumber = compute_condition_number_norm2(&solver);
  if (solver.isProvenOptimal()) {
    LSICBound = solver.getObjValue();
  } else {
    LSICBound = solver.getInfinity();
  }

  if (WRITE_LP_COMPARE_FILE) {
    solver.writeLp((lp_name_stub + "InitLSICs").c_str());
    SolutionInfo solnInfoTmp(solver, true);
    solnInfoTmp.printBriefSolutionInfo(solver, lp_name_stub + "InitLSICs");
  }

  // Delete LSIC rows added
  vector<int> delRowsAllCuts(totalNumCuts);
  for (int c = 0; c < totalNumCuts; c++)
    delRowsAllCuts[c] = solnInfo.numRows + c;
  if (totalNumCuts > 0)
    solver.deleteRows(totalNumCuts, &delRowsAllCuts[0]);

#ifdef TRACE
  printf("\nSolving to get initial solution back.\n");
#endif
  solver.initialSolve();

  // Evaluate bound after adding all OSICs *and* initial LSICs
#ifdef TRACE
  printf("\n## Solving problem with all OSICs and initial LSICs added. ##\n");
#endif
  for (int i = 0; i < chooseSplits.nEffCutGenSets; i++)
    solver.addRow(solnInfo.numCols, &constColIndx[0], &OSIC[i].coeff[0],
        OSIC[i].RHS, solver.getInfinity());
  for (int j = 0; j < (int) cutStore.size(); j++) {
    solver.addRow(solnInfo.numCols, &constColIndx[0],
        &cutStore[j][0].coeff[0], cutStore[j][0].RHS,
        solver.getInfinity());
  }
  solver.initialSolve();
  OSICAndLSICBasisCondNumber = compute_condition_number_norm2(&solver);
  if (solver.isProvenOptimal()) {
    OSICAndLSICBound = solver.getObjValue();
  } else {
    OSICAndLSICBound = solver.getInfinity();
  }

  if (WRITE_LP_COMPARE_FILE) {
    solver.writeLp((lp_name_stub + "AllOSICsAndInitLSICs").c_str());
    SolutionInfo solnInfoTmp(solver, true);
    solnInfoTmp.printBriefSolutionInfo(solver,
        lp_name_stub + "AllOSICsAndInitLSICs");
  }

}

void compareCutRootNodeBoundsRounds(int numRounds, FILE* cutSum,
    string &instName, int hplaneHeur, int cutSelHeur, int nRaysToBeCut,
    PointCutsSolverInterface* solver, SolutionInfo &solnInfo,
    chooseCutGeneratingSets &chooseSplits, vector<cut> &stdIntersectionCuts,
    vector<cut> &cutStore, vector<vector<int> > hplanesIndxToBeActivated,
    string lp_name_stub, FILE *inst_info_out) {
  solver.setHintParam(OsiDoPresolveInInitial, 1);
  solver.setHintParam(OsiDoPresolveInResolve, 1);
  int nHplanes = -1;
  for (int i = 0; i < chooseSplits.nEffCutGenSets; i++) {
    if ((int) hplanesIndxToBeActivated[i].size() > nHplanes)
      nHplanes = hplanesIndxToBeActivated[i].size();
  }

  fprintf(cutSum,
      "instName, hplaneHeur, cutSelHeur, nEffCutGenSets, nRaysToBeCut, nHplanes, numSICs, SIC_Bound, SIC_Basis_Cond_Num, currCutNo, SIC_Bound, GIC_Bound, GIC_Basis_Cond_Num, Ratio\n");
  fprintf(cutSum, "%s,%d,%d,%d,%d,%d,", instName.c_str(), hplaneHeur,
      cutSelHeur, chooseSplits.nEffCutGenSets, nRaysToBeCut, nHplanes);
  vector<int> constColIndx(solnInfo.numCols);
  for (int i = 0; i < solnInfo.numCols; i++)
    constColIndx[i] = i;

//  solver.enableSimplexInterface(true);
  //Add intersection cuts and evaluate bound and if you get the fractional closure
#ifdef TRACE
  printf(
      "\n## Solving problem with all standard intersection cuts added. ##\n");
#endif
  for (int i = 0; i < chooseSplits.nEffCutGenSets; i++)
    solver.addRow(solnInfo.numCols, &constColIndx[0],
        &stdIntersectionCuts[i].coeff[0], stdIntersectionCuts[i].RHS,
        solver.getInfinity());
//  solver.disableSimplexInterface();
  solver.initialSolve();
  double stdInterCutBound = solver.getObjValue();
  double stdInterCutBasisCondNumber = compute_condition_number_norm2(&solver);
  if (solver.isProvenOptimal()) {
    fprintf(cutSum, "%d,%lf,%lf,", (int) stdIntersectionCuts.size(),
        stdInterCutBound, stdInterCutBasisCondNumber);
  } else {
    printf(
        "*** ERROR: LP after adding standard intersection cuts is not optimal in compareCutRootNodeBoundsRounds.\n");
    writeErrorToLog(
        "*** ERROR: LP after adding standard intersection cuts is not optimal in compareCutRootNodeBoundsRounds.\n",
        inst_info_out);
    char outname[300];
    snprintf(outname, sizeof(outname) / sizeof(char), "%sSICfail",
        lp_name_stub.c_str());
    solver.writeLp(outname, "lp");
    exit(1);
  }
  vector<int> delRows0(chooseSplits.nEffCutGenSets);
  for (int i = 0; i < chooseSplits.nEffCutGenSets; i++)
    delRows0[i] = solnInfo.numRows + i;
//  solver.enableSimplexInterface(true);
  solver.deleteRows(chooseSplits.nEffCutGenSets, &delRows0[0]);
//  solver.disableSimplexInterface();
  solver.initialSolve();

  //Add rounds of GICs
#ifdef TRACE
  printf(
      "\n## Solving problem with generalized intersection cuts added in rounds. ##\n");
#endif
  int totalNumCuts = 0;
  totalNumCuts = (int) cutStore.size();
  int currCutNo = 0, incr = chooseSplits.nEffCutGenSets;
  for (int r = 0; r < numRounds; r++) {
    cout << "*****Round no : " << r << "\n";
    if (currCutNo + chooseSplits.nEffCutGenSets > totalNumCuts)
      incr = cutStore.size() - currCutNo;
//    solver.enableSimplexInterface(true);
    for (int j = 0; j < incr; j++) {
      solver.addRow(solnInfo.numCols, &constColIndx[0],
          &cutStore[j].coeff[0], cutStore[j].RHS,
          solver.getInfinity());
      currCutNo++;

    }
//    solver.disableSimplexInterface();
    solver.initialSolve();
    double genInterCutBasisCondNumber = compute_condition_number_norm2(
        &solver);
    double GICBound = solver.getObjValue();
    if (solver.isProvenOptimal()) {
      fprintf(cutSum, "%d,%lf,%lf,%lf,", currCutNo, stdInterCutBound,
          GICBound, genInterCutBasisCondNumber);
      if (solver.getObjSense() == 1) {
        fprintf(cutSum, "%lf,",
            ((GICBound - stdInterCutBound) / stdInterCutBound)
                * 100.0);
      } else {
        fprintf(cutSum, "%lf,",
            ((stdInterCutBound - GICBound) / GICBound) * 100.0);
      }
    } else {
#ifdef TRACE
      printf(
          "*** ERROR: LP after adding generalized intersection cuts is not optimal in compareCutRootNodeBoundsRounds.\n");
#endif
      writeErrorToLog(
          "*** ERROR: LP after adding generalized intersection cuts is not optimal in compareCutRootNodeBoundsRounds.\n",
          inst_info_out);
      char outname[300];
      snprintf(outname, sizeof(outname) / sizeof(char), "%sGICfail",
          lp_name_stub.c_str());
      solver.writeLp(outname, "lp");
      exit(1);
    }
  }
//  assert(totalNumCuts == (int )cutStore.size());

//  //Add GICs with standard intersection cuts
//  totalNumCuts += chooseSplits.nEffCutGenSets;
//  for (int i = 0; i < chooseSplits.nEffCutGenSets; i++)
//    solver.addRow(solnInfo.numcols, &constColIndx[0],
//        &stdIntersectionCuts[i].coeff[0], stdIntersectionCuts[i].RHS,
//        solver.getInfinity());
//  solver.initialSolve();
//  double genInterCutBasisCondNumber = compute_condition_number_norm2(&solver);
//  double GICSICBound = solver.getObjValue();
//  if (solver.isProvenOptimal()) {
//    fprintf(cutSum, "%d,%lf,%lf,", totalNumCuts, GICSICBound,
//        genInterCutBasisCondNumber);
//    if (solver.getObjSense() == 1) {
//      fprintf(cutSum, "%lf", ((GICSICBound - stdInterCutBound)
//          / stdInterCutBound) * 100.0);
//    } else {
//      fprintf(cutSum, "%lf", ((stdInterCutBound - GICSICBound)
//          / GICSICBound) * 100.0);
//    }
//
//  } else {
//    printf("LP after adding generalized and standard intersection cuts is not optimal\n");
//    exit(1);
//  }
  fprintf(cutSum, "\n");
//  solver.initialSolve();

//  vector<int> delRowsAllCuts(totalNumCuts);
//  for (int c = 0; c < totalNumCuts; c++)
//    delRowsAllCuts[c] = solnInfo.numrows + c;
//  if (totalNumCuts > 0)
//    solver.deleteRows(totalNumCuts, &delRowsAllCuts[0]);
//
//  solver.initialSolve();
}

/**
 * We already have the bound from just LSICs and OSICs,
 * so we need to compute:
 * 1. Bound from only adding LGICs
 * 2. Bound from LGICs+OSICs
 * 3. Bound from LGICs+LSICs
 */
void compare_LGIC_to_LSIC_and_OSIC_Root_All(const char* instName,
    PointCutsSolverInterface* tmpsolver, SolutionInfo &solnInfo,
    chooseCutGeneratingSets &chooseSplits, vector<cut> &OSIC,
    vector<vector<cut> > &LSIC, vector<vector<cut> > &LGIC,
    double &LGICBound, double &LGICBasisCondNumber,
    double &LGICAndOSICBound, double &LGICAndOSICBasisCondNumber,
    double &LGICAndLSICBound, double &LGICAndLSICBasisCondNumber,
    string lp_name_stub, FILE *inst_info_out) {

  PointCutsSolverInterface solver(tmpsolver);
  solver.setHintParam(OsiDoPresolveInInitial, 1);
  solver.setHintParam(OsiDoPresolveInResolve, 1);
  solver.initialSolve();

  vector<int> constColIndx(solnInfo.numCols);
  for (int i = 0; i < solnInfo.numCols; i++)
    constColIndx[i] = i;

  //Add LGICs and evaluate bound and if you get the fractional closure
#ifdef TRACE
  printf(
      "\n## Solving problem with all lifted generalized intersection cuts added. ##\n");
#endif
  int numLGICsAdded = 0;
  for (int cutind = 0; cutind < (int) LGIC.size(); cutind++) {
    for (int currcut = 0; currcut < (int) LGIC[cutind].size(); currcut++) {
      solver.addRow(solnInfo.numCols, &constColIndx[0],
          &LGIC[cutind][currcut].coeff[0], LGIC[cutind][currcut].RHS,
          solver.getInfinity());
      numLGICsAdded++;
    }
  }
  solver.initialSolve();
  LGICBasisCondNumber = compute_condition_number_norm2(&solver);
  if (solver.isProvenOptimal()) {
    LGICBound = solver.getObjValue();
  } else {
    LGICBound = solver.getInfinity();
  }

  if (WRITE_LP_COMPARE_FILE) {
    solver.writeLp((lp_name_stub + "All_LGICs").c_str());
    SolutionInfo solnInfoTmp(solver, true);
    solnInfoTmp.printBriefSolutionInfo(solver, lp_name_stub + "All_LGICs");
  }

#ifdef TRACE
  printf("\n## Adding all original standard intersection cuts. ##\n");
#endif
  for (int i = 0; i < chooseSplits.nEffCutGenSets; i++)
    solver.addRow(solnInfo.numCols, &constColIndx[0], &OSIC[i].coeff[0],
        OSIC[i].RHS, solver.getInfinity());
  solver.initialSolve();
  LGICAndOSICBasisCondNumber = compute_condition_number_norm2(&solver);
  if (solver.isProvenOptimal()) {
    LGICAndOSICBound = solver.getObjValue();
  } else {
    LGICAndOSICBound = solver.getInfinity();
  }

  if (WRITE_LP_COMPARE_FILE) {
    solver.writeLp((lp_name_stub + "All_LGICs_OSICs").c_str());
    SolutionInfo solnInfoTmp(solver, true);
    solnInfoTmp.printBriefSolutionInfo(solver,
        lp_name_stub + "All_LGICs_OSICs");
  }

  // Delete OSIC rows added
  vector<int> delRows0((int) OSIC.size());
  for (int i = 0; i < (int) OSIC.size(); i++)
    delRows0[i] = solnInfo.numRows + numLGICsAdded + i;
  solver.deleteRows((int) delRows0.size(), &delRows0[0]);

#ifdef TRACE
  printf("\n## Solving problem with all LGICs and all LSICs. ##\n");
#endif
  for (int cutind = 0; cutind < (int) LSIC.size(); cutind++) {
    for (int currcut = 0; currcut < (int) LSIC[cutind].size(); currcut++) {
      solver.addRow(solnInfo.numCols, &constColIndx[0],
          &LSIC[cutind][currcut].coeff[0], LSIC[cutind][currcut].RHS,
          solver.getInfinity());
    }
  }
  solver.initialSolve();
  LGICAndLSICBasisCondNumber = compute_condition_number_norm2(&solver);
  if (solver.isProvenOptimal()) {
    LGICAndLSICBound = solver.getObjValue();
  } else {
    LGICAndLSICBound = solver.getInfinity();
  }

  if (WRITE_LP_COMPARE_FILE) {
    solver.writeLp((lp_name_stub + "All_LGICs_LSICs").c_str());
    SolutionInfo solnInfoTmp(solver, true);
    solnInfoTmp.printBriefSolutionInfo(solver,
        lp_name_stub + "All_LGICs_LSICs");
  }
}

void compare_LGIC_to_InitLSIC_and_OSIC_Root_All(const char* instName,
    PointCutsSolverInterface* tmpsolver, SolutionInfo &solnInfo,
    chooseCutGeneratingSets &chooseSplits, vector<cut> &OSIC,
    vector<vector<cut> > &LSIC, vector<vector<cut> > &LGIC,
    double &LGICBound, double &LGICBasisCondNumber,
    double &LGICAndOSICBound, double &LGICAndOSICBasisCondNumber,
    double &LGICAndLSICBound, double &LGICAndLSICBasisCondNumber,
    string lp_name_stub, FILE *inst_info_out) {

  PointCutsSolverInterface solver(tmpsolver);
  solver.setHintParam(OsiDoPresolveInInitial, 1);
  solver.setHintParam(OsiDoPresolveInResolve, 1);
  solver.initialSolve();

  vector<int> constColIndx(solnInfo.numCols);
  for (int i = 0; i < solnInfo.numCols; i++)
    constColIndx[i] = i;

  //Add LGICs and evaluate bound and if you get the fractional closure
#ifdef TRACE
  printf(
      "\n## Solving problem with all lifted generalized intersection cuts added. ##\n");
#endif
  int numLGICsAdded = 0;
  for (int cutind = 0; cutind < (int) LGIC.size(); cutind++) {
    for (int currcut = 0; currcut < (int) LGIC[cutind].size(); currcut++) {
      solver.addRow(solnInfo.numCols, &constColIndx[0],
          &LGIC[cutind][currcut].coeff[0], LGIC[cutind][currcut].RHS,
          solver.getInfinity());
      numLGICsAdded++;
    }
  }
  solver.initialSolve();
  LGICBasisCondNumber = compute_condition_number_norm2(&solver);
  if (solver.isProvenOptimal()) {
    LGICBound = solver.getObjValue();
  } else {
    LGICBound = solver.getInfinity();
  }

  if (WRITE_LP_COMPARE_FILE) {
    solver.writeLp((lp_name_stub + "All_LGICs").c_str());
    SolutionInfo solnInfoTmp(solver, true);
    solnInfoTmp.printBriefSolutionInfo(solver, lp_name_stub + "All_LGICs");
  }

#ifdef TRACE
  printf("\n## Adding all original standard intersection cuts. ##\n");
#endif
  for (int i = 0; i < chooseSplits.nEffCutGenSets; i++)
    solver.addRow(solnInfo.numCols, &constColIndx[0], &OSIC[i].coeff[0],
        OSIC[i].RHS, solver.getInfinity());
  solver.initialSolve();
  LGICAndOSICBasisCondNumber = compute_condition_number_norm2(&solver);
  if (solver.isProvenOptimal()) {
    LGICAndOSICBound = solver.getObjValue();
  } else {
    LGICAndOSICBound = solver.getInfinity();
  }

  if (WRITE_LP_COMPARE_FILE) {
    solver.writeLp((lp_name_stub + "All_LGICs_OSICs").c_str());
    SolutionInfo solnInfoTmp(solver, true);
    solnInfoTmp.printBriefSolutionInfo(solver,
        lp_name_stub + "All_LGICs_OSICs");
  }

  // Delete OSIC rows added
  vector<int> delRows0((int) OSIC.size());
  for (int i = 0; i < (int) OSIC.size(); i++)
    delRows0[i] = solnInfo.numRows + numLGICsAdded + i;
  solver.deleteRows((int) delRows0.size(), &delRows0[0]);

#ifdef TRACE
  printf("\n## Solving problem with all LGICs and *initial* LSICs. ##\n");
#endif
  for (int cutind = 0; cutind < (int) LSIC.size(); cutind++) {
    for (int currcut = 0; currcut < 1; currcut++) {
      solver.addRow(solnInfo.numCols, &constColIndx[0],
          &LSIC[cutind][currcut].coeff[0], LSIC[cutind][currcut].RHS,
          solver.getInfinity());
    }
  }
  solver.initialSolve();
  LGICAndLSICBasisCondNumber = compute_condition_number_norm2(&solver);
  if (solver.isProvenOptimal()) {
    LGICAndLSICBound = solver.getObjValue();
  } else {
    LGICAndLSICBound = solver.getInfinity();
  }

  if (WRITE_LP_COMPARE_FILE) {
    solver.writeLp((lp_name_stub + "All_LGICs_Init_LSICs").c_str());
    SolutionInfo solnInfoTmp(solver, true);
    solnInfoTmp.printBriefSolutionInfo(solver,
        lp_name_stub + "All_LGICs_Init_LSICs");
  }
}

void compare_LGIC_to_LSIC_and_OSIC_Root_Initial(const char* instName,
    PointCutsSolverInterface* tmpsolver, SolutionInfo &solnInfo,
    chooseCutGeneratingSets &chooseSplits, vector<cut> &OSIC,
    vector<vector<cut> > &LSIC, vector<vector<cut> > &LGIC,
    double &LGICBoundInit, double &LGICBasisCondNumberInit,
    double &LGICAndOSICBoundInit, double &LGICAndOSICBasisCondNumberInit,
    double &LGICAndLSICBoundInit, double &LGICAndLSICBasisCondNumberInit,
    string lp_name_stub, FILE *inst_info_out) {

  PointCutsSolverInterface solver(tmpsolver);
  solver.setHintParam(OsiDoPresolveInInitial, 1);
  solver.setHintParam(OsiDoPresolveInResolve, 1);
  solver.initialSolve();

  vector<int> constColIndx(solnInfo.numCols);
  for (int i = 0; i < solnInfo.numCols; i++)
    constColIndx[i] = i;

  // Add LGICs and evaluate bound and if you get the fractional closure
#ifdef TRACE
  printf(
      "\n## Solving problem with only *initial* lifted generalized intersection cuts added. ##\n");
#endif
  int numLGICsAdded = 0;
  for (int cutind = 0; cutind < (int) LGIC.size(); cutind++) {
    for (int currcut = 0; currcut < 1; currcut++) {
      solver.addRow(solnInfo.numCols, &constColIndx[0],
          &LGIC[cutind][currcut].coeff[0], LGIC[cutind][currcut].RHS,
          solver.getInfinity());
      numLGICsAdded++;
    }
  }
  solver.initialSolve();
  LGICBasisCondNumberInit = compute_condition_number_norm2(&solver);
  if (solver.isProvenOptimal()) {
    LGICBoundInit = solver.getObjValue();
  } else {
    LGICBoundInit = solver.getInfinity();
  }

  if (WRITE_LP_COMPARE_FILE) {
    solver.writeLp((lp_name_stub + "Init_LGICs").c_str());
    SolutionInfo solnInfoTmp(solver, true);
    solnInfoTmp.printBriefSolutionInfo(solver, lp_name_stub + "Init_LGICs");
  }

#ifdef TRACE
  printf("\n## Adding all original standard intersection cuts. ##\n");
#endif
  for (int i = 0; i < chooseSplits.nEffCutGenSets; i++)
    solver.addRow(solnInfo.numCols, &constColIndx[0], &OSIC[i].coeff[0],
        OSIC[i].RHS, solver.getInfinity());
  solver.initialSolve();
  LGICAndOSICBasisCondNumberInit = compute_condition_number_norm2(&solver);
  if (solver.isProvenOptimal()) {
    LGICAndOSICBoundInit = solver.getObjValue();
  } else {
    LGICAndOSICBoundInit = solver.getInfinity();
  }

  if (WRITE_LP_COMPARE_FILE) {
    solver.writeLp((lp_name_stub + "Init_LGICs_OSICs").c_str());
    SolutionInfo solnInfoTmp(solver, true);
    solnInfoTmp.printBriefSolutionInfo(solver,
        lp_name_stub + "Init_LGICs_OSICs");
  }

  // Delete OSIC rows added
  vector<int> delRows0((int) OSIC.size());
  for (int i = 0; i < (int) OSIC.size(); i++)
    delRows0[i] = solnInfo.numRows + numLGICsAdded + i;
  solver.deleteRows((int) delRows0.size(), &delRows0[0]);

#ifdef TRACE
  printf(
      "\n## Solving problem with only *initial* LGICs and *initial* LSICs. ##\n");
#endif
  for (int cutind = 0; cutind < (int) LSIC.size(); cutind++) {
    for (int currcut = 0; currcut < 1; currcut++) {
      solver.addRow(solnInfo.numCols, &constColIndx[0],
          &LSIC[cutind][currcut].coeff[0], LSIC[cutind][currcut].RHS,
          solver.getInfinity());
    }
  }
  solver.initialSolve();
  LGICAndLSICBasisCondNumberInit = compute_condition_number_norm2(&solver);
  if (solver.isProvenOptimal()) {
    LGICAndLSICBoundInit = solver.getObjValue();
  } else {
    LGICAndLSICBoundInit = solver.getInfinity();
  }

  if (WRITE_LP_COMPARE_FILE) {
    solver.writeLp((lp_name_stub + "Init_LGICs_LSICs").c_str());
    SolutionInfo solnInfoTmp(solver, true);
    solnInfoTmp.printBriefSolutionInfo(solver,
        lp_name_stub + "Init_LGICs_LSICs");
  }
}
