/*
 * myStructs.hpp
 *
 *  Created on: Jun 20, 2012
 *      Author: selva
 */

#ifndef MYSTRUCTS_HPP_
#define MYSTRUCTS_HPP_

using namespace std;

struct ptSortInfo {
  unsigned splitIndx;
  unsigned ptIndex;
  double cost;
};

struct ptOrRayInfo {
  //True if it is a points and false otherwise
  bool ptFlag;
  int hplaneIndx; // hplane col index of hplane activated
  int cutRayColIndx; // ray of C1 was cut by hplane
  int hplaneRowIndx; // row of hplane act
  int newRayColIndx; // new nb basis
};

/**
 * Cuts are assumed to be of the form coeff^\T x \ge RHS
 */
struct cut {
  int cutGenVertexIndex; // ?
  int splitVarIndex; // Index of variable on which we take split
  string splitVarName; // name of splitVarIndex
  int splitIndex; // Index of the split in vector of split var indices (feasSplit)
  double RHS;
  vector<double> coeff;
  double compNB_RHS;
  vector<double> compNB_coeff;
  double beta0; // <coeff, \bar x> (< RHS)
  double norm; // norm(coeff)^2
  double viol; // RHS - beta0 (should be non-negative)
  double eucl; // viol / norm
  double obj; // obj after adding the single cut to the original problem
  //How close to the centroid the obj was to generate this cut
  //1 implies the centroid was used
  double th;

  bool feas;
  bool lopsided;
};
#endif /* MYSTRUCTS_HPP_ */
