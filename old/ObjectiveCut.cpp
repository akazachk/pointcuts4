//============================================================================
// Name        : ObjectiveCut.cpp
// Author      : Aleksandr M. Kazachkov
// Version     : 0.2014.03.10
// Copyright   : Your copyright notice
// Description : Generates simple objective cuts
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#include "ObjectiveCut.hpp"

void genAllObjCuts(OsiCuts& cuts, const PointCutsSolverInterface* const solver,
    const SolutionInfo& probData, const bool working_in_subspace) {
  for (int j = 0; j < probData.numFeasSplits; j++) {
    int col = probData.feasSplitVar[j];
    OsiRowCut cut;
    if (genObjCut(cut, solver, probData, col, working_in_subspace))
      cuts.insert(cut);
  }
}

/**
 * Given column col, corresponding to a variable in the fractional core,
 * we optimize over floor and ceiling and use these values to generate
 * a simple objective cut.
 *
 * Details
 * 1. Let
 *      beta0 = min \{ cx : x \in P_0^k \}
 *    and
 *      beta1 = min \{ cx : x \in P_1^k \}.
 * 2. Define hat(x) that takes in an n-dim vector
 *    and returns the (n-1)-dim vector containing
 *    x_1, ..., x_n except x_k.
 * 3. There are no points x \in P_0^k with cx < beta0,
 *    and similarly none in P_1^k with cx < beta1.
 *    Thus, we define the simple objective cut.
 *    We should have
 *      hat(c) hat(x) >= beta0 - c_k floorxk =: delta0
 *    as the trace for P_0^k, and for P_1^k the trace is
 *      hat(c) hat(x) >= beta1 - c_k ceilxk =: delta1.
 *    Thus, we are looking for the cut alpha x >= beta, where
 *    (a) hat(alpha) = hat(c),
 *    (b) alpha_k = beta0 - beta1 + c_k,
 *    (c) beta = beta1 + ceilxk(beta0 - beta1).
 *
 *  The definition of alpha and beta follow from the reasoning that,
 *  on P_0^k, alpha x >= beta should be exactly hat(c) hat(x) >= delta0,
 *  and similarly on P_1^k, alpha x >= beta is hat(c) hat(x) >= delta1.
 *  Thus, we can define hat(alpha) = hat(c). Using P_1^k,
 *    beta = delta1 + alpha_k ceilxk
 *    = beta1 - c_k ceilxk + alpha_k ceilxk.
 *  Going back to P_0^k, we want
 *    beta - alpha_k floorxk = delta0 = beta0 - c_k floorxk.
 *  Thus,
 *    beta1 - c_k ceilxk + alpha_k ceilxk - alpha_k floorxk
 *    = beta1 - c_k ceilxk + alpha_k
 *    = beta0 - c_k floorxk.
 *  This yields alpha_k = beta0 - beta1 + c_k.
 *  Then beta simplifies to beta1 + ceilxk(beta0 - beta1).
 *
 *  @return True if a cut was generated (false if, for ex, in subspace and both sides infeas)
 */
bool genObjCut(OsiRowCut& cut, const PointCutsSolverInterface* const solver,
    const SolutionInfo& probData, int currFracVar,
    const bool working_in_subspace) {
#ifdef TRACE
  printf(
      "\n## Generating objective cut for fractional variable %d (%s). ##\n",
      currFracVar, solver.getColName(currFracVar).c_str());
#endif
  double beta0, beta1;
  bool floorFeas = solveFloor(beta0, solver, currFracVar);
  bool ceilFeas = solveCeil(beta1, solver, currFracVar);
  double xk = solver.getColSolution()[currFracVar];
  double floorxk = floor(xk);
  double ceilxk = ceil(xk);

  // If exactly one side of the split is feasible, simply add the strongest cut available
  // If both infeasible, and we are in the subspace, then it is okay, this could happen.
  // If we are not in the subspace, then this is a certificate of infeasibility of the original problem.
  if (!floorFeas && !ceilFeas) {
    if (!working_in_subspace) {
      error_msg(errstr,
          "Solver is primal infeasible when variable %d with name %s and value %f is fixed to floor %f and when it is fixed to ceil %f.\n",
          currFracVar, solver.getColName(currFracVar).c_str(), xk,
          floorxk, ceilxk);
      writeErrorToII(errstr, LiftGICsParam::inst_info_out);
      exit(1);
    }
    return false;
  } else if (!floorFeas || !ceilFeas) {
    bool fixFloor = floorFeas;
    genOneSidedOsiRowCut(cut, fixFloor, solver, currFracVar);
    return true;
  } else {
    // Otherwise, both sides of the cut are feasible, and we proceed
    // Constant side of the cut
    double beta = beta1 + ceilxk * (beta0 - beta1);

    // Gather necessary elements for the cut
    int numElem = 0;
    std::vector<int> index;
    std::vector<double> elem;
    index.reserve(solver.getNumCols()); // This is the maximum space we would need
    elem.reserve(solver.getNumCols());

    const double* obj = solver.getObjCoefficients();
    for (int j = 0; j < solver.getNumCols(); j++) {
      if (j == currFracVar) {
        numElem++;
        index.push_back(currFracVar);
        elem.push_back(beta0 - beta1 + obj[currFracVar]);
      } else if (!isZero(obj[j])) {
        numElem++;
        index.push_back(j);
        elem.push_back(obj[j]);
      }
    }

//  CoinPackedVector vec(numElem, index, elem);
    // If solver.getInfinity() is < COIN_DBL_MAX we're in trouble, so better to use COIN_DBL_MAX.
//  OsiRowCut cut(beta, solver.getInfinity(), numElem, numElem, index.data(),
//      elem.data());
//  OsiRowCut cut;

    cut.setLb(beta);
    cut.setRow(numElem, index.data(), elem.data());
    return true;

//  (beta, COIN_DBL_MAX, numElem, numElem,
//      index.data(), elem.data());

//  return cut;
  }
}
