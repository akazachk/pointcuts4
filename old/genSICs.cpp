//============================================================================
// Name        : genSICs.cpp
// Author      : akazachk
// Version     : 0.2013.mm.dd
// Copyright   : Your copyright notice
// Description : 
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#include "genSICs.hpp"

/***********************************************************************/
/**
 * @brief Generate a single SIC.
 */
void genSIC(cut &NBSIC, std::vector<double> &distAlongRay, double &absSumCoeffs,
    PointCutsSolverInterface* solver, std::vector<int> &nonBasicOrigVarIndex,
    std::vector<int> &nonBasicSlackVarIndex, int splitVarIndex,
    int splitVarRowIndex) {
  int numcols = solver.getNumCols();
  int numrows = solver.getNumRows();
  int numNBOrig = (int) nonBasicOrigVarIndex.size();
  int numNBSlack = (int) nonBasicSlackVarIndex.size();
  int numNB = numNBOrig + numNBSlack;

  const double* UB = solver.getColUpper();
  const double* LB = solver.getColLower();

  double splitVarVal = solver.getColSolution()[splitVarIndex];

  NBSIC.coeff.resize(numNB);
  NBSIC.compNB_coeff.resize(numNB);
  distAlongRay.resize(numNB);
  NBSIC.splitVarIndex = splitVarIndex;
  NBSIC.splitVarName = solver.getColName(splitVarIndex);
  NBSIC.RHS = 1.0;
  NBSIC.compNB_RHS = 1.0;

  double rayMoveSize;
  std::vector<int> cstat(numcols, 0), rstat(numrows, 0), slackRayIndices(
      numcols, 0);

  std::vector<double> basisCol(numrows), rayMult(numNB);
  const char* rowSense = solver.getRowSense();

  // In case something disabled it before
  solver.enableFactorization();

  solver.getBasisStatus(&cstat[0], &rstat[0]);
  double pi0 = floor(splitVarVal);
  for (int col = 0; col < numNBOrig; col++) {
    int currvar = nonBasicOrigVarIndex[col];

    solver.getBInvACol(currvar, &basisCol[0]);

    //If the nonbasic variable is at its upper bound, flip the sign of the ray
    if (cstat[currvar] == 2) {
//      for (int j = 0; j < numrows; j++)
//        basisCol[j] = -basisCol[j];
      basisCol[splitVarRowIndex] = -basisCol[splitVarRowIndex];
    }

    //The ray component is the -a_{ij}, where a_{ij} is the entry in the tableau
    if (-basisCol[splitVarRowIndex] >= param.getEPS()) {
      rayMoveSize = (pi0 + 1 - splitVarVal)
          / (-basisCol[splitVarRowIndex]);
      NBSIC.coeff[col] = 1.0 / rayMoveSize;
      //printf("RAYMOVESIZE %d: %f.\n", col, rayMoveSize);
      distAlongRay[col] = rayMoveSize;
    } else {
      if (-basisCol[splitVarRowIndex] <= -param.getEPS()) {
        rayMoveSize = (splitVarVal - pi0) / basisCol[splitVarRowIndex];
        NBSIC.coeff[col] = 1.0 / rayMoveSize;
        //printf("RAYMOVESIZE %d: %f.\n", col, rayMoveSize);
        distAlongRay[col] = rayMoveSize;
      } else {
        NBSIC.coeff[col] = 0.0;
        //printf("RAYMOVESIZE %d: +inf.\n", col);
        distAlongRay[col] = solver.getInfinity();
      }

    }
    NBSIC.compNB_coeff[col] = NBSIC.coeff[col];
  }

  for (int var = 0; var < numNBSlack; var++) {
    int currvar = nonBasicSlackVarIndex[var];

//    printf("\n!! Currvar: %d. !!\n", currvar);

    solver.getBInvACol(currvar, &basisCol[0]); // currvar is an index > numcols, so ends up same as getBInvCol(currvar - numcols, &basisCol[0])

    //The ray component is the -a_{ij}, where a_{ij} is the entry in the tableau
    if (rowSense[currvar - numcols] == 'G') {
//      for (int j = 0; j < numrows; j++)
//        basisCol[j] = -basisCol[j]; // This is because Clp stores "slack" vars, which are negative excess for the >= rows
      basisCol[splitVarRowIndex] = -basisCol[splitVarRowIndex];
    }
//    printf("\n!! -basisCol[%d]: %f. param.getEPS(): %f. -basisCol[splitVarRowIndex] >= param.getEPS(): %d. !!\n", splitVarRowIndex,
//        -basisCol[splitVarRowIndex], param.getEPS(), -basisCol[splitVarRowIndex] >= param.getEPS());
    if (-basisCol[splitVarRowIndex] >= param.getEPS()) {
      rayMoveSize = (pi0 + 1 - splitVarVal)
          / (-basisCol[splitVarRowIndex]); //splitVarValue/basisCol[varBasisRowIndex];
      NBSIC.coeff[numNBOrig + var] = 1.0 / rayMoveSize;
      //printf("RAYMOVESIZE %d: %f.\n", numNBOrig + var, rayMoveSize);
      distAlongRay[numNBOrig + var] = rayMoveSize;
    } else {
      if (-basisCol[splitVarRowIndex] <= -param.getEPS()) {
        rayMoveSize = (splitVarVal - pi0) / basisCol[splitVarRowIndex]; //(splitVarValue - 1)/basisCol[varBasisRowIndex];
        NBSIC.coeff[numNBOrig + var] = 1.0 / rayMoveSize;
        //printf("RAYMOVESIZE %d: %f.\n", numNBOrig + var, rayMoveSize);
        distAlongRay[numNBOrig + var] = rayMoveSize;
      } else {
        NBSIC.coeff[numNBOrig + var] = 0.0;
        //printf("RAYMOVESIZE %d: +inf.\n", numNBOrig + var);
        distAlongRay[numNBOrig + var] = solver.getInfinity();
      }

    }
    NBSIC.compNB_coeff[numNBOrig + var] = NBSIC.coeff[numNBOrig + var];
//    printf("\n!! distAlongRay[%d + %d]: %f. !!\n", numNBOrig, var,
//        distAlongRay[numNBOrig + var]);
  }
#ifdef TRACE
  printf("Before complementing.\n");
  printf("\tSplit var: %d\n", NBSIC.splitVarIndex);
  printf("\tSplit var name: %s\n",
      solver.getColName(NBSIC.splitVarIndex).c_str());
  printf("\tRHS: %f\n", NBSIC.RHS);
  for (int coeff = 0; coeff < (int) NBSIC.coeff.size(); coeff++) {
    printf("\tCoeff %d: %f\n", coeff, NBSIC.coeff[coeff]);
  }
  printf("\n");
#endif

  // Account for upper-bounded variables correctly (their ray directions are actually negative)
  // Also lower-bounded variables adjust the RHS
  for (int i = 0; i < (int) nonBasicOrigVarIndex.size(); i++) {

    int currvar = nonBasicOrigVarIndex[i];
    if (cstat[currvar] == 2) {
#ifdef TRACE
      printf(
          "Found variable at UB to complement. Var %d (%s) with curr coeff %f and UB %f. Current rhs: %f.\n",
          currvar, solver.getColName(currvar).c_str(), NBSIC.coeff[i],
          UB[currvar], NBSIC.RHS);
#endif
      NBSIC.RHS = NBSIC.RHS - NBSIC.coeff[i] * UB[currvar];
      NBSIC.coeff[i] = -NBSIC.coeff[i];
    }
    if (cstat[currvar] == 3) {
      NBSIC.RHS += NBSIC.coeff[i] * LB[currvar];
    }
  }

#ifdef TRACE
  printf("After complementing.\n");
  printf("\tSplit var: %d\n", NBSIC.splitVarIndex);
  printf("\tSplit var name: %s\n",
      solver.getColName(NBSIC.splitVarIndex).c_str());
  printf("\tRHS: %f\n", NBSIC.RHS);
  for (int coeff = 0; coeff < (int) NBSIC.coeff.size(); coeff++) {
    printf("\tCoeff %d: %f\n", coeff, NBSIC.coeff[coeff]);
  }
  printf("\n");
#endif

  // Normalize
  double absrhs = std::abs(NBSIC.RHS);
  if (absrhs > param.getEPS()) {
    if (NBSIC.RHS < param.getEPS()) {
      NBSIC.RHS = -1.0;
    } else {
      NBSIC.RHS = 1.0;
    }
    for (int col = 0; col < numNB; col++) {
      NBSIC.coeff[col] = NBSIC.coeff[col] / absrhs;
      absSumCoeffs += NBSIC.coeff[col];
    }
  } else {
    NBSIC.RHS = 0.0;
    for (int col = 0; col < numNB; col++) {
      absSumCoeffs += NBSIC.coeff[col];
    }
  }
}

/***********************************************************************/
/**
 * @brief Generate SICs, except only generate at most numCutGenSets splits.
 */
void genSICs(std::vector<cut> &NBSIC,
    std::vector<std::vector<double> > &distAlongRay, std::vector<cut> &SIC,
    PointCutsSolverInterface* solver, SolutionInfo &solnInfo,
    int numCutGenSets, string &f_stub, const bool subspace_option,
    FILE* inst_info_out) {
  int numrows = solnInfo.numRows;
  int numcols = solnInfo.numCols;
  // Although we will not use all these splits,
  // since some may be lopsided, better to calculate all.
  // Much better to calculate one at a time, decide if it is good,
  // then stop when we have reached our limit... TODO This.
  int numsplits = solnInfo.fractionalCore.size();

//  printf("\nnumrows: %d. numcols: %d.\n", numrows, numcols);

  // We want to sort based on objective coefficient to choose which splits to select
  const double* objCost = solver.getObjCoefficients();
  vector<double> SplitCost(solnInfo.fractionalCore.size());
  vector<int> sortIndex(solnInfo.fractionalCore.size());
  for (int i = 0; i < (int) solnInfo.fractionalCore.size(); i++) {
    SplitCost[i] = objCost[solnInfo.fractionalCore[i]];
    sortIndex[i] = i;
  }
  sort(sortIndex.begin(), sortIndex.end(), index_cmp_dsc<double*>(&SplitCost[0]));

  // Resize NBSICcutinfo to account for all possible splits
  NBSIC.resize(numsplits);
  distAlongRay.resize(numsplits);
  std::vector<double> absSumCoeffs(numsplits);

  // Generate a SIC per each split in the subspace
  for (int split = 0; split < numsplits; split++) {
//    int s = solnInfo.fractionalCore[split];
    int s = solnInfo.fractionalCore[sortIndex[split]];
    int s_row = solnInfo.rowOfVar[s];
    string colname = solver.getColName(s);
#ifdef TRACE
    printf("\n## Generating cut for split %d on variable %s. ##\n", s,
        colname.c_str());
#endif

    genSIC(NBSIC[split], distAlongRay[split], absSumCoeffs[split], solver,
        solnInfo.nonBasicOrigVarIndex, solnInfo.nonBasicSlackVarIndex,
        s, s_row);
  }

  char filename[256];
  char headerText[256];

  // Print cuts in NB space
  snprintf(filename, sizeof(filename) / sizeof(char), "%s-NB_SICs.csv",
      f_stub.c_str());
  snprintf(headerText, sizeof(headerText) / sizeof(char),
      "\n## Printing SICs in non-basic space. ##\n");
#ifdef TRACE
  printf("%s", headerText);
#endif
  writeCutsInNBSpace(headerText, NBSIC, solnInfo, solver, filename,
      inst_info_out);

  // Convert cuts to structural space, and print them
  SIC.resize(numsplits);
  for (int currcut = 0; currcut < (int) NBSIC.size(); currcut++) {
    convertCutFromJSpaceToStructSpace(SIC[currcut],
        solnInfo.nonBasicOrigVarIndex, solnInfo.nonBasicSlackVarIndex,
        solver, NBSIC[currcut]);
  }

  // Print cuts in structural space
  snprintf(filename, sizeof(filename) / sizeof(char), "%s-SICs.csv",
      f_stub.c_str());
  snprintf(headerText, sizeof(headerText) / sizeof(char),
      "\n## Printing SICs in structural space. ##\n");
#ifdef TRACE
  printf("%s", headerText);
#endif
  writeCutsInStructSpace(headerText, SIC, solnInfo, solver, filename,
      inst_info_out);

  int numSICs = (int) SIC.size();

  // Add each SIC, and see whether it helps
#ifdef TRACE
  printf("\n## Solving for root node bound after adding each SIC. ##\n");
#endif
  PointCutsSolverInterface SICLP(solver);
  int* cstat = new int[solnInfo.numCols];
  int* rstat = new int[solnInfo.numRows];
  solver.getBasisStatus(cstat, rstat);
  SICLP.setBasisStatus(cstat, rstat);
  delete[] cstat;
  delete[] rstat;
//  subprob.enableSimplexInterface(true); // This caused problems with non-negative variables getting negative values in the solution.
  if (!SICLP.basisIsAvailable())
    SICLP.resolve();

  solnInfo.splitFeasFlag.resize(numSICs, false);

  // Generate indices 1,...,numcols
  int *indices = new int[numcols];
  for (int i = 0; i < numcols; i++) {
    indices[i] = i;
  }

  for (int currcut = 0; currcut < numSICs; currcut++) {
#ifdef TRACE
    printf("\n## Adding SIC %d. ##\n", currcut);
#endif
    double rhs = SIC[currcut].RHS;

    SICLP.addRow(SIC[currcut].coeff.size(), indices,
        SIC[currcut].coeff.data(), rhs, SICLP.getInfinity());
    SICLP.initialSolve();
    if (!SICLP.isProvenOptimal()) {
      if (!subspace_option) {
        // When we are not in the subspace, this is a critical error; means problem is infeasible.
        error_msg(errorstring,
            "SICLP not proven optimal after adding SIC %d.\n",
            currcut);
        writeErrorToLog(errorstring, inst_info_out);
        exit(1);
      }
#ifdef TRACE
      printf("SICLP not proven optimal after adding SIC %d.\n", currcut);
#endif
      solnInfo.splitFeasFlag[currcut] = false;
      SIC[currcut].obj = solver.getInfinity();
      NBSIC[currcut].obj = solver.getInfinity();

      SIC[currcut].feas = false;
      NBSIC[currcut].feas = false;
    } else {
#ifdef TRACE
      printf("SICLP proven optimal after adding SIC %d.\n", currcut);
#endif
      // If the cut has both sides empty, then we should not consider the cut
      // Depending on YESLOP, may also not consider splits with one side empty
      bool lopBool = false;
      // If we are okay using splits in which both sides are infeasible,
      // then we can consider lopBool = true always.
      if (USE_EMPTY) {
        lopBool = true;
      } else if (YESLOP) {
        lopBool = solnInfo.fracCoreFloorFeasFlag[currcut]
            || solnInfo.fracCoreCeilFeasFlag[currcut];
      } else {
        lopBool = solnInfo.fracCoreFloorFeasFlag[currcut]
            && solnInfo.fracCoreCeilFeasFlag[currcut];
      }
      if (lopBool && ((int) solnInfo.feasSplitVar.size() < numCutGenSets)) {
        // We now know which index it will be in feasSplit
        SIC[currcut].splitIndex = (int) solnInfo.feasSplitVar.size();
        NBSIC[currcut].splitIndex = (int) solnInfo.feasSplitVar.size();

        // And we add it to feasSplit
        solnInfo.feasSplitVar.push_back(SIC[currcut].splitVarIndex);
        solnInfo.splitFeasFlag[currcut] = true;

        SIC[currcut].feas = true;
        NBSIC[currcut].feas = true;

        if (solnInfo.lopsidedFlag[currcut]) {
          SIC[currcut].lopsided = true;
          NBSIC[currcut].lopsided = true;
        } else {
          SIC[currcut].lopsided = false;
          NBSIC[currcut].lopsided = false;
        }
//        printf(
//            "\n!!! Here: SICLP proven optimal and YES added to feasSplit, fracCoreFloorFeas? %d, fracCoreCeilFeas? %d, feasSplit size: %d, numCutGenSets: %d. \n",
//            (int) solnInfo.fracCoreFloorFeas[currcut],
//            (int) solnInfo.fracCoreCeilFeas[currcut],
//            (int) solnInfo.feasSplit.size(), numCutGenSets);

      } else {
//        printf(
//            "\n!!! Here: SICLP proven optimal but NOT added to feasSplit, fracCoreFloorFeas? %d, fracCoreCeilFeas? %d, feasSplit size: %d, numCutGenSets: %d. \n",
//            (int) solnInfo.fracCoreFloorFeas[currcut],
//            (int) solnInfo.fracCoreCeilFeas[currcut],
//            (int) solnInfo.feasSplit.size(), numCutGenSets);
        solnInfo.splitFeasFlag[currcut] = false;

        SIC[currcut].feas = false;
        NBSIC[currcut].feas = false;
      }

      char buffer[300];
      snprintf(buffer, sizeof(buffer) / sizeof(char), "%sSIC%d",
          f_stub.c_str(), currcut);
      //SICLP.writeLp(buffer, "lp");

      SIC[currcut].obj = SICLP.getObjValue();
      NBSIC[currcut].obj = SICLP.getObjValue();
#ifdef TRACE
      printf(
          "LP relaxation optimal value: %f. After cut optimal value: %f.\n",
          solver.getObjValue(), SICLP.getObjValue());
#endif
    }

    // Don't forget to remove the cut
    std::vector<int> rowsToDelete(1);
    rowsToDelete[0] = numrows;
    SICLP.deleteRows(1, rowsToDelete.data());
  }

  // Compute Euclidean distances
#ifdef TRACE
  printf("\n## Computing norms of SICs in the subspace. ##\n");
#endif
  for (int split = 0; split < numSICs; split++) {
    SIC[split].norm = inner_product(SIC[split].coeff.begin(),
        SIC[split].coeff.end(), SIC[split].coeff.begin(), (double) 0);
    SIC[split].beta0 = inner_product(SIC[split].coeff.begin(),
        SIC[split].coeff.end(), solver.getColSolution(), (double) 0);
    SIC[split].viol = SIC[split].RHS - SIC[split].beta0;
    if (SIC[split].norm > -param.getEPS()) { // TODO Alex: Is this "0.0" instead of param.getEPS() going to cause issues?
      SIC[split].eucl = SIC[split].viol / SIC[split].norm;
    } else if (SIC[split].feas) { //if (SIC[cut].norm < -param.getEPS()) {
      char errorstring[256];
      snprintf(errorstring, sizeof(errorstring) / sizeof(char),
          "*** ERROR: Norm of SIC %d is non-positive! This SIC is feasible, and we don't want this. Value is %f.\n",
          split, SIC[split].norm);
      cerr << errorstring << endl;
      writeErrorToLog(errorstring, inst_info_out);
      exit(1);
    }
//    else {
//      SICnorm[cut] = 0.0;
//    }

    if (SIC[split].eucl < -param.getEPS()
        && (solnInfo.fracCoreFloorFeasFlag[split]
            || solnInfo.fracCoreCeilFeasFlag[split])
        && (absSumCoeffs[split] > param.getEPS())) { // Should be cut off by some positive amount
      char errorstring[256];
      snprintf(errorstring, sizeof(errorstring) / sizeof(char),
          "*** ERROR: Eucl distance by which SIC %d cuts off xbar is negative: %e. RHS: %e. SICbeta0[%d]: %e. SICnorm[%d]: %e.\n",
          split, SIC[split].eucl, SIC[split].RHS, split,
          SIC[split].beta0, split, SIC[split].norm);
//      printf("Inner prod 1: %f\n",
//          inner_product(SICcutinfo[cut].begin() + 2, SICcutinfo[cut].end(),
//              SICcutinfo[cut].begin() + 2, (double) 0));
//
//      double inprod;
//      for (int coeff = 2; coeff < (int) SICcutinfo[cut].size(); coeff++) {
//        double curr = SICcutinfo[cut][coeff];
//        if (curr != 0) {
//          printf("Found non-zero coeff %d with value %e.\n", coeff - 2, curr);
//        }
//        inprod += curr * curr;
//      }
//      printf("Inner prod 2: %f\n", inprod);
      cerr << errorstring << endl;
      writeErrorToLog(errorstring, inst_info_out);
      exit(1);
    }
  }

  // Print euclidean distance and objective for each cut
  snprintf(filename, sizeof(filename) / sizeof(char), "%s-SICOverview.csv",
      f_stub.c_str());
  snprintf(headerText, sizeof(headerText) / sizeof(char),
      "\n## Printing sub SICs Euclidean distance and root node bound info. ##\n");
#ifdef TRACE
  printf("%s", headerText);
#endif
  writeCutSummary(headerText, solver, solnInfo, SIC, filename, inst_info_out);

  // Print the distances traveled along each ray by each cut
  snprintf(filename, sizeof(filename) / sizeof(char),
      "%s-SICsNBRayDistances.csv", f_stub.c_str());
  snprintf(headerText, sizeof(headerText) / sizeof(char),
      "\n## Printing SIC distances along each ray. ##\n");
#ifdef TRACE
  printf("%s", headerText);
#endif
  writeRayDistInfo(headerText, solver, solnInfo, SIC, distAlongRay, filename,
      inst_info_out);

  delete[] indices;
}

/***********************************************************************/
/**
 * @brief Generate SICs in original space (well, works for any space),
 * but restrict the set of splits on which we generate to feasSplits.
 * Note that if the split is feasible in the subspace, then it should
 * also be feasible in the full space, since the trace of the orig SIC in the
 * subspace is the sub SIC.
 */
void genSICsOrig(std::vector<cut> &NBSIC,
    std::vector<std::vector<double> > &distAlongRay, std::vector<cut> &SIC,
    PointCutsSolverInterface* solver, SolutionInfo &solnInfo,
    SolutionInfo &solnInfoSub, std::vector<int> &oldColIndices,
    string &f_stub, FILE* inst_info_out) {
  int numrows = solnInfo.numRows;
  int numcols = solnInfo.numCols;
  int numsplits = solnInfoSub.feasSplitVar.size();

//  printf("\nnumrows: %d. numcols: %d.\n", numrows, numcols);

  // Resize NBSICcutinfo to account for all possible splits
  NBSIC.resize(numsplits);
  distAlongRay.resize(numsplits);
  std::vector<double> absSumCoeffs(numsplits);

  // Generate a SIC per each split in the subspace
  for (int split = 0; split < numsplits; split++) {
    // If feasSplitVar[split] = j, this is j in subspace, which is oldColIndices[j] in orig space.
//    printf("Feasible split %d is on subspace var %d.\n", split, solnInfoSub.feasSplitVar[split]);
    int s = oldColIndices[solnInfoSub.feasSplitVar[split]];
    int s_row = solnInfo.rowOfVar[s];
#ifdef TRACE
    printf("\n## Generating orig SIC for split %d on variable %s. ##\n", s,
        solver.getColName(s).c_str());
#endif

    genSIC(NBSIC[split], distAlongRay[split], absSumCoeffs[split], solver,
        solnInfo.nonBasicOrigVarIndex, solnInfo.nonBasicSlackVarIndex,
        s, s_row);
  }

  char filename[256];
  char headerText[256];

  // Print cuts in NB space
  snprintf(filename, sizeof(filename) / sizeof(char), "%s-NB_SICs.csv",
      f_stub.c_str());
  snprintf(headerText, sizeof(headerText) / sizeof(char),
      "\n## Printing orig SICs in non-basic space. ##\n");
#ifdef TRACE
  printf("%s", headerText);
#endif
  writeCutsInNBSpace(headerText, NBSIC, solnInfo, solver, filename,
      inst_info_out);

  // Convert cuts to structural space, and print them
  SIC.resize(numsplits);
  for (int currcut = 0; currcut < (int) NBSIC.size(); currcut++) {
    convertCutFromJSpaceToStructSpace(SIC[currcut],
        solnInfo.nonBasicOrigVarIndex, solnInfo.nonBasicSlackVarIndex,
        solver, NBSIC[currcut]);
  }

  // Print cuts in structural space
  snprintf(filename, sizeof(filename) / sizeof(char), "%s-SICs.csv",
      f_stub.c_str());
  snprintf(headerText, sizeof(headerText) / sizeof(char),
      "\n## Printing orig SICs in structural space. ##\n");
#ifdef TRACE
  printf("%s", headerText);
#endif
  writeCutsInStructSpace(headerText, SIC, solnInfo, solver, filename,
      inst_info_out);

  int numSICs = (int) SIC.size();

  // Add each SIC, and see whether it helps
#ifdef TRACE
  printf("\n## Solving for root node bound after adding each orig SIC. ##\n");
#endif
  PointCutsSolverInterface SICLP(solver);
//  solnInfo.splitFeas.resize(numSICs, false);

  // Generate indices 1,...,numcols
  int *indices = new int[numcols];
  for (int i = 0; i < numcols; i++) {
    indices[i] = i;
  }

  for (int currcut = 0; currcut < numSICs; currcut++) {
#ifdef TRACE
    printf("\n## Adding orig SIC %d. ##\n", currcut);
#endif
    double rhs = SIC[currcut].RHS;

    SICLP.addRow(SIC[currcut].coeff.size(), indices,
        SIC[currcut].coeff.data(), rhs, SICLP.getInfinity());
    SICLP.initialSolve();
    if (!SICLP.isProvenOptimal()) {
//      char errorstring[300];
//      snprintf(errorstring, sizeof(errorstring) / sizeof(char),
//          "*** ERROR: SICLP not proven optimal after adding SIC %d.\n", cut);
//      cerr << errorstring << endl;
//      exit(1);
#ifdef TRACE
      printf("SICLP not proven optimal after adding orig SIC %d.\n",
          currcut);
#endif
//      solnInfo.infeasSplit.push_back(currcut);
//      solnInfo.splitFeas[currcut] = false;
      SIC[currcut].obj = solver.getInfinity();
      NBSIC[currcut].obj = solver.getInfinity();
      SIC[currcut].feas = false;
      NBSIC[currcut].feas = false;
    } else {
#ifdef TRACE
      printf("SICLP proven optimal after adding orig SIC %d.\n", currcut);
#endif
      // We now know which index it will be in feasSplit
      SIC[currcut].splitIndex = (int) solnInfo.feasSplitVar.size();
      NBSIC[currcut].splitIndex = (int) solnInfo.feasSplitVar.size();

      // And we add it to feasSplit
      solnInfo.feasSplitVar.push_back(SIC[currcut].splitVarIndex);
//      solnInfo.splitFeas[currcut] = true;

      SIC[currcut].feas = true;
      NBSIC[currcut].feas = true;

      char buffer[300];
      snprintf(buffer, sizeof(buffer) / sizeof(char), "%sSIC%d",
          f_stub.c_str(), currcut);
      //SICLP.writeLp(buffer, "lp");

      SIC[currcut].obj = SICLP.getObjValue();
#ifdef TRACE
      printf(
          "LP relaxation optimal value: %f. After orig SIC optimal value: %f.\n",
          solver.getObjValue(), SICLP.getObjValue());
#endif
    }

    // Don't forget to remove the cut
    std::vector<int> rowsToDelete(1);
    rowsToDelete[0] = numrows;
    SICLP.deleteRows(1, rowsToDelete.data());
  }

  // Compute Euclidean distances
#ifdef TRACE
  printf("\n## Computing norms of orig SICs in the subspace. ##\n");
#endif

  for (int split = 0; split < numSICs; split++) {
    SIC[split].norm = inner_product(SIC[split].coeff.begin(),
        SIC[split].coeff.end(), SIC[split].coeff.begin(), (double) 0);
    SIC[split].beta0 = inner_product(SIC[split].coeff.begin(),
        SIC[split].coeff.end(), solver.getColSolution(), (double) 0);
    SIC[split].viol = SIC[split].RHS - SIC[split].beta0;
    if (SIC[split].norm > -param.getEPS()) { // TODO Alex: Is this "0.0" instead of param.getEPS() going to cause issues?
      SIC[split].eucl = SIC[split].viol / SIC[split].norm;
    } else { //if (SIC[cut].norm < -param.getEPS()) {
      char errorstring[256];
      snprintf(errorstring, sizeof(errorstring) / sizeof(char),
          "*** ERROR: Norm of orig SIC %d is non-positive! Value is %f.\n",
          split, SIC[split].norm);
      cerr << errorstring << endl;
      writeErrorToLog(errorstring, inst_info_out);
      exit(1);
    }
//    else {
//      SICnorm[cut] = 0.0;
//    }

    if (SIC[split].eucl < -param.getEPS()
        && (solnInfo.fracCoreFloorFeasFlag[split]
            || solnInfo.fracCoreCeilFeasFlag[split])
        && (absSumCoeffs[split] > param.getEPS())) { // Should be cut off by some positive amount
      char errorstring[256];
      snprintf(errorstring, sizeof(errorstring) / sizeof(char),
          "*** ERROR: Eucl distance by which orig SIC %d cuts off xbar is negative: %e. RHS: %e. SICbeta0[%d]: %e. SICnorm[%d]: %e.\n",
          split, SIC[split].eucl, SIC[split].RHS, split,
          SIC[split].beta0, split, SIC[split].norm);
      cerr << errorstring << endl;
      writeErrorToLog(errorstring, inst_info_out);
      exit(1);
    }
  }

  // Print euclidean distance and objective for each cut
  snprintf(filename, sizeof(filename) / sizeof(char), "%s-SICOverview.csv",
      f_stub.c_str());
  snprintf(headerText, sizeof(headerText) / sizeof(char),
      "\n## Printing orig SICs Euclidean distance and root node bound info. ##\n");
#ifdef TRACE
  printf("%s", headerText);
#endif
  writeCutSummary(headerText, solver, solnInfo, SIC, filename, inst_info_out);

  // Print the distances traveled along each ray by each cut
  snprintf(filename, sizeof(filename) / sizeof(char),
      "%s-SICsNBRayDistances.csv", f_stub.c_str());
  snprintf(headerText, sizeof(headerText) / sizeof(char),
      "\n## Printing orig SIC distances along each ray. ##\n");
#ifdef TRACE
  printf("%s", headerText);
#endif
  writeRayDistInfo(headerText, solver, solnInfo, SIC, distAlongRay, filename,
      inst_info_out);
}
