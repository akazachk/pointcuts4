//============================================================================
// Name        : PointCuts4.cpp
// Author      : Aleksandr M. Kazachkov
// Version     : 2018.06.08
// Copyright   : Your copyright notice
// Description : V-polyhedral cuts, generalized intersection cuts via partial hyperplane activation, and tilted cuts
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

//#ifdef _WIN32
//#include <direct.h>
//#define getcwd _getcwd // gives MSFT "deprecation" warning
//#else
//#include <unistd.h>
//#endif
#include <string>
//#include <cmath> // For floor, ceil
//#include <numeric> // For inner_product
//#include <algorithm> // For max_element, min_element
#include <algorithm>  // std::random_shuffle
#include <ctime>
#include <chrono>

#include "optionhelper.hpp"

// COIN-OR
#include "CoinPackedMatrix.hpp"
#include "typedefs.hpp"
#include "CglGomory.hpp"
#include "CglGMI.hpp"
#include "CglLandP.hpp"
#include "OsiCuts.hpp"

// My files
#include "GlobalConstants.hpp"
#include "Utility.hpp"
#include "Output.hpp"

#include "SolutionInfo.hpp"
#include "Vertex.hpp"
#include "Point.hpp"
#include "Ray.hpp"
#include "Hplane.hpp"
#include "CglPHA.hpp"
#include "CglTilted.hpp"
#include "CglSIC.hpp"
#include "CglVPC.hpp"
#include "CutHelper.hpp"
#include "BBHelper.hpp"

// Catch abort signal if it ever gets sent
#include <csignal>

void signal_handler_with_error_msg(int signal_number) {
  /*Your code goes here. You can output debugging info.
   If you return from this function, and it was called
   because abort() was called, your program will exit or crash anyway
   (with a dialog box on Windows).
   */
  error_msg(errorstring, "Abort or seg fault message received. Signal number: %d.\n", signal_number);
  writeErrorToLog(errorstring, GlobalVariables::log_file);
  exit(1);
} /* signal_handler_with_error_msg */

/*************/
/* VARIABLES */
/*************/
std::string instdir;
std::string opt_file;
std::string out_f_name_stubSub, out_f_name_stubSubExpanded, out_f_name_stubOrig,
    out_f_name_stubLifted;
std::string FINAL_LOG_FILE_NAME;
int CLEANING_MODE_OPTION;
std::string compare_cuts_output;
BBInfo best_bb_info_mycuts, best_bb_info_allcuts;
BBInfo avg_bb_info_mycuts, avg_bb_info_allcuts;
std::vector<BBInfo> vec_bb_info_mycuts, vec_bb_info_allcuts;
double strong_branching_lb = std::numeric_limits<double>::max();
double strong_branching_ub = std::numeric_limits<double>::lowest();
int num_obj_tried = 0;

// For timing related names
std::time_t start_time_t, end_time_t;
struct tm* timeinfo;

/**********************************************************/
void introMessage() {
  printf("===\n");
  printf("Point cuts, version 4\n");
  printf("Start time: %s", asctime(timeinfo));
  printf("===\n\n");
  return;
} /* introMessage */

int init(int argc, char** argv, CglGICParam &param); 
int wrapUp(int returncode); 

/**************/
/* PROCEDURES */
/**************/

/********************************************************************************/
/********************************************************************************/
/*******************************  MAIN  *****************************************/
/********************************************************************************/
/********************************************************************************/
/**
 * NOTE:   This is meant to be run on *NIX systems. For any other OS, the code
 *         may need to be modified before it will compile.
 *
 * PROCEDURE:
 * 1. Read in the problem name (this is the original problem).
 * 2. Generate the subspace.
 * 3. For both the original problem and the subspace, figure out which are the
 *     non-basic variables, which are the fractional integer variables.
 *     ** At this step, we can also compute how many of the splits in the subspace
 *        are empty (i.e., how many have 0 / 1 / 2 sides empty, and which ones).
 * 4. For each fractional integer variable in the original space, generate a SIC.
 *     Generate a LandP LP to certify that this cut can be obtained by lifting the
 *     corresponding subspace SIC.
 * 5. Generate a SIC for each fractional integer variable in the subspace. Lift
 *     each of these SICs to be valid in the original space.
 * 6. Compute the root node bound for adding each SIC (individually).
 * 7. Compute the Euclidean distance by which each SIC cuts off the (orig) LP optimum.
 * 8. Directly compare the distances traveled along each ray. Ensure that these
 *     distances are the same for rays that appear in the subspace. Compile statistics
 *     on this that can be used in a frequency histogram.
 */
int main(int argc, char **argv) {
  /*Do this early in your program's initialization */
  std::signal(SIGABRT, signal_handler_with_error_msg);
  std::signal(SIGSEGV, signal_handler_with_error_msg);

  /***********************************************************************************
   * Read inputs (if any) and set directory / file information
   ***********************************************************************************/
  GlobalVariables::start_time = std::chrono::system_clock::now();
  time(&start_time_t);
  timeinfo = localtime(&start_time_t);

  introMessage();
  init(argc, argv, saved_param);
  param = saved_param;

  /***********************************************************************************
   * Initialize solver and initially solve
   ***********************************************************************************/
  GlobalVariables::timeStats.start_timer(GlobalConstants::TOTAL_TIME);
  GlobalVariables::timeStats.start_timer(INIT_SOLVE_TIME);

  PointCutsSolverInterface* solver = new PointCutsSolverInterface;
  setClpParameters(solver);

  if (GlobalVariables::in_file_ext.compare("lp") == 0) {
    // Read LP file
#ifdef TRACE
    printf("\n## Reading LP file. ##\n");
#endif
    solver->readLp(GlobalVariables::in_f_name.c_str());
  } else {
    if (GlobalVariables::in_file_ext.compare("mps") == 0) {
      // Read MPS file
#ifdef TRACE
      printf("\n## Reading MPS file. ##\n");
#endif
      solver->readMps(GlobalVariables::in_f_name.c_str());
    } else {
      error_msg(errorstring, "Unrecognized extension: %s.\n",
          GlobalVariables::in_file_ext.c_str());
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }
  }

  // Make sure we are doing a minimization problem; this is just to make later
  // comparisons simpler (i.e., a higher LP obj after adding the cut is better).
  if (solver->getObjSense() < 1e-3) {
    printf(
        "\n## Detected maximization problem. Negating objective function to make it minimization. ##\n");
    solver->setObjSense(1.0);
    const double* obj = solver->getObjCoefficients();
    for (int col = 0; col < solver->getNumCols(); col++) {
      solver->setObjCoeff(col, -1. * obj[col]);
    }
    double objOffset = 0.;
    solver->getDblParam(OsiDblParam::OsiObjOffset, objOffset);
    if (objOffset != 0.) {
      solver->setDblParam(OsiDblParam::OsiObjOffset, -1. * objOffset);
    }
  }

  // solver->enableFactorization(); // Used for solution info // should be enabled when needed
  ////  solver->enableSimplexInterface(1); // this ruins things somehow, really badly
#ifdef TRACE
  printf("\n## Performing initial solve. ##\n");
#endif

  solver->initialSolve();
  if (!checkSolverOptimality(solver, true)) {
    error_msg(errorstring, "After initial solve, solver is not optimal.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  GlobalVariables::timeStats.end_timer(INIT_SOLVE_TIME);

  if (CLEANING_MODE_OPTION > 0) {
    /***********************************************************************************
     * Clean the instance
     ***********************************************************************************/
    performCleaning(solver, param, CLEANING_MODE_OPTION);

    printf("\n## Finished cleaning. ##\n");

    // Wrap up
    if (solver) {
      delete solver;
    }
    return wrapUp(0);
  } /* cleaning mode */

  // Check the size of the instance is not outside our capabilities
  if (solver->getNumRows() >= 10000 || solver->getNumCols() >= 10000) {
    error_msg(errorstring,
        "Currently I think we may hang on instances with over 10000 rows or columns; this instance has %d rows and %d columns. Comment out the exit here at your own risk.\n",
        solver->getNumRows(), solver->getNumCols());
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

#ifdef TRACE
  printf("Saving LP relaxation solution information.\n");
#endif
  // If PHA, we will be using the ray info, so we should not use the brief SolutionInfo
  SolutionInfo solnInfoOrig(solver,
      param.paramVal[ParamIndices::MAX_FRAC_VAR_PARAM_IND],
      param.paramVal[ParamIndices::SUBSPACE_PARAM_IND],
      (param.getParamVal(ParamIndices::PHA_PARAM_IND) == 0));

#ifdef SHOULD_WRITE_BRIEF_SOLN
  solnInfoOrig.printBriefSolutionInfo(solver, GlobalVariables::out_f_name_stub);
#endif

#ifdef SHOULD_WRITE_SOLN
  writeSoln(solver, GlobalVariables::out_f_name_stub);

  char OptTabName[300];
  snprintf(OptTabName, sizeof(OptTabName) / sizeof(char), "%s-Opt_Tableau.csv",
      out_f_name_stubOrig.c_str());
  FILE* OptTabOut = fopen(OptTabName, "w");
  if (OptTabOut != NULL)
    printSimplexTableauWithNames(OptTabOut, solver);
  else {
    error_msg(errorstring,
        "Could not open file %s to write optimal simplex tableau.\n",
        OptTabName);
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }
  fclose(OptTabOut);

  char NBTabName[300];
  snprintf(NBTabName, sizeof(NBTabName) / sizeof(char), "%s-NB_Opt_Tableau.csv",
      out_f_name_stubOrig.c_str());
  FILE* NBTabOut = fopen(NBTabName, "w");
  if (NBTabOut != NULL)
    printNBSimplexTableauWithNames(NBTabOut, solver);
  else {
    error_msg(errorstring,
        "Could not open file %s to write NB optimal simplex tableau.\n",
        NBTabName);
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }
  fclose(NBTabOut);
#endif

  /***********************************************************************************
   * Finalize parameters
   ***********************************************************************************/
#ifdef TRACE
  printf("\n## Finalizing parameters. ##\n");
#endif

  if (solnInfoOrig.numSplits == 0) {
    error_msg(errorstring, "solnInfoOrig.numSplits: %d. Should not be zero.\n",
        solnInfoOrig.numSplits);
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  if (param.paramVal[ParamIndices::SUBSPACE_PARAM_IND]) {
#ifdef TRACE
    printf("Num0: %d. Num1: %d. Num2: %d.\n", solnInfoOrig.num0feas,
        solnInfoOrig.num1feas, solnInfoOrig.num2feas);
    printf("Num floor infeas: %d. Num ceil infeas: %d.\n",
        solnInfoOrig.numFloorInfeas, solnInfoOrig.numCeilInfeas);
#endif

    // If all the splits have zero feasible sides, then there is no point of continuing on
    if (solnInfoOrig.numSplits == solnInfoOrig.num0feas) {
      error_msg(errorstring, "All possible splits are empty on both sides.\n");
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }
  }

  if (!param.finalizeParams(solnInfoOrig.numNB, solnInfoOrig.numSplits)) {
    error_msg(errstr,
        "Issue finalizing parameters (no rays to cut or issue with parameter combinations).\n");
    writeErrorToLog(errstr, GlobalVariables::log_file);
    exit(1);
  }

  AdvCuts structSIC(false), structGMI(false), structLandP(false),
      structTilted(false), structVPC(false), structPHA(false);
//  AdvCuts structSICStrengthened(false);
  AdvCuts NBSICs(true);

  if (param.getParamVal(ParamIndices::SICS_PARAM_IND)) {
    /***********************************************************************************
     * Generate SICs
     ***********************************************************************************/
#ifdef TRACE
    printf(
        "\n## Generating SICs for the main problem (subproblem if working in subspace). ##\n");
#endif

    GlobalVariables::timeStats.start_timer(GEN_MSICS_TIME);
    // Only feasible ones are returned
    CglSIC SICGen(param);
    SICGen.generateCuts(*solver, NBSICs, structSIC, solnInfoOrig);
    GlobalVariables::numCutsFromHeur[CutHeuristics::SIC_CUT_GEN] =
        structSIC.sizeCuts();
    GlobalVariables::timeStats.end_timer(GEN_MSICS_TIME);

#ifdef TRACE
    printf("***** SICs generated: %d.\n", structSIC.sizeCuts());
#endif

#ifdef SHOULD_WRITE_SICS
    if (param.getParamVal(ParamIndices::SUBSPACE_PARAM_IND)) {
      NBSICs.printCuts("SSICs", solver, solnInfoOrig);
      structSIC.printCuts("SSICs", solver, solnInfoOrig);
    } else {
      NBSICs.printCuts("SICs", solver, solnInfoOrig);
      structSIC.printCuts("SICs", solver, solnInfoOrig);
    }
#endif

    if (structSIC.sizeCuts() == 0) {
      error_msg(errorstring, "Number of splits is 0.\n");
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }
  } /* generate SICs? */

  /***********************************************************************************
   * Check parameters
   ***********************************************************************************/
  if (solnInfoOrig.numFeasSplits == 0) {
    error_msg(errorstring, "Number of *feasible* subspace splits is 0.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }
  if (!param.getParamVal(ParamIndices::SUBSPACE_PARAM_IND)
      && param.getParamVal(ParamIndices::MAX_FRAC_VAR_PARAM_IND)
          != solnInfoOrig.numFeasSplits) {
    error_msg(errorstring,
        "Number of *feasible* subspace splits %d is not equal to the number of requested splits %d, "
        "and this is not in subspace, so should not happen unless problem is infeasible.\n",
        solnInfoOrig.numFeasSplits,
        param.getParamVal(ParamIndices::MAX_FRAC_VAR_PARAM_IND));
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }
  if (solnInfoOrig.numFeasSplits
      < param.getParamVal(ParamIndices::MAX_FRAC_VAR_PARAM_IND)) {
    warning_msg(errorstring,
        "Number of *feasible* subspace splits is %d, which is less than the requested %d. "
        "Reducing the parameter for maximum number of splits.\n",
        solnInfoOrig.numFeasSplits,
        param.getParamVal(ParamIndices::MAX_FRAC_VAR_PARAM_IND));
    param.setParamVal(ParamIndices::MAX_FRAC_VAR_PARAM_IND,
        solnInfoOrig.numFeasSplits);
  }

  const int max_num_cgs = param.getMaxNumCgs(solnInfoOrig.numFeasSplits,
      param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND));
//      (param.getParamVal(CglGICParamNamespace::ParamIndices::CGS_PARAM_IND) == 0) ?
//          solnInfoOrig.numFeasSplits :
//          solnInfoOrig.numFeasSplits * (solnInfoOrig.numFeasSplits - 1) / 2;
  if (!param.getParamVal(ParamIndices::SUBSPACE_PARAM_IND)
      && param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND) != max_num_cgs) {
    error_msg(errorstring,
        "Number of *feasible* cut-generating sets %d is not equal to the number of "
        "cut-generating sets %d, and this is not in subspace, "
        "so should not happen unless problem is infeasible.\n",
        param.paramVal[ParamIndices::NUM_CGS_PARAM_IND], max_num_cgs);
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }
  if (param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND) > max_num_cgs) {
    warning_msg(warnstring,
        "Number of requested cut generating sets is %d, but the maximum number we "
        "can choose is %d. Reducing the requested number of cgs.\n",
        param.paramVal[ParamIndices::NUM_CGS_PARAM_IND], max_num_cgs);
    param.setParamVal(ParamIndices::NUM_CGS_PARAM_IND, max_num_cgs);
  }

  // We moved the SolnInfo and SubSolnInfo here because params are only set now.
  writeParamInfoToLog(GlobalVariables::log_file);
  writeSolnInfoToLog(solnInfoOrig, GlobalVariables::log_file);
  if (param.getParamVal(ParamIndices::SUBSPACE_PARAM_IND)) {
    writeSubSolnInfoToLog(&solnInfoOrig, GlobalVariables::log_file);
  }

  // Count intersecting rays for the chosen splits
  solnInfoOrig.countIntersectingRays(solnInfoOrig.feasSplitVar);

#ifdef SHOULD_WRITE_SPLIT_FEAS_INFO
  printSplitInfo(solver, solnInfoOrig);
#endif

  if (param.getParamVal(ParamIndices::GMI_PARAM_IND)) {
    /***********************************************************************************
     * Generate GMI cuts
     ***********************************************************************************/
#ifdef TRACE
    printf(
        "\n## Generating GMICs. ##\n");
#endif
    GlobalVariables::timeStats.start_timer(GEN_GMI_TIME);
    CglGMI GMIGen(param);
#ifdef TRACE
#define GMI_TRACE_CLEAN
#endif
    GMIGen.generateCuts(*solver, structGMI);
    GlobalVariables::numCutsFromHeur[CutHeuristics::GMI_CUT_GEN] =
        structGMI.sizeCuts();
    GlobalVariables::timeStats.end_timer(GEN_GMI_TIME);

#ifdef SHOULD_WRITE_GMI
    if (param.paramVal[ParamIndices::SUBSPACE_PARAM_IND]) {
      structGMI.printCuts("SGMI", solver, false);
    } else {
      structGMI.printCuts("OGMI", solver, false);
    }
#endif
  } /* generate GMICs? */

  if (param.getParamVal(ParamIndices::LANDP_PARAM_IND)) {
    /***********************************************************************************
     * Generate lift-and-project cuts
     ***********************************************************************************/
    GlobalVariables::timeStats.start_timer(GEN_LANDP_TIME);
    CglLandP LandPGen;
    LandPGen.parameter().modularize = 0;
    LandPGen.parameter().strengthen = (param.getParamVal(ParamIndices::STRENGTHEN_PARAM_IND) > 0);
    LandPGen.parameter().maxCutPerRound = 10000;
//    LandPGen.parameter().generateExtraCuts =
//        CglLandP::ExtraCutsMode::AllViolatedMigs; // generate many more cuts // DOES NOT WORK
    LandPGen.generateCuts(*solver, structLandP);
    GlobalVariables::numCutsFromHeur[CutHeuristics::LANDP_CUT_GEN] =
        structLandP.sizeCuts();
    GlobalVariables::timeStats.end_timer(GEN_LANDP_TIME);
  } /* generate L&PCs? */

  if (param.getParamVal(ParamIndices::TILTED_DEPTH_PARAM_IND) >= 0) {
    /***********************************************************************************
     * Generate tilted cuts
     ***********************************************************************************/
    GlobalVariables::timeStats.start_timer(GEN_TILTED_TIME);
    CglTilted TiltedGen(param);
    TiltedGen.generateCuts(*solver, structTilted, solnInfoOrig);
    for (int round_ind = 2;
        round_ind <= param.getParamVal(ParamIndices::ROUNDS_PARAM_IND);
        round_ind++) {
      // Set up current solver
      PointCutsSolverInterface* tmpSolver = dynamic_cast<PointCutsSolverInterface*>(solver->clone());
      tmpSolver->applyCuts(structTilted);
      tmpSolver->resolve();

      // Generate new cuts
      AdvCuts currCuts;
      CglGICParam tmpParam(saved_param);
      CglTilted CurrTiltedGen(tmpParam);
      CurrTiltedGen.generateCuts(*tmpSolver, currCuts);

      // Add new cuts to the total collection
      for (int i = 0; i < (int) currCuts.size(); i++) {
        structTilted.addNonDuplicateCut(currCuts.cuts[i], false);
      }
      structTilted.setOsiCuts();

      if (tmpSolver) {
        delete tmpSolver;
      }

      // Check if we should stop early
      if (currCuts.size() == 0) {
        break;
      }
    } /* rounds of cuts */
    GlobalVariables::numCutsFromHeur[CutHeuristics::TILTED_GEN] =
        structTilted.sizeCuts();
    GlobalVariables::timeStats.end_timer(GEN_TILTED_TIME);

#ifdef SHOULD_WRITE_TILTED
    if (param.paramVal[ParamIndices::SUBSPACE_PARAM_IND]) {
      structTilted.printCuts("STiltedCuts", solver, true);
    } else {
      structTilted.printCuts("OTiltedCuts", solver, true);
    }
#endif
  } /* generate tilted cuts? */

  if ((param.getParamVal(ParamIndices::VPC_DEPTH_PARAM_IND) >= 1)
      || (param.getParamVal(ParamIndices::VPC_DEPTH_PARAM_IND) <= -2)) {
    /***********************************************************************************
     * Generate V-polyhedral cuts
     ***********************************************************************************/
    GlobalVariables::timeStats.start_timer(GEN_VPC_TIME);
    CglVPC VPCGen(param);
    VPCGen.generateCuts(*solver, structVPC, solnInfoOrig, structSIC);
    GlobalVariables::timeStats.add_value(GEN_TREE_FOR_VPC_TIME, VPCGen.getTimer().get_value("GEN_TREE_FOR_VPC"));
    num_obj_tried += VPCGen.getNumObjTried();
    strong_branching_lb = VPCGen.getStrongBranchingLB();
    strong_branching_ub = VPCGen.getStrongBranchingUB();
    for (int round_ind = 2;
        round_ind <= param.getParamVal(ParamIndices::ROUNDS_PARAM_IND);
        round_ind++) {
      // Set up current solver
      PointCutsSolverInterface* tmpSolver = dynamic_cast<PointCutsSolverInterface*>(solver->clone());
      tmpSolver->applyCuts(structVPC);
      tmpSolver->resolve();

      // Generate new cuts but not with new SICs
      const int savedSICParamVal = saved_param.getParamVal(ParamIndices::SICS_PARAM_IND);
      saved_param.setParamVal(ParamIndices::SICS_PARAM_IND, 0);
      AdvCuts currCuts;
      CglGICParam tmpParam(saved_param);
      CglVPC CurrVPCGen(tmpParam);
      CurrVPCGen.generateCuts(*tmpSolver, currCuts);
      num_obj_tried += CurrVPCGen.getNumObjTried();
      strong_branching_lb = CurrVPCGen.getStrongBranchingLB();
      strong_branching_ub = CurrVPCGen.getStrongBranchingUB();
      saved_param.setParamVal(ParamIndices::SICS_PARAM_IND, savedSICParamVal);

      // Add new cuts to the total collection
      for (int i = 0; i < (int) currCuts.size(); i++) {
        structVPC.addNonDuplicateCut(currCuts.cuts[i], false);
      }
      structVPC.setOsiCuts();

      if (tmpSolver) {
        delete tmpSolver;
      }

      // Check if we should stop early
      if (currCuts.size() == 0) {
        break;
      }
    } /* rounds of cuts */
    GlobalVariables::timeStats.end_timer(GEN_VPC_TIME);
  } /* generate VPCs? */

  if (param.getParamVal(ParamIndices::PHA_PARAM_IND)) {
    /***********************************************************************************
     * Generate GICs
     ***********************************************************************************/
    GlobalVariables::timeStats.start_timer(GEN_PHA_TIME);
    CglPHA GICGen(param);
    GICGen.generateCuts(*solver, structPHA, solnInfoOrig, structSIC, NBSICs);
    num_obj_tried += GICGen.getNumObjTried();
    for (int round_ind = 2;
        round_ind <= param.getParamVal(ParamIndices::ROUNDS_PARAM_IND);
        round_ind++) {
      // Set up current solver
      PointCutsSolverInterface* tmpSolver = dynamic_cast<PointCutsSolverInterface*>(solver->clone());
      tmpSolver->applyCuts(structPHA);
      tmpSolver->resolve();

      // Generate new cuts but not with new SICs
      const int savedSICParamVal = saved_param.getParamVal(ParamIndices::SICS_PARAM_IND);
      saved_param.setParamVal(ParamIndices::SICS_PARAM_IND, 0);
      AdvCuts currCuts;
      CglGICParam tmpParam(saved_param);
      CglPHA CurrPHAGen(tmpParam);
      CurrPHAGen.generateCuts(*tmpSolver, currCuts);
      num_obj_tried += CurrPHAGen.getNumObjTried();
      saved_param.setParamVal(ParamIndices::SICS_PARAM_IND, savedSICParamVal);

      // Add new cuts to the total collection
      for (int i = 0; i < (int) currCuts.size(); i++) {
        structPHA.addNonDuplicateCut(currCuts.cuts[i], false);
      }
      structPHA.setOsiCuts();

      if (tmpSolver) {
        delete tmpSolver;
      }

      // Check if we should stop early
      if (currCuts.size() == 0) {
        break;
      }
    } /* rounds of cuts */
    GlobalVariables::timeStats.end_timer(GEN_PHA_TIME);
  } /* generate GICs? */

  /***********************************************************************************
   * Compare cuts
   ***********************************************************************************/
  printf("\n## Finished cut generation.");
  if (structSIC.sizeCuts() > 0) {
    printf("  SIC: %d.", structSIC.sizeCuts());
  }
  if (structGMI.sizeCuts() > 0) {
    printf("  GMI: %d.", structGMI.sizeCuts());
  }
  if (structLandP.sizeCuts() > 0) {
    printf("  LandP: %d.", structLandP.sizeCuts());
  }
  if (structTilted.sizeCuts() > 0) {
    printf("  Tilted: %d.", structTilted.sizeCuts());
  }
  if (structVPC.sizeCuts() > 0) {
    printf("  VPC: %d.", structVPC.sizeCuts());
  }
  if (structPHA.sizeCuts() > 0) {
    printf("  PHA: %d.", structPHA.sizeCuts());
  }
  printf(" ##\n");
//  std::vector<double> boundbyRoundTilted, boundByRoundVPC, boundByRoundPHA;
  const int numCutsToAddPerRound =
      (structSIC.sizeCuts() > 0) ? structSIC.sizeCuts() : max_num_cgs;
  compareCuts(num_obj_tried, solver, structSIC, structGMI, structLandP,
      structTilted, structVPC, structPHA,
      strong_branching_lb, strong_branching_ub,
      numCutsToAddPerRound, compare_cuts_output);

//  for (int round_ind = 0; round_ind < (int) boundByRoundVPC.size();
//      round_ind++) {
//    std::cout << "VPC round " << round_ind << ": " << boundByRoundVPC[round_ind]
//        << std::endl;
//  }
//  for (int round_ind = 0; round_ind < (int) boundByRoundPHA.size();
//      round_ind++) {
//    std::cout << "PHA round " << round_ind << ": " << boundByRoundPHA[round_ind]
//        << std::endl;
//  }

  /***********************************************************************************
   * Perform branch-and-bound
   ***********************************************************************************/
  const int num_bb_runs = std::abs(param.getParamVal(ParamIndices::BB_RUNS_PARAM_IND)) 
      * ((param.getParamVal(ParamIndices::VPC_DEPTH_PARAM_IND) == 0) || structVPC.sizeCuts() > 0);
  const bool should_test_bb_with_sics = param.getParamVal(ParamIndices::BB_RUNS_PARAM_IND) > 0;
  const bool should_permute_rows_and_cols = (num_bb_runs >= 2)
      && !(param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND) & BB_Strategy_Options::gurobi)
      && !(param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND) & BB_Strategy_Options::cplex);
  if (num_bb_runs >= 1) {
    GlobalVariables::timeStats.start_timer(BB_TIME);
#ifdef TRACE
    printf("\n## Performing branch-and-bound tests. ##\n");
#endif

    vec_bb_info_mycuts.resize(num_bb_runs);
    vec_bb_info_allcuts.resize(num_bb_runs);
    std::vector<int> row_permutation, col_permutation;
    if (should_permute_rows_and_cols) {
      row_permutation.resize(solver->getNumRows());
      col_permutation.resize(solver->getNumRows());
      for (int row = 0; row < solver->getNumRows(); row++) {
        row_permutation[row] = row;
      }
      for (int col = 0; col < solver->getNumCols(); col++) {
        col_permutation[col] = col;
      }
    }

    AdvCuts SICsAndVPCs = structVPC;
    if (should_test_bb_with_sics) {
      for (int i = 0; i < (int) structSIC.size(); i++) {
        SICsAndVPCs.addNonDuplicateCut(structSIC.cuts[i], false);
      }
      SICsAndVPCs.setOsiCuts();
    }

    PointCutsSolverInterface* runSolver = NULL;
    int initial_random_seed = GlobalVariables::random_seed;
    for (int run_ind = 0; run_ind < num_bb_runs; run_ind++) {
      // For Cbc, for every run after the first, we will randomize the rows and columns of the input
      if (should_permute_rows_and_cols) {
        std::srand(run_ind + 2);  // sets the seed for pseudo-randomness in C++
        std::vector<int> this_row_permutation(row_permutation);
        std::vector<int> this_col_permutation(col_permutation);
        std::random_shuffle(this_row_permutation.begin(),
            this_row_permutation.end());
        std::random_shuffle(this_col_permutation.begin(),
            this_col_permutation.end());

        // Create the permuted problem
        std::vector<double> rowLB(solver->getNumRows()), rowUB(
            solver->getNumRows());
        std::vector<double> colLB(solver->getNumCols()), colUB(
            solver->getNumCols());
        std::vector<double> obj(solver->getNumCols());
        const CoinPackedMatrix* mat = solver->getMatrixByRow();

        // Set the rows
        CoinPackedMatrix row_mat;
        row_mat.reverseOrdering();  // make it row-ordered
        row_mat.setMinorDim(solver->getNumCols());
        for (int row = 0; row < solver->getNumRows(); row++) {
          const int curr_row = this_row_permutation[row];
          row_mat.appendRow(mat->getVector(curr_row));
          rowLB[row] = solver->getRowLower()[curr_row];
          rowUB[row] = solver->getRowUpper()[curr_row];
          printf("New row: %d. Orig row: %d. Row upper: %s. Row lower: %s.\n",
              row, curr_row, stringValue(rowLB[row]).c_str(),
              stringValue(rowUB[row]).c_str());
        }
        row_mat.reverseOrdering();  // make it col-ordered for use in the next part

        // Set the cols
        CoinPackedMatrix col_mat;
        col_mat.setMinorDim(solver->getNumRows());
        for (int col = 0; col < solver->getNumCols(); col++) {
          const int curr_col = this_col_permutation[col];
          col_mat.appendCol(row_mat.getVector(curr_col));
          colLB[col] = solver->getColLower()[curr_col];
          colUB[col] = solver->getColUpper()[curr_col];
          obj[col] = solver->getObjCoefficients()[curr_col];
        }

        runSolver = new PointCutsSolverInterface;
        runSolver->setObjSense(solver->getObjSense());
        double objOffset = 0.;
        solver->getDblParam(OsiDblParam::OsiObjOffset, objOffset);
        runSolver->setDblParam(OsiDblParam::OsiObjOffset, objOffset);

        runSolver->loadProblem(col_mat, colLB.data(), colUB.data(), obj.data(),
            rowLB.data(), rowUB.data());

        // Set integers
        for (int col = 0; col < solver->getNumCols(); col++) {
          const int curr_col = this_col_permutation[col];
          if (solver->isInteger(curr_col)) {
            runSolver->setInteger(curr_col);
          }
        }
      } /* test whether num_bb_runs >= 2 and permute row/col order */
      else {
        runSolver = solver;
      }

      // Change the random seed per run
      GlobalVariables::random_seed = initial_random_seed * (run_ind + 1); // or is it better to just get a good sense of how it runs for this random seed?

      // Do branch and bound
      if (param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND) & BB_Strategy_Options::gurobi) {
#ifdef VPC_USE_GUROBI
        if (param.getTEMP() == 1) {
          // Get the original solution
          BBInfo tmp_bb_info;
          std::vector<double> solution;
          doBranchAndBoundWithGurobi(GlobalVariables::in_f_name.c_str(),
              tmp_bb_info, &solution);
          
          // Check cuts
          for (int cut_ind = 0; cut_ind < structVPC.sizeCuts(); cut_ind++) {
            const double rhs = structVPC.cuts[cut_ind].rhs();
            const int num_el = structVPC.cuts[cut_ind].row().getNumElements();
            const int* ind = structVPC.cuts[cut_ind].row().getIndices();
            const double* el = structVPC.cuts[cut_ind].row().getElements();
            const double activity = dotProduct(num_el, ind, el, solution.data());

            if (lessThanVal(activity, rhs)) {
              warning_msg(warnstring, "Cut %d removes optimal solution. Activity: %.10f. Rhs: %.10f.\n", cut_ind, activity, rhs);
            }
          }
        } // checking cuts for violating the IP opt 
        doBranchAndBoundWithUserCutsGurobi(GlobalVariables::in_f_name.c_str(),
            &structVPC, vec_bb_info_mycuts[run_ind]);
        if (should_test_bb_with_sics) {
          doBranchAndBoundWithUserCutsGurobi(GlobalVariables::in_f_name.c_str(),
              &SICsAndVPCs, vec_bb_info_allcuts[run_ind]);
        }
#endif
      } else if (param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND) & BB_Strategy_Options::cplex) {
#ifdef VPC_USE_CPLEX
        doBranchAndBoundWithUserCutsCplexCallable(
            GlobalVariables::in_f_name.c_str(), &structVPC, vec_bb_info_mycuts[run_ind]);
        doBranchAndBoundWithUserCutsCplexCallable(
            GlobalVariables::in_f_name.c_str(), &SICsAndVPCs,
            vec_bb_info_allcuts[run_ind]);
#endif
      } else if (param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND) & BB_Strategy_Options::cbc) {
        doBranchAndBoundYesCuts(runSolver, vec_bb_info_mycuts[run_ind],
            structVPC, false, numCutsToAddPerRound, 1, "\nBB with VPCs.\n");
        if (should_test_bb_with_sics) {
          doBranchAndBoundYesCuts(runSolver, vec_bb_info_allcuts[run_ind],
              SICsAndVPCs, false, numCutsToAddPerRound, 1,
              "\nBB with VPC+Gomorys.\n");
        }
      }

//      doBranchAndBoundNoCuts(runSolver, this_bb_nodes_nocuts, this_bb_time_nocuts);
//      doBranchAndBoundYesCuts(runSolver, this_bb_nodes_sics, this_bb_time_sics, structSIC,
//          false, numCutsToAddPerRound, 1, "\nBB with SICs.\n");
//      doBranchAndBoundYesCuts(runSolver, this_bb_nodes_vpcs, this_bb_time_vpcs, structVPC,
//          false, numCutsToAddPerRound, 1, "\nBB with VPCs.\n");
//      doBranchAndBoundYesCuts(runSolver, this_bb_nodes_sics_and_vpcs,
//          this_bb_time_sics_and_vpcs, SICsAndVPCs, false, numCutsToAddPerRound, 1,
//          "\nBB with SICs+VPCs.\n");

      updateBestBBInfo(best_bb_info_mycuts, vec_bb_info_mycuts[run_ind], (run_ind == 0));
      updateBestBBInfo(best_bb_info_allcuts, vec_bb_info_allcuts[run_ind], (run_ind == 0));

      // Free memory if necessary
      if (should_permute_rows_and_cols && runSolver && (runSolver != solver)) {
        delete runSolver;
      }
    } /* end iterating over runs */
    averageBBInfo(avg_bb_info_mycuts, vec_bb_info_mycuts);
    averageBBInfo(avg_bb_info_allcuts, vec_bb_info_allcuts);
    GlobalVariables::timeStats.end_timer(BB_TIME);
  } /* end branch-and-bound tests */

//  if (num_bb_runs >= 1) {
//    GlobalVariables::timeStats.start_timer(BB_TIME);
//#ifdef TRACE
//    printf("\n## Performing branch-and-bound tests. ##\n");
//#endif
//    doBranchAndBoundNoCuts(solver, bb_nodes_nocuts, bb_time_nocuts);
//    doBranchAndBoundYesCuts(solver, bb_nodes_sics, bb_time_sics, structSIC,
//        false, numCutsToAddPerRound, 1, "\nBB with SICs.\n");
//    doBranchAndBoundYesCuts(solver, bb_nodes_vpcs, bb_time_vpcs, structVPC,
//        false, numCutsToAddPerRound, 1, "\nBB with VPCs.\n");
//    AdvCuts SICsAndVPCs = structVPC;
//    for (int i = 0; i < (int) structSIC.size(); i++) {
//      SICsAndVPCs.addNonDuplicateCut(structSIC.cuts[i], false);
//    }
//    SICsAndVPCs.setOsiCuts();
//    doBranchAndBoundYesCuts(solver, bb_nodes_sics_and_vpcs, bb_time_sics_and_vpcs, SICsAndVPCs,
//        false, numCutsToAddPerRound, 1, "\nBB with SICs+VPCs.\n");
////    addAllCutsToBranchAndBound(solver, structSIC, structGMI, structLandP, structTilted,
////        structVPC, structPHA, bb_nodes_nocuts, bb_time_nocuts, bb_nodes_sics,
////        bb_time_sics, bb_nodes_allcuts, bb_time_allcuts);
//    GlobalVariables::timeStats.end_timer(BB_TIME);
//  }
  writeBBInforToLog(
      best_bb_info_mycuts, best_bb_info_allcuts,
      avg_bb_info_mycuts, avg_bb_info_allcuts,
      vec_bb_info_mycuts, vec_bb_info_allcuts,
      GlobalVariables::log_file);

  /***********************************************************************************
   * Wrap up
   ***********************************************************************************/
  if (solver) {
    delete solver;
  }
  return wrapUp(0);
} /* main */

/**********************************************************
 * Parse arguments and set up parameters
 *********************************************************/
int init(int argc, char** argv, CglGICParam &param) {
  // These will prove useful
  std::string tmppath, tmpname, tmpoutdir;

  processArgs(argc, argv, param, tmppath, tmpname, tmpoutdir, opt_file, instdir,
      CLEANING_MODE_OPTION);

  // FINISHED READING INPUT
  // NOW WE PROCESS IT
  if (CLEANING_MODE_OPTION > 0) {
    if (GlobalVariables::LOG_FILE_NAME.empty()) {
      GlobalVariables::LOG_FILE_NAME = "cleaning_log";
    }
    param.setParamVal(ParamIndices::MAX_FRAC_VAR_PARAM_IND, 0);
  }

  GlobalVariables::initialize();

  // Check for correctness in the next_hplane_final parameter
  if (!param.getParamVal(ParamIndices::NEXT_HPLANE_FINAL_PARAM_IND)
      && (std::abs(
          param.getParamVal(ParamIndices::NUM_EXTRA_ACT_ROUNDS_PARAM_IND)) >= 2)) {
    error_msg(errstr,
        "Number of hyperplanes activated per ray needs to be at most 1 "
        "if being chosen not by most number of final points. Provided is: %d.\n",
        param.getParamVal(ParamIndices::NUM_EXTRA_ACT_ROUNDS_PARAM_IND));
    exit(1);
  }

  // Check that if using PHA, then cgs == 0
  if ((param.getParamVal(ParamIndices::CGS_PARAM_IND) > 0)
      && (param.getParamVal(ParamIndices::PHA_PARAM_IND) > 0)) {
    error_msg(errstr, "Cannot use PHA with non-splits.\n");
    exit(1);
  }

  // If nothing entered, assume the out directory is the same place as the instance location
  if (tmpoutdir.empty()) {
    tmpoutdir = instdir;
  }

  // Check to make sure that instdir has a '/' on the end
  if (!instdir.empty() && instdir.at(instdir.size() - 1) != '/')
    instdir += '/';

  // Check to make sure that tmpoutdir has a "/" on the end
  if (!tmpoutdir.empty() && tmpoutdir.at(tmpoutdir.size() - 1) != '/')
    tmpoutdir += '/';

  const std::string TEST_NAME = tmpname;
  tmpname.clear();

  // Open instances info output file
  const size_t found_slash_ii = GlobalVariables::LOG_FILE_NAME.find_last_of(
      "/\\");
  const size_t found_dot_ii = GlobalVariables::LOG_FILE_NAME.find_last_of('.');
  const std::string FINAL_LOG_FILE_DIR =
      (found_slash_ii != std::string::npos)
          && (found_slash_ii < GlobalVariables::LOG_FILE_NAME.length()) ?
          "" : tmpoutdir;
  const std::string FINAL_LOG_FILE_EXT =
      (found_dot_ii != std::string::npos)
          && (found_dot_ii < GlobalVariables::LOG_FILE_NAME.length())
          && ((found_slash_ii == std::string::npos)
              || (found_slash_ii < found_dot_ii)) ?
          "" : GlobalConstants::DEFAULT_LOG_FILE_EXT;  // last condition is to ensure input was not something like "../stub"
  FINAL_LOG_FILE_NAME = (FINAL_LOG_FILE_DIR + GlobalVariables::LOG_FILE_NAME
      + FINAL_LOG_FILE_EXT);
  bool log_exists = fexists(FINAL_LOG_FILE_NAME.c_str());
  GlobalVariables::log_file = fopen(FINAL_LOG_FILE_NAME.c_str(), "a");
  if (GlobalVariables::log_file == NULL) {
    error_msg(errstr, "Cannot open file %s to write instance info.\n",
        FINAL_LOG_FILE_NAME.c_str());
    exit(1);
  }

  // Get file name stub
  size_t found_dot = TEST_NAME.find_last_of('.');

  // Put string after last '.' into string in_file_ext
  if (found_dot >= TEST_NAME.length()) { // Previously checked found_dot >= 0
    std::cerr
        << "*** ERROR: Cannot find the file extension (no '.' in input file name).\n"
        << std::endl;
    exit(1);
  }

  GlobalVariables::prob_name = TEST_NAME.substr(0, found_dot);

  std::string tmp_file_ext(TEST_NAME.substr(found_dot + 1));
  unsigned found_dot_tmp = found_dot;

  if (tmp_file_ext.compare("gz") == 0 || tmp_file_ext.compare("bz2") == 0) {
    found_dot_tmp = GlobalVariables::prob_name.find_last_of('.');

    // Put string after last '.' into string in_file_ext
    if (found_dot_tmp >= GlobalVariables::prob_name.length()) {
      std::cerr
          << "*** ERROR: Other than gz or bz2, cannot find the file extension (no '.' in input file name).\n"
          << std::endl;
      exit(1);
    }

    tmp_file_ext = GlobalVariables::prob_name.substr(found_dot_tmp + 1);
    GlobalVariables::prob_name = GlobalVariables::prob_name.substr(0,
        found_dot_tmp);
  }

  GlobalVariables::in_file_ext.assign(tmp_file_ext);

  std::cout << "Directory for instance has been set to \"" + instdir + "\"."
      << std::endl;

  std::cout << "The filename is \"" + TEST_NAME + "\"." << std::endl;

  std::cout << "Output directory has been set to \"" + tmpoutdir + "\"."
      << std::endl;

  std::cout
      << "Summary instance information will be sent to \"" + FINAL_LOG_FILE_NAME
          + "\"." << std::endl;

  if (!opt_file.empty() || (param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND) & BB_Strategy_Options::use_best_bound)) {
    std::cout << "Reading objective information from \"" + opt_file + "\"."
        << std::endl;
    GlobalVariables::bestObjValue = getObjValueFromFile(opt_file.c_str(), prob_name);
    if (isInfinity(std::abs(GlobalVariables::bestObjValue))) {
      error_msg(errorstring, "Could not find instance %s in obj value file.\n", prob_name.c_str());
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }
    std::cout << "Best known IP objective value (read from file): "
        << GlobalVariables::bestObjValue << "." << std::endl;
  }

  // If the instance info file did not exist, then we want to add a header so that we know what each column is
  if (!log_exists) {
    if (CLEANING_MODE_OPTION == 0) {
      writeHeaderToLog(GlobalVariables::log_file);
    } else {
      writeCleaningHeaderToLog(GlobalVariables::log_file);
    }
  }

  // Set f_name to directory+TEST_NAME. Same for the stub.
  GlobalVariables::in_f_name = instdir + TEST_NAME;
  GlobalVariables::in_f_name_stub = instdir + GlobalVariables::prob_name;
  // Do the same for the out stub
  GlobalVariables::out_f_name_stub = tmpoutdir + GlobalVariables::prob_name;

  // Set up out file names
  out_f_name_stubSub = GlobalVariables::out_f_name_stub + "Sub";
  out_f_name_stubSubExpanded = GlobalVariables::out_f_name_stub + "SubExpanded";
  out_f_name_stubOrig = GlobalVariables::out_f_name_stub + "Orig";
  out_f_name_stubLifted = GlobalVariables::out_f_name_stub + "Lifted";

  // Check that the file exists
  bool f_exists = fexists(GlobalVariables::in_f_name.c_str());
  if (!f_exists) {
    // Check if maybe it is a compressed file and the ".gz" extension was forgotten
    f_exists = fexists((GlobalVariables::in_f_name + ".gz").c_str());
    if (f_exists) {
#ifdef TRACE
      std::cout << "Inputted filename was missing .gz extension. "
          << "As no file exists (as given), but an archived version does exist, working with the archived file."
          << std::endl;
#endif
      GlobalVariables::in_f_name = GlobalVariables::in_f_name + ".gz";
    } else {
      // Check if maybe it is a compressed file and the ".bz2" extension was forgotten
      f_exists = fexists((GlobalVariables::in_f_name + ".bz2").c_str());
      if (f_exists) {
#ifdef TRACE
        std::cout << "Inputted filename was missing .bz2 extension. "
            << "As no file exists (as given), but an archived version does exist, working with the archived file."
            << std::endl;
#endif
        GlobalVariables::in_f_name = GlobalVariables::in_f_name + ".bz2";
      } else {
        error_msg(errorstring, "File %s does not exist!\n", GlobalVariables::in_f_name.c_str());
        writeErrorToLog(errorstring, GlobalVariables::log_file);
        exit(1);
      }
    }
  }

  // Write the problem name into the instances info file
  writeProbNameToLog(GlobalVariables::prob_name, GlobalVariables::log_file);

  return 0;
} /* init */

/***********************************************************************************
 * Wrap up
 ***********************************************************************************/
int wrapUp(int returncode) {
  char start_time_string[25];
  snprintf(start_time_string, sizeof(start_time_string) / sizeof(char), "%s",
      asctime(timeinfo));

  time(&end_time_t);
  timeinfo = localtime(&end_time_t);
  char end_time_string[25];
  snprintf(end_time_string, sizeof(end_time_string) / sizeof(char), "%s",
      asctime(timeinfo));

  GlobalVariables::timeStats.end_timer(GlobalConstants::TOTAL_TIME);

  if (CLEANING_MODE_OPTION == 0) {
#if defined(SHOULD_WRITE_TIME_INFO) || defined(SHOULD_WRITE_CUTSOLVER_ERRORS) || defined(SHOULD_WRITE_PARAMS)
    char filename[300];
#endif

#ifdef SHOULD_WRITE_TIME_INFO
#  ifdef TRACE
    printf("\n## Saving time information to file. ##\n");
#  endif
    snprintf(filename, sizeof(filename) / sizeof(char), "%s-Times.csv",
        GlobalVariables::out_f_name_stub.c_str());
    writeTimeInfo(GlobalVariables::timeStats, GlobalVariables::time_name,
        filename, end_time_string);
#endif
    writeTimeInfoToLog(GlobalVariables::timeStats, GlobalVariables::time_name,
        //end_time_string,
        GlobalVariables::log_file);

#ifdef SHOULD_WRITE_CUTSOLVER_ERRORS
    if (!NO_GEN_CUTS) {
#  ifdef TRACE
      printf("\n## Saving errors in cut solver to file. ##\n");
#  endif
      snprintf(filename, sizeof(filename) / sizeof(char),
          "%s-CutSolverErrors.csv", GlobalVariables::out_f_name_stub.c_str());
      writeCutSolverErrors(num_obj_tried, filename);
    }
#endif

    // For reference print parameters that were used
    printf("\n## Parameters that were used. ##\n");
#ifdef SHOULD_WRITE_PARAMS
    snprintf(filename, sizeof(filename) / sizeof(char), "%s-params.txt",
        GlobalVariables::out_f_name_stub.c_str());
    FILE* paramOut = fopen(filename, "w");
    param.printParams(paramOut);
    fclose(paramOut);
#endif
    printf("PROB_NAME: %s\n", GlobalVariables::prob_name.c_str());
    printf("OUT_F_NAME_STUB: %s\n", GlobalVariables::out_f_name_stub.c_str());
    printf("INSTANCES_INFO: %s\n", FINAL_LOG_FILE_NAME.c_str());
    for (int param_ind = 0; param_ind < NUM_PARAMS; param_ind++) {
      printf("%s: %d\n", ParamName[param_ind].c_str(),
          param.paramVal[param_ind]);
    }
    printf("EPS: %e\n", param.getEPS());
    printf("RAYEPS: %e\n", param.getRAYEPS());
    printf("MIN_ORTHOGONALITY: %e\n", param.getMINORTHOGONALITY());
    printf("MIN_VIOL_ABS: %e\n", param.getMIN_VIOL_ABS());
    printf("MIN_VIOL_REL: %e\n", param.getMIN_VIOL_REL());
    printf("TIMELIMIT: %e\n", param.getTIMELIMIT());
    printf("PARTIAL_BB_TIMELIMIT: %e\n", param.getPARTIAL_BB_TIMELIMIT());
    printf("BB_TIMELIMIT: %e\n", param.getBB_TIMELIMIT());
    printf("CUTSOLVER_TIMELIMIT: %e\n", param.getCUTSOLVER_TIMELIMIT());

    printf("%s", compare_cuts_output.c_str());
    if (param.getParamVal(ParamIndices::BB_RUNS_PARAM_IND) != 0) {
      printf("\n## Branch-and-bound results ##\n");
//      printf("Opt (VPCs, VPCs+Gomory): %.2f, %.2f\n",
//          bb_opt_mycuts, bb_opt_allcuts);
//      printf("Iters (VPCs, VPCs+Gomory): %ld, %ld\n",
//          bb_iters_mycuts, bb_iters_allcuts);
//      printf("Nodes (VPCs, VPCs+Gomory): %ld, %ld\n",
//          bb_nodes_mycuts, bb_nodes_allcuts);
//      printf("Root passes (VPCs, VPCs+Gomory): %ld, %ld\n",
//          bb_num_root_passes_mycuts, bb_num_root_passes_allcuts);
//      printf("First cut pass (VPCs, VPCs+Gomory): %.2f, %.2f\n",
//          bb_first_cut_pass_mycuts, bb_first_cut_pass_allcuts);
//      printf("Last cut pass (VPCs, VPCs+Gomory): %.2f, %.2f\n",
//          bb_last_cut_pass_mycuts, bb_last_cut_pass_allcuts);
//      printf("Time (VPCs, VPCs+Gomory): %.2f, %.2f\n",
//          bb_time_mycuts, bb_time_allcuts);
      printf("Obj (VPCs, VPCs+Gomory): %s, %s\n",
          stringValue(avg_bb_info_mycuts.obj, "%.2f").c_str(), 
          stringValue(avg_bb_info_allcuts.obj, "%.2f").c_str());
      printf("Bound (VPCs, VPCs+Gomory): %s, %s\n",
          stringValue(avg_bb_info_mycuts.bound, "%.2f").c_str(), 
          stringValue(avg_bb_info_allcuts.bound, "%.2f").c_str());
      printf("Iters (VPCs, VPCs+Gomory): %ld, %ld\n",
          avg_bb_info_mycuts.iters, avg_bb_info_allcuts.iters);
      printf("Nodes (VPCs, VPCs+Gomory): %ld, %ld\n",
          avg_bb_info_mycuts.nodes, avg_bb_info_allcuts.nodes);
      printf("Root passes (VPCs, VPCs+Gomory): %ld, %ld\n",
          avg_bb_info_mycuts.root_passes, avg_bb_info_allcuts.root_passes);
      printf("First cut pass (VPCs, VPCs+Gomory): %.2f, %.2f\n",
          avg_bb_info_mycuts.first_cut_pass, avg_bb_info_allcuts.first_cut_pass);
      printf("Last cut pass (VPCs, VPCs+Gomory): %.2f, %.2f\n",
          avg_bb_info_mycuts.last_cut_pass, avg_bb_info_allcuts.last_cut_pass);
      printf("Root time (VPCs, VPCs+Gomory): %.2f, %.2f\n",
          avg_bb_info_mycuts.root_time, avg_bb_info_allcuts.root_time);
      printf("Last solution time (VPCs, VPCs+Gomory): %.2f, %.2f\n",
          avg_bb_info_mycuts.last_sol_time, avg_bb_info_allcuts.last_sol_time);
      printf("Time (VPCs, VPCs+Gomory): %.2f, %.2f\n",
          avg_bb_info_mycuts.time, avg_bb_info_allcuts.time);
    }
  } /* check if not cleaning mode */
  else {
    writeEntryToLog(
        GlobalVariables::timeStats.get_total_time(GlobalConstants::TOTAL_TIME),
        GlobalVariables::log_file);
  }

  writeEntryToLog(end_time_string, GlobalVariables::log_file);
  writeEntryToLog(prob_name, GlobalVariables::log_file);
  writeEntryToLog("DONE!", GlobalVariables::log_file, '\n');

  if (GlobalVariables::log_file != NULL) {
    fclose(GlobalVariables::log_file);
  }

  printf("\n");
  printf("Start time: %s\n", start_time_string);
  printf("Finish time: %s\n", end_time_string);
  printf("Elapsed time: %.f seconds\n", difftime(end_time_t, start_time_t));
  printf("\n## DONE! ##\n");
  return returncode;
} /* wrapUp */

