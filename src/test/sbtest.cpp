#ifdef USE_C
#include <math.h>
#include <stdio.h>
#else
#include <cstdio>
#include <cmath>
#endif

#define error_msg(str, fmt, ...) \
  char str[500]; \
    snprintf(str, sizeof(str)/sizeof(char), "*** ERROR: %s:%d: " fmt, __FILE__, __LINE__, ##__VA_ARGS__); \
      std::cerr << str

#ifdef VPC_USE_COIN
#include "OsiClpSolverInterface.hpp"
#endif

#ifdef VPC_USE_CBC
#include "CbcModel.hpp"
#include "CbcCompareObjective.hpp"
#include "CbcCompareEstimate.hpp"
#include "CbcStrategy.hpp"
#include "CbcBranchDefaultDecision.hpp"
#include "CbcBranchStrongDecision.hpp"
#include "OsiChooseVariable.hpp"
#include "CbcTree.hpp"
#include "CbcNode.hpp"
#include "CbcEventHandler.hpp"
#endif

#ifdef VPC_USE_GUROBI
#include "gurobi_c++.h"
#endif

#ifdef VPC_USE_CPLEX
#include <ilcplex/cplexx.h>
#endif

#ifdef VPC_USE_COIN
void printStatus(CoinWarmStartBasis* info) {
  const char* structStatus = info->getStructuralStatus();
  const char* artifStatus = info->getArtificialStatus();
  printf("Structural variables:\n");
  for (int i = 0; i < info->getNumStructural(); i++) {
    printf("\t%d: %d\n", i, info->getStructStatus(i));
    //printf("\t%d: %c\n", i, structStatus[i]);
  }

  printf("Artificial variables:\n");
  for (int i = 0; i < info->getNumArtificial(); i++) {
    printf("\t%d: %d\n", i, info->getArtifStatus(i));
    //printf("\t%d: %c\n", i, artifStatus[i]);
  }
}

bool solveFromHotStart(OsiSolverInterface* const solver, const int col,
    const bool isChangedUB, const double origBound, const double newBound) {
  solver->solveFromHotStart();
  if (solver->isIterationLimitReached()) {
    // This sometimes happens, e.g., with arki001
    solver->unmarkHotStart();
    //solver->disableFactorization();
    solver->resolve();
    if (isChangedUB) {
      solver->setColUpper(col, origBound);
    } else {
      solver->setColLower(col, origBound);
    }
    solver->resolve();
    //solver->enableFactorization();
    solver->markHotStart();
    if (isChangedUB) {
      solver->setColUpper(col, newBound);
    } else {
      solver->setColLower(col, newBound);
    }
    solver->solveFromHotStart();
  }
  return (solver->isProvenOptimal());
} /* solveFromHotStart overload */
#endif

/******************************/
int main(int argc, char* argv[]) {
  //error_msg(errorstring, "Testing error.\n");
  //exit(1);
  char f_name[256]; 

  printf("Name of the input file (.lp or .mps):\n");
  if (argc > 1) {
    sprintf(f_name, "%s", argv[1]);
  } else {
    scanf("%s", f_name);
  }
  //snprintf(f_name, sizeof(f_name)/sizeof(char), "/home/akazachk/utility/bbtest/data/bm23.mps");
  //snprintf(f_name, sizeof(f_name)/sizeof(char), "/home/akazachk/utility/bbtest/data/p0033.mps");
  //snprintf(f_name, sizeof(f_name)/sizeof(char), "/home/akazachk/utility/bbtest/data/bell3b.mps");
  //snprintf(f_name, sizeof(f_name)/sizeof(char), "/home/akazachk/utility/bbtest/data/bell4.mps");
  printf("%s\n",f_name);
  
#ifdef VPC_USE_COIN
  printf("Testing cleaning.\n");
  char in_file_ext[256];
  // Settings
  const double limit = 10000000;
  //const int maxNumLeafNodes = (argc > 1) ? atoi(argv[1]) : 2;

  // Set up solver
  OsiClpSolverInterface solver;

#ifndef TRACE
  solver.messageHandler()->setLogLevel(0);
  solver.getModelPtr()->messageHandler()->setLogLevel(0);
#endif

//  solver.setHintParam(OsiDoPresolveInInitial, false);
//  solver.setHintParam(OsiDoPresolveInResolve, false);
  solver.setIntParam(OsiMaxNumIterationHotStart, limit);
  solver.getModelPtr()->setIntParam(ClpMaxNumIterationHotStart, 999999);
//  solver.setSpecialOptions(16);
//  int moreSpecialOptions = solver.getModelPtr()->moreSpecialOptions();
//  solver.getModelPtr()->setMoreSpecialOptions(moreSpecialOptions+256);


  // put string after last '.' in string in_file_ext
  char *pchar = strrchr(f_name, '.');
  if(pchar == NULL) {
    printf("### ERROR: can not find the file extension (no '.' in input file name)\n");
    exit(1);
  }
  strcpy(in_file_ext, &(f_name[pchar-f_name+1]));

  if(strcmp(in_file_ext, "lp") == 0) {
    // Read LP file
    solver.readLp(f_name);        
  }
  else {
    if(strcmp(in_file_ext, "mps") == 0) {
      // Read MPS file
      solver.readMps(f_name);    
    }
    else {
      printf("### ERROR: unrecognized extension: %s\n", in_file_ext);
      exit(1);
    }
  }

  // Initial solve
  solver.initialSolve();

  if (solver.isProvenOptimal()) {
    printf("LP optimal. Obj value: %e.\n", solver.getObjValue());
  }

  int col;
  /*
  // Down branch on 924 --- nevermind?
  col = 924;
  printf("\n## Testing col: %d. ##\n", col);
  solver.enableFactorization();
  solver.markHotStart();

  solver.setColUpper(col,0);
  solver.solveFromHotStart();
  //solveFromHotStart(&solver, col, true, 1, 0);
  if (solver.isProvenOptimal()) {
    printf("Hot start optimal. Objective: %e.\n", solver.getObjValue());
  } else if (solver.isIterationLimitReached()) {
    printf("Iteration limit reached.\n");
  } else {
    printf("Something else happened.\n");
  }
  solver.unmarkHotStart();
  solver.disableFactorization();

  solver.resolve();
  if (solver.isProvenOptimal()) {
    printf("Resolve optimal. Objective: %e.\n", solver.getObjValue());
  } else if (solver.isIterationLimitReached()) {
    printf("Iteration limit reached.\n");
  } else {
    printf("Something else happened.\n");
  }
  solver.setColUpper(col,1);
  */

  // Down branch on 877
  col = 877;
  printf("\n## Testing col: %d. ##\n", col);
  solver.enableFactorization();
  solver.markHotStart();

  solver.setColUpper(col,0);
  solver.solveFromHotStart();
  //solveFromHotStart(&solver, col, true, 1, 0);
  if (solver.isProvenOptimal()) {
    printf("Hot start optimal. Objective: %e.\n", solver.getObjValue());
  } else if (solver.isIterationLimitReached()) {
    printf("Iteration limit reached.\n");
    printf("Iteration count: %d.\n", solver.getIterationCount());
  } else {
    printf("Something else happened.\n");
  }
  solver.unmarkHotStart();
  solver.disableFactorization();

  solver.resolve();
  if (solver.isProvenOptimal()) {
    printf("Resolve optimal. Objective: %e.\n", solver.getObjValue());
  } else if (solver.isIterationLimitReached()) {
    printf("Iteration limit reached.\n");
  } else {
    printf("Something else happened.\n");
  }
  solver.setColUpper(col,1);
#endif

#ifdef VPC_USE_GUROBI
  // Solve LP with Gurobi
  try {
    GRBEnv env = GRBEnv();
    GRBModel model = GRBModel(env, f_name);

    model.optimize();

    int optimstatus = model.get(GRB_IntAttr_Status);

    if (optimstatus == GRB_INF_OR_UNBD) {
      model.set(GRB_IntParam_Presolve, 0);
      model.optimize();
      optimstatus = model.get(GRB_IntAttr_Status);
    }

    if (optimstatus == GRB_OPTIMAL) {
      double objval = model.get(GRB_DoubleAttr_ObjVal);
      std::cout << "Optimal objective: " << objval << std::endl;
    } else if (optimstatus == GRB_INFEASIBLE) {
      std::cout << "Model is infeasible" << std::endl;

      /*
      // compute and write out IIS
      model.computeIIS();
      model.write("model.ilp");
      */
    } else if (optimstatus == GRB_UNBOUNDED) {
      std::cout << "Model is unbounded" << std::endl;
    } else {
      std::cout << "Optimization was stopped with status = "
        << optimstatus << std::endl;
    }

  } catch(GRBException e) {
    std::cout << "Error code = " << e.getErrorCode() << std::endl;
    std::cout << e.getMessage() << std::endl;
  } catch (...) {
    std::cout << "Error during optimization" << std::endl;
  }
#endif

#ifdef VPC_USE_CPLEX
   int      solstat;
   double   objval;
   double   *x     = NULL;

   CPXENVptr     env = NULL;
   CPXLPptr      lp = NULL;
   int           status;
   int           j;
   int           cur_numcols;
   
   /* Initialize the CPLEX environment */

   env = CPXXopenCPLEX (&status);

   /* If an error occurs, the status value indicates the reason for
      failure.  A call to CPXXgeterrorstring will produce the text of
      the error message.  Note that CPXXopenCPLEX produces no output,
      so the only way to see the cause of the error is to use
      CPXXgeterrorstring.  For other CPLEX routines, the errors will
      be seen if the CPXXPARAM_ScreenOutput indicator is set to CPXX_ON.  */

   if ( env == NULL ) {
      char  errmsg[CPXMESSAGEBUFSIZE];
      fprintf (stderr, "Could not open CPLEX environment.\n");
      CPXXgeterrorstring (env, status, errmsg);
      fprintf (stderr, "%s", errmsg);
      exit(1);
   }

   /* Turn on output to the screen */

   status = CPXXsetintparam (env, CPXPARAM_ScreenOutput, CPX_ON);
   if ( status ) {
      fprintf (stderr, 
               "Failure to turn on screen indicator, error %d.\n", status);
      exit(1);
   }

   /* Create the problem, using the filename as the problem name */

   lp = CPXXcreateprob (env, &status, f_name);

   /* A returned pointer of NULL may mean that not enough memory
      was available or there was some other problem.  In the case of 
      failure, an error message will have been written to the error 
      channel from inside CPLEX.  In this example, the setting of
      the parameter CPXXPARAM_ScreenOutput causes the error message to
      appear on stdout.  Note that most CPLEX routines return
      an error code to indicate the reason for failure.   */

   if ( lp == NULL ) {
      fprintf (stderr, "Failed to create LP.\n");
      exit(1);
   }

   /* Now read the file, and copy the data into the created lp */

   status = CPXXreadcopyprob (env, lp, f_name, NULL);
   if ( status ) {
      fprintf (stderr, "Failed to read and copy the problem data.\n");
      exit(1);
   }


   /* Optimize the problem and obtain solution. */

   status = CPXXmipopt (env, lp);

   if ( status ) {
      fprintf (stderr, "Failed to optimize MIP.\n");
      exit(1);
   }

   solstat = CPXXgetstat (env, lp);
   printf ("Solution status %d.\n", solstat);

   status  = CPXXgetobjval (env, lp, &objval);

   if ( status ) {
      fprintf (stderr,"Failed to obtain objective value.\n");
      exit(1);
   }

   printf ("Objective value %.10g\n", objval);
   
   /* Free up the problem as allocated by CPXXcreateprob, if necessary */

   if ( lp != NULL ) {
      status = CPXXfreeprob (env, &lp);
      if ( status ) {
         fprintf (stderr, "CPXXfreeprob failed, error code %d.\n", status);
      }
   }

   /* Free up the CPLEX environment, if necessary */

   if ( env != NULL ) {
      status = CPXXcloseCPLEX (&env);

      /* Note that CPXXcloseCPLEX produces no output,
         so the only way to see the cause of the error is to use
         CPXXgeterrorstring.  For other CPLEX routines, the errors will
         be seen if the CPXXPARAM_ScreenOutput indicator is set to CPXX_ON. */

      if ( status ) {
      char  errmsg[CPXMESSAGEBUFSIZE];
         fprintf (stderr, "Could not close CPLEX environment.\n");
         CPXXgeterrorstring (env, status, errmsg);
         fprintf (stderr, "%s", errmsg);
      }
   }
     
   return (status);
#endif
  return 0;
} /* main */

#ifdef VPC_USE_CPLEX
/* This simple routine frees up the pointer *ptr, and sets *ptr to NULL */
static void
free_and_null (char **ptr)
{
   if ( *ptr != NULL ) {
      free (*ptr);
      *ptr = NULL;
   }
} /* END free_and_null */ 
#endif
