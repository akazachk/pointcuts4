#ifdef USE_C
#include <math.h>
#include <stdio.h>
#else
#include <cstdio>
#include <cmath>
#endif

#include "OsiClpSolverInterface.hpp"
#include "CbcModel.hpp"
#include "CbcCompareObjective.hpp"
#include "CbcCompareEstimate.hpp"
#include "CbcStrategy.hpp"
#include "CbcBranchDefaultDecision.hpp"
#include "CbcBranchStrongDecision.hpp"
#include "OsiChooseVariable.hpp"
#include "CbcTree.hpp"
#include "CbcNode.hpp"
#include "CbcEventHandler.hpp"

int numNodes;
std::vector<CbcNode*> node;
std::vector<CbcNode> nodes;
std::vector<CbcNodeInfo*> nodeInfo;

// Information that we will save
std::vector<std::vector<double> > newBound;

void printStatus(CoinWarmStartBasis* info) {
  const char* structStatus = info->getStructuralStatus();
  const char* artifStatus = info->getArtificialStatus();
  printf("Structural variables:\n");
  for (int i = 0; i < info->getNumStructural(); i++) {
    printf("\t%d: %d\n", i, info->getStructStatus(i));
    //printf("\t%d: %c\n", i, structStatus[i]);
  }

  printf("Artificial variables:\n");
  for (int i = 0; i < info->getNumArtificial(); i++) {
    printf("\t%d: %d\n", i, info->getArtifStatus(i));
    //printf("\t%d: %c\n", i, artifStatus[i]);
  }
}

void saveInformation(CbcModel* model) {
  CbcTree* tree = model->tree();
  numNodes = tree->size();
  node.resize(numNodes);
  nodes.resize(numNodes);
  nodeInfo.resize(numNodes);

  // Get new bounds
  //const OsiClpSolverInterface* solver = dynamic_cast<OsiClpSolverInterface*>(model->
  for (int i = 0; i < model->numberObjects(); i++) {
    const OsiSimpleInteger* obj = dynamic_cast<const OsiSimpleInteger*>(model->object(i));
    const int col = obj->columnNumber();
    newBound[0][col] = obj->originalLowerBound();
    newBound[1][col] = obj->originalUpperBound();
  }

  for (int i = 0; i < numNodes; i++) {
    node[i] = tree->nodePointer(i);
    //nodes[i] = *(tree->nodePointer(i));
    //node[i] = new CbcNode(*(tree->nodePointer(i)));
    //nodeInfo[i] = dynamic_cast<CbcFullNodeInfo*>(node[i]->nodeInfo());
  //  nodeInfo[i] = (CbcFullNodeInfo*)(node[i]->nodeInfo());
    nodeInfo[i] = node[i]->nodeInfo();
    

    //CbcFullNodeInfo* tmp = (CbcFullNodeInfo*)(nodeInfo[i]);
    //CbcPartialNodeInfo* tmp_partial = dynamic_cast<CbcPartialNodeInfo*>(nodeInfo[i]);
    //printf("test.\n");
    /*
    const double* lower = tmp->lower();
    const double* upper = tmp->upper();
    for (int i = 0; i < 27; i++) {
      printf("i: %d\tlower: %f\tupper: %f\n", i, lower[i], upper[i]);
    }
    */
  }
}

/************************************************************************/
void setMySelection(CbcModel *model) {

  // Set comparison rule
  //p5_CompareDFS_BFS compareDB;
  CbcCompareObjective compare;
  model->setNodeComparison(compare);
} /* setMySelection */

// Return non-zero to return quickly
// whereFrom: 1, 2, 3, 4, or 5 with meaning given below
static int callBack(CbcModel * model, int whereFrom)
{
    printf("\n####################### WHERE FROM: %d ####### \n", (int) whereFrom);
  int returnCode = 0;
  switch (whereFrom) {
    case 1: // after initial solve by dualsimplex etc
      break;
    case 2: // after preprocessing
      // check model->status() and model->secondaryStatus() to stop if
      // problem is infeasible, unbounded or other abnormal termination occurred
      if ((!model->status()) && (model->secondaryStatus())) {
        returnCode = 1;
      }
      break;
    case 3: // just before branchAndBound
      {
        // Add customized objects
        setMySelection(model);

        //CbcCompareUser compare;
        //      //model->setNodeComparison(compare);
      }
      break;
    case 4: // just after branchAndBound (before postprocessing)
      // If not good enough could skip postprocessing
      break;
    case 5: // after postprocessing
      break;
    default:
      abort();
  }
  return returnCode;
} /* callBack */

/*
static int cancelAsap=0;
*/
/*
 *   0 - not yet in Cbc
 *   1 - in Cbc with new signal handler
 *   2 - ending Cbc
 */
/*
static int statusOfCbc=0;

#include "CoinSignal.hpp"
static CoinSighandler_t saveSignal = static_cast<CoinSighandler_t> (0);

extern "C" {static void signal_handler(int )
  {
    cancelAsap=3;
    return;
  }
}
*/

/** This is so user can trap events and do useful stuff.  
 *
 *     CbcModel model_ is available as well as anything else you care 
 *         to pass in
 */
class VPCEventHandler : public CbcEventHandler {
  public:
    /**@name Overrides */
    //@{
    virtual CbcAction event(CbcEvent whichEvent);
    //@}
    
    /**@name Constructors, destructor, etc. */
    //@{
    /** Default constructor */
    VPCEventHandler();
    /** VPC special constructor */
    VPCEventHandler(const int maxNumLeafNodes);
    /// Constructor with pointer to model (redundant as setEventHandler does)
    VPCEventHandler(CbcModel* model);
    /** Destructor */
    virtual ~VPCEventHandler();
    /** The copy constructor. */
    VPCEventHandler(const VPCEventHandler& rhs);
    /// Assignment
    VPCEventHandler& operator=(const VPCEventHandler& rhs);
    /// Clone
    virtual CbcEventHandler* clone() const;
    /// Copy our stuff
    virtual void copyOurStuff(const VPCEventHandler* const rhs);
    //@}
    
  protected:
    int maxNumLeafNodes;
    std::vector<double> lb;
    std::vector<double> ub;
}; /* VPCEventHandler definition */

/* Now we implement the methods for VPCEventHandler */
VPCEventHandler::VPCEventHandler () : CbcEventHandler() {
  copyOurStuff(NULL);
}

VPCEventHandler::VPCEventHandler (const int maxNumLeafNodes) : CbcEventHandler() {
  this->maxNumLeafNodes = maxNumLeafNodes;
}

VPCEventHandler::VPCEventHandler (const VPCEventHandler & rhs) : CbcEventHandler(rhs) {
  copyOurStuff(&rhs);
}

VPCEventHandler::VPCEventHandler(CbcModel * model) : CbcEventHandler(model) {
  try {
    copyOurStuff(dynamic_cast<VPCEventHandler*>(model->getEventHandler()));
  } catch (std::exception& e) {
    copyOurStuff(NULL);
  }
}

VPCEventHandler::~VPCEventHandler () {
  printf("\n## VPCEventHandler: destroyer called. ##\n");
}

VPCEventHandler& VPCEventHandler::operator=(const VPCEventHandler& rhs) {
  if (this != &rhs) {
    CbcEventHandler::operator=(rhs);
  }
  this->maxNumLeafNodes = rhs.maxNumLeafNodes;
  return *this;
}

CbcEventHandler * VPCEventHandler::clone() const {
  return new VPCEventHandler(*this);
}

void VPCEventHandler::copyOurStuff(const VPCEventHandler* const rhs) {
  if (rhs) {
    this->maxNumLeafNodes = rhs->maxNumLeafNodes;
    this->lb = rhs->lb;
    this->ub = rhs->ub;
  } else {
    this->maxNumLeafNodes = 0;
  }
}

/*
enum CbcEvent { 
  *! Processing of the current node is complete.
  node = 200,
  *! A tree status interval has arrived.
  treeStatus,
  *! A solution has been found.
  solution,
  *! A heuristic solution has been found.
  heuristicSolution,
  *! A solution will be found unless user takes action (first check).
  beforeSolution1,
  *! A solution will be found unless user takes action (thorough check).
  beforeSolution2,
  *! After failed heuristic.
  afterHeuristic,
  *! On entry to small branch and bound.
  smallBranchAndBound,
  *! After a pass of heuristic.
  heuristicPass,
  *! When converting constraints to cuts.
  convertToCuts,
  *! End of search. 
  endSearch
} ; 
*/

CbcEventHandler::CbcAction
VPCEventHandler::event(CbcEvent whichEvent) {
    printf("\n####################### WHERE FROM: %d #######\n", (int) whichEvent);
  if (whichEvent == treeStatus) {
    const int numNodesOnTree = model_->tree()->size();
    int numLeafNodes = 0;
    for (int i = 0; i < numNodesOnTree; i++) {
      numLeafNodes += model_->tree()->nodePointer(i)->nodeInfo()->numberBranchesLeft();
    }
    printf("\nNumber nodes on tree: %d. Number leaf nodes: %d.\n", numNodesOnTree, numLeafNodes);
    if (numLeafNodes >= maxNumLeafNodes) {
      printf("Reached maximum number leaf nodes (%d). Exiting.\n", this->maxNumLeafNodes);
      saveInformation(model_);
      lb.resize(model_->solver()->getNumCols());
      ub.resize(model_->solver()->getNumCols());
      for (int i = 0; i < model_->solver()->getNumCols(); i++) {
        lb[i] = model_->solver()->getColLower()[i];
        ub[i] = model_->solver()->getColUpper()[i];
      }
 //     cancelAsap = 1;
      return stop;
    }
  }
  return noAction;
  /*
  if (!statusOfCbc) {
    // override signal handler
    // register signal handler
    saveSignal = signal(SIGINT, signal_handler);
    statusOfCbc = 1;
  }
  if ( (cancelAsap&2)!=0 ) {
    printf("Cbc got cancel\n");
    // switch off Clp cancel
    cancelAsap &= 2;
    return stop;
  }
  // If in sub tree carry on
  if (!model_->parentModel()) {
    if ((whichEvent == endSearch) && (statusOfCbc == 1)) {
      // switch off cancel
      cancelAsap = 0;
      // restore signal handler
      signal(SIGINT, saveSignal);
      statusOfCbc = 2;
    }
    if (whichEvent==solution||whichEvent==heuristicSolution) {
  #ifdef STOP_EARLY
      return stop; // say finished
  #else
  #ifdef WANT_SOLUTION
      // If preprocessing was done solution will be to processed model
      int numberColumns = model_->getNumCols();
      const double * bestSolution = model_->bestSolution();
      assert (bestSolution);
      printf("value of solution is %g\n",model_->getObjValue());
      for (int i=0;i<numberColumns;i++) {
        if (fabs(bestSolution[i])>1.0e-8)
          printf("%d %g\n",i,bestSolution[i]);
      }  
  #endif
      return noAction; // carry on
  #endif
    } else {
      return noAction; // carry on
    }
  } else {
    return noAction; // carry on
  }
  */
} /* VPCEventHandler::event  */

/******************************/
int main(int argc, char* argv[]) {
  char f_name_lp[256], in_file_ext[256];

  printf("Name of the input file (.lp or .mps):\n");
  //scanf("%s", f_name_lp);
  //snprintf(f_name_lp, sizeof(f_name_lp)/sizeof(char), "/home/akazachk/utility/bbtest/data/bm23.mps");
  //snprintf(f_name_lp, sizeof(f_name_lp)/sizeof(char), "/home/akazachk/utility/bbtest/data/p0033.mps");
  //snprintf(f_name_lp, sizeof(f_name_lp)/sizeof(char), "/home/akazachk/utility/bbtest/data/bell3b.mps");
  snprintf(f_name_lp, sizeof(f_name_lp)/sizeof(char), "/home/akazachk/utility/bbtest/data/bell4.mps");
  printf("%s\n",f_name_lp);
  
  // Settings
  const int numBeforeTrusted = 10000;
  const bool useCustomBranching = true;
  const double limit = 10000000;
  const int maxNumLeafNodes = (argc > 1) ? atoi(argv[1]) : 2;

  // Set up solver
  OsiClpSolverInterface solver;

#ifndef TRACE
  solver.messageHandler()->setLogLevel(0);
  solver.getModelPtr()->messageHandler()->setLogLevel(0);
#endif

  solver.setHintParam(OsiDoPresolveInInitial, false);
  solver.setHintParam(OsiDoPresolveInResolve, false);
//  solver.setIntParam(OsiMaxNumIterationHotStart, limit);
  solver.setSpecialOptions(16);
  int moreSpecialOptions = solver.getModelPtr()->moreSpecialOptions();
  solver.getModelPtr()->setMoreSpecialOptions(moreSpecialOptions+256);

  // put string after last '.' in string in_file_ext
  char *pchar = strrchr(f_name_lp, '.');
  if(pchar == NULL) {
    printf("### ERROR: can not find the file extension (no '.' in input file name)\n");
    exit(1);
  }
  strcpy(in_file_ext, &(f_name_lp[pchar-f_name_lp+1]));

  if(strcmp(in_file_ext, "lp") == 0) {
    // Read LP file
    solver.readLp(f_name_lp);        
  }
  else {
    if(strcmp(in_file_ext, "mps") == 0) {
      // Read MPS file
      solver.readMps(f_name_lp);    
    }
    else {
      printf("### ERROR: unrecognized extension: %s\n", in_file_ext);
      exit(1);
    }
  }

  // Initial solve
  solver.initialSolve();

  // Set up model
  CbcModel model(solver);
  model.setLogLevel(3);
  model.messagesPointer()->setDetailMessages(10, 10000, (int *) NULL);
  
  model.setTypePresolve(0);
  model.setMaximumCutPassesAtRoot(0);
  model.setMaximumCutPasses(0);
  model.setWhenCuts(0);
  model.setNumberBeforeTrust(numBeforeTrusted);
  model.setNumberStrong(solver.getNumCols());
  model.setPrintFrequency(1);

  // Set up branching and variable choosing
  OsiChooseStrong choose;
  choose.setNumberStrong(solver.getNumCols());
  choose.setNumberBeforeTrusted(numBeforeTrusted);
  if (useCustomBranching) {
    CbcBranchDefaultDecision branch;
    //CbcBranchStrongDecision branch;
    branch.setChooseMethod(choose);
    model.setBranchingMethod(branch);
  } 

  // Set up event handler
  VPCEventHandler* eventHandler = new VPCEventHandler(maxNumLeafNodes);
  model.passInEventHandler(eventHandler);
  if (eventHandler) {
    delete eventHandler;
  }
  eventHandler = dynamic_cast<VPCEventHandler*>(model.getEventHandler());
  newBound.resize(2);
  newBound[0].resize(solver.getNumCols());
  newBound[1].resize(solver.getNumCols());
  for (int i = 0; i < solver.getNumCols(); i++) {
    newBound[0][i] = solver.getColLower()[i];
    newBound[1][i] = solver.getColUpper()[i];
  }

  /*
  // Set up which node is selected from the tree
  //CbcCompareObjective compare;
  CbcCompareEstimate compare;
  model.setNodeComparison(compare);
  */

  // Finally, run branch-and-bound procedure
  model.branchAndBound(3);

  printf("\n## Debugging... ##\n");
  // Changes in bounds
  for (int i = 0; i < solver.getNumCols(); i++) {
    const double olb = solver.getColLower()[i];
    const double oub = solver.getColUpper()[i];
    if (olb != newBound[0][i]) { 
      printf("Column %d\tOld LB: %1.6f\tNew LB: %1.6f\n", i, olb, newBound[0][i]);
    }
    if (oub != newBound[1][i]) { 
      printf("Column %d\tOld UB: %1.6f\tNew UB: %1.6f\n", i, oub, newBound[1][i]);
    }
  }

  // Free stuff
  for (int i = 0; i < numNodes; i++) {
//    delete nodes[i]; 
  }
  node.clear();
  node.resize(0);
  nodes.clear();
  nodes.resize(0);
  nodeInfo.clear();
  nodeInfo.resize(0);
  return 0;
}
