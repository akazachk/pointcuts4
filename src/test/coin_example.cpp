// Last edit: 10/19/2015
//
// Name:     coin_or.cpp
// Author:   Aleksandr M. Kazachkov (adapted from proj1.cpp by Francois Margot)
//           Tepper School of Business 
//           Carnegie Mellon University, Pittsburgh, PA 15213
//           email: akazachk@cmu.edu
// Date:     
//-----------------------------------------------------------------------------

#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <cfloat>
#include <climits> // numeric_limits

#include <sys/stat.h> // For checking whether a file exists

#include "OsiSolverInterface.hpp"
#include "OsiClpSolverInterface.hpp"
#include "CoinTime.hpp"

// For LandP test
//#include "CglLandP.hpp"

const double EPS = 1e-7;
const int NUMSPACES = 10;

#ifdef VPC_USE_CBC
#include "CbcSolver.hpp"
#include "CbcModel.hpp"
#include "CbcBranchDefaultDecision.hpp"
#include "CbcCompareDefault.hpp"
#include "OsiChooseVariable.hpp"

void setMessageHandler(CbcModel* cbc_model) {
  //cbc_model->solver()->setHintParam(OsiDoReducePrint, false, OsiHintTry);
  //cbc_model->messagesPointer()->setDetailMessages(10, 10000, (int *) NULL);
  //cbc_model->setLogLevel(0);
  //cbc_model->setPrintFrequency(10000);
  cbc_model->setLogLevel(3);
  cbc_model->messagesPointer()->setDetailMessages(10, 10000, (int *) NULL);
  cbc_model->setPrintFrequency(1);
}
#endif

void setConstantObjectiveFromPackedVector(
    OsiClpSolverInterface* const cutSolver, const double val = 0.,
    const int numIndices = 0, const int* indices = NULL) {
  if (numIndices > 0 && indices) {
    for (int i = 0; i < numIndices; i++) {
      cutSolver->setObjCoeff(indices[i], val);
    }
  } else {
    for (int i = 0; i < cutSolver->getNumCols(); i++) {
      cutSolver->setObjCoeff(i, val);
    }
  }
} /* setConstantObjectiveFromPackedVector */

bool isVal(const double val1, const double val2,
    const double eps = EPS) {
  return (std::abs((val1) - (val2)) <= eps);
}

bool greaterThanVal(const double val1, const double val2,
    const double eps = EPS) {
  return (val1 > (val2 + eps));
}

bool isZero(const double val, const double eps = EPS) {
  return isVal(val, 0.0, eps);
}

bool badlyScaled(int num_cols, const double* colSolution, const double rhs,
    const double SENSIBLE_MAX = 1e8) {
  double minAbsElem = 0.0, maxAbsElem = 0.0;
  for (int i = 0; i < num_cols; i++) {
    double absCurr = std::abs(colSolution[i]);
    if (isZero(absCurr, EPS)) { // make sure min and max are not zero
      continue;
    }

    if ((minAbsElem > absCurr) || isZero(minAbsElem, EPS)) {
      minAbsElem = absCurr;
    }
    if (maxAbsElem < absCurr) {
      maxAbsElem = absCurr;
    }
  }
  const double absCurr = std::abs(rhs);
  if (absCurr > maxAbsElem) {
    maxAbsElem = absCurr;
  }
  if (isZero(maxAbsElem, EPS)
      || greaterThanVal(maxAbsElem / minAbsElem, SENSIBLE_MAX, EPS)) {
    return true;
  }
  return false;
} /* badlyScaled */

bool checkSolverOptimality(OsiSolverInterface* const solver,
    const bool exitOnDualInfeas = false, const double timeLimit = 5,
    const int maxNumResolves = 5) {
  OsiClpSolverInterface* clpsolver = NULL;
  try {
    clpsolver = dynamic_cast<OsiClpSolverInterface*>(solver);
  } catch (std::exception& e) {
    // disregard
  }

  // First clean
  if (clpsolver) {
    const int status = clpsolver->getModelPtr()->secondaryStatus();
    if (status == 2 || status == 3 || status == 4) {
      clpsolver->getModelPtr()->cleanup(1);
      if (maxNumResolves > 0) {
        clpsolver->getModelPtr()->setNumberIterations(0);
        clpsolver->getModelPtr()->setMaximumSeconds(timeLimit);
        clpsolver->resolve();
        return checkSolverOptimality(solver, exitOnDualInfeas, timeLimit, 
            maxNumResolves - 1);
      }
    }
  }

  int resolve_count = 0;
  if (maxNumResolves > 0 && clpsolver) {
    // Do some resolves, but first save whether the initial status is dual infeasible
    // The reason is that for neos15 w/str=2, we were getting a dual infeasible problem
    // turn into a primal infeasible problem after resolves 
    // (probably the strengthening causes numerical issues)
    const bool oldStatusDualInfeasible = (clpsolver->isProvenDualInfeasible());
    bool resolve = true;
    double infeas = std::numeric_limits<double>::max();
    while (resolve && resolve_count < maxNumResolves) {
      const double curr_infeas =
          clpsolver->getModelPtr()->sumPrimalInfeasibilities()
              + clpsolver->getModelPtr()->sumDualInfeasibilities();
      resolve = (resolve_count == 0)
          || (!isZero(curr_infeas) && (curr_infeas < infeas));
      if (resolve) {
        clpsolver->getModelPtr()->setNumberIterations(0);
        clpsolver->getModelPtr()->setMaximumSeconds(timeLimit);
        clpsolver->resolve();
        resolve_count++;
      }
      infeas = curr_infeas;
    }

    // If dual infeas -> primal infeas, do a dual resolve, which should fix the issue
    if (oldStatusDualInfeasible && clpsolver->isProvenPrimalInfeasible()) {
      clpsolver->getModelPtr()->dual(2,0); // just do values pass
      if (!clpsolver->isProvenDualInfeasible() && !clpsolver->isProvenOptimal()) {
        clpsolver->getModelPtr()->setProblemStatus(2);
        resolve_count = maxNumResolves; // stop resolving
      }
    }
  }

  // Clean once more if we can
  if (clpsolver && resolve_count < maxNumResolves) {
    const int status = clpsolver->getModelPtr()->secondaryStatus();
    if (status == 2 || status == 3 || status == 4) {
      clpsolver->getModelPtr()->cleanup(1);
      if (maxNumResolves > 0) {
        clpsolver->getModelPtr()->setNumberIterations(0);
        clpsolver->getModelPtr()->setMaximumSeconds(timeLimit);
        clpsolver->resolve();
        resolve_count++;
        return checkSolverOptimality(solver, exitOnDualInfeas, timeLimit,
            maxNumResolves - resolve_count);
      }
    }
  }

  if (solver->isProvenPrimalInfeasible()) {
    return false;
  } else if (!(solver->isProvenOptimal())) {
    // Sometimes need to resolve once more to get the correct status
    if (resolve_count < maxNumResolves) {
      solver->resolve();
    }
    if (solver->isProvenPrimalInfeasible()) {
      return false;
    } else if (solver->isProvenDualInfeasible()) {
      if (exitOnDualInfeas) {
        fprintf(stderr,
            "Solver is dual infeasible. Check why this happened!\n");
        exit(1);
      } else {
        return false;
      }
    } else if (!(solver->isProvenOptimal())) {
      return false;
    }
  }

  return true;
} /* checkSolverOptimality */

  /**********************************************************/
  /**
   * Check if a file exists
 */
bool fexists(const char* filename) {
  struct stat buffer;
  return (stat (filename, &buffer) == 0);
}

void printStuff(OsiClpSolverInterface* solver) {
  const int row_to_test = 458;
  printf("\n## After initial solve. ##\n");
  printf("Sum dual infeasibilities: %f\n", solver->getModelPtr()->sumDualInfeasibilities());
  printf("Row price on row %d: %f\n", row_to_test, solver->getRowPrice()[row_to_test]);
  
  printf("\n## Enabling factorization. ##\n");
  solver->enableFactorization();
  printf("Sum dual infeasibilities: %f\n", solver->getModelPtr()->sumDualInfeasibilities());
  printf("Row price on row %d: %f\n", row_to_test, solver->getRowPrice()[row_to_test]);

  printf("\n## Initial solve once more. ##\n");
  solver->initialSolve();
  solver->disableFactorization(); // this is only to check that one can disable then enable factorization at this point and not break things
  solver->enableFactorization();
  printf("Sum dual infeasibilities: %f\n", solver->getModelPtr()->sumDualInfeasibilities());
  printf("Row price on row %d: %f\n", row_to_test, solver->getRowPrice()[row_to_test]);
}

CoinWarmStartBasis::Status getStatus(const char c) {
  if (c == 'B')
    return CoinWarmStartBasis::Status::basic;
  else if (c == 'U')
    return CoinWarmStartBasis::Status::atUpperBound;
  else if (c == 'L')
    return CoinWarmStartBasis::Status::atLowerBound;
  else
    return CoinWarmStartBasis::Status::isFree;
}

int test(const char* fname) {
  OsiClpSolverInterface* solver = new OsiClpSolverInterface;
  OsiClpSolverInterface* copysolver = new OsiClpSolverInterface;
  solver->readMps(fname);

  printf("\n## Parent node. \n##");
  solver->initialSolve();
  solver->setColLower(37,1);
  printf("\n## Resolve after setting col 37 lower bound to 1. \n##");
  solver->resolve();

  solver->readMps(fname);
  solver->setColLower(37,1);
  printf("\n## Initial solve with col 37 lower bound set to 1. ##\n");
  solver->initialSolve();

  //solver->disableFactorization();
  //solver->setHintParam(OsiDoPresolveInInitial, 1);

  //solver->readMps("bc1_presolved.mps.gz");
  //solver->readMps("infeasibleParentNode.mps.gz");
  //solver->initialSolve();
  /*
  CbcModel* cbc_model = new CbcModel;
  cbc_model->swapSolver(solver);
  cbc_model->setModelOwnsSolver(true); // solver will be deleted with cbc object
  cbc_model->setLogLevel(3);
  cbc_model->messagesPointer()->setDetailMessages(10, 10000, (int *) NULL);
  cbc_model->setPrintFrequency(1);
  cbc_model->setMaximumNodes(656);
  cbc_model->branchAndBound(3);

  return 0;

  setMessageHandler(cbc_model);
  int num_strong = 5;
  int num_before_trusted = std::numeric_limits<int>::max();
  
  CbcBranchDecision* branch = new CbcBranchDefaultDecision();
  CbcCompareBase* compare = new CbcCompareDefault();
  cbc_model->setTypePresolve(0);
  //cbc_model->setMaximumSeconds(max_time);
  cbc_model->setMaximumCutPassesAtRoot(0);
  cbc_model->setMaximumCutPasses(0);
  cbc_model->setWhenCuts(0);
  cbc_model->setNumberStrong(num_strong);
  cbc_model->setNumberBeforeTrust(num_before_trusted);
  OsiChooseStrong choose;
  choose.setNumberStrong(num_strong);
  choose.setNumberBeforeTrusted(num_before_trusted);
  branch->setChooseMethod(choose);
  cbc_model->setBranchingMethod(*branch);
  cbc_model->setNodeComparison(compare);


  if (branch) {
    delete branch;
  }
  if (compare) {
    delete compare;
  }
  if (cbc_model) {
    delete cbc_model;
  }
  */

  /*
  CoinWarmStartBasis* basis = new CoinWarmStartBasis;
  basis->setSize(solver->getNumCols(), solver->getNumRows());

  // Read basis from file
  std::string filename = "basis.txt";
  std::ifstream infile(filename.c_str());
  if (infile.is_open()) {
    std::string line;
    if (!std::getline(infile, line)) {
      printf("\n## ERROR: Cannot read row basis. ##\n");
      exit(1);
    }
    int i = 0;
    for (std::string::iterator it=line.begin(); it!=line.end(); ++it) {
       const CoinWarmStartBasis::Status st = getStatus(*it);
       basis->setArtifStatus(i, st);
       i++;
    }

    if (!std::getline(infile, line)) {
      printf("\n## ERROR: Cannot read col basis. ##\n");
      exit(1);
    }
    i = 0;
    for (std::string::iterator it=line.begin(); it!=line.end(); ++it) {
       const CoinWarmStartBasis::Status st = getStatus(*it);
       basis->setStructStatus(i, st);
       i++;
    }
    infile.close();
  } else {
    printf("\n## ERROR: no such file named %s. ##\n", filename.c_str());
    exit(1);
  }

  OsiClpSolverInterface* clone = dynamic_cast<OsiClpSolverInterface*>(solver->clone());
  clone->disableFactorization();
  clone->setWarmStart(solver->getWarmStart());
  clone->resolve();
  */

  /*
  OsiClpSolverInterface* tmpSolver = new OsiClpSolverInterface;
  tmpSolver->readMps("infeasibleParentNode.mps.gz");
  tmpSolver->initialSolve();
  CoinWarmStartBasis* basis = tmpSolver->getPointerToWarmStart();
  */

  /*for (int i = 0; i < 10; i++) {
    printf("\n## Attempt %d/%d ##\n", i+1, 10);
    solver->setColLower(29,1);
    solver->resolve();
    if (!solver->isProvenPrimalInfeasible()) {
      printf("Solver is not proven primal infeasible, which is what we expected. Secondary status is %d.\n", solver->getModelPtr()->secondaryStatus());
      exit(1);
    }
    solver->setColLower(29,0);
    solver->resolve();
  }*/

  // Get all the bounds that were changed
  /*
  int num_changed = 0; 
  for (int col = 0; col < solver->getNumCols(); col++) {
    if (solver->getColLower()[col] != tmpSolver->getColLower()[col]) {
      solver->setColLower(col, tmpSolver->getColLower()[col]);
      num_changed++;
    }
    if (solver->getColUpper()[col] != tmpSolver->getColUpper()[col]) {
      solver->setColUpper(col, tmpSolver->getColLower()[col]);
      num_changed++;
    }
  }*/

  //solver->setWarmStart(basis);
  //solver->resolve();

  //printStuff(solver);
  //solver->disableFactorization();

  //OsiClpSolverInterface* copy = dynamic_cast<OsiClpSolverInterface*>(solver->clone());
  //copy->disableFactorization();
  //copy->initialSolve();
  //printStuff(copy);

  if (solver) {
    delete solver;
  }
  //if (basis) {
   // delete basis;
 // }
  return 0;
} /* test */

/**********************************************************/
int main(int argc, char** argv) 
{
  std::string dir, outdir, f_name, f_name_stub, prob_name;
  std::string tmppath, instdir, tmpdir, tmpname;

  if (argc < 2) {
    printf("Name of the input file (.lp or .mps):\n");
    std::getline(std::cin, tmppath);
  } else {
    tmppath = argv[1];
  }

  // Extract the overall directory as well as the instance directory and filename stub
  size_t found_slash = tmppath.find_last_of("/\\");
  if ((found_slash != std::string::npos) && (found_slash < tmppath.length())) {
    instdir = tmppath.substr(0, found_slash + 1);
    tmpname = tmppath.substr(found_slash + 1);

    // We can now look for the second slash, to see where to save the instances information
    size_t found_slash2 = instdir.find_last_of("/\\",
        instdir.length() - 2);
    if ((found_slash2 != std::string::npos) && (found_slash2 < instdir.length())
        && (found_slash2 != instdir.length() - 1)) {
      tmpdir = instdir.substr(0, found_slash2 + 1);
    } else
      // Otherwise, only one folder was specified, so the current directory is where to save instance info
      tmpdir = "./";
  } else { // Otherwise, no directory was specified, so the parent directory is where to save instance info
    tmpname = tmppath;
    instdir = "./";
    tmpdir = "../";
  }

  // Check to make sure that tmpdir has a '/' on the end
  if (!tmpdir.empty() && tmpdir.at(tmpdir.size() - 1) != '/')
    tmpdir += '/';

  // Check to make sure that instdir has a '/' on the end
  if (!instdir.empty() && instdir.at(instdir.size() - 1) != '/')
    instdir += '/';

  printf(
      "\nThe overall directory is set to \"%s\".\nThe instance directory is \"%s\".\nThe filename is \"%s\".\n",
      tmpdir.c_str(), instdir.c_str(), tmpname.c_str());

  const std::string TEST_DIR = tmpdir;
  const std::string TEST_NAME = tmpname;
  // Get file name stub
  unsigned found_dot = TEST_NAME.find_last_of('.');

  // Put string after last '.' into string in_file_ext
  if (found_dot >= TEST_NAME.length()) {
    std::cerr
      << "*** ERROR: Cannot find the file extension (no '.' in input file name).\n"
      << std::endl;
    exit(1);
  }

  prob_name = TEST_NAME.substr(0, found_dot);

  std::string tmp_file_ext(TEST_NAME.substr(found_dot + 1));
  unsigned found_dot_tmp = found_dot;

  if (tmp_file_ext.compare("gz") == 0) {
    found_dot_tmp = prob_name.find_last_of('.');

    // Put string after last '.' into string in_file_ext
    if (found_dot_tmp >= prob_name.length()) {
      std::cerr
        << "*** ERROR: Other than gz, cannot find the file extension (no '.' in input file name).\n"
        << std::endl;
      exit(1);
    }

    tmp_file_ext = prob_name.substr(found_dot_tmp + 1);
    prob_name = prob_name.substr(0, found_dot_tmp);
  }

  const std::string in_file_ext(tmp_file_ext);
  dir.assign(instdir);

#ifdef TRACE
  std::cout << "\nProblem name is \"" + prob_name + "\"." << std::endl;
  std::cout << "\nExtension is \"" + in_file_ext + "\"." << std::endl;
  std::cout << "\nDirectory for instance has been set to \"" + dir + "\"."
    << std::endl;
#endif

  // Set f_name to directory+TEST_NAME. Same for the stub.
  f_name = dir + TEST_NAME;
  f_name_stub = dir + prob_name;

  // Check that the file exists
  bool f_exists = fexists(f_name.c_str());
  if (!f_exists) {
    // Check if maybe it is a compressed file and the ".gz" extension was forgotten
    f_exists = fexists((f_name + ".gz").c_str());
    if (f_exists) {
#ifdef TRACE
      std::cout << "Inputted filename was missing .gz extension. "
        << "As no such file exists, but an archived version does exist, working with the archived file."
        << std::endl;
#endif
      f_name = f_name + ".gz";
    } else {
      char errorstring[256];
      snprintf(errorstring, sizeof(errorstring) / sizeof(char),
        "*** ERROR: File %s does not exist!\n", f_name.c_str());
      std::cerr << errorstring << std::endl;
      exit(1);
    }   
  }
  return test(f_name.c_str());

  OsiClpSolverInterface* solver = new OsiClpSolverInterface;

  if (in_file_ext.compare("lp") == 0) {
    // Read LP file
#ifdef TRACE
    std::cout << "Reading LP file." << std::endl;
#endif
    solver->readLp(f_name.c_str());
  } else {
    if (in_file_ext.compare("mps") == 0) {
      // Read MPS file
#ifdef TRACE
      std::cout << "Reading MPS file." << std::endl;
#endif
      solver->readMps(f_name.c_str());
    } else {
      char errorstring[256];
      snprintf(errorstring, sizeof(errorstring) / sizeof(char),
          "*** ERROR: Unrecognized extension: %s.\n",
          in_file_ext.c_str());
      std::cerr << errorstring << std::endl;
      exit(1);
    }
  }

  //printf("\n## Performing initial solve ##\n");
  ////solver->enableFactorization();
  std::vector<double> origObj(solver->getObjCoefficients(), solver->getObjCoefficients() + solver->getNumCols());
  std::vector<double> onesObj(solver->getNumCols(), 1.);
  std::vector<double> zeroesObj(solver->getNumCols(), 0.);

  int iter = 0;
  const int num_iters = 1000;

  const bool generateError1 = argc > 2 ? atoi(argv[2]) : true;
  const bool generateError2 = argc > 3 ? atoi(argv[3]) : true;
  const bool generateError3 = argc > 4 ? atoi(argv[4]) : true;

  //solver->getModelPtr()->setMaximumIterations(9999999);
  //solver->getModelPtr()->setMaximumSeconds(5);
  //solver->getModelPtr()->setMaximumIterations(2147483647);
  //solver->getModelPtr()->setNumberIterations(0);
  //solver->getModelPtr()->setMaximumSeconds(-10.);
  solver->setHintParam(OsiDoPresolveInInitial, 1);
  //if (generateError3) {
  //  solver->setHintParam(OsiDoPresolveInResolve, 1);
  //}

  //solver->setObjective(zeroesObj.data());
  //solver->disableFactorization();
  //solver->getModelPtr()->cleanMatrix();
  solver->initialSolve();
  return 1;
  checkSolverOptimality(solver);

  std::vector<int> indices;
  for (int i = 0; i < (int) origObj.size(); i++) {
    if (origObj[i] != 0) {
      indices.push_back(i);
    }
  }
  setConstantObjectiveFromPackedVector(solver, 1., indices.size(), indices.data());
  solver->resolve();
  checkSolverOptimality(solver);
  if (solver) {
    delete solver;
  }
  return 1;

  /*
  solver->setObjective(onesObj.data());
  solver->resolve();
  solver->resolve();
  if (solver->isProvenOptimal()) {
    printf("Solver optimal.\n");
  } else if (solver->isProvenPrimalInfeasible()) {
    printf("Solver primal infeasible.\n");
  } else if (solver->isProvenDualInfeasible()) {
    printf("Solver dual infeasible.\n");
  } else {
    printf("Solver other status.\n");
  }

  const CoinPackedMatrix* mat = solver->getMatrixByRow();
  for (int i = 0; i < solver->getNumRows(); i++) {
    solver->getModelPtr()->setMaximumSeconds(5);
    solver->setObjective(zeroesObj.data()); // zero out the objective
    const CoinShallowPackedVector vec = mat->getVector(i);
    const int num_el = vec.getNumElements();
    const int* ind = vec.getIndices();
    const double* el = vec.getElements();
    for (int j = 0; j < num_el; j++) {
      solver->setObjCoeff(ind[j], el[j]);
    }
    solver->resolve();
    solver->resolve();
    if (solver->isProvenOptimal()) {
      printf("Solver optimal.\n");
    } else if (solver->isProvenPrimalInfeasible()) {
      printf("Solver primal infeasible.\n");
    } else if (solver->isProvenDualInfeasible()) {
      printf("Solver dual infeasible.\n");
    } else {
      printf("Solver other status.\n");
    }
  }
    
  solver->setObjective(zeroesObj.data()); // zero out the objective

  while (iter < num_iters) {
  for (int i = 0; i < solver->getNumCols(); i++) {
    solver->getModelPtr()->setMaximumSeconds(5);
    solver->setObjCoeff(i, 1.);
    solver->resolve();
    solver->resolve();
    if (solver->isProvenOptimal()) {
        printf("Solver optimal.\n");
      } else if (solver->isProvenPrimalInfeasible()) {
        printf("Solver primal infeasible.\n");
      } else if (solver->isProvenDualInfeasible()) {
        printf("Solver dual infeasible.\n");
      } else {
        printf("Solver other status.\n");
      }
    solver->setObjCoeff(i, 0.);
  }
  iter++;
  }

  for (int i = 0; i < solver->getNumRows(); i++) {
    solver->getModelPtr()->setMaximumSeconds(5);
    solver->setObjective(zeroesObj.data()); // zero out the objective
    const CoinShallowPackedVector vec = mat->getVector(i);
    const int num_el = vec.getNumElements();
    const int* ind = vec.getIndices();
    const double* el = vec.getElements();
    for (int j = 0; j < num_el; j++) {
      solver->setObjCoeff(ind[j], el[j]);
    }
    solver->resolve();
    solver->resolve();
    if (solver->isProvenOptimal()) {
      printf("Solver optimal.\n");
    } else if (solver->isProvenPrimalInfeasible()) {
      printf("Solver primal infeasible.\n");
    } else if (solver->isProvenDualInfeasible()) {
      printf("Solver dual infeasible.\n");
    } else {
      printf("Solver other status.\n");
    }
  }
  */
  iter = 0;
  while (iter < num_iters) {
    for (int i = 0; i < solver->getNumCols(); i++) {
      solver->getModelPtr()->setMaximumSeconds(5);
      solver->setObjCoeff(i, 1.);
      solver->resolve();
      checkSolverOptimality(solver);
      if (std::isnan(solver->getObjValue())) {
        fprintf(stderr, "\n*** ERROR: Detected nan objective.\n");
        exit(1);
      }
      if (!generateError1 && std::isnan(solver->getObjValue())) {
        solver->getModelPtr()->setMaximumSeconds(5);
        solver->initialSolve();
        if (generateError2) {
          if (solver->isProvenOptimal()) {
            solver->resolve();
          }
        } 
      } else if (generateError1) {
        solver->resolve();
      }
      if (0) {
      if (badlyScaled(solver->getNumCols(), solver->getColSolution(), 1.)) {
        solver->getModelPtr()->setMaximumSeconds(5);
        solver->initialSolve();
        if (solver->isProvenOptimal()) {
          solver->resolve();
        }
      }
      }
      if (solver->isProvenOptimal()) {
        printf("Solver optimal.\n");
      } else if (solver->isProvenPrimalInfeasible()) {
        printf("Solver primal infeasible.\n");
      } else if (solver->isProvenDualInfeasible()) {
        printf("Solver dual infeasible.\n");
      } else {
        printf("Solver other status.\n");
      }
      solver->setObjCoeff(i, 0.);
    }
    solver->setObjective(origObj.data());
    solver->getModelPtr()->setMaximumSeconds(5);
    solver->resolve();
    checkSolverOptimality(solver);
    if (std::isnan(solver->getObjValue())) {
      fprintf(stderr, "\n*** ERROR: Detected nan objective.\n");
      exit(1);
    }
    if (1) {
    solver->setObjective(zeroesObj.data());
    solver->resolve();
    }
    iter++;
  }

  solver->setObjective(origObj.data());
  solver->getModelPtr()->setMaximumSeconds(5);
  solver->resolve();
  solver->resolve();
  if (solver->isProvenOptimal()) {
    printf("Solver optimal.\n");
  } else if (solver->isProvenPrimalInfeasible()) {
    printf("Solver primal infeasible.\n");
  } else if (solver->isProvenDualInfeasible()) {
    printf("Solver dual infeasible.\n");
  } else {
    printf("Solver other status.\n");
  }

  if (solver)
    delete solver;
  return 0;

  //CbcModel model(*solver);
  
#ifdef VPC_USE_CBC
  printf("\n## Testing first solver ##\n");
  CbcModel* first_cbc_model = new CbcModel;
  first_cbc_model->swapSolver(solver);
  setMessageHandler(first_cbc_model);

  CbcMain0(*first_cbc_model);
  const char * argv2[]={"driver4","-cuts=off","-presolve=off","-preprocess=off","-solve","-quit"};
  CbcMain1(6, argv2, *first_cbc_model);
  //first_cbc_model->branchAndBound();

  printf("Cpu Time: %10.2f\n", CoinCpuTime() - first_cbc_model->getDblParam(CbcModel::CbcStartSeconds));
  printf("Optimal solution: value: %10.2f\n", first_cbc_model->getObjValue());
  printf("Num nodes: %d\n", first_cbc_model->getNodeCount());
  
  first_cbc_model->setModelOwnsSolver(false);
  solver = dynamic_cast<OsiClpSolverInterface*>(first_cbc_model->solver());
  delete first_cbc_model;
  //printf("\n## Deleting solver ##\n");
  delete solver;

  printf("\n## Testing tmpSolver ##\n");
//  OsiClpSolverInterface* tmpSolver = new OsiClpSolverInterface;
  std::cout << "\nExtension is \"" + in_file_ext + "\"." << std::endl;
  OsiClpSolverInterface* tmpSolver = new OsiClpSolverInterface;
  if (in_file_ext.compare("lp") == 0) {
    // Read LP file
    std::cout << "Reading LP file." << std::endl;
    tmpSolver->readLp(f_name.c_str());
  } else { //if (in_file_ext.compare("mps") == 0) {
      // Read MPS file
      std::cout << "Reading MPS file." << std::endl;
      tmpSolver->readMps(f_name.c_str());
    }
  //tmpSolver->initialSolve();
  //setMessageHandler(tmpSolver);
  CbcModel* tmp_cbc_model = new CbcModel;
  tmp_cbc_model->swapSolver(tmpSolver);
  setMessageHandler(tmp_cbc_model);

  //CbcMain0(*tmp_cbc_model);
  tmp_cbc_model->solver()->initialSolve();
  printf("Init obj val: %f\n", tmp_cbc_model->solver()->getObjValue());
  printf("When cuts? %d.\n", tmp_cbc_model->whenCuts());
  printf("Type presolve: %d.\n", tmp_cbc_model->typePresolve());
  tmp_cbc_model->setMaximumCutPassesAtRoot(0);
  tmp_cbc_model->branchAndBound();
  
  printf("Cpu Time: %10.2f\n", CoinCpuTime() - tmp_cbc_model->getDblParam(CbcModel::CbcStartSeconds));
  printf("Optimal solution: value: %10.2f\n", tmp_cbc_model->getObjValue());
  printf("Num nodes: %d\n", tmp_cbc_model->getNodeCount());
  
  tmp_cbc_model->solver()->initialSolve();
  printf("Init obj val: %f\n", tmp_cbc_model->solver()->getObjValue());

  //printf("\n## Deleting model ##\n");
  tmp_cbc_model->setModelOwnsSolver(false);
  tmpSolver = dynamic_cast<OsiClpSolverInterface*>(tmp_cbc_model->solver());
  delete tmp_cbc_model;
  
  //printf("\n## Deleting solver ##\n");
  if (tmpSolver)
    delete tmpSolver;
  //printf("Deleted solver.\n");
  return 0;
#endif

  /*
  CglLandP LandPGen;
  LandPGen.parameter().modularize = 0;
  LandPGen.parameter().strengthen = 0;

  OsiCuts structLandP;
  LandPGen.generateCuts(*solver, structLandP);
  OsiSolverInterface::ApplyCutsReturnCode code = solver->applyCuts(structLandP);
  const int numCuts = structLandP.sizeCuts();
  printf("Cuts applied: %d (of the %d possible).\n",
      code.getNumApplied(), numCuts);
  solver->resolve();

  printf("After applying cuts, value is: %f.\n", solver->getObjValue());
  if (solver)
    delete solver;
  return 0;
  */
}

//  // Get matrices
//  const CoinPackedMatrix* clp_byRow = solver->getMatrixByRow();
//  const CoinPackedMatrix* clp_byCol = solver->getMatrixByCol();
//
//  const int* clp_start = clp_byRow->getVectorStarts();
//  const int* clp_indices = clp_byRow->getIndices();
//  const int* clp_rowLength = clp_byRow->getVectorLengths();
//
//  // Where does row 0 start?
//  const int row = 0;
//  const int row_start = clp_start[row];
//  const int row_len = clp_rowLength[row];
//  assert(row_len > 0);
//  const int first_index = clp_indices[row_start];
//  
//  // Modify coefficient
//  printf("\n###\n");
//  printf("From getMatrixByRow: Coefficient on variable %d in row %d is %f.\n", first_index, row, clp_byRow->getElements()[0]);
//  printf("From getMatrixByCol: Coefficient on variable %d in row %d is %f.\n", first_index, row, clp_byCol->getElements()[0]);
//
//  printf("\n## Modifying coefficient on variable 0 in row 0 to have value 1000. ##\n");
//  solver->modifyCoefficient(first_index,row,1000,false);
//
//  printf("\n###\n");
//  printf("From getMatrixByRow: Coefficient on variable %d in row %d is %f.\n", first_index, row, clp_byRow->getElements()[0]);
//  printf("From getMatrixByCol: Coefficient on variable %d in row %d is %f.\n", first_index, row, clp_byCol->getElements()[0]);
//  return(0);
//}
//
//  OsiClpSolverInterface* copySolver = dynamic_cast<OsiClpSolverInterface*>(solver->clone());
//  copySolver->enableSimplexInterface(true);
//
//  printf("\n## Save which variable is basic in each row, and get basis status ##\n");
//  copySolver->enableFactorization();
//  std::vector<int> varBasicInRow(copySolver->getNumRows());
//  copySolver->getBasics(varBasicInRow.data());
//  std::vector<int> cstat(copySolver->getNumCols()), rstat(copySolver->getNumRows());
//  copySolver->getBasisStatus(cstat.data(), rstat.data());
//  
//  printf("\n## Save the variable values ##\n");
//  std::vector<double> varVal(copySolver->getNumCols() + copySolver->getNumRows());
//  for (int var = 0; var < copySolver->getNumCols() + copySolver->getNumRows(); var++) {
//    if (var < copySolver->getNumCols()) {
//      varVal[var] = copySolver->getColSolution()[var];
//    } else {
//      int row = var - copySolver->getNumCols();
//      varVal[var] = copySolver->getRightHandSide()[row] - copySolver->getRowActivity()[row];
//    }
//  }
//
//  printf("\n## Choose incoming variable ##\n");
//  ClpSimplex* model = copySolver->getModelPtr();
//  int varIn = -1;
//  int nbvar = -1;
//  int numConfirmed = 0;
//  for (int var = 0; var < copySolver->getNumCols() + copySolver->getNumRows(); var++) {
//    copySolver->enableSimplexInterface(true);
//    model = copySolver->getModelPtr();
//    if (model->getStatus(var) == 1) {
//      continue;
//    } 
//   
//    nbvar++;
//    printf("\n#### PROCESSING NB VAR %d ####\n", nbvar);
//    
//    varIn = var;
//    int varIn_row = (varIn < copySolver->getNumCols()) ? -1 : varIn - copySolver->getNumCols();
//    /*
//       if (prob_name.compare("bell5_no37") == 0) {
//       assert(varIn == 12); // For bell5; remove this assert for other instances
//       }
//     */
//    int in_stat_old = model->getStatus(varIn);
//    int inStatusForPivot = (in_stat_old == ClpSimplex::Status::atUpperBound) ? -1 : 1;
//
//    printf("\n## Manually find outgoing variable ##\n");
//    std::vector<double> BInvA(copySolver->getNumRows());
//    copySolver->getBInvACol(varIn, BInvA.data());
//    int rayDirn;
//    if (varIn < copySolver->getNumCols()) {
//      rayDirn = (model->getStatus(varIn) == ClpSimplex::Status::atUpperBound) ? -1 : 1;
//      assert(cstat[varIn] == model->getStatus(varIn));
//    } else {
//      if (copySolver->getRowSense()[varIn_row] == 'G') {
//        rayDirn = -1;
//      } else {
//        rayDirn = 1;
//      } 
//    }
//
//    int varOut_manual = -1, pivotRow_manual = -1, out_stat_new_manual = -1;
//    double theta_manual = copySolver->getInfinity(), value_new_manual;
//    for (int row = 0; row < copySolver->getNumRows(); row++) {
//      int candidate = varBasicInRow[row];
//      assert(candidate == model->pivotVariable()[row]);
//      int candidate_row = (candidate < copySolver->getNumCols()) ? -1 : candidate - copySolver->getNumCols();
//      double LB, UB;
//      if (candidate_row < 0) {
//        LB = copySolver->getColLower()[candidate];
//        UB = copySolver->getColUpper()[candidate];
//      } else {
//        if (copySolver->getRowSense()[candidate_row] == 'L') {
//          LB = 0;
//          UB = copySolver->getInfinity();
//        } else {
//          LB = -1. * copySolver->getInfinity();
//          UB = 0;
//        }
//      }
//
//      // Get the ray value
//      // Usually it is -1 * abar_{ij}, but we negate if it is an ub var
//      double rayVal = BInvA[row] * rayDirn * -1; 
//
//      double bound;
//      int tmp_stat = -1;
//      if (rayVal < -1. * EPS) {
//        // Intersects LB (check that it exists)
//        if (LB <= -1. * copySolver->getInfinity() + EPS) {
//          continue;
//        }
//        bound = LB;
//        tmp_stat = ClpSimplex::Status::atLowerBound;
//      } else if (rayVal > EPS) {
//        // Intersects LB (check that it exists)
//        if (UB >= copySolver->getInfinity() - EPS) {
//          continue;
//        }
//        bound = UB;
//        tmp_stat = ClpSimplex::Status::atUpperBound;
//      } else {
//        continue; // ray does not intersect this hyperplane
//      }
//
//      double dist = (bound - varVal[candidate]) / rayVal;
//
//      if (dist < theta_manual) {
//        theta_manual = dist;
//        varOut_manual = candidate;
//        pivotRow_manual = row;
//        out_stat_new_manual = tmp_stat;
//        value_new_manual = bound;
//      }
//    }
//
//    // Could also intersect the bound on the ray first
//    if (varIn < copySolver->getNumCols()) {
//      double bound;
//      if (rayDirn > 0) {
//        bound = copySolver->getColUpper()[varIn];
//      } else {
//        bound = copySolver->getColLower()[varIn];
//      }
//
//      if (std::abs(bound) < copySolver->getInfinity() - EPS) { 
//        double dist = (bound - varVal[varIn]) / rayDirn;
//        if (dist < theta_manual) {
//          theta_manual = dist;
//          varOut_manual = varIn;
//          pivotRow_manual = -2;
//          // Moves to other bound
//          out_stat_new_manual = (-1 * (in_stat_old - 2)) + 1;
//          value_new_manual = bound;
//        }
//      }
//    }
//
//    if (theta_manual >= copySolver->getInfinity() - EPS) {
//      printf("\n## No pivot found manually ##\n");
//    } else {
//      printf("\n## Manually found pivot:\n");
//      printf(" in: %-*dstat_new: %-*dstat_old: %-*d\n", NUMSPACES, varIn, NUMSPACES, 1, NUMSPACES, in_stat_old);
//      printf("out: %-*dstat_new: %-*dvalue_new: %-*f\n", NUMSPACES, varOut_manual, NUMSPACES, out_stat_new_manual, NUMSPACES, value_new_manual);
//      printf("pivotRow: %-*d\n", NUMSPACES, pivotRow_manual);
//      printf("theta: %-*f\n", NUMSPACES, theta_manual);
//    }
//
//    printf("\n## Pivoting on variable %d with inStatus %d ##\n", varIn, inStatusForPivot);
//    //  model->setSequenceIn(varIn);
//    //  model->setDirectionIn(inStatusForPivot);
//    //  int return_code = model->primalPivotResult();
//    int in_stat_new = -1, varOut = -1, out_stat_new = -1, outStatusForPivot, pivotRow = -1;
//    double theta = -1;
//    int return_code = copySolver->primalPivotResult(varIn, inStatusForPivot, varOut, outStatusForPivot, theta, NULL);
//
//    printf("Return code: %d\n", return_code);
//    if (return_code >= 0) {
//      in_stat_new = model->getStatus(varIn);
//      varOut = model->sequenceOut(); // Because Osi uses some negative index for slacks
//      out_stat_new = model->getStatus(varOut);
//      // Switch out_stat if it is a slack
//      if (varOut >= copySolver->getNumCols()) {
//        out_stat_new = (out_stat_new == ClpSimplex::Status::atLowerBound) ? ClpSimplex::Status::atUpperBound : ClpSimplex::Status::atLowerBound;
//      }
//      pivotRow = model->pivotRow();
//      theta = model->theta();
//      theta *= inStatusForPivot;
//      double value_new = model->valueOut();
//      if (varOut >= copySolver->getNumCols()) {
//        value_new = copySolver->getRightHandSide()[varOut - copySolver->getNumCols()] - value_new;
//      }
//
//      printf(" in: %-*dstat_new: %-*dstat_old: %-*d\n", NUMSPACES, varIn, NUMSPACES, in_stat_new, NUMSPACES, in_stat_old);
//      printf("out: %-*dstat_new: %-*dvalue_new: %-*f\n", NUMSPACES, varOut, NUMSPACES, out_stat_new, NUMSPACES, value_new);
//      printf("outStatusForPivot: %-*d\n", NUMSPACES, outStatusForPivot);
//      printf("pivotRow: %-*d\n", NUMSPACES, pivotRow);
//      printf("theta: %-*f\n", NUMSPACES, theta);
//      printf("obj: %-*f\n", NUMSPACES, copySolver->getObjValue());
//
//      printf("\n## Confirm that pivot is equivalent to the manual one ##\n");
//      assert( std::abs(theta - theta_manual) < EPS );
//      double theta_check;
//      if (pivotRow >= 0) {
//        double tmpRayVal = BInvA[pivotRow] * rayDirn * -1; 
//        theta_check = (value_new - varVal[varOut]) / tmpRayVal;
//      } else {
//        theta_check = copySolver->getColUpper()[varOut] - copySolver->getColLower()[varOut];
//      }
//      if (std::abs( theta_check - theta ) < EPS) {
//        printf("Confirmed.\n");
//        numConfirmed++;
//      } else {
//        printf("\n*** ERROR: Unable to confirm equivalence of pivots. Theta_check: %f, theta: %f (do not match).\n", theta_check, theta); 
//      }
//
//      /*
//      if (in_stat_new != in_stat_old) { // Check to make sure a good pivot happened
//        printf("\n## Check that the same result happens when pivoting in the original model using Osi pivot ##\n");
//        ClpSimplex* copyModel = copySolver->getModelPtr();
//        assert(copyModel->getStatus(varIn) == in_stat_old);
//        int outStatusForPivot;
//        if (varOut < solver->getNumCols()) {
//          outStatusForPivot = (out_stat_new == ClpSimplex::Status::atUpperBound) ? 1 : -1;
//        } else {
//          outStatusForPivot = (solver->getRowSense()[varOut - solver->getNumCols()] == 'L') ? 1 : -1;
//        }
//        copySolver->enableSimplexInterface(true);
//        int copy_return_code = copySolver->pivot(varIn, varOut, outStatusForPivot);
//        printf("Copy return code: %d "
//            "(Return code is 0 for okay, "
//            "1 if inaccuracy forced re-factorization (should be okay) "
//            "and -1 for singular factorization)\n", copy_return_code);
//        if (copy_return_code >= 0) {
//          printf(" in: %-*dstat_new: %-*dstat_old: %d\n", NUMSPACES, varIn, NUMSPACES, copyModel->getStatus(varIn), in_stat_old);
//          printf("out: %-*dstat_new: %d\n", NUMSPACES, varOut, copyModel->getStatus(varOut));
//          printf("pivotRow: %d\n", copyModel->pivotRow());
//          printf("theta: %f\n", copyModel->theta());
//        } else {
//          printf("Singular factorization detected.\n");
//          exit(1);
//        }
//      } else {
//        printf("\n*** ERROR: in_stat_new and in_stat_old are the same, so no pivot happened! ##\n");
//      }
//      */
//
//      // Pivot back
//      /*
//      printf("copySolver->pivot(%d, %d, %d)\n", varIn, varOut, outStatusForPivot);
//      printf("copySolver->pivot(%d, %d, %d)\n", varOut, varIn, -1 * inStatusForPivot);
//      int back_return_code = copySolver->pivot(varOut, varIn, -1 * inStatusForPivot);
//      // Cannot use the below because of degeneracy
//      //int var_in_back, in_status_back;
//      //double theta_back;
//      // int back_return_code = solver->primalPivotResult(varOut, -1 * outStatusForPivot, var_in_back, in_status_back, theta_back, NULL);
//      if (back_return_code < 0) {
//        printf("\n*** ERROR: Unsuccessfully tried to pivot back ##\n");
//      }
//      */
//      copySolver = dynamic_cast<OsiClpSolverInterface*>(solver->clone());
//    } else {
//      printf("Unsuccessful pivot.\n");
//      exit(1);
//    }
//  }
//  printf("\n####\n");
//  printf("Finished processing all non-basic variables.\n");
//  printf("Number confirmed pivots: %d / %d.\n", numConfirmed, nbvar + 1);
//  printf("Exiting.\n");
//  return(0);
