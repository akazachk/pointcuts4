//============================================================================
// Name        : BBHelper.cpp
// Author      : akazachk
// Version     : 2017.03.16
// Description : Helper functions for branch-and-bound
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#include <cstdio> // for tmpnam

// For VPC
#include "BBHelper.hpp"
#include "VPCEventHandler.hpp"
#include "GlobalConstants.hpp"
#include "Utility.hpp"
#include "Output.hpp"
#include "CutHelper.hpp"
#include "CoinTime.hpp"
#include "CglSIC.hpp"

// Cbc
#include "CbcSolver.hpp"
#include "CbcTree.hpp"
#include "CbcBranchDefaultDecision.hpp"

// Cgl
#include "CglGomory.hpp"
#include "CglMixedIntegerRounding2.hpp"

#include "CbcStrategy.hpp"

void updateBestBBInfo(BBInfo& best_info, const BBInfo& curr_info, const bool first) {
  best_info.obj = first ? curr_info.obj : CoinMin(best_info.obj, curr_info.obj);
  best_info.bound = first ? curr_info.bound : CoinMax(best_info.bound, curr_info.bound);
  best_info.iters = first ? curr_info.iters : CoinMin(best_info.iters, curr_info.iters);
  best_info.nodes = first ? curr_info.nodes : CoinMin(best_info.nodes, curr_info.nodes);
  best_info.root_passes = first ? curr_info.root_passes : CoinMin(best_info.root_passes, curr_info.root_passes);
  best_info.first_cut_pass = first ? curr_info.first_cut_pass : CoinMax(best_info.first_cut_pass, curr_info.first_cut_pass);
  best_info.last_cut_pass = first ? curr_info.last_cut_pass : CoinMax(best_info.last_cut_pass, curr_info.last_cut_pass);
  best_info.root_time = first ? curr_info.root_time : CoinMin(best_info.root_time, curr_info.root_time);
  best_info.last_sol_time = first ? curr_info.last_sol_time : CoinMin(best_info.last_sol_time, curr_info.last_sol_time);
  best_info.time = first ? curr_info.time : CoinMin(best_info.time, curr_info.time);
} /* updateMinBBInfo */

void averageBBInfo(BBInfo& avg_info, const std::vector<BBInfo>& info) {
  for (BBInfo curr_info : info) {
    avg_info.obj += curr_info.obj;
    avg_info.bound += curr_info.bound;
    avg_info.iters += curr_info.iters;
    avg_info.nodes += curr_info.nodes;
    avg_info.root_passes += curr_info.root_passes;
    avg_info.first_cut_pass += curr_info.first_cut_pass;
    avg_info.last_cut_pass += curr_info.last_cut_pass;
    avg_info.root_time += curr_info.root_time;
    avg_info.last_sol_time += curr_info.last_sol_time;
    avg_info.time += curr_info.time;
  }
  const int num_bb_runs = info.size();
  avg_info.obj /= num_bb_runs;
  avg_info.bound /= num_bb_runs;
  avg_info.iters /= num_bb_runs;
  avg_info.nodes /= num_bb_runs;
  avg_info.root_passes /= num_bb_runs;
  avg_info.first_cut_pass /= num_bb_runs;
  avg_info.last_cut_pass /= num_bb_runs;
  avg_info.root_time /= num_bb_runs;
  avg_info.last_sol_time /= num_bb_runs;
  avg_info.time /= num_bb_runs;
} /* averageBBInfo */

void printBBInfo(const BBInfo& info, FILE* myfile, const bool print_blanks, const char SEP) {
  if (!print_blanks) {
    fprintf(myfile, "%s%c", stringValue(info.obj, "%.20f").c_str(), SEP);
    fprintf(myfile, "%s%c", stringValue(info.bound, "%.20f").c_str(), SEP);
    fprintf(myfile, "%ld%c", info.iters, SEP);
    fprintf(myfile, "%ld%c", info.nodes, SEP);
    fprintf(myfile, "%ld%c", info.root_passes, SEP);
    fprintf(myfile, "%.20f%c", info.first_cut_pass, SEP);
    fprintf(myfile, "%.20f%c", info.last_cut_pass, SEP);
    fprintf(myfile, "%2.3f%c", info.root_time, SEP);
    fprintf(myfile, "%2.3f%c", info.last_sol_time, SEP);
    fprintf(myfile, "%2.3f%c", info.time, SEP);
  } else {
    for (int i = 0; i < (int) BB_INFO_CONTENTS.size(); i++) {
      fprintf(myfile, "%c", SEP);
    }
  }
} /* printBBInfo */

void printBBInfo(const BBInfo& info_mycuts, const BBInfo& info_allcuts,
    FILE* myfile, const bool print_blanks, const char SEP) {
  if (!print_blanks) {
    fprintf(myfile, "%s%c", stringValue(info_mycuts.obj, "%.20f").c_str(), SEP);
    fprintf(myfile, "%s%c", stringValue(info_allcuts.obj, "%.20f").c_str(), SEP);
    fprintf(myfile, "%s%c", stringValue(info_mycuts.bound, "%.20f").c_str(), SEP);
    fprintf(myfile, "%s%c", stringValue(info_allcuts.bound, "%.20f").c_str(), SEP);
    fprintf(myfile, "%ld%c", info_mycuts.iters, SEP);
    fprintf(myfile, "%ld%c", info_allcuts.iters, SEP);
    fprintf(myfile, "%ld%c", info_mycuts.nodes, SEP);
    fprintf(myfile, "%ld%c", info_allcuts.nodes, SEP);
    fprintf(myfile, "%ld%c", info_mycuts.root_passes, SEP);
    fprintf(myfile, "%ld%c", info_allcuts.root_passes, SEP);
    fprintf(myfile, "%.20f%c", info_mycuts.first_cut_pass, SEP);
    fprintf(myfile, "%.20f%c", info_allcuts.first_cut_pass, SEP);
    fprintf(myfile, "%.20f%c", info_mycuts.last_cut_pass, SEP);
    fprintf(myfile, "%.20f%c", info_allcuts.last_cut_pass, SEP);
    fprintf(myfile, "%2.3f%c", info_mycuts.root_time, SEP);
    fprintf(myfile, "%2.3f%c", info_allcuts.root_time, SEP);
    fprintf(myfile, "%2.3f%c", info_mycuts.last_sol_time, SEP);
    fprintf(myfile, "%2.3f%c", info_allcuts.last_sol_time, SEP);
    fprintf(myfile, "%2.3f%c", info_mycuts.time, SEP);
    fprintf(myfile, "%2.3f%c", info_allcuts.time, SEP);
  } else {
    for (int i = 0; i < (int) BB_INFO_CONTENTS.size() * 2; i++) {
      fprintf(myfile, "%c", SEP);
    }
  }
} /* printBBInfo */

void createStringFromBBInfoVec(const std::vector<BBInfo>& vec_info,
    std::vector<std::string>& vec_str) {
  vec_str.resize(BB_INFO_CONTENTS.size());
  for (BBInfo info : vec_info) {
    vec_str[OBJ_BB_INFO_IND] += (!vec_str[OBJ_BB_INFO_IND].empty() ? ";" : "") + stringValue(info.obj, "%.20f");
    vec_str[BOUND_BB_INFO_IND] += (!vec_str[BOUND_BB_INFO_IND].empty() ? ";" : "") + stringValue(info.bound, "%.20f");
    vec_str[ITERS_BB_INFO_IND] += (!vec_str[ITERS_BB_INFO_IND].empty() ? ";" : "") + std::to_string(info.iters);
    vec_str[NODES_BB_INFO_IND] += (!vec_str[NODES_BB_INFO_IND].empty() ? ";" : "") + std::to_string(info.nodes);
    vec_str[ROOT_PASSES_BB_INFO_IND] += (!vec_str[ROOT_PASSES_BB_INFO_IND].empty() ? ";" : "") + std::to_string(info.root_passes);
    vec_str[FIRST_CUT_PASS_BB_INFO_IND] += (!vec_str[FIRST_CUT_PASS_BB_INFO_IND].empty() ? ";" : "") + std::to_string(info.first_cut_pass);
    vec_str[LAST_CUT_PASS_BB_INFO_IND] += (!vec_str[LAST_CUT_PASS_BB_INFO_IND].empty() ? ";" : "") + std::to_string(info.last_cut_pass);
    vec_str[ROOT_TIME_BB_INFO_IND] += (!vec_str[ROOT_TIME_BB_INFO_IND].empty() ? ";" : "") + std::to_string(info.root_time);
    vec_str[LAST_SOL_TIME_BB_INFO_IND] += (!vec_str[LAST_SOL_TIME_BB_INFO_IND].empty() ? ";" : "") + std::to_string(info.last_sol_time);
    vec_str[TIME_BB_INFO_IND] += (!vec_str[TIME_BB_INFO_IND].empty() ? ";" : "") + std::to_string(info.time);
  }
} /* createStringFromBBInfoVec */

/**
 * Creates temporary file (in /tmp) so that it can be read by a different solver
 * It does not delete the file
 */
void createTmpFileCopy(const OsiSolverInterface* const solver, std::string& f_name) {
  // Generate temporary file name
  char template_name[] = "/tmp/tmpmpsXXXXXX";

  mktemp(template_name);
  f_name = template_name;
  if (f_name.empty()) {
    error_msg(errorstring, "Could not generate temp file.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }
  solver->writeMps(template_name, "mps", solver->getObjSense());
  f_name += ".mps.gz"; // writeMps calls writeMpsNative, which invokes the CoinMpsIO writer with gzip option = 1
  //doBranchAndBoundWithCplex(f_name.c_str(), bb_opt, bb_iters, bb_nodes, bb_time);
  //remove(f_name.c_str());
} /* createTmpFileCopy (Osi) */

void setStrategyForBBTestCbc(CbcModel* const cbc_model,
    const int seed = GlobalVariables::random_seed) {
  // Parameters that should always be set
  cbc_model->setMaximumSeconds(param.getBB_TIMELIMIT()); // time limit
  cbc_model->setRandomSeed(seed); // random seed

  int strategy = param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND);
  if (strategy <= 0) {
    // Default strategy
    CbcStrategyDefault strategy;
    cbc_model->setStrategy(strategy);
    /*
    CbcStrategyDefault strategy(-1);
    strategy.setupPreProcessing(-1,0);
    cbc_model->setStrategy(strategy);
    cbc_model->setMaximumCutPassesAtRoot(0);
    cbc_model->setMaximumCutPasses(0);
    cbc_model->setWhenCuts(0);
    */
  } else {
    if (strategy & BB_Strategy_Options::user_cuts) {
      // Make sure dual reductions are off
    }

    // Turn off all cuts
    if (strategy & BB_Strategy_Options::all_cuts_off) {
      cbc_model->setMaximumCutPassesAtRoot(0);
      cbc_model->setMaximumCutPasses(0);
      cbc_model->setWhenCuts(0);
    }

    // Presolve
    if (strategy & BB_Strategy_Options::presolve_off) {
      cbc_model->setTypePresolve(0);
    }
  }

  // Check if we should use strong branching
  // Not sure this works when using StrategyDefault as well...
  if (std::abs(strategy) & BB_Strategy_Options::strong_branching_on) {
    CbcBranchDefaultDecision branch;
    OsiChooseStrong choose;
    choose.setNumberStrong(cbc_model->solver()->getNumCols());
    choose.setNumberBeforeTrusted(std::numeric_limits<int>::max());
    branch.setChooseMethod(choose);
    cbc_model->setBranchingMethod(branch);
  }
} /* setStrategyForBBTestCbc */

/************************************************************/
/**
 * Perform branch-and-bound without cuts
 */
void doBranchAndBoundNoCuts(const OsiSolverInterface* const solver,
    BBInfo& info) {
#ifdef TRACE
  printf("\nBB with no cuts.\n");
#endif
  const bool test_using_main = false;

  // Set up solver
  PointCutsSolverInterface* BBSolver;
  BBSolver = dynamic_cast<PointCutsSolverInterface*>(solver->clone());
  setClpParameters(BBSolver);

  // Set up model
  CbcModel* cbc_model = new CbcModel;
  cbc_model->swapSolver(BBSolver);
  cbc_model->setModelOwnsSolver(true);
  setMessageHandler(cbc_model);

  // Set up options and run B&B
  if (!test_using_main) {
    setStrategyForBBTestCbc(cbc_model);
#ifdef TRACE
    cbc_model->branchAndBound(3);
#else
    cbc_model->branchAndBound(0);
#endif
  } else {
    CbcMain0(*cbc_model);
    std::string name, logLevel, presolveOnOff, preprocessOnOff, cutsOnOff, heurOnOff, solveOption;
    name = "BBHelper_doBranchAndBoundNoCuts";
    presolveOnOff = "-presolve=off";
    preprocessOnOff = "-preprocess=off";
    cutsOnOff = "-cuts=off";
    heurOnOff = "-heur=off";
    solveOption = "-solve";
#ifdef TRACE
    logLevel = "-loglevel=3";
#else
    logLevel = "-loglevel=0";
#endif

    int argc = 0;
    const char** cbc_options = new const char*[20];
    cbc_options[argc++] = name.c_str();
    cbc_options[argc++] = logLevel.c_str();
    cbc_options[argc++] = presolveOnOff.c_str();
    cbc_options[argc++] = preprocessOnOff.c_str();
    cbc_options[argc++] = cutsOnOff.c_str();
    cbc_options[argc++] = heurOnOff.c_str();
    cbc_options[argc++] = solveOption.c_str();

    CbcMain1(argc, cbc_options, *cbc_model);
    delete[] cbc_options;
  }
  
  // Collect statistics
  info.time = CoinCpuTime()
      - cbc_model->getDblParam(CbcModel::CbcStartSeconds);
  info.obj = cbc_model->getObjValue();
  info.iters = cbc_model->getIterationCount();
  info.nodes = cbc_model->getNodeCount();

  // Free
  if (cbc_model) {
    delete cbc_model;
    cbc_model = NULL;
  }
} /* doBranchAndBoundNoCuts */

/************************************************************/
/**
 * Perform branch-and-bound using the given cuts, perhaps doing cut selection
 */
void doBranchAndBoundYesCuts(const OsiSolverInterface* const solver,
    BBInfo& info, AdvCuts& structCuts,
    const bool doCutSelection, const int numCutsToAddPerRound,
    const int maxRounds, const std::string logstring) {
//#ifdef TRACE
  printf("%s", logstring.c_str());
//#endif
  const bool test_using_main = false;

  // Check that there are cuts
  const int numCuts = structCuts.sizeCuts();
  if (numCuts == 0) {
    info.nodes = 0;
    info.time = 0.;
    return;
  }

  // Set up solver
  PointCutsSolverInterface* BBSolver;
  BBSolver = dynamic_cast<PointCutsSolverInterface*>(solver->clone());
  setClpParameters(BBSolver);
  
  // Apply cuts
  if (doCutSelection) {
    applyCutsInRounds(BBSolver, structCuts, numCutsToAddPerRound, maxRounds);
  } else {
    applyCuts(BBSolver, structCuts);
  }

  // Set up model
  CbcModel* cbc_model = new CbcModel;
  cbc_model->swapSolver(BBSolver);
  cbc_model->setModelOwnsSolver(true);
  setMessageHandler(cbc_model);

  // Set up options and run B&B
  if (!test_using_main) {
    setStrategyForBBTestCbc(cbc_model);
#ifdef TRACE
    cbc_model->branchAndBound(3);
#else
    cbc_model->branchAndBound(0);
#endif
  } else {
    CbcMain0(*cbc_model);
    std::string name, logLevel, presolveOnOff, preprocessOnOff, cutsOnOff, heurOnOff, solveOption;
    name = "BBHelper_doBranchAndBoundYesCuts";
    presolveOnOff = "-presolve=off";
    preprocessOnOff = "-preprocess=off";
    cutsOnOff = "-cuts=off";
    heurOnOff = "-heur=off";
    solveOption = "-solve";
#ifdef TRACE
    logLevel = "-loglevel=3";
#else
    logLevel = "-loglevel=0";
#endif

    int argc = 0;
    const char** cbc_options = new const char*[20];
    cbc_options[argc++] = name.c_str();
    cbc_options[argc++] = logLevel.c_str();
    cbc_options[argc++] = presolveOnOff.c_str();
    cbc_options[argc++] = preprocessOnOff.c_str();
    cbc_options[argc++] = cutsOnOff.c_str();
    cbc_options[argc++] = heurOnOff.c_str();
    cbc_options[argc++] = solveOption.c_str();

    CbcMain1(argc, cbc_options, *cbc_model);
    delete[] cbc_options;
  }

  // Collect statistics
  info.time = CoinCpuTime()
      - cbc_model->getDblParam(CbcModel::CbcStartSeconds);
  info.obj = cbc_model->getObjValue();
  info.iters = cbc_model->getIterationCount();
  info.nodes = cbc_model->getNodeCount();

  // Free
  if (cbc_model) {
    delete cbc_model;
    cbc_model = NULL;
  }
} /* doBranchAndBoundYesCuts */

/************************************************************/
/**
 * Perform branch-and-bound on given solver and collect statistics
 */
/*
void addAllCutsToBranchAndBound(const OsiSolverInterface* const solver,
    AdvCuts& structSICs, AdvCuts& structGMICuts, AdvCuts& structLandPCuts,
    AdvCuts& structTilted, AdvCuts& structVPCs, AdvCuts& structPHAs,
    int& bb_nodes_nocuts, double& bb_time_nocuts, int& bb_nodes_sics,
    double& bb_time_sics, int& bb_nodes_allcuts, double& bb_time_allcuts) {
#ifdef TRACE
  printf("\n## Performing branch-and-bound. ##\n");
#endif
  const bool test_using_main = false;

  const int numSICs = structSICs.sizeCuts();
//  const int numGMICuts = structGMICuts.sizeCuts();
//  const int numLandPCuts = structLandPCuts.sizeCuts();
  const int numTilted = structTilted.sizeCuts();
  const int numVPCs = structVPCs.sizeCuts();
  const int numPHA = structPHAs.sizeCuts();

#ifdef WRITE_LP_WITH_CUTS
  char filename[300];
#endif

  PointCutsSolverInterface* BBSolver;
  BBSolver = dynamic_cast<PointCutsSolverInterface*>(solver->clone());
  setMessageHandler(BBSolver);

  CbcModel* cbc_model = new CbcModel;
  cbc_model->swapSolver(BBSolver);
  cbc_model->setModelOwnsSolver(true);
  setMessageHandler(cbc_model);

  if (!test_using_main) {
  } else {
    CbcMain0(*cbc_model);
    const char* cbc_options[] = { "CutHelper_doBranchAndBound", "-cuts=off",
        "-solve", "-quit" };

    //Stats BBTimeStats;
    //BBTimeStats.register_name("No cuts");
    //BBTimeStats.register_name("SICs");
    //BBTimeStats.register_name("All cuts");

#ifdef TRACE
    printf("\nBB with no cuts.\n");
#endif
    cbc_model->saveReferenceSolver();
    //BBTimeStats.start_timer("No cuts");
    CbcMain1(4, cbc_options, *cbc_model);
    //cbc_model->branchAndBound();
    bb_time_nocuts = CoinCpuTime()
        - cbc_model->getDblParam(CbcModel::CbcStartSeconds);
    //BBTimeStats.end_timer("No cuts");
    //bb_time_nocuts = BBTimeStats.get_time("No cuts");
    bb_nodes_nocuts = cbc_model->getNodeCount();
    cbc_model->resetToReferenceSolver();
    BBSolver = dynamic_cast<OsiClpSolverInterface*>(cbc_model->solver());

    if (numSICs > 0) {
#ifdef TRACE
      printf("\nBB with SICs only.\n");
#endif
      applyCuts(BBSolver, structSICs);

#ifdef WRITE_LP_WITH_CUTS
      snprintf(filename, sizeof(filename) / sizeof(char), "%s%s",
          GlobalVariables::out_f_name_stub.c_str(), "_SICs");
      BBSolver->writeMps(filename, "mps", BBSolver->getObjSense());
#endif

//    CbcModel cbc_model_sics(*BBSolver);
//    setMessageHandler(&cbc_model_sics);
//    cbc_model_sics.branchAndBound();
//    bb_nodes_sics = cbc_model_sics.getNodeCount();
//    bb_time_sics = CoinCpuTime()
//        - cbc_model_sics.getDblParam(CbcModel::CbcStartSeconds);
      cbc_model->swapSolver(BBSolver);
      cbc_model->saveReferenceSolver();
      //BBTimeStats.start_timer("SICs");
      bb_time_sics = CoinCpuTime();
      CbcMain1(4, cbc_options, *cbc_model);
      //cbc_model->branchAndBound();
      bb_time_sics = CoinCpuTime() - bb_time_sics;
      //BBTimeStats.end_timer("SICs");
      //bb_time_sics = BBTimeStats.get_time("SICs");
      bb_nodes_sics = cbc_model->getNodeCount();
      cbc_model->resetToReferenceSolver();
      BBSolver = dynamic_cast<OsiClpSolverInterface*>(cbc_model->solver());
    } else {
      bb_nodes_sics = bb_nodes_nocuts;
      bb_time_sics = bb_time_nocuts;
    }

    if (numTilted + numVPCs + numPHA > 0) {
#ifdef TRACE
      printf("\nBB with all cuts.\n");
#endif
      applyCuts(BBSolver, structTilted);
      applyCuts(BBSolver, structVPCs);
      applyCuts(BBSolver, structPHAs);

#ifdef WRITE_LP_WITH_CUTS
      snprintf(filename, sizeof(filename) / sizeof(char), "%s%s",
          GlobalVariables::out_f_name_stub.c_str(), "_AllCuts");
      BBSolver->writeMps(filename, "mps", BBSolver->getObjSense());
#endif

//    CbcModel cbc_model_allcuts(*BBSolver);
//    setMessageHandler(&cbc_model_allcuts);
//    cbc_model_allcuts.branchAndBound();
//    bb_nodes_allcuts = cbc_model_allcuts.getNodeCount();
//    bb_time_allcuts = CoinCpuTime()
//        - cbc_model_allcuts.getDblParam(CbcModel::CbcStartSeconds);
      cbc_model->swapSolver(BBSolver);
      cbc_model->saveReferenceSolver();
      //BBTimeStats.start_timer("All cuts");
      bb_time_allcuts = CoinCpuTime();
      //cbc_model->branchAndBound();
      CbcMain1(4, cbc_options, *cbc_model);
      bb_time_allcuts = CoinCpuTime() - bb_time_allcuts;
      //BBTimeStats.end_timer("All cuts");
      //bb_time_allcuts = BBTimeStats.get_time("All cuts");
      bb_nodes_allcuts = cbc_model->getNodeCount();
      cbc_model->resetToReferenceSolver();
      BBSolver = dynamic_cast<OsiClpSolverInterface*>(cbc_model->solver());
    } else {
      bb_nodes_allcuts = bb_nodes_sics;
      bb_time_allcuts = bb_time_sics;
    }
  }

  // Free
  cbc_model->setModelOwnsSolver(true);
  if (cbc_model) {
    //if (cbc_model->continuousSolver()) {
    //  cbc_model->clearContinuousSolver();
    //}
    delete cbc_model;
    cbc_model = NULL;
  }
}*/ /* addAllCutsToBranchAndBound */

/************************************************************/
/**
 * Generate a partial branch-and-bound tree with at most max_leaf_nodes leaf nodes 
 */
void generatePartialBBTree(CbcModel* cbc_model,
    const OsiSolverInterface* const solver, const int max_leaf_nodes,
    const int num_strong, const int num_before_trusted) {
  bool test_using_main = false; //param.getTEMP();
  //  double partial_timelimit = 100 * max_leaf_nodes * solver->getNumCols()
  //      * GlobalVariables::timeStats.get_time(GlobalConstants::INIT_SOLVE_TIME);
  const double partial_timelimit = param.getPARTIAL_BB_TIMELIMIT(); //1000; // will be checked manually by the eventHandler
   
  // Set up options
  VPCEventHandler* eventHandler = new VPCEventHandler(max_leaf_nodes, partial_timelimit);
  eventHandler->setOriginalSolver(solver);

  // This sets branching decision, event handling, etc.
  setCbcParametersForPartialBB(cbc_model, eventHandler, num_strong,
      num_before_trusted, std::numeric_limits<double>::max());

  if (!test_using_main) {
#ifdef TRACE
    cbc_model->branchAndBound(3);
#else
    cbc_model->branchAndBound(0);
#endif
  } else {
    CbcMain0(*cbc_model);
    std::string name, logLevel, presolveOnOff, preprocessOnOff, cutsOnOff, heurOnOff, solveOption;
    name = "BBHelper_generatePartialBBTree";
    presolveOnOff = "-presolve=off";
    preprocessOnOff = "-preprocess=off";
    cutsOnOff = "-cuts=off";
    heurOnOff = "-heur=off";
    solveOption = "-solve";
#ifdef TRACE
    logLevel = "-loglevel=3";
#else
    logLevel = "-loglevel=0";
#endif
    //const char* cbc_options[]={"BBHelper_generatePartialBBTree","-loglevel=3","-presolve=off","-preprocess=off","-cuts=off","-feasibilitypump=off","-rins=off","-divingcoeff=off","-solve","-quit"};
    //const char* cbc_options[]={"BBHelper_generatePartialBBTree","-loglevel=3","-presolve=off","-preprocess=off","-cuts=off","-heur=off","-solve"};
    //const char* cbc_options[]={"BBHelper_generatePartialBBTree","-loglevel=0","-presolve=off","-preprocess=off","-cuts=off","-feasibilitypump=off","-rins=off","-divingcoeff=off","-solve","-quit"};
    //const char* cbc_options[]={"BBHelper_generatePartialBBTree","-loglevel=0","-presolve=off","-preprocess=off","-cuts=off","-heur=off","-solve"};

    int argc = 0;
    const char** cbc_options = new const char*[20];
    cbc_options[argc++] = name.c_str();
    cbc_options[argc++] = logLevel.c_str();
    cbc_options[argc++] = presolveOnOff.c_str();
    cbc_options[argc++] = preprocessOnOff.c_str();
    cbc_options[argc++] = cutsOnOff.c_str();
    cbc_options[argc++] = heurOnOff.c_str();
    cbc_options[argc++] = solveOption.c_str();

    CbcMain1(argc, cbc_options, *cbc_model);
    delete[] cbc_options;
  }

  // Free
  // When eventHandler is passed, Cbc currently clones it and does not delete the original
  if (eventHandler) { // in case this behavior gets changed in the future
    delete eventHandler;
  }
} /* generatePartialBBTree */

// CPLEX
#ifdef VPC_USE_CPLEX
// C interface 
#include <ilcplex/cplexx.h>

/**
 * Creates temporary file (in /tmp) so that it can be read by a different solver
 * It does not delete the file
 */
void createTmpFileCopy(CPXENVptr& env, CPXLPptr& lp, std::string& f_name) {
  if (f_name.empty()) {
    // Generate temporary file name
    char template_name[] = "/tmp/tmpmpsXXXXXX";

    mktemp(template_name);
    f_name = template_name;
    if (f_name.empty()) {
      error_msg(errorstring, "Could not generate temp file.\n");
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }
    f_name += ".mps.gz";
  }
  CPXXwriteprob(env, lp, f_name.c_str(), NULL);
} /* createTmpFileCopy (Cplex) */

void setStrategyForBBTestCplexCallable(CPXENVptr& env,
    const int seed = GlobalVariables::random_seed,
    const double best_bound = GlobalVariables::bestObjValue) {
  int status = 0;

  // Parameters that should always be set
  status += CPXXsetdblparam(env, CPXPARAM_TimeLimit, GlobalVariables::param.getBB_TIMELIMIT()); // time limit
  status += CPXXsetlongparam(env, CPXPARAM_Threads, 1); // single-threaded
  status += CPXXsetintparam(env, CPXPARAM_RandomSeed, seed); // random seed

#ifndef TRACE
  status += CPXXsetintparam(env, CPXPARAM_ScreenOutput, CPX_OFF);
#endif
#ifdef TRACE
  status += CPXXsetintparam(env, CPXPARAM_ScreenOutput, CPX_ON);
  status += CPXXsetintparam(env, CPXPARAM_MIP_Interval, 1);
#endif

  int strategy = param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND);
  if (strategy <= 0) {
    // Default strategy
  } else {
    if (strategy & BB_Strategy_Options::user_cuts) {
      status += CPXXsetlongparam(env, CPXPARAM_MIP_Strategy_Search, CPX_MIPSEARCH_TRADITIONAL); // disable dynamic search
      status += CPXXsetlongparam(env, CPXPARAM_Preprocessing_Linear, 0); // disable this so that presolve does not discard user cuts during preprocessing
      status += CPXXsetlongparam(env, CPXPARAM_Preprocessing_Reduce, CPX_PREREDUCE_PRIMALONLY); // disable dual reductions
    }

    // Turn off all cuts
    if (strategy & BB_Strategy_Options::all_cuts_off) {
      //status += CPXXsetdblparam(env, CPXPARAM_MIP_Limits_CutsFactor, 0);
      //status += CPXXsetlongparam(env, CPXPARAM_MIP_Limits_CutPasses, -1);
      //status += CPXXsetlongparam(env, CPXPARAM_MIP_Limits_EachCutLimit, 0); // all but Gomory
      status += CPXXsetlongparam(env, CPXPARAM_MIP_Cuts_BQP, -1);
      status += CPXXsetlongparam(env, CPXPARAM_MIP_Cuts_Cliques, -1);
      status += CPXXsetlongparam(env, CPXPARAM_MIP_Cuts_Covers, -1);
      status += CPXXsetlongparam(env, CPXPARAM_MIP_Cuts_Disjunctive, -1);
      status += CPXXsetlongparam(env, CPXPARAM_MIP_Cuts_FlowCovers, -1);
      status += CPXXsetlongparam(env, CPXPARAM_MIP_Cuts_PathCut, -1);
      status += CPXXsetlongparam(env, CPXPARAM_MIP_Cuts_Gomory, -1);
      status += CPXXsetlongparam(env, CPXPARAM_MIP_Cuts_GUBCovers, -1);
      status += CPXXsetlongparam(env, CPXPARAM_MIP_Cuts_Implied, -1);
      status += CPXXsetlongparam(env, CPXPARAM_MIP_Cuts_LocalImplied, -1);
      status += CPXXsetlongparam(env, CPXPARAM_MIP_Cuts_LiftProj, -1);
      status += CPXXsetlongparam(env, CPXPARAM_MIP_Cuts_MIRCut, -1);
      status += CPXXsetlongparam(env, CPXPARAM_MIP_Cuts_MCFCut, -1);
      status += CPXXsetlongparam(env, CPXPARAM_MIP_Cuts_RLT, -1);
      status += CPXXsetlongparam(env, CPXPARAM_MIP_Cuts_ZeroHalfCut, -1);
      //    status += CPXXsetlongparam(env, CPXPARAM_MIP_Limits_GomoryPass, 0);
      //    status += CPXXsetlongparam(env, CPXPARAM_MIP_Limits_GomoryCand, 0);
    }

    // Presolve
    if (strategy & BB_Strategy_Options::presolve_off) {
      status += CPXXsetlongparam(env, CPXPARAM_Preprocessing_Presolve, 0); // turn off presolve overall
      status += CPXXsetlongparam(env, CPXPARAM_MIP_Strategy_PresolveNode, 0); // turn off presolve at nodes (not turned off by above?)
      status += CPXXsetlongparam(env, CPXPARAM_Preprocessing_Relax, 0); // turn off LP presolve at root
      status += CPXXsetlongparam(env, CPXPARAM_MIP_Strategy_Probe, -1); // turn off probing
    }

    // Heuristics
    if (strategy & BB_Strategy_Options::heuristics_off) {
      status += CPXXsetlongparam(env, CPXPARAM_MIP_Strategy_HeuristicFreq, -1); // turn off the "periodic" heuristic
      status += CPXXsetlongparam(env, CPXPARAM_MIP_Strategy_FPHeur, -1); // turn off feasibility pump
      status += CPXXsetlongparam(env, CPXPARAM_MIP_Strategy_LBHeur, 0); // local branching heuristic (default is off anyway)
      status += CPXXsetlongparam(env, CPXPARAM_MIP_Strategy_RINSHeur, 0); // turn off RINS
    }

    // Feed the solver the best bound provided
    if (strategy & BB_Strategy_Options::use_best_bound) {
      if (!isInfinity(best_bound)) {
        status += CPXXsetdblparam(env, CPXPARAM_MIP_Tolerances_UpperCutoff, best_bound + 1e-3); // give the solver the best IP objective value (it is a minimization problem) with a tolerance
      }
    }
  } /* else, strategy > 0 */

  // Check if we should use strong branching
  if (std::abs(strategy) & BB_Strategy_Options::strong_branching_on) {
    status += CPXXsetintparam(env, CPXPARAM_MIP_Strategy_VariableSelect, CPX_VARSEL_STRONG);
    status += CPXXsetintparam(env, CPXPARAM_MIP_Limits_StrongCand, CPX_BIGINT); // smaller than max int
    //std::numeric_limits<int>::max());
    status += CPXXsetlongparam(env, CPXPARAM_MIP_Limits_StrongIt, CPX_BIGLONG); // smaller than max long 
        //std::numeric_limits<int>::max());
    status += CPXXsetlongparam(env, CPXPARAM_MIP_Strategy_BBInterval, 1); // always select best bound node
  }

  // Check that there are no errors
  if (status) {
    error_msg(errorstring, "CPLEX (C): Error occurred when setting parameters. Status: %d.\n", status);
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }
} /* setStrategyForBBTestCplexCallable */

void readFileIntoCplexCallable(const char* f_name, CPXENVptr& env, CPXLPptr& lp) {
  int status = 0;

  /* Initialize the CPLEX environment */
  env = CPXXopenCPLEX (&status);

  /* If an error occurs, the status value indicates the reason for
     failure.  A call to CPXXgeterrorstring will produce the text of
     the error message.  Note that CPXXopenCPLEX produces no output,
     so the only way to see the cause of the error is to use
     CPXXgeterrorstring.  For other CPLEX routines, the errors will
     be seen if the CPXPARAM_ScreenOutput indicator is set to CPX_ON.  */
  if ( env == NULL ) {
    char errmsg[CPXMESSAGEBUFSIZE];
    CPXXgeterrorstring (env, status, errmsg);
    error_msg(errorstring, "CPLEX (C): Could not open CPLEX environment. Error: %s", errmsg);
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  /* Create the problem, using the filename as the problem name */
  lp = CPXXcreateprob (env, &status, f_name);
   
  /* A returned pointer of NULL may mean that not enough memory
     was available or there was some other problem.  In the case of 
     failure, an error message will have been written to the error 
     channel from inside CPLEX.  In this example, the setting of
     the parameter CPXXPARAM_ScreenOutput causes the error message to
     appear on stdout.  Note that most CPLEX routines return
     an error code to indicate the reason for failure.   */
  if ( lp == NULL ) {
    error_msg(errorstring, "CPLEX (C): Failed to create the LP.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  /* Now read the file, and copy the data into the created lp */
  status = CPXXreadcopyprob (env, lp, f_name, NULL);
  if ( status ) {
    error_msg(errorstring, "CPLEX (C): Failed to read and copy the problem data.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }
} /* readFileIntoCplexCallable */

void presolveModelWithCplexCallable(CPXENVptr& env, CPXLPptr& lp, double& presolved_opt, std::string& presolved_name) {
#ifdef TRACE
  printf("\n## CPLEX (C): Presolving model ##\n");
#endif
  //warning_msg(warnstring, "CPLEX presolve saving not currently working.\n");
  //return; // Currently not functioning
  const int strategy = param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND);
  param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, BB_Strategy_Options::presolve_on);
  setStrategyForBBTestCplexCallable(env);
  param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, strategy);

  int status = 0;
   
  // Save presolved model
  if (presolved_name.empty()) {
    char template_name[] = "/tmp/tmpmpsXXXXXX"; // generate temporary file name
    mktemp(template_name);
    presolved_name = template_name;
  }
  if (presolved_name.empty()) {
    error_msg(errorstring, "Could not generate temp file.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  double objoff; // objective offset between the original and presolved models
  status = CPXXpreslvwrite(env, lp, (presolved_name + ".pre").c_str(), &objoff);
  if (status) {
    error_msg(errorstring, "CPLEX (C): Error occurred during call to CPXXpreslvwrite.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  CPXENVptr new_env = NULL;
  CPXLPptr new_lp = NULL;
  // Initialize the CPLEX environment
  new_env = CPXXopenCPLEX(&status);
  if ( new_env == NULL ) {
    char errmsg[CPXMESSAGEBUFSIZE];
    CPXXgeterrorstring (new_env, status, errmsg);
    error_msg(errorstring, "CPLEX (C): Could not open CPLEX environment. Error: %s", errmsg);
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  // Create the problem, using the correct problem name
  new_lp = CPXXcreateprob(new_env, &status, GlobalVariables::prob_name.c_str());
  if ( new_lp == NULL ) {
    error_msg(errorstring, "CPLEX (C): Failed to create the LP.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  // Now read the file, and copy the data into the created lp
  status = CPXXreadcopyprob(new_env, new_lp, (presolved_name + ".pre").c_str(), NULL);
  if (status) {
    char errmsg[CPXMESSAGEBUFSIZE];
    CPXXgeterrorstring (new_env, status, errmsg);
    error_msg(errorstring, "CPLEX (C): Error occurred during reading in of presolved file. Error: %s", errmsg);
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  // Write the problem to an mps file format
  presolved_name += ".mps.gz";
  status = CPXXwriteprob(new_env, new_lp, presolved_name.c_str(), NULL);
  if (status) {
    error_msg(errorstring, "CPLEX (C): Error occurred during writing of presolved file to mps file.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  // Change to an lp to get the presolved lp optimum
  status = CPXXchgprobtype(new_env, new_lp, CPXPROB_LP);
  if (status) {
    error_msg(errorstring, "CPLEX (C): Error occurred during changing problem type.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  // Optimize the lp
  status = CPXXlpopt(new_env, new_lp);
  if (status) {
    error_msg(errorstring, "CPLEX (C): Error occurred during solving presolved lp.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  // Ensure that we are optimal
  int optimstatus = CPXXgetstat(new_env, new_lp);
  if (optimstatus != CPX_STAT_OPTIMAL) {
    error_msg(errorstring, "CPLEX (C): Error occurred during solving presolved lp.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  // Save the presolved objective value
  status = CPXXgetobjval(new_env, new_lp, &presolved_opt);
  if (status) {
    error_msg(errorstring, "CPLEX (C): Unable to get presolved objective value.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  // Free up the problem as allocated by CPXXcreateprob, if necessary
  if ( new_lp != NULL ) {
    status = CPXXfreeprob (new_env, &new_lp);
    if ( status ) {
      error_msg(errorstring, "CPLEX (C): CPXXfreeprob failed, error code %d.\n", status);
      writeErrorToLog(errorstring, GlobalVariables::log_file);
    }
  }

  // Free up the CPLEX new_environment, if necessary
  if ( new_env != NULL ) {
    status = CPXXcloseCPLEX (&new_env);
    if ( status ) {
      char errmsg[CPXMESSAGEBUFSIZE];
      CPXXgeterrorstring (new_env, status, errmsg);
      error_msg(errorstring, "CPLEX (C): Could not close CPLEX environment. Error: %s", errmsg);
      writeErrorToLog(errorstring, GlobalVariables::log_file);
    }
  }
  /* CPXCLPptr presolved;
  CPXXgetredlp(env, lp, &presolved);
  if (presolved) {
    //CPXXpreslvwrite
   //CPXwriteprob(env, presolved, ...);
  } else {
    error_msg(errorstring, "CPLEX (C): Not able to get presolved model from CPLEX.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }*/
  /*status = CPXXpresolve(env, lp, CPX_ALG_NONE); // CPX_ALG_NONE should be used for MIP
    if (status) {
    error_msg(errorstring, "CPLEX (C): Error occurred during presolve.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }*/

  // Convert variables to continuous
  /*
  const CPXDIM num_vars = CPXXgetnumcols(env, lp);
  CPXDIM* indices = new CPXDIM[num_vars];
  char* xctype = new char[num_vars];
  for (CPXDIM i = 0; i < num_vars; i++) {
    indices[i] = i;
    xctype[i] = CPX_CONTINUOUS;
  }
  status = CPXXchgctype(env, lp, num_vars, indices, xctype);
  if (status) {
    error_msg(errorstring, "CPLEX (C): Error occurred during changing variable type.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }
  delete indices;
  delete xctype;
  */
} /* presolveModelWithCplexCallable (CPXENVptr, CPXLPptr) */

void presolveModelWithCplexCallable(const char* f_name, double& presolved_opt, std::string& presolved_name) {
  CPXENVptr env = NULL;
  CPXLPptr lp = NULL;
  readFileIntoCplexCallable(f_name, env, lp);
  presolveModelWithCplexCallable(env, lp, presolved_opt, presolved_name);

  /* Free up the problem as allocated by CPXXcreateprob, if necessary */
  int status = 0;
  if ( lp != NULL ) {
    status = CPXXfreeprob (env, &lp);
    if ( status ) {
      error_msg(errorstring, "CPLEX (C): CPXXfreeprob failed, error code %d.\n", status);
      writeErrorToLog(errorstring, GlobalVariables::log_file);
    }
  }

  /* Free up the CPLEX environment, if necessary */
  if ( env != NULL ) {
    status = CPXXcloseCPLEX (&env);

    /* Note that CPXXcloseCPLEX produces no output,
       so the only way to see the cause of the error is to use
       CPXXgeterrorstring.  For other CPLEX routines, the errors will
       be seen if the CPXXPARAM_ScreenOutput indicator is set to CPXX_ON. */

      if ( status ) {
        char errmsg[CPXMESSAGEBUFSIZE];
        CPXXgeterrorstring (env, status, errmsg);
        error_msg(errorstring, "CPLEX (C): Could not close CPLEX environment. Error: %s", errmsg);
        writeErrorToLog(errorstring, GlobalVariables::log_file);
      }
   }
} /* presolveModelWithCplexCallable (filename) */

void presolveModelWithCplexCallable(const OsiSolverInterface* const solver, double& presolved_opt, std::string& presolved_name) {
  std::string f_name;
  createTmpFileCopy(solver, f_name);
  presolveModelWithCplexCallable(f_name.c_str(), presolved_opt, presolved_name);
  remove(f_name.c_str()); // remove temporary file
} /* presolveModelWithCplexCallable (Osi) */

void doBranchAndBoundWithCplexCallable(CPXENVptr& env, CPXLPptr& lp, BBInfo& info) {
//#ifdef TRACE
  printf("\n## Running B&B with CPLEX (Callable). Strategy: %d. ##\n", param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND));
//#endif
  int status = 0;

  // Set CPLEX parameters
  setStrategyForBBTestCplexCallable(env);
  
  // Optimize the LP
  //status = CPXXlpopt (env, lp);
  
  /* Optimize the problem and obtain solution. */
  // Exceeding a user-specified CPLEX limit is not considered an error. 
  // Proving the problem infeasible or unbounded is not considered an error.
  double start_time = 0., end_time = 0.;
  CPXXgettime(env, &start_time);
  status = CPXXmipopt (env, lp);
  CPXXgettime(env, &end_time);
  if ( status ) {
    error_msg(errorstring, "CPLEX (C): Failed to optimize MIP.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }
  
  int optimstatus = CPXXgetstat (env, lp);
  /*
  status = CPXXsolution(env, lp, &optimstatus, &bb_opt, NULL, NULL, NULL, NULL);
  if ( status ) {
    error_msg(errorstring, "CPLEX (C): Failed to get solution.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  char* optimstatusstring = new char[CPXMESSAGEBUFSIZE];
  CPXXgetstatstring (env, optimstatus, optimstatusstring);
  printf("Sol stat string: %s\n", optimstatusstring);
  */

  // Possibly re-run
  if (optimstatus == CPXMIP_INForUNBD) {
    int presolve_flag = 0;
    CPXXgetintparam(env, CPXPARAM_Preprocessing_Presolve, &presolve_flag);
    if (presolve_flag) {
      status = CPXXsetlongparam(env, CPXPARAM_Preprocessing_Presolve, 0); // turn off presolve overall
      if (status) {
        error_msg(errorstring, "CPLEX (C): Failed to set presolve parameter.\n");
        writeErrorToLog(errorstring, GlobalVariables::log_file);
        exit(1);
      }

      status = CPXXmipopt(env, lp);
      if ( status ) {
        error_msg(errorstring, "CPLEX (C): Failed to optimize MIP.\n");
        writeErrorToLog(errorstring, GlobalVariables::log_file);
        exit(1);
      }

      optimstatus = CPXXgetstat (env, lp);
    }
  }

  if (optimstatus == CPXMIP_INForUNBD || optimstatus == CPXMIP_INFEASIBLE || optimstatus == CPXMIP_UNBOUNDED) {
    error_msg(errorstring, "CPLEX (C): Failed to optimize MIP.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  // Get best objective value
  switch (optimstatus) {
    case CPXMIP_DETTIME_LIM_FEAS: {
    }
    case CPXMIP_DETTIME_LIM_INFEAS: {
    }
    case CPXMIP_NODE_LIM_FEAS: {
    }
    case CPXMIP_NODE_LIM_INFEAS: {
    }
    case CPXMIP_TIME_LIM_FEAS: {
    }
    case CPXMIP_TIME_LIM_INFEAS: {
      status = CPXXgetbestobjval (env, lp, &info.obj);
      break;
    }
    case CPXMIP_OPTIMAL_TOL: {
    }
    case CPXMIP_OPTIMAL: {
      status = CPXXgetobjval (env, lp, &info.obj);
      break;
    }
    default: {
      char* optimstatusstring = new char[CPXMESSAGEBUFSIZE];
      CPXXgetstatstring (env, optimstatus, optimstatusstring);
      error_msg(errorstring, "CPLEX (C): Other status after solve: %d, message: %s.\n", optimstatus, optimstatusstring);
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }
  } /* switch optimstatus */

  if ( status ) {
    error_msg(errorstring, "CPLEX (C): Failed to obtain objective value.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  info.iters = CPXXgetmipitcnt(env, lp);
  info.nodes = CPXXgetnodecnt(env, lp);
  info.time = end_time - start_time;

#ifdef TRACE
  printf("CPLEX (C): Solution value: %1.6f.\n", info.obj);
  printf("CPLEX (C): Number iterations: %ld.\n", info.iters);
  printf("CPLEX (C): Number nodes: %ld.\n", info.nodes);
  printf("CPLEX (C): Time: %f.\n", info.time);
#endif

  /* Free up the problem as allocated by CPXXcreateprob, if necessary */
  if ( lp != NULL ) {
    status = CPXXfreeprob (env, &lp);
    if ( status ) {
      error_msg(errorstring, "CPLEX (C): CPXXfreeprob failed, error code %d.\n", status);
      writeErrorToLog(errorstring, GlobalVariables::log_file);
    }
  }

  /* Free up the CPLEX environment, if necessary */
  if ( env != NULL ) {
    status = CPXXcloseCPLEX (&env);

    /* Note that CPXXcloseCPLEX produces no output,
       so the only way to see the cause of the error is to use
       CPXXgeterrorstring.  For other CPLEX routines, the errors will
       be seen if the CPXXPARAM_ScreenOutput indicator is set to CPXX_ON. */

      if ( status ) {
        char errmsg[CPXMESSAGEBUFSIZE];
        CPXXgeterrorstring (env, status, errmsg);
        error_msg(errorstring, "CPLEX (C): Could not close CPLEX environment. Error: %s\n", errmsg);
        writeErrorToLog(errorstring, GlobalVariables::log_file);
      }
   }
} /* doBranchAndBoundWithCplexCallable (CPXENVptr, CPXLPptr) */

void doBranchAndBoundWithUserCutsCplexCallable(CPXENVptr& env, CPXLPptr& lp,
    const OsiCuts* cuts, BBInfo& info, const bool addAsLazy) {
  // Ensure that user cuts setting is enabled
  const int strategy = GlobalVariables::param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND);
  if (!(strategy & BB_Strategy_Options::user_cuts)) {
    warning_msg(warnstring, "Need to use user_cuts option; strategy currently: %d.\n", strategy);
    GlobalVariables::param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND,
        strategy | BB_Strategy_Options::user_cuts);
  }

  // Add user cuts
  if (cuts) {
    const int num_cuts = cuts.sizeCuts();
    std::vector<CPXNNZ> cutbeg(num_cuts); // index where each cut starts
    std::vector<CPXDIM> cutind; // variables involved in each cut
    std::vector<double> cutval; // coefficients for the variabels in each cut
    std::vector<double> cutrhs(num_cuts); // rhs of each cut
    std::string cutsens = ""; // sense of each cut
    for (int cut_ind = 0; cut_ind < num_cuts; cut_ind++) {
      const OsiRowCut* curr_cut = cuts.rowCutPtr(cut_ind);
      const int num_el = curr_cut->row().getNumElements();
      const int* ind = curr_cut->row().getIndices();
      const double* vals = curr_cut->row().getElements();

      cutbeg[cut_ind] = cutind.size();
      cutind.insert(cutind.end(), ind, ind + num_el);
      cutval.insert(cutval.end(), vals, vals + num_el);
      cutrhs[cut_ind] = curr_cut->rhs();
      cutsens += "G";
    } /* end iterating over cuts */

    int status = CPXXaddusercuts(env, lp, cutbeg.size(), cutind.size(), cutrhs.data(), cutsens.c_str(), cutbeg.data(), cutind.data(), cutval.data(), NULL);
    if (status) {
      error_msg(errorstring, "CPLEX (C): Error adding user cuts. Error: %d.\n", status);
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }

    if (addAsLazy) {
      status = CPXXaddlazyconstraints(env, lp, cutbeg.size(), cutind.size(), cutrhs.data(), cutsens.c_str(), cutbeg.data(), cutind.data(), cutval.data(), NULL);
      if (status) {
        error_msg(errorstring, "CPLEX (C): Error adding lazy cuts. Error: %d.\n", status);
        writeErrorToLog(errorstring, GlobalVariables::log_file);
        exit(1);
      }
    }
  } /* ensure cuts is not NULL */
  // Continue in normal routine
  doBranchAndBoundWithCplexCallable(env, lp, info); // does freeing

  GlobalVariables::param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, strategy);
} /* doBranchAndBoundWithUserCutsCplexCallable (CPXenvptr) */

void doBranchAndBoundWithCplexCallable(const char* f_name, BBInfo& info) {
#ifdef TRACE
  printf("\n## Reading from file into CPLEX (C). ##\n");
#endif
  CPXENVptr env = NULL;
  CPXLPptr lp = NULL;
  readFileIntoCplexCallable(f_name, env, lp);
  const int strategy = GlobalVariables::param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND);
  if (strategy & BB_Strategy_Options::user_cuts) {
    doBranchAndBoundWithUserCutsCplexCallable(env, lp, NULL, info, false); // add as lazy switch not available here
  } else {
    doBranchAndBoundWithCplexCallable(env, lp, info); // does freeing
  }
} /* doBranchAndBoundWithCplexCallable (file) */

void doBranchAndBoundWithCplexCallable(const OsiSolverInterface* const solver,
    BBInfo& info) {
  std::string f_name;
  createTmpFileCopy(solver, f_name);
  doBranchAndBoundWithCplexCallable(f_name.c_str(), info);
  remove(f_name.c_str());
} /* doBranchAndBoundWithCplexCallable (Osi) */

void doBranchAndBoundWithUserCutsCplexCallable(const char* f_name,
    const OsiCuts* cuts, BBInfo& info, const bool addAsLazy) {
#ifdef TRACE
  printf("\n## Reading from file into CPLEX (C) and adding user cuts. ##\n");
#endif
  CPXENVptr env = NULL;
  CPXLPptr lp = NULL;
  int status = 0;

  /* Initialize the CPLEX environment */
  env = CPXXopenCPLEX (&status);

  /* If an error occurs, the status value indicates the reason for
     failure.  A call to CPXXgeterrorstring will produce the text of
     the error message.  Note that CPXXopenCPLEX produces no output,
     so the only way to see the cause of the error is to use
     CPXXgeterrorstring.  For other CPLEX routines, the errors will
     be seen if the CPXPARAM_ScreenOutput indicator is set to CPX_ON.  */
  if ( env == NULL ) {
    char errmsg[CPXMESSAGEBUFSIZE];
    CPXXgeterrorstring (env, status, errmsg);
    error_msg(errorstring, "CPLEX (C): Could not open CPLEX environment. Error: %s", errmsg);
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  /* Create the problem, using the filename as the problem name */
  lp = CPXXcreateprob (env, &status, f_name);
   
  /* A returned pointer of NULL may mean that not enough memory
     was available or there was some other problem.  In the case of 
     failure, an error message will have been written to the error 
     channel from inside CPLEX.  In this example, the setting of
     the parameter CPXXPARAM_ScreenOutput causes the error message to
     appear on stdout.  Note that most CPLEX routines return
     an error code to indicate the reason for failure.   */
  if ( lp == NULL ) {
    error_msg(errorstring, "CPLEX (C): Failed to create the LP.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  /* Now read the file, and copy the data into the created lp */
  status = CPXXreadcopyprob (env, lp, f_name, NULL);
  if ( status ) {
    error_msg(errorstring, "CPLEX (C): Failed to read and copy the problem data.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  doBranchAndBoundWithUserCutsCplexCallable(env, lp, cuts, info, addAsLazy); // does freeing
} /* doBranchAndBoundWithUserCutsCplexCallable (filename) */

void doBranchAndBoundWithUserCutsCplexCallable(const OsiSolverInterface* const solver,
    const OsiCuts& cuts, BBInfo& info, const bool addAsLazy) {
  std::string f_name;
  createTmpFileCopy(solver, f_name);
  doBranchAndBoundWithUserCutsCplexCallable(f_name.c_str(), cuts, info, addAsLazy);
  remove(f_name.c_str()); // remove temporary file
} /* doBranchAndBoundWithUserCutsCplexCallable (Osi) */

// C++ interface 
#ifdef VPC_USE_CPLEX_CONCERT
#include <ilcplex/ilocplex.h>
ILOSTLBEGIN

void setStrategyForBBTestCplexConcert(IloCplex& cplex,
    const int seed = GlobalVariables::random_seed,
    const double best_bound = GlobalVariables::bestObjValue) {
  // Parameters that should always be set
  cplex.setParam(IloCplex::Param::TimeLimit, GlobalVariables::param.getBB_TIMELIMIT()); // time limit
  cplex.setParam(IloCplex::Param::Threads, 1); // single-threaded
  cplex.setParam(IloCplex::Param::RandomSeed, seed); // random seed

#ifndef TRACE
    cplex.setOut(cplex.getEnv().getNullStream());
#endif

  int strategy = param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND);
  if (strategy <= 0) {
    // Default strategy
  } else {
    if (strategy & BB_Strategy_Options::user_cuts) {
      cplex.setParam(IloCplex::Param::MIP::Strategy::Search, CPX_MIPSEARCH_TRADITIONAL); // disable dynamic search
      cplex.setParam(IloCplex::Param::Preprocessing::Linear, 0); // presolved model vars linear comb of original
      cplex.setParam(IloCplex::Param::Preprocessing::Reduce, CPX_PREREDUCE_PRIMALONLY); // disable dual reductions
    }

    // Turn off cuts
    if (strategy & BB_Strategy_Options::all_cuts_off) {
      //cplex.setParam(IloCplex::Param::MIP::Limits::CutsFactor, 0.);
      //    cplex.setParam(IloCplex::Param::MIP::Limits::CutPasses, -1);
      //cplex.setParam(IloCplex::Param::MIP::Limits::EachCutLimit, 0); // disable all but Gomory
      cplex.setParam(IloCplex::Param::MIP::Cuts::BQP, -1); 
      cplex.setParam(IloCplex::Param::MIP::Cuts::Cliques, -1);
      cplex.setParam(IloCplex::Param::MIP::Cuts::Covers, -1);
      cplex.setParam(IloCplex::Param::MIP::Cuts::Disjunctive, -1);
      cplex.setParam(IloCplex::Param::MIP::Cuts::FlowCovers, -1);
      cplex.setParam(IloCplex::Param::MIP::Cuts::PathCut, -1);
      cplex.setParam(IloCplex::Param::MIP::Cuts::Gomory, -1);
      cplex.setParam(IloCplex::Param::MIP::Cuts::GUBCovers, -1);
      cplex.setParam(IloCplex::Param::MIP::Cuts::Implied, -1);
      cplex.setParam(IloCplex::Param::MIP::Cuts::LocalImplied, -1);
      cplex.setParam(IloCplex::Param::MIP::Cuts::LiftProj, -1);
      cplex.setParam(IloCplex::Param::MIP::Cuts::MIRCut, -1);
      cplex.setParam(IloCplex::Param::MIP::Cuts::MCFCut, -1);
      cplex.setParam(IloCplex::Param::MIP::Cuts::RLT, -1);
      cplex.setParam(IloCplex::Param::MIP::Cuts::ZeroHalfCut, -1);
      //    cplex.setParam(IloCplex::Param::MIP::Limits::GomoryPass, 0);
      //    cplex.setParam(IloCplex::Param::MIP::Limits::GomoryCand, 0);
    }

    // Presolve
    if (strategy & BB_Strategy_Options::presolve_off) {
      cplex.setParam(IloCplex::Param::Preprocessing::Presolve, 0); // turn off presolve overall
      cplex.setParam(IloCplex::Param::MIP::Strategy::PresolveNode, 0); // turn off presolve at nodes
      cplex.setParam(IloCplex::Param::Preprocessing::Relax, 0); // turn off LP presolve at root
      cplex.setParam(IloCplex::Param::MIP::Strategy::Probe, -1); // turn off probing
    }

    // Heuristics
    if (strategy & BB_Strategy_Options::heuristics_off) {
      cplex.setParam(IloCplex::Param::MIP::Strategy::HeuristicFreq, -1); // turn off the "periodic" heuristic
      cplex.setParam(IloCplex::Param::MIP::Strategy::FPHeur, -1); // turn off feasibility pump
      cplex.setParam(IloCplex::Param::MIP::Strategy::LBHeur, 0); // local branching heuristic (default is off anyway)
      cplex.setParam(IloCplex::Param::MIP::Strategy::RINSHeur, 0); // turn off RINS
    }

    // Feed the solver the best bound provided
    if (strategy & BB_Strategy_Options::use_best_bound) {
      if (!isInfinity(best_bound)) {
        cplex.setParam(IloCplex::Param::MIP::Tolerances::UpperCutoff, best_bound + 1e-3); // give the solver the best IP objective value (it is a minimization problem) with a tolerance
      }
    }
  }

  // Check if we should use strong branching
  if (std::abs(strategy) & BB_Strategy_Options::strong_branching_on) {
    cplex.setParam(IloCplex::Param::MIP::Strategy::VariableSelect,
        IloCplex::VariableSelect::Strong);
    cplex.setParam(IloCplex::Param::MIP::Limits::StrongCand, CPX_BIGINT); // smaller than max int
        //std::numeric_limits<int>::max());
    cplex.setParam(IloCplex::Param::MIP::Limits::StrongIt, CPX_BIGLONG); // smaller than max long
        //std::numeric_limits<int>::max());
    cplex.setParam(IloCplex::Param::MIP::Strategy::BBInterval, 1); // always select best bound node
  }
} /* setStrategyForBBTestCplexConcert */

void doBranchAndBoundWithCplexConcert(IloEnv& env, IloModel& model, IloCplex& cplex,
    BBInfo& info) {
#ifdef TRACE
  printf("\n## Running B&B with CPLEX (Concert). Strategy: %d. ##\n", param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND));
#endif
  setStrategyForBBTestCplexConcert(cplex); // set parameters

  try {
    IloObjective obj;
    IloNumVarArray var(env);
    IloRangeArray rng(env);
    //model.add(IloConversion(env, var, ILOFLOAT));

    const IloNum start_time = cplex.getCplexTime();
    bool isFeasibleSolutionFound = cplex.solve();
    IloNum end_time = cplex.getCplexTime();

    IloAlgorithm::Status optimstatus = cplex.getStatus();
    if (optimstatus == IloAlgorithm::Status::InfeasibleOrUnbounded) {
      if (presolve_flag) {
        isFeasibleSolutionFound = cplex.solve();
        end_time = cplex.getCplexTime();
        optimstatus = cplex.getStatus();
      }
    }
    
    /*
    if (!isFeasibleSolutionFound) {
      error_msg(errorstring, "CPLEX (C): No feasible solution found using Cplex. Status: %d.\n", optimstatus);
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }
    */

    if (optimstatus == IloAlgorithm::Status::Optimal) {
      info.obj = cplex.getObjValue();
    } else if (optimstatus == IloCplex::CplexStatus::AbortTimeLim) {
      // Abort due to time limit
      info.obj = cplex.getBestObjValue();
    } else if (optimstatus == IloCplex::CplexStatus::AbortItLim) {
      // Abort due to iteration limit
      info.obj = cplex.getBestObjValue();
    } else {
      error_msg(errorstring, "CPLEX (C): Other status after solve: %d.\n", optimstatus);
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }
    info.iters = cplex.getNiterations();
    info.nodes = cplex.getNnodes();
    info.time = end_time - start_time;

#ifdef TRACE
    printf("CPLEX (Concert): Solution value: %e.\n", info.obj);
    printf("CPLEX (Concert): Number iterations: %ld.\n", info.iters);
    printf("CPLEX (Concert): Number nodes: %ld.\n", info.nodes);
    printf("CPLEX (Concert): Time: %f.\n", info.time);
#endif
    //IloNumArray vals(env);
    //cplex.getValues(vals, var);
    //printf("Solution status: %s.\n", cplex.getStatus());

    //env.out() << "Solution vector = " << vals << endl;

    /*
    try {     // basis may not exist
      IloCplex::BasisStatusArray cstat(env);
      cplex.getBasisStatuses(cstat, var);
      env.out() << "Basis statuses  = " << cstat << endl;
    } catch (...) {
    }
    */
    //env.out() << "Maximum bound violation = "
    //    << cplex.getQuality(IloCplex::MaxPrimalInfeas) << endl;
//    IloNumVarArray vars(env);
//    vars.add(IloNumVar(env, 0.0, 40.0));
//    vars.add(IloNumVar(env));
//    vars.add(IloNumVar(env));
//    model.add(IloMaximize(env, vars[0] + 2 * vars[1] + 3 * vars[2]));
//    model.add(-vars[0] + vars[1] + vars[2] <= 20);
//    model.add(vars[0] - 3 * vars[1] + vars[2] <= 30);
//    IloCplex cplex(model);
//    if (!cplex.solve()) {
//      env.error() << "Failed to optimize LP." << std::endl;
//      throw(-1);
//    }
//    IloNumArray vals(env);
//    env.out() << "Solution status = " << cplex.getStatus() << std::endl;
//    env.out() << "Solution value = " << cplex.getObjValue() << std::endl;
//    cplex.getValues(vals, vars);
//    env.out() << "Values = " << vals << std::endl;
  } catch (IloException& e) {
    error_msg(errorstring, "CPLEX (C++): Exception caught: %s.\n", e.getMessage());
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  } catch (...) {
    error_msg(errorstring, "CPLEX (C++): Unknown exception caught.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }
  env.end();
} /* doBranchAndBoundWithCplexConcert (IloEnv, IloModel, IloCplex) */

void doBranchAndBoundWithUserCutsCplexConcert(IloEnv& env, IloModel& model, IloCplex& cplex,
    const OsiCuts* cuts, BBInfo& info, const bool addAsLazy) {
  // Ensure that user cuts setting is enabled
  const int strategy = GlobalVariables::param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND);
  if (!(strategy & BB_Strategy_Options::user_cuts)) {
    warning_msg(warnstring,
        "Need to use user_cuts option; strategy currently: %d.\n", strategy);
    GlobalVariables::param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND,
        strategy | BB_Strategy_Options::user_cuts);
  }

#ifdef TRACE
  printf("\n## Reading from file into CPLEX (Concert) and adding user cuts. ##\n");
#endif
  try {
    if (cuts) {
      // Add user cuts
      IloRangeArray cplex_cuts(env);
      for (int cut_ind = 0; cut_ind < cuts.sizeCuts(); cut_ind++) {
        const OsiRowCut* curr_cut = cuts.rowCutPtr(cut_ind);
  //      const double* curr_coeff = curr_cut->row().denseVector(n);
        const int* ind = curr_cut->row().getIndices();
        const double* vals = curr_cut->row().getElements();

        // Set IloConstraint
        IloRange curr_cplex_cut(env, curr_cut->rhs(), IloInfinity, "ObjCut");
        for (int i = 0; i < curr_cut->row().getNumElements(); i++) {
          const IloNumVar curr_var = var[ind[i]];
          const IloNum curr_value = vals[i];
          curr_cplex_cut.setLinearCoef(curr_var, curr_value);
        }
        cplex_cuts.add(curr_cplex_cut);
      } /* finish setting cplex version of given cuts */
      cplex.addUserCuts(cplex_cuts);
      if (addAsLazy) {
        cplex.addLazyConstraints(cplex_cuts);
      }
      cplex_cuts.endElements();
      cplex_cuts.end();
    } /* check that cuts is not NULL */

    doBranchAndBoundWithCplexConcert(env, model, cplex, info); // does freeing
  } catch (IloException& e) {
    error_msg(errorstring, "CPLEX (C++): Exception caught: %s.\n", e.getMessage());
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  } catch (...) {
    error_msg(errorstring, "CPLEX (C++): Unknown exception caught.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  GlobalVariables::param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, strategy);
} /* doBranchAndBoundWithUserCutsCplexConcert (IloEnv, IloModel, IloCplex) */

void doBranchAndBoundWithCplexConcert(const char* f_name, BBInfo& info) {
#ifdef TRACE
  printf("\n## Reading from file into CPLEX (Concert). ##\n");
#endif
  IloEnv env;
  try {
    IloModel model(env);
    IloCplex cplex(model);
    cplex.importModel(model, f_name, obj, var, rng);
    if (strategy & BB_Strategy_Options::user_cuts) {
      doBranchAndBoundWithUserCutsCplexConcert(env, model, cplex, NULL, info, false); // add as lazy is turned off here because no option is passed
    } else {
      doBranchAndBoundWithCplexConcert(env, model, cplex, info); // does freeing
    }
  } catch (IloException& e) {
    error_msg(errorstring, "CPLEX (C++): Exception caught: %s.\n", e.getMessage());
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  } catch (...) {
    error_msg(errorstring, "CPLEX (C++): Unknown exception caught.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }
} /* doBranchAndBoundWithCplexConcert (filename) */

void doBranchAndBoundWithCplexConcert(const OsiSolverInterface* const solver, BBInfo& info) {
  std::string f_name;
  createTmpFileCopy(solver, f_name);
  doBranchAndBoundWithCplexConcert(f_name.c_str(), info);
  remove(f_name.c_str()); // remove temporary file
} /* doBranchAndBoundWithCplexConcert (Osi, creates temporary file from solver) */

void doBranchAndBoundWithUserCutsCplexConcert(const char* f_name,
    const OsiCuts* cuts, BBInfo& info, const bool addAsLazy) {
  IloEnv env;
  try {
    IloModel model(env);
    IloCplex cplex(model);
    cplex.importModel(model, f_name, obj, var, rng);

    doBranchAndBoundWithUserCutsCplexConcert(env, model, cplex, cuts, info, addAsLazy);
  } catch (IloException& e) {
    error_msg(errorstring, "CPLEX (C++): Exception caught: %s.\n", e.getMessage());
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  } catch (...) {
    error_msg(errorstring, "CPLEX (C++): Unknown exception caught.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }
} /* doBranchAndBoundWithUserCutsCplexConcert (f_name) */

void doBranchAndBoundWithUserCutsCplexConcert(const OsiSolverInterface* const solver,
    const OsiCuts& cuts, BBInfo& info, const bool addAsLazy) {
  std::string f_name;
  createTmpFileCopy(solver, f_name);
  doBranchAndBoundWithUserCutsCplexConcert(f_name.c_str(), cuts, info, addAsLazy);
  remove(f_name.c_str()); // remove temporary file
} /* doBranchAndBoundWithUserCutsCplexConcert (Osi) */
#endif /* VPC_USE_CPLEX_CONCERT */
#endif /* VPC_USE_CPLEX */

// Gurobi
#ifdef VPC_USE_GUROBI
#include "gurobi_c++.h"

/**
 * Creates temporary file (in /tmp) so that it can be read by a different solver
 * It does not delete the file
 */
void createTmpFileCopy(GRBModel& model, std::string& f_name) {
  if (f_name.empty()) {
    // Generate temporary file name
    char template_name[] = "/tmp/tmpmpsXXXXXX";

    mktemp(template_name);
    f_name = template_name;
    if (f_name.empty()) {
      error_msg(errorstring, "Could not generate temp file.\n");
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }
  }
  f_name += ".mps.gz"; // if something is given, it should be given as a stub
  model.write(f_name.c_str());
} /* createTmpFileCopy (Gurobi) */

void setStrategyForBBTestGurobi(GRBModel& model, 
    const int seed = GlobalVariables::random_seed,
    const double best_bound = GlobalVariables::bestObjValue) {
  // Parameters that should always be set
  model.set(GRB_DoubleParam_TimeLimit, GlobalVariables::param.getBB_TIMELIMIT()); // time limit
  model.set(GRB_IntParam_Threads, 1); // single-threaded
  model.set(GRB_IntParam_Seed, seed); // random seed
//  model.set(GRB_DoubleParam_MIPGap, param.getEPS()); // I guess the default 1e-4 is okay, though it messes up for large objective values

#ifndef TRACE
  model.set(GRB_IntParam_OutputFlag, 0); // turn off output
#endif

  int strategy = param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND);
  if (strategy <= 0) {
    // Default strategy
  } else {
    if (strategy & BB_Strategy_Options::user_cuts) {
      model.set(GRB_IntParam_DualReductions, 0); // disable dual reductions
      model.set(GRB_IntParam_PreCrush, 1); // must be enabled when using user cuts
    }

    // Turn off all cuts
    if (strategy & BB_Strategy_Options::all_cuts_off) {
      model.set(GRB_IntParam_Cuts, 0); // turn off all cuts
    }
    
    // Presolve
    if (strategy & BB_Strategy_Options::presolve_off) {
      model.set(GRB_IntParam_Presolve, 0); // disable presolve
    }

    // Heuristics
    if (strategy & BB_Strategy_Options::heuristics_off) {
      model.set(GRB_DoubleParam_Heuristics, 0.); // disable heuristics
      //model.set(GRB_IntParam_PumpPasses, 0); // disable feasibility pump; is this turned off by setting heuristic frequency to 0.?
      //model.set(GRB_IntParam_RINS, 0); // disable RINS; is this turned off by setting heuristic frequency to 0.?
    }
    
    // Feed the solver the best bound provided
    if (strategy & BB_Strategy_Options::use_best_bound) {
      if (!isInfinity(best_bound)) {
//        model.set(GRB_DoubleParam_BestObjStop, best_bound + 1e-3); // give the solver the best IP objective value (it is a minimization problem) with a tolerance
        model.set(GRB_DoubleParam_BestBdStop, best_bound + 1e-3); // give the solver the best IP objective value (it is a minimization problem) with a tolerance
      }
    }
  } /* strategy > 0 */

  // Check if we should use strong branching
  if (std::abs(strategy) & BB_Strategy_Options::strong_branching_on) {
    model.set(GRB_IntParam_VarBranch, 3); // turn on strong branching
  }
} /* setStrategyForBBTestGurobi */

/**
 * User cut callback
 */
class GurobiUserCutCallback : public GRBCallback {
  public:
    int num_vars;
    double obj_offset;
    GRBVar* grb_vars;
    const OsiCuts* cuts;
    bool addAsLazy;
    double first_lp_opt;
    BBInfo info;
    
    GurobiUserCutCallback(int num_vars, double obj_offset, GRBVar* vars,
        const OsiCuts* cutsToAdd, const bool lazyFlag = false) :
        num_vars(num_vars), obj_offset(obj_offset), grb_vars(vars),
        cuts(cutsToAdd), addAsLazy(lazyFlag) {
      this->info.obj = param.getINFINITY();
      this->first_lp_opt = -1 * param.getINFINITY();
      this->info.root_passes = 0;
      this->info.first_cut_pass = -1 * param.getINFINITY();
      this->info.last_cut_pass = -1 * param.getINFINITY();
      this->info.root_time = 0.;
      this->info.last_sol_time = 0.;
//      this->numTimesApplied = 0;
    } /* constructor */

    /**
     * Destructor
     */
    ~GurobiUserCutCallback() {
      if (this->grb_vars) {
        delete[] grb_vars;
      }
    } /* destructor */

  protected:
    double getObjValue() {
      double* vals = GRBCallback::getNodeRel(grb_vars, num_vars);
      double obj_val = obj_offset;
      for (int i = 0; i < num_vars; i++) {
        obj_val += vals[i] * grb_vars[i].get(GRB_DoubleAttr_Obj);
      }
      if (vals) {
        delete[] vals;
      }
      return obj_val;
    } /* getObjValue */

    void callback() {
      try {
        if (where == GRB_CB_MIPSOL) {
          const double obj = GRBCallback::getDoubleInfo(GRB_CB_MIPSOL_OBJ);
          if (obj < this->info.obj) { // integer-feasible solution with better solution has been found
            this->info.last_sol_time = GRBCallback::getDoubleInfo(GRB_CB_RUNTIME);
            this->info.obj = obj;
          }
        } else if (where == GRB_CB_MIPNODE) {
          const int num_nodes = GRBCallback::getDoubleInfo(GRB_CB_MIPNODE_NODCNT);
          if (num_nodes > 0) {
            return;
          }
          this->info.root_passes++;
          this->info.root_time = GRBCallback::getDoubleInfo(GRB_CB_RUNTIME);

          if (GRBCallback::getIntInfo(GRB_CB_MIPNODE_STATUS) != GRB_OPTIMAL) {
            return;
          }

          const double objValue = getObjValue();
          this->info.last_cut_pass = objValue;

          // Make sure this is our first entry into the root
          if (this->info.root_passes > 1) {
            if (this->info.root_passes == 2) {
              this->info.first_cut_pass = objValue;
            }
            return;
          }

          this->first_lp_opt = objValue;
          this->info.first_cut_pass  = objValue;

          // Add cuts to the model, one at a time
          if (cuts) {
            for (int cut_ind = 0; cut_ind < cuts->sizeCuts(); cut_ind++) {
              const OsiRowCut* curr_cut = cuts->rowCutPtr(cut_ind);
              const int num_el = curr_cut->row().getNumElements();
              const int* ind = curr_cut->row().getIndices();
              const double* vals = curr_cut->row().getElements();

              // Cannot add it all at once due to the C++ interface being worse than the C one
              //GRBaddconstr(model, num_el, ind, vals, GRB_GREATER_EQUAL, curr_cut->rhs(), NULL);
              GRBLinExpr lhs = 0;
              for (int i = 0; i < num_el; i++) {
                const int curr_var = ind[i];
                const double curr_value = vals[i];
                lhs += curr_value * grb_vars[curr_var];
              }
              // model.addConstr(lhs, GRB_GREATER_EQUAL, curr_cut->rhs());
              addCut(lhs, GRB_GREATER_EQUAL, curr_cut->rhs());
              if (addAsLazy) {
                addCut(lhs, GRB_GREATER_EQUAL, curr_cut->rhs());
              }
            } /* add cuts to the model */
          } /* check that cuts is not null */
        } /* where == MIPNODE (make sure we are in the right "where") */
//        else if (where == GRB_CB_MIP) {
//          // Get the number of times cuts were applied
//          num_total_cuts_applied = GRBCallback::getIntInfo(GRB_CB_MIP_CUTCNT);
//        } /* where == MIP (make sure we are in the right "where") */
//        else if (where == GRB_CB_MESSAGE) {
//          // Number user cuts
//          std::string msg = GRBCallback::getStringInfo(GRB_CB_MSG_STRING);
//        } /* where == MESSAGE */
      } catch (GRBException& e) {
        error_msg(errorstring, "Gurobi: Error during callback: %s\n", e.getMessage().c_str());
        writeErrorToLog(errorstring, GlobalVariables::log_file);
        exit(1);
      } catch (...) {
        error_msg(errorstring, "Gurobi: Error during callback.\n");
        writeErrorToLog(errorstring, GlobalVariables::log_file);
        exit(1);
      }
    } /* callback */
}; /* class GurobiUserCutCallback */

void presolveModelWithGurobi(GRBModel& model, double& presolved_opt,
    std::string& presolved_name) {
//#ifdef TRACE
  printf("\n## Gurobi: Presolving model ##\n");
//#endif
  try {
    GRBModel presolved_model = model.presolve();
    GRBModel presolved_model_mip = presolved_model;
    GRBVar* vars = presolved_model.getVars();
    const int num_vars = presolved_model.get(GRB_IntAttr_NumVars);
    for (int i = 0; i < num_vars; i++) {
      vars[i].set(GRB_CharAttr_VType, GRB_CONTINUOUS);
    }
    const int strategy = param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND);
    param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, BB_Strategy_Options::presolve_on);
    setStrategyForBBTestGurobi(presolved_model);
    param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, strategy);

    presolved_model.optimize();

    // Save optimal value
    int optimstatus = presolved_model.get(GRB_IntAttr_Status);
    if (optimstatus == GRB_OPTIMAL) {
        presolved_opt = presolved_model.get(GRB_DoubleAttr_ObjVal);
        createTmpFileCopy(presolved_model_mip, presolved_name);
        if (vars) {
          delete[] vars;
        }
    } else {
      error_msg(errorstring, "Gurobi: Was not able to solve presolved model to optimality. Status: %d.\n", optimstatus);
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }
  } catch (GRBException& e) {
    error_msg(errorstring, "Gurobi: Exception caught: %s\n", e.getMessage().c_str());
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  } catch (...) {
    error_msg(errorstring, "Gurobi: Unknown exception caught.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }
} /* presolveModelWithGurobi (GRBModel) */

void presolveModelWithGurobi(const char* f_name, double& presolved_opt,
    std::string& presolved_name) {
  try {
    GRBEnv env = GRBEnv();
    GRBModel model = GRBModel(env, f_name);
    presolveModelWithGurobi(model, presolved_opt, presolved_name);
  } catch (GRBException& e) {
    error_msg(errorstring, "Gurobi: Exception caught: %s\n", e.getMessage().c_str());
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  } catch (...) {
    error_msg(errorstring, "Gurobi: Unknown exception caught.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }
} /* presolveModelWithGurobi (filename) */

void presolveModelWithGurobi(const OsiSolverInterface* const solver,
    double& presolved_opt, std::string& presolved_name) {
  std::string f_name;
  createTmpFileCopy(solver, f_name);
  presolveModelWithGurobi(f_name.c_str(), presolved_opt, presolved_name);
  remove(f_name.c_str()); // remove temporary file
} /* presolveModelWithGurobi (Osi) */

void doBranchAndBoundWithGurobi(GRBModel& model, BBInfo& info,
    std::vector<double>* const solution = NULL) {
//#ifdef TRACE
  printf("\n## Running B&B with Gurobi. Strategy: %d. Random seed: %d. ##\n", param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND), GlobalVariables::random_seed);
//#endif
  try {
    setStrategyForBBTestGurobi(model);

    model.optimize();

    int optimstatus = model.get(GRB_IntAttr_Status);

    if (optimstatus == GRB_INF_OR_UNBD) {
      const int presolve_flag = model.get(GRB_IntParam_Presolve);
      if (presolve_flag) {
        model.set(GRB_IntParam_Presolve, 0);
        model.optimize();
        optimstatus = model.get(GRB_IntAttr_Status);
      }
    }

    if (optimstatus == GRB_INFEASIBLE || optimstatus == GRB_UNBOUNDED || optimstatus == GRB_INF_OR_UNBD) {
      error_msg(errorstring, "Gurobi: Failed to optimize MIP.\n");
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }

    switch (optimstatus) {
      case GRB_CUTOFF:
      case GRB_ITERATION_LIMIT:
      case GRB_NODE_LIMIT:
      case GRB_TIME_LIMIT: {
        info.obj = model.get(GRB_DoubleAttr_ObjVal);
        info.bound = model.get(GRB_DoubleAttr_ObjBound);
        break;
      }
      case GRB_USER_OBJ_LIMIT: {
        info.obj = model.get(GRB_DoubleAttr_ObjVal);
        info.bound = model.get(GRB_DoubleAttr_ObjBound);
        break;
      }
      case GRB_OPTIMAL: {
        info.obj = model.get(GRB_DoubleAttr_ObjVal);
        info.bound = model.get(GRB_DoubleAttr_ObjBound);
        break;
      }
      default: {
        error_msg(errorstring, "Gurobi: Other status after solve: %d.\n", optimstatus);
        writeErrorToLog(errorstring, GlobalVariables::log_file);
        exit(1);
      }
    } /* switch optimistatus */
    info.iters = (long) model.get(GRB_DoubleAttr_IterCount);
    info.nodes = (long) model.get(GRB_DoubleAttr_NodeCount);
    info.time = model.get(GRB_DoubleAttr_Runtime);

#ifdef TRACE
    printf("Gurobi: Solution value: %1.6f.\n", info.obj);
    printf("Gurobi: Best bound: %1.6f.\n", info.bound);
    printf("Gurobi: Number iterations: %ld.\n", info.iters);
    printf("Gurobi: Number nodes: %ld.\n", info.nodes);
    printf("Gurobi: Time: %f.\n", info.time);
#endif

   // Save the solution if needed
   if (solution) {
     // Get variables
     GRBVar* vars = model.getVars();
     const int num_vars = model.get(GRB_IntAttr_NumVars);
     (*solution).resize(num_vars);
     for (int i = 0; i < num_vars; i++) {
       (*solution)[i] = vars[i].get(GRB_DoubleAttr_X);
     }
     if (vars) {
       delete[] vars;
     }
   }
  } catch(GRBException& e) {
    error_msg(errorstring, "Gurobi: Exception caught: %s\n", e.getMessage().c_str());
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  } catch (...) {
    error_msg(errorstring, "Gurobi: Unknown exception caught.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }
} /* doBranchAndBoundWithGurobi (GRBModel) */

void doBranchAndBoundWithUserCutsGurobi(GRBModel& model, const OsiCuts* cuts,
    BBInfo& info, const bool addAsLazy, std::vector<double>* const solution = NULL) {
  // Ensure that user cuts setting is enabled
  const int strategy = GlobalVariables::param.getParamVal(
      ParamIndices::BB_STRATEGY_PARAM_IND);
  if (!(strategy & BB_Strategy_Options::user_cuts)) {
    warning_msg(warnstring,
        "Need to use user_cuts option; strategy currently: %d.\n", strategy);
    GlobalVariables::param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND,
        strategy | BB_Strategy_Options::user_cuts);
  }

  try {
    // Due to using the C++ interface, we need to access the variable list first
    //GRBVar* grb_vars = model.getVars();
    GurobiUserCutCallback cb = GurobiUserCutCallback(
        model.get(GRB_IntAttr_NumVars), model.get(GRB_DoubleAttr_ObjCon),
        model.getVars(), cuts, addAsLazy);
    model.setCallback(&cb);

    // Update the model
    model.update();
    /*
    const int retcode = GRBupdatemodel(&model);
    if (retcode) {
      error_msg(errorstring, "Gurobi: Error updating the model; error code: %d.\n", retcode);
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }
    */

    // Finally, run B&B
    doBranchAndBoundWithGurobi(model, info, solution);

    // Save information
    info.root_passes = cb.info.root_passes;
    if (info.root_passes > 0) {
      info.first_cut_pass = cb.info.first_cut_pass; // second because first is lp opt val
      info.last_cut_pass = cb.info.last_cut_pass;
      info.root_time = cb.info.root_time;
      info.last_sol_time = cb.info.last_sol_time;
    } else {
      info.first_cut_pass = info.obj;
      info.last_cut_pass = info.obj;
      info.root_time = info.time; // all time was spent at the root
      info.last_sol_time = cb.info.last_sol_time; // roughly the same as total time in this case
    }
//#ifdef TRACE
//    printf("Gurobi: Times cuts applied: %d.\n", cb.numTimesApplied);
//#endif
  } catch (GRBException& e) {
    error_msg(errorstring, "Gurobi: Exception caught: %s\n", e.getMessage().c_str());
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  } catch (...) {
    error_msg(errorstring, "Gurobi: Unknown exception caught.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  GlobalVariables::param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, strategy);
} /* doBranchAndBoundWithUserCutsGurobi (GRBModel) */

void doBranchAndBoundWithGurobi(const char* f_name, BBInfo& info,
    std::vector<double>* const solution) {
#ifdef TRACE
  printf("\n## Reading from file into Gurobi. ##\n");
#endif
  try {
    GRBEnv env = GRBEnv();
    GRBModel model = GRBModel(env, f_name);
    const int strategy = GlobalVariables::param.getParamVal(
        ParamIndices::BB_STRATEGY_PARAM_IND);
    if (strategy & BB_Strategy_Options::user_cuts) {
      doBranchAndBoundWithUserCutsGurobi(model, NULL, info, false, solution);
    } else {
      doBranchAndBoundWithGurobi(model, info, solution);
    }
  } catch (GRBException& e) {
    error_msg(errorstring, "Gurobi: Exception caught: %s\n", e.getMessage().c_str());
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  } catch (...) {
    error_msg(errorstring, "Gurobi: Unknown exception caught.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }
} /* doBranchAndBoundWithGurobi (filename) */

void doBranchAndBoundWithGurobi(const OsiSolverInterface* const solver,
    BBInfo& info, std::vector<double>* const solution) {
  std::string f_name;
  createTmpFileCopy(solver, f_name);
  doBranchAndBoundWithGurobi(f_name.c_str(), info, solution);
  remove(f_name.c_str()); // remove temporary file
} /* doBranchAndBoundWithGurobi (Osi) */

void doBranchAndBoundWithUserCutsGurobi(const char* f_name, const OsiCuts* cuts,
    BBInfo& info, const bool addAsLazy) {
#ifdef TRACE
  printf("\n## Reading from file into Gurobi and adding user cuts. ##\n");
#endif
  try {
    GRBEnv env = GRBEnv();
    GRBModel model = GRBModel(env, f_name);
    doBranchAndBoundWithUserCutsGurobi(model, cuts, info, addAsLazy);
  } catch (GRBException& e) {
    error_msg(errorstring, "Gurobi: Exception caught: %s\n", e.getMessage().c_str());
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  } catch (...) {
    error_msg(errorstring, "Gurobi: Unknown exception caught.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }
} /* doBranchAndBoundWithUserCutsGurobi (filename) */

void doBranchAndBoundWithUserCutsGurobi(const OsiSolverInterface* const solver,
    const OsiCuts* cuts, BBInfo& info, const bool addAsLazy) {
  std::string f_name;
  createTmpFileCopy(solver, f_name);
  doBranchAndBoundWithUserCutsGurobi(f_name.c_str(), cuts, info, addAsLazy);
  remove(f_name.c_str()); // remove temporary file
} /* doBranchAndBoundWithUserCutsGurobi (Osi) */
#endif /* VPC_USE_GUROBI */
