//============================================================================
// Name        : BBHelper.hpp
// Author      : akazachk
// Version     : 2017.03.16
// Description : Helper functions for branch-and-bound
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#pragma once

#include "typedefs.hpp"
#include "AdvCut.hpp"
#include "CbcModel.hpp"

struct BBInfo {
  double obj;
  double bound;
  long iters;
  long nodes;
  long root_passes;
  double first_cut_pass;
  double last_cut_pass;
  double root_time;
  double last_sol_time;
  double time;
}; /* BBInfo */

enum BBInfoEnum {
  OBJ_BB_INFO_IND,
  BOUND_BB_INFO_IND,
  ITERS_BB_INFO_IND,
  NODES_BB_INFO_IND,
  ROOT_PASSES_BB_INFO_IND,
  FIRST_CUT_PASS_BB_INFO_IND,
  LAST_CUT_PASS_BB_INFO_IND,
  ROOT_TIME_BB_INFO_IND,
  LAST_SOL_TIME_BB_INFO_IND,
  TIME_BB_INFO_IND,
  NUM_BB_INFO
};

const std::vector<std::string> BB_INFO_CONTENTS = {
    "OBJ", "BOUND", "ITERS", "NODES", "ROOT_PASSES", "FIRST_CUT_PASS", "LAST_CUT_PASS", "ROOT_TIME", "LAST_SOL_TIME", "TIME"
};

inline void initializeBBInfo(BBInfo& info, double opt = 0.) {
  info.obj = opt;
  info.bound = opt;
  info.iters = 0;
  info.nodes = 0;
  info.root_passes = 0;
  info.first_cut_pass = 0.;
  info.last_cut_pass = 0.;
  info.root_time = 0.;
  info.last_sol_time = 0.;
  info.time = 0.;
}

void updateBestBBInfo(BBInfo& min_info, const BBInfo& curr_info, const bool first);
void averageBBInfo(BBInfo& avg_info, const std::vector<BBInfo>& info);
void printBBInfo(const BBInfo& info, FILE* myfile, const bool print_blanks =
    false, const char SEP = ',');
void printBBInfo(const BBInfo& info_mycuts, const BBInfo& info_allcuts,
    FILE* myfile, const bool print_blanks = false, const char SEP = ',');
void createStringFromBBInfoVec(const std::vector<BBInfo>& vec_info,
    std::vector<std::string>& vec_str);

void doBranchAndBoundNoCuts(const OsiSolverInterface* const solver, BBInfo& info);
void doBranchAndBoundYesCuts(const OsiSolverInterface* const solver,
    BBInfo& info, AdvCuts& structCuts, const bool doCutSelection,
    const int numCutsToAddPerRound, const int maxRounds,
    const std::string logstring);

void generatePartialBBTree(CbcModel* cbc_model,
    const OsiSolverInterface* const solver, const int max_nodes,
    const int num_strong, const int num_before_trusted);

#ifdef VPC_USE_CPLEX
// CPLEX stuff
// Callable
void presolveModelWithCplexCallable(const char* f_name, double& presolved_opt, std::string& presolved_name);
void presolveModelWithCplexCallable(const OsiSolverInterface* const solver, double& presolved_opt, std::string& presolved_name);
void doBranchAndBoundWithCplexCallable(const char* f_name, BBInfo& info);
void doBranchAndBoundWithCplexCallable(const OsiSolverInterface* const solver, BBInfo& info);
void doBranchAndBoundWithUserCutsCplexCallable(const char* f_name,
    const OsiCuts* cuts, BBInfo& info, const bool addAsLazy = false);
void doBranchAndBoundWithUserCutsCplexCallable(const OsiSolverInterface* const solver,
    const OsiCuts* cuts, BBInfo& info, const bool addAsLazy = false);

#ifdef VPC_USE_CPLEX_CONCERT
// Concert
void doBranchAndBoundWithCplexConcert(const char* f_name, BBInfo& info);
void doBranchAndBoundWithCplexConcert(const OsiSolverInterface* const solver, BBInfo& info);
void doBranchAndBoundWithUserCutsCplexConcert(const char* f_name,
    const OsiCuts* cuts, BBInfo& info, const bool addAsLazy = false);
void doBranchAndBoundWithUserCutsCplexConcert(const OsiSolverInterface* const solver,
    const OsiCuts* cuts, BBInfo& info, const bool addAsLazy = false);
#endif /* VPC_USE_CPLEX_CONCERT */
#endif /* VPC_USE_CPLEX */

#ifdef VPC_USE_GUROBI
// Gurobi stuff
void presolveModelWithGurobi(const char* f_name, double& presolved_opt, std::string& presolved_name);
void presolveModelWithGurobi(const OsiSolverInterface* const solver, double& presolved_opt, std::string& presolved_name);
void doBranchAndBoundWithGurobi(const char* f_name, BBInfo& info, std::vector<double>* const solution = NULL);
void doBranchAndBoundWithGurobi(const OsiSolverInterface* const solver,
    BBInfo& info, std::vector<double>* const solution = NULL);
void doBranchAndBoundWithUserCutsGurobi(const char* f_name,
    const OsiCuts* cuts, BBInfo& info, const bool addAsLazy = false);
void doBranchAndBoundWithUserCutsGurobi(const OsiSolverInterface* const solver,
    const OsiCuts* cuts, BBInfo& info, const bool addAsLazy = false);
#endif /* VPC_USE_GUROBI */
