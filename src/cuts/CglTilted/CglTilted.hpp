// Last edit: 08/26/2015
//
// Name:     CglClosedPCut.hpp
// Author:   A. M. Kazachkov (based on file by F. Margot)
//           Tepper School of Business
//           email: akazachk@cmu.edu
// Date:     01/10/2015
//-----------------------------------------------------------------------------

#pragma once

#include "CglGICParam.hpp"
#include "CglCutGenerator.hpp"
#include "CoinWarmStartBasis.hpp"
#include "CoinFactorization.hpp"

#include "SolutionInfo.hpp"

#include "OsiRowCut.hpp"
#include "OsiCuts.hpp"

/* Debug output */
//#define NEWGEN_TRACE

/* Debug output: print optimal tableau */
//#define NEWGEN_TRACETAB

class CglTilted : public CglCutGenerator {

  friend void CglTiltedUnitTest(const OsiSolverInterface * siP,
				const std::string mpdDir);
public:

  /**@name generateCuts */
  //@{
  /** Generate cuts for the model of the solver
      interface si.

      Insert the generated cuts into OsiCuts cs.
  */
  virtual void generateCuts(const OsiSolverInterface & si, OsiCuts & cs,
      const CglTreeInfo info = CglTreeInfo());
  virtual void generateCuts(const OsiSolverInterface & si, OsiCuts & cs,
      const SolutionInfo& probData, const CglTreeInfo info = CglTreeInfo());
  virtual int genTiltedCuts(OsiCuts& cs, const SolutionInfo& probData,
      const int split_ind,
      std::vector<std::vector<std::vector<int> > >& splitVarForIneq,
      std::vector<std::vector<std::vector<int> > >& facetOfSplitForIneq,
      std::vector<std::vector<std::vector<double> > >& splitHighValueForIneq,
      std::vector<std::vector<double>>& overallLBForIneq);
  virtual void insertCutHelper(OsiRowCut& cut, OsiCuts& cs);
  virtual void updateMixingVectorsHelper(const int set_ind, const int ineq_ind,
      const int currFracVar, const double beta0, const double beta1,
      std::vector<std::vector<std::vector<int> > >& splitVarForIneq,
      std::vector<std::vector<std::vector<int> > >& facetOfSplitForIneq,
      std::vector<std::vector<std::vector<double> > >& splitHighValueForIneq,
      std::vector<std::vector<double>>& overallLBForIneq);
  virtual bool genTiltedObj(OsiCuts& cs, OsiRowCut& cut, const double beta0,
      const double beta1, const int currFracVar, const double floorxk,
      const double ceilxk);
  virtual bool genTiltedBound(OsiCuts& cs, OsiRowCut& cut,
      PointCutsSolverInterface* betaSolverFloor,
      PointCutsSolverInterface* betaSolverCeil, double& beta0, double& beta1,
      const int tmph, const int currFracVar, const double ceilxk,
      const bool lb_flag, const bool optimizeBothSides, const bool onFloor);
  virtual bool genTiltedRow(OsiCuts& cs, OsiRowCut& cut,
      PointCutsSolverInterface* betaSolverFloor,
      PointCutsSolverInterface* betaSolverCeil, double& beta0, double& beta1,
      const int row, const int currFracVar, const double floorxk,
      const double ceilxk, const std::vector<double>& zeroObj,
      const bool optimizeBothSides, const bool onFloor);
  virtual bool genTiltedSubspaceCut(OsiCuts& cs, OsiRowCut& cut,
      PointCutsSolverInterface* betaSolverFloor,
      PointCutsSolverInterface* betaSolverCeil, double& beta0, double& beta1,
      const OsiRowCut* cutToLift, const int currFracVar, const double floorxk,
      const double ceilxk, const std::vector<double>& zeroObj,
      const bool optimizeBothSides, const bool onFloor);
  virtual bool genMixedIneq(OsiCuts& cs, OsiRowCut& cut, const int ineq,
      const double overallLB, const std::vector<double>& unsortedBoundFromSplit,
      const std::vector<int>& splitVar, const std::vector<int>& sortedOrder,
      const std::vector<int>& facetOfSplit, const bool isColLB,
      const bool isColUB, const bool isRow);
//  bool boundIsActive(const LiftGICsSolverInterface* tmpSolver, const int tmph,
//      double& bound, bool& lb_flag);
  bool hplaneIsActive(const PointCutsSolverInterface* tmpSolver,
      const int tmprow, const bool lowerBound = true);

  /// For compatibility with CglCutGenerator (const method)
  virtual void generateCuts(const OsiSolverInterface & si, OsiCuts & cs,
			    const CglTreeInfo info = CglTreeInfo()) const;

  /// Return true if needs optimal basis to do cuts (will return true)
  virtual bool needsOptimalBasis() const;
  //@}
  
  /**@name Public Methods */
  //@{

  // Set the parameters to the values of the given CglGMIParam object.
  void setParam(const CglGICParam &source); 
  // Return the CglGICParam object of the generator. 
  inline CglGICParam getParam() const {return param;}
//  inline int getMaxStrongBranchingValue() const {
//    return max_strong_branching_value;
//  }

  //@}

  /**@name Constructors and destructors */
  //@{
  /// Default constructor 
  CglTilted();

  // Constructor with specified parameters
  CglTilted(const CglGICParam&);

  /// Copy constructor 
  CglTilted(const CglTilted &);

  /// Clone
  virtual CglCutGenerator * clone() const;

  /// Assignment operator 
  CglTilted & operator=(const CglTilted& rhs);
  
  /// Destructor 
  virtual ~CglTilted();
  /// Create C++ lines to get to current state
  virtual std::string generateCpp( FILE * fp);

  //@}
    
private:
  
  // Private member methods

/**@name Private member methods */

  //@{

  // Method generating the cuts after all CglClosedPCut members are properly set.
  void generateCuts(OsiCuts & cs, const SolutionInfo& probData);

  /// print a vector of integers
  void printvecINT(const char *vecstr, const int *x, int n) const;
  /// print a vector of doubles: dense form
  void printvecDBL(const char *vecstr, const double *x, int n) const;
  /// print a vector of doubles: sparse form
  void printvecDBL(const char *vecstr, const double *elem, const int * index, 
		   int nz) const;

  //@}
  
  // Private member data

/**@name Private member data */

  //@{

  /// Object with CglGICParam members. 
  CglGICParam param;
  
  /// Pointer on solver. Reset by each call to generateCuts().
  OsiSolverInterface *solver;

  /// Pointer on point to separate. Reset by each call to generateCuts().
  const double *xlp;

  /// Pointer on matrix of coefficient ordered by rows. 
  /// Reset by each call to generateCuts().
  const CoinPackedMatrix *byRow;

  /// Pointer on matrix of coefficient ordered by columns. 
  /// Reset by each call to generateCuts().
  const CoinPackedMatrix *byCol;

  OsiCuts structSICs;

  // Strong branching value
//  double max_strong_branching_value = 0.;
  double strong_branching_lb = std::numeric_limits<double>::max();
  double strong_branching_ub = std::numeric_limits<double>::lowest();

  // For time keeping
  Stats timeStats;

  const bool splitVarDeleted = param.getParamVal(
      CglGICParamNamespace::ParamIndices::SPLIT_VAR_DELETED_PARAM_IND);
  //@}
};

//#############################################################################
/** A function that tests the methods in the CglClosedPCut class. The
    only reason for it not to be a member method is that this way it doesn't
    have to be compiled into the library. And that's a gain, because the
    library should be compiled with optimization on, but this method should be
    compiled with debugging. */
void CglTiltedUnitTest(const OsiSolverInterface * siP,
			 const std::string mpdDir );
