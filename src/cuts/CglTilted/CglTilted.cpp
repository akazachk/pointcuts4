// Last edit: 08/26/2015
//
// Name:     CglTilted.hpp
// Author:   A. M. Kazachkov (based on file from F. Margot)
//           Tepper School of Business
//           email: akazachk@cmu.edu
// Date:     01/10/2015
//-----------------------------------------------------------------------------

#if defined(_MSC_VER)
// Turn off compiler warning about long names
#  pragma warning(disable:4786)
#endif
#include <cstdlib>
#include <cstdio>
#include <cmath>  // Remember to put std::abs
#include <cfloat>
#include <cassert>
#include <iostream>
#include <fenv.h>
#include <climits>

#include <CglTilted.hpp>
#include "OsiSolverInterface.hpp"

#include "CoinHelperFunctions.hpp"
#include "CoinPackedVector.hpp"
#include "CoinPackedMatrix.hpp"
#include "CoinIndexedVector.hpp"
#include "OsiRowCutDebugger.hpp"
#include "CoinFactorization.hpp"
#include "CoinFinite.hpp"

#include "GlobalConstants.hpp"
#include "Utility.hpp"
#include "CutHelper.hpp"

#include "CglGomory.hpp"
#include "CglSIC.hpp"

// Useful classes and such for GICs
// (the AdvCut and AdvCuts classes could probably be removed eventually,
// but they have some useful features wrt OsiCuts)
#include "AdvCut.hpp"

/***************************************************************************/
CglTilted::CglTilted() :
  CglCutGenerator(), solver(NULL), xlp(NULL), byRow(NULL), byCol(NULL) {
    param.setParams();
}

/***************************************************************************/
CglTilted::CglTilted(const CglGICParam& parameters) :
  CglCutGenerator(), param(parameters), solver(NULL), xlp(NULL), byRow(NULL), byCol(NULL) {
  // Nothing to do here
}

/***************************************************************************/
CglTilted::CglTilted(const CglTilted& rhs) :
  CglCutGenerator(rhs), param(rhs.param), solver(rhs.solver), xlp(rhs.xlp), byRow(
      rhs.byRow), byCol(rhs.byCol) {
  // Nothing to do here
}

/***************************************************************************/
CglTilted & CglTilted::operator=(const CglTilted& rhs) {
  if (this != &rhs) {
    CglCutGenerator::operator=(rhs);
    param = rhs.param;
    solver = rhs.solver;
    xlp = rhs.xlp;
    byRow = rhs.byRow;
    byCol = rhs.byCol;
  }
  return *this;
}

/***************************************************************************/
CglTilted::~CglTilted() {

}

/*********************************************************************/
CglCutGenerator *
CglTilted::clone() const {
  return new CglTilted(*this);
}

/************************************************************************/
/**
 * Kludge to be able to modify the CglTilted object if it is const
 */
void CglTilted::generateCuts(const OsiSolverInterface & si, OsiCuts & cs,
    const CglTreeInfo info) const {
  CglTilted temp(*this);
  temp.generateCuts(si, cs, info);
} /* generateCuts */

/************************************************************************/
/**
 * When SolutionInfo not provided, need to make it ourselves
 */
void CglTilted::generateCuts(const OsiSolverInterface &si, OsiCuts & cs,
    const CglTreeInfo info) {
  solver = const_cast<OsiSolverInterface *>(&si); // Removes const-ness
  SolutionInfo tmpSolnInfo(solver,
    param.paramVal[ParamIndices::NUM_CGS_PARAM_IND],
    param.paramVal[ParamIndices::SUBSPACE_PARAM_IND], true);
  if (tmpSolnInfo.numSplits == 0) {
    return;
  }
  if ((param.getParamVal(ParamIndices::CGS_PARAM_IND) > 0)
      && (tmpSolnInfo.numSplits < 2)) {
    return;
  }
  if (!param.finalizeParams(tmpSolnInfo.numNB, tmpSolnInfo.numSplits)) {
    error_msg(errstr,
        "Issue finalizing parameters (no rays to cut or issue with parameter combinations).\n");
    writeErrorToLog(errstr, GlobalVariables::log_file);
    exit(1);
  }
  if (param.paramVal[NUM_CGS_PARAM_IND]
      > tmpSolnInfo.numFeasSplits) {
    warning_msg(warnstring,
        "Number of requested cut generating sets is %d, but the maximum number we can choose is %d. Reducing the requested number of splits.\n",
        param.paramVal[NUM_CGS_PARAM_IND],
        tmpSolnInfo.numFeasSplits);
    param.paramVal[NUM_CGS_PARAM_IND] =
        tmpSolnInfo.numFeasSplits;
  }

  //GlobalVariables::param = this->param;
  generateCuts(si, cs, tmpSolnInfo, info);
} /* generateCuts */

/************************************************************************/
void CglTilted::generateCuts(const OsiSolverInterface &si, OsiCuts & cs,
    const SolutionInfo& probData, const CglTreeInfo info) {
  solver = const_cast<OsiSolverInterface *>(&si); // Removes const-ness
  if (solver == NULL) {
    printf("### WARNING: CglTilted::generateCuts(): no solver available.\n");
    return;
  }

  if (!solver->optimalBasisIsAvailable()) {
    printf(
        "### WARNING: CglTilted::generateCuts(): no optimal basis available.\n");
    return;
  }

  xlp = solver->getColSolution();
  generateCuts(cs, probData);
  try {
    dynamic_cast<AdvCuts&>(cs).setCuts();
  } catch (std::exception& e) {
  }
}

/************************************************************************/
/**
 * Generate cuts when SolutionInfo is given
 */
void CglTilted::generateCuts(OsiCuts &cs, const SolutionInfo& probData) {
#ifdef TRACE
  printf(
      "\n############### Starting tilted cut generation. ###############\n");
#endif

  // Timing
  timeStats.register_name("TOTAL");

  if (reachedTimeLimit(timeStats, "TOTAL", param.getTIMELIMIT())) { // make sure that time is available
    return;
  }

  // For mixing
  // Pretty huge storage overhead
  // The first set of inequalities will be lower-bounds on the columns
  // The second set of inequalities will be upper-bounds on the columns
  // The third set will be improving the row lower-bounds
  // where the rows are possibly flipped (the equality ones are ignored)
  // The last entry of the third set of inequalities will be the objective
  // Actually the fourth set will eventually exist --- tilting Gomory cuts
  const int numSetsOfIneq = 3;
  std::vector<std::vector<std::vector<int> > > splitVarForIneq(numSetsOfIneq);
  std::vector<std::vector<std::vector<int> > > facetOfSplitForIneq(numSetsOfIneq);
  std::vector<std::vector<std::vector<double> > > splitHighValueForIneq(numSetsOfIneq);
  std::vector<std::vector<double> > overallLBForIneq(numSetsOfIneq);
  std::vector<int> numIneqsToTiltPerSet = { probData.numCols, probData.numCols, probData.numRows + 1 };

  if (param.getParamVal(ParamIndices::MIX_PARAM_IND)) {
    for (int set_ind = 0; set_ind < numSetsOfIneq; set_ind++) {
      splitVarForIneq[set_ind].resize(numIneqsToTiltPerSet[set_ind]);
      facetOfSplitForIneq[set_ind].resize(numIneqsToTiltPerSet[set_ind]);
      splitHighValueForIneq[set_ind].resize(numIneqsToTiltPerSet[set_ind]);
      overallLBForIneq[set_ind].resize(numIneqsToTiltPerSet[set_ind]);

      for (int ineq_ind = 0; ineq_ind < numIneqsToTiltPerSet[set_ind]; ineq_ind++) {
        splitVarForIneq[set_ind][ineq_ind].reserve(probData.numFeasSplits);
        facetOfSplitForIneq[set_ind][ineq_ind].reserve(probData.numFeasSplits);
        splitHighValueForIneq[set_ind][ineq_ind].reserve(probData.numFeasSplits);

        double ineqLBFromSolver;
        switch (set_ind) {
          case 0:
            ineqLBFromSolver = solver->getColLower()[ineq_ind];
            break;
          case 1:
            ineqLBFromSolver = -1. * solver->getColUpper()[ineq_ind];
            break;
          case 2:
            if (ineq_ind == solver->getNumRows()) {
              // Objective value
              ineqLBFromSolver = solver->getObjValue();
            } else {
              double mult, rowLB;
              if (solver->getRowSense()[ineq_ind] == 'G') {
                mult = 1.0;
                rowLB = solver->getRowLower()[ineq_ind];
              } else {
                mult = -1.0;
                rowLB = solver->getRowUpper()[ineq_ind];
              }
              ineqLBFromSolver = mult * rowLB;
            }
            break;
        }

        overallLBForIneq[set_ind][ineq_ind] = ineqLBFromSolver;
      }
    }
  } /* if mix, set up for mixing */

  for (int split_ind = 0; split_ind < probData.numFeasSplits; split_ind++) {
    if (reachedTimeLimit(timeStats, "TOTAL", param.getTIMELIMIT())) { // make sure that time is available
      break;
    }
    genTiltedCuts(cs, probData, split_ind, splitVarForIneq, facetOfSplitForIneq, splitHighValueForIneq, overallLBForIneq);
  }

  if (param.getParamVal(ParamIndices::MIX_PARAM_IND)) {
    // Let:
    //   \sigma = set of split used (variables indexed starting at 1)
    //   j \in \sigma
    //   \beta_j^0 = min { hx : x \in P, x_j = floor(\bar x_j) }
    //   \beta_j^1 = min { hx : x \in P, x_j = ceil(\bar x_j) }
    //   B_j = max { \beta_j^0, \beta_j^1 }
    //   b_j = min { \beta_j^0, \beta_j^1 }
    //   B_0 = max { b_j : j \in \sigma }
    //
    // Sort splits in increasing order by B_j,
    // i.e., j < j' implies B_j \le B_{j'}.
    // Ignore splits with B_j < B_0.
    // Let T0 = { t \in \sigma : B_t = \beta_t^0 }, T1 = \sigma \ T0.
    //
    // Generate the cut:
    //   hx >= B_0
    //         + \sum_{t \in T0} (B_t - B_{t-1}) (\ceil{\bar x_t} - x_t)
    //         + \sum_{t \in T1} (B_t - B_{t-1}) (x_t - \floor{\bar x_t}).

#ifdef TRACE
    printf("\n## Performing mixing procedure. ##\n");
#endif

    // Cut to generate
    OsiRowCut cut;
    std::vector<int> sortIndex(probData.numFeasSplits);
    for (int split_ind = 0; split_ind < probData.numFeasSplits; split_ind++) {
      sortIndex[split_ind] = split_ind;
    }
    for (int set_ind = 0; set_ind < numSetsOfIneq; set_ind++) {
      for (int ineq_ind = 0; ineq_ind < numIneqsToTiltPerSet[set_ind]; ineq_ind++) {
        if (reachedCutLimit(0, cs.sizeCuts())) {
          return;
        }
        if (reachedTimeLimit(timeStats, "TOTAL", param.getTIMELIMIT())) { // make sure that time is available
          return;
        }
        const int numSplitsForIneq = splitHighValueForIneq[set_ind][ineq_ind].size();
        if (numSplitsForIneq == 0) {
          continue;
        }
        std::vector<int> sortIndexForIneq(sortIndex.begin(),
            sortIndex.begin() + numSplitsForIneq);
        sort(sortIndexForIneq.begin(), sortIndexForIneq.end(),
            index_cmp_asc<double*>(
                splitHighValueForIneq[set_ind][ineq_ind].data()));

        genMixedIneq(cs, cut, ineq_ind, overallLBForIneq[set_ind][ineq_ind],
            splitHighValueForIneq[set_ind][ineq_ind],
            splitVarForIneq[set_ind][ineq_ind], sortIndexForIneq,
            facetOfSplitForIneq[set_ind][ineq_ind], (set_ind == 0),
            (set_ind == 1), (set_ind == 2));
      } /* loop over inequalities in set */
    } /* loop over ineq sets */
  } /* if mix, generate mixed inequalities */
} /* generateCuts */

/************************************************************************/
/**
 */
/**
 * Given column col, corresponding to a variable in the fractional core,
 * we optimize over floor and ceiling and use these values to generate
 * a tilted cut from the objective and each of the inequalities selected.
 *
 * Strengthening a la Gomory may be applied to the disjunction beforehand.
 * (It would be faster to do monoidal strengthening but not yet implemented.)
 *
 * Details
 * 0. Suppose the disjunction is \pi x <= pi_0 or \pi x >= \pi_1, and we are tilting h.
 * 1. Let
 *      \beta_0 = min \{ hx : x \in P_0^k \}
 *    and
 *      \beta_1 = min \{ hx : x \in P_1^k \}.
 * 2. We are looking for the cut alpha x >= beta such that
 *      \{ x : \alpha x = \beta, \pi x = \pi_q \} (for q = 0 or 1)
 *      = \{ x : hx = \beta_q \}.
 * 3. Take any k such that \pi_k \ne 0. Then:
 *      \alpha x
 *      = \sum_{j \ne k} \alpha_j x_j + \alpha_k (1/\pi_k) (\pi_q - \sum_j \pi_j x_j)
 *    This implies that
 *      \alpha x = \beta
 *    is equivalent to
 *      \sum_j (\alpha_j \pi_k - \alpha_k \pi_j) x_j = \pi_k \beta - \alpha_k \pi_q.
 *    Similarly, we can reduce \{ x : hx = \beta_q, \pi_x = \pi_q \} to
 *      \sum_j (h_j \pi_k - h_k \pi_j) x_j = \pi_k \beta_q - h_k \pi_q.
 * 4. Take a guess! Suppose
 *      \pi_k \beta_q - h_k \pi_q = \pi_k \beta - \alpha_k \pi_q.
 *    Thus,
 *      \beta = (1/\pi_k) (\pi_k \beta_q + (\alpha_k - h_k) \pi_q).
 *    Since this is true for q = 0 and 1, we have
 *      (1/\pi_k) (\pi_k \beta_0 + (\alpha_k - h_k) \pi_0)
 *      = (1/\pi_k) (\pi_k \beta_1 + (\alpha_k - h_k) \pi_1).
 *    This implies
 *      \alpha_k (\pi_1 - \pi_0) = h_k (\pi_1 - \pi_0) + \pi_k (\beta_0 - \beta_1)
 *    or
 *      \alpha_k = h_k + \pi_k (\beta_0 - \beta_1) / (\pi_1 - \pi_0).
 *    In fact, since k is essentially arbitrary, we can conclude this is the value for all
 *    indices (including the ones in which \pi_k = 0, as then \alpha_k = h_k and still works).
 * 5. From this, we get that
 *      \beta = (1/\pi_k) (\pi_k \beta_q + \pi_k \pi_q (\beta_0 - \beta_1) / (\pi_1 - \pi_0))
 *      = \beta_q + \pi_q (\beta_0 - \beta_1) / (\pi_1 - \pi_0).
 *
 * All this simplifies vastly for simple splits of course.
 * Then only one \pi entry is non-zero, and \pi_1 - \pi_0 is 1.
 * When we do strengthening, we will use (where \bar a are the entries of the split row in basis)
 *            { 1                 if j = split variable
 *   \pi_j =  { \floor{\bar a_j}  if j \in N \cap I and f_j \le f_0
 *            { \ceil{\bar a_j}   if j \in N \cap I and f_j > f_0
 *            { 0                 otherwise.
 *
 *  @return Number of cuts generated (none if, for ex, in subspace and both sides infeas)
 */
int CglTilted::genTiltedCuts(OsiCuts& cs, const SolutionInfo& probData,
    const int split_ind,
    std::vector<std::vector<std::vector<int> > >& splitVarForIneq,
    std::vector<std::vector<std::vector<int> > >& facetOfSplitForIneq,
    std::vector<std::vector<std::vector<double> > >& splitHighValueForIneq,
    std::vector<std::vector<double>>& overallLBForIneq) {
  if (reachedCutLimit(cs.sizeCuts(), cs.sizeCuts())) {
    return 0;
  }
  const int currFracVar = probData.feasSplitVar[split_ind];
#ifdef TRACE
  printf("\n## Generating tilted cuts for fractional variable %d (%s). ##\n",
      currFracVar, this->solver->getColName(currFracVar).c_str());
#endif
  const int num_cuts_start = cs.sizeCuts();

  // Get the column corresponding to the split variable
  const CoinShallowPackedVector fracCol = solver->getMatrixByCol()->getVector(
      currFracVar);
  const double xk = this->solver->getColSolution()[currFracVar];
  std::vector<double> currRoundedVal = { std::floor(xk), std::ceil(xk) };
  const double f0 = xk - currRoundedVal[0];
  std::vector<double> splitMultiplier = { -1.0, 1.0 };
//  const double floorxk = floor(xk);
//  const double ceilxk = ceil(xk);
//  currRoundedVal[0] = std::floor(xk);
//  currRoundedVal[1] = std::ceil(xk);

  std::vector<double> structVarsBInv(solver->getNumCols()), slackVarsBInv(
      solver->getNumRows());

  // Set up facet information
  const int num_disj_terms_cgs = 2;
  std::vector<std::vector<int> > facetIndices(num_disj_terms_cgs);
  std::vector<std::vector<double> > facetCoeff(num_disj_terms_cgs);
  std::vector<double> facetRHS(num_disj_terms_cgs);
  std::string cgsName;

  if (param.getParamVal(ParamIndices::STRENGTHEN_PARAM_IND) > 0) {
    solver->enableFactorization();
    solver->getBInvARow(probData.rowOfVar[currFracVar], &structVarsBInv[0],
        &slackVarsBInv[0]);
    solver->disableFactorization();

    for (int d = 0; d < num_disj_terms_cgs; d++) {
      facetIndices[d].reserve(this->solver->getNumCols());
      facetCoeff[d].reserve(this->solver->getNumCols());
      facetRHS[d] = splitMultiplier[d] * currRoundedVal[d];
    }
    bool curr_split_inserted = false;
    for (int nb_ind = 0; nb_ind < probData.numNB; nb_ind++) {
      const int nb_var = probData.nonBasicVarIndex[nb_ind];

      // Slack variables should be skipped
      if (nb_var >= solver->getNumCols()) {
        break;
      }

      // Skip continuous variables
      if (!solver->isInteger(nb_var)) {
        continue;
      }

      // Insert the fractional variable where appropriate
      if (!curr_split_inserted && (nb_var > currFracVar)) {
        for (int d = 0; d < num_disj_terms_cgs; d++) {
          facetIndices[d].push_back(currFracVar);
          facetCoeff[d].push_back(splitMultiplier[d]);
        }
        curr_split_inserted = true;
      }

      const double binv_val =
          (nb_var < probData.numCols) ?
              structVarsBInv[nb_var] : slackVarsBInv[nb_var - probData.numCols];
      const double fj = binv_val - std::floor(binv_val); // may need to be negated
      double zj; // integer value to be assigned
      if (fj <= f0) {
        zj = std::floor(binv_val);
      } else {
        zj = std::ceil(binv_val);
      }
      for (int t = 0; t < num_disj_terms_cgs; t++) {
        facetIndices[t].push_back(nb_var);
        facetCoeff[t].push_back(splitMultiplier[t] * zj);
      }
    }
    if (!curr_split_inserted) {
      for (int t = 0; t < num_disj_terms_cgs; t++) {
        facetIndices[t].push_back(currFracVar);
        facetCoeff[t].push_back(splitMultiplier[t]);
      }
    }
  } /* should the GMI disjunction be used? */
  else {
    for (int d = 0; d < 2; d++) {
      facetIndices[d].resize(1, currFracVar);
      facetCoeff[d].resize(1, splitMultiplier[d]);
      facetRHS[d] = splitMultiplier[d] * currRoundedVal[d];
    }
  } /* else, use simple splits */

  for (int t = 0; t < num_disj_terms_cgs; t++) {
    if (t > 0) {
      cgsName += " V ";
    }
    cgsName += "(";
//    for (int i = 0; i < 1; i++) {
//      if (i > 0) {
//        cgsName += "; ";
//      }
    for (int coeff_ind = 0; coeff_ind < (int) facetIndices[t].size();
        coeff_ind++) {
      cgsName += (facetCoeff[t][coeff_ind] > 0) ? "+" : "-";
      cgsName += "x";
      cgsName += std::to_string(facetIndices[t][coeff_ind]);
    }
    cgsName += " >= ";
    cgsName += std::to_string((int) facetRHS[t]);
//    }
    cgsName += ")";
  }
  printf("cgs name: %s\n", cgsName.c_str());
//  return 0;

  // Set up solvers for facets of S
  std::vector<PointCutsSolverInterface*> tmpSolver(num_disj_terms_cgs);
  std::vector<bool> disjTermFeas(num_disj_terms_cgs);
  std::vector<double> disjTermObjVal(num_disj_terms_cgs);

  for (int t = 0; t < num_disj_terms_cgs; t++) {
#ifdef TRACE
    printf(
        "\n## Calculating optimal solution on disj term %d of cgs %d/%d with name %s. ##\n",
        t, split_ind + 1, probData.numFeasSplits, cgsName.c_str());
#endif
    tmpSolver[t] = dynamic_cast<PointCutsSolverInterface*>(solver->clone());
    setClpParameters(tmpSolver[t]);
    tmpSolver[t]->disableFactorization();

    if (t < 2 && splitVarDeleted) {
      // Get the column corresponding to the split variable
      const CoinShallowPackedVector fracCol =
          solver->getMatrixByCol()->getVector(facetIndices[t][0]);
      disjTermFeas[t] = removeFixedVar(tmpSolver[t], facetIndices[t][0],
          facetRHS[t], fracCol);
    } else {
      disjTermFeas[t] = fixDisjTerm(tmpSolver[t], facetIndices[t],
          facetCoeff[t], facetRHS[t], true);
    }
    disjTermObjVal[t] = tmpSolver[t]->getObjValue();
    if (splitVarDeleted && disjTermFeas[t]) {
      disjTermObjVal[t] += solver->getObjCoefficients()[currFracVar]
          * currRoundedVal[t];
    }
  }

  // Cut to generate
  OsiRowCut cut;

  // If exactly one side of the split is feasible, simply add the strongest cut available
  // If both infeasible, and we are in the subspace, then it is okay, this could happen.
  // If we are not in the subspace, then this is a certificate of infeasibility of the original problem.
  if (!disjTermFeas[0] && !disjTermFeas[1]) {
    if (!param.getParamVal(ParamIndices::SUBSPACE_PARAM_IND)) {
      error_msg(errstr,
          "Solver is primal infeasible when variable %d with name %s and value %f is fixed to floor %f and when it is fixed to ceil %f.\n",
          currFracVar, this->solver->getColName(currFracVar).c_str(), xk,
          currRoundedVal[0], currRoundedVal[1]);
      writeErrorToLog(errstr, GlobalVariables::log_file);
      exit(1);
    }
    for (int t = 0; t < num_disj_terms_cgs; t++) {
      if (tmpSolver[t]) {
        delete tmpSolver[t];
      }
    }
    return 0;
  } else if (!disjTermFeas[0] || !disjTermFeas[1]) {
    if (!reachedCutLimit(cs.sizeCuts() - num_cuts_start, cs.sizeCuts())) {
      genOneSidedOsiRowCut(cut, disjTermFeas[0], this->solver, currFracVar);
      insertCutHelper(cut, cs);
    }
    for (int t = 0; t < num_disj_terms_cgs; t++) {
      if (tmpSolver[t]) {
        delete tmpSolver[t];
      }
    }
    return cs.sizeCuts() - num_cuts_start;
  } else {
    // Check if reached cut limit
    if (reachedCutLimit(cs.sizeCuts() - num_cuts_start, cs.sizeCuts())) {
      for (int t = 0; t < num_disj_terms_cgs; t++) {
        if (tmpSolver[t]) {
          delete tmpSolver[t];
        }
      }
      return cs.sizeCuts() - num_cuts_start;
    }

    // Otherwise, both sides of the cut are feasible, and we proceed
    // We will generate the depth-0 cut, unless, if it is a new cut
    if (param.paramVal[ParamIndices::TILTED_DEPTH_PARAM_IND] <= 3) {
      genTiltedObj(cs, cut, disjTermObjVal[0], disjTermObjVal[1], currFracVar,
          currRoundedVal[0], currRoundedVal[1]);

      // Update mixing vectors
      // Need the split index, updated lower-bound, and best upper-bound
      if (param.getParamVal(ParamIndices::MIX_PARAM_IND)) {
        const int set_ind = 2;
        const int ineq_ind = solver->getNumRows();
        updateMixingVectorsHelper(set_ind, ineq_ind, currFracVar,
            disjTermObjVal[0], disjTermObjVal[1], splitVarForIneq,
            facetOfSplitForIneq, splitHighValueForIneq, overallLBForIneq);
      }

      if (param.paramVal[ParamIndices::TILTED_DEPTH_PARAM_IND] < 1) {
        for (int t = 0; t < num_disj_terms_cgs; t++) {
          if (tmpSolver[t]) {
            delete tmpSolver[t];
          }
        }
        return cs.sizeCuts() - num_cuts_start;
      }
    } /* finished generating tilted objective cuts */

    // Check if reached cut limit
    if (reachedCutLimit(cs.sizeCuts() - num_cuts_start, cs.sizeCuts())) {
      for (int t = 0; t < num_disj_terms_cgs; t++) {
        if (tmpSolver[t]) {
          delete tmpSolver[t];
        }
      }
      return cs.sizeCuts() - num_cuts_start;
    }

    const bool check_activity_flag =
        (param.paramVal[ParamIndices::TILTED_DEPTH_PARAM_IND] == 1);

    // This involves choosing a hyperplane tight at the optimal point on one facet
    // and making it valid for the other facet
    // Make copies of tmpSolverFloor and tmpSolverCeil to use for finding beta0 and beta1
    std::vector<PointCutsSolverInterface*> betaSolver(num_disj_terms_cgs);
    std::vector<double> zeroObj(tmpSolver[0]->getNumCols(), 0.0); // Same as # cols of tmpSolver[1]
    for (int t = 0; t < num_disj_terms_cgs; t++) {
      betaSolver[t] = dynamic_cast<PointCutsSolverInterface*>(tmpSolver[t]->clone());
      setClpParameters(betaSolver[t]);
//      betaSolver[t]->disableFactorization(); // caused trouble... not sure why; would go away with tmpSolver[t]->enableFactorization(), or even disableFactorization() on that after
      // Zero out the objective
      betaSolver[t]->setObjective(zeroObj.data());
    }

    // Keep the bound for each hyperplane on each facet of the split
    std::vector<int> numIneqsToTiltPerSet = { probData.numCols, probData.numCols,
          probData.numRows + 1 };
    const int numSetsOfIneq = 3;
    std::vector<std::vector<std::vector<double> > > boundForIneqOnFacet(numSetsOfIneq);
    for (int set_ind = 0; set_ind < numSetsOfIneq; set_ind++) {
      boundForIneqOnFacet[set_ind].resize(numIneqsToTiltPerSet[set_ind]);
      for (int ineq_ind = 0; ineq_ind < numIneqsToTiltPerSet[set_ind]; ineq_ind++) {
        boundForIneqOnFacet[set_ind][ineq_ind].resize(num_disj_terms_cgs, -1.);
      }
    } // TODO Finish implementing this speed-up idea.

    // Generate cuts from bounds and inequalities
    if (param.paramVal[ParamIndices::TILTED_DEPTH_PARAM_IND] <= 3) {
      for (int h = 0; h < solver->getNumRows() + solver->getNumCols(); h++) {
        // Check if reached cut limit
        if (reachedCutLimit(cs.sizeCuts() - num_cuts_start, cs.sizeCuts())) {
          for (int t = 0; t < num_disj_terms_cgs; t++) {
            if (tmpSolver[t]) {
              delete tmpSolver[t];
            }
            if (betaSolver[t]) {
              delete betaSolver[t];
            }
          }
          return cs.sizeCuts() - num_cuts_start;
        }

        if (h == currFracVar) {
          continue;
        }

        // Adjust for deleted variable
        const int tmph = (splitVarDeleted && (h > currFracVar)) ? h - 1 : h;

        // Check whether this hyperplane is tight, and then try to get a cut
        if (h < solver->getNumCols()) {
          // Do this for both the lower and upper bounds
          for (int b = 0; b < 2; b++) {
            bool lb_flag = (b == 0);
            // Tilt if !check_for__activity or is active on floor solver
            bool can_do_mixing = false;
            double beta0, beta1;
            if (!check_activity_flag
                || hplaneIsActive(tmpSolver[0], tmph, lb_flag)) {
              // If !check_activity_flag, we will optimize both sides in any case
              can_do_mixing = genTiltedBound(cs, cut, betaSolver[0], betaSolver[1], beta0,
                  beta1, tmph, currFracVar, currRoundedVal[1], lb_flag,
                  !check_activity_flag, true);
            } else if (hplaneIsActive(tmpSolver[1], tmph, lb_flag)) {
              // If not active on floor, then maybe active on ceiling
              can_do_mixing = genTiltedBound(cs, cut, betaSolver[0],
                  betaSolver[1], beta0, beta1, tmph, currFracVar,
                  currRoundedVal[1], lb_flag, false, false);
            }

            // Update mixing vectors
            if (param.getParamVal(ParamIndices::MIX_PARAM_IND) && can_do_mixing) {
              const int set_ind = b;
              const int ineq_ind = h;
              updateMixingVectorsHelper(set_ind, ineq_ind, currFracVar,
                  beta0, beta1, splitVarForIneq, facetOfSplitForIneq,
                  splitHighValueForIneq, overallLBForIneq);
            }

            // Check if reached cut limit
            if (reachedCutLimit(cs.sizeCuts() - num_cuts_start,
                cs.sizeCuts())) {
              for (int t = 0; t < num_disj_terms_cgs; t++) {
                if (tmpSolver[t]) {
                  delete tmpSolver[t];
                }
                if (betaSolver[t]) {
                  delete betaSolver[t];
                }
              }
              return cs.sizeCuts() - num_cuts_start;
            }
          }
        } else {
          const int row = h - solver->getNumCols();
          // Check if it is tight at p^0, or activate anyway if !check_activity_flag
          double beta0, beta1;
          bool can_do_mixing = false;
          if (!check_activity_flag || hplaneIsActive(tmpSolver[0], tmph)) {
            can_do_mixing = genTiltedRow(cs, cut, betaSolver[0], betaSolver[1],
                beta0, beta1, row, currFracVar, currRoundedVal[0],
                currRoundedVal[1], zeroObj, !check_activity_flag, true);
          } else if (hplaneIsActive(tmpSolver[1], tmph)) {
            can_do_mixing = genTiltedRow(cs, cut, betaSolver[0], betaSolver[1], beta0, beta1,
                row, currFracVar, currRoundedVal[0], currRoundedVal[1], zeroObj,
                false, false);
          }

          // Update mixing vectors
          if (param.getParamVal(ParamIndices::MIX_PARAM_IND) && can_do_mixing) {
            const int set_ind = 2;
            const int ineq_ind = row;
            updateMixingVectorsHelper(set_ind, ineq_ind, currFracVar, beta0,
                beta1, splitVarForIneq, facetOfSplitForIneq,
                splitHighValueForIneq, overallLBForIneq);
          }

          // Check if reached cut limit
          if (reachedCutLimit(cs.sizeCuts() - num_cuts_start, cs.sizeCuts())) {
            for (int t = 0; t < num_disj_terms_cgs; t++) {
              if (tmpSolver[t]) {
                delete tmpSolver[t];
              }
              if (betaSolver[t]) {
                delete betaSolver[t];
              }
            }
            return cs.sizeCuts() - num_cuts_start;
          }
        }
      }
    } /* generating cuts from col bounds and rows; if TILTED_DEPTH <= 3 */

    // Check if reached cut limit
    if (reachedCutLimit(cs.sizeCuts() - num_cuts_start, cs.sizeCuts())) {
      for (int t = 0; t < num_disj_terms_cgs; t++) {
        if (tmpSolver[t]) {
          delete tmpSolver[t];
        }
        if (betaSolver[t]) {
          delete betaSolver[t];
        }
      }
      return cs.sizeCuts() - num_cuts_start;
    }

    if (param.paramVal[ParamIndices::TILTED_DEPTH_PARAM_IND] < 3) {
      for (int t = 0; t < num_disj_terms_cgs; t++) {
        if (tmpSolver[t]) {
          delete tmpSolver[t];
        }
        if (betaSolver[t]) {
          delete betaSolver[t];
        }
      }
      return cs.sizeCuts() - num_cuts_start;
    }

    // Try to lift cuts in subspace
    // Only apply if the current fractional variable is binary
    if (solver->isBinary(currFracVar)) {
      std::vector<OsiCuts> SIC(2);
      CglGomory SICGen;
      for (int b = 0; b < 2; b++) {
        SICGen.generateCuts(*tmpSolver[b], SIC[b]);
        // Compute cut coefficients
        double beta0, beta1;
        for (int c = 0; c < SIC[b].sizeCuts(); c++) {
          // Check if reached cut limit
          if (reachedCutLimit(cs.sizeCuts() - num_cuts_start, cs.sizeCuts())) {
            for (int t = 0; t < num_disj_terms_cgs; t++) {
              if (tmpSolver[t]) {
                delete tmpSolver[t];
              }
              if (betaSolver[t]) {
                delete betaSolver[t];
              }
            }
            return cs.sizeCuts() - num_cuts_start;
          }
          SIC[b].rowCutPtr(c)->sortIncrIndex();
          genTiltedSubspaceCut(cs, cut, betaSolver[0], betaSolver[1], beta0,
              beta1, SIC[b].rowCutPtr(c), currFracVar, currRoundedVal[0],
              currRoundedVal[1], zeroObj, false, (b == 0));
        }
      } // TODO Delete SICs created?
    } /* Lifting subSICs; if solver->isBinary(currFracVar) */

  for (int t = 0; t < num_disj_terms_cgs; t++) {
    if (tmpSolver[t]) {
      delete tmpSolver[t];
    }
    if (betaSolver[t]) {
      delete betaSolver[t];
    }
  }

  return (cs.sizeCuts() - num_cuts_start);
  } /* else both sides of split are feasible */
} /* genTiltedCuts */

void CglTilted::insertCutHelper(OsiRowCut& cut, OsiCuts& cs) {
  const int old_num_cuts = cs.sizeCuts();
  cs.insertIfNotDuplicate(cut);
  if (cs.sizeCuts() > old_num_cuts) {
    cs.rowCutPtr(old_num_cuts)->setEffectiveness(cut.effectiveness());
  }
}

void CglTilted::updateMixingVectorsHelper(const int set_ind, const int ineq_ind,
    const int currFracVar, const double beta0, const double beta1,
    std::vector<std::vector<std::vector<int> > >& splitVarForIneq,
    std::vector<std::vector<std::vector<int> > >& facetOfSplitForIneq,
    std::vector<std::vector<std::vector<double> > >& splitHighValueForIneq,
    std::vector<std::vector<double>>& overallLBForIneq) {
  if (!isInfinity(beta0) && !isInfinity(beta1)) {
      //&& solver->isBinary(currFracVar)) {
    const int higher_facet_ind = (beta0 > beta1) ? 0 : 1;
    const double lower_bound = (beta0 > beta1) ? beta1 : beta0;
    const double higher_bound = (beta0 > beta1) ? beta0 : beta1;
    const double overall_lb = overallLBForIneq[set_ind][ineq_ind];
    const double true_higher_bound =
        (overall_lb < higher_bound) ? higher_bound : overall_lb;

    if (!greaterThanVal(true_higher_bound, overall_lb)) {
      return;
    }

    splitVarForIneq[set_ind][ineq_ind].push_back(currFracVar);
    facetOfSplitForIneq[set_ind][ineq_ind].push_back(higher_facet_ind);
    splitHighValueForIneq[set_ind][ineq_ind].push_back(true_higher_bound);
    if ((greaterThanVal(lower_bound, overall_lb))) {
      overallLBForIneq[set_ind][ineq_ind] = lower_bound;
    }
  }
}

/************************************************************************/
/**
 * It is assumed that betaSolver's objective is zero at the start (and will be zeroed out at end)
 * Also note that betaSolver is in the space in which xk has been deleted
 */
bool CglTilted::genTiltedObj(OsiCuts& cs, OsiRowCut& cut, const double beta0,
    const double beta1, const int currFracVar, const double floorxk,
    const double ceilxk) {
  if (param.getParamVal(ParamIndices::MIX_PARAM_IND)) {
    return false;
  }

  bool cut_generated = false;

  const int hplaneNumElem = solver->getNumCols();
  const double* hplaneElements = solver->getObjCoefficients();

  // Set cut value if this is indeed a new hyperplane
  // Note that if beta0 = beta1, this does not necessarily imply it is a duplicate,
  // if we have optimized both sides (if bound < min(beta0,beta1)).
  bool duplicateCut = isVal(beta0, beta1);
  if (duplicateCut && lessThanVal(solver->getObjValue(), beta0)) {
    duplicateCut = false;
  }
  if (!duplicateCut) {
    std::vector<int> cutIndex;
    std::vector<double> cutElem;
    cutIndex.reserve(hplaneNumElem);
    cutElem.reserve(hplaneNumElem);
    for (int el = 0; el < hplaneNumElem; el++) {
      const int curr_ind = el;

      const double val = hplaneElements[el]
          + ((curr_ind == currFracVar) ? (beta0 - beta1) : 0);
      if (!isZero(val)) {
        cutIndex.push_back(curr_ind);
        cutElem.push_back(val);
      }
    }
    cut.setLb(beta1 + ceilxk * (beta0 - beta1));
    cut.setRow((int) cutIndex.size(), cutIndex.data(), cutElem.data());
    const double currCutNorm = cut.row().twoNorm();
    const double violation = cut.violated(solver->getColSolution());
    cut.setEffectiveness(violation / currCutNorm);
    insertCutHelper(cut, cs);
    cut_generated = true;
  } else {
    GlobalVariables::numCutSolverFails[GlobalConstants::CutSolverFails::EXISTING_HPLANE_TILTED_FAIL_IND]++;
  }
  return cut_generated;
} /* genTiltedObj */

/************************************************************************/
bool CglTilted::genTiltedBound(OsiCuts& cs, OsiRowCut& cut,
    PointCutsSolverInterface* betaSolverFloor,
    PointCutsSolverInterface* betaSolverCeil,
    double& beta0, double& beta1, const int tmph,
    const int currFracVar, const double ceilxk, const bool lb_flag,
    const bool optimizeBothSides, const bool onFloor) {
  bool cut_generated = false;
  bool do_mixing = param.getParamVal(ParamIndices::MIX_PARAM_IND);
  const int h = (!splitVarDeleted || (tmph < currFracVar)) ? tmph : tmph + 1;

  PointCutsSolverInterface *betaSolverThisFacet, *betaSolverOtherFacet;

  if (onFloor) {
    betaSolverThisFacet = betaSolverFloor;
    betaSolverOtherFacet = betaSolverCeil;
  } else {
    betaSolverThisFacet = betaSolverCeil;
    betaSolverOtherFacet = betaSolverFloor;
  }

  const double tmpbound =
      (lb_flag) ?
          betaSolverThisFacet->getColLower()[tmph] :
          -1.0 * betaSolverThisFacet->getColUpper()[tmph];

  if (isNegInfinity(tmpbound, solver->getInfinity())) {
    return false;
  }

  // Activate (+ or -) x_h \ge bound
  // Need to find the minimum value of x_h on opposite facet
  betaSolverOtherFacet->setObjCoeff(tmph, 2. * lb_flag - 1.);
  betaSolverOtherFacet->resolve();
  if (optimizeBothSides) {
    betaSolverThisFacet->setObjCoeff(tmph, 2. * lb_flag - 1.);
    betaSolverThisFacet->resolve();
  }

  if (betaSolverOtherFacet->isProvenOptimal()
      && (!optimizeBothSides || betaSolverThisFacet->isProvenOptimal())) {
    // Both sides are bounded so we can generate the cut
    // Since we are assuming that currFracVar != h, and this is a bound,
    // the objective coefficient on currFracVar (the removed variable) is 0
    const double bound =
        (optimizeBothSides) ? betaSolverThisFacet->getObjValue() : tmpbound;
    beta0 = (onFloor) ? bound : betaSolverOtherFacet->getObjValue();
    beta1 = (onFloor) ? betaSolverOtherFacet->getObjValue() : bound;

    if (!do_mixing) {
      // Set cut value (only non-zero values will be on x_h and x_k), if this is indeed a new hyperplane
      // Note that if beta0 = beta1, this does not necessarily imply it is a duplicate,
      // if we have optimized both sides (if tmpbound < min(beta0,beta1)).
      bool duplicateCut = isVal(beta0, beta1);
      if (optimizeBothSides && duplicateCut && lessThanVal(tmpbound, beta0)) {
        duplicateCut = false;
      }
      if (!duplicateCut) {
        const int numCutElem = 2;
        std::vector<int> cutIndex;
        std::vector<double> cutElem;
        cutIndex.reserve(numCutElem);
        cutElem.reserve(numCutElem);

        // Does not matter what order we add it in
        cutIndex.push_back(h);
        cutElem.push_back(2. * lb_flag - 1.);
        if (!isZero(beta0 - beta1)) {
          cutIndex.push_back(currFracVar);
          cutElem.push_back(beta0 - beta1);
        }

        cut.setLb(beta1 + ceilxk * (beta0 - beta1));
        cut.setRow(cutIndex.size(), cutIndex.data(), cutElem.data());
        const double currCutNorm = cut.row().twoNorm();
        const double violation = cut.violated(solver->getColSolution());
        cut.setEffectiveness(violation / currCutNorm);
        insertCutHelper(cut, cs);
        cut_generated = true;
      } else {
        GlobalVariables::numCutSolverFails[GlobalConstants::CutSolverFails::EXISTING_HPLANE_TILTED_FAIL_IND]++;
        do_mixing = false;
      }
    }
  } else {
    GlobalVariables::numCutSolverFails[GlobalConstants::CutSolverFails::UNBOUNDED_BETA_TILTED_FAIL_IND]++;
    do_mixing = false;
  }

  // Zero out the objective
  betaSolverThisFacet->setObjCoeff(tmph, 0.0);
  betaSolverOtherFacet->setObjCoeff(tmph, 0.0);
  return cut_generated || do_mixing;
} /* genTiltedBound */

/************************************************************************/
/**
 * It is assumed that betaSolver's objective is zero at the start (and will be zeroed out at end)
 * Also note that betaSolver is in the space in which xk has been deleted
 */
bool CglTilted::genTiltedRow(OsiCuts& cs, OsiRowCut& cut,
    PointCutsSolverInterface* betaSolverFloor,
    PointCutsSolverInterface* betaSolverCeil, double& beta0, double& beta1,
    const int row, const int currFracVar, const double floorxk,
    const double ceilxk, const std::vector<double>& zeroObj,
    const bool optimizeBothSides, const bool onFloor) {
  // Ensure that the inequality is of the form h^T x \ge b_h
  if (solver->getRowSense()[row] == 'E') {
    return false;
  }
  const double mult = (solver->getRowSense()[row] == 'G') ? 1.0 : -1.0;

  bool cut_generated = false;
  bool do_mixing = param.getParamVal(ParamIndices::MIX_PARAM_IND);

  PointCutsSolverInterface *betaSolverThisFacet, *betaSolverOtherFacet;

  if (onFloor) {
    betaSolverThisFacet = betaSolverFloor;
    betaSolverOtherFacet = betaSolverCeil;
  } else {
    betaSolverThisFacet = betaSolverCeil;
    betaSolverOtherFacet = betaSolverFloor;
  }
  const double xkFixedValThisFacet = (onFloor) ? floorxk : ceilxk;
  const double xkFixedValOtherFacet = (onFloor) ? ceilxk : floorxk;

  // Activate (+ or -) h^T x \ge bound
  // Need to find the minimum value of h on opposite facet
  // The objective to minimize
  const CoinShallowPackedVector hplaneToAct =
      solver->getMatrixByRow()->getVector(row);
  double xkCoeffInRow = 0.0;

  const int hplaneNumElem = hplaneToAct.getNumElements();
  const int* hplaneIndices = hplaneToAct.getIndices();
  const double* hplaneElements = hplaneToAct.getElements();

  for (int el = 0; el < hplaneNumElem; el++) {
    const int curr_ind = hplaneIndices[el];
    const int tmp_ind =
        (!splitVarDeleted
            || (curr_ind < currFracVar)) ? curr_ind : curr_ind - 1;
    if (splitVarDeleted && (curr_ind == currFracVar)) {
      xkCoeffInRow = mult * hplaneElements[el];
    } else { // Should we check for non-zero here?
      betaSolverOtherFacet->setObjCoeff(tmp_ind, mult * hplaneElements[el]);
      if (optimizeBothSides) {
        betaSolverThisFacet->setObjCoeff(tmp_ind, mult * hplaneElements[el]);
      }
    }
  }
  betaSolverOtherFacet->resolve();
  if (optimizeBothSides) {
    betaSolverThisFacet->resolve();
  }

  if (betaSolverOtherFacet->isProvenOptimal()
      && (!optimizeBothSides || betaSolverThisFacet->isProvenOptimal())) {
    // Both sides are bounded so we can generate the cut
    // Since we are assuming that currFracVar != h, and this is a bound,
    // the objective coefficient on currFracVar (the removed variable) is 0
    const double bound =
        (optimizeBothSides) ?
            betaSolverThisFacet->getObjValue()
                + xkCoeffInRow * xkFixedValThisFacet :
            mult * solver->getRightHandSide()[row];
    beta0 =
        (onFloor) ?
            bound :
            betaSolverOtherFacet->getObjValue()
                + xkCoeffInRow * xkFixedValOtherFacet;
    beta1 =
        (onFloor) ?
            betaSolverOtherFacet->getObjValue()
                + xkCoeffInRow * xkFixedValOtherFacet :
            bound;

    if (!do_mixing) {
      // Set cut value if this is indeed a new hyperplane
      // Note that if beta0 = beta1, this does not necessarily imply it is a duplicate,
      // if we have optimized both sides (if bound < min(beta0,beta1)).
      bool duplicateCut = isVal(beta0, beta1);
      if (optimizeBothSides && duplicateCut
          && lessThanVal(mult * solver->getRightHandSide()[row], beta0)) {
        duplicateCut = false;
      }
      if (!duplicateCut) {
        std::vector<int> cutIndex;
        std::vector<double> cutElem;
        cutIndex.reserve(hplaneNumElem + 1);
        cutElem.reserve(hplaneNumElem + 1);
        bool currFracVarValInserted = false;
        const double currValToAdd = (beta0 - beta1);
        for (int el = 0; el < hplaneNumElem; el++) {
          const int curr_ind = hplaneIndices[el];

          const double val = mult * hplaneElements[el]
              + ((curr_ind == currFracVar) ? currValToAdd : 0);
          if (!isZero(val)) {
            cutIndex.push_back(curr_ind);
            cutElem.push_back(val);
          }

          if (curr_ind == currFracVar) {
            currFracVarValInserted = true;
          }
        }
        if (!currFracVarValInserted) { // Will be out of order, potentially
          if (!isZero(currValToAdd)) {
            cutIndex.push_back(currFracVar);
            cutElem.push_back(currValToAdd);
          }
        }
        cut.setLb(beta1 + ceilxk * currValToAdd);
        cut.setRow((int) cutIndex.size(), cutIndex.data(), cutElem.data());
        const double currCutNorm = cut.row().twoNorm();
        const double violation = cut.violated(solver->getColSolution());
        cut.setEffectiveness(violation / currCutNorm);
        insertCutHelper(cut, cs);
        cut_generated = true;
      } else {
        GlobalVariables::numCutSolverFails[GlobalConstants::CutSolverFails::EXISTING_HPLANE_TILTED_FAIL_IND]++;
        do_mixing = false;
      }
    }
  } else {
    GlobalVariables::numCutSolverFails[GlobalConstants::CutSolverFails::UNBOUNDED_BETA_TILTED_FAIL_IND]++;
    do_mixing = false;
  }

  // Zero out the objective
  betaSolverOtherFacet->setObjective(zeroObj.data());
  if (optimizeBothSides) {
    betaSolverThisFacet->setObjective(zeroObj.data());
  }
  return cut_generated || do_mixing;
} /* genTiltedRow */

/************************************************************************/
/**
 * It is assumed that betaSolver's objective is zero at the start (and will be zeroed out at end)
 * Also note that betaSolver is in the space in which xk has been deleted
 */
bool CglTilted::genTiltedSubspaceCut(OsiCuts& cs, OsiRowCut& cut,
    PointCutsSolverInterface* betaSolverFloor,
    PointCutsSolverInterface* betaSolverCeil, double& beta0, double& beta1,
    const OsiRowCut* cutToLift, const int currFracVar, const double floorxk,
    const double ceilxk, const std::vector<double>& zeroObj,
    const bool optimizeBothSides, const bool onFloor) {
  // Ensure that the inequality is of the form h^T x \ge b_h
  if (cutToLift->sense() == 'E') {
    return false;
  }
  const double mult = (cutToLift->sense() == 'G') ? 1.0 : -1.0;

  bool cut_generated = false;

  PointCutsSolverInterface *betaSolverThisFacet, *betaSolverOtherFacet;

  if (onFloor) {
    betaSolverThisFacet = betaSolverFloor;
    betaSolverOtherFacet = betaSolverCeil;
  } else {
    betaSolverThisFacet = betaSolverCeil;
    betaSolverOtherFacet = betaSolverFloor;
  }
//  const double xkFixedValThisFacet = (onFloor) ? floorxk : ceilxk;
//  const double xkFixedValOtherFacet = (onFloor) ? ceilxk : floorxk;

  // Activate (+ or -) h^T x \ge bound
  // Need to find the minimum value of h on opposite facet
  // The objective to minimize
  const CoinPackedVector subHplaneToAct = cutToLift->row();
//  double xkCoeffInRow = 0.0; // No such xkCoeff if in subspace

  const int subHplaneNumElem = subHplaneToAct.getNumElements();
  const int* subHplaneIndices = subHplaneToAct.getIndices();
  const double* subHplaneElements = subHplaneToAct.getElements();

  for (int el = 0; el < subHplaneNumElem; el++) {
    const int tmp_ind = subHplaneIndices[el];
//    const int tmp_ind = (curr_ind < currFracVar) ? curr_ind : curr_ind - 1;
//    if (curr_ind == currFracVar) {
//      xkCoeffInRow = mult * hplaneElements[el];
//    } else { // Should we check for non-zero here?
    betaSolverOtherFacet->setObjCoeff(tmp_ind, mult * subHplaneElements[el]);
    if (optimizeBothSides) {
      betaSolverThisFacet->setObjCoeff(tmp_ind, mult * subHplaneElements[el]);
    }
//    }
  }
  betaSolverOtherFacet->resolve();
  if (optimizeBothSides) {
    betaSolverThisFacet->resolve();
  }

  if (betaSolverOtherFacet->isProvenOptimal()
      && (!optimizeBothSides || betaSolverThisFacet->isProvenOptimal())) {
    // Both sides are bounded so we can generate the cut
    // Since we are assuming that currFracVar != h, and this is a bound,
    // the objective coefficient on currFracVar (the removed variable) is 0
    const double bound =
        (optimizeBothSides) ?
            betaSolverThisFacet->getObjValue() : mult * cutToLift->rhs();
    beta0 =
        (onFloor) ? bound : betaSolverOtherFacet->getObjValue();
    beta1 =
        (onFloor) ? betaSolverOtherFacet->getObjValue() : bound;

    // Set cut value if this is indeed a new hyperplane
    // Note that if beta0 = beta1, this does not necessarily imply it is a duplicate,
    // if we have optimized both sides (if tmpbound < min(beta0,beta1)).
    bool duplicateCut = isVal(beta0, beta1);
    if (optimizeBothSides && duplicateCut
        && lessThanVal(mult * cutToLift->rhs(), beta0)) {
      duplicateCut = false;
    }
    if (!duplicateCut) {
      std::vector<int> cutIndex;
      std::vector<double> cutElem;
      cutIndex.reserve(subHplaneNumElem + 1);
      cutElem.reserve(subHplaneNumElem + 1);
      bool currFracVarValInserted = false;
      const double currValToAdd = (beta0 - beta1);
      for (int el = 0; el < subHplaneNumElem; el++) {
        const int tmp_ind = subHplaneIndices[el];
        const int curr_ind =
            (!splitVarDeleted || (tmp_ind < currFracVar)) ?
                tmp_ind : tmp_ind + 1;

        const double val = mult * subHplaneElements[el]
            + ((curr_ind == currFracVar) ? currValToAdd : 0);
        if (!isZero(val)) {
          cutIndex.push_back(curr_ind);
          cutElem.push_back(val);
        }

        if (curr_ind == currFracVar) {
          currFracVarValInserted = true;
        }
      }
      if (!currFracVarValInserted) { // Will be out of order, potentially
        if (!isZero(currValToAdd)) {
          cutIndex.push_back(currFracVar);
          cutElem.push_back(currValToAdd);
        }
      }
      cut.setLb(beta1 + ceilxk * currValToAdd);
      cut.setRow((int) cutIndex.size(), cutIndex.data(), cutElem.data());
      const double currCutNorm = cut.row().twoNorm();
      const double violation = cut.violated(solver->getColSolution());
      cut.setEffectiveness(violation / currCutNorm);
      insertCutHelper(cut, cs);
      cut_generated = true;
    } else {
      GlobalVariables::numCutSolverFails[GlobalConstants::CutSolverFails::EXISTING_HPLANE_TILTED_FAIL_IND]++;
    }
  } else {
    GlobalVariables::numCutSolverFails[GlobalConstants::CutSolverFails::UNBOUNDED_BETA_TILTED_FAIL_IND]++;
  }

  // Zero out the objective
  betaSolverOtherFacet->setObjective(zeroObj.data());
  if (optimizeBothSides) {
    betaSolverThisFacet->setObjective(zeroObj.data());
  }
  return cut_generated;
} /* genTiltedSubspaceCut */

/************************************************************************/
/**
 * Generate a mixed inequality from a particular row after computing all bounds.
 */
bool CglTilted::genMixedIneq(OsiCuts& cs, OsiRowCut& cut, const int ineq,
    const double overallLB, const std::vector<double>& unsortedBoundFromSplit,
    const std::vector<int>& splitVar, const std::vector<int>& sortedOrder,
    const std::vector<int>& facetOfSplit, const bool isColLB,
    const bool isColUB, const bool isRow) {
  const bool isCol = isColLB || isColUB;
  const bool isObj = isRow && ineq == solver->getNumRows();

  // Ensure that the inequality is not of the form h^T x = b_h
  if ((isCol && (getVarLB(solver, ineq) == getVarUB(solver, ineq)))
      || ((isRow && !isObj) && (solver->getRowSense()[ineq] == 'E'))) {
    return false;
  }

  const int numSplitsForIneq = splitVar.size();

  // Cut to populate
  // As a reminder, we are working with
  // (where, when isCol, hx = +x_k if isColLB, hx = -x_k if isColUB)
  //   hx >= B_0
  //         + \sum_{t \in T0} (B_t - B_{t-1}) (ceil(x_t) - x_t)
  //         + \sum_{t \in T1} (B_t - B_{t-1}) (x_t - floor(x_t)).
  // This means we have, letting D_t = B_t - B_{t-1}:
  //   \sum_{j \notin T} h_j x_j
  //     + \sum_{j \in T0} (h_j + D_j) x_j
  //     + \sum_{j \in T1} (h_j - D_j) x_j
  //   \ge B_0 + \sum_{t \in T0} D_t ceil(\bar x_t) - \sum_{t \in T1} D_t floor(\bar x_t).
  bool cut_generated = false;
  std::vector<int> cutIndex, tmpCutIndex;
  std::vector<double> cutElem, tmpCutElem;
  double boundAdjustment = 0.0;

  tmpCutIndex.reserve(numSplitsForIneq + 1);
  tmpCutElem.reserve(numSplitsForIneq + 1);
  double prev_bound = overallLB;
  for (int split_ind = 0; split_ind < numSplitsForIneq; split_ind++) {
    const int sorted_ind = sortedOrder[split_ind];
    const double curr_bound = unsortedBoundFromSplit[sorted_ind];
    const double delta = curr_bound - prev_bound;
    if (isVal(delta, 0.0) || lessThanVal(curr_bound, overallLB)) {
      continue;
    }

    const int split_var = splitVar[sorted_ind];
    const double xk = solver->getColSolution()[split_var];
    const double mult = (facetOfSplit[sorted_ind] == 0) ? 1.0 : -1.0;
    const double facet_val = (mult < 0) ? -1.0 * std::floor(xk) : std::ceil(xk);

    // With everything set up, update cut coefficients and lower-bound
    boundAdjustment += delta * facet_val;
    tmpCutIndex.push_back(split_var);
    tmpCutElem.push_back(mult * delta);

    prev_bound = curr_bound;
  }

  const int numSplitsForIneqActual = tmpCutIndex.size();

  if (isCol) {
    // Insert the variable whose bound is being mixed
    tmpCutIndex.push_back(ineq);
    tmpCutElem.push_back(2 * isColLB - 1);
    cut.setRow((int) tmpCutIndex.size(), tmpCutIndex.data(), tmpCutElem.data());
  } else if (isRow) {
    // Needs to be sorted for efficiency
    std::vector<int> sortSplitsByVarIndex(numSplitsForIneqActual);
    for (int split_ind = 0; split_ind < numSplitsForIneqActual; split_ind++) {
      sortSplitsByVarIndex[split_ind] = split_ind;
    }
    sort(sortSplitsByVarIndex.begin(), sortSplitsByVarIndex.end(),
        index_cmp_asc<int*>(tmpCutIndex.data()));

    // Get the hyperplane to be inserted
    const double mult =
        (isObj || (solver->getRowSense()[ineq] == 'G')) ? 1.0 : -1.0;
    const CoinShallowPackedVector hplaneToAct =
        (!isObj) ?
            solver->getMatrixByRow()->getVector(ineq) :
            CoinShallowPackedVector();
    const int hplaneNumElem =
        (!isObj) ? hplaneToAct.getNumElements() : solver->getNumCols();
    const int* hplaneIndices = (!isObj) ? hplaneToAct.getIndices() : NULL;
    const double* hplaneElements =
        (!isObj) ? hplaneToAct.getElements() : solver->getObjCoefficients();

    cutIndex.reserve(hplaneNumElem);
    cutElem.reserve(hplaneNumElem);

    // Get the sorted cut vector for changing the coefficients
    int curr_cut_pos = 0;
    int curr_cut_var =
        (numSplitsForIneqActual > 0) ?
            tmpCutIndex[sortSplitsByVarIndex[curr_cut_pos]] : -1;

    for (int el = 0; el < hplaneNumElem; el++) {
      const int hplane_var = (!isObj) ? hplaneIndices[el] : el;
      while ((curr_cut_pos < numSplitsForIneqActual)
          && (hplane_var > curr_cut_var)) {
        const double val = tmpCutElem[sortSplitsByVarIndex[curr_cut_pos]];
        if (!isZero(val)) {
          cutIndex.push_back(curr_cut_var);
          cutElem.push_back(val);
        }
        curr_cut_pos++;
        if (curr_cut_pos < numSplitsForIneqActual) {
          curr_cut_var = tmpCutIndex[sortSplitsByVarIndex[curr_cut_pos]];
        }
      }

      const double val = mult * hplaneElements[el]
          + ((hplane_var == curr_cut_var) ?
              tmpCutElem[sortSplitsByVarIndex[curr_cut_pos]] : 0);
      if (!isZero(val)) {
        cutIndex.push_back(hplane_var);
        cutElem.push_back(val);
      }

      if (hplane_var == curr_cut_var) {
        curr_cut_pos++;
        if (curr_cut_pos < numSplitsForIneqActual) {
          curr_cut_var = tmpCutIndex[sortSplitsByVarIndex[curr_cut_pos]];
        }
      }
    }

    // Add remaining split elements
    while (curr_cut_pos < numSplitsForIneqActual) {
      const double val = tmpCutElem[sortSplitsByVarIndex[curr_cut_pos]];
      if (!isZero(val)) {
        cutIndex.push_back(curr_cut_var);
        cutElem.push_back(val);
      }
      curr_cut_pos++;
      if (curr_cut_pos < numSplitsForIneqActual) {
        curr_cut_var = tmpCutIndex[sortSplitsByVarIndex[curr_cut_pos]];
      }
    }

    cut.setRow((int) cutIndex.size(), cutIndex.data(), cutElem.data());
  }

  cut.setLb(overallLB + boundAdjustment);
  const double currCutNorm = cut.row().twoNorm();
  const double violation = cut.violated(solver->getColSolution());
  cut.setEffectiveness(violation / currCutNorm);
  insertCutHelper(cut, cs);
  cut_generated = true;

  return cut_generated;
} /* genMixedIneq */

/***********************************************************************/
//bool CglTilted::boundIsActive(const LiftGICsSolverInterface* tmpSolver,
//    const int tmph, double& bound, bool& lb_flag) {
//  if (isVal(tmpSolver->getColSolution()[tmph],
//      tmpSolver->getColLower()[tmph])) {
//    bound = tmpSolver->getColLower()[tmph];
//    lb_flag = true;
//    return true;
//  } else if (isVal(tmpSolver->getColSolution()[tmph],
//      tmpSolver->getColUpper()[tmph])) {
//    bound = -1.0 * tmpSolver->getColUpper()[tmph];
//    lb_flag = false;
//    return true;
//  }
//  return false;
//} /* boundIsActive */

/***********************************************************************/
bool CglTilted::hplaneIsActive(const PointCutsSolverInterface* tmpSolver,
    const int tmph, const bool lowerBound) {
  if (tmph < tmpSolver->getNumCols()) {
    if (lowerBound) {
      return isVal(tmpSolver->getColSolution()[tmph],
          tmpSolver->getColLower()[tmph]);
    } else {
      return isVal(tmpSolver->getColSolution()[tmph],
          tmpSolver->getColUpper()[tmph]);
    }
  } else {
    const int tmprow = tmph - tmpSolver->getNumCols();
    return isVal(tmpSolver->getRowActivity()[tmprow],
        tmpSolver->getRightHandSide()[tmprow]);
  }
} /* hplaneIsActive */

/***********************************************************************/
void CglTilted::setParam(const CglGICParam &source) {
  param = source;
} /* setParam */

/*********************************************************************/
// Returns true if needs optimal basis to do cuts
bool CglTilted::needsOptimalBasis() const {
  return true;
}

/*********************************************************************/
// Create C++ lines to get to current state
std::string CglTilted::generateCpp(FILE * fp) {
  return "CglTilted";
}

/**********************************************************/
void CglTilted::printvecINT(const char *vecstr, const int *x, int n) const {
  int num, fromto, upto;

  num = (n / 10) + 1;
  printf("%s :\n", vecstr);
  for (int j = 0; j < num; ++j) {
    fromto = 10 * j;
    upto = 10 * (j + 1);
    if (n <= upto)
      upto = n;
    for (int i = fromto; i < upto; ++i)
      printf(" %4d", x[i]);
    printf("\n");
  }
  printf("\n");
} /* printvecINT */

/**********************************************************/
void CglTilted::printvecDBL(const char *vecstr, const double *x, int n) const {
  int num, fromto, upto;

  num = (n / 10) + 1;
  printf("%s :\n", vecstr);
  for (int j = 0; j < num; ++j) {
    fromto = 10 * j;
    upto = 10 * (j + 1);
    if (n <= upto)
      upto = n;
    for (int i = fromto; i < upto; ++i)
      printf(" %7.3f", x[i]);
    printf("\n");
  }
  printf("\n");
} /* printvecDBL */

/**********************************************************/
void CglTilted::printvecDBL(const char *vecstr, const double *elem,
    const int * index, int nz) const {
  printf("%s\n", vecstr);
  int written = 0;
  for (int j = 0; j < nz; ++j) {
    written += printf("%d:%.3f ", index[j], elem[j]);
    if (written > 70) {
      printf("\n");
      written = 0;
    }
  }
  if (written > 0) {
    printf("\n");
  }

} /* printvecDBL */

