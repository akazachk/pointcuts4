// Last edit: 01/10/2015
//
// Name:     CglGIC.hpp
// Author:   F. Margot (edited by A. M. Kazachkov)
//           Tepper School of Business
//           email: fmargot@andrew.cmu.edu
// Date:     01/10/2015
//-----------------------------------------------------------------------------

#if defined(_MSC_VER)
// Turn off compiler warning about long names
#  pragma warning(disable:4786)
#endif
#include <cstdlib>
#include <cstdio>
#include <cmath> // log2
#include <cfloat>
#include <cassert>
#include <iostream>
#include <fenv.h>
#include <climits>

#include "OsiSolverInterface.hpp"

#include "CoinHelperFunctions.hpp"
#include "CoinPackedVector.hpp"
#include "CoinPackedMatrix.hpp"
#include "CoinIndexedVector.hpp"
#include "OsiRowCutDebugger.hpp"
#include "CoinFactorization.hpp"
#include "CglPHA.hpp"
#include "CoinFinite.hpp"

#include "GlobalConstants.hpp"

#include "Utility.hpp"
#include "CutHelper.hpp"

// Useful classes and such for GICs
#include "SolutionInfo.hpp"
#include "Output.hpp"
#include "CglSIC.hpp"

/***************************************************************************/
CglPHA::CglPHA() :
    CglCutGenerator(), solver(NULL), castsolver(NULL), xlp(NULL), byRow(NULL), byCol(
        NULL), nb_advcs(true), struct_advcs(false), structSICs(false), NBSICs(
        true), num_obj_tried(0) {
  param.setParams();
}

/***************************************************************************/
CglPHA::CglPHA(const CglGICParam& parameters) :
    CglCutGenerator(), param(parameters), solver(NULL), castsolver(NULL), xlp(
        NULL), byRow(NULL), byCol(NULL), nb_advcs(true), struct_advcs(false), structSICs(
        false), NBSICs(true), num_obj_tried(0) {
  // Nothing to do here
}

/***************************************************************************/
CglPHA::CglPHA(const CglPHA& rhs) :
    CglCutGenerator(rhs), param(rhs.param), solver(rhs.solver), castsolver(
        rhs.castsolver), xlp(rhs.xlp), byRow(rhs.byRow), byCol(rhs.byCol), nb_advcs(
        rhs.nb_advcs), struct_advcs(rhs.struct_advcs), structSICs(
        rhs.structSICs), NBSICs(rhs.NBSICs), num_obj_tried(0) {
  // Nothing to do here
}

/***************************************************************************/
CglPHA & CglPHA::operator=(const CglPHA& rhs) {
  if (this != &rhs) {
    CglCutGenerator::operator=(rhs);
    param = rhs.param;
    solver = rhs.solver;
    castsolver = rhs.castsolver;
    xlp = rhs.xlp;
    byRow = rhs.byRow;
    byCol = rhs.byCol;
    nb_advcs = rhs.nb_advcs;
    struct_advcs = rhs.struct_advcs;
    structSICs = rhs.structSICs;
    NBSICs = rhs.NBSICs;
    num_obj_tried = rhs.num_obj_tried;
  }
  return *this;
}

/***************************************************************************/
CglPHA::~CglPHA() {

}

/*********************************************************************/
CglCutGenerator *
CglPHA::clone() const {
  return new CglPHA(*this);
}

/************************************************************************/
void CglPHA::generateCuts(const OsiSolverInterface & si, OsiCuts & cs,
    const CglTreeInfo info) const {
  // kludge to be able to modify the CglGIC object if it is const
  CglPHA temp(*this);
  temp.generateCuts(si, cs, info);
} /* generateCuts */

/************************************************************************/
void CglPHA::generateCuts(const OsiSolverInterface &si, OsiCuts & cs,
    const CglTreeInfo info) {
  // Create SolutionInfo
  solver = const_cast<OsiSolverInterface *>(&si); // Removes const-ness
  SolutionInfo tmpSolnInfo(solver,
    param.paramVal[ParamIndices::NUM_CGS_PARAM_IND],
      param.paramVal[ParamIndices::SUBSPACE_PARAM_IND], false);
  if (tmpSolnInfo.numSplits == 0) {
    return;
  }
  if ((param.getParamVal(ParamIndices::CGS_PARAM_IND) > 0)
      && (tmpSolnInfo.numSplits < 2)) {
    return;
  }
  if (!param.finalizeParams(tmpSolnInfo.numNB, tmpSolnInfo.numSplits)) {
    error_msg(errstr,
        "Issue finalizing parameters (no rays to cut or issue with parameter combinations).\n");
    writeErrorToLog(errstr, GlobalVariables::log_file);
    exit(1);
  }
  if (param.paramVal[NUM_CGS_PARAM_IND] > tmpSolnInfo.numFeasSplits) {
    warning_msg(warnstring,
        "Number of requested cut generating sets is %d, but the maximum number we can choose is %d. Reducing the requested number of splits.\n",
        param.paramVal[NUM_CGS_PARAM_IND], tmpSolnInfo.numFeasSplits);
    param.paramVal[NUM_CGS_PARAM_IND] = tmpSolnInfo.numFeasSplits;
  }
  
  if (param.paramVal[ParamIndices::SICS_PARAM_IND]) {
    generateSICs(&si, NBSICs, structSICs, tmpSolnInfo);
  }
  //GlobalVariables::param = this->param;
  generateCuts(si, cs, tmpSolnInfo, structSICs, NBSICs, info);
} /* generateCuts */

/************************************************************************/
void CglPHA::generateCuts(const OsiSolverInterface &si, OsiCuts & cs,
    const SolutionInfo& probData,
    const AdvCuts& structMSICs, const AdvCuts& NBMSICs,
    const CglTreeInfo info) {
  solver = const_cast<OsiSolverInterface *>(&si); // Removes const-ness
  if (solver == NULL) {
    printf("### WARNING: CglGIC::generateCuts(): no solver available.\n");
    return;
  }

  if (!solver->optimalBasisIsAvailable()) {
    printf(
        "### WARNING: CglGIC::generateCuts(): no optimal basis available.\n");
    return;
  }

  xlp = solver->getColSolution();
  try {
    generateCuts(dynamic_cast<AdvCuts&>(cs), probData, structMSICs, NBMSICs);
    this->struct_advcs = dynamic_cast<AdvCuts&>(cs);
  } catch (std::exception& e) {
//    warning_msg(errorstr,
//        "Not able to cast OsiCuts as AdvCuts, used to keep extra information.\n");

    generateCuts(this->struct_advcs, probData, structMSICs, NBMSICs);
    cs = struct_advcs;
  }
} /* generateCuts */

/************************************************************************/

/**
 * Assuming we are not working in the subspace (not tested or implemented yet anyway)
 */
void CglPHA::generateCuts(AdvCuts &structPHA, const SolutionInfo& solnInfo,
    const AdvCuts &structMSICs, const AdvCuts &NBSICs) {
#ifdef TRACE
  printf(
      "\n############### Starting PHA generation. ###############\n");
#endif

  castsolver = dynamic_cast<OsiClpSolverInterface*>(this->solver->clone());
  castsolver->enableFactorization(); // Need factorization for chooseHplanes and splitShare

  // TODO For now assuming no subspace
  std::vector<int> oldRowIndices(solnInfo.numRows);
  for (int row = 0; row < solnInfo.numRows; row++) {
    oldRowIndices[row] = row;
  }

  /***********************************************************************************
   * Save objective
   ***********************************************************************************/
  // In order to calculate how good the points we find are
  // with respect to the objective function (which should be in NB space when appropriate)
  // Clearly a valid inequality is c^T x \ge c^\T v,
  // where v is the LP optimum, since we are minimizing
  AdvCut NBObj(true, 1), structObj(false, 1);
  structObj.setOsiRowCut(std::vector<int>(), solver->getNumCols(),
      solver->getObjCoefficients(), solver->getObjValue(),
      CoinMin(solnInfo.EPS, param.getEPS()));

  // Either we have the objective, or we have the reduced costs
  // of the non-basic variables (which is exactly the objective
  // in the non-basic space).
  // Recall that the reduced cost of slack variables is exactly
  // the negative of the row price (dual variable) for that row.
  //	if (!inNBSpace) {
  //		objCut.assign(structObjCut);
  //	} else {
  NBObj.NBSpace = true;
  NBObj.num_coeff = solnInfo.numNB;
  NBObj.setOsiRowCut(solnInfo.nonBasicReducedCost, 0.0,
      CoinMin(solnInfo.EPS, param.getEPS()));
  const double objNorm = NBObj.getTwoNorm();

  /***********************************************************************************
   * Setup for generating GICs
   ***********************************************************************************/
  printf(
      "\n## Instance info:\n== Splits chosen on the following variables:\n");
  for (int s = 0; s < solnInfo.numFeasSplits; s++) {
    const int curr_var = solnInfo.feasSplitVar[s];
    printf(
        ":::: Var name: %s. Var value: %2.3f. Var index: %d. Row index for var: %d.\n",
        solver->getColName(curr_var).c_str(),
        solver->getColSolution()[curr_var],
        solnInfo.feasSplitVar[s],
        solnInfo.rowOfVar[curr_var]);
  }
  printf("====\n");

  printf("== Number rays to be cut set to %d. ==\n",
      param.paramVal[NUM_RAYS_CUT_PARAM_IND]);
  printf("== Number effective cut-generating sets is %d. ==\n",
      solnInfo.numFeasSplits);
  printf("== Number rounds is %d. ==\n",
      param.paramVal[NUM_CUTS_PER_CGS_PARAM_IND]);

  // For tracking and early exiting
  Stats phaTimeStats;
  phaTimeStats.register_name("PHA_TOTAL");
  phaTimeStats.start_timer("PHA_TOTAL");

  /***********************************************************************************
   * Generate points and rays
   ***********************************************************************************/
  std::vector<Vertex> vertexStore; // Vertices generated along the way
  std::vector<Ray> rayStore; // Rays generated along the way
  std::vector<Hplane> hplaneStore; // Activated hyperplanes

  // For when GICs are generated
//  std::vector<int> num_gics_per_split(solnInfo.numFeasSplits, 0);
//  int num_total_gics = 0;

  // Set up Vertex and Ray stores
  vertexStore.resize(1); // Initially just the origin
  vertexStore[0].rayIndices.resize(solnInfo.numNB);
  vertexStore[0].rayActiveFlag.resize(solnInfo.numNB, true);
  rayStore.resize(solnInfo.numNB); // Initially the rays of C1
  for (int i = 0; i < solnInfo.numNB; i++) {
    const int rayIndices[1] = { i };
    const double rayValues[1] = { 1.0 };
    rayStore[i].setVector(1, rayIndices, rayValues);
    rayStore[i].cutFlag.resize(solnInfo.numFeasSplits, false);

    // This ray emanates from the original vertex
    vertexStore[0].rayIndices[i] = i;
  }

  // The constraint matrices for each split
  std::vector<int> num_SIC_points(solnInfo.numFeasSplits, 0);
  std::vector<int> num_SIC_final_points(solnInfo.numFeasSplits, 0);
  std::vector<int> num_SIC_rays(solnInfo.numFeasSplits, 0);
  std::vector<std::vector<bool> > SICPointIsFinal(solnInfo.numFeasSplits);
  std::vector<std::vector<double> > distToSplitAlongRay(solnInfo.numFeasSplits);
  std::vector<IntersectionInfo> interPtsAndRays(solnInfo.numFeasSplits);
  for (int split_ind = 0; split_ind < solnInfo.numFeasSplits; split_ind++) {
    // Reverse ordering so that the packed matrix is row ordered
    // and set number of columns
    interPtsAndRays[split_ind].reverseOrdering();
    interPtsAndRays[split_ind].setDimensions(0, solnInfo.numNB);

    SICPointIsFinal[split_ind].resize(solnInfo.numNB);
//    distToSplitAlongRay[split_ind].resize(solnInfo.numNB);
    const bool recalc_dist = NBSICs.sizeCuts() != solnInfo.numFeasSplits;
    if (recalc_dist) {
      distToSplitAlongRay[split_ind].resize(solnInfo.numNB);
    } else {
      distToSplitAlongRay[split_ind] = NBSICs.cuts[split_ind].distAlongRay;
    }
    // This adds all the original intersection points
    for (int ray_ind = 0; ray_ind < solnInfo.numNB; ray_ind++) {
      addInterPtFromUncutRayOfC1(interPtsAndRays[split_ind],
          distToSplitAlongRay[split_ind][ray_ind], solnInfo, split_ind,
          rayStore, ray_ind, vertexStore, recalc_dist);

      // Gather information about number of final initial points
      GlobalVariables::timeStats.start_timer(COLLECT_STATS_TIME);
      // If it's a point get it, o/w count the ray
      if (isZero(interPtsAndRays[split_ind].RHS[ray_ind])) {
        num_SIC_rays[split_ind]++;
      } else {
        num_SIC_points[split_ind]++;
        // TODO Check if the ray value is equal to the distance to split; should be the same
        if (interPtsAndRays[split_ind].finalFlag[ray_ind]) {
          SICPointIsFinal[split_ind][ray_ind] = true;
          num_SIC_final_points[split_ind]++;
        }
      }
      GlobalVariables::timeStats.end_timer(COLLECT_STATS_TIME);
    }
  }

//  PHAMaster(structPHA, param.getParamVal(ParamIndices::NUM_RAYS_CUT_PARAM_IND),
//      interPtsAndRays, solnInfo, structMSICs, NBSICs, pointIsFinal, vertexStore,
//      rayStore, hplaneStore, distToSplitAlongRay);
  //Choose rays to be cut based on weakest average cost coefficients among Gomory cuts over the chosen splits
  const int num_rays_to_be_cut = param.getParamVal(
      ParamIndices::NUM_RAYS_CUT_PARAM_IND);
  std::vector<double> avgCoeffAcrossCuts(solnInfo.numNB, 0.0);
  std::vector<int> sortIndex(solnInfo.numNB);
  for (int j = 0; j < solnInfo.numNB; j++) {
    sortIndex[j] = j;
    for (int s = 0; s < (int) NBSICs.size(); s++) {
      avgCoeffAcrossCuts[j] += NBSICs[s][j];
    }
    avgCoeffAcrossCuts[j] /= (double) solnInfo.numFeasSplits;
  }

  //Sort average cut coefficients in descending order.
  //Larger cut coefficients in the J-space correspond to weaker cuts
  sort(sortIndex.begin(), sortIndex.end(),
      index_cmp_dsc<double*>(&avgCoeffAcrossCuts[0]));

  // Resize arrays and put default values
  std::vector<int> allRayIndices(solnInfo.numNB);
  for (int j = 0; j < solnInfo.numNB; j++) {
    allRayIndices[j] = j;
  }

  // This stores index, in hplaneStore, of first hplane intersected
  // for each ray *that is cut for that split*
  // [split][ray]
//  std::vector<std::vector<int> > strongVertHplaneIndex(solnInfo.numFeasSplits);

  // Setup how the first sweep through the rays works
  // We double the number of rays cut each time
//  std::vector<int> numRaysCutThroughFirstActRound;
//  numRaysCutThroughFirstActRound.reserve((int) log2(num_rays_to_be_cut) + 1);
//  numRaysCutThroughFirstActRound.push_back(1);
  std::vector<int> numRaysCutInFirstActRound;
  numRaysCutInFirstActRound.reserve((int) log2(num_rays_to_be_cut) + 1);
  numRaysCutInFirstActRound.push_back(1);
  int num_first_act_rounds = 1;
  int total_num_rays_cut = 1;
  while (total_num_rays_cut < num_rays_to_be_cut) {
    numRaysCutInFirstActRound.push_back(total_num_rays_cut);
    total_num_rays_cut = 2 * total_num_rays_cut;
//    numRaysCutThroughFirstActRound.push_back(total_num_rays_cut);
    num_first_act_rounds++;
  }
//  numRaysCutThroughFirstActRound[num_first_act_rounds - 1] = num_rays_to_be_cut;
  if (total_num_rays_cut > num_rays_to_be_cut) {
    numRaysCutInFirstActRound[num_first_act_rounds - 1] = num_rays_to_be_cut
        - total_num_rays_cut / 2;
  }
  if (param.getParamVal(ParamIndices::NUM_EXTRA_ACT_ROUNDS_PARAM_IND) < 0) {
    num_first_act_rounds = 0;
  }
  const int num_extra_rounds = std::abs(
      param.getParamVal(ParamIndices::NUM_EXTRA_ACT_ROUNDS_PARAM_IND));
  const int num_total_act_rounds = num_first_act_rounds
      + ((param.getParamVal(ParamIndices::NEXT_HPLANE_FINAL_PARAM_IND) == 1) ?
          num_extra_rounds : (num_extra_rounds > 0));

  for (int split_ind = 0; split_ind < solnInfo.numFeasSplits; split_ind++) {
    interPtsAndRays[split_ind].resizeInfoByActivationRound(
        num_total_act_rounds);
  }

  // Across all splits, all rays that have been cut
  // [act][ray]
//  std::vector<std::vector<int> > allRaysCut(num_total_act_rounds);
  std::vector<int> allRaysCutAcrossSplits;

  // For each split, which rays are cut by at least one hplane
  // [act][split][rays]
//  std::vector<std::vector<std::vector<int> > > raysCutForSplit(
//      num_total_act_rounds);
//  for (int i = 0; i < num_total_act_rounds; i++) {
//    raysCutForSplit[i].resize(solnInfo.numFeasSplits);
//  }
  std::vector<std::vector<int> > raysCutForSplit(solnInfo.numFeasSplits);

  int num_total_gics = 0;
  std::vector<int> num_gics_per_split(solnInfo.numFeasSplits, 0);

  const int int_option = param.getParamVal(
      ParamIndices::PHA_ACT_OPTION_PARAM_IND);
  PHAActivationOption option = static_cast<PHAActivationOption>(int_option);
  int curr_num_rays_cut = 0;
  for (int first_act_ind = 0; first_act_ind < num_first_act_rounds;
      first_act_ind++) {
    const bool isTimeLimitReached = reachedTimeLimit(phaTimeStats, "PHA_TOTAL",
        param.getTIMELIMIT());
    if (reachedCutLimit(0, num_total_gics) || isTimeLimitReached) {
      break;
    }
    printf(
        "\n## Performing ``first'' activation round %d/%d of PHA(1.%d,%s), %s. ##\n",
        first_act_ind + 1, num_first_act_rounds, int_option,
        PHAActivationOptionName[int_option].c_str(),
        PHAActivationOptionDescription[int_option].c_str());
    PHAHelper(curr_num_rays_cut, numRaysCutInFirstActRound[first_act_ind],
        solnInfo, structSICs, NBSICs, NBObj, objNorm, distToSplitAlongRay,
        SICPointIsFinal, hplaneStore, vertexStore, rayStore, sortIndex,
        allRayIndices, allRaysCutAcrossSplits,
        raysCutForSplit, //strongVertHplaneIndex,
        interPtsAndRays, first_act_ind, num_total_act_rounds, false, option,
        structPHA, num_gics_per_split, num_total_gics, phaTimeStats);
    curr_num_rays_cut += numRaysCutInFirstActRound[first_act_ind];
  }

  if (param.getParamVal(ParamIndices::NEXT_HPLANE_FINAL_PARAM_IND) == 1) {
    option = PHAActivationOption::final;
  } else {
//    option = PHAActivationOption::mostremoved;
    error_msg(errorstring,
        "All extra activations need to be activating most final points at this point.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  for (int act_ind = num_first_act_rounds; act_ind < num_total_act_rounds;
      act_ind++) {
    const bool isTimeLimitReached = reachedTimeLimit(phaTimeStats, "PHA_TOTAL",
        param.getTIMELIMIT());
    if (reachedCutLimit(0, num_total_gics) || isTimeLimitReached) {
      break;
    }
    printf(
        "\n## Act %d/%d (extra act %d/%d): ``Extra'' hyperplane per split, %s. ##\n",
        act_ind + 1, num_total_act_rounds, act_ind - num_first_act_rounds + 1,
        num_total_act_rounds - num_first_act_rounds,
        PHAActivationOptionDescription[static_cast<int>(option)].c_str());
    PHAHelper(0, solnInfo.numNB, solnInfo, structSICs, NBSICs, NBObj,
        objNorm, distToSplitAlongRay, SICPointIsFinal, hplaneStore, vertexStore,
        rayStore, sortIndex, allRayIndices, allRaysCutAcrossSplits,
        raysCutForSplit,
        //strongVertHplaneIndex,
        interPtsAndRays, act_ind, num_total_act_rounds, true, option, structPHA,
        num_gics_per_split, num_total_gics, phaTimeStats);
  }

  structPHA.setOsiCuts();

#ifdef SHOULD_WRITE_GICS
  structPHA.printCuts("PHA", solver, solnInfo);
#endif

  // Analyze resulting point and ray collection
#ifdef TRACE
  printf("\n## Analyzing last point-ray collection. ##\n");
#endif
  GlobalVariables::timeStats.start_timer(COLLECT_STATS_TIME);
  std::vector<double> minSICDepth(solnInfo.numFeasSplits), maxSICDepth(
      solnInfo.numFeasSplits), avgSICDepth(solnInfo.numFeasSplits);
  std::vector<double> minObjDepth(solnInfo.numFeasSplits), maxObjDepth(
      solnInfo.numFeasSplits), avgObjDepth(solnInfo.numFeasSplits);
  for (int split_ind = 0; split_ind < (int) interPtsAndRays.size();
      split_ind++) {
    minSICDepth[split_ind] = interPtsAndRays[split_ind].minSICDepth;
    maxSICDepth[split_ind] = interPtsAndRays[split_ind].maxSICDepth;
    avgSICDepth[split_ind] = interPtsAndRays[split_ind].totalSICDepth
        / interPtsAndRays[split_ind].numPoints;
    minObjDepth[split_ind] = interPtsAndRays[split_ind].minObjDepth;
    maxObjDepth[split_ind] = interPtsAndRays[split_ind].maxObjDepth;
    avgObjDepth[split_ind] = interPtsAndRays[split_ind].totalObjDepth
        / interPtsAndRays[split_ind].numPoints;
  }
  // TODO To fix!
//  analyzePointCollection("MPHA", solnInfo, hplaneStore, NBSICs, NBObj,
//      interPtsAndRays, num_rays_parallel_to_split, minSICDepth, maxSICDepth,
//      avgSICDepth, minObjDepth, maxObjDepth, avgObjDepth, num_total_act_rounds);

#ifdef SHOULD_WRITE_INTPOINTS
  printf("\n## Save information about the points and rays. ##\n");
  char filename[300];
  snprintf(filename, sizeof(filename) / sizeof(char),
      "%s-PointsAndRays.csv", GlobalVariables::out_f_name_stub.c_str());
  FILE *ptsRaysOut = fopen(filename, "w");
  writeInterPtsAndRaysInJSpace(ptsRaysOut, solnInfo, 
      interPtsAndRays, hplaneStore, num_SIC_points,
      num_SIC_final_points, num_SIC_rays);
  fclose(ptsRaysOut);
#endif

  // Write GIC point info to II
  int totalNumPoints = 0, totalNumFinalPoints = 0, totalNumRays = 0;
  std::vector<int> num_points_per_split(solnInfo.numFeasSplits);
  std::vector<int> num_final_points_per_split(solnInfo.numFeasSplits);
  std::vector<int> num_rays_per_split(solnInfo.numFeasSplits);
  for (int s = 0; s < solnInfo.numFeasSplits; s++) {
    num_points_per_split[s] = interPtsAndRays[s].numPoints;
    num_final_points_per_split[s] = interPtsAndRays[s].numFinalPoints;
    num_rays_per_split[s] = interPtsAndRays[s].numRays;
    totalNumPoints += num_points_per_split[s];
    totalNumFinalPoints += num_final_points_per_split[s];
    totalNumRays += num_rays_per_split[s];
  }

  const double avg_num_SIC_final_points = computeAverage(
      num_SIC_final_points);
  const double avg_num_par_rays = computeAverage(num_SIC_rays);
  writeGICPointInfoToLog(param.getParamVal(ParamIndices::PHA_ACT_OPTION_PARAM_IND),
      param.getParamVal(ParamIndices::NEXT_HPLANE_FINAL_PARAM_IND),
      param.getParamVal(ParamIndices::NUM_EXTRA_ACT_ROUNDS_PARAM_IND),
      solnInfo.numNB, avg_num_par_rays, avg_num_SIC_final_points,
      param.getParamVal(ParamIndices::NUM_RAYS_CUT_PARAM_IND),
      param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND),
      param.getParamVal(ParamIndices::NUM_CUTS_PER_CGS_PARAM_IND),
      num_points_per_split, num_final_points_per_split, num_rays_per_split,
      totalNumPoints, totalNumFinalPoints, totalNumRays, avgSICDepth,
      minSICDepth, maxSICDepth, avgObjDepth, minObjDepth, maxObjDepth,
      GlobalVariables::log_file);
  GlobalVariables::timeStats.end_timer(COLLECT_STATS_TIME);

  // Clear things
  if (castsolver) {
    delete castsolver;
  }
  phaTimeStats.end_timer("PHA_TOTAL");
} /* generateCuts */

void CglPHA::PHAHelper(const int indexOfFirstRayToCut, const int numRaysToBeCut,
    const SolutionInfo& solnInfo, const AdvCuts &structSICs,
    const AdvCuts &NBSICs, const AdvCut& NBObj, const double objNorm,
    const std::vector<std::vector<double> >& distToSplitAlongRay,
    std::vector<std::vector<bool> >& pointIsFinal,
    std::vector<Hplane>& hplaneStore, std::vector<Vertex>& vertexStore,
    std::vector<Ray>& rayStore, const std::vector<int>& sortIndex,
    std::vector<int>& allRayIndices,
//    std::vector<std::vector<int> > &allRaysCutAcrossSplits,
    std::vector<int> &allRaysCutAcrossSplits,
//    std::vector<std::vector<std::vector<int> > > &raysCutForSplit,
    std::vector<std::vector<int> > &raysCutForSplit,
//    std::vector<std::vector<int> > &strongVertHplaneIndex,
    std::vector<IntersectionInfo>& interPtsAndRays, const int act_ind,
    const int total_num_act, const bool isExtraAct,
    const PHAActivationOption option, AdvCuts& structPHA,
    std::vector<int>& num_gics_per_split, int& num_total_pha, Stats& phaTimeStats) {
  // Choose hyperplanes and update vertices
  GlobalVariables::timeStats.start_timer(CHOOSE_HPLANES_TIME);

//  const int numSplits = solnInfo.numFeasSplits;
//  const bool choosePerRay = !isExtraAct; // Otherwise, we choose one per split

  // Set of splits that the hyperplane activation affects;
  // i.e., split s is here if the ray intersects the
  // chosen hplane *before* the ray intersects the bd of split s.
  std::vector<int> splitsAffByHplane(1);
  const int int_option = (int) option;

  if (isExtraAct) {
#ifdef TRACE
    printf(
        "Act %d/%d: PHA(1.%d,%s): Choosing extra hplane for all splits.\n",
        act_ind + 1, total_num_act, int_option,
        PHAActivationOptionName[int_option].c_str());
#endif
    chooseHplaneBySplit(splitsAffByHplane, allRayIndices, 0, numRaysToBeCut,
        sortIndex, castsolver, solnInfo, hplaneStore, vertexStore, rayStore,
        NBSICs, distToSplitAlongRay, allRaysCutAcrossSplits, raysCutForSplit,
        interPtsAndRays, act_ind, (option == PHAActivationOption::final),
        (option == PHAActivationOption::mostremoved));
  } else {
    for (int ray_ind = indexOfFirstRayToCut;
        ray_ind < indexOfFirstRayToCut + numRaysToBeCut; ray_ind++) {
//    if (pointIsFinal[split_ind][ray_ind]) {
//      continue;
//    }

#ifdef TRACE
      const int nonBasicVarIndex = solnInfo.nonBasicVarIndex[sortIndex[ray_ind]];
      const std::string nb_var_name =
          (nonBasicVarIndex < solnInfo.numCols) ?
              solver->getColName(nonBasicVarIndex) :
              solver->getRowName(nonBasicVarIndex - solnInfo.numCols);
      printf(
          "Act %d/%d: PHA(1.%d,%s): Choosing hplane for ray %d/%d (index %d, variable %d, and name %s).\n",
          act_ind + 1, total_num_act, int_option,
          PHAActivationOptionName[int_option].c_str(),
          ray_ind - indexOfFirstRayToCut + 1, numRaysToBeCut,
          sortIndex[ray_ind], nonBasicVarIndex, nb_var_name.c_str());
#endif

      Ray rayOut;
      const int rayIndices[1] = { sortIndex[ray_ind] };
      const double rayValues[1] = { 1.0 };
      rayOut.setVector(1, rayIndices, rayValues);

      // Find hyperplane that intersects ray
      // either (0) first, (1) yielding highest avg depth of resulting points, or (2) yielding most final points
      // Returns hplane.var = -1 if no hyperplane is intersected by the ray before bd S for all splits
//    Hplane tmpHplane(HplaneVarFlag::none, HplaneRowFlag::none,
//        HplaneBoundFlag::none, numSplits);
//    chooseHplane(splitsAffByHplane, allRayIndices, 0,
//        rayStore[sortIndex[ray_ind]], sortIndex[ray_ind], castsolver, solnInfo,
//        hplaneStore, vertexStore, rayStore, NBSICs, distToSplitAlongRay,
////        allRaysCut[act_ind], raysToBeCut[act_ind],
//        allRaysCutAcrossSplits, raysCutForSplit,
////        strongVertHplaneIndex,
//        interPtsAndRays, act_ind, (option == PHAActivationOption::neighbor),
//        (option == PHAActivationOption::depth),
//        (option == PHAActivationOption::final),
//        (option == PHAActivationOption::mostremoved), isExtraAct);
      chooseHplaneByRay(splitsAffByHplane, allRayIndices, 0, rayOut,
          sortIndex[ray_ind], castsolver, solnInfo, hplaneStore, vertexStore,
          rayStore, NBSICs, distToSplitAlongRay,
          //        allRaysCut[act_ind], raysToBeCut[act_ind],
          allRaysCutAcrossSplits, raysCutForSplit,
          //        strongVertHplaneIndex,
          interPtsAndRays, act_ind, (option == PHAActivationOption::neighbor),
          (option == PHAActivationOption::depth),
          (option == PHAActivationOption::final));
    }
//    splitsAffByHplane.clear();
  }

//  // Now that we have chosen all hplanes, update count of num rays cut by each hplane
//  updateHplaneInfoAfterActivation(hplaneStore, act_ind);

  GlobalVariables::timeStats.end_timer(CHOOSE_HPLANES_TIME);

#ifdef SHOULD_WRITE_HPLANES
  printf(
      "\n## Save information about which hyperplanes will be activated. ##\n");
  char filename[300];
  snprintf(filename, sizeof(filename) / sizeof(char), "%s-hplanesAct%dOpt%d.csv",
      GlobalVariables::out_f_name_stub.c_str(), act_ind,
      static_cast<int>(option));
  FILE *hplaneOut = fopen(filename, "w");
  printActivatedHplaneDetails(hplaneOut, solnInfo, hplaneStore,
      interPtsAndRays);
  fclose(hplaneOut);
#endif

  // Create new intersection points and rays
  GlobalVariables::timeStats.start_timer(GEN_INTERSECTION_POINTS_TIME);
#ifdef TRACE
  printf("\n## Act %d/%d: Perform activations. ##\n", act_ind + 1,
      total_num_act);
#endif
  performActivations(solnInfo, vertexStore, rayStore, hplaneStore,
      interPtsAndRays, NBSICs, NBObj, objNorm, act_ind);
  GlobalVariables::timeStats.end_timer(GEN_INTERSECTION_POINTS_TIME);

  if (!GlobalConstants::NO_GEN_CUTS) {
    // Generate cuts after activation
    printf(
        "\n## Act %d/%d, PHA(1.%d,%s): Generating PHA cuts (total so far: %d) ##\n",
        act_ind + 1, total_num_act, int_option,
        PHAActivationOptionName[int_option].c_str(), num_total_pha);
//      printf("\nLast ray cut: %d\n", indexOfFirstRayToCut + numRaysToBeCut);
    if (!param.getTEMP() || isExtraAct
        || (param.getTEMP()
            && (indexOfFirstRayToCut + numRaysToBeCut
                == param.getParamVal(ParamIndices::NUM_RAYS_CUT_PARAM_IND)))) // DEBUG // TODO
      tryObjectives(structPHA, num_gics_per_split, num_total_pha, num_obj_tried,
          solnInfo, solver->getNumCols(),
          param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND), interPtsAndRays,
          rayStore, vertexStore, hplaneStore, structSICs, phaTimeStats);
  }
} /* PHAHelper */

//bool CglPHA::chooseHplane(std::vector<int>& splitsAffByHplane,
//    const std::vector<int>& allRayIndices, const int originatingVertexIndex,
//    const Ray& rayOut, const int ray_ind,
//    const LiftGICsSolverInterface* const solver, const SolutionInfo& solnInfo,
//    std::vector<Hplane>& hplaneStore, std::vector<Vertex>& vertexStore,
//    std::vector<Ray>& rayStore, const AdvCuts& NBSIC,
//    const std::vector<std::vector<double> >& distToSplitAlongRay,
//    std::vector<int>& allRaysCutAcrossSplits,
////    std::vector<std::vector<int> >& allRaysCutAcrossSplits,
////    std::vector<std::vector<std::vector<int> > >& raysCutForSplit,
//    std::vector<std::vector<int> >& raysCutForSplit,
////    std::vector<std::vector<int> > &strongVertHplaneIndex,
//    std::vector<IntersectionInfo>& interPtsAndRays, const int act_ind,
//    const bool selectByDist, const bool selectByDepth,
//    const bool selectByFinalPoints, const bool selectByMostRemoved,
//    const bool isExtraAct) {
//  if (isExtraAct) {
//    return chooseHplaneBySplit(splitsAffByHplane, allRayIndices,
//        originatingVertexIndex, rayOut, ray_ind, solver, solnInfo, hplaneStore,
//        vertexStore, rayStore, NBSIC, distToSplitAlongRay,
//        allRaysCutAcrossSplits, raysCutForSplit, interPtsAndRays, act_ind,
//        selectByDist, selectByDepth, selectByFinalPoints, selectByMostRemoved,
//        isExtraAct);
//  } else {
//    return chooseHplaneByRay(splitsAffByHplane, allRayIndices,
//        originatingVertexIndex, rayOut, ray_ind, solver, solnInfo, hplaneStore,
//        vertexStore, rayStore, NBSIC, distToSplitAlongRay,
//        allRaysCutAcrossSplits, raysCutForSplit, interPtsAndRays, act_ind,
//        selectByDist, selectByDepth, selectByFinalPoints, selectByMostRemoved,
//        isExtraAct);
//  }
//} /* chooseHplane */

bool CglPHA::chooseHplaneByRay(std::vector<int>& splitsAffByHplane,
    const std::vector<int>& allRayIndices, const int originatingVertexIndex,
    const Ray& rayOut, const int ray_ind,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    std::vector<Hplane>& hplaneStore, std::vector<Vertex>& vertexStore,
    std::vector<Ray>& rayStore, const AdvCuts& NBSIC,
    const std::vector<std::vector<double> >& distToSplitAlongRay,
    std::vector<int>& allRaysCutAcrossSplits,
//    std::vector<std::vector<int> >& allRaysCutAcrossSplits,
//    std::vector<std::vector<std::vector<int> > >& raysCutForSplit,
    std::vector<std::vector<int> >& raysCutForSplit,
//    std::vector<std::vector<int> > &strongVertHplaneIndex,
    std::vector<IntersectionInfo>& interPtsAndRays, const int act_ind,
    const bool selectByDist, const bool selectByDepth,
    const bool selectByFinalPoints) {
  // Initialize values, in case no hplane is found at all
  const int numSplitsToCheck = solnInfo.numFeasSplits; //splitsAffByHplane.size();
  Hplane tmpHplane(HplaneVarFlag::none, HplaneRowFlag::none,
      HplaneBoundFlag::none, 1); // var, row, ubflag, numsplits
  std::vector<Hplane> hplane(numSplitsToCheck);
//  for (int split_ind = 0; split_ind < numSplitsToCheck; split_ind++) {
//    hplane[split_ind].resizeVectors(solnInfo.numFeasSplits);
//  }

//  std::vector<int> tmpSplitsAffByHplane;
//  tmpSplitsAffByHplane.reserve(numSplitsToCheck);

  // For each of the splits we care about (splitsAffByHplane)
  // look at min distance to the hyperplane
  double minDistOverall = solver->getInfinity();
  std::vector<double> distToHplaneChosenForSplit(numSplitsToCheck, -1.0);
//  // look at the average depth of the points that would be created if we activated each hplane
//  double maxAverageDepth = -1.0;
//  // look at total number of final points that would be created
//  int maxNumFinalPoints = -1;
  // look at the average depth of the points that would be created if we activated each hplane
  std::vector<double> addedDepthByHplaneChosenForSplit(numSplitsToCheck, -1.0);
  // look at total number of final points that would be created
  std::vector<int> numFinalPointsByHplaneChosenForSplit(numSplitsToCheck, -1);
  int numPointsCut = 0; // not used by this function

  std::vector<Vertex> vertexCreatedForSplit(numSplitsToCheck);

//  const int tmpRayMult =
//      (param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND)
//          && solnInfo.isNBUBVar(rayVarStructIndex)) ? -1 : 1;

  // Check the basic variable in each row to see if it can be activated
  if (ACTIVATE_BASIC_BOUNDS) {
    for (int row_ind = 0; row_ind < solver->getNumRows(); row_ind++) {
      tmpHplane.var = solnInfo.varBasicInRow[row_ind];
      const bool isStructVar = (tmpHplane.var < solver->getNumCols());
      tmpHplane.row = row_ind;
      if (solver->getRowSense()[row_ind] == 'E') {
        continue; // Do not activate equality rows
      }

//      const double varVal =
//          (isStructVar) ?
//              getVarVal(solver, tmpHplane.var) :
//              std::abs(getVarVal(solver, tmpHplane.var));
      const double varVal = getVarVal(solver, tmpHplane.var);
      const double rayVal = solnInfo.raysOfC1[ray_ind][row_ind]; // rayMult * -1 * currRay[r];

      tmpHplane.bound = 0.0;
      if (isZero(rayVal, param.getRAYEPS())) {
        continue; // ray does not intersect this hyperplane
      } else if (lessThanVal(rayVal, 0.0, param.getRAYEPS())) {
        tmpHplane.bound = getVarLB(solver, tmpHplane.var);
        tmpHplane.ubflag = static_cast<int>(HplaneBoundFlag::toLB);
      } else if (greaterThanVal(rayVal, 0.0, param.getRAYEPS())) {
        tmpHplane.bound = getVarUB(solver, tmpHplane.var);
        tmpHplane.ubflag = static_cast<int>(HplaneBoundFlag::toUB);
      }

      // If this bound is actually at infinity; we will never intersect it
      if (isInfinity(std::abs(tmpHplane.bound), solver->getInfinity())) {
        continue;
      }

      if (!isStructVar) {
        tmpHplane.ubflag = static_cast<int>(HplaneBoundFlag::toLBSlack);
      }

      // Otherwise, find the distance to the hyperplane, and ensure it is actually intersected
      const double tmpDistToHplane = (tmpHplane.bound - varVal) / rayVal;
      if (isInfinity(tmpDistToHplane, solver->getInfinity())) {
        continue; // ray does not intersect this hyperplane
      }

      // Set packed vector corresponding to this vertex
      Vertex vertCreated;
      calcNewVectorCoordinates(vertCreated, tmpDistToHplane,
          vertexStore[originatingVertexIndex], rayOut);

      std::vector<double> addedDepthForThisHplane(numSplitsToCheck, 0.0);
//      double avgDepthForThisHplane = 0.0;
      std::vector<int> numFinalPointsThisHplane(numSplitsToCheck, 0);

//      bool hplaneAffectsASplit = false;
//      tmpSplitsAffByHplane.clear();
      for (int split_ind = 0; split_ind < numSplitsToCheck; split_ind++) {
//        const int split_ind = solnInfo.feasSplitVar[s]; // splitsAffByHplane[s];
        const int split_var = solnInfo.feasSplitVar[split_ind];
        if (split_var == tmpHplane.var) {
          continue; // Skip if the split var bound is the one being activated
        }

        // Check split value
        const double distToSplit =
            (originatingVertexIndex > 0) ?
                distanceToSplit(vertexStore[originatingVertexIndex], rayOut,
                    split_var, solver, solnInfo) :
                distToSplitAlongRay[split_ind][ray_ind];
        const double split_row = solnInfo.rowOfVar[split_var];
        const double splitRayVal = solnInfo.raysOfC1[ray_ind][split_row];
        const double vertexSplitVal = solnInfo.a0[split_row]
            + splitRayVal * tmpDistToHplane;
        const bool isVertexOnFloor = !greaterThanVal(vertexSplitVal,
            std::floor(solnInfo.a0[split_row]), param.getAWAY());
        const bool isVertexOnCeil = !lessThanVal(vertexSplitVal,
            std::ceil(solnInfo.a0[split_row]), param.getAWAY());
        const bool isVertexOnSplit = isVertexOnFloor || isVertexOnCeil;

        if (chooseHplaneHelper(split_ind, distToSplit, isVertexOnSplit, ray_ind,
            hplane, tmpHplane, tmpDistToHplane, minDistOverall,
            distToHplaneChosenForSplit[split_ind],
            addedDepthByHplaneChosenForSplit[split_ind],
            numFinalPointsByHplaneChosenForSplit[split_ind], numPointsCut,
            vertCreated, addedDepthForThisHplane[split_ind],
            numFinalPointsThisHplane[split_ind], numPointsCut, allRayIndices,
            solver, solnInfo, interPtsAndRays[split_ind], vertexStore, rayStore,
            NBSIC, raysCutForSplit, selectByDist, selectByDepth,
            selectByFinalPoints, false, false, true)) {
          vertexCreatedForSplit[split_ind] = vertCreated;
        }
      }
    }
  }

  // Check non-basic bound
  const int rayVarStructIndex = solnInfo.nonBasicVarIndex[ray_ind];
  if (ACTIVATE_NB_BOUNDS && rayVarStructIndex < solver->getNumCols()) {
    // distToHplane is (UB - LB) / 1.0
    const double LB = getVarLB(solver, rayVarStructIndex);
    const double UB = getVarUB(solver, rayVarStructIndex);
    if (!isNegInfinity(LB, solver->getInfinity())
        && !isInfinity(UB, solver->getInfinity())) {
      tmpHplane.var = rayVarStructIndex;
      tmpHplane.row = static_cast<int>(HplaneRowFlag::NB);
      tmpHplane.ubflag = static_cast<int>(HplaneBoundFlag::toUBNB);
      tmpHplane.bound =
          param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND) ? UB - LB : UB;

      const double tmpDistToHplane = UB - LB;
      // Set packed vector corresponding to this vertex
      Vertex vertCreated;
      calcNewVectorCoordinates(vertCreated, tmpDistToHplane,
          vertexStore[originatingVertexIndex], rayOut);

//      int numFinalPointsThisHplane = 0;
//      double avgDepthForThisHplane = 0.0;
      std::vector<double> addedDepthForThisHplane(numSplitsToCheck, 0.0);
      //      double avgDepthForThisHplane = 0.0;
      std::vector<int> numFinalPointsThisHplane(numSplitsToCheck, 0);

//      bool hplaneAffectsASplit = false;
//      tmpSplitsAffByHplane.clear();
      for (int split_ind = 0; split_ind < numSplitsToCheck; split_ind++) {
        const double distToSplit =
            (originatingVertexIndex > 0) ?
                distanceToSplit(vertexStore[originatingVertexIndex], rayOut,
                    solnInfo.feasSplitVar[split_ind], solver, solnInfo) :
                distToSplitAlongRay[split_ind][ray_ind];
        // Check split value
        const int split_var = solnInfo.feasSplitVar[split_ind];
        const double split_row = solnInfo.rowOfVar[split_var];
        const double splitRayVal = solnInfo.raysOfC1[ray_ind][split_row];
        const double vertexSplitVal = solnInfo.a0[split_row]
            + splitRayVal * tmpDistToHplane;
        const bool isVertexOnFloor = !greaterThanVal(vertexSplitVal,
            std::floor(solnInfo.a0[split_row]), param.getAWAY());
        const bool isVertexOnCeil = !lessThanVal(vertexSplitVal,
            std::ceil(solnInfo.a0[split_row]), param.getAWAY());
        const bool isVertexOnSplit = isVertexOnFloor || isVertexOnCeil;

        if (chooseHplaneHelper(split_ind, distToSplit, isVertexOnSplit, ray_ind,
            hplane, tmpHplane, tmpDistToHplane, minDistOverall,
            distToHplaneChosenForSplit[split_ind],
            addedDepthByHplaneChosenForSplit[split_ind],
            numFinalPointsByHplaneChosenForSplit[split_ind], numPointsCut,
            vertCreated, addedDepthForThisHplane[split_ind],
            numFinalPointsThisHplane[split_ind], numPointsCut, allRayIndices,
            solver, solnInfo, interPtsAndRays[split_ind], vertexStore, rayStore,
            NBSIC, raysCutForSplit, selectByDist, selectByDepth,
            selectByFinalPoints, false, true, true)) {
          vertexCreatedForSplit[split_ind] = vertCreated;
        }
      }
    }
  }

  if (isInfinity(minDistOverall, solver->getInfinity())) {
//    hplane.structvar = static_cast<int>(HplaneVarFlag::none);
//    hplane.row = static_cast<int>(HplaneRowFlag::none);
//    hplane.ubflag = static_cast<int>(HplaneBoundFlag::none);
    return false;
  }

  // If we did find a hyperplane
  // Recall var >= 0 if found, -1 if not found
  for (int split_ind = 0; split_ind < numSplitsToCheck; split_ind++) {
    // Set packed vector corresponding to this vertex
    if (hplane[split_ind].var >= 0) {
      splitsAffByHplane[0] = split_ind; // Allocated above
      updateHplaneStore(interPtsAndRays, vertexStore, 0, rayStore,
          allRaysCutAcrossSplits, raysCutForSplit, hplaneStore, solver,
          solnInfo, vertexCreatedForSplit[split_ind], hplane[split_ind],
          splitsAffByHplane, ray_ind, act_ind);
    }
  }
  return true;
} /* chooseHplaneByRay */

bool CglPHA::chooseHplaneBySplit(std::vector<int>& splitsAffByHplane,
    const std::vector<int>& allRayIndices, const int originatingVertexIndex,
    const int numRaysToCut, const std::vector<int>& sortIndex,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    std::vector<Hplane>& hplaneStore, std::vector<Vertex>& vertexStore,
    std::vector<Ray>& rayStore, const AdvCuts& NBSIC,
    const std::vector<std::vector<double> >& distToSplitAlongRay,
    std::vector<int>& allRaysCutAcrossSplits,
//    std::vector<std::vector<int> >& allRaysCutAcrossSplits,
//    std::vector<std::vector<std::vector<int> > >& raysCutForSplit,
    std::vector<std::vector<int> >& raysCutForSplit,
//    std::vector<std::vector<int> > &strongVertHplaneIndex,
    std::vector<IntersectionInfo>& interPtsAndRays, const int act_ind,
    const bool selectByFinalPoints, const bool selectByMostRemoved) {
  // Initialize values, in case no hplane is found at all
  const int numSplitsToCheck = solnInfo.numFeasSplits; //splitsAffByHplane.size();
  Hplane tmpHplane(HplaneVarFlag::none, HplaneRowFlag::none,
      HplaneBoundFlag::none, numSplitsToCheck); // var, row, ubflag, numsplits
  std::vector<Hplane> hplane(numSplitsToCheck);
  for (int split_ind = 0; split_ind < numSplitsToCheck; split_ind++) {
    hplane[split_ind].resizeVectors(1);
  }

//  std::vector<int> tmpSplitsAffByHplane;
//  tmpSplitsAffByHplane.reserve(numSplitsToCheck);

  // For each of the splits we care about (splitsAffByHplane) look at:
  // (1) min distance to the hyperplane
  double minDistOverall = solver->getInfinity();
  std::vector<double> distToHplaneChosenForSplit(numSplitsToCheck, -1.0);
//  // (2) the average depth of the points that would be created if we activated each hplane
//  double maxAverageDepth = -1.0;
//  // look at total number of final points that would be created
//  int maxNumFinalPoints = -1;
  // (2) the average depth of the points that would be created if we activated each hplane
  std::vector<double> addedDepthByHplaneChosenForSplit(numSplitsToCheck, -1.0);
  // (3) total number of final points that would be created
  std::vector<int> numFinalPointsByHplaneChosenForSplit(numSplitsToCheck, -1);
  // (4) number of points removed by the hyperplane
  std::vector<int> numPointsCutByHplaneChosenForSplit(numSplitsToCheck, -1);

//  const int tmpRayMult =
//      (param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND)
//          && solnInfo.isNBUBVar(rayVarStructIndex)) ? -1 : 1;

  // Check the basic variable in each row to see if it can be activated
  if (ACTIVATE_BASIC_BOUNDS) {
    for (int row_ind = 0; row_ind < solver->getNumRows(); row_ind++) {
      tmpHplane.var = solnInfo.varBasicInRow[row_ind];
      const bool isStructVar = (tmpHplane.var < solver->getNumCols());
      tmpHplane.row = row_ind;
      if (solver->getRowSense()[row_ind] == 'E') {
        continue; // Do not activate equality rows
      }
      const int numBoundsToCheck = 1 + isStructVar;

      for (int b = 0; b < numBoundsToCheck; b++) {
        std::vector<double> addedDepthForThisHplane(numSplitsToCheck, 0.0);
        std::vector<int> numFinalPointsThisHplane(numSplitsToCheck, 0);
        std::vector<int> numRaysIntersectedThisHplane(numSplitsToCheck, 0);
        std::vector<double> totalDistToThisHplane(numSplitsToCheck, 0.0);
        std::vector<int> numPointsCutByThisHplane(numSplitsToCheck, -1);

        if (b == 0) {
          tmpHplane.bound = getVarLB(solver, tmpHplane.var);
          tmpHplane.ubflag = static_cast<int>(HplaneBoundFlag::toLB);
        } else {
          tmpHplane.bound = getVarUB(solver, tmpHplane.var);
          tmpHplane.ubflag = static_cast<int>(HplaneBoundFlag::toUB);
        }

        // If this bound is actually at infinity; we will never intersect it
        if (isInfinity(std::abs(tmpHplane.bound), solver->getInfinity())) {
          continue;
        }

        if (!isStructVar) {
          tmpHplane.ubflag = static_cast<int>(HplaneBoundFlag::toLBSlack);
        }

        const double varVal = getVarVal(solver, tmpHplane.var);

        for (int tmp_ray_ind = 0; tmp_ray_ind < numRaysToCut; tmp_ray_ind++) {
          const int ray_ind = sortIndex[tmp_ray_ind];
          const double rayVal = solnInfo.raysOfC1[ray_ind][row_ind]; // rayMult * -1 * currRay[r];

          // Check that ray intersects this hyperplane
          if (isZero(rayVal, param.getRAYEPS())) {
            continue;
          }
          if (b == 1 && lessThanVal(rayVal, 0.0, param.getRAYEPS())) {
            continue;
          }
          if (b == 0 && greaterThanVal(rayVal, 0.0, param.getRAYEPS())) {
            continue;
          }

          // Otherwise, find the distance to the hyperplane, and ensure it is actually intersected
          const double tmpDistToHplane = (tmpHplane.bound - varVal) / rayVal;
          if (isInfinity(tmpDistToHplane, solver->getInfinity())) {
            continue; // ray does not intersect this hyperplane
          }

          // Set packed vector corresponding to this vertex
          Vertex vertCreated;
          Ray rayOut;
          const int rayIndices[1] = { ray_ind };
          const double rayValues[1] = { 1.0 };
          rayOut.setVector(1, rayIndices, rayValues);
          calcNewVectorCoordinates(vertCreated, tmpDistToHplane,
              vertexStore[originatingVertexIndex], rayOut);

          for (int split_ind = 0; split_ind < numSplitsToCheck; split_ind++) {
            if (solnInfo.feasSplitVar[split_ind] == tmpHplane.var) {
              continue; // Skip if the split var bound is the one being activated
            }

            const int oldHplaneForSplitOverall = hasHplaneBeenActivated(
                hplaneStore, interPtsAndRays[split_ind].allHplaneToAct,
                tmpHplane, true);
            if (oldHplaneForSplitOverall >= 0) {
              const bool rayCutByCurrHplane =
                  (find_val(ray_ind,
                      hplaneStore[oldHplaneForSplitOverall].rayToBeCutByHplane[split_ind])
                      >= 0);
              if (rayCutByCurrHplane) {
                continue; // Skip if this hplane has already been activated on this ray
              }
            }

            const int split_var = solnInfo.feasSplitVar[split_ind];
            if (split_var == tmpHplane.var) {
              continue; // Skip if the split var bound is the one being activated
            }

            // Check distance to split
            const double distToSplit =
                (originatingVertexIndex > 0) ?
                    distanceToSplit(vertexStore[originatingVertexIndex], rayOut,
                        split_var, solver, solnInfo) :
                    distToSplitAlongRay[split_ind][ray_ind];

            // Check split value
            const double split_row = solnInfo.rowOfVar[split_var];
            const double splitRayVal =
                solnInfo.raysOfC1[ray_ind][split_row];
            const double vertexSplitVal = solnInfo.a0[split_row]
                + splitRayVal * tmpDistToHplane;
            const bool isVertexOnFloor = !greaterThanVal(vertexSplitVal,
                std::floor(solnInfo.a0[split_row]), param.getAWAY());
            const bool isVertexOnCeil = !lessThanVal(vertexSplitVal,
                std::ceil(solnInfo.a0[split_row]), param.getAWAY());
            const bool isVertexOnSplit = isVertexOnFloor || isVertexOnCeil;

            // Decide whether to use this hplane for this split
            if (chooseHplaneHelper(split_ind, distToSplit, isVertexOnSplit,
                ray_ind, hplane, tmpHplane, tmpDistToHplane, minDistOverall,
                distToHplaneChosenForSplit[split_ind],
                addedDepthByHplaneChosenForSplit[split_ind],
                numFinalPointsByHplaneChosenForSplit[split_ind],
                numPointsCutByHplaneChosenForSplit[split_ind], vertCreated,
                addedDepthForThisHplane[split_ind],
                numFinalPointsThisHplane[split_ind],
                numPointsCutByThisHplane[split_ind], allRayIndices, solver,
                solnInfo, interPtsAndRays[split_ind], vertexStore, rayStore,
                NBSIC, raysCutForSplit, false, false, selectByFinalPoints, true,
                false, false)) {
              // If this ray is cut by the hplane
              // Count the distance
              numRaysIntersectedThisHplane[split_ind]++;
              totalDistToThisHplane[split_ind] += tmpDistToHplane;
              tmpHplane.rayToBeCutByHplane[split_ind].push_back(ray_ind);
            } /* end hyperplane update for the specific split */
          } /* end iterating over splits to check if hplane affects ray on each split */
        } /* end iterating over rays */

        // Check if this hyperplane is better than the previous one chosen for each split
        for (int split_ind = 0; split_ind < numSplitsToCheck; split_ind++) {
          if ((int) tmpHplane.rayToBeCutByHplane[split_ind].size() == 0) {
            continue;
          }
          const double avgDistToThisHplane = totalDistToThisHplane[split_ind]
              / numRaysIntersectedThisHplane[split_ind];
          const bool moreFinalPoints = (selectByFinalPoints
              && (numFinalPointsThisHplane[split_ind]
                  > numFinalPointsByHplaneChosenForSplit[split_ind]));
          const bool cutMorePoints = numPointsCutByThisHplane[split_ind]
              > numPointsCutByHplaneChosenForSplit[split_ind];
          const bool tieBreaking = cutMorePoints
              || tieBreakingForChooseHplane(false, false, selectByFinalPoints,
                  split_ind, distToHplaneChosenForSplit,
                  addedDepthByHplaneChosenForSplit,
                  numFinalPointsByHplaneChosenForSplit, avgDistToThisHplane,
                  addedDepthForThisHplane, numFinalPointsThisHplane);

          if ((moreFinalPoints || tieBreaking)) {
            if (lessThanVal(avgDistToThisHplane, minDistOverall)) {
              minDistOverall = avgDistToThisHplane;
            }
            distToHplaneChosenForSplit[split_ind] = avgDistToThisHplane;
            addedDepthByHplaneChosenForSplit[split_ind] =
                addedDepthForThisHplane[split_ind];
            numFinalPointsByHplaneChosenForSplit[split_ind] =
                numFinalPointsThisHplane[split_ind];
            numPointsCutByHplaneChosenForSplit[split_ind] =
                numPointsCutByThisHplane[split_ind];

            hplane[split_ind].var = tmpHplane.var;
            hplane[split_ind].row = tmpHplane.row;
            hplane[split_ind].ubflag = tmpHplane.ubflag;
            hplane[split_ind].bound = tmpHplane.bound;
            if (!(tmpHplane.var < solver->getNumCols())) {
              hplane[split_ind].ubflag =
                  static_cast<int>(HplaneBoundFlag::toLBSlack);
            } else {
              // If this variable was originally NB and at its upper-bound,
              // then we had complemented it to be at its lower-bound
              // Hence, if it is at its upper-bound now, this is the same as its lower-bound
              // in the complemented non-basic space
              if (param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND)
                  && solnInfo.isNBUBVar(tmpHplane.var)) {
                hplane[split_ind].ubflag *= -1 * tmpHplane.ubflag + 1; // Switch between 0 and 1
              } // TODO Double check this
            }
            hplane[split_ind].rayToBeCutByHplane[0] =
                tmpHplane.rayToBeCutByHplane[split_ind];
          } /* end hyperplane update for the specific split */
          tmpHplane.rayToBeCutByHplane[split_ind].clear();
        } /* end looping over splits to update hyperplanes */
      } /* end looping over bounds to check */
    } /* end looping over rows */
  } /* checking if should activate basic bounds */

  // Check non-basic bounds
  if (ACTIVATE_NB_BOUNDS) {
    for (int tmp_ray_ind = 0; tmp_ray_ind < numRaysToCut; tmp_ray_ind++) {
      const int ray_ind = sortIndex[tmp_ray_ind];
      const int rayVarStructIndex = solnInfo.nonBasicVarIndex[ray_ind];
      if (rayVarStructIndex < solver->getNumCols()) {
        Ray rayOut;
        const int rayIndices[1] = { ray_ind };
        const double rayValues[1] = { 1.0 };
        rayOut.setVector(1, rayIndices, rayValues);

        // distToHplane is (UB - LB) / 1.0
        const double LB = getVarLB(solver, rayVarStructIndex);
        const double UB = getVarUB(solver, rayVarStructIndex);
        if (!isNegInfinity(LB, solver->getInfinity())
            && !isInfinity(UB, solver->getInfinity())) {
          tmpHplane.var = rayVarStructIndex;
          tmpHplane.row = static_cast<int>(HplaneRowFlag::NB);
          tmpHplane.ubflag = static_cast<int>(HplaneBoundFlag::toUBNB);
          tmpHplane.bound =
              param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND) ?
                  UB - LB : UB;

          const double tmpDistToHplane = UB - LB;
          // Set packed vector corresponding to this vertex
          Vertex vertCreated;
          calcNewVectorCoordinates(vertCreated, tmpDistToHplane,
              vertexStore[originatingVertexIndex], rayOut);

          //      int numFinalPointsThisHplane = 0;
          //      double avgDepthForThisHplane = 0.0;
          std::vector<double> addedDepthForThisHplane(numSplitsToCheck, 0.0);
          //      double avgDepthForThisHplane = 0.0;
          std::vector<int> numFinalPointsThisHplane(numSplitsToCheck, 0);
          std::vector<int> numPointsCutThisHplane(numSplitsToCheck, 0);

          //      bool hplaneAffectsASplit = false;
          //      tmpSplitsAffByHplane.clear();
          for (int split_ind = 0; split_ind < numSplitsToCheck; split_ind++) {
            const int oldHplaneForSplitOverall = hasHplaneBeenActivated(
                hplaneStore, interPtsAndRays[split_ind].allHplaneToAct,
                tmpHplane, true);
            if (oldHplaneForSplitOverall >= 0) {
              const bool rayCutByCurrHplane =
                  (find_val(ray_ind,
                      hplaneStore[oldHplaneForSplitOverall].rayToBeCutByHplane[split_ind])
                      >= 0);
              if (rayCutByCurrHplane) {
                continue; // Skip if this hplane has already been activated on this ray
              }
            }
            const double distToSplit =
                (originatingVertexIndex > 0) ?
                    distanceToSplit(vertexStore[originatingVertexIndex], rayOut,
                        solnInfo.feasSplitVar[split_ind], solver, solnInfo) :
                    distToSplitAlongRay[split_ind][ray_ind];

            // Check split value
            const int split_var = solnInfo.feasSplitVar[split_ind];
            const double split_row = solnInfo.rowOfVar[split_var];
            const double splitRayVal = solnInfo.raysOfC1[ray_ind][split_row];
            const double vertexSplitVal = solnInfo.a0[split_row]
                + splitRayVal * tmpDistToHplane;
            const bool isVertexOnFloor = !greaterThanVal(vertexSplitVal,
                std::floor(solnInfo.a0[split_row]), param.getAWAY());
            const bool isVertexOnCeil = !lessThanVal(vertexSplitVal,
                std::ceil(solnInfo.a0[split_row]), param.getAWAY());
            const bool isVertexOnSplit = isVertexOnFloor || isVertexOnCeil;

            if (chooseHplaneHelper(split_ind, distToSplit, isVertexOnSplit,
                ray_ind, hplane, tmpHplane, tmpDistToHplane, minDistOverall,
                distToHplaneChosenForSplit[split_ind],
                addedDepthByHplaneChosenForSplit[split_ind],
                numFinalPointsByHplaneChosenForSplit[split_ind],
                numPointsCutByHplaneChosenForSplit[split_ind], vertCreated,
                addedDepthForThisHplane[split_ind],
                numFinalPointsThisHplane[split_ind],
                numPointsCutThisHplane[split_ind], allRayIndices, solver,
                solnInfo, interPtsAndRays[split_ind], vertexStore, rayStore,
                NBSIC, raysCutForSplit, false, false, selectByFinalPoints, true,
                true, true)) {
              // Update rays cut; other items updated within chooseHplaneHelper since selecting by "ray"
              hplane[split_ind].rayToBeCutByHplane[0].assign(rayIndices,
                  rayIndices + 1);
            } /* end hyperplane update for the specific split */
          } /* end looping over splits */
        } /* ensure ray is bounded from both sides */
      } /* ensure the ray is not corresponding to a slack variable */
    } /* looping over rays to activate non-basic bounds */
  } /* checking if should activate non-basic bounds */


  if (isInfinity(minDistOverall, solver->getInfinity())) {
//    hplane.structvar = static_cast<int>(HplaneVarFlag::none);
//    hplane.row = static_cast<int>(HplaneRowFlag::none);
//    hplane.ubflag = static_cast<int>(HplaneBoundFlag::none);
    return false;
  }


  // If we did find a hyperplane
  // Recall var >= 0 if found, -1 if not found
  for (int split_ind = 0; split_ind < numSplitsToCheck; split_ind++) {
    // Set packed vector corresponding to this vertex
    if (hplane[split_ind].var >= 0) {
      const int numRaysToBeCutByThisHplane =
          hplane[split_ind].rayToBeCutByHplane[0].size();
      for (int r = 0; r < numRaysToBeCutByThisHplane; r++) {
        const int ray_ind = hplane[split_ind].rayToBeCutByHplane[0][r];
        const int row_ind = hplane[split_ind].row;
        const double tmpDistToHplane = (hplane[split_ind].bound
            - getVarVal(solver, hplane[split_ind].var))
            / ((row_ind < 0) ? 1. : solnInfo.raysOfC1[ray_ind][row_ind]);
        Vertex vertCreated;
        Ray rayOut;
        const int rayIndices[1] = { ray_ind };
        const double rayValues[1] = { 1.0 };
        rayOut.setVector(1, rayIndices, rayValues);
        calcNewVectorCoordinates(vertCreated, tmpDistToHplane,
            vertexStore[originatingVertexIndex], rayOut);
        splitsAffByHplane[0] = split_ind; // Allocated above
        updateHplaneStore(interPtsAndRays, vertexStore, 0, rayStore,
            allRaysCutAcrossSplits, raysCutForSplit, hplaneStore, solver,
            solnInfo, vertCreated, hplane[split_ind], splitsAffByHplane,
            ray_ind, act_ind, true);
      }
    }
  }
  return true;
} /* chooseHplaneBySplit */

bool CglPHA::chooseHplaneHelper(const int split_ind, const double distToSplit,
    const bool isVertexOnSplit, const int ray_ind, std::vector<Hplane>& hplane,
    const Hplane& tmpHplane, const double tmpDistToHplane,
    double& minDistOverall, double& distToHplaneChosenForSplit,
    double& addedDepthByHplaneChosenForSplit,
    int& numFinalPointsByHplaneChosenForSplit,
    int& numPointsCutByHplaneChosenForSplit,
//    std::vector<double>& distToHplaneChosenForSplit,
//    std::vector<double>& addedDepthByHplaneChosenForSplit,
//    std::vector<int>& numFinalPointsByHplaneChosenForSplit,
//    std::vector<int>& numPointsCutByHplane,
    const Vertex& vertCreated, double& addedDepthForThisHplane,
    int& numFinalPointsThisHplane, int& numPointsCutThisHplane,
//    std::vector<double>& addedDepthForThisHplane,
//    std::vector<int>& numFinalPointsThisHplane,
    const std::vector<int>& allRayIndices,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    const IntersectionInfo& interPtsAndRays, std::vector<Vertex>& vertexStore,
    std::vector<Ray>& rayStore, const AdvCuts& NBSIC,
    std::vector<std::vector<int> >& raysCutForSplit, const bool selectByDist,
    const bool selectByDepth, const bool selectByFinalPoints,
    const bool calcPointsRemoved, const bool isNBBound,
    const bool chooseByRay) {
  if (solnInfo.feasSplitVar[split_ind] == tmpHplane.var) {
    return false; // Skip if the split var bound is the one being activated
  }

  if (!CUT_PARALLEL_RAYS && isInfinity(distToSplit, solver->getInfinity())) {
    return false;
  }

  // Check whether the point hits the split first
  if (!lessThanVal(tmpDistToHplane, distToSplit) || isVertexOnSplit) {
    return false;
  }

    // The hplane is intersected before bd S
  IntersectionInfo* tmpInterPtsAndRays;
  if (!selectByDist) { // Currently skip this for selectByDist
    tmpInterPtsAndRays = new IntersectionInfo;
    tmpInterPtsAndRays->reverseOrdering();
    tmpInterPtsAndRays->setDimensions(0, solnInfo.numNB);
    fakeActivateHplane(numFinalPointsThisHplane, selectByFinalPoints,
        addedDepthForThisHplane, selectByDepth, tmpDistToHplane, solver,
        solnInfo, split_ind, vertexStore, vertCreated, rayStore, ray_ind,
        tmpHplane, NBSIC[split_ind], allRayIndices, *tmpInterPtsAndRays,
        interPtsAndRays, raysCutForSplit[split_ind]);
  }

  if (calcPointsRemoved) {
    std::vector<int> tmpPointsToRemove;
//      std::vector<int> splitsAffByHplane = { split_ind };
//      std::vector<std::vector<int> > tmpPointsToRemove;
    pointsFromRayViolatingHplane(numPointsCutByHplaneChosenForSplit,
        tmpPointsToRemove, ray_ind, vertexStore, rayStore, tmpHplane,
        interPtsAndRays, split_ind, solver, solnInfo);
  }

  if (!selectByDist) {
    if (tmpInterPtsAndRays) {
      delete tmpInterPtsAndRays;
    }
  }

  if (!chooseByRay) {
    return true;
  }

  // If the distance is closer than what we had before, use this hplane instead
  const bool hplaneIsCloser = (selectByDist
      && (lessThanVal(distToHplaneChosenForSplit, 0.0)
          || lessThanVal(tmpDistToHplane, distToHplaneChosenForSplit)));
  const bool higherDepth = (selectByDepth
      && greaterThanVal(addedDepthForThisHplane,
          addedDepthByHplaneChosenForSplit));
  const bool moreFinalPoints = (selectByFinalPoints
      && (numFinalPointsThisHplane > numFinalPointsByHplaneChosenForSplit));
  const bool cutMorePoints = (calcPointsRemoved
      && (numPointsCutThisHplane > numPointsCutByHplaneChosenForSplit));
  const bool tieBreaking = tieBreakingForChooseHplane(selectByDist,
      selectByDepth, selectByFinalPoints, distToHplaneChosenForSplit,
      addedDepthByHplaneChosenForSplit, numFinalPointsByHplaneChosenForSplit,
      tmpDistToHplane, addedDepthForThisHplane, numFinalPointsThisHplane);

  if ((hplaneIsCloser || higherDepth || moreFinalPoints || cutMorePoints
      || tieBreaking)) {
    if (lessThanVal(tmpDistToHplane, minDistOverall)) {
      minDistOverall = tmpDistToHplane;
    }
    distToHplaneChosenForSplit = tmpDistToHplane;
    addedDepthByHplaneChosenForSplit = addedDepthForThisHplane;
    numFinalPointsByHplaneChosenForSplit = numFinalPointsThisHplane;
    numPointsCutByHplaneChosenForSplit = numPointsCutThisHplane;

    hplane[split_ind].var = tmpHplane.var;
    hplane[split_ind].row = tmpHplane.row;
    hplane[split_ind].ubflag = tmpHplane.ubflag;
    hplane[split_ind].bound = tmpHplane.bound;
    if (!isNBBound) {
      if (!(tmpHplane.var < solver->getNumCols())) {
        hplane[split_ind].ubflag = static_cast<int>(HplaneBoundFlag::toLBSlack);
      } else {
        // If this variable was originally NB and at its upper-bound,
        // then we had complemented it to be at its lower-bound
        // Hence, if it is at its upper-bound now, this is the same as its lower-bound
        // in the complemented non-basic space
        if (param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND)
            && solnInfo.isNBUBVar(tmpHplane.var)) {
          hplane[split_ind].ubflag *= -1 * tmpHplane.ubflag + 1; // Switch between 0 and 1
        } // TODO Double check this
      }
    }
    return true;
  } else {
    return false;
  }
} /* chooseHplaneHelper */

void CglPHA::fakeActivateHplane(int& numFinalPointsAdded,
    const bool calcFinalPoints, double& avgAddedDepth, const bool calcDepth,
    const double distToHplane, const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo, const int split_ind,
    const std::vector<Vertex>& vertexStore, const Vertex& vertCreated,
    const std::vector<Ray>& rayStore, const int ray_ind, const Hplane& hplane,
    const AdvCut& NBSIC, const std::vector<int>& allRayIndices,
    IntersectionInfo& interPtsAndRays,
    const IntersectionInfo& oldInterPtsAndRays,
    const std::vector<int>& raysPrevCut) {
//  int totalNumPointsAdded = 0;
//  int totalNumPointsRemoved = 0;
//  double avgRemovedDepth = 0.0;

  const std::vector<int> raysToBeCutByHplane(1, ray_ind);
  std::vector<int> vertOfAllRaysIntByHplane, allRaysIntByHplane;

  // Calculate totalSOS for the SIC for this split
  CoinPackedVector SICVec;
  if (calcDepth) {
    SICVec = NBSIC.row();
  }
  const double norm = (calcDepth) ? std::sqrt(SICVec.normSquare()) : 0;

  // Get all rays intersected by this hplane
  addRaysIntByHplaneBeforeBdS(vertOfAllRaysIntByHplane, allRaysIntByHplane,
      solver, solnInfo, hplane, vertexStore, 0, rayStore, split_ind);

  // We we want to track the average depth of the removed intersection points
//  double sumRemovedDepth = 0.0;
  double sumAddedDepth = 0.0;
//  int numNewPointsRemoved = 0;
  int numNewPointsAdded = 0;

  // Rays cut: ray_ind + all rays from allRaysIntByHplane that have previously been cut
  std::vector<bool> raysCutFlag(solnInfo.numNB, false);
  std::vector<bool> rayIntersectedFlag(solnInfo.numNB, false);

  raysCutFlag[ray_ind] = true;
  const int numRaysIntByHplane = allRaysIntByHplane.size();
  for (int r = 0; r < numRaysIntByHplane; r++) {
    rayIntersectedFlag[allRaysIntByHplane[r]] = true;
  }
  for (int r = 0; r < (int) raysPrevCut.size(); r++) {
    if (rayIntersectedFlag[raysPrevCut[r]])
      raysCutFlag[raysPrevCut[r]] = true;
  }

  // Remove all intersection points cut off by this hyperplane
  const int split_var = solnInfo.feasSplitVar[split_ind];
//  if (calcDepth) {
////    numNewPointsRemoved +=
//    removePtsAndRaysCutByHplane(solver, solnInfo, raysCutFlag, split_var,
//        hplane, vertexStore, rayStore, interPtsAndRays, &NBSIC, norm,
//        sumRemovedDepth, true);
//  }

  // Calculate average depth of the removed points
//  if (numNewPointsRemoved > 0) {
//    avgRemovedDepth += sumRemovedDepth / numNewPointsRemoved;
//    totalNumPointsRemoved += numNewPointsRemoved;
//  }

  // Create the set J \setminus K
  std::vector<int> newRayColIndices;
  storeNewRayNBIndices(solnInfo.numNB, newRayColIndices, allRaysIntByHplane,
      raysCutFlag);
  const int numNewRays = newRayColIndices.size();

  //For ray intersected by hyperplane H generate new rays corresponding to each element in J \setminus K
  //This is of course available in closed form from the tableau
  for (int r = 0; r < numNewRays; r++) {
    Ray newRay;
    Point tmpPoint;
    calcNewRayCoordinatesRank1(newRay, /*solver,*/ solnInfo, ray_ind,
        newRayColIndices[r], /*rayStore,*/ hplane, param.getRAYEPS());
    bool ptFlagToAdd = calcIntersectionPointWithSplit(tmpPoint, solver,
        solnInfo, vertCreated, newRay, split_var);

    if (ptFlagToAdd) {
      if (!duplicatePoint(interPtsAndRays, interPtsAndRays.RHS, tmpPoint)) {
        numNewPointsAdded++;
        interPtsAndRays.addPointOrRayToIntersectionInfo(tmpPoint, 1.0, ray_ind,
            newRayColIndices[r], -1, -1, -1, -1, -1., -1., false);

        // Check whether it is a final point
        if (calcFinalPoints && pointInP(solver, solnInfo, tmpPoint)) {
          if (!duplicatePoint(oldInterPtsAndRays, oldInterPtsAndRays.RHS,
              tmpPoint)) {
            numFinalPointsAdded++;
          }
        }

        if (calcDepth) {
          // Calculate depth of added point
          const double SIC_activity = getSICActivity(split_var, vertexStore,
              rayStore, solver, solnInfo, tmpPoint);
          double curr_normed_viol = (SIC_activity - NBSIC.rhs()) / norm;
          if (isZero(curr_normed_viol)) {
            curr_normed_viol = +0.0;
          }
          sumAddedDepth += curr_normed_viol;

          if (lessThanVal(curr_normed_viol, 0.0)) {
            error_msg(errstr,
                "Depth %f is negative! Point cannot be above the SIC.\n",
                curr_normed_viol);
            writeErrorToLog(errstr, GlobalVariables::log_file);
            exit(1);
          }
        }
      }
    }
//    else if (!CHECK_FOR_DUP_RAYS
//        || !duplicateRay(interPtsAndRays, interPtsAndRays.RHS, newRay)) {
//      interPtsAndRays.addPointOrRayToIntersectionInfo(newRay, 0.0, ray_ind,
//          newRayColIndices[r], -1, -1);
//    }
  }
  if (numNewPointsAdded > 0) {
    avgAddedDepth += sumAddedDepth / numNewPointsAdded;
//    totalNumPointsAdded += numNewPointsAdded;
  }
} /* fakeActivateHplane */

/**
 * Finds points created from rayInd that are cut by this hyperplane
 */
void CglPHA::pointsFromRayViolatingHplane(int& numPointsCutByHplane,
    //std::vector<int>& numPointsCutByHplane,
    std::vector<int>& pointsToRemove,
//    std::vector<std::vector<int> >& pointsToRemove,
    const int rayInd,
    const std::vector<Vertex>& vertexStore, const std::vector<Ray>& rayStore,
    const Hplane& hplane, const IntersectionInfo& interPtsAndRays,
    const int splitsAffByHplane,
//    const std::vector<IntersectionInfo>& interPtsAndRays,
//    const std::vector<int> & splitsAffByHplane,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo) {
//  const int numSplitsAffected = (int) splitsAffByHplane.size();
  pointsToRemove.clear();
//  pointsToRemove.resize(numSplitsAffected);
//  numPointsCutByHplane.resize(numSplitsAffected, 0);
  int numViolPoints = 0;
//  for (int s_ind = 0; s_ind < numSplitsAffected; s_ind++) {
//    const int s = splitsAffByHplane[s_ind];
//  const int s = splitsAffByHplane;
  for (int row_ind = 0; row_ind < interPtsAndRays.getNumRows(); row_ind++) {
//    const int numPointsAndRaysByRay =
//        rayStore[rayInd].partitionOfPtsAndRaysByRaysOfC1[s].size();
//    for (int ind = 0; ind < numPointsAndRaysByRay; ind++) {
//      const int curr_ptray_ind =
//          rayStore[rayInd].partitionOfPtsAndRaysByRaysOfC1[s][ind];
    const int ray_ind =
        (interPtsAndRays.cutRay[row_ind] >= 0) ?
            interPtsAndRays.cutRay[row_ind] :
            interPtsAndRays.newRay[row_ind];
    if (ray_ind != rayInd) {
      continue;
    }

    // Ensure this is a point, not a ray
    if (isZero(interPtsAndRays.RHS[row_ind])) {
      continue;
    }

    const CoinShallowPackedVector tmpVector = interPtsAndRays.getVector(
        row_ind);
    const int numElmts = tmpVector.getNumElements();
    const int* elmtIndices = tmpVector.getIndices();
    const double* elmtVals = tmpVector.getElements();

    const bool NBFlag = (hplane.row == static_cast<int>(HplaneRowFlag::NB));

    // The NBFlag prevents us from using a -1 row index
    const double xVal = !NBFlag * solnInfo.a0[!NBFlag * hplane.row];
    double NBRowActivity = 0.0;
    for (int k = 0; k < numElmts; k++) {
      if (!NBFlag) {
        NBRowActivity += solnInfo.raysOfC1[elmtIndices[k]][hplane.row]
            * elmtVals[k];
      } else if (elmtIndices[k] == hplane.var) {
        NBRowActivity += elmtVals[k];
      }
    }
    const double newVal = xVal + NBRowActivity;
    double colLB, colUB;

    if (hplane.var < solver->getNumCols()) {
      if (!NBFlag) {
        colLB = getVarLB(solver, hplane.var);
        colUB = getVarUB(solver, hplane.var);
      } else {
        colLB = 0.0;
        colUB = getVarUB(solver, hplane.var) - getVarLB(solver, hplane.var);
      }
    } else {
      colLB = 0.0;
      colUB = solver->getInfinity();
    }

    if (lessThanVal(newVal, colLB) || greaterThanVal(newVal, colUB)) {
      numViolPoints++;
      pointsToRemove.push_back(row_ind);
      if (numPointsCutByHplane == -1) {
        numPointsCutByHplane++;
      }
      numPointsCutByHplane++;
    }
  }
//}
} /* pointsFromRayViolatingHplane */

/***********************************************************************/
void CglPHA::addInterPtFromUncutRayOfC1(IntersectionInfo& interPtsAndRays,
    double& distToSplitAlongRay, const SolutionInfo& solnInfo,
    const int split_ind, const std::vector<Ray>& rayStore, const int ray_ind,
    const std::vector<Vertex>& vertexStore, const bool recalc_dist) {
  const char* rowSense = solver->getRowSense();

  if (rayStore[ray_ind].cutFlag[split_ind]) {
    return;
  }

  const int nb_var = solnInfo.nonBasicVarIndex[ray_ind];
  if ((nb_var >= solnInfo.numCols)
      && (rowSense[nb_var - solnInfo.numCols] == 'E')) {
    return;
  }

  Point tmpPoint;
  bool rayIntSplitFlag = calcIntersectionPointWithSplit(tmpPoint,
      distToSplitAlongRay, castsolver, solnInfo, vertexStore[0],
      rayStore[ray_ind], solnInfo.feasSplitVar[split_ind], recalc_dist);
  if (!rayIntSplitFlag) {
//    // Make sure this is not a duplicate
//    if (!CHECK_FOR_DUP_RAYS
//        || !duplicateRay(interPtsAndRays, interPtsAndRays.RHS,
//            rayStore[ray_ind])) {
    interPtsAndRays.addPointOrRayToIntersectionInfo(rayStore[ray_ind], 0.0, -1,
        ray_ind, -1, -1, 0, -1, 0.0, 0.0, false); // TODO Should check for finality somewhere
//    }
  } else {
//    if (!duplicatePoint(interPtsAndRays, interPtsAndRays.RHS, tmpPoint)) {
    // TODO Check if the ray value is equal to the distance to split; should be the same
    const bool currPointIsFinal = pointInP(castsolver, solnInfo, tmpPoint);
    interPtsAndRays.addPointOrRayToIntersectionInfo(tmpPoint, 1.0, -1, ray_ind,
        -1, -1, 0, -1, 0.0, 0.0, currPointIsFinal);
//    }
  }
} /* addInterPtFromUncutRayOfC1 */

/***********************************************************************/
void CglPHA::performActivations(const SolutionInfo& solnInfo,
    const std::vector<Vertex>& vertexStore, std::vector<Ray>& rayStore,
    const std::vector<Hplane>& hplaneStore,
    std::vector<IntersectionInfo>& interPtsAndRays, const AdvCuts& NBSIC,
    const AdvCut& objCut, const double objNorm, const int act_ind) {
  for (int split_ind = 0; split_ind < solnInfo.numFeasSplits; split_ind++) {
    const int numHplanesActForSplit =
        interPtsAndRays[split_ind].hplaneIndexByAct[act_ind].size();

    // Calculate norm for the SIC for this split
    //const CoinPackedVector SICVec = NBSIC[split_ind].row();
    const AdvCut* currNBSIC = NBSIC.getCutPointer(split_ind);
    const double NBSICNorm = (currNBSIC) ? (currNBSIC->row()).twoNorm() : 0.0;

    for (int h = 0; h < numHplanesActForSplit; h++) {
      const int hplane_ind =
          interPtsAndRays[split_ind].hplaneIndexByAct[act_ind][h];
      const int hplane_ind_in_split =
          interPtsAndRays[split_ind].indexOfHplaneInOverallOrderForSplit[act_ind][h];
#ifdef TRACE
      printf(
          "Act %d, split %d (var %d), hplane %d/%d: hplane var %d, hplane row index %d.\n",
          act_ind + 1, split_ind, solnInfo.feasSplitVar[split_ind], h + 1,
          numHplanesActForSplit, hplaneStore[hplane_ind].var,
          hplaneStore[hplane_ind].row);
#endif
      activateHplane(interPtsAndRays[split_ind].numPointsRemoved[act_ind],
          interPtsAndRays[split_ind].avgRemovedDepth[act_ind],
          interPtsAndRays[split_ind].numPointsAdded[act_ind],
          interPtsAndRays[split_ind].avgAddedDepth[act_ind],
          interPtsAndRays[split_ind], split_ind, hplane_ind,
          hplane_ind_in_split, hplaneStore, hplaneStore[hplane_ind], rayStore,
          vertexStore, castsolver, solnInfo, currNBSIC, NBSICNorm, objCut,
          objNorm, act_ind);
    }

    // Average average depth
    if (numHplanesActForSplit > 0) {
      interPtsAndRays[split_ind].avgRemovedDepth[act_ind] /=
          numHplanesActForSplit;
      interPtsAndRays[split_ind].avgAddedDepth[act_ind] /=
          numHplanesActForSplit;
    }
  }
} /* performActivations */

/***********************************************************************/
void CglPHA::tryObjectives(AdvCuts& structPHA,
    std::vector<int>& num_cuts_per_cgs, int& num_total_gics, int& num_obj_tried,
    const SolutionInfo& solnInfo, const int dim, const bool inNBSpace,
//    const int cgs_ind, const std::string& cgsName,
    const std::vector<IntersectionInfo>& interPtsAndRays,
    const std::vector<Ray>& rayStore, const std::vector<Vertex>& vertexStore,
    const std::vector<Hplane>& hplaneStore, const AdvCuts& structSICs, Stats& phaTimeStats) {
  GlobalVariables::timeStats.start_timer(GEN_MPHA_TIME);

  // Set up solvers with points and rays found
  std::vector<PointCutsSolverInterface*> cutSolver(solnInfo.numFeasSplits);
  // This is in the * complemented * non-basic space,
  // so the origin is the solution to the LP relaxation
  // Moreover, every cut will have right-hand side 1,
  // since we want to separate the origin
  //const double beta = 1.0;
  std::vector<bool> isCutSolverPrimalFeasible(solnInfo.numFeasSplits);
  std::vector<int> nonZeroColIndex;
  for (int s = 0; s < solnInfo.numFeasSplits; s++) {
    if (reachedCutLimit(num_cuts_per_cgs[s], num_total_gics)) {
      isCutSolverPrimalFeasible[s] = false;
      continue;
    }

    cutSolver[s] = new PointCutsSolverInterface;
    isCutSolverPrimalFeasible[s] = setupCutSolver(cutSolver[s],
        interPtsAndRays[s], s);
  }

  bool isTimeLimitReached = reachedTimeLimit(phaTimeStats, "PHA_TOTAL",
      param.getTIMELIMIT());
  if (param.getParamVal(ParamIndices::USE_SPLIT_SHARE_PARAM_IND) == 1
      && !isTimeLimitReached) {
    // Try split sharing idea
#ifdef TRACE
    printf("\n## Try split sharing idea. ##\n");
#endif
    splitSharingCutGeneration(cutSolver, nonZeroColIndex, structPHA,
        num_cuts_per_cgs, num_total_gics, num_obj_tried, interPtsAndRays,
        castsolver, solnInfo,
        /*rayStore,*/vertexStore, hplaneStore, structSICs,
        isCutSolverPrimalFeasible);
  }

  for (int s = 0; s < solnInfo.numFeasSplits; s++) {
    if (reachedTimeLimit(phaTimeStats, "PHA_TOTAL", param.getTIMELIMIT())) {
      break;
    }
    if (isCutSolverPrimalFeasible[s]) {
#ifdef TRACE
  printf("\n## CglPHA: Trying objectives for cgs %d/%d with name %s. ##\n",
      s + 1, param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND),
      solver->getColName(solnInfo.feasSplitVar[s]).c_str());
#endif
//      const int old_num_cuts = num_total_gics;
//      const int old_num_obj = num_obj_tried;
      tryObjectivesForCgs(cutSolver[s], nonZeroColIndex, structPHA,
          num_cuts_per_cgs[s], num_total_gics, num_obj_tried, solnInfo,
          solver->getNumCols(), true, s,
          solver->getColName(solnInfo.feasSplitVar[s]), interPtsAndRays[s],
          vertexStore, structSICs);
//      if (num_total_gics > old_num_cuts) {
//        GlobalVariables::numCgsActuallyUsed++;
//        GlobalVariables::numCgsLeadingToCuts++;
//      } else if (num_obj_tried > old_num_obj) {
//        GlobalVariables::numCgsActuallyUsed++;
//      }
    }
  }

  isTimeLimitReached = reachedTimeLimit(phaTimeStats, "PHA_TOTAL",
      param.getTIMELIMIT());
  if (param.getParamVal(ParamIndices::USE_SPLIT_SHARE_PARAM_IND) == 2
      && !isTimeLimitReached) {
    // Try split sharing idea
#ifdef TRACE
    printf("\n## Try split sharing idea. ##\n");
#endif
    splitSharingCutGeneration(cutSolver, nonZeroColIndex, structPHA,
        num_cuts_per_cgs, num_total_gics, num_obj_tried, interPtsAndRays,
        castsolver, solnInfo,
        /*rayStore,*/vertexStore, hplaneStore, structSICs,
        isCutSolverPrimalFeasible);
  }

  // Clear things
  for (int s = 0; s < solnInfo.numFeasSplits; s++) {
    if (cutSolver[s]) {
      delete cutSolver[s];
    }
  }
  GlobalVariables::timeStats.end_timer(GEN_MPHA_TIME);
} /* tryObjectives */

/***********************************************************************/
void CglPHA::tryObjectivesForCgs(PointCutsSolverInterface* cutSolver,
    const std::vector<int>& nonZeroColIndex,
    AdvCuts& structPHA, int& num_cuts_per_cgs, int& num_cuts_generated,
    int& num_obj_tried, const SolutionInfo& probData, const int dim,
    const bool inNBSpace, const int cgs_ind, const std::string& cgsName,
    const IntersectionInfo& interPtsAndRays,
    const std::vector<Vertex>& vertexStore, const AdvCuts& structSICs) {
  if (reachedCutLimit(num_cuts_per_cgs, num_cuts_generated)) {
    return;
  }

#ifdef TRACE
  printf("\n## CglPHA: Trying objectives for cgs %d/%d with name %s. ##\n",
      cgs_ind, param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND),
      cgsName.c_str());
#endif

  const double beta = 1.0;
  tightOnPointsRaysCutGeneration(cutSolver, beta, nonZeroColIndex, structPHA,
      num_cuts_per_cgs, num_cuts_generated, num_obj_tried,
      dynamic_cast<PointCutsSolverInterface*>(solver), probData, cgs_ind,
      cgsName, structSICs, GlobalVariables::timeStats,
      GlobalConstants::GEN_MPHA_TIME, inNBSpace);

  if (!inNBSpace) {
    //genCutsFromPointsRays(cutSolver, beta, structPCuts,
    //    num_pcuts_per_split[split_ind], num_cuts_generated, num_obj_tried,
    //    dynamic_cast<LiftGICsSolverInterface*>(solver), probData, interPtsAndRays[split_ind],
    //    probData.feasSplitVar[split_ind], structSICs);
  } else {
    // This is in the * complemented * non-basic space,
    // so the origin is the solution to the LP relaxation
    // Moreover, every cut will have right-hand side 1,
    // since we want to separate the origin
    genCutsFromPointsRaysNB(cutSolver, beta, nonZeroColIndex, structPHA,
        num_cuts_per_cgs, num_cuts_generated, num_obj_tried, castsolver,
        probData, cgs_ind, cgsName, structSICs);
    if (param.paramVal[USE_CUT_VERT_HEUR_PARAM_IND] == 1) {
      cutVerticesCutGeneration(cutSolver, beta, nonZeroColIndex, structPHA,
          num_cuts_per_cgs, num_cuts_generated, num_obj_tried, castsolver,
          probData,
          /*interPtsAndRays,*/vertexStore, cgs_ind, cgsName, structSICs);
    }
  }
//  // Free cutSolver
//  if (cutSolver) {
//    delete cutSolver;
//  }
} /* tryObjectivesForCgs */

/**
 * Calculate depth of points wrt to SIC and objective,
 * and also check whether points are on bd S and on proper hyperplanes
 */
void CglPHA::analyzePointCollection(const std::string cutType,
    const SolutionInfo& solnInfo, const std::vector<Hplane>& hplaneStore,
    const AdvCuts& NBSIC, const AdvCut& objCut,
    std::vector<IntersectionInfo>& interPtsAndRays,
    std::vector<int>& num_rays_parallel_to_split,
    std::vector<double>& minSICDepth, std::vector<double>& maxSICDepth,
    std::vector<double>& avgSICDepth, std::vector<double>& minObjDepth,
    std::vector<double>& maxObjDepth, std::vector<double>& avgObjDepth,
    const int num_activations) const {
//  const int num_activations =
//      param.paramVal[ParamIndices::NUM_EXTRA_ACT_ROUNDS_PARAM_IND];

  // Get norm of objCut
  const double objNorm = objCut.row().twoNorm();

  // Resize vectors
  minSICDepth.resize(solnInfo.numFeasSplits, -1.0);
  maxSICDepth.resize(solnInfo.numFeasSplits, -1.0);
  avgSICDepth.resize(solnInfo.numFeasSplits, 0.0);
  minObjDepth.resize(solnInfo.numFeasSplits, -1.0);
  maxObjDepth.resize(solnInfo.numFeasSplits, -1.0);
  avgObjDepth.resize(solnInfo.numFeasSplits, 0.0);
  num_rays_parallel_to_split.resize(solnInfo.numFeasSplits, 0);
  std::vector<std::vector<int> > num_hplanes_for_split(solnInfo.numFeasSplits),
      num_struct_hplanes_for_split(solnInfo.numFeasSplits);

  // Compute how many of initial rays of C1 are parallel to each split
  for (int split_ind = 0; split_ind < solnInfo.numFeasSplits;
      split_ind++) {
    int splitRow = solnInfo.rowOfVar[solnInfo.feasSplitVar[split_ind]];
    for (int r = 0; r < (int) solnInfo.raysOfC1.size(); r++) {
      if (isZero(solnInfo.raysOfC1[r][splitRow], param.getRAYEPS())) {
        num_rays_parallel_to_split[split_ind]++;
      }
    }
  }

  // Compute relevant statistics about hyperplanes
  for (int split_ind = 0; split_ind < solnInfo.numFeasSplits;
      split_ind++) {
    num_hplanes_for_split[split_ind].resize(num_activations, 0);
    num_struct_hplanes_for_split[split_ind].resize(num_activations, 0);

    for (int act_ind = 0; act_ind < num_activations; act_ind++) {
      num_hplanes_for_split[split_ind][act_ind] =
          (int) interPtsAndRays[split_ind].hplaneIndexByAct[act_ind].size();
      for (int h = 0; h < num_hplanes_for_split[split_ind][act_ind];
          h++) {
        const int curr_hplane_ind =
            interPtsAndRays[split_ind].hplaneIndexByAct[act_ind][h];
        if (hplaneStore[curr_hplane_ind].var >= solnInfo.numCols) {
          num_struct_hplanes_for_split[split_ind][act_ind]++;
        }
      }
    }
  }

  // Measure effectiveness of points selected
#ifdef SHOULD_WRITE_PTSUMMARY
  char filename[300];
  snprintf(filename, sizeof(filename) / sizeof(char), "%s-%sPointSummary.csv",
      GlobalVariables::out_f_name_stub.c_str(), cutType.c_str());
  FILE* ptSummaryOut = fopen(filename, "w");
  fprintf(ptSummaryOut, "Split,splitVal,numPoints,numFinalPoints,"
      "numRays,numParallelRaysOfC1,");
  for (int act_ind = 0; act_ind < num_activations; act_ind++) {
    fprintf(ptSummaryOut, "numHplanesAct%d,", act_ind+1);
    fprintf(ptSummaryOut, "numStructHplanesAct%d,", act_ind+1);
    fprintf(ptSummaryOut, "numRemovedPtsAct%d,", act_ind+1);
    fprintf(ptSummaryOut, "avgRemovedDepthAct%d,", act_ind+1);
    fprintf(ptSummaryOut, "numAddedPtsAct%d,", act_ind+1);
    fprintf(ptSummaryOut, "avgAddedDepthAct%d,", act_ind+1);
    fprintf(ptSummaryOut, "avgDeltaAct%d,", act_ind+1);
  }
  fprintf(ptSummaryOut, "min_SIC_depth,max_SIC_depth,avg_SIC_depth,std_dev_SIC_depth,"
      "min_obj_depth,max_obj_depth,avg_obj_depth,std_dev_obj_depth,"
      "\n");
#endif
  for (int split_ind = 0; split_ind < solnInfo.numFeasSplits;
      split_ind++) {
    // Get the SIC for this split
    const AdvCut* currNBSIC = NBSIC.getCutPointer(split_ind);
    const double SICNorm = (currNBSIC) ? (currNBSIC->row()).twoNorm() : 0.0;
//    const double SICNorm = NBSIC[split_ind].getTwoNorm();

    // For each point generated for this split
    // Measure efficacy, and determine whether it is final
    const int numIntPointsOrRays = interPtsAndRays[split_ind].getNumRows();
//    interPtsAndRays[split_ind].pointRowIndex.clear();
    interPtsAndRays[split_ind].objDepth.clear();
    interPtsAndRays[split_ind].SICDepth.clear();
    interPtsAndRays[split_ind].finalFlag.clear();
    interPtsAndRays[split_ind].numFinalPoints = 0;

//    interPtsAndRays[split_ind].pointRowIndex.reserve(
//        interPtsAndRays[split_ind].numPoints);
    interPtsAndRays[split_ind].objDepth.reserve(numIntPointsOrRays);
    interPtsAndRays[split_ind].SICDepth.reserve(numIntPointsOrRays);
    interPtsAndRays[split_ind].finalFlag.resize(numIntPointsOrRays, false);

    double sum_SIC_depth = 0.0, sumSquares_SIC_depth = 0.0;
    double sum_obj_depth = 0.0, sumSquares_obj_depth = 0.0;

    for (int row_ind = 0; row_ind < numIntPointsOrRays; row_ind++) {
      // If it's a point get it, o/w count the ray
      if (!isZero(interPtsAndRays[split_ind].RHS[row_ind])) {
//        interPtsAndRays[split_ind].numRays++;
//      } else {
//        interPtsAndRays[split_ind].numPoints++;
//        interPtsAndRays[split_ind].pointRowIndex.push_back(row_ind);
        calculatePointStats(minSICDepth[split_ind], maxSICDepth[split_ind],
            sum_SIC_depth, sumSquares_SIC_depth, minObjDepth[split_ind],
            maxObjDepth[split_ind], sum_obj_depth, sumSquares_obj_depth,
            /*split_ind,*/ row_ind, castsolver, solnInfo,
            interPtsAndRays[split_ind], /*hplaneStore,*/ currNBSIC, SICNorm, objCut,
            objNorm);
      } else {
        const CoinShallowPackedVector tmpVector = interPtsAndRays[split_ind].getVector(row_ind);
        // Get depth wrt SIC
        const double SICactivity =
            (currNBSIC) ? currNBSIC->getActivity(tmpVector) : 0.0;
        double curr_SIC_depth =
            (currNBSIC) ? (SICactivity - currNBSIC->rhs()) / SICNorm : 0.0;
        if (!greaterThanVal(curr_SIC_depth, 0.)) {
          if (lessThanVal(curr_SIC_depth, 0.)) {
            error_msg(errorstring,
                "SIC depth cannot be negative, but it is: %s.\n",
                stringValue(curr_SIC_depth, "%.3f").c_str());
            writeErrorToLog(errorstring, GlobalVariables::log_file);
            exit(1);
          }
          curr_SIC_depth = +0.0;
        }
        interPtsAndRays[split_ind].SICDepth.push_back(curr_SIC_depth);

        // Get depth wrt objective
        const double objActivity = objCut.getActivity(tmpVector);
        double curr_obj_depth = (objActivity - objCut.rhs()) / objNorm;
        if (!greaterThanVal(curr_obj_depth, 0.)) {
          if (lessThanVal(curr_obj_depth, 0.)) {
            error_msg(errorstring,
                "Obj depth cannot be negative, but it is: %s.\n",
                stringValue(curr_obj_depth, "%.3f").c_str());
            writeErrorToLog(errorstring, GlobalVariables::log_file);
            exit(1);
          }
          curr_obj_depth = +0.0;
        }
        interPtsAndRays[split_ind].objDepth.push_back(curr_obj_depth);
      }
    } /* iterate over the rows of the matrix */

    // Measure min, max, average, and std_dev
    avgSICDepth[split_ind] = sum_SIC_depth
        / interPtsAndRays[split_ind].numPoints;
    avgObjDepth[split_ind] = sum_obj_depth
        / interPtsAndRays[split_ind].numPoints;

#ifdef SHOULD_WRITE_PTSUMMARY
    double std_dev_SIC_depth = 0.0, std_dev_obj_depth = 0.0;
    std_dev_SIC_depth = std::sqrt(
        sumSquares_SIC_depth / interPtsAndRays[split_ind].numPoints
        - avgSICDepth[split_ind] * avgSICDepth[split_ind]);
    std_dev_obj_depth = std::sqrt(
        sumSquares_obj_depth / interPtsAndRays[split_ind].numPoints
        - avgObjDepth[split_ind] * avgObjDepth[split_ind]);

    const int curr_split = solnInfo.feasSplitVar[split_ind];
    // Print
    // Split, num points, num final points, num rays, num parallel rays of C1,
    // min_SIC_depth, max_SIC_depth, avg_SIC_depth, std_dev_SIC_depth
    // min_obj_depth, max_obj_depth, avg_obj_depth, std_dev_obj_depth
    fprintf(ptSummaryOut, "%d,", curr_split);
    fprintf(ptSummaryOut, "%f,", solver->getColSolution()[curr_split]);
    fprintf(ptSummaryOut, "%d,", interPtsAndRays[split_ind].numPoints);
    fprintf(ptSummaryOut, "%d,", interPtsAndRays[split_ind].numFinalPoints);
    fprintf(ptSummaryOut, "%d,", interPtsAndRays[split_ind].numRays);
    fprintf(ptSummaryOut, "%d,", num_rays_parallel_to_split[split_ind]);
    for (int act_ind = 0; act_ind < num_activations; act_ind++) {
      fprintf(ptSummaryOut, "%d,",
          num_hplanes_for_split[split_ind][act_ind]);
      fprintf(ptSummaryOut, "%d,",
          num_struct_hplanes_for_split[split_ind][act_ind]);
      fprintf(ptSummaryOut, "%d,",
          interPtsAndRays[split_ind].numPointsRemoved[act_ind]);
      fprintf(ptSummaryOut, "%f,",
          interPtsAndRays[split_ind].avgRemovedDepth[act_ind]);
      fprintf(ptSummaryOut, "%d,",
          interPtsAndRays[split_ind].numPointsAdded[act_ind]);
      fprintf(ptSummaryOut, "%f,",
          interPtsAndRays[split_ind].avgAddedDepth[act_ind]);
      fprintf(ptSummaryOut, "%f,",
          interPtsAndRays[split_ind].avgAddedDepth[act_ind]
          - interPtsAndRays[split_ind].avgRemovedDepth[act_ind]);
    }
    fprintf(ptSummaryOut, "%f,", minSICDepth[split_ind]);
    fprintf(ptSummaryOut, "%f,", maxSICDepth[split_ind]);
    fprintf(ptSummaryOut, "%f,", avgSICDepth[split_ind]);
    fprintf(ptSummaryOut, "%f,", std_dev_SIC_depth);
    fprintf(ptSummaryOut, "%f,", minObjDepth[split_ind]);
    fprintf(ptSummaryOut, "%f,", maxObjDepth[split_ind]);
    fprintf(ptSummaryOut, "%f,", avgObjDepth[split_ind]);
    fprintf(ptSummaryOut, "%f,", std_dev_obj_depth);
    fprintf(ptSummaryOut, "\n");
#endif
  }
#ifdef SHOULD_WRITE_PTSUMMARY
  fclose(ptSummaryOut);
#endif
}

/***********************************************************************/
void CglPHA::generateSICs(const OsiSolverInterface* si, AdvCuts& NBSICs, AdvCuts& structSICs, SolutionInfo& solnInfo) {
  // Only feasible ones are returned
  CglSIC SICGen(param);
  SICGen.generateCuts(*si, NBSICs, structSICs, solnInfo);
#ifdef TRACE
  printf("***** SICs generated: %d.\n", structSICs.sizeCuts());
#endif
}

/***********************************************************************/
void CglPHA::setParam(const CglGICParam &source) {
  param = source;
} /* setParam */

/*********************************************************************/
// Returns true if needs optimal basis to do cuts
bool CglPHA::needsOptimalBasis() const {
  return true;
}

/*********************************************************************/
// Create C++ lines to get to current state
std::string CglPHA::generateCpp(FILE * fp) {
  return "CglGIC";
}

/**********************************************************/
void CglPHA::printvecINT(const char *vecstr, const int *x, int n) const {
  int num, fromto, upto;

  num = (n / 10) + 1;
  printf("%s :\n", vecstr);
  for (int j = 0; j < num; ++j) {
    fromto = 10 * j;
    upto = 10 * (j + 1);
    if (n <= upto)
      upto = n;
    for (int i = fromto; i < upto; ++i)
      printf(" %4d", x[i]);
    printf("\n");
  }
  printf("\n");
} /* printvecINT */

/**********************************************************/
void CglPHA::printvecDBL(const char *vecstr, const double *x, int n) const {
  int num, fromto, upto;

  num = (n / 10) + 1;
  printf("%s :\n", vecstr);
  for (int j = 0; j < num; ++j) {
    fromto = 10 * j;
    upto = 10 * (j + 1);
    if (n <= upto)
      upto = n;
    for (int i = fromto; i < upto; ++i)
      printf(" %7.3f", x[i]);
    printf("\n");
  }
  printf("\n");
} /* printvecDBL */

/**********************************************************/
void CglPHA::printvecDBL(const char *vecstr, const double *elem,
    const int * index, int nz) const {
  printf("%s\n", vecstr);
  int written = 0;
  for (int j = 0; j < nz; ++j) {
    written += printf("%d:%.3f ", index[j], elem[j]);
    if (written > 70) {
      printf("\n");
      written = 0;
    }
  }
  if (written > 0) {
    printf("\n");
  }

} /* printvecDBL */
