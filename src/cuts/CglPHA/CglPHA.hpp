#pragma once

#include "CglGICParam.hpp"
#include "CglCutGenerator.hpp"
#include "CoinWarmStartBasis.hpp"
#include "CoinFactorization.hpp"
#include "hplaneActivation.hpp"

// (the AdvCut and AdvCuts classes could probably be removed eventually,
// but they have some useful features wrt OsiCuts)
#include "AdvCut.hpp"

/* Debug output */
//#define NEWGEN_TRACE

/* Debug output: print optimal tableau */
//#define NEWGEN_TRACETAB

class CglPHA : public CglCutGenerator {

  friend void CglPHAUnitTest(const OsiSolverInterface * siP,
				const std::string mpdDir);
public:

  /**@name generateCuts */
  //@{
  /** Generate cuts for the model of the solver
      interface si.

      Insert the generated cuts into OsiCuts cs.
  */
  virtual void generateCuts(const OsiSolverInterface & si, OsiCuts & cs,
      const SolutionInfo& probData,
      const AdvCuts& structMSICs, const AdvCuts& NBMSICs,
      const CglTreeInfo info = CglTreeInfo());

  virtual void generateCuts(const OsiSolverInterface & si, OsiCuts & cs,
			    const CglTreeInfo info = CglTreeInfo());

  /// For compatibility with CglCutGenerator (const method)
  virtual void generateCuts(const OsiSolverInterface & si, OsiCuts & cs,
			    const CglTreeInfo info = CglTreeInfo()) const;

  /// Return true if needs optimal basis to do cuts (will return true)
  virtual bool needsOptimalBasis() const;
  //@}
  
  /**@name Public Methods */
  //@{

  // Set the parameters to the values of the given CglGMIParam object.
  void setParam(const CglGICParam &source); 
  // Return the CglGMIParam object of the generator. 
  inline CglGICParam getParam() const {return param;}
  inline int getNumObjTried() const {return num_obj_tried;}
  inline const OsiCuts getStructSICs() const {return structSICs;}

  //@}

  /**@name Constructors and destructors */
  //@{
  /// Default constructor 
  CglPHA();

  // Constructor with specified parameters
  CglPHA(const CglGICParam&);

  /// Copy constructor 
  CglPHA(const CglPHA &);

  /// Clone
  virtual CglCutGenerator * clone() const;

  /// Assignment operator 
  CglPHA & operator=(const CglPHA& rhs);
  
  /// Destructor 
  virtual ~CglPHA();
  /// Create C++ lines to get to current state
  virtual std::string generateCpp( FILE * fp);

  //@}
    
private:
  
  // Private member methods

/**@name Private member methods */

  //@{
  // Method generating the cuts after all CglGIC members are properly set.
  void generateCuts(AdvCuts &structMGICs, const SolutionInfo& solnInfoMain,
      const AdvCuts &structMSICs, const AdvCuts &NBMSICs);
  void generateSICs(const OsiSolverInterface* si, AdvCuts& NBSICs, AdvCuts& structSICs,
      SolutionInfo& solnInfo);

//  void PHAMaster(AdvCuts &structPHA, const int num_rays_to_be_cut,
//      std::vector<IntersectionInfo>& interPtsAndRays,
//      const SolutionInfo& solnInfo, const AdvCuts& structSICs,
//      const AdvCuts& NBSICs, std::vector<std::vector<bool> >& pointIsFinal,
//      std::vector<Vertex>& vertexStore, std::vector<Ray>& rayStore,
//      std::vector<Hplane>& hplaneStore,
//      const std::vector<std::vector<double> >& distToSplitAlongRay);

  void PHAHelper(const int indexOfFirstRayToCut, const int numRaysToBeCut,
      const SolutionInfo& solnInfo, const AdvCuts& structSICs,
      const AdvCuts &NBSICs, const AdvCut& NBObj, const double objNorm,
      const std::vector<std::vector<double> >& distToSplitAlongRay,
      std::vector<std::vector<bool> >& pointIsFinal,
      std::vector<Hplane>& hplaneStore, std::vector<Vertex>& vertexStore,
      std::vector<Ray>& rayStore, const std::vector<int>& sortIndex,
      std::vector<int>& allRayIndices,
//      std::vector<std::vector<int> > &allRaysCutAcrossSplits,
      std::vector<int> &allRaysCutAcrossSplits,
//      std::vector<std::vector<std::vector<int> > > &raysCutForSplit,
      std::vector<std::vector<int> > &raysCutForSplit,
//      std::vector<std::vector<int> > &strongVertHplaneIndex,
      std::vector<IntersectionInfo>& interPtsAndRays, const int act_ind,
      const int total_num_act, const bool isExtraAct,
      const PHAActivationOption option, AdvCuts& structPHA,
      std::vector<int>& num_gics_per_split, int& num_total_pha, Stats& phaTimeStats);

//  void chooseHplanesPHAOne(const int indexOfFirstRayToCut,
//      const int numRaysToBeCut, const LiftGICsSolverInterface* const solver,
//      const SolutionInfo& solnInfo, const AdvCuts& NBSIC,
//      const std::vector<std::vector<double> >& distToSplitAlongRay,
//      std::vector<std::vector<bool> >& pointIsFinal,
//      std::vector<Hplane>& hplaneStore, std::vector<Vertex>& vertexStore,
//      std::vector<Ray>& rayStore, // Changes when we are computing first hplane
//      const std::vector<int>& sortIndex, const std::vector<int>& allRayIndices,
//      std::vector<std::vector<int> >& allRaysCut,
//      std::vector<std::vector<std::vector<int> > >& raysToBeCut,
//      std::vector<std::vector<int> > &strongVertHplaneIndex,
//      std::vector<IntersectionInfo>& interPtsAndRays, const int act_ind,
//      const int total_num_act, const PHAActivationOption& option);
//  bool chooseHplane(std::vector<int>& splitsAffByHplane,
//      const std::vector<int>& allRayIndices, const int originatingVertexIndex,
//      const Ray& rayOut, const int ray_ind,
//      const LiftGICsSolverInterface* const solver, const SolutionInfo& solnInfo,
//      std::vector<Hplane>& hplaneStore, std::vector<Vertex>& vertexStore,
//      std::vector<Ray>& rayStore, const AdvCuts& NBSIC,
//      const std::vector<std::vector<double> >& distToSplitAlongRay,
//      std::vector<int>& allRaysCutAcrossSplits,
//      //    std::vector<std::vector<int> >& allRaysCutAcrossSplits,
//      std::vector<std::vector<int> >& raysCutForSplit,
////      std::vector<std::vector<std::vector<int> > >& raysCutForSplit,
////      std::vector<std::vector<int> > &strongVertHplaneIndex,
//      std::vector<IntersectionInfo>& interPtsAndRays, const int act_ind,
//      const bool selectByDist, const bool selectByDepth,
//      const bool selectByFinalPoints, const bool selectByMostRemoved,
//      const bool isExtraAct);
  bool chooseHplaneByRay(std::vector<int>& splitsAffByHplane,
      const std::vector<int>& allRayIndices, const int originatingVertexIndex,
      const Ray& rayOut, const int ray_ind,
      const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
      std::vector<Hplane>& hplaneStore, std::vector<Vertex>& vertexStore,
      std::vector<Ray>& rayStore, const AdvCuts& NBSIC,
      const std::vector<std::vector<double> >& distToSplitAlongRay,
      std::vector<int>& allRaysCutAcrossSplits,
      //    std::vector<std::vector<int> >& allRaysCutAcrossSplits,
      std::vector<std::vector<int> >& raysCutForSplit,
      //      std::vector<std::vector<std::vector<int> > >& raysCutForSplit,
      //      std::vector<std::vector<int> > &strongVertHplaneIndex,
      std::vector<IntersectionInfo>& interPtsAndRays, const int act_ind,
      const bool selectByDist, const bool selectByDepth,
      const bool selectByFinalPoints);
  bool chooseHplaneBySplit(std::vector<int>& splitsAffByHplane,
      const std::vector<int>& allRayIndices, const int originatingVertexIndex,
      const int numRaysToCut, const std::vector<int>& sortIndex,
      const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
      std::vector<Hplane>& hplaneStore, std::vector<Vertex>& vertexStore,
      std::vector<Ray>& rayStore, const AdvCuts& NBSIC,
      const std::vector<std::vector<double> >& distToSplitAlongRay,
      std::vector<int>& allRaysCutAcrossSplits,
      //    std::vector<std::vector<int> >& allRaysCutAcrossSplits,
      //    std::vector<std::vector<std::vector<int> > >& raysCutForSplit,
      std::vector<std::vector<int> >& raysCutForSplit,
      //    std::vector<std::vector<int> > &strongVertHplaneIndex,
      std::vector<IntersectionInfo>& interPtsAndRays, const int act_ind,
      const bool selectByFinalPoints, const bool selectByMostRemoved);
  bool chooseHplaneHelper(const int split_ind, const double distToSplit,
      const bool isVertexOnSplit, const int ray_ind,
      std::vector<Hplane>& hplane, const Hplane& tmpHplane,
      const double tmpDistToHplane, double& minDistOverall,
      double& distToHplaneChosenForSplit,
      double& addedDepthByHplaneChosenForSplit,
      int& numFinalPointsByHplaneChosenForSplit,
      int& numPointsCutByHplaneChosenForSplit,
      //    std::vector<double>& distToHplaneChosenForSplit,
      //    std::vector<double>& addedDepthByHplaneChosenForSplit,
      //    std::vector<int>& numFinalPointsByHplaneChosenForSplit,
      //    std::vector<int>& numPointsCutByHplane,
      const Vertex& vertCreated, double& addedDepthForThisHplane,
      int& numFinalPointsThisHplane, int& numPointsCutThisHplane,
      //    std::vector<double>& addedDepthForThisHplane,
      //    std::vector<int>& numFinalPointsThisHplane,
      const std::vector<int>& allRayIndices,
      const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
      const IntersectionInfo& interPtsAndRays, std::vector<Vertex>& vertexStore,
      std::vector<Ray>& rayStore, const AdvCuts& NBSIC,
      std::vector<std::vector<int> >& raysCutForSplit, const bool selectByDist,
      const bool selectByDepth, const bool selectByFinalPoints,
      const bool calcPointsRemoved, const bool isNBBound,
      const bool chooseByRay);
//  bool chooseHplaneHelper(const int split_ind, const double distToSplit,
//      const int ray_ind, std::vector<Hplane>& hplane, const Hplane& tmpHplane,
//      const double tmpDistToHplane, double& minDistOverall,
//      std::vector<double>& distToHplaneChosenForSplit,
//      std::vector<double>& addedDepthByHplaneChosenForSplit,
//      std::vector<int>& numFinalPointsByHplaneChosenForSplit,
//      const Vertex& vertCreated, std::vector<double>& addedDepthForThisHplane,
//      std::vector<int>& numFinalPointsThisHplane,
//      const std::vector<int>& allRayIndices,
//      const LiftGICsSolverInterface* const solver, const SolutionInfo& solnInfo,
//      std::vector<Vertex>& vertexStore, std::vector<Ray>& rayStore,
//      const AdvCuts& NBSIC, std::vector<std::vector<int> >& raysCutForSplit,
//      const bool selectByDist, const bool selectByDepth,
//      const bool selectByFinalPoints, const bool isNBBound,
//      const bool chooseByRay = true);
//  bool chooseHplane(Hplane& hplane, std::vector<int>& splitsAffByHplane,
//      const std::vector<int>& allRayIndices, Vertex& newVertex,
//      const int originatingVertexIndex, const Ray& rayOut, const int ray_ind,
//      const LiftGICsSolverInterface* const solver, const SolutionInfo& solnInfo,
//      const std::vector<Vertex>& vertexStore, const std::vector<Ray>& rayStore,
//      const AdvCuts& NBSIC,
//      const std::vector<std::vector<double> >& distToSplitAlongRay,
//      const std::vector<int>& raysPrevCut, const bool selectByDist,
//      const bool selectByDepth, const bool selectByFinalPoints);
  void fakeActivateHplane(int& numFinalPointsAdded, const bool calcFinalPoints,
      double& avgAddedDepth, const bool calcDepth, const double distToHplane,
      const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
      const int split_ind, const std::vector<Vertex>& vertexStore,
      const Vertex& vertCreated, const std::vector<Ray>& rayStore,
      const int ray_ind, const Hplane& hplane, const AdvCut& NBSIC,
      const std::vector<int>& allRayIndices, IntersectionInfo& interPtsAndRays,
      const IntersectionInfo& oldInterPtsAndRays,
      const std::vector<int>& raysPrevCut);

  void pointsFromRayViolatingHplane(int& numPointsCutByHplane,
      //std::vector<int>& numPointsCutByHplane,
      std::vector<int>& pointsToRemove,
  //    std::vector<std::vector<int> >& pointsToRemove,
      const int rayInd,
      const std::vector<Vertex>& vertexStore, const std::vector<Ray>& rayStore,
      const Hplane& hplane, const IntersectionInfo& interPtsAndRays,
      const int splitsAffByHplane,
  //    const std::vector<IntersectionInfo>& interPtsAndRays,
  //    const std::vector<int> & splitsAffByHplane,
      const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo);

//  void addInterPtsFromUncutRaysOfC1(IntersectionInfo& interPtsAndRays,
//      const SolutionInfo& solnInfo, const int split_ind,
//      const std::vector<Ray>& rayStore, const std::vector<Vertex>& vertexStore);
//  void addInterPtsFromUncutRaysOfC1(IntersectionInfo& interPtsAndRays,
//      std::vector<double>& distToSplitAlongRay, const SolutionInfo& solnInfo,
//      const int split_ind, const std::vector<Ray>& rayStore,
//      const std::vector<Vertex>& vertexStore);
  void addInterPtFromUncutRayOfC1(IntersectionInfo& interPtsAndRays,
      double& distToSplitAlongRay, const SolutionInfo& solnInfo,
      const int split_ind, const std::vector<Ray>& rayStore, const int ray_ind,
      const std::vector<Vertex>& vertexStore, const bool recalc_dist = true);

  void performActivations(const SolutionInfo& solnInfo,
      const std::vector<Vertex>& vertexStore, std::vector<Ray>& rayStore,
//      const std::vector<int> &allRaysCut,
//      const std::vector<std::vector<int> > &raysToBeCut,
      const std::vector<Hplane>& hplaneStore,
      std::vector<IntersectionInfo>& interPtsAndRays, const AdvCuts& NBSIC,
      const AdvCut& objCut, const double objNorm, const int act_ind);

  void tryObjectives(AdvCuts& structPHA, std::vector<int>& num_cuts_per_cgs,
      int& num_total_gics, int& num_obj_tried, const SolutionInfo& solnInfo,
      const int dim, const bool inNBSpace,
      //    const int cgs_ind, const std::string& cgsName,
      const std::vector<IntersectionInfo>& interPtsAndRays,
      const std::vector<Ray>& rayStore,
      const std::vector<Vertex>& vertexStore,
      const std::vector<Hplane>& hplaneStore, const AdvCuts& structSICs,
      Stats& phaTimeStats);
  void tryObjectivesForCgs(PointCutsSolverInterface* cutSolver,
      const std::vector<int>& nonZeroColIndex, AdvCuts& structPHA,
      int& num_cuts_per_cgs, int& num_cuts_generated, int& num_obj_tried,
      const SolutionInfo& probData, const int dim, const bool inNBSpace,
      const int cgs_ind, const std::string& cgsName,
      const IntersectionInfo& interPtsAndRays,
      const std::vector<Vertex>& vertexStore, const AdvCuts& structSICs);

  void analyzePointCollection(const std::string cutType,
      const SolutionInfo& solnInfo, const std::vector<Hplane>& hplaneStore,
      const AdvCuts& NBSIC, const AdvCut& objCut,
      std::vector<IntersectionInfo>& interPtsAndRays,
      std::vector<int>& num_rays_parallel_to_split,
      std::vector<double>& minSICDepth, std::vector<double>& maxSICDepth,
      std::vector<double>& avgSICDepth, std::vector<double>& minObjDepth,
      std::vector<double>& maxObjDepth, std::vector<double>& avgObjDepth,
      const int num_activations) const;

  inline bool tieBreakingForChooseHplane(const bool selectByDist,
      const bool selectByDepth, const bool selectByFinalPoints,
      const double distToHplaneChosenForSplit,
      const double addedDepthByHplaneChosenForSplit,
      const int numFinalPointsByHplaneChosenForSplit,
      const double tmpDistToHplane,
      const double& addedDepthForThisHplane,
      const int numFinalPointsThisHplane) const {
    bool tieBreaking = false;
    if (selectByDist && isVal(tmpDistToHplane, distToHplaneChosenForSplit)) {
      tieBreaking = greaterThanVal(addedDepthForThisHplane,
          addedDepthByHplaneChosenForSplit)
          || (numFinalPointsThisHplane > numFinalPointsByHplaneChosenForSplit);
    } else if (selectByDepth
        && isVal(addedDepthForThisHplane, addedDepthByHplaneChosenForSplit)) {
      tieBreaking = lessThanVal(tmpDistToHplane, distToHplaneChosenForSplit)
          || (numFinalPointsThisHplane > numFinalPointsByHplaneChosenForSplit);
    } else if (selectByFinalPoints
        && (numFinalPointsThisHplane == numFinalPointsByHplaneChosenForSplit)) {
      tieBreaking = lessThanVal(tmpDistToHplane, distToHplaneChosenForSplit)
          || greaterThanVal(addedDepthForThisHplane,
              addedDepthByHplaneChosenForSplit);
    }
    return tieBreaking;
  }
  inline bool tieBreakingForChooseHplane(const bool selectByDist,
      const bool selectByDepth, const bool selectByFinalPoints,
      const int split_ind,
      const std::vector<double>& distToHplaneChosenForSplit,
      const std::vector<double>& addedDepthByHplaneChosenForSplit,
      const std::vector<int>& numFinalPointsByHplaneChosenForSplit,
      const double tmpDistToHplane,
      const std::vector<double>& addedDepthForThisHplane,
      const std::vector<int>& numFinalPointsThisHplane) const {
    bool tieBreaking = false;
    if (selectByDist
        && isVal(tmpDistToHplane, distToHplaneChosenForSplit[split_ind])) {
      tieBreaking = greaterThanVal(addedDepthForThisHplane[split_ind],
          addedDepthByHplaneChosenForSplit[split_ind])
          || (numFinalPointsThisHplane[split_ind]
              > numFinalPointsByHplaneChosenForSplit[split_ind]);
    } else if (selectByDepth
        && isVal(addedDepthForThisHplane[split_ind],
            addedDepthByHplaneChosenForSplit[split_ind])) {
      tieBreaking = lessThanVal(tmpDistToHplane,
          distToHplaneChosenForSplit[split_ind])
          || (numFinalPointsThisHplane[split_ind]
              > numFinalPointsByHplaneChosenForSplit[split_ind]);
    } else if (selectByFinalPoints
        && (numFinalPointsThisHplane[split_ind]
            == numFinalPointsByHplaneChosenForSplit[split_ind])) {
      tieBreaking = lessThanVal(tmpDistToHplane,
          distToHplaneChosenForSplit[split_ind])
          || greaterThanVal(addedDepthForThisHplane[split_ind],
              addedDepthByHplaneChosenForSplit[split_ind]);
    }
    return tieBreaking;
  }

  /// print a vector of integers
  void printvecINT(const char *vecstr, const int *x, int n) const;
  /// print a vector of doubles: dense form
  void printvecDBL(const char *vecstr, const double *x, int n) const;
  /// print a vector of doubles: sparse form
  void printvecDBL(const char *vecstr, const double *elem, const int * index, 
		   int nz) const;

  //@}
  
  // Private member data

/**@name Private member data */

  //@{

  /// Object with CglGICParam members. 
  CglGICParam param;
  
  /// Pointer on solver. Reset by each call to generateCuts().
  OsiSolverInterface *solver;

  /// Pointer to cast verson of solver
  PointCutsSolverInterface* castsolver; // Currently has to be Clp because of some methods checking Clp status

  /// Pointer on point to separate. Reset by each call to generateCuts().
  const double *xlp;

  /// Pointer on matrix of coefficient ordered by rows. 
  /// Reset by each call to generateCuts().
  const CoinPackedMatrix *byRow;

  /// Pointer on matrix of coefficient ordered by columns. 
  /// Reset by each call to generateCuts().
  const CoinPackedMatrix *byCol;

  // AdvCuts versions of cuts we generate
  AdvCuts nb_advcs, struct_advcs;

  // If SICs are not provided, assume there are none
  AdvCuts structSICs, NBSICs;

  // Number of objectives tried
  int num_obj_tried = 0;

  //@}
};

//#############################################################################
/** A function that tests the methods in the CglGIC class. The
    only reason for it not to be a member method is that this way it doesn't
    have to be compiled into the library. And that's a gain, because the
    library should be compiled with optimization on, but this method should be
    compiled with debugging. */
void CglPHAUnitTest(const OsiSolverInterface * siP,
			 const std::string mpdDir );
