//============================================================================
// Name        : hplaneActivation.hpp
// Author      : Aleksandr M. Kazachkov
// Version     : 0.2014.05.23
// Copyright   : Your copyright notice
// Description : Methods to activate hyperplanes
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#pragma once

#include "Hplane.hpp"
#include "Vertex.hpp"
#include "Ray.hpp"
#include "SolutionInfo.hpp"
#include "IntersectionInfo.hpp"
#include "AdvCut.hpp"
#include "typedefs.hpp"


/**
 * @return Number of final points that would be created by activating this hplane
 */
int fakeActivateFinalPoints(//const double distToHplane,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    const int split_ind,
    const std::vector<Vertex>& vertexStore, const Vertex& vertCreated,
    const std::vector<Ray>& rayStore, const int ray_ind,
    const Hplane& hplane,
//    const std::vector<int>& allRayIndices,
    const IntersectionInfo* interPtsAndRaysOld,
    const std::vector<int>& raysPrevCut);

/**
 * The ray ray_ind is being cut, creating the new vertex tmpVertex
 */
void updateHplaneStore(std::vector<std::vector<int> >& strongVertHplaneIndex,
    std::vector<IntersectionInfo>& interPtsAndRays,
    std::vector<Vertex>& vertexStore, const int init_vert_ind,
    std::vector<Ray>& rayStore, std::vector<int>& allRaysCut,
    std::vector<std::vector<int> >& raysToBeCut,
    std::vector<Hplane>& hplaneStore,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    const Vertex& newVertex, const Hplane& tmpHplane,
    const std::vector<int>& splitsAffByHplane, const int ray_ind,
    const int act_ind);
/**
 * New version, 06/07/2016
 * The ray ray_ind is being cut, creating the new vertex tmpVertex
 */
void updateHplaneStore(std::vector<IntersectionInfo>& interPtsAndRays,
    std::vector<Vertex>& vertexStore, const int init_vert_ind,
    std::vector<Ray>& rayStore,
//    std::vector<std::vector<int> >& allRaysCut,
    std::vector<int>& allRaysCutAcrossSplits,
//    std::vector<std::vector<std::vector<int> > >& raysToBeCut,
    std::vector<std::vector<int> >& raysCutForSplit,
    std::vector<Hplane>& hplaneStore,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    const Vertex& newVertex, const Hplane& tmpHplane,
    const std::vector<int>& splitsAffByHplane, const int ray_ind,
    const int act_ind, const bool checkIfRayPrevCut = false);

/**
 * @brief Adds a new vertex that has not been calculated yet
 *
 * @param check_split :: If true, ensures distToSplit > distToHplane
 */
void newVertexHelper(std::vector<Vertex>& vertexStore, const int init_vert_ind,
    const std::vector<Ray>& rayStore, std::vector<Hplane>& hplaneStore,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    const int ray_ind,
    const int split_ind, const int hplane_ind, const bool check_split =
        false);

/**
 * @brief Adds a new vertex
 */
void newVertexHelper(std::vector<Vertex>& vertexStore, const int init_vert_ind,
    std::vector<Hplane>& hplaneStore, const Vertex& newVertex,
    const int ray_ind, const int split_ind, const int hplane_ind);

bool findHplaneToIntersectRayFirst(double& minDist, Hplane& hplane,
    std::vector<int>& splitsAffByHplane, Vertex& newVertex,
    const Vertex& vertex, const Ray& rayOut,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo);

void addRaysIntByHplaneBeforeBdS(std::vector<int>& vertOfRayIndicesIntByHplane,
    std::vector<int>& rayIndicesIntByHplane,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    const Hplane& hplane,
    const std::vector<Vertex>& vertexStore, const int init_vert_ind,
    const std::vector<Ray>& rayStore, const int split_ind);

/**
 * Chooses one hyperplane to activate per split, based on the most number of previously generated points it cuts
 */
void chooseNewHplanesPointsRemoved(std::vector<Hplane>& hplaneStore,
    std::vector<int>& allRaysCutAct2,
    std::vector<std::vector<int> >& raysToBeCutAct2,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    std::vector<Vertex>& vertexStore, // will be modified to add new vertices from activation
    const std::vector<Ray>& rayStore,
    std::vector<IntersectionInfo>& interPtsAndRays);

/**
 * Finds number of points that would be removed by activating this hplane on this split
 */
void newHplaneHelperPointsRemoved(std::vector<int>& numPointsCutByHplane,
    const Hplane& hplane, std::vector<std::vector<int> >& tmpRaysToBeCut,
    std::vector<std::vector<int> >& tmpRaysCutByHplane,
    std::vector<std::vector<int> >& tmpAllRaysIntersected,
//    std::vector<double>& tmpDistToHplane,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    const std::vector<Vertex>& vertexStore,
    const std::vector<Ray>& rayStore,
    const std::vector<Hplane>& hplaneStore, const int oldHplaneIndex,
    const std::vector<IntersectionInfo>& interPtsAndRays,
    const int nb_ray_ind = -1);

///**
// * Add one hyperplane per split, where the hyperplane is chosen such that the most number of final points are added
// */
//void chooseNewHplanesFinalPoints(std::vector<Hplane>& hplaneStore,
//    std::vector<std::vector<std::vector<int> > >& actHplaneIndexForSplit,
//    std::vector<std::vector<int> >& allRaysCut,
//    std::vector<std::vector<std::vector<int> > >& raysToBeCut,
//    const LiftGICsSolverInterface* const solver, const SolutionInfo& solnInfo,
//    const chooseCutGeneratingSets& chooseSplits,
//    std::vector<Vertex>& vertexStore, // will be modified to add new vertices from activation
//    const std::vector<Ray>& rayStore,
//    const std::vector<IntersectionInfo>& interPtsAndRays,
//    const AdvCuts& NBSIC, const int act_ind);

/**
 * Add one hyperplane per split, where the hyperplane is chosen such that the most number of final points are added
 */
void chooseNewHplanesFinalPoints(std::vector<Hplane>& hplaneStore,
    std::vector<std::vector<int> >& allRaysCut,
    std::vector<std::vector<std::vector<int> > >& raysToBeCut,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    std::vector<Vertex>& vertexStore, // will be modified to add new vertices from activation
    const std::vector<Ray>& rayStore,
    std::vector<IntersectionInfo>& interPtsAndRays, //const AdvCuts& NBSIC,
    const int act_ind);

/**
 * Finds number of points that would be removed by activating this hplane on this split
 */
void newHplaneHelperFinalPoints(std::vector<int>& numFinalPointsCreatedByHplane,
    std::vector<int>& numPointsCutByHplane, const Hplane& hplane,
    std::vector<std::vector<int> >& tmpRaysToBeCut,
    std::vector<std::vector<int> >& tmpRaysCutByHplane,
    std::vector<std::vector<int> >& tmpAllRaysIntersected,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    const std::vector<Vertex>& vertexStore,
    const std::vector<Ray>& rayStore,
    const std::vector<Hplane>& hplaneStore, const int oldHplaneIndex,
    const std::vector<IntersectionInfo>& interPtsAndRays,
//    const std::vector<int>& allRayIndices, const AdvCuts& NBSIC,
    const int nb_ray_ind = -1);

/**
 * Finds points created from rayInd that are cut by this hyperplane
 */
void pointsFromRayViolatingHplane(std::vector<int>& numPointsCutByHplane,
    std::vector<std::vector<int> >& pointsToRemove, const int rayInd,
//    const std::vector<Vertex>& vertexStore,
//    const std::vector<Ray>& rayStore,
    const Hplane& hplane,
    const std::vector<IntersectionInfo>& interPtsAndRays,
    const std::vector<int> & splitsAffByHplane,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo);

/**
 * Calculates numRaysCutByHplane and numAllRaysInt
 * @param hplane
 * @param act_ind :: 0 for first activation, etc.
 * @param split_ind :: split to calculate for
 */
void updateHplaneInfoAfterActivation(Hplane& hplane, const int act_ind,
    const int split_ind);

/**
 * Calculates numRaysCutByHplane
 * @param hplane
 * @param act_ind :: 0 for first activation, etc.
 */
void updateHplaneInfoAfterActivation(Hplane& hplane, const int act_ind);

/**
 * Calculates numRaysCutByHplane
 * @param hplaneStore
 * @param act_ind :: 0 for first activation, etc.
 */
void updateHplaneInfoAfterActivation(std::vector<Hplane>& hplaneStore,
    const int act_ind);

int addVertex(std::vector<Vertex>& vertexStore, const Vertex& tmpVertex);
//    const int newHplaneIndex

void activateHplane(int& totalNumPointsRemoved, double& avgRemovedDepth,
    int& totalNumPointsAdded, double& avgAddedDepth,
    IntersectionInfo& interPtsAndRays, const int split_ind,
    const int hplane_ind, const int hplane_ind_in_split,
    const std::vector<Hplane>& hplaneStore, const Hplane& hplane,
    std::vector<Ray>& rayStore, const std::vector<Vertex>& vertexStore,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    const AdvCut* NBSIC, const double norm, const AdvCut& objCut,
    const double objNorm, const int act_ind);

int removePtsAndRaysCutByHplane(const PointCutsSolverInterface* const solver,
    const SolutionInfo &solnInfo, const std::vector<bool> &raysCutFlag,
    const int split_var, const Hplane& hplane, const int hplane_ind_in_split,
//    const int hplane_ind, const std::vector<Hplane>& hplaneStore,
    const std::vector<Vertex>& vertexStore, const std::vector<Ray>& rayStore,
    IntersectionInfo& interPtsAndRays, const AdvCut* NBSIC, const double norm,
    double& sumRemovedDepth, const bool fake_act = false);

bool pointIsCutByHplane(const PointCutsSolverInterface* const solver,
    const SolutionInfo &solnInfo, const CoinPackedVector& row,
    const double RHS, const Hplane& hplane);

void printActivatedHplaneDetails(FILE* fptr,
    const SolutionInfo& solnInfo,
    const std::vector<Hplane>& hplaneStore,
    const std::vector<IntersectionInfo>& interPtsAndRays,
    const int act_ind = 0);
