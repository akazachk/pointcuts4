/*
 * hplaneActivation.cpp
 *
 *  Created: May 25, 2014
 *  Edited: November 30, 2015
 *  Author: akazachk (based on snadarajah work)
 */

#include <hplaneActivation.hpp>
#include "GlobalConstants.hpp"
#include "Utility.hpp"

/**
 * @return Number of final points that would be created by activating this hplane
 */
int fakeActivateFinalPoints(//const double distToHplane,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    const int split_ind,
    const std::vector<Vertex>& vertexStore, const Vertex& vertCreated,
    const std::vector<Ray>& rayStore, const int ray_ind,
    const Hplane& hplane, 
//    const std::vector<int>& allRayIndices,
    const IntersectionInfo* interPtsAndRaysOld,
    const std::vector<int>& raysPrevCut) {
//  int totalNumPointsRemoved = 0, totalNumPointsAdded = 0;
//  double avgRemovedDepth = 0.0, avgAddedDepth = 0.0;
  int numFinalPointsAdded = 0;

  const std::vector<int> raysToBeCutByHplane(1, ray_ind);
  std::vector<int> vertOfAllRaysIntByHplane, allRaysIntByHplane;

  IntersectionInfo interPtsAndRays;
  if (interPtsAndRaysOld != NULL) {
    interPtsAndRays = *interPtsAndRaysOld; // TODO check if this actually works
  } else {
    interPtsAndRays.reverseOrdering();
    interPtsAndRays.setDimensions(0, solnInfo.numNB);
  }

  // Get all rays cut by this hplane
  addRaysIntByHplaneBeforeBdS(vertOfAllRaysIntByHplane, allRaysIntByHplane,
      solver, solnInfo, hplane, vertexStore, 0, rayStore, split_ind);

  // Rays cut: ray_ind + all rays from allRaysIntByHplane that have previously been cut
  std::vector<bool> raysCutFlag(solnInfo.numNB, false);
  std::vector<bool> rayIntersectedFlag(solnInfo.numNB, false);

  raysCutFlag[ray_ind] = true;
  const int numRaysIntByHplane = allRaysIntByHplane.size();
  for (int r = 0; r < numRaysIntByHplane; r++) {
    rayIntersectedFlag[allRaysIntByHplane[r]] = true;
  }
  for (int r = 0; r < (int) raysPrevCut.size(); r++) {
    if (rayIntersectedFlag[raysPrevCut[r]])
      raysCutFlag[raysPrevCut[r]] = true;
  }

  const int split_var = solnInfo.feasSplitVar[split_ind];
//  numNewPointsRemoved += removePtsAndRaysCutByHplane(solver, solnInfo,
//          raysCutFlag, split_var, hplane, vertexStore, rayStore, interPtsAndRays,
//          &NBSIC, norm, sumRemovedDepth, true);

  // Create the set J \setminus K
  std::vector<int> newRayColIndices;
  storeNewRayNBIndices(solnInfo.numNB, newRayColIndices, allRaysIntByHplane,
      raysCutFlag);
  const int numNewRays = newRayColIndices.size();

  //For ray intersected by hyperplane H generate new rays corresponding to each element in J \setminus K
  //This is of course available in closed form from the tableau
  for (int r = 0; r < numNewRays; r++) {
    Ray newRay;
    // TODO I think the Point class is not being used actually anywhere, change this to CoinPackedVector
    Point tmpPoint;
    calcNewRayCoordinatesRank1(newRay, /*solver,*/ solnInfo, ray_ind,
        newRayColIndices[r], /*rayStore,*/ hplane, param.getRAYEPS());
    bool ptFlagToAdd = calcIntersectionPointWithSplit(tmpPoint, solver,
        solnInfo, vertCreated, newRay, split_var);

    if (ptFlagToAdd
        && !duplicatePoint(interPtsAndRays, interPtsAndRays.RHS, tmpPoint)) {
//      numNewPointsAdded++;
      interPtsAndRays.addPointOrRayToIntersectionInfo(tmpPoint, 1.0, ray_ind,
          newRayColIndices[r], -1, -1, -1, -1, -1., -1., false);
//      if ((interPtsAndRaysOld == NULL)
//          || !duplicatePoint(*interPtsAndRaysOld,
//              interPtsAndRaysOld->RHS, tmpPoint)) {
      if (pointInP(solver, solnInfo, tmpPoint)) {
        numFinalPointsAdded++;
      }
//      }
    }
    // We do not need to add the rays, since these will not be adding to the num final points
//    else if (!CHECK_FOR_DUP_RAYS
//        || !duplicateRay(interPtsAndRays, interPtsAndRays.RHS,
//            newRay)) {
//      interPtsAndRays.addPointOrRayToIntersectionInfo(newRay, 0.0,
//          ray_ind, newRayColIndices[r], -1, -1);
//    }
  }
//  if (numNewPointsAdded > 0) {
//    avgAddedDepth += sumAddedDepth / numNewPointsAdded;
//    totalNumPointsAdded += numNewPointsAdded;
//  }

  return numFinalPointsAdded;
}

/**
 * The ray ray_ind is being cut, creating the new vertex tmpVertex
 */
void updateHplaneStore(std::vector<std::vector<int> >& strongVertHplaneIndex,
    std::vector<IntersectionInfo>& interPtsAndRays,
    std::vector<Vertex>& vertexStore, const int init_vert_ind,
    std::vector<Ray>& rayStore, std::vector<int>& allRaysCut,
    std::vector<std::vector<int> >& raysToBeCut,
    std::vector<Hplane>& hplaneStore, const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo,
    const Vertex& newVertex,
    const Hplane& tmpHplane, const std::vector<int>& splitsAffByHplane,
    const int ray_ind, const int act_ind) {

  // If the hyperplane activated affects at least one split, then this ray is cut for some split.
  // (I.e., if ||splitsEffByHplane|| = 0, then for all splits,
  // the ray intersects the boundary of the split before it intersects
  // the hplane chosen.)
  if (splitsAffByHplane.size() == 0) {
    return; // If hplane affects no splits, then we do not activate this hplane
  }

  // The current ray will be cut by a hplane prior to bd S (for at least one split)
  allRaysCut.push_back(ray_ind);

  // Add the new hplane
  const int oldHplaneIndex = hasHplaneBeenActivated(hplaneStore, tmpHplane);
  const int newHplaneIndex =
      (oldHplaneIndex == -1) ? hplaneStore.size() : oldHplaneIndex;
  if (oldHplaneIndex == -1) {
    hplaneStore.push_back(tmpHplane);
  }

  // Add the new vertex
  const int newVertexIndex = addVertex(vertexStore, newVertex);
//      newHplaneIndex);

//  // Store that this ray is "first" cut by the proper hplane / vertex index
//  rayStore[ray_ind].firstHplaneOverall = newHplaneIndex;
//  rayStore[ray_ind].firstVertexOverall = newVertexIndex;

  // Only iterate over splits where the hyperplane intersects the ray before the bdS
  for (unsigned s1 = 0; s1 < splitsAffByHplane.size(); s1++) {
    const int split_ind = splitsAffByHplane[s1];

    // This ray is cut for this split
    raysToBeCut[split_ind].push_back(ray_ind);

    // Stores only those rays of C1 that have to be cut by the new hplane for validity of the procedure
    hplaneStore[newHplaneIndex].rayToBeCutByHplane[split_ind].push_back(
        ray_ind); // Clearly this ray, r^j, is cut
    hplaneStore[newHplaneIndex].vertOfRayToBeCutByHplane[split_ind].push_back(
        init_vert_ind);
    hplaneStore[newHplaneIndex].vertCreatedByRayToBeCutByHplane[split_ind].push_back(
        newVertexIndex);

//        numRaysCutByHplane[split_ind][tmpHplane.var] =
//            hplaneStore[newHplaneIndex].rayToBeCutByHplane[split_ind].size();

    // Store hplane index that cuts this ray first
    // Note that strongVertHplaneIndex only has indices for rays that are cut for this split,
    // so to get which ray this corresponds to, say in index i, then we look at
    // raysToBeCut[split_ind][i]
    strongVertHplaneIndex[split_ind].push_back(newHplaneIndex);
//        rayStore[sortIndex[j]].firstHplane[split_ind] = newHplaneIndex;
//        rayStore[sortIndex[j]].firstVertex[split_ind] = newVertexIndex;

    // Check if activated hyperplane is new or has been previously activated
    const int oldHplaneForSplit = hasHplaneBeenActivated(hplaneStore,
        interPtsAndRays[split_ind].hplaneIndexByAct[act_ind],
        tmpHplane, true);

    // If the hplane is new for this split, we need to add it to activated hplanes
    // And we need to get the other rays affected by this hplane for this split
    if (oldHplaneForSplit == -1) {
      // Store the index of this hplane in hplanesToAct
      interPtsAndRays[split_ind].hplaneIndexByAct[act_ind].push_back(
          newHplaneIndex);

      // This stores *all* the rays of C1 intersected by the new hyperplane before bd S
      addRaysIntByHplaneBeforeBdS(
          hplaneStore[newHplaneIndex].vertOfAllRaysIntByHplane[split_ind],
          hplaneStore[newHplaneIndex].allRaysIntByHplane[split_ind],
          solver, solnInfo, hplaneStore[newHplaneIndex],
          vertexStore, init_vert_ind, rayStore, split_ind);

      // We need to include in R* (rays cut by the hplane) all rays previously cut
      // This is sufficient for the validity of the procedure
      std::vector<bool> rayIntersectedFlag(solnInfo.numNB, false);
      const int numRaysIntByHplane =
          hplaneStore[newHplaneIndex].allRaysIntByHplane[split_ind].size();
      for (int r = 0; r < numRaysIntByHplane; r++) {
        rayIntersectedFlag[hplaneStore[newHplaneIndex].allRaysIntByHplane[split_ind][r]] =
            true;
      }

      // For all rays previously cut for this split, check whether the hplane intersects it
      for (int r = 0; r < (int) raysToBeCut[split_ind].size() - 1; r++) {
        const int curr_ray = raysToBeCut[split_ind][r];
        if (rayIntersectedFlag[curr_ray]) {
          newVertexHelper(vertexStore, init_vert_ind, rayStore,
              hplaneStore, solver, solnInfo, 
              curr_ray, split_ind, newHplaneIndex, true);
        }
      }
    } else {
      // This hplane has previously been activated on this split
      // We simply need to add r^j to all the hyperplanes activated after
      const int numHplanesActForSplit =
          interPtsAndRays[split_ind].hplaneIndexByAct[act_ind].size();
      for (int h = oldHplaneForSplit + 1; h < numHplanesActForSplit;
          h++) {
        const int curr_hplane_ind =
            interPtsAndRays[split_ind].hplaneIndexByAct[act_ind][h];
        const int rayIntCurrHplaneIndex =
            find_val(ray_ind,
                hplaneStore[curr_hplane_ind].allRaysIntByHplane[split_ind]);

        // If the hplane intersects the ray prior to bd S
        if (rayIntCurrHplaneIndex >= 0) {
          newVertexHelper(vertexStore, init_vert_ind, rayStore,
              hplaneStore, solver, solnInfo, 
              ray_ind, split_ind, curr_hplane_ind);
        }
      }

    }
  }
} /* updateHplaneStore (old) */

/**
 * New version, 06/07/2016
 * The ray ray_ind is being cut, creating the new vertex tmpVertex
 */
void updateHplaneStore(std::vector<IntersectionInfo>& interPtsAndRays,
    std::vector<Vertex>& vertexStore, const int init_vert_ind,
    std::vector<Ray>& rayStore,
//    std::vector<std::vector<int> >& allRaysCut,
    std::vector<int>& allRaysCutAcrossSplits,
//    std::vector<std::vector<std::vector<int> > >& raysToBeCut,
    std::vector<std::vector<int> >& raysCutForSplit,
    std::vector<Hplane>& hplaneStore,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    const Vertex& newVertex, const Hplane& tmpHplane,
    const std::vector<int>& splitsAffByHplane, const int ray_ind,
    const int act_ind, const bool checkIfRayPrevCut) {

  // If the hyperplane activated affects at least one split, then this ray is cut for some split.
  // (I.e., if ||splitsEffByHplane|| = 0, then for all splits,
  // the ray intersects the boundary of the split before it intersects
  // the hplane chosen.)
  if (splitsAffByHplane.size() == 0) {
    return; // If hplane affects no splits, then we do not activate this hplane
  }

  // The current ray will be cut by a hplane prior to bd S (for at least one split)
  bool rayPrevCutAcrossSplits = false;
  if (checkIfRayPrevCut) {
    rayPrevCutAcrossSplits = (find_val(ray_ind, allRaysCutAcrossSplits) >= 0);
  }
  if (!rayPrevCutAcrossSplits) {
    allRaysCutAcrossSplits.push_back(ray_ind);
  }

  // Add the new hplane
  const int oldHplaneIndex = hasHplaneBeenActivated(hplaneStore, tmpHplane);
  const int newHplaneIndex =
      (oldHplaneIndex == -1) ? hplaneStore.size() : oldHplaneIndex;
  if (oldHplaneIndex == -1) {
    hplaneStore.push_back(tmpHplane);
    if (hplaneStore[newHplaneIndex].rayToBeCutByHplane.size() > 0) {
      for (int s = 0; s < (int) hplaneStore[newHplaneIndex].rayToBeCutByHplane.size(); s++) {
        hplaneStore[newHplaneIndex].rayToBeCutByHplane[s].clear();
      }
    }
    hplaneStore[newHplaneIndex].resizeVectors(solnInfo.numFeasSplits);
  }

  // Add the new vertex
  const int newVertexIndex = addVertex(vertexStore, newVertex);
//      newHplaneIndex);

//  // Store that this ray is "first" cut by the proper hplane / vertex index
//  rayStore[ray_ind].firstHplaneOverall = newHplaneIndex;
//  rayStore[ray_ind].firstVertexOverall = newVertexIndex;

  // Only iterate over splits where the hyperplane intersects the ray before the bdS
  for (unsigned s1 = 0; s1 < splitsAffByHplane.size(); s1++) {
    const int split_ind = splitsAffByHplane[s1];

    // This ray is cut for this split
    bool rayPrevCutForSplit = false;
    if (checkIfRayPrevCut) {
      rayPrevCutForSplit = (find_val(ray_ind, raysCutForSplit[split_ind]) >= 0);
    }
    if (!rayPrevCutForSplit) {
      raysCutForSplit[split_ind].push_back(ray_ind);
    }

    // Stores only those rays of C1 that have to be cut by the new hplane for validity of the procedure
    const int prevTotalNumRaysCutByHplane =
        hplaneStore[newHplaneIndex].rayToBeCutByHplane[split_ind].size();
    hplaneStore[newHplaneIndex].rayToBeCutByHplane[split_ind].push_back(
        ray_ind); // Clearly this ray, r^j, is cut
    hplaneStore[newHplaneIndex].vertOfRayToBeCutByHplane[split_ind].push_back(
        init_vert_ind);
    hplaneStore[newHplaneIndex].vertCreatedByRayToBeCutByHplane[split_ind].push_back(
        newVertexIndex);

//        numRaysCutByHplane[split_ind][tmpHplane.var] =
//            hplaneStore[newHplaneIndex].rayToBeCutByHplane[split_ind].size();

    // Store hplane index that cuts this ray first
    // Note that strongVertHplaneIndex only has indices for rays that are cut for this split,
    // so to get which ray this corresponds to, say in index i, then we look at
    // raysToBeCut[split_ind][i]
//    strongVertHplaneIndex[split_ind].push_back(newHplaneIndex);
//        rayStore[sortIndex[j]].firstHplane[split_ind] = newHplaneIndex;
//        rayStore[sortIndex[j]].firstVertex[split_ind] = newVertexIndex;

    // Check if activated hyperplane is new or has been previously activated
    // The vector actHplaneIndexForSplit is needed for performing activations later
    const int oldHplaneForActAndSplit = hasHplaneBeenActivated(hplaneStore,
        interPtsAndRays[split_ind].hplaneIndexByAct[act_ind], tmpHplane, true);
    const int oldHplaneForSplitOverall = hasHplaneBeenActivated(hplaneStore,
        interPtsAndRays[split_ind].allHplaneToAct, tmpHplane, true);

    // If the hplane is new for this split, we need to add it to activated hplanes
    // And we need to get the other rays affected by this hplane for this split
    if (oldHplaneForSplitOverall == -1) {
      // Store the index of this hplane in hplanesToAct
      interPtsAndRays[split_ind].hplaneIndexByAct[act_ind].push_back(
          newHplaneIndex);
      interPtsAndRays[split_ind].indexOfHplaneInOverallOrderForSplit[act_ind].push_back(
          interPtsAndRays[split_ind].allHplaneToAct.size());
      interPtsAndRays[split_ind].allHplaneToAct.push_back(newHplaneIndex);
      // This stores *all* the rays of C1 intersected by the new hyperplane before bd S
      addRaysIntByHplaneBeforeBdS(
          hplaneStore[newHplaneIndex].vertOfAllRaysIntByHplane[split_ind],
          hplaneStore[newHplaneIndex].allRaysIntByHplane[split_ind], solver,
          solnInfo, hplaneStore[newHplaneIndex], vertexStore, init_vert_ind,
          rayStore, split_ind);

      // We need to include in R* (rays cut by the hplane) all rays previously cut
      // This is sufficient for the validity of the procedure
      // TODO might be better to sort here instead of creating this rayIntersectedFlag
      std::vector<bool> rayIntersectedFlag(solnInfo.numNB, false);
      const int numRaysIntByHplane =
          hplaneStore[newHplaneIndex].allRaysIntByHplane[split_ind].size();
      for (int r = 0; r < numRaysIntByHplane; r++) {
        rayIntersectedFlag[hplaneStore[newHplaneIndex].allRaysIntByHplane[split_ind][r]] =
            true;
      }

      // The hiccup is that this hyperplane may have been included for the split on a previous activation
      // Some of the rays cut by other hyperplanes may already be cut by this hyperlane previously
//      // We remedy this by ignoring all rays cut by this hyperplane in a previous activation
//      const int numRaysCutByHplane =
//          hplaneStore[newHplaneIndex].rayToBeCutByHplane[split_ind].size();
//      for (int r = 0; r < numRaysCutByHplane; r++) {
//        rayIntersectedFlag[hplaneStore[newHplaneIndex].rayToBeCutByHplane[split_ind][r]] =
//            false;
//      }

      // For all rays previously cut for this split, check whether the hplane intersects it
      for (int r = 0; r < (int) raysCutForSplit[split_ind].size() - 1; r++) {
        const int curr_ray = raysCutForSplit[split_ind][r];
        if (rayIntersectedFlag[curr_ray]) {
          newVertexHelper(vertexStore, init_vert_ind, rayStore,
              hplaneStore, solver, solnInfo,
              curr_ray, split_ind, newHplaneIndex, true);
        }
      }
    } else { /* oldHplaneForSplitOverall != -1 */
      // Insert this hplane in the proper order in hplaneIndexByAct
      // Also need to update indexOfHplaneInOverallOrder
      if (oldHplaneForActAndSplit == -1) {
        std::vector<int>::iterator it =
            std::lower_bound(
                interPtsAndRays[split_ind].indexOfHplaneInOverallOrderForSplit[act_ind].begin(),
                interPtsAndRays[split_ind].indexOfHplaneInOverallOrderForSplit[act_ind].end(),
                oldHplaneForSplitOverall);
        const int tmp_h =
            (it
                - interPtsAndRays[split_ind].indexOfHplaneInOverallOrderForSplit[act_ind].begin());

        interPtsAndRays[split_ind].hplaneIndexByAct[act_ind].insert(
            interPtsAndRays[split_ind].hplaneIndexByAct[act_ind].begin()
                + tmp_h, newHplaneIndex);
        interPtsAndRays[split_ind].indexOfHplaneInOverallOrderForSplit[act_ind].insert(
            it, oldHplaneForSplitOverall);
      }
      /* finished inserting hplane into proper place in this activation */

      // Simply need to add r^j to all the hyperplanes activated after
      const int numHplanesActForSplit =
          interPtsAndRays[split_ind].allHplaneToAct.size();
      for (int h = oldHplaneForSplitOverall + 1; h < numHplanesActForSplit;
          h++) {
        const int curr_hplane_ind = interPtsAndRays[split_ind].allHplaneToAct[h];
        const bool rayIntByCurrHplane = (find_val(ray_ind,
            hplaneStore[curr_hplane_ind].allRaysIntByHplane[split_ind]) >= 0);
        const bool rayCutByCurrHplane = (find_val(ray_ind,
            hplaneStore[curr_hplane_ind].rayToBeCutByHplane[split_ind]) >= 0);

        // If the hplane intersects the ray prior to bd S, but has not cut it yet
        if (rayIntByCurrHplane && !rayCutByCurrHplane) {
          newVertexHelper(vertexStore, init_vert_ind, rayStore, hplaneStore,
              solver, solnInfo, ray_ind, split_ind, curr_hplane_ind);
        }

        // Need to update numRaysCutByHplaneInclPrevAct
        // Ensure the vectors are properly sized
        const int old_num_act_rounds_for_hplane =
            (int) hplaneStore[curr_hplane_ind].numRaysCutByHplaneInclPrevAct[split_ind].size();
        const int newTotalNumRaysCutByHplane =
            hplaneStore[curr_hplane_ind].rayToBeCutByHplane[split_ind].size();
        const int prevTotal =
            (old_num_act_rounds_for_hplane > 0) ?
                hplaneStore[curr_hplane_ind].numRaysCutByHplaneInclPrevAct[split_ind][old_num_act_rounds_for_hplane
                    - 1] :
                0;
        if (old_num_act_rounds_for_hplane < act_ind + 1) {
          hplaneStore[curr_hplane_ind].numRaysCutByHplaneInclPrevAct[split_ind].resize(
              act_ind + 1, prevTotal);
        }
        hplaneStore[curr_hplane_ind].numRaysCutByHplaneInclPrevAct[split_ind][act_ind] =
            newTotalNumRaysCutByHplane;

        // The hplane has been activated before, but perhaps not for this activation round
        const int oldCurrHplaneForActAndSplit = hasHplaneBeenActivated(
            hplaneStore, interPtsAndRays[split_ind].hplaneIndexByAct[act_ind],
            hplaneStore[curr_hplane_ind], true);
        if (oldCurrHplaneForActAndSplit == -1) {
          std::vector<int>::iterator it =
              std::lower_bound(
                  interPtsAndRays[split_ind].indexOfHplaneInOverallOrderForSplit[act_ind].begin(),
                  interPtsAndRays[split_ind].indexOfHplaneInOverallOrderForSplit[act_ind].end(),
                  h);
          const int tmp_h =
              (it
                  - interPtsAndRays[split_ind].indexOfHplaneInOverallOrderForSplit[act_ind].begin());

          interPtsAndRays[split_ind].hplaneIndexByAct[act_ind].insert(
              interPtsAndRays[split_ind].hplaneIndexByAct[act_ind].begin()
                  + tmp_h, curr_hplane_ind);
          interPtsAndRays[split_ind].indexOfHplaneInOverallOrderForSplit[act_ind].insert(
              it, h);
        }
      }
    } /* else oldHplaneForSplit != -1 */

    // Update numRaysCutByHplaneInclPrevAct, including all activations "missed" by this hyperplane
    // Ensure the vectors are properly sized
    const int old_num_act_rounds_for_hplane =
        (int) hplaneStore[newHplaneIndex].numRaysCutByHplaneInclPrevAct[split_ind].size();
    const int newTotalNumRaysCutByHplane =
            hplaneStore[newHplaneIndex].rayToBeCutByHplane[split_ind].size();
    if (old_num_act_rounds_for_hplane < act_ind + 1) {
      hplaneStore[newHplaneIndex].numRaysCutByHplaneInclPrevAct[split_ind].resize(
          act_ind + 1, prevTotalNumRaysCutByHplane);
    }
    hplaneStore[newHplaneIndex].numRaysCutByHplaneInclPrevAct[split_ind][act_ind] =
        newTotalNumRaysCutByHplane;
  } /* iterating over splitsAffByHplane */
} /* updateHplaneStore (new) */

/**
 * @brief Adds a new vertex that has not been calculated yet
 *
 * @param check_split :: If true, ensures distToSplit > distToHplane
 */
void newVertexHelper(std::vector<Vertex>& vertexStore, const int init_vert_ind,
    const std::vector<Ray>& rayStore, std::vector<Hplane>& hplaneStore,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    const int ray_ind,
    const int split_ind, const int hplane_ind, const bool check_split) {
  // We have to create the vertex from this new intersection
  Vertex newVertex;
  const double distToHplane = hplaneStore[hplane_ind].distanceToHplane(
      vertexStore[init_vert_ind], rayStore[ray_ind], solver,
      param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND), solnInfo);

  if (check_split) {
    const double distToSplit = distanceToSplit(vertexStore[init_vert_ind],
        rayStore[ray_ind], solnInfo.feasSplitVar[split_ind],
        solver, solnInfo);

    // If the ray intersects the split before hitting the hyperplane,
    // then it does not lead to a vertex for this split
    if (!lessThanVal(distToHplane, distToSplit)) {
      return;
    }
  }

  calcNewVectorCoordinates(newVertex, distToHplane,
      vertexStore[init_vert_ind], rayStore[ray_ind]);

  // Add this new vertex
  newVertexHelper(vertexStore, init_vert_ind, hplaneStore, newVertex, ray_ind,
      split_ind, hplane_ind);
} /* newVertexHelper */

/**
 * @brief Adds a new vertex that has already been calculated
 */
void newVertexHelper(std::vector<Vertex>& vertexStore, const int init_vert_ind,
    std::vector<Hplane>& hplaneStore, const Vertex& newVertex,
    const int ray_ind, const int split_ind, const int hplane_ind) {
  // Add this new vertex
  const int vertex_ind = addVertex(vertexStore, newVertex);//, hplane_ind);

  // Add ray to set of rays cut by the hyperplane
  hplaneStore[hplane_ind].rayToBeCutByHplane[split_ind].push_back(ray_ind);
  hplaneStore[hplane_ind].vertOfRayToBeCutByHplane[split_ind].push_back(
      init_vert_ind);
  hplaneStore[hplane_ind].vertCreatedByRayToBeCutByHplane[split_ind].push_back(
      vertex_ind);
} /* newVertexHelper */

/**
 * @return rayIndicesIntByHplane [vertex][ray ind] std::vector keeping all rays cut by this hplane
 */
void addRaysIntByHplaneBeforeBdS(std::vector<int>& vertOfRayIndicesIntByHplane,
    std::vector<int>& rayIndicesIntByHplane,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    const Hplane& hplane,
    const std::vector<Vertex>& vertexStore, const int init_vert_ind,
    const std::vector<Ray>& rayStore, const int split_ind) {
  // For the vertex, check the rays emanating from it (that still remain to be cut)
  const int numRaysForVertex = vertexStore[init_vert_ind].rayIndices.size();
  for (int r = 0; r < numRaysForVertex; r++) {
    if (!vertexStore[init_vert_ind].rayActiveFlag[r]) {
      continue;
    }

    const int ray_ind = vertexStore[init_vert_ind].rayIndices[r];
    const int split_var = solnInfo.feasSplitVar[split_ind];

    const double distToSplit = distanceToSplit(vertexStore[init_vert_ind],
        rayStore[ray_ind], split_var, solver, solnInfo);

//    const double distToHplane = hplane.distanceToHplane(
//        vertexStore[init_vert_ind], rayStore[ray_ind], solver,
//        param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND),
//        &(solnInfo.nonBasicVarIndex));
    const double distToHplane = hplane.distanceToHplane(
        vertexStore[init_vert_ind], rayStore[ray_ind], solver,
        param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND), solnInfo);

//      if (distToHplane >= solver->getInfinity() - param.getEPS()) {
    if (isInfinity(distToHplane)) {
      continue; // hplane does not intersect this ray
//      } else if (distToHplane <= distToSplit - param.getEPS()) {
    } else if (lessThanVal(distToHplane, distToSplit)) {
      vertOfRayIndicesIntByHplane.push_back(init_vert_ind);
      rayIndicesIntByHplane.push_back(ray_ind);
    }
  }
} /* addRaysIntByHplaneBeforeBdS */

/**
 * Chooses one hyperplane to activate per split, based on the most number of previously generated points it cuts
 */
void chooseNewHplanesPointsRemoved(std::vector<Hplane>& hplaneStore,
    std::vector<int>& allRaysCutAct2,
    std::vector<std::vector<int> >& raysToBeCutAct2,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    std::vector<Vertex>& vertexStore, // will be modified to add new vertices from activation
    const std::vector<Ray>& rayStore,
    std::vector<IntersectionInfo>& interPtsAndRays) {
  const int act_ind = 1; // Currently only works with second activation
  const int numSplits = solnInfo.numFeasSplits;
  const int numRaysOfC1 = solnInfo.raysOfC1.size();
  const char* rowSense = solver->getRowSense();
  std::vector<std::vector<int> > raysToBeCutByHplaneAct2(numSplits);
  std::vector<std::vector<int> > allRaysIntByHplaneAct2(numSplits);

  std::vector<std::vector<int> > tmpRaysToBeCut(numSplits); // will be same as tmpRaysCutByHplane
  std::vector<std::vector<int> > tmpRaysCutByHplane(numSplits); // rays that are cut by this hplane
  std::vector<std::vector<int> > tmpAllRaysIntersected(numSplits); // this may include more rays

  // For each potential hyperplane to activate, count how many points would be removed
  // by activating the hyperplane on each split
  std::vector<Hplane> tmpHplaneInfo(numSplits);
  std::vector<int> prevHplaneIndex(numSplits, -1); // Index of hplane if it has been previously activated
  std::vector<int> maxPointsRemoved(numSplits, -1); // Points removed by the hyperplane
//  std::vector<double> distToHplane(numSplits, -1.0); // Distance to hplane chosen for each split
//  std::vector<double> tmpDistToHplane(numSplits, -1.0); // Distance to hplane chosen for each split

  for (int h = 0; h < (int) solnInfo.basicSlackVarIndex.size(); h++) {
    // This is the index of the variable and the row corresponding to the slack variable
    const int currVar = solnInfo.basicSlackVarIndex[h];
    const int rowIndex = solnInfo.rowOfVar[currVar];

    // Equality constraints correspond to slack variables that are basic but have value zero
    // They clearly have zero distance from the optimal solution
    if (rowSense[rowIndex] == 'E') {
      continue;
    }

    Hplane tmpHplane(currVar, rowIndex, static_cast<int>(HplaneBoundFlag::toLBSlack));
    const int oldHplaneIndex = hasHplaneBeenActivated(hplaneStore,
        tmpHplane);

    std::vector<int> numPointsCutByHplane(numSplits, -1);
    newHplaneHelperPointsRemoved(numPointsCutByHplane, tmpHplane,
        tmpRaysToBeCut, tmpRaysCutByHplane, tmpAllRaysIntersected,
        solver, solnInfo, vertexStore, rayStore,
        hplaneStore, oldHplaneIndex, interPtsAndRays);

    for (int split_ind = 0; split_ind < numSplits; split_ind++) {
      if (numPointsCutByHplane[split_ind] > maxPointsRemoved[split_ind]) {
        maxPointsRemoved[split_ind] = numPointsCutByHplane[split_ind];
        tmpHplaneInfo[split_ind] = tmpHplane;
        raysToBeCutAct2[split_ind] = tmpRaysToBeCut[split_ind];
        raysToBeCutByHplaneAct2[split_ind] =
            tmpRaysCutByHplane[split_ind];
        allRaysIntByHplaneAct2[split_ind] =
            tmpAllRaysIntersected[split_ind];
        prevHplaneIndex[split_ind] = oldHplaneIndex;
//        distToHplane[split_ind] = tmpDistToHplane[split_ind];
      }
    }
  }

  if (SECOND_ACTIVATE_BASIC_BOUNDS) {
    // For each structural basic variable, see if one of its bound does better
    for (int v = 0; v < (int) solnInfo.basicOrigVarIndex.size(); v++) {
      // This is the index of the row corresponding to the slack variable
      const int currVar = solnInfo.basicOrigVarIndex[v];
      const int rowIndex = solnInfo.rowOfVar[currVar];

      // Equality constraints do not lead to interesting hyperplanes
      if (rowSense[rowIndex] == 'E') {
        continue;
      }

      // Check lower bound of this variable
      const double LB = getVarLB(solver, currVar);
      if (!isNegInfinity(LB, solver->getInfinity())) {
        Hplane tmpHplane(currVar, rowIndex, static_cast<int>(HplaneBoundFlag::toLB));
        const int oldHplaneIndex = hasHplaneBeenActivated(hplaneStore,
            tmpHplane);

        std::vector<int> numPointsCutByHplane(numSplits, -1);
        newHplaneHelperPointsRemoved(numPointsCutByHplane, tmpHplane,
            tmpRaysToBeCut, tmpRaysCutByHplane,
            tmpAllRaysIntersected, solver, solnInfo, 
            vertexStore, rayStore, hplaneStore, oldHplaneIndex,
            interPtsAndRays);

        for (int split_ind = 0; split_ind < numSplits; split_ind++) {
          if (numPointsCutByHplane[split_ind]
              > maxPointsRemoved[split_ind]) {
            maxPointsRemoved[split_ind] =
                numPointsCutByHplane[split_ind];
            tmpHplaneInfo[split_ind] = tmpHplane;
            raysToBeCutAct2[split_ind] = tmpRaysToBeCut[split_ind];
            raysToBeCutByHplaneAct2[split_ind] =
                tmpRaysCutByHplane[split_ind];
            allRaysIntByHplaneAct2[split_ind] =
                tmpAllRaysIntersected[split_ind];
            prevHplaneIndex[split_ind] = oldHplaneIndex;
//            distToHplane[split_ind] = tmpDistToHplane[split_ind];
          }
        }
      }

      // Check upper bound of this variable
      const double UB = getVarUB(solver, currVar);
      if (!isInfinity(UB, solver->getInfinity())) {
        Hplane tmpHplane(currVar, rowIndex, static_cast<int>(HplaneBoundFlag::toUB));
        const int oldHplaneIndex = hasHplaneBeenActivated(hplaneStore,
            tmpHplane);

        std::vector<int> numPointsCutByHplane(numSplits, -1);
        newHplaneHelperPointsRemoved(numPointsCutByHplane, tmpHplane,
            tmpRaysToBeCut, tmpRaysCutByHplane,
            tmpAllRaysIntersected, solver, solnInfo, 
            vertexStore, rayStore, hplaneStore, oldHplaneIndex,
            interPtsAndRays);

        for (int split_ind = 0; split_ind < numSplits; split_ind++) {
          if (numPointsCutByHplane[split_ind]
              > maxPointsRemoved[split_ind]) {
            maxPointsRemoved[split_ind] =
                numPointsCutByHplane[split_ind];
            tmpHplaneInfo[split_ind] = tmpHplane;
            raysToBeCutAct2[split_ind] = tmpRaysToBeCut[split_ind];
            raysToBeCutByHplaneAct2[split_ind] =
                tmpRaysCutByHplane[split_ind];
            allRaysIntByHplaneAct2[split_ind] =
                tmpAllRaysIntersected[split_ind];
            prevHplaneIndex[split_ind] = oldHplaneIndex;
//            distToHplane[split_ind] = tmpDistToHplane[split_ind];
          }
        }
      }
    }
  }

  if (SECOND_ACTIVATE_NB_BOUNDS) {
    // Check if one of the bounds of the non-basic columns does better
    for (int ray_ind = 0; ray_ind < solnInfo.numNBOrig; ray_ind++) {
      const int currVar = solnInfo.nonBasicVarIndex[ray_ind];

      // Check that both bounds exist
      const double LB = getVarLB(solver, currVar);
      const double UB = getVarUB(solver, currVar);

      if (!isNegInfinity(LB, solver->getInfinity())
          && !isInfinity(UB, solver->getInfinity())) {
        Hplane tmpHplane(currVar, static_cast<int>(HplaneRowFlag::NB),
            static_cast<int>(HplaneBoundFlag::toUBNB)); // Currently at lb = 0
        const int oldHplaneIndex = hasHplaneBeenActivated(hplaneStore,
            tmpHplane);

        std::vector<int> numPointsCutByHplane(numSplits, -1);
        newHplaneHelperPointsRemoved(numPointsCutByHplane, tmpHplane,
            tmpRaysToBeCut, tmpRaysCutByHplane,
            tmpAllRaysIntersected, solver, solnInfo, 
            vertexStore, rayStore, hplaneStore, oldHplaneIndex,
            interPtsAndRays, ray_ind);

        for (int split_ind = 0; split_ind < numSplits; split_ind++) {
          if (numPointsCutByHplane[split_ind]
              > maxPointsRemoved[split_ind]) {
            maxPointsRemoved[split_ind] =
                numPointsCutByHplane[split_ind];
            tmpHplaneInfo[split_ind] = tmpHplane;
            raysToBeCutAct2[split_ind] = tmpRaysToBeCut[split_ind];
            raysToBeCutByHplaneAct2[split_ind] =
                tmpRaysCutByHplane[split_ind];
            allRaysIntByHplaneAct2[split_ind] =
                tmpAllRaysIntersected[split_ind];
            prevHplaneIndex[split_ind] = oldHplaneIndex;
//            distToHplane[split_ind] = tmpDistToHplane[split_ind];
          }
        }
      }
    }
  }

  // Now update allRaysCut and other properties
  const int numHplanesPrevAct = hplaneStore.size();
  std::vector<bool> allRaysCutFlag(numRaysOfC1, false);
  for (int s = 0; s < numSplits; s++) {
#ifdef TRACE
    printf(
        "Second hplane for split %d removes %d points. Hplane var index: %d. Hplane row index: %d. Hplane UB flag: %d.\n",
        s, maxPointsRemoved[s], tmpHplaneInfo[s].var,
        tmpHplaneInfo[s].row, tmpHplaneInfo[s].ubflag);
#endif
    // If we did not find a new hplane to activate for this split, skip it
    if (tmpHplaneInfo[s].var == -1)
      continue;

    for (int r = 0; r < (int) raysToBeCutAct2[s].size(); r++) {
      if (!allRaysCutFlag[raysToBeCutAct2[s][r]]) {
        allRaysCutFlag[raysToBeCutAct2[s][r]] = true;
        allRaysCutAct2.push_back(raysToBeCutAct2[s][r]);
      }
    }

    // It was not activated prior to this round of hyperplanes, but maybe it has been already activated for a different split
    const int oldHplaneIndex =
        (prevHplaneIndex[s] >= 0) ?
            prevHplaneIndex[s] :
            hasHplaneBeenActivated(hplaneStore, tmpHplaneInfo[s],
                numHplanesPrevAct);

    // Now update information on hyperplanes, adding new ones when necessary
    const int numRaysCutByHplaneAct2 = raysToBeCutByHplaneAct2[s].size();
    const int numAllRaysIntByHplaneAct2 = allRaysIntByHplaneAct2[s].size();
    if (oldHplaneIndex == -1) {
      const int numHplanes = hplaneStore.size();
      interPtsAndRays[s].hplaneIndexByAct[act_ind].push_back(
          numHplanes);
      hplaneStore.push_back(tmpHplaneInfo[s]);
      hplaneStore[numHplanes].resizeVectors(numSplits);
      hplaneStore[numHplanes].rayToBeCutByHplane[s] =
          raysToBeCutByHplaneAct2[s];
      hplaneStore[numHplanes].vertOfRayToBeCutByHplane[s].resize(
          numRaysCutByHplaneAct2, 0);
      hplaneStore[numHplanes].allRaysIntByHplane[s] =
          allRaysIntByHplaneAct2[s];
      hplaneStore[numHplanes].vertOfAllRaysIntByHplane[s].resize(
          numAllRaysIntByHplaneAct2, 0);

      // Calculate new vertex for each ray intersected by this hyperplane
      Vertex newVertex;
      for (int r = 0; r < numRaysCutByHplaneAct2; r++) {
        // How far to go?
        const int ray_ind = raysToBeCutByHplaneAct2[s][r];
//        const double distToHplane = distanceToHplane(vertexStore[0],
//            rayStore[ray_ind], hplaneStore[numHplanes], solver,
//            solnInfo);
        const double distToHplane = (hplaneStore[numHplanes]).distanceToHplane(
            rayStore[ray_ind], solver,
            param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND),
            &(solnInfo.nonBasicVarIndex));
        calcNewVectorCoordinates(newVertex, distToHplane,
            vertexStore[0], rayStore[ray_ind]);

        const int newVertexIndex = addVertex(vertexStore, newVertex);
//            numHplanes);

        hplaneStore[numHplanes].vertCreatedByRayToBeCutByHplane[s].push_back(
            newVertexIndex);
      }

      updateHplaneInfoAfterActivation(hplaneStore[numHplanes], act_ind,
          s);
    } else {
      interPtsAndRays[s].hplaneIndexByAct[act_ind].push_back(
          oldHplaneIndex); // We are activating this hplane a second time
      // The arrays have already been resized correctly
      // And all rays to be intersected will be the same, since this is rank 1
      const int numRaysCutByHplaneAct1 =
          hplaneStore[oldHplaneIndex].rayToBeCutByHplane[s].size();

      // rayToBeCutByHplane
      hplaneStore[oldHplaneIndex].rayToBeCutByHplane[s].insert(
          hplaneStore[oldHplaneIndex].rayToBeCutByHplane[s].end(),
          raysToBeCutByHplaneAct2[s].begin(),
          raysToBeCutByHplaneAct2[s].end());
      hplaneStore[oldHplaneIndex].vertOfRayToBeCutByHplane[s].resize(
          numRaysCutByHplaneAct1 + numRaysCutByHplaneAct2, 0);

      // allRaysIntByHplane may not need to be updated (same set of rays still intersected for this split)
      // (if this hplane has already been activated for this split)
      if (hplaneStore[oldHplaneIndex].allRaysIntByHplane[s].size() == 0) {
        hplaneStore[oldHplaneIndex].allRaysIntByHplane[s] =
            allRaysIntByHplaneAct2[s];
        hplaneStore[oldHplaneIndex].vertOfAllRaysIntByHplane[s].resize(
            numAllRaysIntByHplaneAct2, 0);
      }

      // Calculate new vertex for each ray intersected by this hyperplane
      Vertex newVertex;
      for (int r = 0; r < numRaysCutByHplaneAct2; r++) {
        const int ray_ind = raysToBeCutByHplaneAct2[s][r];
//        const double distToHplane = distanceToHplane(vertexStore[0],
//            rayStore[ray_ind], hplaneStore[oldHplaneIndex], solver,
//            solnInfo);
        const double distToHplane = (hplaneStore[oldHplaneIndex]).distanceToHplane(
               rayStore[ray_ind], solver,
               param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND),
               &(solnInfo.nonBasicVarIndex));
        calcNewVectorCoordinates(newVertex, distToHplane,
            vertexStore[0], rayStore[ray_ind]);

        const int newVertexIndex = addVertex(vertexStore, newVertex);
//            oldHplaneIndex);

        hplaneStore[oldHplaneIndex].vertCreatedByRayToBeCutByHplane[s].push_back(
            newVertexIndex);
      }

      updateHplaneInfoAfterActivation(hplaneStore[oldHplaneIndex],
          act_ind, s);
    }

  }
}

/**
 * Finds number of points that would be removed by activating this hplane on this split
 */
void newHplaneHelperPointsRemoved(std::vector<int>& numPointsCutByHplane,
    const Hplane& hplane, std::vector<std::vector<int> >& tmpRaysToBeCut,
    std::vector<std::vector<int> >& tmpRaysCutByHplane,
    std::vector<std::vector<int> >& tmpAllRaysIntersected,
//    std::vector<double>& tmpDistToHplane,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    const std::vector<Vertex>& vertexStore,
    const std::vector<Ray>& rayStore,
    const std::vector<Hplane>& hplaneStore, const int oldHplaneIndex,
    const std::vector<IntersectionInfo>& interPtsAndRays,
    const int nb_ray_ind) {
  // Clear data structures
  tmpRaysToBeCut.clear();
  tmpRaysCutByHplane.clear();
  tmpAllRaysIntersected.clear();
  tmpRaysToBeCut.resize(solnInfo.numFeasSplits);
  tmpRaysCutByHplane.resize(solnInfo.numFeasSplits);
  tmpAllRaysIntersected.resize(solnInfo.numFeasSplits);
//  tmpDistToHplane.resize(solnInfo.numFeasSplits, -1.0);

  const int numRaysOfC1 = solnInfo.raysOfC1.size();

  // Check if this is a nb bound
  if (hplane.row == static_cast<int>(HplaneRowFlag::NB)) {
    // The only ray that intersects this nb bound is the ray itself
    // How far to go?
//    const double distToHplane = distanceToHplane(vertexStore[0],
//        rayStore[nb_ray_ind], hplane, solver, solnInfo);
    const double distToHplane = hplane.distanceToHplane(rayStore[nb_ray_ind],
        solver, param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND),
        &(solnInfo.nonBasicVarIndex));
    // If this is infinite, we do not intersect this hyperplane
    // Should not happen here, since both bounds should be finite, since we chose this hplane for activation
    if (isInfinity(distToHplane, solver->getInfinity())) {
      return;
    }

    // For each split, check whether the hplane is intersected before the split boundary
    std::vector<int> splitsAffByHplane;
    for (int split_ind = 0; split_ind < solnInfo.numFeasSplits;
        split_ind++) {

      const double distToSplit = distanceToSplit(vertexStore[0],
          rayStore[nb_ray_ind], solnInfo.feasSplitVar[split_ind],
          solver, solnInfo);

      if (lessThanVal(distToHplane, distToSplit)) {
        tmpAllRaysIntersected[split_ind].push_back(nb_ray_ind);

        // Do not consider rays parallel to the split
        if (isInfinity(distToSplit, solver->getInfinity())) {
          continue;
        }
        // If this ray has already been cut for this split by this hplane, skip it
        if (!rayCutByHplaneForSplit(oldHplaneIndex, hplaneStore,
            nb_ray_ind, rayStore, split_ind)) {
          splitsAffByHplane.push_back(split_ind);
          tmpRaysToBeCut[split_ind].push_back(nb_ray_ind);
          tmpRaysCutByHplane[split_ind].push_back(nb_ray_ind);
//          tmpDistToHplane[split_ind] = distToHplane;
        }
      }
    }

    // Check which points are cut by the hplane for each split
    std::vector<std::vector<int> > tmpPointsToRemove;
    pointsFromRayViolatingHplane(numPointsCutByHplane, tmpPointsToRemove,
        nb_ray_ind, /*vertexStore, rayStore,*/ hplane, interPtsAndRays,
        splitsAffByHplane, solver, solnInfo);

    return;
  }

  // Otherwise this is a basic variable
  double bound = 0.0; // the value if this is a slack
  if (hplane.ubflag == static_cast<int>(HplaneBoundFlag::toLB)) {
    bound = getVarLB(solver, hplane.var);
  } else if (hplane.ubflag == static_cast<int>(HplaneBoundFlag::toUB)) {
    bound = getVarUB(solver, hplane.var);
  }

  if (isInfinity(std::abs(bound), solver->getInfinity())) {
    return;
  }

  // For each ray, find distance to this hyperplane
  for (int ray_ind = 0; ray_ind < numRaysOfC1; ray_ind++) {
    // How far to go?
//    const double distToHplane = distanceToHplane(vertexStore[0],
//        rayStore[ray_ind], hplane, solver, solnInfo);
    const double distToHplane = hplane.distanceToHplane(rayStore[ray_ind],
        solver, param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND),
        &(solnInfo.nonBasicVarIndex));
    // If this is infinite, we do not intersect this hyperplane
    // Should not happen here, since both bounds should be finite, since we chose this hplane for activation
    if (isInfinity(distToHplane, solver->getInfinity())) {
      continue;
    }

    // For each split, check whether the hplane is intersected before the split boundary
    std::vector<int> splitsAffByHplane;
    for (int split_ind = 0; split_ind < solnInfo.numFeasSplits;
        split_ind++) {
      const double distToSplit = distanceToSplit(vertexStore[0],
          rayStore[ray_ind], solnInfo.feasSplitVar[split_ind],
          solver, solnInfo);

      if (lessThanVal(distToHplane, distToSplit)) {
        tmpAllRaysIntersected[split_ind].push_back(ray_ind);

        // Do not consider rays parallel to the split
        if (isInfinity(distToSplit, solver->getInfinity())) {
          continue;
        }
        // If this ray has already been cut for this split by this hplane, skip it
        if (!rayCutByHplaneForSplit(oldHplaneIndex, hplaneStore,
            ray_ind, rayStore, split_ind)) {
          splitsAffByHplane.push_back(split_ind);
          tmpRaysToBeCut[split_ind].push_back(ray_ind);
          tmpRaysCutByHplane[split_ind].push_back(ray_ind);
//          tmpDistToHplane[split_ind] = distToHplane;
        }
      }
    }

    // Check which points are cut by the hplane for each split
    std::vector<std::vector<int> > tmpPointsToRemove;
    pointsFromRayViolatingHplane(numPointsCutByHplane, tmpPointsToRemove,
        ray_ind, /*vertexStore, rayStore,*/ hplane, interPtsAndRays,
        splitsAffByHplane, solver, solnInfo);
  }
}

/**
 * Add one hyperplane per split, where the hyperplane is chosen such that the most number of final points are added
 */
void chooseNewHplanesFinalPoints(std::vector<Hplane>& hplaneStore,
    std::vector<std::vector<int> >& allRaysCut,
    std::vector<std::vector<std::vector<int> > >& raysToBeCut,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    std::vector<Vertex>& vertexStore, // will be modified to add new vertices from activation
    const std::vector<Ray>& rayStore,
    std::vector<IntersectionInfo>& interPtsAndRays, //const AdvCuts& NBSIC,
    const int act_ind) {
  const int numSplits = solnInfo.numFeasSplits;
  const int numRaysOfC1 = solnInfo.raysOfC1.size();
  const char* rowSense = solver->getRowSense();
  std::vector<std::vector<int> > raysToBeCutByHplaneNewAct(numSplits);
  std::vector<std::vector<int> > allRaysIntByHplaneNewAct(numSplits);

  std::vector<std::vector<int> > tmpRaysToBeCut(numSplits); // will be same as tmpRaysCutByHplane
  std::vector<std::vector<int> > tmpRaysCutByHplane(numSplits); // rays that are cut by this hplane
  std::vector<std::vector<int> > tmpAllRaysIntersected(numSplits); // this may include more rays

  // For each potential hyperplane to activate, count how many points would be removed
  // by activating the hyperplane on each split
  std::vector<Hplane> tmpHplaneInfo(numSplits);
  std::vector<int> prevHplaneIndex(numSplits, -1); // Index of hplane if it has been previously activated
  std::vector<int> maxPointsRemoved(numSplits, -1); // Points removed by the hyperplane
  std::vector<int> maxNumFinalPointsCreated(numSplits, -1);
//  std::vector<double> distToHplane(numSplits, -1.0); // Distance to hplane chosen for each split
//  std::vector<double> tmpDistToHplane(numSplits, -1.0); // Distance to hplane chosen for each split

  std::vector<int> AllRayIndices(solnInfo.numNB);
  for (int j = 0; j < solnInfo.numNB; j++) {
    AllRayIndices[j] = j;
  }

  for (int h = 0; h < (int) solnInfo.basicSlackVarIndex.size(); h++) {
    // This is the index of the variable and the row corresponding to the slack variable
    const int currVar = solnInfo.basicSlackVarIndex[h];
    const int rowIndex = solnInfo.rowOfVar[currVar];

    // Equality constraints correspond to slack variables that are basic but have value zero
    // They clearly have zero distance from the optimal solution
    if (rowSense[rowIndex] == 'E') {
      continue;
    }

    Hplane tmpHplane(currVar, rowIndex,
        static_cast<int>(HplaneBoundFlag::toLBSlack));
    const int oldHplaneIndex = hasHplaneBeenActivated(hplaneStore, tmpHplane);

    std::vector<int> tmpNumFinalPointsCreated(numSplits, -1);
    std::vector<int> numPointsCutByHplane(numSplits, -1);
    newHplaneHelperFinalPoints(tmpNumFinalPointsCreated,
        numPointsCutByHplane, tmpHplane, tmpRaysToBeCut,
        tmpRaysCutByHplane, tmpAllRaysIntersected, solver, solnInfo,
        vertexStore, rayStore, hplaneStore,
        oldHplaneIndex, interPtsAndRays/*, AllRayIndices, NBSIC*/);

    for (int split_ind = 0; split_ind < numSplits; split_ind++) {
      if (tmpNumFinalPointsCreated[split_ind] >= 0)
        tmpNumFinalPointsCreated[split_ind]++; // Because we started at -1 to see if none
      bool updateHplaneToAdd = (tmpNumFinalPointsCreated[split_ind]
          > maxNumFinalPointsCreated[split_ind]);
      updateHplaneToAdd = updateHplaneToAdd
          || ((tmpNumFinalPointsCreated[split_ind]
              == maxNumFinalPointsCreated[split_ind])
              && (numPointsCutByHplane[split_ind]
                  > maxPointsRemoved[split_ind]));

      if (updateHplaneToAdd) {
        maxNumFinalPointsCreated[split_ind] =
            tmpNumFinalPointsCreated[split_ind];
        maxPointsRemoved[split_ind] = numPointsCutByHplane[split_ind];
        tmpHplaneInfo[split_ind] = tmpHplane;
        raysToBeCut[act_ind][split_ind] = tmpRaysToBeCut[split_ind];
        raysToBeCutByHplaneNewAct[split_ind] =
            tmpRaysCutByHplane[split_ind];
        allRaysIntByHplaneNewAct[split_ind] =
            tmpAllRaysIntersected[split_ind];
        prevHplaneIndex[split_ind] = oldHplaneIndex;
//        distToHplane[split_ind] = tmpDistToHplane[split_ind];
      }
    }
  }

  if (SECOND_ACTIVATE_BASIC_BOUNDS) {
    // For each structural basic variable, see if one of its bound does better
    for (int v = 0; v < (int) solnInfo.basicOrigVarIndex.size(); v++) {
      // This is the index of the row corresponding to the slack variable
      const int currVar = solnInfo.basicOrigVarIndex[v];
      const int rowIndex = solnInfo.rowOfVar[currVar];

      // Equality constraints do not lead to interesting hyperplanes
      if (rowSense[rowIndex] == 'E') {
        continue;
      }

      // Check lower bound of this variable
      const double LB = getVarLB(solver, currVar);
      if (!isNegInfinity(LB, solver->getInfinity())) {
        Hplane tmpHplane(currVar, rowIndex, static_cast<int>(HplaneBoundFlag::toLB));
        const int oldHplaneIndex = hasHplaneBeenActivated(hplaneStore,
            tmpHplane);

        std::vector<int> tmpNumFinalPointsCreated(numSplits, -1);
        std::vector<int> numPointsCutByHplane(numSplits, -1);
        newHplaneHelperFinalPoints(tmpNumFinalPointsCreated,
            numPointsCutByHplane, tmpHplane, tmpRaysToBeCut,
            tmpRaysCutByHplane, tmpAllRaysIntersected, solver,
            solnInfo, vertexStore, rayStore,
            hplaneStore, oldHplaneIndex, interPtsAndRays/*,
            AllRayIndices, NBSIC*/);

        for (int split_ind = 0; split_ind < numSplits; split_ind++) {
          if (tmpNumFinalPointsCreated[split_ind] >= 0)
            tmpNumFinalPointsCreated[split_ind]++; // Because we started at -1 to see if none
          bool updateHplaneToAdd =
              (tmpNumFinalPointsCreated[split_ind]
                  > maxNumFinalPointsCreated[split_ind]);
          updateHplaneToAdd = updateHplaneToAdd
              || ((tmpNumFinalPointsCreated[split_ind]
                  == maxNumFinalPointsCreated[split_ind])
                  && (numPointsCutByHplane[split_ind]
                      > maxPointsRemoved[split_ind]));

          if (updateHplaneToAdd) {
            maxNumFinalPointsCreated[split_ind] =
                tmpNumFinalPointsCreated[split_ind];
            maxPointsRemoved[split_ind] =
                numPointsCutByHplane[split_ind];
            tmpHplaneInfo[split_ind] = tmpHplane;
            raysToBeCut[act_ind][split_ind] =
                tmpRaysToBeCut[split_ind];
            raysToBeCutByHplaneNewAct[split_ind] =
                tmpRaysCutByHplane[split_ind];
            allRaysIntByHplaneNewAct[split_ind] =
                tmpAllRaysIntersected[split_ind];
            prevHplaneIndex[split_ind] = oldHplaneIndex;
            //        distToHplane[split_ind] = tmpDistToHplane[split_ind];
          }
        }
      }

      // Check upper bound of this variable
      const double UB = getVarUB(solver, currVar);
      if (!isInfinity(UB, solver->getInfinity())) {
        Hplane tmpHplane(currVar, rowIndex, static_cast<int>(HplaneBoundFlag::toUB));
        const int oldHplaneIndex = hasHplaneBeenActivated(hplaneStore,
            tmpHplane);

        std::vector<int> tmpNumFinalPointsCreated(numSplits, -1);
        std::vector<int> numPointsCutByHplane(numSplits, -1);
        newHplaneHelperFinalPoints(tmpNumFinalPointsCreated,
            numPointsCutByHplane, tmpHplane, tmpRaysToBeCut,
            tmpRaysCutByHplane, tmpAllRaysIntersected, solver,
            solnInfo, vertexStore, rayStore,
            hplaneStore, oldHplaneIndex, interPtsAndRays/*,
            AllRayIndices, NBSIC*/);

        for (int split_ind = 0; split_ind < numSplits; split_ind++) {
          if (tmpNumFinalPointsCreated[split_ind] >= 0)
            tmpNumFinalPointsCreated[split_ind]++; // Because we started at -1 to see if none
          bool updateHplaneToAdd =
              (tmpNumFinalPointsCreated[split_ind]
                  > maxNumFinalPointsCreated[split_ind]);
          updateHplaneToAdd = updateHplaneToAdd
              || ((tmpNumFinalPointsCreated[split_ind]
                  == maxNumFinalPointsCreated[split_ind])
                  && (numPointsCutByHplane[split_ind]
                      > maxPointsRemoved[split_ind]));

          if (updateHplaneToAdd) {
            maxNumFinalPointsCreated[split_ind] =
                tmpNumFinalPointsCreated[split_ind];
            maxPointsRemoved[split_ind] =
                numPointsCutByHplane[split_ind];
            tmpHplaneInfo[split_ind] = tmpHplane;
            raysToBeCut[act_ind][split_ind] =
                tmpRaysToBeCut[split_ind];
            raysToBeCutByHplaneNewAct[split_ind] =
                tmpRaysCutByHplane[split_ind];
            allRaysIntByHplaneNewAct[split_ind] =
                tmpAllRaysIntersected[split_ind];
            prevHplaneIndex[split_ind] = oldHplaneIndex;
            //        distToHplane[split_ind] = tmpDistToHplane[split_ind];
          }
        }
      }
    }
  }

  if (SECOND_ACTIVATE_NB_BOUNDS) {
    // Check if one of the bounds of the non-basic columns does better
    for (int ray_ind = 0; ray_ind < solnInfo.numNBOrig; ray_ind++) {
      const int currVar = solnInfo.nonBasicVarIndex[ray_ind];

      // Check that both bounds exist
      const double LB = getVarLB(solver, currVar);
      const double UB = getVarUB(solver, currVar);

      if (!isNegInfinity(LB, solver->getInfinity())
          && !isInfinity(UB, solver->getInfinity())) {
        Hplane tmpHplane(currVar, static_cast<int>(HplaneRowFlag::NB),
            static_cast<int>(HplaneBoundFlag::toUBNB)); // Currently at lb = 0
        const int oldHplaneIndex = hasHplaneBeenActivated(hplaneStore,
            tmpHplane);

        std::vector<int> tmpNumFinalPointsCreated(numSplits, -1);
        std::vector<int> numPointsCutByHplane(numSplits, -1);
        newHplaneHelperFinalPoints(tmpNumFinalPointsCreated,
            numPointsCutByHplane, tmpHplane, tmpRaysToBeCut,
            tmpRaysCutByHplane, tmpAllRaysIntersected, solver,
            solnInfo, vertexStore, rayStore,
            hplaneStore, oldHplaneIndex, interPtsAndRays,
            /*AllRayIndices, NBSIC,*/ ray_ind);

        for (int split_ind = 0; split_ind < numSplits; split_ind++) {
          if (tmpNumFinalPointsCreated[split_ind] >= 0)
            tmpNumFinalPointsCreated[split_ind]++; // Because we started at -1 to see if none
          bool updateHplaneToAdd =
              (tmpNumFinalPointsCreated[split_ind]
                  > maxNumFinalPointsCreated[split_ind]);
          updateHplaneToAdd = updateHplaneToAdd
              || ((tmpNumFinalPointsCreated[split_ind]
                  == maxNumFinalPointsCreated[split_ind])
                  && (numPointsCutByHplane[split_ind]
                      > maxPointsRemoved[split_ind]));

          if (updateHplaneToAdd) {
            maxNumFinalPointsCreated[split_ind] =
                tmpNumFinalPointsCreated[split_ind];
            maxPointsRemoved[split_ind] =
                numPointsCutByHplane[split_ind];
            tmpHplaneInfo[split_ind] = tmpHplane;
            raysToBeCut[act_ind][split_ind] =
                tmpRaysToBeCut[split_ind];
            raysToBeCutByHplaneNewAct[split_ind] =
                tmpRaysCutByHplane[split_ind];
            allRaysIntByHplaneNewAct[split_ind] =
                tmpAllRaysIntersected[split_ind];
            prevHplaneIndex[split_ind] = oldHplaneIndex;
            //        distToHplane[split_ind] = tmpDistToHplane[split_ind];
          }
        }
      }
    }
  }

  // Now update allRaysCut and other properties
  const int numHplanesPrevAct = hplaneStore.size();
  std::vector<bool> allRaysCutFlag(numRaysOfC1, false);
  for (int s = 0; s < numSplits; s++) {
    if (maxNumFinalPointsCreated[s] < 0) 
      maxNumFinalPointsCreated[s] = 0;
#ifdef TRACE
    printf(
        "Hplane (act %d) for split %d removes %d points and adds %d final points. Hplane var index: %d. Hplane row index: %d. Hplane UB flag: %d.\n",
        act_ind, s, maxPointsRemoved[s],
        maxNumFinalPointsCreated[s], tmpHplaneInfo[s].var,
        tmpHplaneInfo[s].row, tmpHplaneInfo[s].ubflag);
#endif
    // If we did not find a new hplane to activate for this split, skip it
    if (tmpHplaneInfo[s].var == static_cast<int>(HplaneVarFlag::none)) {
      continue;
    }

    for (int r = 0; r < (int) raysToBeCut[act_ind][s].size(); r++) {
      if (!allRaysCutFlag[raysToBeCut[act_ind][s][r]]) {
        allRaysCutFlag[raysToBeCut[act_ind][s][r]] = true;
        allRaysCut[act_ind].push_back(raysToBeCut[act_ind][s][r]);
      }
    }

    // It was not activated prior to this round of hyperplanes, but maybe it has been already activated for a different split
    const int oldHplaneIndex =
        (prevHplaneIndex[s] >= 0) ?
            prevHplaneIndex[s] :
            hasHplaneBeenActivated(hplaneStore, tmpHplaneInfo[s],
                numHplanesPrevAct);

    // Now update information on hyperplanes, adding new ones when necessary
    const int numRaysCutByHplaneNewAct =
        raysToBeCutByHplaneNewAct[s].size();
    const int numAllRaysIntByHplaneNewAct =
        allRaysIntByHplaneNewAct[s].size();
    if (oldHplaneIndex == -1) {
      const int numHplanes = hplaneStore.size();
      interPtsAndRays[s].hplaneIndexByAct[act_ind].push_back(
          numHplanes);
      hplaneStore.push_back(tmpHplaneInfo[s]);
      hplaneStore[numHplanes].resizeVectors(numSplits);
      hplaneStore[numHplanes].rayToBeCutByHplane[s] =
          raysToBeCutByHplaneNewAct[s];
      hplaneStore[numHplanes].vertOfRayToBeCutByHplane[s].resize(
          numRaysCutByHplaneNewAct, 0);
      hplaneStore[numHplanes].allRaysIntByHplane[s] =
          allRaysIntByHplaneNewAct[s];
      hplaneStore[numHplanes].vertOfAllRaysIntByHplane[s].resize(
          numAllRaysIntByHplaneNewAct, 0);

      // Calculate new vertex for each ray intersected by this hyperplane
      Vertex newVertex;
      for (int r = 0; r < numRaysCutByHplaneNewAct; r++) {
        // How far to go?
        const int ray_ind = raysToBeCutByHplaneNewAct[s][r];
//        const double distToHplane = distanceToHplane(vertexStore[0],
//            rayStore[ray_ind], hplaneStore[numHplanes], solver,
//            solnInfo);
        const double distToHplane = hplaneStore[numHplanes].distanceToHplane(
            rayStore[ray_ind], solver,
            param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND),
            &(solnInfo.nonBasicVarIndex));
        calcNewVectorCoordinates(newVertex, distToHplane,
            vertexStore[0], rayStore[ray_ind]);

        const int newVertexIndex = addVertex(vertexStore, newVertex);
//            numHplanes);

        hplaneStore[numHplanes].vertCreatedByRayToBeCutByHplane[s].push_back(
            newVertexIndex);
      }

      updateHplaneInfoAfterActivation(hplaneStore[numHplanes], act_ind,
          s);
    } else {
      interPtsAndRays[s].hplaneIndexByAct[act_ind].push_back(
          oldHplaneIndex); // We are activating this hplane a second time
      // The arrays have already been resized correctly
      // And all rays to be intersected will be the same, since this is rank 1
      const int numRaysPrevCutByHplane =
          hplaneStore[oldHplaneIndex].rayToBeCutByHplane[s].size();
//      for (int i = 0; i < act_ind - 1; i++) {
//        numRaysPrevCutByHplane += hplaneStore[oldHplaneIndex].numRaysCutByHplane[s][i];
//      }

      // rayToBeCutByHplane
      hplaneStore[oldHplaneIndex].rayToBeCutByHplane[s].insert(
          hplaneStore[oldHplaneIndex].rayToBeCutByHplane[s].end(),
          raysToBeCutByHplaneNewAct[s].begin(),
          raysToBeCutByHplaneNewAct[s].end());
      hplaneStore[oldHplaneIndex].vertOfRayToBeCutByHplane[s].resize(
          numRaysPrevCutByHplane + numRaysCutByHplaneNewAct, 0);

      // allRaysIntByHplane may not need to be updated (same set of rays still intersected for this split)
      // (if this hplane has already been activated for this split)
      if (hplaneStore[oldHplaneIndex].allRaysIntByHplane[s].size() == 0) {
        hplaneStore[oldHplaneIndex].allRaysIntByHplane[s] =
            allRaysIntByHplaneNewAct[s];
        hplaneStore[oldHplaneIndex].vertOfAllRaysIntByHplane[s].resize(
            numAllRaysIntByHplaneNewAct, 0);
      }

      // Calculate new vertex for each ray intersected by this hyperplane
      Vertex newVertex;
      for (int r = 0; r < numRaysCutByHplaneNewAct; r++) {
        const int ray_ind = raysToBeCutByHplaneNewAct[s][r];
//        const double distToHplane = distanceToHplane(vertexStore[0],
//            rayStore[ray_ind], hplaneStore[oldHplaneIndex], solver,
//            solnInfo);
        const double distToHplane = 
            hplaneStore[oldHplaneIndex].distanceToHplane(rayStore[ray_ind],
                solver, param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND),
                &(solnInfo.nonBasicVarIndex));
        calcNewVectorCoordinates(newVertex, distToHplane,
            vertexStore[0], rayStore[ray_ind]);

        const int newVertexIndex = addVertex(vertexStore, newVertex);
//            oldHplaneIndex);

        hplaneStore[oldHplaneIndex].vertCreatedByRayToBeCutByHplane[s].push_back(
            newVertexIndex);
      }

      updateHplaneInfoAfterActivation(hplaneStore[oldHplaneIndex],
          act_ind, s);
    }

  }
}

/**
 * Finds number of points that would be removed by activating this hplane on this split
 */
void newHplaneHelperFinalPoints(std::vector<int>& numFinalPointsCreatedByHplane,
    std::vector<int>& numPointsCutByHplane, const Hplane& hplane,
    std::vector<std::vector<int> >& tmpRaysToBeCut,
    std::vector<std::vector<int> >& tmpRaysCutByHplane,
    std::vector<std::vector<int> >& tmpAllRaysIntersected,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    const std::vector<Vertex>& vertexStore,
    const std::vector<Ray>& rayStore,
    const std::vector<Hplane>& hplaneStore, const int oldHplaneIndex,
    const std::vector<IntersectionInfo>& interPtsAndRays,
//    const std::vector<int>& allRayIndices, const AdvCuts& NBSIC,
    const int nb_ray_ind) {
  // Clear data structures
  tmpRaysToBeCut.clear();
  tmpRaysCutByHplane.clear();
  tmpAllRaysIntersected.clear();
  tmpRaysToBeCut.resize(solnInfo.numFeasSplits);
  tmpRaysCutByHplane.resize(solnInfo.numFeasSplits);
  tmpAllRaysIntersected.resize(solnInfo.numFeasSplits);
//  tmpDistToHplane.resize(solnInfo.numFeasSplits, -1.0);

  const int numRaysOfC1 = solnInfo.raysOfC1.size();

  // Check if this is a nb bound
  if (hplane.row == static_cast<int>(HplaneRowFlag::NB)) {
    // The only ray that intersects this nb bound is the ray itself
    // How far to go?
//    const double distToHplane = distanceToHplane(vertexStore[0],
//        rayStore[nb_ray_ind], hplane, solver, solnInfo);
    const double distToHplane = hplane.distanceToHplane(rayStore[nb_ray_ind],
        solver, param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND),
        &(solnInfo.nonBasicVarIndex));
    // If this is infinite, we do not intersect this hyperplane
    // Should not happen here, since both bounds should be finite, since we chose this hplane for activation
    if (isInfinity(distToHplane, solver->getInfinity())) {
      return;
    }

    // Set packed vector corresponding to this vertex
    Vertex vertCreated;
    calcNewVectorCoordinates(vertCreated, distToHplane, vertexStore[0],
        rayStore[nb_ray_ind]);

    // For each split, check whether the hplane is intersected before the split boundary
    std::vector<int> splitsAffByHplane;
    for (int split_ind = 0; split_ind < solnInfo.numFeasSplits;
        split_ind++) {

      const double distToSplit = distanceToSplit(vertexStore[0],
          rayStore[nb_ray_ind], solnInfo.feasSplitVar[split_ind],
          solver, solnInfo);

      if (lessThanVal(distToHplane, distToSplit)) {
        tmpAllRaysIntersected[split_ind].push_back(nb_ray_ind);

        // Do not consider rays parallel to the split
        if (isInfinity(distToSplit, solver->getInfinity())) {
          continue;
        }
        // If this ray has already been cut for this split by this hplane, skip it
        if (!rayCutByHplaneForSplit(oldHplaneIndex, hplaneStore,
            nb_ray_ind, rayStore, split_ind)) {
          splitsAffByHplane.push_back(split_ind);
          tmpRaysToBeCut[split_ind].push_back(nb_ray_ind);
          tmpRaysCutByHplane[split_ind].push_back(nb_ray_ind);
//          tmpDistToHplane[split_ind] = distToHplane;

          const std::vector<int> veryTmpRayToBeCut = { nb_ray_ind };
          numFinalPointsCreatedByHplane[split_ind] +=
              fakeActivateFinalPoints(/*distToHplane,*/ solver,
                  solnInfo, split_ind,
                  vertexStore, vertCreated, rayStore,
                  nb_ray_ind, hplane, 
                  /*allRayIndices,*/ &interPtsAndRays[split_ind],
                  veryTmpRayToBeCut);
        }
      }
    }

    // Check which points are cut by the hplane for each split
    std::vector<std::vector<int> > tmpPointsToRemove;
    pointsFromRayViolatingHplane(numPointsCutByHplane, tmpPointsToRemove,
        nb_ray_ind, /*vertexStore, rayStore,*/ hplane, interPtsAndRays,
        splitsAffByHplane, solver, solnInfo);

    return;
  }

  // Otherwise this is a basic variable
  double bound = 0.0;
  if (hplane.ubflag == static_cast<int>(HplaneBoundFlag::toLB)) {
    bound = getVarLB(solver, hplane.var);
  } else if (hplane.ubflag == static_cast<int>(HplaneBoundFlag::toUB)) {
    bound = getVarUB(solver, hplane.var);
  }

  if (isInfinity(std::abs(bound), solver->getInfinity()))
    return;

  // For each ray, find distance to this hyperplane
  for (int ray_ind = 0; ray_ind < numRaysOfC1; ray_ind++) {
    // How far to go?
//    const double distToHplane = distanceToHplane(vertexStore[0],
//        rayStore[ray_ind], hplane, solver, solnInfo);
    const double distToHplane = hplane.distanceToHplane(rayStore[ray_ind],
        solver, param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND),
        &(solnInfo.nonBasicVarIndex));
    // If this is infinite, we do not intersect this hyperplane
    // Should not happen here, since both bounds should be finite, since we chose this hplane for activation
    if (isInfinity(distToHplane, solver->getInfinity())) {
      continue;
    }

    // Set packed vector corresponding to this vertex
    Vertex vertCreated;
    calcNewVectorCoordinates(vertCreated, distToHplane, vertexStore[0],
        rayStore[ray_ind]);

    // For each split, check whether the hplane is intersected before the split boundary
    std::vector<int> splitsAffByHplane;
    for (int split_ind = 0; split_ind < solnInfo.numFeasSplits;
        split_ind++) {
      const double distToSplit = distanceToSplit(vertexStore[0],
          rayStore[ray_ind], solnInfo.feasSplitVar[split_ind],
          solver, solnInfo);

      if (lessThanVal(distToHplane, distToSplit)) {
        tmpAllRaysIntersected[split_ind].push_back(ray_ind);

        // Do not consider rays parallel to the split
        if (isInfinity(distToSplit, solver->getInfinity())) {
          continue;
        }
        // If this ray has already been cut for this split by this hplane, skip it
        if (!rayCutByHplaneForSplit(oldHplaneIndex, hplaneStore,
            ray_ind, rayStore, split_ind)) {
          splitsAffByHplane.push_back(split_ind);
          tmpRaysToBeCut[split_ind].push_back(ray_ind);
          tmpRaysCutByHplane[split_ind].push_back(ray_ind);
//          tmpDistToHplane[split_ind] = distToHplane;

          const std::vector<int> veryTmpRayToBeCut = { ray_ind };
          numFinalPointsCreatedByHplane[split_ind] +=
              fakeActivateFinalPoints(/*distToHplane,*/ solver,
                  solnInfo, split_ind,
                  vertexStore, vertCreated, rayStore, ray_ind,
                  hplane, /*allRayIndices,*/
                  &interPtsAndRays[split_ind],
                  veryTmpRayToBeCut);

        }
      }
    }

    // Check which points are cut by the hplane for each split
    std::vector<std::vector<int> > tmpPointsToRemove;
    pointsFromRayViolatingHplane(numPointsCutByHplane, tmpPointsToRemove,
        ray_ind, /*vertexStore, rayStore,*/ hplane, interPtsAndRays,
        splitsAffByHplane, solver, solnInfo);
  }
}

/**
 * Finds points created from rayInd that are cut by this hyperplane
 */
void pointsFromRayViolatingHplane(std::vector<int>& numPointsCutByHplane,
    std::vector<std::vector<int> >& pointsToRemove, const int rayInd,
//    const std::vector<Vertex>& vertexStore,
//    const std::vector<Ray>& rayStore,
    const Hplane& hplane,
    const std::vector<IntersectionInfo>& interPtsAndRays,
    const std::vector<int> & splitsAffByHplane,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo) {
  const int numSplitsAffected = (int) splitsAffByHplane.size();
  pointsToRemove.clear();
  pointsToRemove.resize(numSplitsAffected);
//  numPointsCutByHplane.resize(numSplitsAffected, 0);
  int numViolPoints = 0;
  for (int s_ind = 0; s_ind < numSplitsAffected; s_ind++) {
    const int s = splitsAffByHplane[s_ind];
    for (int row_ind = 0; row_ind < interPtsAndRays[s].getNumRows();
        row_ind++) {
//    const int numPointsAndRaysByRay =
//        rayStore[rayInd].partitionOfPtsAndRaysByRaysOfC1[s].size();
//    for (int ind = 0; ind < numPointsAndRaysByRay; ind++) {
//      const int curr_ptray_ind =
//          rayStore[rayInd].partitionOfPtsAndRaysByRaysOfC1[s][ind];
      const int ray_ind =
          (interPtsAndRays[s].cutRay[row_ind] >= 0) ?
              interPtsAndRays[s].cutRay[row_ind] :
              interPtsAndRays[s].newRay[row_ind];
      if (ray_ind != rayInd) {
        continue;
      }

      // Ensure this is a point, not a ray
      if (isZero(interPtsAndRays[s].RHS[row_ind])) {
        continue;
      }

      const CoinShallowPackedVector tmpVector =
          interPtsAndRays[s].getVector(row_ind);
      const int numElmts = tmpVector.getNumElements();
      const int* elmtIndices = tmpVector.getIndices();
      const double* elmtVals = tmpVector.getElements();

      const bool NBFlag = (hplane.row == static_cast<int>(HplaneRowFlag::NB));

      // The NBFlag prevents us from using a -1 row index
      const double xVal = !NBFlag * solnInfo.a0[!NBFlag * hplane.row];
      double NBRowActivity = 0.0;
      for (int k = 0; k < numElmts; k++) {
        if (!NBFlag) {
          NBRowActivity +=
              solnInfo.raysOfC1[elmtIndices[k]][hplane.row]
                  * elmtVals[k];
        } else if (elmtIndices[k] == hplane.var) {
          NBRowActivity += elmtVals[k];
        }
      }
      const double newVal = xVal + NBRowActivity;
      double colLB = 0.0, colUB = solver->getInfinity();

      if (hplane.var < solver->getNumCols()) {
        if (!NBFlag) {
          colLB = getVarLB(solver, hplane.var);
          colUB = getVarUB(solver, hplane.var);
        } else {
          colLB = 0.0;
          colUB = getVarUB(solver, hplane.var)
              - getVarLB(solver, hplane.var);
        }
      } 

      if (lessThanVal(newVal, colLB) || greaterThanVal(newVal, colUB)) {
        numViolPoints++;
        pointsToRemove[s_ind].push_back(row_ind);
        if (numPointsCutByHplane[s] == -1) {
          numPointsCutByHplane[s]++;
        }
        numPointsCutByHplane[s]++;
      }
    }
  }
}

/**
 * Calculates numRaysCutByHplane and numAllRaysInt
 * @param hplane
 * @param act_ind :: 0 for first activation, etc.
 * @param split_ind :: split to calculate for
 */
void updateHplaneInfoAfterActivation(Hplane& hplane, const int act_ind,
    const int split_ind) {
  const int totalNumRaysCutByHplane =
      hplane.rayToBeCutByHplane[split_ind].size();

  // Ensure the std::vectors are properly sized
  const int old_size_vec =
      (int) hplane.numRaysCutByHplaneInclPrevAct[split_ind].size();
  const int prevTotal =
      (old_size_vec > 0) ?
          hplane.numRaysCutByHplaneInclPrevAct[split_ind][old_size_vec
              - 1] :
          0;

  if (old_size_vec < act_ind + 1) {
    hplane.numRaysCutByHplaneInclPrevAct[split_ind].resize(act_ind + 1,
        prevTotal);
  }

//  const int prevNumRaysCut =
//      (act_ind == 0) ?
//          0 : hplane.numRaysCutByHplane[split_ind][act_ind - 1];
//  int prevNumRaysCut = 0;
//  for (int i = 0; i < act_ind; i++) {
//    prevNumRaysCut += hplane.numRaysCutByHplane[split_ind][i];
//  }
//  hplane.numRaysCutByHplane[split_ind][act_ind] = totalNumRaysCutByHplane
//      - prevNumRaysCut;
  hplane.numRaysCutByHplaneInclPrevAct[split_ind][act_ind] =
      totalNumRaysCutByHplane;
}

/**
 * Calculates numRaysCutByHplane
 * @param hplane
 * @param act_ind :: 0 for first activation, etc.
 */
void updateHplaneInfoAfterActivation(Hplane& hplane, const int act_ind) {
  for (int split_ind = 0; split_ind < (int) hplane.rayToBeCutByHplane.size();
      split_ind++) {
    updateHplaneInfoAfterActivation(hplane, act_ind, split_ind);
  }
}

/**
 * Calculates numRaysCutByHplane
 * @param hplaneStore
 * @param act_ind :: 0 for first activation, etc.
 */
void updateHplaneInfoAfterActivation(std::vector<Hplane>& hplaneStore,
    const int act_ind) {
  for (int h = 0; h < (int) hplaneStore.size(); h++) {
    updateHplaneInfoAfterActivation(hplaneStore[h], act_ind);
  }
}

int addVertex(std::vector<Vertex>& vertexStore, const Vertex& tmpVertex) {
//    const int newHplaneIndex
  const int oldVertexIndex = hasVertexBeenActivated(vertexStore, tmpVertex);
  const int newVertexIndex =
      (oldVertexIndex == -1) ? vertexStore.size() : oldVertexIndex;
  if (oldVertexIndex == -1) {
    vertexStore.push_back(tmpVertex);
  }
//  vertexStore[newVertexIndex].hplaneIndices.push_back(newHplaneIndex);
  return newVertexIndex;
}

void activateHplane(int& totalNumPointsRemoved, double& avgRemovedDepth,
    int& totalNumPointsAdded, double& avgAddedDepth,
    IntersectionInfo& interPtsAndRays, const int split_ind,
    const int hplane_ind, const int hplane_ind_in_split,
    const std::vector<Hplane>& hplaneStore, const Hplane& hplane,
    std::vector<Ray>& rayStore, const std::vector<Vertex>& vertexStore,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    const AdvCut* NBSIC, const double NBSICnorm, const AdvCut& objCut,
    const double objNorm, const int act_ind) {
  const int split_var = solnInfo.feasSplitVar[split_ind];

  // We we want to track the average depth of the removed intersection points
  double sumRemovedDepth = 0.0, sumAddedDepth = 0.0;
  int numNewPointsRemoved = 0, numNewPointsAdded = 0;

  std::vector<int> newRayColIndices;

  // Mark rays that will be cut by a hyperplane
//  // In PHA 1.1., all the rays for which the first hplane is this one
  std::vector<bool> raysCutFlag(solnInfo.numNB, false);
  const int start_ind = getStartIndex(hplane, split_ind, act_ind);
  const int end_ind = getEndIndex(hplane, split_ind, act_ind);
  for (int j = start_ind; j < end_ind; j++) {
    const int ray_ind = hplane.rayToBeCutByHplane[split_ind][j];
    raysCutFlag[ray_ind] = true;
    rayStore[ray_ind].cutFlag[split_ind] = true;
//    // Removed depth is zero for these points, since they are from C1
//    // However, don't count any rays that have been previously cut
//    if (!rayStore[ray_ind].cutFlag[split_ind]) {
//      rayStore[ray_ind].cutFlag[split_ind] = true;
//      // Only count it if it has not been added;
//      // otherwise the point should be removed in removePtsAndRaysCutByHplane
//      if (!rayStore[ray_ind].addedPointFlag[split_ind])
//        numNewPointsRemoved++;
//    }
  }

  //Remove all intersection points cut off by this hyperplane
  numNewPointsRemoved += removePtsAndRaysCutByHplane(solver, solnInfo,
      raysCutFlag, split_var, hplane, hplane_ind_in_split, vertexStore,
      rayStore, interPtsAndRays, NBSIC, NBSICnorm, sumRemovedDepth);

  // Calculate average depth of the removed points
  if (numNewPointsRemoved > 0) {
    avgRemovedDepth += sumRemovedDepth / numNewPointsRemoved;
    totalNumPointsRemoved += numNewPointsRemoved;
  }

  //For each hyperplane, create the set J \setminus K
  storeNewRayNBIndices(solnInfo.numNB, newRayColIndices,
      hplane.allRaysIntByHplane[split_ind], raysCutFlag);
  const int numNewRays = newRayColIndices.size();

  //For ray intersected by hyperplane H generate new rays corresponding to each element in J \setminus K
  //This is of course available in closed form from the tableau
  for (int j = start_ind; j < end_ind; j++) {
    const int ray_to_cut = hplane.rayToBeCutByHplane[split_ind][j];
    const int vertex_ind = hplane.vertCreatedByRayToBeCutByHplane[split_ind][j];

    for (int r = 0; r < numNewRays; r++) {
      Ray newRay;
      // TODO I think the Point class is not being used actually anywhere, change this to CoinPackedVector
      Point tmpPoint;
      calcNewRayCoordinatesRank1(newRay, /*solver,*/ solnInfo, ray_to_cut,
          newRayColIndices[r], /*rayStore,*/ hplane, param.getRAYEPS());
      bool ptFlagToAdd = calcIntersectionPointWithSplit(tmpPoint, solver,
          solnInfo, vertexStore[vertex_ind], newRay, split_var);

      if (ptFlagToAdd) {
        if (!duplicatePoint(interPtsAndRays, interPtsAndRays.RHS, tmpPoint)) {
          // Check that the point lies on bd S, as it should
          const int split_row_ind = solnInfo.rowOfVar[split_var];
          const double split_val = solnInfo.a0[split_row_ind];
          double newValOnSplit = dotProductWithOptTableau(tmpPoint,
              split_row_ind, solnInfo);
          double newValOnHplane = 0.0;

          bool notOnSplit = (!isVal(newValOnSplit, std::floor(split_val),
              param.getEPS()))
              && (!isVal(newValOnSplit, std::ceil(split_val), param.getEPS()));

          // Check that the point is on the hyperplane it is supposed to lie on
          // If hplane.row < 0, meaning the hplane activate was the "other" bound
          // on a nb variable, then there is no point checking anything;
          // the only nb var that intersects this hplane is the one it bounds,
          // and we force the variable to have the appropriate value (ub - lb)
          bool notOnHplane = false;
          double colLB = 0.0, colUB = solver->getInfinity();
          if (hplane.row >= 0) {
            newValOnHplane = dotProductWithOptTableau(tmpPoint, hplane.row,
                solnInfo);

            // The hplane is either structural or slack
            if (hplane.var < solver->getNumCols()) {
              colLB = getVarLB(solver, hplane.var);
              colUB = getVarUB(solver, hplane.var);
            } else {
              colLB = 0.0;
              colUB = solver->getInfinity();
            }
            notOnHplane = !isVal(newValOnHplane, colLB, param.getEPS())
                && !isVal(newValOnHplane, colUB, param.getEPS());
          }

          if (notOnSplit || notOnHplane) {
            // Sometimes worth redoing things with a higher eps tolerance
            const double prevRAYEPS = param.getRAYEPS();
            param.setRAYEPS(prevRAYEPS * prevRAYEPS);
            calcNewRayCoordinatesRank1(newRay, /*solver,*/ solnInfo, ray_to_cut,
                newRayColIndices[r], /*rayStore,*/ hplane,
                param.getRAYEPS());
            calcIntersectionPointWithSplit(tmpPoint, solver, solnInfo,
                vertexStore[vertex_ind], newRay, split_var);
            newValOnSplit = dotProductWithOptTableau(tmpPoint, split_row_ind,
                solnInfo);
            notOnSplit =
                (!isVal(newValOnSplit, std::floor(split_val), param.getEPS()))
                    && (!isVal(newValOnSplit, std::ceil(split_val),
                        param.getEPS()));
            if (hplane.row >= 0) {
              newValOnHplane = dotProductWithOptTableau(tmpPoint, hplane.row,
                  solnInfo);
              notOnHplane = (!isVal(newValOnHplane, colLB, param.getEPS()))
                  && (!isVal(newValOnHplane, colUB, param.getEPS()));
            }
            param.setRAYEPS(prevRAYEPS);
          }

          if (notOnSplit) {
            error_msg(errstr,
                "Point %s not on bd of split %d (var %d), created by activating var %d (hplane %d). newVal: %s, splitVal: %s.\n",
                tmpPoint.vectorString().c_str(), split_ind, split_var,
                hplane.var, hplane_ind, stringValue(newValOnSplit).c_str(),
                stringValue(split_val).c_str());
            writeErrorToLog(errstr, GlobalVariables::log_file);
            exit(1);
          }
          if (notOnHplane) {
            error_msg(errstr,
                "Basic var %d corresponding to hplane %d activated on split %d yielding pt %s is not on the activated hyperplane. newVal: %s, colLB: %s, colUB: %s.\n",
                hplane.var, hplane_ind, split_ind,
                tmpPoint.vectorString().c_str(),
                stringValue(newValOnHplane).c_str(), stringValue(colLB).c_str(),
                stringValue(colUB).c_str());
            writeErrorToLog(errstr, GlobalVariables::log_file);
            exit(1);
          }

          // Ensure that no ``future'' hyperplane cuts the point
          bool isCut = false;
          for (int h_ind = hplane_ind_in_split;
              h_ind < (int) interPtsAndRays.allHplaneToAct.size(); h_ind++) {
            const int curr_hplane_ind = interPtsAndRays.allHplaneToAct[h_ind];
            // The hyperplane needs to have cut the current ray
            const bool hplaneCutsThisRay =
                (find_val(ray_to_cut,
                    hplaneStore[curr_hplane_ind].rayToBeCutByHplane[split_ind])
                    >= 0);
            if (hplaneCutsThisRay
                && pointIsCutByHplane(solver, solnInfo, tmpPoint, 1.0,
                    hplaneStore[curr_hplane_ind])) {
              isCut = true;
              break;
            }
          }

          if (!isCut) {
            numNewPointsAdded++;
            const bool pointIsFinal = pointInP(solver, solnInfo,
                tmpPoint.getVectorNumElements(), tmpPoint.getVectorIndices(),
                tmpPoint.getVectorElements());

            // Get depth wrt SIC
            const double SICactivity =
            //    (NBSIC) ?
            //        NBSIC->getActivity(tmpPoint) :
                    getSICActivity(split_var, vertexStore, rayStore, solver,
                        solnInfo, tmpPoint);
            double curr_SIC_depth =
                (NBSIC) ? (SICactivity - NBSIC->rhs()) / NBSICnorm : 0.0;
            if (isZero(curr_SIC_depth)) {
              curr_SIC_depth = +0.0;
            } else if (lessThanVal(curr_SIC_depth, 0.0)) {
              error_msg(errstr,
                  "Depth %f is negative! Point cannot be above the SIC.\n",
                  curr_SIC_depth);
              writeErrorToLog(errstr, GlobalVariables::log_file);
              exit(1);
            }

            // Get depth wrt objective
            const double objActivity = objCut.getActivity(tmpPoint);
            double curr_obj_depth = (objActivity - objCut.rhs()) / objNorm;
            if (isZero(curr_obj_depth)) {
              curr_obj_depth = +0.0;
            }
            // interPtsAndRays.objDepth.push_back(curr_obj_depth); // this caused the objective depth to be improperly calculated

            interPtsAndRays.addPointOrRayToIntersectionInfo(tmpPoint, 1.0,
                ray_to_cut, newRayColIndices[r], hplane_ind,
                hplane_ind_in_split, vertex_ind, -1, curr_SIC_depth,
                curr_obj_depth, pointIsFinal);

            sumAddedDepth += curr_SIC_depth;
          }
        }
      } else if (!CHECK_FOR_DUP_RAYS
          || !duplicateRay(interPtsAndRays, interPtsAndRays.RHS, newRay)) {
        // Ensure that no ``future'' hyperplane cuts the point
        bool isCut = false;
        for (int h_ind = hplane_ind_in_split;
            h_ind < (int) interPtsAndRays.allHplaneToAct.size(); h_ind++) {
          const int curr_hplane_ind = interPtsAndRays.allHplaneToAct[h_ind];
          // The hyperplane needs to have cut the current ray
          const bool hplaneCutsThisRay = (find_val(ray_to_cut,
              hplaneStore[curr_hplane_ind].rayToBeCutByHplane[split_ind]) >= 0);
          if (hplaneCutsThisRay
              && pointIsCutByHplane(solver, solnInfo, newRay, 0.0,
                  hplaneStore[curr_hplane_ind])) {
            isCut = true;
            break;
          }
        }
        if (!isCut) {
          interPtsAndRays.addPointOrRayToIntersectionInfo(newRay, 0.0,
              ray_to_cut, newRayColIndices[r], hplane_ind, hplane_ind_in_split,
              vertex_ind, -1, 0.0, 0.0, false);
        }
      }
    } /* end iterating over new rays to be created */
  } /* end iterating over rays intersected by hplane in this activation */

  if (numNewPointsAdded > 0) {
    avgAddedDepth += sumAddedDepth / numNewPointsAdded;
    totalNumPointsAdded += numNewPointsAdded;
  }
} /* activateHplane */

int removePtsAndRaysCutByHplane(const PointCutsSolverInterface* const solver,
    const SolutionInfo &solnInfo, const std::vector<bool> &raysCutFlag,
    const int split_var, const Hplane& hplane, const int hplane_ind_in_split,
//    const int hplane_ind, const std::vector<Hplane>& hplaneStore,
    const std::vector<Vertex>& vertexStore, const std::vector<Ray>& rayStore,
    IntersectionInfo& interPtsAndRays, const AdvCut* NBSIC, const double norm,
    double& sumRemovedDepth, const bool fake_act) {
  std::vector<int> delIndices;

  const double* UB = solver->getColUpper();
  const double* LB = solver->getColLower();

  // Only remove points that correspond to rays that are cut by this hplane
  // Also the points should not be those created by hplanes activated "later"
  for (int row_ind = 0; row_ind < interPtsAndRays.getNumRows(); row_ind++) {
    const int curr_hplane_ind_in_split =
        interPtsAndRays.hplaneIndexForSplit[row_ind];
    if (curr_hplane_ind_in_split >= hplane_ind_in_split) {
      continue;
    }

    const int ray_ind =
        (interPtsAndRays.cutRay[row_ind] >= 0) ?
            interPtsAndRays.cutRay[row_ind] :
            interPtsAndRays.newRay[row_ind];
    if (!raysCutFlag[ray_ind]) {
      continue;
    }
    const bool currRowIsRay = isZero(interPtsAndRays.RHS[row_ind]);

    double SIC_activity = +0.0;
    //This if condition ensures that intersection points are removed only
    //if they emanate from a vertex of a ray that is cut
    // In PHA 1.1, this should never be true, since we never cut a ray twice

    //      if ((*intPtOrRayInfo[r]).cutRayColIndx >= 0) { // Uncomment this if we want to remove all points violating hplane
    const CoinShallowPackedVector tmpVector = interPtsAndRays.getVector(
        row_ind);
    const int numElmts = tmpVector.getNumElements();
    const int* elmtIndices = tmpVector.getIndices();
    const double* elmtVals = tmpVector.getElements();
    double activity = 0.0;
    for (int e = 0; e < numElmts; e++) {
      // Either hplaneRowIndx >= 0, in which case we activated a basic var
      // or it is -1, and we activated an upper or lower bound of a nb var
      if (hplane.row >= 0) {
        activity += elmtVals[e]
            * solnInfo.raysOfC1[elmtIndices[e]][hplane.row];
      } else {
        const int nb_ind = (-1 - solnInfo.rowOfVar[hplane.var]);
        if (elmtIndices[e] == nb_ind)
          activity = elmtVals[e];
      }
//      SIC_activity += NBSIC[elmtIndices[e]] * elmtVals[e];
      // NBSIC dist recalculated here to avoid some numerical issues from taking inverses
      SIC_activity += elmtVals[e]
          / distanceToSplit(vertexStore[0], rayStore[elmtIndices[e]],
              split_var, solver, solnInfo);
    }

    // Note that if hplaneRowIndx == -1, then we activated a bound of a nb var,
    // and it can't be a bound of a slack variable, since they are at zero in the cobasis,
    // and that is their only bound.
    if (hplane.var >= solnInfo.numCols) {
      // Recall that all slack variables are non-negative in this
      // setting, since they were complemented in SolutionInfo
      if (!currRowIsRay
          && lessThanVal(solnInfo.a0[hplane.row] + activity, 0.0)) {
        delIndices.push_back(row_ind);
      } else if (currRowIsRay && lessThanVal(activity, 0.0)) {
        delIndices.push_back(row_ind);
      }
    } else {
      // If this was a basic variable, no bounds were changed
      // Otherwise things may have shifted
      double lb, ub, newValue;
      if (hplane.row >= 0) {
        lb = LB[hplane.var];
        ub = UB[hplane.var];
        newValue = solnInfo.a0[hplane.row] + activity;
      } else {
        lb = 0.0;
        ub = UB[hplane.var] - LB[hplane.var];
        newValue = activity;
      }
      if (hplane.ubflag == static_cast<int>(HplaneBoundFlag::toUB)) {
        if (!currRowIsRay && greaterThanVal(newValue, ub)) {
          delIndices.push_back(row_ind);
        } else if (currRowIsRay && greaterThanVal(activity, 0.0)) {
          delIndices.push_back(row_ind);
        }
      } else {
        if (!currRowIsRay && lessThanVal(newValue, lb)) {
          delIndices.push_back(row_ind);
        } else if (currRowIsRay && lessThanVal(activity, 0.0)) {
          delIndices.push_back(row_ind);
        }
      }
    }

    // If we are deleting this point, then count it in deleted depth calculations
    if (!currRowIsRay && (delIndices.size() > 0)
        && (delIndices[delIndices.size() - 1] == row_ind)) {
      double curr_normed_viol = (NBSIC) ? (SIC_activity - NBSIC->rhs()) / norm : 0.0;
      if (isZero(curr_normed_viol)) {
        curr_normed_viol = +0.0;
      }
      sumRemovedDepth += curr_normed_viol;
      //        cout << "sumRemoved" << ": " << sumRemovedDepth << endl;

      if (lessThanVal(curr_normed_viol, 0.0)) {
        error_msg(errstr,
            "Depth %f is negative! Point cannot be above the SIC.\n",
            curr_normed_viol);
        writeErrorToLog(errstr, GlobalVariables::log_file);
        exit(1);
      }

    }
  }

  if (!fake_act) {
    // Sort deleted indices
    sort(delIndices.begin(), delIndices.end());
    interPtsAndRays.deleteRowInfo(delIndices.size(), &delIndices[0]);
  }
  return delIndices.size();
} /* removePtsAndRaysCutByHplane */

bool pointIsCutByHplane(const PointCutsSolverInterface* const solver,
    const SolutionInfo &solnInfo, const CoinPackedVector& row,
    const double RHS, const Hplane& hplane) {
  const double* UB = solver->getColUpper();
  const double* LB = solver->getColLower();

  const bool currRowIsRay = isZero(RHS);

  const int numElmts = row.getNumElements();
  const int* elmtIndices = row.getIndices();
  const double* elmtVals = row.getElements();
  double activity = 0.0;
  for (int e = 0; e < numElmts; e++) {
    // Either hplaneRowIndx >= 0, in which case we activated a basic var
    // or it is -1, and we activated an upper or lower bound of a nb var
    if (hplane.row >= 0) {
      activity += elmtVals[e] * solnInfo.raysOfC1[elmtIndices[e]][hplane.row];
    } else {
      const int nb_ind = (-1 - solnInfo.rowOfVar[hplane.var]);
      if (elmtIndices[e] == nb_ind)
        activity = elmtVals[e];
    }
  }

  // Note that if hplaneRowIndx == -1, then we activated a bound of a nb var,
  // and it can't be a bound of a slack variable, since they are at zero in the cobasis,
  // and that is their only bound.
  if (hplane.var >= solnInfo.numCols) {
    // Recall that all slack variables are non-negative in this
    // setting, since they were complemented in SolutionInfo
    if (!currRowIsRay && lessThanVal(solnInfo.a0[hplane.row] + activity, 0.0)) {
      return true;
    } else if (currRowIsRay && lessThanVal(activity, 0.0)) {
      return true;
    }
  } else {
    // If this was a basic variable, no bounds were changed
    // Otherwise things may have shifted
    double lb, ub, newValue;
    if (hplane.row >= 0) {
      lb = LB[hplane.var];
      ub = UB[hplane.var];
      newValue = solnInfo.a0[hplane.row] + activity;
    } else {
      lb = 0.0;
      ub = UB[hplane.var] - LB[hplane.var];
      newValue = activity;
    }
    if (hplane.ubflag == static_cast<int>(HplaneBoundFlag::toUB)) {
      if (!currRowIsRay && greaterThanVal(newValue, ub)) {
        return true;
      } else if (currRowIsRay && greaterThanVal(activity, 0.0)) {
        return true;
      }
    } else {
      if (!currRowIsRay && lessThanVal(newValue, lb)) {
        return true;
      } else if (currRowIsRay && lessThanVal(activity, 0.0)) {
        return true;
      }
    }
  }
  return false;
}

void printActivatedHplaneDetails(FILE* fptr,
    const SolutionInfo& solnInfo,
    const std::vector<Hplane>& hplaneStore,
    const std::vector<IntersectionInfo>& interPtsAndRays,
    const int act_ind) {
  fprintf(fptr,
      "This file contains details on the partially activated hyperplanes. UB flag: -1 = hplane not a bound. 0 = lower-bound. 1 = upper-bound.\n\n");
  fprintf(fptr,
      "(UBlag = -1 implies the hyperplane is structural and not a variable bound)\n\n");
  fprintf(fptr,
      "SplitVarIndex,No,hplaneVarIndex,hplaneRowIndex,UBFlag,RaysToBeCut...\n");
  for (int s = 0; s < solnInfo.numFeasSplits; s++) {
    for (unsigned h = 0;
        h < interPtsAndRays[s].hplaneIndexByAct[act_ind].size();
        h++) {
      const int hplane_ind =
          interPtsAndRays[s].hplaneIndexByAct[act_ind][h];
      fprintf(fptr, "%d,%d,%d,%d,%d,", solnInfo.feasSplitVar[s], h,
          hplaneStore[hplane_ind].var, hplaneStore[hplane_ind].row,
          hplaneStore[hplane_ind].ubflag);

      const int start_ind = getStartIndex(hplaneStore[hplane_ind], s,
          act_ind);
      const int end_ind = getEndIndex(hplaneStore[hplane_ind], s, act_ind);
      for (int r = start_ind; r < end_ind; r++) {
        fprintf(fptr, "%d,",
            hplaneStore[hplane_ind].rayToBeCutByHplane[s][r]);
      }
      fprintf(fptr, "\n");
    }
  }

  fprintf(fptr, "\nAll rays to be cut\n");
  for (int s = 0; s < solnInfo.numFeasSplits; s++) {
    for (unsigned h = 0;
        h < interPtsAndRays[s].hplaneIndexByAct[act_ind].size();
        h++) {
      const int hplane_ind =
          interPtsAndRays[s].hplaneIndexByAct[act_ind][h];
      fprintf(fptr, "%d,%d,%d,%d,%d,", solnInfo.feasSplitVar[s], h,
          hplaneStore[hplane_ind].var, hplaneStore[hplane_ind].row,
          hplaneStore[hplane_ind].ubflag);

      const int numAllRaysIntByHplane =
          hplaneStore[hplane_ind].allRaysIntByHplane[s].size();
      for (int r = 0; r < numAllRaysIntByHplane; r++) {
        fprintf(fptr, "%d,",
            hplaneStore[hplane_ind].allRaysIntByHplane[s][r]);
      }
      fprintf(fptr, "\n");
    }
  }
} /* printActivatedHplaneDetails */

//void activateHplaneOnRay(const LiftGICsSolverInterface* const solver,
//    const SolutionInfo& solnInfo, const Vertex vertex, const Ray rayOut,
//    const Hplane& hplane, const int split_ind) {
//  // We we want to track the average depth of the removed intersection points
//  double sumRemovedDepth = 0.0, sumAddedDepth = 0.0;
//  int numNewPointsRemoved = 0, numNewPointsAdded = 0;
//
//  std::vector<double> tempPt(2, 0.0);
//  std::vector<int> tempElmt(2, 0);
//  std::vector<int> delIndices, newRayColIndices;
//
//  //Mark rays that will be cut by a hyperplane
//  std::vector<bool> raysCutFlag(solnInfo.numNonBasicCols, false);
//  // In PHA 1.1., there should only be one ray cut by the hplane
//  // (actually, no, all the rays for which the first hplane is this one)
//  for (unsigned j = 0; j < hplane.raysToBeCutByHplane.size(); j++) {
//    raysCutFlag[raysToBeCutByHplane[j]] = true;
//    // Removed depth is zero for these points, since they are from C1
//    numNewPointsRemoved++;
//  }
//
//  //Remove all intersection points cut off by this hyperplane
//  numNewPointsRemoved += removePtsAndRaysCutByHplane(solver, solnInfo,
//      raysCutFlag, hplaneVarRowIndex, hplaneVarIndex, hplaneUBFlag,
//      delIndices, *interPtsAndRays, pointOrRayFlag, numPointsOrRays,
//      intPtOrRayInfo, NBSIC, norm, sumRemovedDepth,
//      partitionOfPtsAndRaysByRaysOfC1, inst_info_out);
//
//  // Calculate average depth of the removed points
////      cout << "sumRemoved" << s << ": " << sumRemovedDepth << endl;
//  if (numNewPointsRemoved > 0) {
//    avgRemovedDepth += sumRemovedDepth / numNewPointsRemoved;
//    totalNumPointsRemoved += numNewPointsRemoved;
//  }
//
//  //For each hyperplane, create the set J \setminus K
//  storeNewRayColIndices(solnInfo.numNonBasicCols, newRayColIndices,
//      allRaysCutByHplane, raysCutFlag);
//
//  //For ray intersected by hyperplane H generate new rays corresponding to each element in J \setminus K
//  //This is of course available in closed form from the tableau
//  for (unsigned j = 0; j < raysToBeCutByHplane.size(); j++) {
//    numNewPointsAdded += computeIntersectionPointsWithSplit(solver,
//        chooseSplits.splitVarRowIndex[splitIndex], hplaneIndexInSplit,
//        hplaneVarIndex, hplaneVarRowIndex, hplaneUBFlag,
//        chooseSplits.splitVarValue[splitIndex],
//        chooseSplits.pi0[splitIndex], raysToBeCutByHplane[j],
//        solnInfo.nonBasicVarIndex[raysToBeCutByHplane[j]],
//        newRayColIndices, partitionOfPtsAndRaysByRaysOfC1,
//        solnInfo.raysOfC1, solnInfo.a0, tempElmt, tempPt,
//        *interPtsAndRays, pointOrRayFlag, numPointsOrRays,
//        intPtOrRayInfo, NBSIC, norm, sumAddedDepth, inst_info_out);
//  }
//  if (numNewPointsAdded > 0) {
//    avgAddedDepth += sumAddedDepth / numNewPointsAdded;
//    totalNumPointsAdded += numNewPointsAdded;
//  }
//
//  // Ensure the average removed depth < average added depth
//  if (!SECOND_ACT
//      && (sumAddedDepth / numNewPointsAdded
//          - sumRemovedDepth / numNewPointsRemoved) < -param.getEPS()) {
//    error_msg(errstr,
//        "Average added depth less than average removed depth:\n" "\tsumAddedDepth: %f, numAddedPoints: %d\n" "\tsumRemovedDepth: %f, numRemovedPoints: %d\n" "\tavgAddedDepth: %f, avgRemovedDepth: %f\n",
//        sumAddedDepth, numNewPointsAdded, sumRemovedDepth,
//        numNewPointsRemoved, sumAddedDepth / numNewPointsAdded,
//        sumRemovedDepth / numNewPointsRemoved);
//    writeErrorToII(errstr, inst_info_out);
//    exit(1);
//  }
//}
