// Last edit: 01/10/2015
//
// Name:     CglVPC.hpp
// Author:   A. M. Kazachkov (based on template by Francois Margot)
//           Tepper School of Business
// Date:     01/10/2015
//-----------------------------------------------------------------------------

#pragma once

#include "CglGICParam.hpp"
#include "CglCutGenerator.hpp"
#include "CoinWarmStartBasis.hpp"
#include "CoinFactorization.hpp"

// (the AdvCut and AdvCuts classes could probably be removed eventually,
// but they have some useful features wrt OsiCuts)
#include "AdvCut.hpp"
#include "IntersectionInfo.hpp"
#include "Hplane.hpp"
#include "Point.hpp"
#include "Ray.hpp"
#include "Vertex.hpp"
#include "Stats.hpp"

/* Debug output */
//#define NEWGEN_TRACE
/* Debug output: print optimal tableau */
//#define NEWGEN_TRACETAB
class CglVPC: public CglCutGenerator {
  friend void CglVPCUnitTest(const OsiSolverInterface * siP,
      const std::string mpdDir);
public:

  /**@name generateCuts */
  //@{
  /** Generate cuts for the model of the solver
   interface si.

   Insert the generated cuts into OsiCuts cs.
   */
  virtual void generateCuts(const OsiSolverInterface & si, OsiCuts & cs,
      const SolutionInfo& probData, const AdvCuts& structSICs,
      const CglTreeInfo info = CglTreeInfo());

  virtual void generateCuts(const OsiSolverInterface & si, OsiCuts & cs,
      const CglTreeInfo info = CglTreeInfo());

  /// For compatibility with CglCutGenerator (const method)
  virtual void generateCuts(const OsiSolverInterface & si, OsiCuts & cs,
      const CglTreeInfo info = CglTreeInfo()) const;

  /// Return true if needs optimal basis to do cuts (will return true)
  virtual bool needsOptimalBasis() const;
  //@}

  /**@name Public Methods */
  //@{
  // Set the parameters to the values of the given CglGMIParam object.
  void setParam(const CglGICParam &source);
  // Return the CglGMIParam object of the generator. 
  inline CglGICParam getParam() const {
    return param;
  }
  inline int getNumObjTried() const {
    return num_obj_tried;
  }
  inline double getStrongBranchingLB() const {
    return this->strong_branching_lb;
  }
  inline double getStrongBranchingUB() const {
    return this->strong_branching_ub;
  }
  inline const OsiCuts& getStructSICs() const {
    return structSICs;
  }
  inline const Stats& getTimer() const {
    return vpcTimeStats;
  }

  //@}

  /**@name Constructors and destructors */
  //@{
  /// Default constructor 
  CglVPC();

  // Constructor with specified parameters
  CglVPC(const CglGICParam&);

  /// Copy constructor 
  CglVPC(const CglVPC &);

  /// Clone
  virtual CglCutGenerator * clone() const;

  /// Assignment operator 
  CglVPC & operator=(const CglVPC& rhs);

  /// Destructor 
  virtual ~CglVPC();
  /// Create C++ lines to get to current state
  virtual std::string generateCpp(FILE * fp);

  /// Copy our stuff
  virtual void copyOurStuff(const CglVPC* const rhs = NULL);

  //@}

private:

  // Private member methods

  /**@name Private member methods */

  //@{
  // Method generating the cuts after all CglVPC members are properly set.
  void generateCutsFromPartialBB(AdvCuts& structVPCs,
      const AdvCuts& structSICs);
  void generateCutsFromSplitsOrCrosses(AdvCuts& structVPCs, const SolutionInfo& probData,
      const AdvCuts& structSICs);
  void setupCgs(const SolutionInfo& probData, int triangle_ind,
//      const int num_cgs,
      const int max_num_cgs, const int num_splits_per_cgs,
      const std::vector<int>& sortSplitIndex,
      const std::vector<std::vector<bool> >& splitFeasOnSide,
      const std::vector<double>& splitStrongBranchingValue,
      const std::vector<double>& splitHigherBound,
      const std::vector<int>& splitHigherBoundFacet,
      const double objNormSquared, std::vector<int>& sortCgsIndex,
      std::vector<double>& distToCgs, const bool calcDistByMaxFractionality,
      const bool calcDistByStrongBranching,
//      std::vector<std::vector<int> >& splitIndexAllCgs,
      std::vector<int>& triangleIndexAllCgs, std::vector<int>& currFracVar);
  void setCgsName(std::string& cgsName, const std::string& disjTermName);
  void setCgsName(std::string& cgsName, const int num_ineq_per_disj,
      const std::vector<std::vector<int> >& termIndices,
      const std::vector<std::vector<double> >& termCoeff,
      const std::vector<double>& termRHS, const bool append = false);
  void generateCutsHelper(AdvCuts& structVPCs, const SolutionInfo& probData,
      Stats& pcutTimeStats, const AdvCuts &structSICs,
      std::vector<IntersectionInfo>& interPtsAndRays, const int num_disj_terms_cgs,
      int& num_fpcs_per_cgs_T1, int& num_generated_fpcsT1,
      int& num_fpcs_per_cgs_T2, int& num_generated_fpcsT2,
      const AdvCut* const objCut,
      const std::vector<std::vector<std::vector<int> > >& termIndices,
      const std::vector<std::vector<std::vector<double> > >& termCoefficients,
      const std::vector<std::vector<double> >& termRHS,
      std::vector<bool>& disjTermFeas, std::vector<double>& disjTermObjVal,
//      const std::vector<std::vector<std::vector<int> > >& splitFacetLocation,
      const int cgs_ind, const int unsorted_cgs_ind, const std::string& cgsName,
      const int num_cgs, const int max_num_cgs, const int dim,
      const bool inNBSpace);
  void generateVPCsT1(AdvCuts &structVPCs, const SolutionInfo& probData,
//      std::vector<std::vector<int> >& tmpVarBasicInRow,
//      std::vector<std::vector<int> >& tmpBasicStructVar,
//      std::vector<std::vector<int> >& tmpBasicSlackVar,
//      std::vector<std::vector<int> >& rowOfTmpVar,
//      std::vector<std::vector<int> >& rowOfOrigNBVar,
//      std::vector<std::vector<int> >& tmpNonBasicVarIndex,
//      std::vector<std::vector<std::vector<double> > >& currBInvACol,
//      std::vector<std::vector<std::vector<double> > >& currRay,
      Stats & pcutTimeStats, const AdvCuts& structSICs,
      std::vector<IntersectionInfo>& interPtsAndRays, int& num_fpcs_per_cgs_T1,
      int& num_generated_fpcsT1, const AdvCut* const objCut,
//      const std::vector<std::vector<std::vector<int> > >& termIndices,
//      const std::vector<std::vector<std::vector<double> > >& termCoefficients,
//      const std::vector<std::vector<double> >& termRHS,
      const std::vector<bool>& calcAndFeasFacet, const int cgs_ind,
      const int unsorted_cgs_ind, const std::string& cgsName,
//      const int num_cgs,
      const int max_num_cgs, const int dim, const bool inNBSpace);
  void generateVPCsT2(AdvCuts& structVPCs, const SolutionInfo& probData,
      const std::vector<PointCutsSolverInterface*>& tmpSolver,
      std::vector<std::vector<int> >& tmpVarBasicInRow,
      std::vector<std::vector<int> >& tmpBasicStructVar,
      std::vector<std::vector<int> >& tmpBasicSlackVar,
      std::vector<std::vector<int> >& rowOfTmpVar,
      std::vector<std::vector<int> >& rowOfOrigNBVar,
      std::vector<std::vector<int> >& tmpNonBasicVarIndex,
      std::vector<std::vector<std::vector<double> > >& currBInvACol,
      std::vector<std::vector<std::vector<double> > >& currRay,
      Stats& pcutTimeStats, const AdvCuts &structSICs,
      const std::vector<IntersectionInfo>& interPtsAndRays, int& num_fpcs_per_cgs_T2,
      int& num_generated_fpcsT2, const AdvCut* const objCut,
      const std::vector<std::vector<std::vector<int> > >& termIndices,
      const std::vector<std::vector<std::vector<double> > >& termCoefficients,
      const std::vector<std::vector<double> >& termRHS,
      const std::vector<bool>& calcAndFeasFacet, const int cgs_ind,
      const int unsorted_cgs_ind, const std::string& cgsName, const int num_cgs,
      const int max_num_cgs, const int dim, const bool inNBSpace);

  // Point generation
  void genDepth1PRCollection(IntersectionInfo& interPtsAndRays,
      const PointCutsSolverInterface* tmpSolver,
      const SolutionInfo& origProbData,
      const std::vector<std::vector<int> >& termIndices,
      const std::vector<int>& varBasicInRow,
      const std::vector<int>& basicStructVar,
      const std::vector<int>& basicSlackVar,
      const std::vector<int>& rowOfVar,
      const std::vector<int>& rowOfOrigNBVar,
      const std::vector<int>& nonBasicVarIndex,
      const std::vector<std::vector<double> >& currRay,
      const int f, const bool splitVarDeleted,
      const int cgs_ind, const int num_cgs);
  void genCorner(IntersectionInfo& interPtsAndRays, const int split_var,
      const PointCutsSolverInterface* const tmpSolver, const int facet_ind,
      const std::vector<int>& basicStructVar, const std::vector<int>& rowOfVar,
      const std::vector<int>& rowOfOrigNBVar,
      const std::vector<int>& nonBasicVarIndex,
      const std::vector<std::vector<double> >& currRay,
      const bool splitVarDeleted);
  void genCornerNB(IntersectionInfo& interPtsAndRays, const int split_var,
      const PointCutsSolverInterface* const tmpSolver,
      const SolutionInfo & origProbData, const int facet_ind,
      const std::vector<int>& basicStructVar, const std::vector<int>& rowOfVar,
      const std::vector<int>& rowOfOrigNBVar,
      const std::vector<int>& nonBasicVarIndex,
      const std::vector<std::vector<double> >& currRay, const bool splitVarDeleted);

  // DEPTH 2
  int pivotHelper(IntersectionInfo& currInterPtsAndRays,
      const PointCutsSolverInterface* const & pivotSolver,
      std::vector<int>& currTmpVarBasicInRow,
      std::vector<int>& currTmpBasicStructVar,
      std::vector<int>& currTmpBasicSlackVar, std::vector<int>& currRowOfTmpVar,
      std::vector<int>& currRowOfOrigNBVar,
      std::vector<int>& currTmpNonBasicVarIndex,
      std::vector<std::vector<double> >& currCurrBInvACol,
      std::vector<std::vector<double> >& currCurrRay, const int hplaneTmpVar,
      const int hplaneTmpRow, const IntersectionInfo& interPtsAndRays,
      const PointCutsSolverInterface* const tmpSolver,
      const SolutionInfo& probData, const int facet_ind, const int ray_ind,
      const int currFracVar, const bool splitVarDeleted);
  void pivotHelper(IntersectionInfo& currInterPtsAndRays,
      PointCutsSolverInterface*& currTmpSolver,
      std::vector<int>& currTmpVarBasicInRow,
      std::vector<int>& currTmpBasicStructVar,
      std::vector<int>& currTmpBasicSlackVar,
      std::vector<int>& currRowOfTmpStructVar,
      std::vector<int>& currRowOfOrigNBVar,
      std::vector<int>& currTmpNonBasicVarIndex,
      std::vector<std::vector<double> >& currCurrBInvACol,
      std::vector<std::vector<double> >& currCurrRay, int& hplaneTmpVar,
      int& hplaneTmpRow, int& currRayTmpVar, int& outStatus,
      const IntersectionInfo& interPtsAndRays,
      const PointCutsSolverInterface* const tmpSolver,
      const SolutionInfo& probData, const int facet_ind, const int ray_ind,
      const int currFirstHplaneIndex, const int currFracVar,
      const std::vector<std::vector<double> >& currRay,
      const std::vector<Hplane>& hplanesToAct,
      const std::vector<int>& tmpNonBasicVarIndex, const bool splitVarDeleted);
  void chooseHplanesDepth2VPCs(std::vector<Hplane>& hplanesToAct,
      std::vector<int>& firstHplane,
      std::vector<std::vector<double> >& distToHplane,
      const PointCutsSolverInterface* const tmpSolver,
      const SolutionInfo& origProbData, const std::vector<int>& varBasicInRow,
      const std::vector<int>& basicStructVar,
      const std::vector<int>& basicSlackVar, const std::vector<int>& rowOfVar,
      const std::vector<int>& rowOfOrigNBVar,
      const std::vector<int>& nonBasicVarIndex,
      const std::vector<std::vector<double> >& currRay, const int facet_ind,
      const int split_ind, const int split_var, const bool splitVarDeleted);

  /**********************************************************/
  /**
   * Should we instead be using Rays obtained from IntersectionInfo
   * (i.e., dot products of CoinPackedVectors)
   * Is that more efficient?
   */
  bool findHplaneToIntersectRayFirst(int& firstHplaneVar,
      const int rayVarStructIndex,
      const PointCutsSolverInterface* const tmpSolver,
      const SolutionInfo& origProbData, const std::vector<double>& currRay,
      const std::vector<int>& varBasicInRow, std::vector<Hplane>& hplanesToAct,
      const int splitVar, const bool splitVarDeleted);
  void activateHplane(IntersectionInfo& interPtsAndRays,
      const std::vector<double>& distToHplane,
      const PointCutsSolverInterface* const tmpSolver,
      const SolutionInfo& origProbData, const Hplane& hplane,
      const int hplane_ind, const std::vector<int>& rowOfTmpStructVar,
      const std::vector<int>& rowOfOrigNBVar,
      const std::vector<int>& tmpNonBasicVarIndex,
      const std::vector<std::vector<double> >& currBInvACol,
      const std::vector<std::vector<double> >& currRay, const int splitVar,
      const double splitVal, const int facet_ind, const bool splitVarDeleted);
  void calcDepth2Point(Point& newPoint,
      const PointCutsSolverInterface* const tmpSolver,
      const SolutionInfo& origProbData, const int rayTmpStructIndex,
      const std::vector<double>& currRay, const double minDist,
      const std::vector<int>& rowOfTmpStructVar,
      const std::vector<int>& rowOfOrigNBVar, const int splitVar,
      const double splitVal, const bool splitVarDeleted);
  void calcDepth2Ray(Ray& newRay,
      const PointCutsSolverInterface* const tmpSolver,
      const SolutionInfo& origProbData, const int hplane_row,
      const int rayTmpStructIndex, const int rayTmpNBIndex,
      const int newRayTmpStructIndex, const int newRayTmpNBIndex,
      const std::vector<std::vector<double> >& currBInvACol,
      const std::vector<int>& rowOfTmpStructVar,
      const std::vector<int>& rowOfOrigNBVar, const int splitVar,
      const bool splitVarDeleted);

  void analyzePRCollection(std::vector<IntersectionInfo>& interPtsAndRays,
      const AdvCut* const objCut, const std::vector<bool>& calcAndFeasFacet);
  void checkPRCollectionForFeasibility(const int term_ind,
      const IntersectionInfo& interPtsAndRays, const double nb_obj_val,
      const AdvCut* const objCut);

  // Generate cuts
  void tryObjectivesForCgs(AdvCuts& structVPCs, int& num_cuts_per_cgs,
      int& num_cuts_generated, int& num_obj_tried, const SolutionInfo& probData,
      const int dim, const bool inNBSpace, const int cgs_ind,
      const std::string& cgsName,
      const std::vector<IntersectionInfo>& interPtsAndRays,
      const SolutionInfo& origProbData, const AdvCuts& structSICs,
      Stats& timeStats, const std::string timeName,
      const bool fixFirstPoint, const AdvCut* objCut = NULL);

  void identifyCobasis(std::vector<int>& varBasicInRow,
      std::vector<int>& basicStructVar, std::vector<int>& basicSlackVar,
      std::vector<int>& rowOfVar, std::vector<int>& rowOfOrigNBVar,
      std::vector<int>& nonBasicVarIndex,
      std::vector<std::vector<double> >& currBInvACol,
      std::vector<std::vector<double> >& currRay,
      const PointCutsSolverInterface* const tmpSolver,
      const SolutionInfo& origProbData, const int splitVar,
      const bool splitVarDeleted, const bool calcRays);

  // Find pivots
  void findPivots(std::vector<int>& varOut, std::vector<int>& dirnOut,
      std::vector<int>& pivotRow, std::vector<double>& dist,
      std::vector<double>& obj, const std::vector<int>& tmpNonBasicVarIndex,
      const OsiClpSolverInterface* const tmpSolver,
      const std::vector<int>& varBasicInRow, const int split_var,
      const bool splitVarDeleted);
  void findPivots(std::vector<OsiClpSolverInterface*>& copySolver,
      std::vector<int>& varOut, std::vector<int>& dirnOut,
      std::vector<int>& pivotRow, std::vector<double>& dist,
      const std::vector<int>& tmpNonBasicVarIndex,
      const OsiClpSolverInterface* const tmpSolver,
      const std::vector<int>& varBasicInRow, const int split_var,
      const bool splitVarDeleted);
  void findPivots(std::vector<OsiClpSolverInterface*>& copySolver,
      std::vector<int>& varOut, std::vector<int>& dirnOut,
      std::vector<int>& pivotRow, std::vector<double>& dist,
      std::vector<int>& varOutNonDegen, std::vector<int>& dirnOutNonDegen,
      std::vector<int>& pivotRowNonDegen, std::vector<double>& distNonDegen,
      const std::vector<int>& tmpNonBasicVarIndex,
      const OsiClpSolverInterface* const tmpSolver,
      const std::vector<std::vector<double> >& rayVal,
      const std::vector<int>& varBasicInRow, const int split_var,
      const bool splitVarDeleted);
  double manualPivot(const int varIn,
      const PointCutsSolverInterface* const tmpSolver,
      const std::vector<double>& currRay, const std::vector<int>& varBasicInRow,
      const bool onlyNonDegenPivot);
  double manualPivot(int& varOut_manual, int& dirnOut_manual,
      int& pivotRow_manual, double& valueOut_manual, const int varIn,
      const PointCutsSolverInterface* const tmpSolver,
      const std::vector<double>& currRay, const std::vector<int>& varBasicInRow,
      const bool onlyNonDegenPivot);

  // The following are simple to derive formulas
  // They are used for deriving which are the splits involved in a cgs
  inline int formulaForLastCgsGivenFirstSplit(const int index_at_zero_split_ind,
      const int num_splits, const int num_triangles) const {
    const int split_ind = index_at_zero_split_ind + 1;
    return num_triangles
        * (int) (num_splits * split_ind - 0.5 * split_ind * (split_ind + 1)) - 1;
  }

  inline int formulaForFirstSplitIndex(const int index_at_zero_cgs_ind,
      const int num_splits, const int num_triangles) const {
    const int cgs_ind = index_at_zero_cgs_ind + 1;
    return std::ceil(
        0.5
            * (2 * num_splits - 1
                - std::sqrt(
                    (2 * num_splits - 1) * (2 * num_splits - 1)
                        - 8. * cgs_ind / num_triangles))) - 1;
  }

  inline int formulaForSecondSplitIndex(const int index_at_zero_cgs_ind,
      const int num_splits, const int num_triangles,
      const int providedFirstSplit = -1) const {
    const int firstSplit =
        (providedFirstSplit >= 0) ?
            providedFirstSplit :
            formulaForFirstSplitIndex(index_at_zero_cgs_ind, num_splits,
                num_triangles);
    return firstSplit
        + std::ceil(
            (index_at_zero_cgs_ind
                - 1.
                    * formulaForLastCgsGivenFirstSplit(firstSplit - 1,
                        num_splits, num_triangles)) / num_triangles);
  }

  inline int formulaForSplitIndex(const int index_at_zero_split_ind,
      const int index_at_zero_cgs_ind, const int num_splits,
      const int num_triangles, const int providedFirstSplit = -1) const {
    if (num_triangles > 0) {
      return
          (index_at_zero_split_ind == 0) ?
              formulaForFirstSplitIndex(index_at_zero_cgs_ind, num_splits,
                  num_triangles) :
              formulaForSecondSplitIndex(index_at_zero_cgs_ind, num_splits,
                  num_triangles, providedFirstSplit);
    } else {
      return index_at_zero_cgs_ind;
    }
  }

//  inline int formulaForTriangleIndex(const int s, const int x, const int n,
//        const int num_triangles) const {
//      return
//          (s == 0) ?
//              formulaForFirstSplitIndex(x, n, num_triangles) :
//              formulaForSecondSplitIndex(x, n, num_triangles);
//    }

  // Generate SICs
  void generateSICs(const OsiSolverInterface* si, AdvCuts& structSICs, SolutionInfo& solnInfo);

  /// print a vector of integers
  void printvecINT(const char *vecstr, const int *x, int n) const;
  /// print a vector of doubles: dense form
  void printvecDBL(const char *vecstr, const double *x, int n) const;
  /// print a vector of doubles: sparse form
  void printvecDBL(const char *vecstr, const double *elem, const int * index,
      int nz) const;

  //@}

  // Private member data

  /**@name Private member data */

  //@{
  //
  /// Object with CglGICParam members. 
  CglGICParam param;

  /// Pointer on solver. Reset by each call to generateCuts().
  OsiSolverInterface *solver;

  /// Pointer to cast verson of solver
  PointCutsSolverInterface* castsolver;

  /// Pointer on point to separate. Reset by each call to generateCuts().
  const double *xlp;

  /// Pointer on matrix of coefficient ordered by rows. 
  /// Reset by each call to generateCuts().
  const CoinPackedMatrix *byRow;

  /// Pointer on matrix of coefficient ordered by columns. 
  /// Reset by each call to generateCuts().
  const CoinPackedMatrix *byCol;

  // AdvCuts versions of cuts we generate
  AdvCuts nb_advcs, struct_advcs;

  // If SICs are not provided, assume there are none
  AdvCuts structSICs;

  // Number of objectives tried
  int num_obj_tried;

  // Strong branching value
  double strong_branching_lb;
  double strong_branching_ub;
  double min_nb_obj_val;

  // For time keeping
  Stats vpcTimeStats;

  const std::string time_T1 = "TIME_TYPE1_";
  const std::string time_T2 = "TIME_TYPE2_";

  //@}
};

//#############################################################################
/** A function that tests the methods in the CglVPC class. The
 only reason for it not to be a member method is that this way it doesn't
 have to be compiled into the library. And that's a gain, because the
 library should be compiled with optimization on, but this method should be
 compiled with debugging. */
void CglVPCUnitTest(const OsiSolverInterface * siP, const std::string mpdDir);
