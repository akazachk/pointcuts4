// Name:     CglVPC.cpp
// Author:   A. M. Kazachkov (based on template by Francois Margot)
//           Tepper School of Business
// Date:     01/10/2015
//-----------------------------------------------------------------------------

#if defined(_MSC_VER)
// Turn off compiler warning about long names
#  pragma warning(disable:4786)
#endif
#include "CglVPC.hpp"

#include <cstdlib>
#include <cstdio>
#include <cmath> // abs, floor, ceil
#include <cfloat>
#include <cassert>
#include <iostream>
#include <fenv.h>
#include <climits>

#include "OsiSolverInterface.hpp"

#include "CoinHelperFunctions.hpp"
#include "CoinPackedVector.hpp"
#include "CoinPackedMatrix.hpp"
#include "CoinIndexedVector.hpp"
#include "OsiRowCutDebugger.hpp"
#include "CoinFactorization.hpp"
#include "CoinFinite.hpp"

#include "GlobalConstants.hpp"

#include "Utility.hpp"
#include "CutHelper.hpp"

// Useful classes and such for VPCs
#include "SolutionInfo.hpp"
#include "Output.hpp"
#include "CglSIC.hpp"
#include "CglGMI.hpp"

// For the B&B solving
#include "CbcModel.hpp"
#include "CbcSolver.hpp"
#include "BBHelper.hpp"
#include "VPCEventHandler.hpp"
#include "OsiBranchingObject.hpp"
#include "CbcTree.hpp"
#include "CbcSimpleInteger.hpp"

/***************************************************************************/
CglVPC::CglVPC() :
    CglCutGenerator(), solver(NULL), castsolver(NULL), xlp(NULL), byRow(NULL), 
    byCol(NULL), nb_advcs(true), struct_advcs(false), structSICs(false) { 
  copyOurStuff();
  param.setParams();
} /* constructor */

/***************************************************************************/
CglVPC::CglVPC(const CglGICParam& parameters) :
    CglCutGenerator(), param(parameters), solver(NULL), castsolver(NULL), xlp(NULL), byRow(NULL), 
    byCol(NULL), nb_advcs(true), struct_advcs(false), structSICs(false) {
  copyOurStuff();
} /* constructor copying params */

/***************************************************************************/
CglVPC::CglVPC(const CglVPC& rhs) :
    CglCutGenerator(rhs) {
  copyOurStuff(&rhs);
} /* copy constructor */

/***************************************************************************/
CglVPC & CglVPC::operator=(const CglVPC& rhs) {
  if (this != &rhs) {
    CglCutGenerator::operator=(rhs);
    copyOurStuff(&rhs);
  }
  return *this;
} /* assignment operator */

/***************************************************************************/
CglVPC::~CglVPC() {} /* destructor */

/*********************************************************************/
CglCutGenerator *
CglVPC::clone() const {
  return new CglVPC(*this);
} /* clone */

void CglVPC::copyOurStuff(const CglVPC* const rhs) {
  if (rhs) {
    this->param = rhs->param;
    this->solver = rhs->solver;
    this->castsolver = rhs->castsolver;
    this->xlp = rhs->xlp;
    this->byRow = rhs->byRow;
    this->byCol = rhs->byCol;
    this->nb_advcs = rhs->nb_advcs;
    this->struct_advcs = rhs->struct_advcs;
    this->structSICs = rhs->structSICs;
    this->num_obj_tried = rhs->num_obj_tried;
    this->strong_branching_lb = rhs->strong_branching_lb;
    this->strong_branching_ub = rhs->strong_branching_ub;
    this->min_nb_obj_val = rhs->min_nb_obj_val;
  } else {
    this->num_obj_tried = 0;
    this->strong_branching_lb = std::numeric_limits<double>::max();
    this->strong_branching_ub = std::numeric_limits<double>::lowest();
    this->min_nb_obj_val = std::numeric_limits<double>::max();
  }
} /* copyOurStuff */

/************************************************************************/
void CglVPC::generateCuts(const OsiSolverInterface & si, OsiCuts & cs,
    const CglTreeInfo info) const {
  // kludge to be able to modify the CglVPC object if it is const
  CglVPC temp(*this);
  temp.generateCuts(si, cs, info);
} /* generateCuts */

/************************************************************************/
void CglVPC::generateCuts(const OsiSolverInterface &si, OsiCuts & cs,
    const CglTreeInfo info) {
  // Create SolutionInfo
  solver = const_cast<OsiSolverInterface *>(&si); // Removes const-ness
  SolutionInfo tmpProbData(solver,
      param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND),
      param.getParamVal(ParamIndices::SUBSPACE_PARAM_IND), true);
  if (tmpProbData.numSplits == 0) {
    return;
  }
  if ((param.getParamVal(ParamIndices::CGS_PARAM_IND) > 0)
      && (tmpProbData.numSplits < 2)) {
    return;
  }
  if (!param.finalizeParams(tmpProbData.numNB, tmpProbData.numSplits)) {
    error_msg(errstr,
        "Issue finalizing parameters (no rays to cut or issue with parameter combinations).\n");
    writeErrorToLog(errstr, GlobalVariables::log_file);
    exit(1);
  }
  if (param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND)
      > tmpProbData.numFeasSplits) {
    warning_msg(warnstring,
        "Number of requested cut generating sets is %d, but the maximum number we can choose is %d. Reducing the requested number of splits.\n",
        param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND),
        tmpProbData.numFeasSplits);
    param.paramVal[ParamIndices::NUM_CGS_PARAM_IND] = tmpProbData.numFeasSplits;
  }

  if (param.getParamVal(ParamIndices::SICS_PARAM_IND)) {
    generateSICs(&si, structSICs, tmpProbData);
  }
  //GlobalVariables::param = this->param;
  generateCuts(si, cs, tmpProbData, structSICs, info);
} /* generateCuts */

/************************************************************************/
void CglVPC::generateCuts(const OsiSolverInterface &si, OsiCuts & cs,
    const SolutionInfo& origProbData, const AdvCuts& structSICs,
    const CglTreeInfo info) {
  solver = const_cast<OsiSolverInterface *>(&si); // Removes const-ness
  if (solver == NULL) {
    printf("### WARNING: CglVPC::generateCuts(): no solver available.\n");
    return;
  }

  if (!solver->optimalBasisIsAvailable()) {
    printf(
        "### WARNING: CglVPC::generateCuts(): no optimal basis available.\n");
    return;
  }

  xlp = solver->getColSolution();

  if (param.getParamVal(ParamIndices::VPC_DEPTH_PARAM_IND) > 0) {
    try {
      generateCutsFromSplitsOrCrosses(dynamic_cast<AdvCuts&>(cs), origProbData,
          structSICs);
      this->struct_advcs = dynamic_cast<AdvCuts&>(cs);
    } catch (std::exception& e) {
//      warning_msg(errorstr,
//          "Not able to cast OsiCuts as AdvCuts, used to keep extra information.\n");

      generateCutsFromSplitsOrCrosses(this->struct_advcs, origProbData,
          structSICs);
      cs = struct_advcs;
    }
  } else {
    try {
      generateCutsFromPartialBB(dynamic_cast<AdvCuts&>(cs), structSICs);
      this->struct_advcs = dynamic_cast<AdvCuts&>(cs);
    } catch (std::exception& e) {
      warning_msg(errorstr,
          "Not able to cast OsiCuts as AdvCuts, used to keep extra information.\n");

      generateCutsFromPartialBB(this->struct_advcs, structSICs);
      cs = struct_advcs;
    }
  }
} /* generateCuts */

/************************************************************************/
/*
 * Get cuts from a partial branch-and-bound tree
 */
void CglVPC::generateCutsFromPartialBB(AdvCuts& structVPCs,
    const AdvCuts& structSICs) {
  // Update params because we are in the special case of branch-and-bound
  // Only done locally so as to not ruin the parameters which may be used for other cut-generation techniques
  param.setParamVal(ParamIndices::NUM_CGS_PARAM_IND, 1);
  param.setParamVal(ParamIndices::CGS_PARAM_IND, 0); // when BB < 0, we can ignore CGS_PARAM_IND
  param.setParamVal(ParamIndices::MAX_FRAC_VAR_PARAM_IND, 0);

  // Set up time keeping
  vpcTimeStats.register_name(time_T1 + "TOTAL");
  vpcTimeStats.register_name(time_T2 + "TOTAL");

  const bool isTimeLimitReachedT1 = reachedTimeLimit(vpcTimeStats,
      time_T1 + "TOTAL", param.getTIMELIMIT());
  const bool isTimeLimitReachedT2 = reachedTimeLimit(vpcTimeStats,
      time_T2 + "TOTAL", param.getTIMELIMIT());
  const bool isTimeLimitReached =
      (param.getParamVal(ParamIndices::VPC_DEPTH_PARAM_IND) >= 2) ?
          isTimeLimitReachedT1 && isTimeLimitReachedT2 : isTimeLimitReachedT1;
  if (isTimeLimitReached) { // make sure that time is available
    return;
  }

#ifdef TRACE
  printf("\n############### Starting VPC generation from partial branch-and-bound tree. ###############\n");
#endif

  // Set up solver
  PointCutsSolverInterface* BBSolver;
  BBSolver = dynamic_cast<PointCutsSolverInterface*>(solver->clone());
  setupClpForCbc(BBSolver);

#ifdef TRACE
  printf("\n## Generating partial branch-and-bound tree. ##\n");
#endif
  CbcModel* cbc_model = new CbcModel;
  cbc_model->swapSolver(BBSolver);
  cbc_model->setModelOwnsSolver(true); // solver will be deleted with cbc object
  setMessageHandler(cbc_model);
  vpcTimeStats.register_name("GEN_TREE_FOR_VPC");
  vpcTimeStats.start_timer("GEN_TREE_FOR_VPC");
  int num_strong = param.getParamVal(ParamIndices::PARTIAL_BB_NUM_STRONG_PARAM_IND);
  if (num_strong == -1) {
    num_strong = solver->getNumCols();
  }
  if (num_strong == -2) {
    num_strong = static_cast<int>(std::ceil(std::sqrt(solver->getNumCols())));
  }
  const int num_before_trusted = std::numeric_limits<int>::max(); // 10;
  generatePartialBBTree(cbc_model, solver,
      -1 * param.getParamVal(ParamIndices::VPC_DEPTH_PARAM_IND), num_strong,
      num_before_trusted);
  vpcTimeStats.end_timer("GEN_TREE_FOR_VPC");
  
  GlobalVariables::numPartialBBNodes = cbc_model->getNodeCount(); // save number of nodes looked at

  // Do stuff with the branch and bound tree
  VPCEventHandler* eventHandler;
  try {
    eventHandler = dynamic_cast<VPCEventHandler*>(cbc_model->getEventHandler());
  } catch (std::exception& e) {
    error_msg(errstr,
        "Could not get event handler.\n");
    writeErrorToLog(errstr, GlobalVariables::log_file);
    exit(1);
  }

  std::vector<NodeStatistics> stats = eventHandler->getStatsVector();
#ifdef TRACE
  printNodeStatistics(stats, false);
  if (eventHandler->getPrunedStatsVector().size() > 0) {
    printf("\n");
    printNodeStatistics(eventHandler->getPrunedStatsVector(), false);
  }
#endif

#ifdef TRACE
  if (std::abs(param.getTEMP()) >= 10 && std::abs(param.getTEMP()) <= 15) {
    generateTikzTreeString(eventHandler,
        param.getParamVal(ParamIndices::PARTIAL_BB_STRATEGY_PARAM_IND),
        solver->getObjValue(), true);
    if (std::abs(param.getTEMP()) == 14) {
      // Free
      if (cbc_model) {
        delete cbc_model;
        cbc_model = NULL;
      }
      if (castsolver) {
        delete castsolver;
        castsolver = NULL;
      }
      return;
    }
    if (std::abs(param.getTEMP()) == 15) {
      exit(1); // this is during debug and does not free memory
    }
  }
#endif

  const int num_disj_terms_cgs = eventHandler->getNumLeafNodes() + eventHandler->isIntegerSolutionFound();
  GlobalVariables::numPrunedNodes =
      eventHandler->getPrunedStatsVector().size()
          - eventHandler->isIntegerSolutionFound();
  if (cbc_model->status() == 0 || cbc_model->status() == 1
      || cbc_model->status() == 2 || num_disj_terms_cgs <= 1) {
    if (cbc_model->isProvenOptimal()) {
      warning_msg(warnstr,
          "An integer (optimal) solution was found prior while branching. "
          "We will generate between n and 2n cuts, restricting the value of each variable.\n");
      const double* solution = cbc_model->getColSolution();
      for (int col = 0; col < cbc_model->getNumCols(); col++) {
        const double val = solution[col];

        // Check which of the bounds needs to be fixed
        for (int b = 0; b < 2; b++) {
          if ((b == 0 && greaterThanVal(val, solver->getColLower()[col]))
              || (b == 1 && lessThanVal(val, solver->getColUpper()[col]))) {
            const double mult = (b == 0) ? 1. : -1.;
            const double el = mult * 1.;

            AdvCut currCut(false, 1);
            currCut.cutHeur = CutHeuristics::ONE_SIDED_CUT_HEUR;
            currCut.splitVarIndex[0] = col;
            currCut.setLb(mult * val);
            currCut.setRow(1, &col, &el, false);
            GlobalVariables::numCutsFromHeur[CutHeuristics::ONE_SIDED_CUT_HEUR]++;
            structVPCs.addNonDuplicateCut(currCut, false);
          }
        }
      }

      structVPCs.setOsiCuts();

      // Set strong branching lb and ub to both be this value
      this->strong_branching_lb = cbc_model->getObjValue();
      this->strong_branching_ub = cbc_model->getObjValue();
    } else {
      warning_msg(warnstr,
          "Giving up on getting cuts from the partial branch-and-bound tree (bad status or too few terms). Model status is %d.\n",
          cbc_model->status());
    }

    // Free
    if (cbc_model) {
      delete cbc_model;
      cbc_model = NULL;
    }
    if (castsolver) {
      delete castsolver;
      castsolver = NULL;
    }
    return;
  } /* exit out early if cbc_model status is 0 or insufficiently many disjunctive terms */

  castsolver = dynamic_cast<OsiClpSolverInterface*>(this->solver->clone());
  castsolver->disableFactorization();
  const int num_cgs = 1;
  const int max_num_cgs = 1;
  int num_generated_vpcsT1 = 0;
  int num_generated_vpcsT2 = 0;
  std::vector<int> num_vpcs_per_cgs_T1(num_cgs, 0);
  std::vector<int> num_vpcs_per_cgs_T2(num_cgs, 0);

  /***********************************************************************************
   * Change initial bounds
   ***********************************************************************************/
  const int num_changed_bounds = stats[0].changed_var.size();
  int num_fixed = 0;
  for (int i = 0; i < num_changed_bounds; i++) {
    const int col = stats[0].changed_var[i];
    if (stats[0].changed_bound[i] <= 0) {
      castsolver->setColLower(col, stats[0].changed_value[i]);
    } else {
      castsolver->setColUpper(col, stats[0].changed_value[i]);
    }

    AdvCut currCut(false, 1);
    currCut.cutHeur = CutHeuristics::ONE_SIDED_CUT_HEUR;
    currCut.splitVarIndex[0] = col;
    const double mult = (stats[0].changed_bound[i] <= 0) ? 1. : -1.;
    const double el = mult * 1.;
    currCut.setLb(mult * stats[0].changed_value[i]);
    currCut.setRow(1, &col, &el, false);
    GlobalVariables::numCutsFromHeur[CutHeuristics::ONE_SIDED_CUT_HEUR]++;
    structVPCs.addNonDuplicateCut(currCut, false);
    num_generated_vpcsT1++;
    num_vpcs_per_cgs_T1[0]++;

    if (isVal(castsolver->getColLower()[col], castsolver->getColUpper()[col])) {
      num_fixed++;
    }
  }
#ifdef TRACE
  printf(
      "\n## Total number changed bounds: %d. Number fixed: %d. Number fixed in course of BB: %d. ##\n",
      num_changed_bounds, num_fixed, cbc_model->strongInfo()[1]);
#endif

  castsolver->resolve();

  if (!checkSolverOptimality(castsolver, false)) {
    error_msg(errorstring, "Solver not proven optimal after updating bounds from root node.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  // Create new SolutionInfo TODO make sure this is correct
  SolutionInfo probData(castsolver, 0,
      param.getParamVal(ParamIndices::SUBSPACE_PARAM_IND), true);
  if (!param.finalizeParams(probData.numNB, probData.numSplits)) {
    error_msg(errstr,
        "Issue finalizing parameters (no rays to cut, no splits available, or issue with parameter combinations).\n");
    writeErrorToLog(errstr, GlobalVariables::log_file);
    exit(1);
  }

  // Decide if we are in the non-basic space, based on option
  const bool inNBSpace = param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND);
  const int dim = inNBSpace ? probData.numNB : solver->getNumCols();

  /***********************************************************************************
   * Save objective
   ***********************************************************************************/
  // In order to calculate how good the points we find are
  // with respect to the objective function (which should be in NB space when appropriate)
  // Clearly a valid inequality is c^T x \ge c^\T v,
  // where v is the LP optimum, since we are minimizing
  // Either we have the objective, or we have the reduced costs
  // of the non-basic variables (which is exactly the objective
  // in the non-basic space).
  // Recall that the reduced cost of slack variables is exactly
  // the negative of the row price (dual variable) for that row.
  AdvCut structObj(false, 1);
  AdvCut NBObj(true, 1);
  structObj.setOsiRowCut(std::vector<int>(), castsolver->getNumCols(),
      castsolver->getObjCoefficients(), castsolver->getObjValue(),
      CoinMin(probData.EPS, param.getEPS()));
  NBObj.setOsiRowCut(probData.nonBasicReducedCost, 0.0,
      CoinMin(probData.EPS, param.getEPS()));
  AdvCut* objCut = inNBSpace ? &NBObj : &structObj;

  /***********************************************************************************
   * Set up intersection point and ray storage
   ***********************************************************************************/
  std::vector<std::vector<std::vector<int> > > termIndices(num_disj_terms_cgs);
  std::vector<std::vector<std::vector<double> > > termCoeff(
      num_disj_terms_cgs);
  std::vector<std::vector<double> > termRHS(num_disj_terms_cgs);
  std::vector<IntersectionInfo> interPtsAndRays(num_disj_terms_cgs);  // one per disjunctive term
  for (int t = 0; t < num_disj_terms_cgs; t++) {
    // Reverse ordering so packed matrix is row ordered and set number of columns
    interPtsAndRays[t].reverseOrdering();
    interPtsAndRays[t].setDimensions(0, dim);
    interPtsAndRays[t].resizeInfoByActivationRound(1);
  }

  /***********************************************************************************
   * Get bases and generate VPCs
   ***********************************************************************************/
  std::vector<std::vector<int> > tmpVarBasicInRow(num_disj_terms_cgs);
  std::vector<std::vector<int> > tmpBasicStructVar(num_disj_terms_cgs);
  std::vector<std::vector<int> > tmpBasicSlackVar(num_disj_terms_cgs);
  std::vector<std::vector<int> > rowOfTmpVar(num_disj_terms_cgs);
  std::vector<std::vector<int> > rowOfOrigNBVar(num_disj_terms_cgs);
  std::vector<std::vector<int> > tmpNonBasicVarIndex(num_disj_terms_cgs);
  std::vector<std::vector<std::vector<double> > > currBInvACol(num_disj_terms_cgs);
  std::vector<std::vector<std::vector<double> > > currRay(num_disj_terms_cgs);

  int term_ind = -1;
  std::vector<std::string> cgsName(1);
  std::vector<bool> calcAndFeasFacet(num_disj_terms_cgs);
  this->strong_branching_lb = std::numeric_limits<double>::max();
  this->strong_branching_ub = std::numeric_limits<double>::lowest();
  this->min_nb_obj_val = std::numeric_limits<double>::max();

  // Start with the integer-feasible solution
  if (eventHandler->isIntegerSolutionFound()) {
    term_ind++;

    // Get solution and calculate slacks
    const double* sol = eventHandler->getIntegerFeasibleSolution();
    std::vector<double> slack(probData.numRows, 0.);
    const CoinPackedMatrix* mx = solver->getMatrixByRow();
    mx->times(sol, &slack[0]);
    for (int row_ind = 0; row_ind < probData.numRows; row_ind++) {
      slack[row_ind] = solver->getRightHandSide()[row_ind] - slack[row_ind];
    }

    // Update the point
    Point point(probData.numCols);
    point.setCompNBCoor(sol, slack.data(), castsolver, probData);
    const double curr_nb_obj_val = point.getObjViolation(); //getNBObjValue(sol, sol + probData.numCols, castsolver, probData);
    const double beta =
        param.getParamVal(ParamIndices::FLIP_BETA_PARAM_IND) >= 0 ? 1.0 : -1.0;
    interPtsAndRays[term_ind].addPointOrRayToIntersectionInfo(point, beta, 0, curr_nb_obj_val);
#ifdef TRACE
    printf("\n## Saving integer feasible solution as term %d. ##\n", term_ind);
#endif
    calcAndFeasFacet[term_ind] = true;
    std::string tmpname = "feasSol" + std::to_string(term_ind);
    setCgsName(cgsName[0], tmpname);
    double objOffset = 0.;
    castsolver->getDblParam(OsiDblParam::OsiObjOffset, objOffset);
    const double objVal = dotProduct(castsolver->getObjCoefficients(), sol, probData.numCols) - objOffset;
    if (objVal < this->strong_branching_lb) {
      this->strong_branching_lb = objVal;
    }
    if (objVal > this->strong_branching_ub) {
      this->strong_branching_ub = objVal;
    }
    if (curr_nb_obj_val < this->min_nb_obj_val) {
      this->min_nb_obj_val = curr_nb_obj_val;
    }
  } /* integer-feasible solution */
  
  // Now we handle the normal terms
  for (int tmp_ind = 0; tmp_ind < eventHandler->getNumNodesOnTree(); tmp_ind++) {
//    printf("\n## DEBUG: Tmp index: %d ##\n", tmp_ind);
    const int node_id = eventHandler->getNodeIndex(tmp_ind);
    const int orig_node_id = stats[node_id].orig_id;

    PointCutsSolverInterface* tmpSolverBase =
      dynamic_cast<PointCutsSolverInterface*>(castsolver->clone());
    tmpSolverBase->disableFactorization();

    // Change bounds in the solver
    const int curr_num_changed_bounds = (orig_node_id == 0) ? 0 : stats[orig_node_id].changed_var.size();
    std::vector<std::vector<int> > commonTermIndices(curr_num_changed_bounds);
    std::vector<std::vector<double> > commonTermCoeff(curr_num_changed_bounds);
    std::vector<double> commonTermRHS(curr_num_changed_bounds);
    for (int i = 0; i < curr_num_changed_bounds; i++) {
      const int col = stats[orig_node_id].changed_var[i];
      const double coeff = (stats[orig_node_id].changed_bound[i] <= 0) ? 1. : -1.;
      const double val = stats[orig_node_id].changed_value[i];
      commonTermIndices[i].resize(1, col);
      commonTermCoeff[i].resize(1, coeff);
      commonTermRHS[i] = coeff * val;
//      tmpSolverBase->addRow(1, &col, &coeff, rhs,
//          tmpSolverBase->getInfinity());
      if (stats[orig_node_id].changed_bound[i] <= 0) {
        tmpSolverBase->setColLower(col, val);
      } else {
        tmpSolverBase->setColUpper(col, val);
      }
    }

    // Set the warm start
    if (!(tmpSolverBase->setWarmStart(eventHandler->getBasisForNode(tmp_ind)))) {
      error_msg(errorstring,
          "Warm start information not accepted for node %d.\n", node_id);
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }

    // Resolve
#ifdef TRACE
    printf("\n## Solving for parent node %d/%d. ##\n", tmp_ind + 1,
        eventHandler->getNumNodesOnTree());
#endif
    tmpSolverBase->resolve();
    if (!checkSolverOptimality(tmpSolverBase, false)) {
      error_msg(errorstring,
          "Solver not proven optimal for node %d.\n", node_id);
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }
    // Sometimes we run into a few issues getting the ``right'' value
    if (!isVal(tmpSolverBase->getObjValue(), stats[node_id].obj, 1e-3)) {
      tmpSolverBase->resolve();
    }

#ifdef TRACE
    double curr_nb_obj_val = tmpSolverBase->getObjValue() - probData.LPopt; //getNBObjValue(tmpSolverBase, castsolver, probData);
    printf("DEBUG: Node: %d .......... Obj val:%.3f .......... NB obj val: %.3f\n", node_id,
        tmpSolverBase->getObjValue(), curr_nb_obj_val);
    printNodeStatistics(stats[node_id], true);
#endif
    if (!isVal(tmpSolverBase->getObjValue(), stats[node_id].obj, 1e-3)) {
      std::string commonName;
      setCgsName(commonName, curr_num_changed_bounds, commonTermIndices,
          commonTermCoeff, commonTermRHS, false);
#ifdef TRACE
      printf("Bounds changed: %s.\n", commonName.c_str());
#endif
      double ratio = tmpSolverBase->getObjValue() / stats[node_id].obj; 
      if (ratio < 1.) {
        ratio = 1. / ratio;
      }
      // Allow it ot be up to 3% off without causing an error
      if (greaterThanVal(ratio, 1.03)) {
        error_msg(errorstring,
            "Objective at parent node %d is incorrect. During BB, it was %s, now it is %s.\n",
            node_id, stringValue(stats[node_id].obj, "%1.3f").c_str(),
            stringValue(tmpSolverBase->getObjValue(), "%1.3f").c_str());
        writeErrorToLog(errorstring, GlobalVariables::log_file);
        exit(1);
      } else {
        warning_msg(warnstring,
            "Objective at parent node %d is incorrect. During BB, it was %s, now it is %s.\n",
            node_id, stringValue(stats[node_id].obj, "%1.3f").c_str(),
            stringValue(tmpSolverBase->getObjValue(), "%1.3f").c_str());
      }
    }

    // Now we go to the branch or branches left for this node
    //    const int num_ineq_per_term = curr_num_changed_bounds + 1;  // leaf nodes have one extra changed
    const int num_ineq_per_term = 1;  // leaf nodes have one extra changed
    const int branching_index = stats[node_id].branch_index;
    const int branching_variable = stats[node_id].variable;
    int branching_way = stats[node_id].way;
    int branching_value =
        (branching_way <= 0) ? stats[node_id].ub : stats[node_id].lb;

    // Set up termIndices, termCoeff, termRHS for this term
    // First direction
    term_ind++;
    termIndices[term_ind].resize(num_ineq_per_term);
    termCoeff[term_ind].resize(num_ineq_per_term);
    termRHS[term_ind].resize(num_ineq_per_term);
    for (int i = 0; i < num_ineq_per_term; i++) {
      termIndices[term_ind][i].resize(1);
      termCoeff[term_ind][i].resize(1);
//      if (i == num_ineq_per_disj - 1) {
      termIndices[term_ind][i][0] = branching_variable;
      termCoeff[term_ind][i][0] = (branching_way <= 0) ? -1. : 1.;
      termRHS[term_ind][i] = termCoeff[term_ind][i][0] * branching_value;
//      } else {
//        termIndices[term_ind][i][0] = stats[node_ind].changed_var[i];
//        termCoeff[term_ind][i][0] = (stats[node_ind].changed_way[i] <= 0) ? -1. : 1.;
//        termRHS[term_ind][i] = termCoeff[term_ind][i][0]
//            * stats[node_ind].changed_bound[i];
//      }
    }

    PointCutsSolverInterface* tmpSolver =
        dynamic_cast<PointCutsSolverInterface*>(tmpSolverBase->clone());
#ifdef TRACE
    printf("\n## Solving first child of parent %d/%d for term %d. ##\n",
        tmp_ind + 1, eventHandler->getNumNodesOnTree(), term_ind);
#endif
    calcAndFeasFacet[term_ind] = (param.getTEMP() >= 100) ? 
        fixDisjTerm(tmpSolver, termIndices[term_ind], termCoeff[term_ind], termRHS[term_ind]) :
        fixDisjTermUsingBounds(tmpSolver, termIndices[term_ind], termCoeff[term_ind], termRHS[term_ind]);

    // Possibly strengthen
    if (calcAndFeasFacet[term_ind]
        && param.getParamVal(ParamIndices::STRENGTHEN_PARAM_IND) == 2) {
      // Add Gomory cuts on disjunctive term and resolve
      OsiCuts GMICs;
      CglGMI GMIGen(param);
      GMIGen.generateCuts(*tmpSolver, GMICs);
      tmpSolver->applyCuts(GMICs);
      tmpSolver->resolve();
      calcAndFeasFacet[term_ind] = checkSolverOptimality(tmpSolver, true);
    }

   if (calcAndFeasFacet[term_ind]) {
      setCgsName(cgsName[0], num_ineq_per_term, termIndices[term_ind],
          termCoeff[term_ind], termRHS[term_ind]);
      setCgsName(cgsName[0], curr_num_changed_bounds, commonTermIndices,
          commonTermCoeff, commonTermRHS, true);
      if (stats[node_id].depth + 1 < GlobalVariables::minNodeDepth) {
        GlobalVariables::minNodeDepth = stats[node_id].depth + 1;
      }
      if (stats[node_id].depth + 1 > GlobalVariables::maxNodeDepth) {
        GlobalVariables::maxNodeDepth = stats[node_id].depth + 1;
      }

      // Update strong branching info
      if (tmpSolver->getObjValue() < this->strong_branching_lb) {
        this->strong_branching_lb = tmpSolver->getObjValue();
      }
      if (tmpSolver->getObjValue() > this->strong_branching_ub) {
        this->strong_branching_ub = tmpSolver->getObjValue();
      }

      // Get cobasis information and PR collection
      enableFactorization(tmpSolver);
//      // After enabling factorization, things sometimes look different and it is worth resolving (e.g., bc1 with num_strong = 5, row price on row 458)
//      if ((tmpSolver->getModelPtr()->sumPrimalInfeasibilities() > param.getEPS())
//          || (tmpSolver->getModelPtr()->sumDualInfeasibilities()
//              > param.getEPS())) {
//        tmpSolver->initialSolve();
//        tmpSolver->disableFactorization();
//        tmpSolver->enableFactorization();
//      }
      identifyCobasis(tmpVarBasicInRow[term_ind], tmpBasicStructVar[term_ind],
          tmpBasicSlackVar[term_ind], rowOfTmpVar[term_ind],
          rowOfOrigNBVar[term_ind], tmpNonBasicVarIndex[term_ind],
          currBInvACol[term_ind], currRay[term_ind], tmpSolver, probData,
          termIndices[term_ind][0][0], false, true);

      vpcTimeStats.register_name(time_T1 + std::to_string(term_ind));
      vpcTimeStats.register_name(time_T2 + std::to_string(term_ind));

      if (!reachedTimeLimit(vpcTimeStats, time_T1 + "TOTAL",
          param.getTIMELIMIT())) {
        vpcTimeStats.start_timer(time_T1 + "TOTAL");
        vpcTimeStats.start_timer(time_T1 + std::to_string(term_ind));

        genDepth1PRCollection(interPtsAndRays[term_ind], tmpSolver, probData,
            termIndices[term_ind], tmpVarBasicInRow[term_ind],
            tmpBasicStructVar[term_ind], tmpBasicSlackVar[term_ind],
            rowOfTmpVar[term_ind], rowOfOrigNBVar[term_ind],
            tmpNonBasicVarIndex[term_ind], currRay[term_ind], term_ind, false,
            0, max_num_cgs);

        vpcTimeStats.end_timer(time_T1 + "TOTAL");
        vpcTimeStats.end_timer(time_T1 + std::to_string(term_ind));

        // Check that the points and rays we have generated satisfy the objective cut
        // This is now done when they are generated
//        checkPRCollectionForFeasibility(term_ind, interPtsAndRays[term_ind],
//            curr_nb_obj_val, objCut);
      }
    }

    if (tmpSolver) {
      delete tmpSolver;
    }

    // Should we compute a second branch?
    if (branching_index == 0) {
      // Other direction
      branching_way = (stats[node_id].way <= 0) ? 1 : 0;
      branching_value =
          (branching_way <= 0) ? branching_value - 1 : branching_value + 1;

      term_ind++;
      termIndices[term_ind].resize(num_ineq_per_term);
      termCoeff[term_ind].resize(num_ineq_per_term);
      termRHS[term_ind].resize(num_ineq_per_term);
      for (int i = 0; i < num_ineq_per_term; i++) {
        termIndices[term_ind][i].resize(1);
        termCoeff[term_ind][i].resize(1);
        //      if (i == num_ineq_per_disj - 1) {
        termIndices[term_ind][i][0] = branching_variable;
        termCoeff[term_ind][i][0] = (branching_way <= 0) ? -1. : 1.;
        termRHS[term_ind][i] = termCoeff[term_ind][i][0] * branching_value;
        //      } else {
        //        termIndices[term_ind][i][0] = stats[node_ind].changed_var[i];
        //        termCoeff[term_ind][i][0] = (stats[node_ind].changed_way[i] <= 0) ? -1. : 1.;
        //        termRHS[term_ind][i] = termCoeff[term_ind][i][0]
        //            * stats[node_ind].changed_bound[i];
        //      }
      }

      // Change back to original state then add this term
      tmpSolver =
          dynamic_cast<PointCutsSolverInterface*>(tmpSolverBase->clone());
#ifdef TRACE
      printf("\n## Solving second child of parent %d/%d for term %d. ##\n",
          tmp_ind + 1, eventHandler->getNumNodesOnTree(), term_ind);
#endif
      calcAndFeasFacet[term_ind] = (param.getTEMP() >= 100) ?
        fixDisjTerm(tmpSolver, termIndices[term_ind], termCoeff[term_ind], termRHS[term_ind]) :
        fixDisjTermUsingBounds(tmpSolver, termIndices[term_ind], termCoeff[term_ind], termRHS[term_ind]);

      // Possibly strengthen
      if (calcAndFeasFacet[term_ind]
          && param.getParamVal(ParamIndices::STRENGTHEN_PARAM_IND) == 2) {
        // Add Gomory cuts on disjunctive term and resolve
        OsiCuts GMICs;
        CglGMI GMIGen(param);
        GMIGen.generateCuts(*tmpSolver, GMICs);
        tmpSolver->applyCuts(GMICs);
        tmpSolver->resolve();
        calcAndFeasFacet[term_ind] = checkSolverOptimality(tmpSolver, true);
      }

      if (calcAndFeasFacet[term_ind]) {
        setCgsName(cgsName[0], num_ineq_per_term, termIndices[term_ind],
            termCoeff[term_ind], termRHS[term_ind]);
        setCgsName(cgsName[0], curr_num_changed_bounds, commonTermIndices,
            commonTermCoeff, commonTermRHS, true);
        if (stats[node_id].depth + 1 < GlobalVariables::minNodeDepth) {
          GlobalVariables::minNodeDepth = stats[node_id].depth + 1;
        }
        if (stats[node_id].depth + 1 > GlobalVariables::maxNodeDepth) {
          GlobalVariables::maxNodeDepth = stats[node_id].depth + 1;
        }

        // Update strong branching info
        if (tmpSolver->getObjValue() < this->strong_branching_lb) {
          this->strong_branching_lb = tmpSolver->getObjValue();
        }
        if (tmpSolver->getObjValue() > this->strong_branching_ub) {
          this->strong_branching_ub = tmpSolver->getObjValue();
        }

        // Get cobasis information and PR collection
        enableFactorization(tmpSolver);
//        // After enabling factorization, things sometimes look different and it is worth resolving (e.g., bc1 with num_strong = 5, row price on row 458)
//        if ((tmpSolver->getModelPtr()->sumPrimalInfeasibilities()
//            > param.getEPS())
//            || (tmpSolver->getModelPtr()->sumDualInfeasibilities()
//                > param.getEPS())) {
//          tmpSolver->initialSolve();
//          tmpSolver->disableFactorization();
//          tmpSolver->enableFactorization();
//        }
        identifyCobasis(tmpVarBasicInRow[term_ind], tmpBasicStructVar[term_ind],
            tmpBasicSlackVar[term_ind], rowOfTmpVar[term_ind],
            rowOfOrigNBVar[term_ind], tmpNonBasicVarIndex[term_ind],
            currBInvACol[term_ind], currRay[term_ind], tmpSolver, probData,
            termIndices[term_ind][0][0], false, true);

        vpcTimeStats.register_name(time_T1 + std::to_string(term_ind));
        vpcTimeStats.register_name(time_T2 + std::to_string(term_ind));

        if (!reachedTimeLimit(vpcTimeStats, time_T1 + "TOTAL",
            param.getTIMELIMIT())) {
          vpcTimeStats.start_timer(time_T1 + "TOTAL");
          vpcTimeStats.start_timer(time_T1 + std::to_string(term_ind));

          genDepth1PRCollection(interPtsAndRays[term_ind], tmpSolver, probData,
              termIndices[term_ind], tmpVarBasicInRow[term_ind],
              tmpBasicStructVar[term_ind], tmpBasicSlackVar[term_ind],
              rowOfTmpVar[term_ind], rowOfOrigNBVar[term_ind],
              tmpNonBasicVarIndex[term_ind], currRay[term_ind], term_ind, false,
              0, max_num_cgs);

          vpcTimeStats.end_timer(time_T1 + "TOTAL");
          vpcTimeStats.end_timer(time_T1 + std::to_string(term_ind));

          // Check that the points and rays we have generated satisfy the objective cut
          // This is now done when they are generated
//          checkPRCollectionForFeasibility(term_ind, interPtsAndRays[term_ind],
//              curr_nb_obj_val, objCut);
        }
      }
      if (tmpSolver) {
        delete tmpSolver;
      }
    } /* end second branch computation */

    if (tmpSolverBase) {
      delete tmpSolverBase;
    }
  } /* end loop over nodes on tree */

#ifdef TRACE
  printf(
      "\nFinished generating cgs from partial BB. Min obj val: Structural: %1.3f, NB: %1.3f.\n",
      this->strong_branching_lb, this->min_nb_obj_val);
#endif

  const bool flipBeta = param.getParamVal(ParamIndices::FLIP_BETA_PARAM_IND) > 0; // if the PRLP is infeasible, we will try flipping the beta
  if (flipBeta || greaterThanVal(this->strong_branching_ub, this->strong_branching_lb)
      || greaterThanVal(this->min_nb_obj_val, 0.)) {
    for (int i = 0; i < (int) interPtsAndRays.size(); i++) {
      GlobalVariables::numDisjTerms += calcAndFeasFacet[i];
    }
    const int old_num_obj_tried = num_obj_tried;
    const int old_num_cuts = num_generated_vpcsT1 + num_generated_vpcsT2;
    const int old_num_primal_infeas =
        GlobalVariables::numCutSolverFails[CutSolverFails::PRIMAL_CUTSOLVER_FAIL_IND];
    generateVPCsT1(structVPCs, probData,
//        tmpVarBasicInRow, tmpBasicStructVar,
//        tmpBasicSlackVar, rowOfTmpVar, rowOfOrigNBVar, tmpNonBasicVarIndex,
//        currBInvACol, currRay,
        vpcTimeStats, structSICs, interPtsAndRays,
        num_vpcs_per_cgs_T1[0], num_generated_vpcsT1, objCut,
//        termIndices, termCoeff, termRHS,
        calcAndFeasFacet, 0, 0, cgsName[0],
//        num_cgs,
        max_num_cgs, dim, inNBSpace);
    if (num_generated_vpcsT1 + num_generated_vpcsT2 > old_num_cuts) {
      GlobalVariables::numCgsLeadingToCuts++;
      GlobalVariables::numCgsActuallyUsed++;
    } else if ((num_obj_tried > old_num_obj_tried)
        && (old_num_primal_infeas
            == GlobalVariables::numCutSolverFails[CutSolverFails::PRIMAL_CUTSOLVER_FAIL_IND])) {
      GlobalVariables::numCgsActuallyUsed++;
    } else {
      // Clear out stuff because this cgs was not used?
    }

    structVPCs.setOsiCuts();

#ifdef SHOULD_WRITE_VPCS
    structVPCs.printCuts("VPCs", castsolver, probData);
#endif

#ifdef TRACE
    if (std::abs(param.getTEMP()) >= 10 && std::abs(param.getTEMP()) <= 13) {
      const int temp_val = std::abs(param.getTEMP());
      //const std::vector<int> testset = {000, 001, 004, 010, 011, 014, 020, 021, 024, 030, 031, 034};
//    const std::vector<int> test_num_trust = {-1, std::numeric_limits<int>::max()};
//    const std::vector<int> testset = {000, 010, 030, 004, 014, 034};
//      const std::vector<int> test_paramset = { 104, 204, 304, 404, 504, -104, -204, -304, -404, -504, -604 };
      const std::vector<int> test_num_trust =
          { std::numeric_limits<int>::max() };
      const std::vector<int> test_paramset = { 000 };
      for (int num_before_trusted : test_num_trust) {
        for (int test_param : test_paramset) {
          const int old_param_val = GlobalVariables::param.getParamVal(ParamIndices::PARTIAL_BB_STRATEGY_PARAM_IND);
          GlobalVariables::param.setParamVal(ParamIndices::PARTIAL_BB_STRATEGY_PARAM_IND, test_param);
          const bool shouldApplyVPCs = temp_val == 10 || temp_val == 12;
          const bool shouldApplySICs = temp_val == 11 || temp_val == 12;
          OsiClpSolverInterface* newBBSolver = dynamic_cast<OsiClpSolverInterface*>(solver->clone());
          newBBSolver->disableFactorization();
          if (shouldApplyVPCs) {
            newBBSolver->applyCuts(structVPCs);
          }
          if (shouldApplySICs) {
            newBBSolver->applyCuts(structSICs);
          }
          newBBSolver->resolve();
          printf(
              "\n## Generating partial branch-and-bound tree after adding cuts (VPCs: %d, SICs: %d, total new num rows: %d, new obj value: %.3f). ##\n",
              shouldApplyVPCs, shouldApplySICs,
              newBBSolver->getNumRows() - solver->getNumRows(),
              newBBSolver->getObjValue());
          setupClpForCbc(newBBSolver);
          CbcModel* new_cbc_model = new CbcModel;
          new_cbc_model->swapSolver(newBBSolver);
          new_cbc_model->setModelOwnsSolver(true); // solver will be deleted with cbc object
          setMessageHandler(new_cbc_model);

          generatePartialBBTree(new_cbc_model, newBBSolver, std::numeric_limits<int>::max(), num_strong, num_before_trusted);

          VPCEventHandler* newEventHandler;
          try {
            newEventHandler = dynamic_cast<VPCEventHandler*>(new_cbc_model->getEventHandler());
          } catch (std::exception& e) {
            error_msg(errstr, "Could not get event handler.\n");
            writeErrorToLog(errstr, GlobalVariables::log_file);
            exit(1);
          }
          printNodeStatistics(newEventHandler->getStatsVector(), false);
          if (newEventHandler->getPrunedStatsVector().size() > 0) {
            printf("\n");
            printNodeStatistics(newEventHandler->getPrunedStatsVector(), false);
          }
          generateTikzTreeString(newEventHandler, old_param_val, this->strong_branching_lb, true);
          if (new_cbc_model->getNodeCount() < 70) {
            printf(
                "Number nodes: %d.\nCut strategy: %d.\n# leafs: %d.\nSolve strategy: %d.\nDo you wish to exit? (y/n) ",
                new_cbc_model->getNodeCount(), old_param_val,
                -1 * param.getParamVal(ParamIndices::VPC_DEPTH_PARAM_IND),
                test_param);
            std::string response;
            std::getline (std::cin, response);
            if (response.compare("y") == 0 || response.compare("yes") == 0) {
              exit(1);
            }
          }
          if (new_cbc_model) {
            delete new_cbc_model;
            new_cbc_model = NULL;
          }
          GlobalVariables::param.setParamVal(ParamIndices::PARTIAL_BB_STRATEGY_PARAM_IND, old_param_val);
        } /* loop over param test set */
      } /* loop over num trusted to be tested */
    } /* check temp for whether to apply cuts and print post-cuts tree */
#endif

#ifdef SHOULD_WRITE_INTPOINTS
    printf("\n## Save information about the points and rays. ##\n");
    char filename[300];
    snprintf(filename, sizeof(filename) / sizeof(char), "%s-PointsAndRays.csv",
        GlobalVariables::out_f_name_stub.c_str());
    FILE *ptsRaysOut = fopen(filename, "w");
    writeInterPtsAndRays(ptsRaysOut, probData, 1, &interPtsAndRays,
        cgsName.data());
    fclose(ptsRaysOut);
#endif
  } /* if (existsTermWithStrictlyBetterObjVal) */
  else {
    GlobalVariables::numCutSolverFails[CutSolverFails::DB_UB_EQUALS_LP_OBJ_NO_OBJ_FAIL_IND]++;
  }

  // Free
  if (cbc_model) {
    delete cbc_model;
    cbc_model = NULL;
  }
  if (castsolver) {
    delete castsolver;
    castsolver = NULL;
  }
} /* generateCutsFromPartialBB */

/************************************************************************/
/**
 * Assuming we are not working in the subspace (not tested or implemented yet anyway)
 */
void CglVPC::generateCutsFromSplitsOrCrosses(AdvCuts& structVPCs, const SolutionInfo& probData,
    const AdvCuts &structSICs) {
#ifdef TRACE
  printf("\n############### Starting VPC generation. ###############\n");
#endif
  castsolver = dynamic_cast<OsiClpSolverInterface*>(this->solver->clone());

  // Decide if we are in the non-basic space, based on option
  const bool inNBSpace = param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND);
  const int dim = inNBSpace ? probData.numNB : solver->getNumCols();

  /***********************************************************************************
   * Save objective
   ***********************************************************************************/
  // In order to calculate how good the points we find are
  // with respect to the objective function (which should be in NB space when appropriate)
  // Clearly a valid inequality is c^T x \ge c^\T v,
  // where v is the LP optimum, since we are minimizing
  // Either we have the objective, or we have the reduced costs
  // of the non-basic variables (which is exactly the objective
  // in the non-basic space).
  // Recall that the reduced cost of slack variables is exactly
  // the negative of the row price (dual variable) for that row.
  AdvCut structObj(false, 1);
  AdvCut NBObj(true, 1);
  structObj.setOsiRowCut(std::vector<int>(), solver->getNumCols(), solver->getObjCoefficients(),
      solver->getObjValue(), CoinMin(probData.EPS, param.getEPS()));
  NBObj.setOsiRowCut(probData.nonBasicReducedCost, 0.0, CoinMin(probData.EPS, param.getEPS()));
  AdvCut* objCut = inNBSpace ? &NBObj : &structObj;

  /***********************************************************************************
   * Setup intersection point and ray storage
   ***********************************************************************************/
  const int cgs_param = param.getParamVal(
      CglGICParamNamespace::ParamIndices::CGS_PARAM_IND);
  const bool use_splits = (cgs_param == 0);
  const bool use_triangles = (cgs_param > 0) && (cgs_param < 7);
  const bool use_cross = (cgs_param == 7);

  const int num_disj_terms_cgs = (use_splits) ? 2 : 3 + use_cross;
  const int num_ineq_per_term = 1 + use_cross;
  // For each variable in the fractional core, we get cuts, or for each pair
  const int num_cgs_limit = param.getParamVal(
      CglGICParamNamespace::ParamIndices::NUM_CGS_PARAM_IND);
  const int max_num_cgs = param.getMaxNumCgs(probData.numFeasSplits); // TODO why different from num_cgs_limit?

  std::vector<std::vector<IntersectionInfo> > interPtsAndRays(max_num_cgs);  // one per cut-generating set
  for (int cgs_ind = 0; cgs_ind < max_num_cgs; cgs_ind++) {
    interPtsAndRays[cgs_ind].resize(num_disj_terms_cgs);
    for (int f = 0; f < num_disj_terms_cgs; f++) {
      // Reverse ordering so packed matrix is row ordered and set number of columns
      interPtsAndRays[cgs_ind][f].reverseOrdering();
      interPtsAndRays[cgs_ind][f].setDimensions(0, dim);
      interPtsAndRays[cgs_ind][f].resizeInfoByActivationRound(1);
    }
  }

  // Save whether a particular facet has been calculated already, and where it is located
  // [split_ind][floor/ceil][cgs/facet]
  std::vector<std::vector<std::vector<int> > > splitFacetLocation(
      probData.numFeasSplits);
  for (int split_ind = 0; split_ind < probData.numFeasSplits; split_ind++) {
    splitFacetLocation[split_ind].resize(2);
    splitFacetLocation[split_ind][0].resize(2, -1);
    splitFacetLocation[split_ind][1].resize(2, -1);
  }

  /***********************************************************************************
   * Choose facets of cut-generating sets and generate VPCs
   ***********************************************************************************/
  // For tracking and early exiting
  vpcTimeStats.register_name(time_T1 + "TOTAL");
  vpcTimeStats.register_name(time_T2 + "TOTAL");

  int num_generated_vpcsT1 = 0;
  int num_generated_vpcsT2 = 0;
  std::vector<int> num_vpcs_per_cgs_T1(num_cgs_limit, 0);
  std::vector<int> num_vpcs_per_cgs_T2(num_cgs_limit, 0);

//  const bool splitVarDeleted = param.getParamVal(
//      CglGICParamNamespace::ParamIndices::SPLIT_VAR_DELETED_PARAM_IND);
  const int num_splits_per_cgs = (use_splits) ? 1 : 2; // triangles: 2 vars, splits: 1 var
//  std::vector<int> splitIndex(num_splits_per_cgs);
  std::vector<std::vector<double> > currRoundedVal(num_splits_per_cgs);

  // Initialize splitIndex values;
  // that there are sufficiently many splits should have been checked prior
  for (int s = 0; s < num_splits_per_cgs; s++) {
//    splitIndex[s] = s;
    currRoundedVal[s].resize(2); // to hold floor and ceiling
  }

  std::vector<double> splitMultiplier(2);
  splitMultiplier[0] = -1.0;
  splitMultiplier[1] = 1.0;

//  std::vector<std::vector<int> > termIndices(num_disj_terms_cgs);
//  std::vector<std::vector<double> > termCoeff(num_disj_terms_cgs);
//  std::vector<double> termRHS(num_disj_terms_cgs);
  std::vector<std::vector<std::vector<int> > > termIndices(num_disj_terms_cgs);
  std::vector<std::vector<std::vector<double> > > termCoeff(
      num_disj_terms_cgs);
  std::vector<std::vector<double> > termRHS(num_disj_terms_cgs);
  for (int f = 0; f < num_disj_terms_cgs; f++) {
    termIndices[f].resize(num_ineq_per_term);
    termCoeff[f].resize(num_ineq_per_term);
    termRHS[f].resize(num_ineq_per_term);
    const int num_coeff_in_ineq = (f < 2) || (use_cross) ? 1 : 2;
    for (int i = 0; i < num_ineq_per_term; i++) {
      termIndices[f][i].resize(num_coeff_in_ineq);
      termCoeff[f][i].resize(num_coeff_in_ineq);
    }
  }

  std::vector<std::vector<std::vector<int> > > termIndicesAllCgs(max_num_cgs);
  std::vector<std::vector<std::vector<double> > > termCoeffAllCgs(max_num_cgs);
  std::vector<std::vector<double> > termRHSAllCgs(max_num_cgs);

  std::vector<std::vector<bool> > disjTermFeas(max_num_cgs);
  std::vector<std::vector<double> > disjTermObjVal(max_num_cgs);
  std::vector<std::string> cgsName(max_num_cgs);

  // Keep track of which triangle is being used
  int triangle_ind = ((cgs_param == 5) || (cgs_param == 6)) ? 0 : cgs_param - 1;

#ifdef TRACE
  printf("\n## Getting strong branching value for all splits ##\n");
#endif
  // Store the strong branching value,
  // which is just the lower of the two bounds obtained (assuming minimization)
  // Also store the higher bound and the facet on which it is obtained, for sorting purposes
  std::vector<double> splitStrongBranchingValue(probData.numFeasSplits);
  std::vector<double> splitHigherBound(probData.numFeasSplits);
  std::vector<int> splitHigherBoundFacet(probData.numFeasSplits);

  // Also store any splits that are detected to be infeasible on one side
  std::vector<std::vector<bool> > splitFeasOnSide(probData.numFeasSplits);
//  std::vector<int> infeasSplitIndex;
//  std::vector<int> infeasSplitVar;
//  std::vector<double> infeasSplitCoeff;
//  std::vector<double> infeasSplitRHS;

  try {
    setupClpForStrongBranching(dynamic_cast<OsiClpSolverInterface*>(solver));
  } catch (std::exception& e) {
    // It's okay, we can continue
  }
  solver->enableFactorization();
  solver->markHotStart();
  for (int split_ind = 0; split_ind < probData.numFeasSplits; split_ind++) {
    // Initialize variables and data structures for this split
    splitFeasOnSide[split_ind].resize(2, true);
    const int var = probData.feasSplitVar[split_ind];
    const double var_val = probData.a0[probData.rowOfVar[var]];
    const double varFixedVal[] = { std::floor(var_val), std::ceil(var_val) };
    const double varBound[] = { solver->getColLower()[var],
        solver->getColUpper()[var] };
    std::vector<double> opt_val(2);

    // Do down then up branch
    for (int s = 0; s < 2; s++) {
      const double newLB = (s == 0) ? varBound[0] : varFixedVal[1];
      const double newUB = (s == 0) ? varFixedVal[0] : varBound[1];
      solver->setColBounds(var, newLB, newUB);
      if (s == 0) {
        solveFromHotStart(solver, var, true, varBound[1], varFixedVal[1]);
      } else {
        solveFromHotStart(solver, var, false, varBound[0], varFixedVal[0]);
      }
      const bool proven_optimal = solver->isProvenOptimal();
      const bool proven_infeasible = solver->isProvenPrimalInfeasible();
      if (proven_optimal) {
        opt_val[s] = solver->getObjValue();
      } else if (proven_infeasible) {
        opt_val[s] = solver->getInfinity();
        splitFeasOnSide[split_ind][s] = false;
//        infeasSplitIndex.push_back(split_ind);
//        infeasSplitVar.push_back(var);
//        infeasSplitCoeff.push_back(splitMultiplier[s]);
//        infeasSplitRHS.push_back(splitMultiplier[s] * varFixedVal[s]);
//        const int indexToUse = infeasSplitVar.size() - 1;
//        const int cgs_ind = -1;
//        const std::string thisCgsName = "(" + "-x" + std::to_string(var)
//            + " >= " + "-" + std::to_string(varFixedVal[0]) + " V " + "x"
//            + std::to_string(var) + " >= " + std::to_string(varFixedVal[1])
//            + ")";
        if (addOneSidedCut(structVPCs, (s == 1), solver, var)) {
#ifdef TRACE
          printf(
              "\n## Generated one-sided VPC excluding infeasible term %d of split %d/%d with name %s. ##\n",
              s, split_ind + 1, probData.numFeasSplits,
              solver->getColName(var).c_str());
#endif
          num_generated_vpcsT1++;
        }
      } else {
        error_msg(errstr,
            "Solver is not proven optimal or infeasible when solving split %d (var %d) on %s. Check why this happened!\n",
            split_ind, var, ((s == 0) ? "floor" : "ceil"));
        writeErrorToLog(errstr, GlobalVariables::log_file);
        exit(1);
      }
      // Return to previous state
      solver->setColBounds(var, varBound[0], varBound[1]);
      solver->solveFromHotStart();  // return to previous value
    } /* end iterating over sides of the split */

    const int higher_facet_ind = (opt_val[0] > opt_val[1]) ? 0 : 1;
    splitStrongBranchingValue[split_ind] = opt_val[!higher_facet_ind];
    splitHigherBound[split_ind] = opt_val[higher_facet_ind];
    splitHigherBoundFacet[split_ind] = higher_facet_ind;
    if ((opt_val[higher_facet_ind] > this->strong_branching_ub)
        && splitFeasOnSide[split_ind][higher_facet_ind]) {
      this->strong_branching_ub = opt_val[higher_facet_ind];
    }
  } /* end getting strong branching value for each split */
  //solver->solveFromHotStart();
  solver->unmarkHotStart();
  solver->disableFactorization();

  std::vector<int> sortSplitIndex;
  sortSplitIndex.resize(probData.numFeasSplits);
  for (int i = 0; i < probData.numFeasSplits; i++) {
    sortSplitIndex[i] = i;
  }
  sort(sortSplitIndex.begin(), sortSplitIndex.end(),
      index_cmp_dsc<const double*>(splitStrongBranchingValue.data())); // descending order
  this->strong_branching_lb = splitStrongBranchingValue[sortSplitIndex[0]];

  // Prepare to sort the cgs
//  std::vector<std::vector<int> > splitIndexAllCgs(max_num_cgs);
//  splitIndexAllCgs[0].resize(num_splits_per_cgs);
//  for (int s = 0; s < num_splits_per_cgs; s++) {
//    splitIndexAllCgs[0][s] = s;
//  }
  std::vector<int> triangleIndexAllCgs(max_num_cgs, triangle_ind);
//  std::vector<std::vector<int> > currFracVarAllCgs(num_cgs);
//  std::vector<std::vector<std::vector<double> > > currRoundedValAllCgs(num_cgs);
  std::vector<int> sortCgsIndex(max_num_cgs); // If we choose to sort

  std::vector<double> distToCgs(max_num_cgs, -1.);
  std::vector<int> currFracVar(num_splits_per_cgs);
  const bool calcDistByMaxFractionality = false;
  const bool calcDistByStrongBranching = true;
  setupCgs(probData, triangle_ind, max_num_cgs, num_splits_per_cgs,
      sortSplitIndex, splitFeasOnSide, splitStrongBranchingValue,
      splitHigherBound, splitHigherBoundFacet, structObj.getTwoNormSquared(),
      sortCgsIndex, distToCgs, calcDistByMaxFractionality,
      calcDistByStrongBranching, triangleIndexAllCgs, currFracVar);

  // Sort the cgs from largest to smallest
  sort(sortCgsIndex.begin(), sortCgsIndex.end(),
      index_cmp_dsc<double*>(distToCgs.data()));

  std::vector<int> splitIndexForCgs(num_splits_per_cgs);

  const int num_triangles = use_triangles
      + 3 * (cgs_param == 5 || cgs_param == 6);

  // DEBUG
//  for (int unsorted_cgs_ind = 0; unsorted_cgs_ind < num_cgs; unsorted_cgs_ind++) {
//    const int cgs_ind = sortCgsIndex[unsorted_cgs_ind]; // Okay somehow need to access the proper set of variables...
//    disjTermFeas[unsorted_cgs_ind].resize(num_disj_terms_cgs, true);
//    // Get fractional variables
//    int num_feas_splits_for_this_cgs = num_splits_per_cgs;
//    for (int s = 0; s < num_splits_per_cgs; s++) {
//      const int unsorted_split_ind = formulaForSplitIndex(s, cgs_ind,
//          probData.numFeasSplits, (cgs_param == 7) ? 1 : num_triangles,
//          (s == 1) ? splitIndexForCgs[0] : -1);
//      splitIndexForCgs[s] = unsorted_split_ind;
//      const int split_ind = sortSplitIndex[unsorted_split_ind];
//      currFracVar[s] = probData.feasSplitVar[split_ind];
//      currRoundedVal[s][0] = std::floor(
//          solver->getColSolution()[currFracVar[s]]);
//      currRoundedVal[s][1] = std::ceil(
//          solver->getColSolution()[currFracVar[s]]);
//      if (!splitFeasOnSide[split_ind][0] || !splitFeasOnSide[split_ind][1]) {
//        num_feas_splits_for_this_cgs--;
//      }
//    }
//
//    if (num_feas_splits_for_this_cgs == 0) {
//      continue;
//    }
//
//    // Setup based on whether we are using triangles or not
//    if (use_splits) {
//      // -xk >= -floor
//      // xk >= ceil
//      for (int f = 0; f < 2; f++) {
//        termIndices[f][0][0] = currFracVar[0];
//        termCoeff[f][0][0] = splitMultiplier[f];
//        termRHS[f][0] = termCoeff[f][0][0] * currRoundedVal[0][f];
//      }
//    } else if (use_triangles) { // Use triangles
//      // -xk1 >= -floor1 || +xk1 >= ceil1
//      // -xk2 >= -floor2 || +xk2 >= ceil2
//      // (0,0) : + xk1 + xk2 >= ceil1 + ceil2    ||
//      // (0,1) : + xk1 - xk2 >= ceil1 - floor2   ||
//      // (1,0) : - xk1 + xk2 >= - floor1 + ceil2 ||
//      // (1,1) : - xk1 - xk2 >= - floor1 - floor2
//      // OR
//      // (0,0) : + xk1 + xk2 >= 2 + floor1 + floor2 ||
//      // (0,1) : + xk1 - xk2 >= 1 + floor1 - floor2 ||
//      // (1,0) : - xk1 + xk2 >= 1 - floor1 + floor2 ||
//      // (1,1) : - xk1 - xk2 >= 0 - floor1 - floor2
//      termRHS[2][0] = 0.0;
//      for (int s = 0; s < 2; s++) {
//        // Use the bitwise & operator to get the parity of each bit
//        // The triangles are numbered 0, 1, 2, 3
//        // I.e., 00, 01, 10, 11
//        // E.g., t = 1 (01):
//        // Facet 0: -x1 >= -floor1
//        //   whichMult0 = 0 since (1 & (2 - 0)) = (01 & 2) = 0
//        //   splitMultiplier[0] = -1
//        // Facet 1: x2 >= ceil2
//        //   whichMult1 = 1 since (1 & (2 - 1)) = (01 & 1) = 1
//        //   splitMultiplier[1] = 1
//        // Facet 2: + x1 - x2 >= ceil1 - floor2
//        const int whichSideOfSplit = (triangleIndexAllCgs[cgs_ind] & (2 - s)) ? 1 : 0;
//        termIndices[s][0][0] = currFracVar[s];
//        termCoeff[s][0][0] = splitMultiplier[whichSideOfSplit];
//        termRHS[s][0] = splitMultiplier[whichSideOfSplit]
//            * currRoundedVal[s][whichSideOfSplit];
//        termIndices[2][0][s] = currFracVar[s];
//        termCoeff[2][0][s] = -1.0 * termCoeff[s][0][0];
//        termRHS[2][0] += termCoeff[2][0][s] * currRoundedVal[s][!whichSideOfSplit];
//        // Disable all terms that are infeasible based on splits involved
//        const int split_ind = sortSplitIndex[splitIndexForCgs[s]];
//        disjTermFeas[unsorted_cgs_ind][s] = splitFeasOnSide[split_ind][whichSideOfSplit];
//        if (!splitFeasOnSide[split_ind][!whichSideOfSplit]) {
//          disjTermFeas[unsorted_cgs_ind][2] = false;
//        }
//      }
//    } else if (use_cross) { // Use cross
//      // 00 : -x1 >= -floor1, -x2 >= -floor2
//      // 01 : -x1 >= -floor1, x2 >= ceil2
//      // 10 : x1 >= ceil1, -x2 >= -floor2
//      // 11 : x1 >= ceil1, x2 >= ceil2
//      for (int s = 0; s < 2; s++) {
//        const int split_ind = sortSplitIndex[splitIndexForCgs[s]];
//        for (int f = 0; f < 4; f++) {
//          const int whichSideOfSplit = (f & (2 - s)) ? 1 : 0;
//          termIndices[f][s][0] = currFracVar[s];
//          termCoeff[f][s][0] = splitMultiplier[whichSideOfSplit];
//          termRHS[f][s] = splitMultiplier[whichSideOfSplit]
//              * currRoundedVal[s][whichSideOfSplit];
//          disjTermFeas[unsorted_cgs_ind][f] = disjTermFeas[unsorted_cgs_ind][f]
//              && splitFeasOnSide[split_ind][whichSideOfSplit];
//        }
//      }
//    }
//
//    for (int f = 0; f < num_disj_terms_cgs; f++) {
//      if (!disjTermFeas[unsorted_cgs_ind][f]) {
//        continue;
//      }
//      if (!cgsName[cgs_ind].empty()) {
//        cgsName[cgs_ind] += " V ";
//      }
//      cgsName[cgs_ind] += "(";
//      for (int i = 0; i < num_ineq_per_disj; i++) {
//        if (i > 0) {
//          cgsName[cgs_ind] += "; ";
//        }
//        for (int coeff_ind = 0; coeff_ind < (int) termIndices[f][i].size();
//            coeff_ind++) {
//          cgsName[cgs_ind] += (termCoeff[f][i][coeff_ind] > 0) ? "+" : "-";
//          cgsName[cgs_ind] += "x";
//          cgsName[cgs_ind] += std::to_string(termIndices[f][i][coeff_ind]);
//        }
//        cgsName[cgs_ind] += " >= ";
//        cgsName[cgs_ind] += std::to_string((int) termRHS[f][i]);
//      }
//      cgsName[cgs_ind] += ")";
//    }
//    printf(
//        "DEBUG: cgs_ind: %d \t\t original_ind: %d \t\t cgs_name: %s \t\t triangle: %d \t\t distToCgs: %.6f \t\t\n",
//        unsorted_cgs_ind, cgs_ind, cgsName[cgs_ind].c_str(),
//        triangleIndexAllCgs[cgs_ind], distToCgs[cgs_ind]);
//    for (int s = 0; s < num_splits_per_cgs; s++) {
//      printf("\tSplit var: %d, val: %f, strong_lb: %f, strong_ub: %f\n",
//          currFracVar[s], solver->getColSolution()[currFracVar[s]],
//          splitStrongBranchingValue[sortSplitIndex[splitIndexForCgs[s]]],
//          splitHigherBound[sortSplitIndex[splitIndexForCgs[s]]]);
//    }
//  }
//  exit(1);

//  for (int cgs_ind = 0; cgs_ind < num_cgs; cgs_ind++) {
  int num_cgs_used = 0;
  for (int unsorted_cgs_ind = 0; unsorted_cgs_ind < max_num_cgs; unsorted_cgs_ind++) {
    if (num_cgs_used >= num_cgs_limit) {
      break;
    }
    const bool isTimeLimitReachedT1 = reachedTimeLimit(
        vpcTimeStats, time_T1 + "TOTAL", param.getTIMELIMIT());
    const bool isTimeLimitReachedT2 = reachedTimeLimit(
        vpcTimeStats, time_T2 + "TOTAL", param.getTIMELIMIT());
    const bool isTimeLimitReached =
        (param.getParamVal(ParamIndices::VPC_DEPTH_PARAM_IND) >= 2) ?
            isTimeLimitReachedT1 && isTimeLimitReachedT2 : isTimeLimitReachedT1;
    if (isTimeLimitReached) {
      break;
    }
    const int cgs_ind = sortCgsIndex[unsorted_cgs_ind];

    // Initialize disjTermFeas for this cgs
    disjTermFeas[unsorted_cgs_ind].resize(num_disj_terms_cgs, true);
    disjTermObjVal[unsorted_cgs_ind].resize(num_disj_terms_cgs);

    // Get fractional variables
    int num_feas_splits_for_this_cgs = num_splits_per_cgs;
    for (int s = 0; s < num_splits_per_cgs; s++) {
      const int unsorted_split_ind = formulaForSplitIndex(s, cgs_ind,
          probData.numFeasSplits, (cgs_param == 7) ? 1 : num_triangles,
          (s == 1) ? splitIndexForCgs[0] : -1);
      splitIndexForCgs[s] = unsorted_split_ind;
      const int split_ind = sortSplitIndex[splitIndexForCgs[s]];
      currFracVar[s] = probData.feasSplitVar[split_ind];
      currRoundedVal[s][0] = std::floor(
          castsolver->getColSolution()[currFracVar[s]]);
      currRoundedVal[s][1] = std::ceil(
          castsolver->getColSolution()[currFracVar[s]]);
      if (!splitFeasOnSide[split_ind][0] || !splitFeasOnSide[split_ind][1]) {
        num_feas_splits_for_this_cgs--;
      }
    } /* finish setting up splits used for this cgs */

    if (num_feas_splits_for_this_cgs == 0) {
      continue;
    }

    // Setup based on whether we are using splits or triangles or crosses
    if (use_splits) {
      // -xk >= -floor
      // xk >= ceil
      for (int f = 0; f < 2; f++) {
        termIndices[f][0][0] = currFracVar[0];
        termCoeff[f][0][0] = splitMultiplier[f];
        termRHS[f][0] = termCoeff[f][0][0] * currRoundedVal[0][f];
      }
    } else if (use_triangles) { // Use triangles
      // -xk1 >= -floor1 || +xk1 >= ceil1
      // -xk2 >= -floor2 || +xk2 >= ceil2
      // (0,0) : + xk1 + xk2 >= ceil1 + ceil2    ||
      // (0,1) : + xk1 - xk2 >= ceil1 - floor2   ||
      // (1,0) : - xk1 + xk2 >= - floor1 + ceil2 ||
      // (1,1) : - xk1 - xk2 >= - floor1 - floor2
      // OR
      // (0,0) : + xk1 + xk2 >= 2 + floor1 + floor2 ||
      // (0,1) : + xk1 - xk2 >= 1 + floor1 - floor2 ||
      // (1,0) : - xk1 + xk2 >= 1 - floor1 + floor2 ||
      // (1,1) : - xk1 - xk2 >= 0 - floor1 - floor2
      termRHS[2][0] = 0.0;
      for (int s = 0; s < 2; s++) {
        // Use the bitwise & operator to get the parity of each bit
        // The triangles are numbered 0, 1, 2, 3
        // I.e., 00, 01, 10, 11
        // E.g., t = 1 (01):
        // Facet 0: -x1 >= -floor1
        //   whichMult0 = 0 since (1 & (2 - 0)) = (01 & 2) = 0
        //   splitMultiplier[0] = -1
        // Facet 1: x2 >= ceil2
        //   whichMult1 = 1 since (1 & (2 - 1)) = (01 & 1) = 1
        //   splitMultiplier[1] = 1
        // Facet 2: + x1 - x2 >= ceil1 - floor2
        const int whichSideOfSplit = (triangleIndexAllCgs[cgs_ind] & (2 - s)) ? 1 : 0;
        termIndices[s][0][0] = currFracVar[s];
        termCoeff[s][0][0] = splitMultiplier[whichSideOfSplit];
        termRHS[s][0] = splitMultiplier[whichSideOfSplit]
            * currRoundedVal[s][whichSideOfSplit];
        termIndices[2][0][s] = currFracVar[s];
        termCoeff[2][0][s] = -1.0 * termCoeff[s][0][0];
        termRHS[2][0] += termCoeff[2][0][s] * currRoundedVal[s][!whichSideOfSplit];
        
        // Disable all terms that are infeasible based on splits involved
        const int split_ind = sortSplitIndex[splitIndexForCgs[s]];
        disjTermFeas[unsorted_cgs_ind][s] = splitFeasOnSide[split_ind][whichSideOfSplit];
        if (!splitFeasOnSide[split_ind][!whichSideOfSplit]) {
          disjTermFeas[unsorted_cgs_ind][2] = false;
        }
      }
    } else if (use_cross) { // Use cross
      // 00 : -x1 >= -floor1, -x2 >= -floor2
      // 01 : -x1 >= -floor1, x2 >= ceil2
      // 10 : x1 >= ceil1, -x2 >= -floor2
      // 11 : x1 >= ceil1, x2 >= ceil2
      for (int s = 0; s < 2; s++) {
        const int split_ind = sortSplitIndex[splitIndexForCgs[s]];
        for (int f = 0; f < 4; f++) {
          const int whichSideOfSplit = (f & (2 - s)) ? 1 : 0;
          termIndices[f][s][0] = currFracVar[s];
          termCoeff[f][s][0] = splitMultiplier[whichSideOfSplit];
          termRHS[f][s] = splitMultiplier[whichSideOfSplit]
              * currRoundedVal[s][whichSideOfSplit];
          disjTermFeas[unsorted_cgs_ind][f] = disjTermFeas[unsorted_cgs_ind][f]
              && splitFeasOnSide[split_ind][whichSideOfSplit];
        }
      }
    }

    // Check if the optimal solution violates the diagonal facet
    // (i.e., checking whether the solution is in the triangle)
    // If it does not, do not use this cgs (when using cgs_param == 6)
    bool use_cgs = true;
    if (cgs_param == 6) {
      double activity = 0.0;
      for (int i = 0; i < (int) termIndices[2][0].size(); i++) {
        const int var_index = termIndices[2][0][i];
        const double var_value = castsolver->getColSolution()[var_index];
        activity += termCoeff[2][0][i] * var_value;
      }
      if (lessThanVal(activity, termRHS[2][0] - 1)) {
        use_cgs = false;
      }
    }

    if (use_cgs) {
      for (int f = 0; f < num_disj_terms_cgs; f++) {
        if (!disjTermFeas[unsorted_cgs_ind][f]) {
          continue;
        }
        setCgsName(cgsName[num_cgs_used], num_ineq_per_term, termIndices[f],
            termCoeff[f], termRHS[f]);
      }
//    printf("cgs: %d/%d, triangle: %d, name: %s\n", cgs_ind + 1, num_cgs,
//        triangle_ind, cgsName[cgs_ind].c_str());

      // Check whether facets have already been calculated
      // (only check for the splits, not the diagonal, as that is uniquely considered)
      for (int s = 0; s < num_splits_per_cgs; s++) {
        if (cgs_param == 7) {
          break;
        }
        const int curr_split_ind = sortSplitIndex[splitIndexForCgs[s]];
        const bool floor_or_ceil = (termCoeff[s][0][0] > 0);
        int* oldCgsIndex = &(splitFacetLocation[curr_split_ind][floor_or_ceil][0]);
        int* oldFacetIndex = &(splitFacetLocation[curr_split_ind][floor_or_ceil][1]);
        if (*oldCgsIndex < 0) {
          *oldCgsIndex = unsorted_cgs_ind;
          *oldFacetIndex = s;
        } else {
          interPtsAndRays[unsorted_cgs_ind][s] =
              interPtsAndRays[*oldCgsIndex][*oldFacetIndex];
          disjTermFeas[unsorted_cgs_ind][s] =
              disjTermFeas[*oldCgsIndex][*oldFacetIndex];
          disjTermObjVal[unsorted_cgs_ind][s] =
              disjTermObjVal[*oldCgsIndex][*oldFacetIndex];
        }

        if (num_splits_per_cgs == 1) {
          // Then what we had above was the floor
          oldCgsIndex = &(splitFacetLocation[curr_split_ind][1][0]);
          oldFacetIndex = &(splitFacetLocation[curr_split_ind][1][1]);
          if (*oldCgsIndex < 0) {
            *oldCgsIndex = unsorted_cgs_ind;
            *oldFacetIndex = s;
          } else {
            interPtsAndRays[unsorted_cgs_ind][1] =
                interPtsAndRays[*oldCgsIndex][*oldFacetIndex];
            disjTermFeas[unsorted_cgs_ind][1] =
                disjTermFeas[*oldCgsIndex][*oldFacetIndex];
          }
        }
      }

      const int old_num_obj_tried = num_obj_tried;
      const int old_num_cuts = num_generated_vpcsT1 + num_generated_vpcsT2;
      const int old_num_primal_infeas =
          GlobalVariables::numCutSolverFails[CutSolverFails::PRIMAL_CUTSOLVER_FAIL_IND];
      generateCutsHelper(structVPCs, probData, vpcTimeStats, structSICs,
          interPtsAndRays[unsorted_cgs_ind], num_disj_terms_cgs,
          num_vpcs_per_cgs_T1[num_cgs_used], num_generated_vpcsT1,
          num_vpcs_per_cgs_T2[num_cgs_used], num_generated_vpcsT2, objCut,
          termIndices, termCoeff, termRHS, disjTermFeas[unsorted_cgs_ind],
          disjTermObjVal[unsorted_cgs_ind], num_cgs_used, unsorted_cgs_ind,
          cgsName[num_cgs_used], num_cgs_limit, max_num_cgs, dim, inNBSpace);
      if (num_generated_vpcsT1 + num_generated_vpcsT2 > old_num_cuts) {
        GlobalVariables::numCgsLeadingToCuts++;
        GlobalVariables::numCgsActuallyUsed++;
        num_cgs_used++;
      } else if ((num_obj_tried > old_num_obj_tried)
          && (old_num_primal_infeas
              == GlobalVariables::numCutSolverFails[CutSolverFails::PRIMAL_CUTSOLVER_FAIL_IND])) {
        GlobalVariables::numCgsActuallyUsed++;
        num_cgs_used++;
      } else {
        // Clear out stuff because this cgs was not used?
        cgsName[num_cgs_used] = "";
      }
    } /* if (use_cgs) */
  } /* end loop over unsorted_cgs_ind */

  structVPCs.setOsiCuts();

#ifdef SHOULD_WRITE_PCUTS
  structVPCs.printCuts("VPCs", solver, probData);
#endif

#ifdef SHOULD_WRITE_INTPOINTS
  printf("\n## Save information about the points and rays. ##\n");
  char filename[300];
  snprintf(filename, sizeof(filename) / sizeof(char), "%s-PointsAndRays.csv",
      GlobalVariables::out_f_name_stub.c_str());
  FILE *ptsRaysOut = fopen(filename, "w");
  writeInterPtsAndRays(ptsRaysOut, probData, num_cgs_limit,
      interPtsAndRays.data(), cgsName.data());
  fclose(ptsRaysOut);
#endif
  if (castsolver) {
    delete castsolver;
    castsolver = NULL;
  }
} /* generateCutsFromSplitsOrCross */

void CglVPC::setupCgs(const SolutionInfo& probData, int triangle_ind,
//    const int num_cgs,
    const int max_num_cgs, const int num_splits_per_cgs,
    const std::vector<int>& sortSplitIndex,
    const std::vector<std::vector<bool> >& splitFeasOnSide,
    const std::vector<double>& splitStrongBranchingValue,
    const std::vector<double>& splitHigherBound,
    const std::vector<int>& splitHigherBoundFacet, const double objNormSquared,
    std::vector<int>& sortCgsIndex, std::vector<double>& distToCgs,
    const bool calcDistByMaxFractionality,
    const bool calcDistByStrongBranching,
//    std::vector<std::vector<int> >& splitIndexAllCgs,
    std::vector<int>& triangleIndexAllCgs, std::vector<int>& currFracVar) {
  const int cgs_param = param.getParamVal(
      CglGICParamNamespace::ParamIndices::CGS_PARAM_IND);
  const bool use_splits = (cgs_param == 0);
  const bool use_triangles = (cgs_param > 0) && (cgs_param < 7);
  const bool use_cross = (cgs_param == 7);

  std::vector<int> splitIndexForCgs(num_splits_per_cgs);
  for (int s = 0; s < num_splits_per_cgs; s++) {
    splitIndexForCgs[s] = s;
  }

  for (int cgs_ind = 0; cgs_ind < max_num_cgs; cgs_ind++) {
    // Store index
    sortCgsIndex[cgs_ind] = cgs_ind;

    // Resize stuff
//    currFracVarAllCgs[cgs_ind].resize(num_facets_cgs - 1); // triangles: 2 vars, splits: 1 var
//    termIndicesAllCgs[cgs_ind].resize(num_facets_cgs);
//    termCoeffAllCgs[cgs_ind].resize(num_facets_cgs);
//    termRHSAllCgs[cgs_ind].resize(num_facets_cgs);
    
    // Get fractional variables
    int num_feas_splits_for_this_cgs = num_splits_per_cgs;
    for (int s = 0; s < num_splits_per_cgs; s++) {
      const int split_ind = sortSplitIndex[splitIndexForCgs[s]];
      if (splitFeasOnSide[split_ind][0] && splitFeasOnSide[split_ind][1]) {
        currFracVar[s] = probData.feasSplitVar[split_ind];
      } else {
        currFracVar[s] = -1;
        num_feas_splits_for_this_cgs--;
      }
    }


    /*
    // Get fractional variables
    for (int s = 0; s < num_splits_per_cgs; s++) {
      const int split_ind = sortSplitIndex[splitIndexForCgs[s]];
      //sortSplitIndex[splitIndexAllCgs[cgs_ind][s]];
      currFracVar[s] = probData.feasSplitVar[split_ind];
    }
    */

    if (num_feas_splits_for_this_cgs > 0) {
      if (num_feas_splits_for_this_cgs == 1) { // Use splits
        // -xk >= -floor
        // xk >= ceil
        const int index = (use_splits || currFracVar[0] >= 0) ? 0 : 1;
        if (calcDistByMaxFractionality) {
          const double curr_val = castsolver->getColSolution()[currFracVar[index]];
          const double fk = curr_val - std::floor(curr_val);
          for (int f = 0; f < 2; f++) {
            const double tmpDist = (f == 0) ? fk : 1 - fk;
            if ((f == 0) || lessThanVal(tmpDist, distToCgs[cgs_ind])) {
              distToCgs[cgs_ind] = tmpDist;
            }
          }
        } else if (calcDistByStrongBranching) {
          // Distance will just be the strong branching value
          // That is, it is the best lower-bound obtainable from this split
          distToCgs[cgs_ind] =
            splitStrongBranchingValue[sortSplitIndex[splitIndexForCgs[index]]];
        }
      } else if (use_triangles) { // Use triangles
        triangleIndexAllCgs[cgs_ind] = triangle_ind;
        // -xk1 >= -floor1 || +xk1 >= ceil1
        // -xk2 >= -floor2 || +xk2 >= ceil2
        // (0,0) : + xk1 + xk2 >= ceil1 + ceil2    ||
        // (0,1) : + xk1 - xk2 >= ceil1 - floor2   ||
        // (1,0) : - xk1 + xk2 >= - floor1 + ceil2 ||
        // (1,1) : - xk1 - xk2 >= - floor1 - floor2
        double distToDiagFacet = 0;
        for (int s = 0; s < 2; s++) {
          // Use the bitwise & operator to get the parity of each bit
          // The triangles are numbered 0, 1, 2, 3
          // I.e., 00, 01, 10, 11
          // E.g., t = 1 (01):
          // Facet 0: -x1 >= -floor1
          //   whichMult0 = 0 since (1 & (2 - 0)) = (01 & 2) = (01 & 10) = 0
          //   splitMultiplier[0] = -1
          // Facet 1: x2 >= ceil2
          //   whichMult1 = 1 since (1 & (2 - 1)) = (01 & 1) = (01 * 01) = 1
          //   splitMultiplier[1] = 1
          // Facet 2: + x1 - x2 >= ceil1 - floor2
          if (currFracVar[s] < 0) {
            continue;
          }
          const int whichSideOfSplit = (triangle_ind & (2 - s)) ? 1 : 0;
          double tmpDist = 0.;
          if (calcDistByMaxFractionality) {
            const double curr_val = castsolver->getColSolution()[currFracVar[s]];
            const double fk = curr_val - std::floor(curr_val);
            tmpDist = (whichSideOfSplit == 0) ? fk : 1 - fk;
            distToDiagFacet += (whichSideOfSplit == 0) ? 1 - fk : fk;
          } else if (calcDistByStrongBranching) {
            const int split_ind = sortSplitIndex[splitIndexForCgs[s]];
            tmpDist =
              (whichSideOfSplit == splitHigherBoundFacet[split_ind]) ?
              splitHigherBound[split_ind] :
              splitStrongBranchingValue[split_ind];
          }
          if (lessThanVal(distToCgs[cgs_ind], 0.0) || lessThanVal(tmpDist, distToCgs[cgs_ind])) {
            distToCgs[cgs_ind] = tmpDist;
          }
        }
        if (calcDistByMaxFractionality) {
          distToDiagFacet /= std::sqrt(2);
          if (lessThanVal(distToDiagFacet, distToCgs[cgs_ind])) {
            distToCgs[cgs_ind] = distToDiagFacet;
          }
        }
      } else if (use_cross) { // Use cross
        // 00 : -x1 >= -floor1, -x2 >= -floor2
        // 01 : -x1 >= -floor1, x2 >= ceil2
        // 10 : x1 >= ceil1, -x2 >= -floor2
        // 11 : x1 >= ceil1, x2 >= ceil2
        if (calcDistByMaxFractionality) {
          for (int s = 0; s < 2; s++) {
            const double curr_val = castsolver->getColSolution()[currFracVar[s]];
            const double fk = curr_val - std::floor(curr_val);
            const double tmpDist = (lessThanVal(fk, 0.5)) ? fk : 1 - fk;
            distToCgs[cgs_ind] += tmpDist * tmpDist;
          }
          distToCgs[cgs_ind] = std::sqrt(distToCgs[cgs_ind]);
        } else if (calcDistByStrongBranching) {
          const int split_ind[] = { sortSplitIndex[splitIndexForCgs[0]],
            sortSplitIndex[splitIndexForCgs[1]] };
          const int index_of_first_higher_bound =
            (splitHigherBound[split_ind[0]] < splitHigherBound[split_ind[1]]) ?
            0 : 1;
          const int split1 = split_ind[index_of_first_higher_bound];
          const int split2 = split_ind[!index_of_first_higher_bound];
          const double overallLB =
            (splitStrongBranchingValue[split_ind[0]]
             > splitStrongBranchingValue[split_ind[1]]) ?
            splitStrongBranchingValue[split_ind[0]] :
            splitStrongBranchingValue[split_ind[1]];

          // First split
          const double delta1 =
            (splitHigherBound[split1] - overallLB > 0) ?
            splitHigherBound[split1] - overallLB : 0.;
          const int x1 = probData.feasSplitVar[split1];
          const double val1 = castsolver->getColSolution()[x1];
          const double mult1 = (splitHigherBoundFacet[split1] == 0) ? 1.0 : -1.0;
          const double facet_val1 =
            (splitHigherBoundFacet[split1] == 1) ?
            std::floor(val1) : std::ceil(val1);

          // Second split
          const double delta2 = splitHigherBound[split2]
            - splitHigherBound[split1];
          const int x2 = probData.feasSplitVar[split2];
          const double val2 = castsolver->getColSolution()[x2];
          const double mult2 = (splitHigherBoundFacet[split2] == 0) ? 1.0 : -1.0;
          const double facet_val2 =
            (splitHigherBoundFacet[split2] == 1) ?
            std::floor(val2) : std::ceil(val2);

          // Calc distance
          const double newBeta = overallLB + delta1 * mult1 * facet_val1
            + delta2 * mult2 * facet_val2;
          const double betaAtLPOpt = probData.LPopt + delta1 * mult1 * val1
            + delta2 * mult2 * val2;
          const double betaDelta = newBeta - betaAtLPOpt;
          const double newObjNormSquared = objNormSquared + delta1 * delta1
            + delta2 * delta2 + 2 * delta1 * castsolver->getObjCoefficients()[x1]
            + 2 * delta2 * castsolver->getObjCoefficients()[x2];
          distToCgs[cgs_ind] = betaDelta / std::sqrt(newObjNormSquared);
        }
      }
    }

    if (cgs_ind == max_num_cgs - 1) {
      break;
    }

    //    splitIndexAllCgs[cgs_ind + 1].resize(num_splits_per_cgs);
    if (((cgs_param == 5) || (cgs_param == 6)) && (triangle_ind < 3)) {
      triangle_ind++;
    } else {
      if (num_splits_per_cgs > 1) {
        triangle_ind =
          ((cgs_param == 5) || (cgs_param == 6)) ? 0 : triangle_ind;
        splitIndexForCgs[1] = splitIndexForCgs[1] + 1;
        if (splitIndexForCgs[1] >= probData.numFeasSplits) {
          splitIndexForCgs[0] = splitIndexForCgs[0] + 1;
          splitIndexForCgs[1] = splitIndexForCgs[0] + 1;
        }
        if (splitIndexForCgs[0] >= probData.numFeasSplits - 1) {
          break;
        }
      } else {
        splitIndexForCgs[0] = splitIndexForCgs[0] + 1;
        if (splitIndexForCgs[0] >= probData.numFeasSplits) {
          break;
        }
      }
    }
  } /* end iterating over cgs */
} /* setupCgs */

void CglVPC::setCgsName(std::string& cgsName, const std::string& disjTermName) {
  if (disjTermName.empty()) {
    return;
  }
  if (!cgsName.empty()) {
    cgsName += " V ";
  }
  cgsName += "(";
  cgsName += disjTermName;
  cgsName += ")";
} /* setCgsName (given disj term name) */

void CglVPC::setCgsName(std::string& cgsName, const int num_ineq_per_term,
    const std::vector<std::vector<int> >& termIndices,
    const std::vector<std::vector<double> >& termCoeff,
    const std::vector<double>& termRHS, const bool append) {
  if (num_ineq_per_term == 0) {
    return;
  }
  if (!cgsName.empty()) {
    if (!append) {
      cgsName += " V ";
    } else {
      cgsName.resize(cgsName.size() - 1);
      cgsName += "; ";
    }
  }
  cgsName += append ? "" : "(";
  for (int i = 0; i < num_ineq_per_term; i++) {
    if (i > 0) {
      cgsName += "; ";
    }
    for (int coeff_ind = 0; coeff_ind < (int) termIndices[i].size();
        coeff_ind++) {
      cgsName += (termCoeff[i][coeff_ind] > 0) ? "+" : "-";
      cgsName += "x";
      cgsName += std::to_string(termIndices[i][coeff_ind]);
    }
    cgsName += " >= ";
    cgsName += std::to_string((int) termRHS[i]);
  }
  cgsName += ")";
} /* setCgsName */

/************************************************************************/
/**
 * Assuming we are not working in the subspace (not tested or implemented yet anyway)
 */
void CglVPC::generateCutsHelper(AdvCuts& structVPCs,
    const SolutionInfo& probData, Stats& vpcTimeStats,
    const AdvCuts &structSICs, std::vector<IntersectionInfo>& interPtsAndRays,
    const int num_disj_terms_cgs, int& num_vpcs_per_cgs_T1,
    int& num_generated_vpcsT1, int& num_vpcs_per_cgs_T2,
    int& num_generated_vpcsT2,
    const AdvCut* const objCut,
    const std::vector<std::vector<std::vector<int> > >& termIndices,
    const std::vector<std::vector<std::vector<double> > >& termCoeff,
    const std::vector<std::vector<double> >& termRHS,
    std::vector<bool>& disjTermFeas, std::vector<double>& disjTermObjVal,
    const int cgs_ind, const int unsorted_cgs_ind, const std::string& cgsName,
    const int num_cgs, const int max_num_cgs, const int dim,
    const bool inNBSpace) {
  //if (!(unsorted_cgs_ind == 137 || unsorted_cgs_ind == 396 || unsorted_cgs_ind == 387 || unsorted_cgs_ind == 146)) {
    //printf("SKIP %d\n", cgs_ind); 
  //  return; }
  if (reachedCutLimit(num_vpcs_per_cgs_T1, num_generated_vpcsT1)) {
    return;
  }

  vpcTimeStats.register_name(time_T1 + std::to_string(unsorted_cgs_ind));
  vpcTimeStats.register_name(time_T2 + std::to_string(unsorted_cgs_ind));

  const int cgs_param = param.getParamVal(ParamIndices::CGS_PARAM_IND);
  const bool use_triangles = (cgs_param > 0) && (cgs_param < 7);
  const bool use_cross = (cgs_param == 7);

  // We will first check if all facets of the cut-generating set are feasible
  // If two are infeasible, we simply add the strongest cut available
  // which is termCoefficients >= termRHS
  // and we skip the computationally intensive step of generating points and rays.
  //
  // In the full space, assuming feasibility of the original problem,
  // at least one side of the split will be feasible.
  // In the subspace, we may end up with both sides of the split being infeasible.
  // For the time being, we will skip these splits.
  //  std::vector<bool> disjTermFeas(num_disj_terms_cgs, true);
  int numFeasDisjTerms = num_disj_terms_cgs;
  bool existsTermWithStrictlyBetterObjVal = false;

  // Set up solvers for facets of S
  const bool splitVarDeleted = param.getParamVal(
      CglGICParamNamespace::ParamIndices::SPLIT_VAR_DELETED_PARAM_IND);
  std::vector<PointCutsSolverInterface*> tmpSolver(num_disj_terms_cgs);

  // Skip facets that already have a point-ray collection
  std::vector<bool> calcDisjTerm(num_disj_terms_cgs);
  std::vector<bool> calcAndFeasFacet(num_disj_terms_cgs);

  // For the cobasis elements
  std::vector<std::vector<int> > tmpVarBasicInRow(num_disj_terms_cgs);
  std::vector<std::vector<int> > tmpBasicStructVar(num_disj_terms_cgs);
  std::vector<std::vector<int> > tmpBasicSlackVar(num_disj_terms_cgs);
  std::vector<std::vector<int> > rowOfTmpVar(num_disj_terms_cgs);
  std::vector<std::vector<int> > rowOfOrigNBVar(num_disj_terms_cgs);
  std::vector<std::vector<int> > tmpNonBasicVarIndex(num_disj_terms_cgs);
  std::vector<std::vector<std::vector<double> > > currBInvACol(
      num_disj_terms_cgs);
  std::vector<std::vector<std::vector<double> > > currRay(num_disj_terms_cgs);

  /***********************************************************************************
   * Check feasibility of splits and generate cuts if both sides of a split feasible
   ***********************************************************************************/
  for (int t = 0; t < num_disj_terms_cgs; t++) {
    calcDisjTerm[t] = (interPtsAndRays[t].getNumRows() == 0) && disjTermFeas[t];
    if (calcDisjTerm[t]) {
#ifdef TRACE
      printf(
          "\n## Calculating optimal solution on disj term %d of cgs %d/%d with name %s. ##\n",
          t, unsorted_cgs_ind + 1, max_num_cgs, cgsName.c_str());
#endif
      tmpSolver[t] = dynamic_cast<PointCutsSolverInterface*>(solver->clone());
      setClpParameters(tmpSolver[t]);
      tmpSolver[t]->disableFactorization();

      if (t < 2 && splitVarDeleted) {
        // Get the column corresponding to the split variable
        const CoinShallowPackedVector fracCol =
            castsolver->getMatrixByCol()->getVector(termIndices[t][0][0]);
        disjTermFeas[t] = removeFixedVar(tmpSolver[t], termIndices[t][0][0],
            termRHS[t][0], fracCol);
      } else {
        disjTermFeas[t] = fixDisjTerm(tmpSolver[t], termIndices[t],
            termCoeff[t], termRHS[t]);
      }

      if (param.getParamVal(ParamIndices::STRENGTHEN_PARAM_IND) == 2
          && disjTermFeas[t]) {
        // Add Gomory cuts on disjunctive term and resolve
        OsiCuts GMICs;
        CglGMI GMIGen(param);
        GMIGen.generateCuts(*tmpSolver[t], GMICs);
        tmpSolver[t]->applyCuts(GMICs);
        tmpSolver[t]->resolve();
        disjTermFeas[t] = checkSolverOptimality(tmpSolver[t], true);
      }
      disjTermObjVal[t] = tmpSolver[t]->getObjValue();
      if (tmpSolver[t]->getObjValue() - castsolver->getObjValue() < this->min_nb_obj_val) {
        this->min_nb_obj_val = tmpSolver[t]->getObjValue()
            - castsolver->getObjValue(); //getNBObjValue(tmpSolver[t], castsolver, probData);
      }

    }
    calcAndFeasFacet[t] = calcDisjTerm[t] && disjTermFeas[t];

    if (!disjTermFeas[t]) {
      numFeasDisjTerms--;
    }

    // Identify the cobasis and T1 PR collection (if time has not run out)
    if (calcAndFeasFacet[t]) {
      // Factorization used in various subroutines
      // May speed up things to not have to enable/disable it every time
      enableFactorization(tmpSolver[t]);
      identifyCobasis(tmpVarBasicInRow[t], tmpBasicStructVar[t],
          tmpBasicSlackVar[t], rowOfTmpVar[t], rowOfOrigNBVar[t],
          tmpNonBasicVarIndex[t], currBInvACol[t], currRay[t], tmpSolver[t],
          probData, termIndices[t][0][0], splitVarDeleted, true);

      /***********************************************************************************
       * Generate type-1 points and rays
       ***********************************************************************************/
      // We check time limit after cobasis in case type2 cuts are still being generated
      // (they use the above information)
      if (!reachedTimeLimit(vpcTimeStats, time_T1 + "TOTAL",
          param.getTIMELIMIT())) {
        vpcTimeStats.start_timer(time_T1 + "TOTAL");
        vpcTimeStats.start_timer(time_T1 + std::to_string(unsorted_cgs_ind));

        genDepth1PRCollection(interPtsAndRays[t], tmpSolver[t], probData,
            termIndices[t], tmpVarBasicInRow[t], tmpBasicStructVar[t],
            tmpBasicSlackVar[t], rowOfTmpVar[t], rowOfOrigNBVar[t],
            tmpNonBasicVarIndex[t], currRay[t], t, splitVarDeleted,
            unsorted_cgs_ind, max_num_cgs);

        vpcTimeStats.end_timer(time_T1 + "TOTAL");
        vpcTimeStats.end_timer(time_T1 + std::to_string(unsorted_cgs_ind));

        // Check that the points and rays we have generated satisfy the objective cut
        // Now done when creating the points and rays
//        checkPRCollectionForFeasibility(t, interPtsAndRays[t], nb_obj_val, objCut);
      } /* check time limit is not reached */
    } /* calcAndFeasFacet */

    if (!existsTermWithStrictlyBetterObjVal
        && !isVal(disjTermObjVal[t], castsolver->getObjValue())) {
      existsTermWithStrictlyBetterObjVal = true;
    }
  } /* end loop over disjunctive terms */

  if (numFeasDisjTerms == num_disj_terms_cgs - 1) {
    GlobalVariables::numCgsOneDisjTermInfeas++;
  }

  // If all sides are infeasible, and we are working in the subspace, we simply skip this cgs.
  // If not in subspace, then the entire problem is then infeasible.
  // For the splits that are infeasible on one side, just add the strongest cut available,
  // which fixes the integer variable to the other side.
  if (numFeasDisjTerms == 0) {
    if (!param.getParamVal(ParamIndices::SUBSPACE_PARAM_IND)) {
      error_msg(errstr,
          "Solver is primal infeasible when solved on each of the facets of a cut-generating set (cgs %d/%d with name %s).\n",
          unsorted_cgs_ind + 1, max_num_cgs, cgsName.c_str());
      writeErrorToLog(errstr, GlobalVariables::log_file);
      exit(1);
    }
  } else if (numFeasDisjTerms != num_disj_terms_cgs) {
    // One-sided cuts from infeasible splits have already been added
    int num_added = 0;
    int t = -1;
    if (use_triangles) {
      // If using triangles, need to check if diagonal facet is infeasible
      if (!disjTermFeas[2]) {
        t = 2;
        num_added += addOneSidedCut(structVPCs, castsolver,
            (int) termIndices[t][0].size(), termIndices[t][0].data(),
            termCoeff[t][0].data(), termRHS[t][0], cgs_ind, cgsName);
      }
    }
    if (use_cross && (numFeasDisjTerms == 1)) {
      // When using cross, either add restriction to one disjunctive term
      // (if only one is feasible)
      // or restrict to one side of a split if not already added
      // (not currently implemented)
      for (t = 0; t < num_disj_terms_cgs; t++) {
        if (disjTermFeas[t]) {
          num_added += fixToDisjTerm(structVPCs, castsolver, termIndices[t],
              termCoeff[t], termRHS[t], cgs_ind, cgsName);
          break;
        }
      }
    }
    if (num_added > 0) {
#ifdef TRACE
      printf(
          "\n## Generated one-sided VPC excluding infeasible term %d of cgs %d/%d with name %s. ##\n",
          t, cgs_ind + 1, num_cgs, cgsName.c_str());
#endif
      num_vpcs_per_cgs_T1 += num_added;
      num_generated_vpcsT1 += num_added;
    }
  }

  const bool isTimeLimitReachedT1 = reachedTimeLimit(
      vpcTimeStats, time_T1 + "TOTAL", param.getTIMELIMIT());
  const bool isTimeLimitReachedT2 = reachedTimeLimit(
      vpcTimeStats, time_T2 + "TOTAL", param.getTIMELIMIT());
  const bool isTimeLimitReached =
      (param.getParamVal(ParamIndices::VPC_DEPTH_PARAM_IND) >= 2) ?
          isTimeLimitReachedT1 && isTimeLimitReachedT2 : isTimeLimitReachedT1;
  if ((numFeasDisjTerms > 1) && existsTermWithStrictlyBetterObjVal && !isTimeLimitReached) {
    // If at least two sides are feasible, we proceed (only if cuts CAN be generated)
    generateVPCsT1(structVPCs, probData,
//        tmpVarBasicInRow,
//        tmpBasicStructVar, tmpBasicSlackVar, rowOfTmpVar, rowOfOrigNBVar,
//        tmpNonBasicVarIndex, currBInvACol, currRay,
        vpcTimeStats, structSICs,
        interPtsAndRays, num_vpcs_per_cgs_T1, num_generated_vpcsT1, objCut,
//        termIndices, termCoeff, termRHS,
        calcAndFeasFacet, cgs_ind, unsorted_cgs_ind, cgsName,
//        num_cgs,
        max_num_cgs, dim, inNBSpace);

    if ((param.getParamVal(ParamIndices::VPC_DEPTH_PARAM_IND) >= 2)
        && !reachedCutLimit(num_vpcs_per_cgs_T2, num_generated_vpcsT2)
        && !reachedTimeLimit(vpcTimeStats, time_T2 + "TOTAL",
            param.getTIMELIMIT())) {
      generateVPCsT2(structVPCs, probData, tmpSolver, tmpVarBasicInRow,
          tmpBasicStructVar, tmpBasicSlackVar, rowOfTmpVar, rowOfOrigNBVar,
          tmpNonBasicVarIndex, currBInvACol, currRay, vpcTimeStats, structSICs,
          interPtsAndRays, num_vpcs_per_cgs_T2, num_generated_vpcsT2, objCut,
          termIndices, termCoeff, termRHS, calcAndFeasFacet, cgs_ind,
          unsorted_cgs_ind, cgsName, num_cgs, max_num_cgs, dim, inNBSpace);
    }
  }

  for (int t = 0; t < num_disj_terms_cgs; t++) {
    if (tmpSolver[t]) {
      delete tmpSolver[t];
    }
  }
} /* generateCutsHelper */

/********************/
/** VPC GENERATION **/
/********************/
/**
 * Generate type-1 VPCs
 */
void CglVPC::generateVPCsT1(AdvCuts& structVPCs, const SolutionInfo& probData,
//    std::vector<std::vector<int> >& tmpVarBasicInRow,
//    std::vector<std::vector<int> >& tmpBasicStructVar,
//    std::vector<std::vector<int> >& tmpBasicSlackVar,
//    std::vector<std::vector<int> >& rowOfTmpVar,
//    std::vector<std::vector<int> >& rowOfOrigNBVar,
//    std::vector<std::vector<int> >& tmpNonBasicVarIndex,
//    std::vector<std::vector<std::vector<double> > >& currBInvACol,
//    std::vector<std::vector<std::vector<double> > >& currRay,
    Stats& vpcTimeStats, const AdvCuts& structSICs,
    std::vector<IntersectionInfo>& interPtsAndRays, int& num_vpcs_per_cgs_T1,
    int& num_generated_vpcsT1, const AdvCut* const objCut,
//    const std::vector<std::vector<std::vector<int> > >& termIndices,
//    const std::vector<std::vector<std::vector<double> > >& termCoefficients,
//    const std::vector<std::vector<double> >& termRHS,
    const std::vector<bool>& calcAndFeasFacet, const int cgs_ind,
    const int unsorted_cgs_ind, const std::string& cgsName,
//    const int num_cgs,
    const int max_num_cgs, const int dim, const bool inNBSpace) {
//  const bool splitVarDeleted = param.getParamVal(
//      CglGICParamNamespace::ParamIndices::SPLIT_VAR_DELETED_PARAM_IND);
//  const int currFracVar = probData.feasSplitVar[cgs_ind];
//  const int num_disj_terms_cgs = interPtsAndRays.size();

  // We check this after in case type2 cuts are still being generated
  // (they use the above information)
  if (reachedTimeLimit(vpcTimeStats, time_T1 + "TOTAL", param.getTIMELIMIT())) {
    return;
  }

  vpcTimeStats.start_timer(time_T1 + "TOTAL");
  vpcTimeStats.start_timer(time_T1 + std::to_string(unsorted_cgs_ind));

  analyzePRCollection(interPtsAndRays, objCut, calcAndFeasFacet);

  /***********************************************************************************
   * Generate cuts from type-1 points and rays
   ***********************************************************************************/
  //if (!isZero(param.getTEMP())) {
  printf(
      "\n## Generating type-1 point cuts for cgs %d/%d. (%d cuts previously generated.) ##\n",
      unsorted_cgs_ind + 1, max_num_cgs,
      (int) structVPCs.cuts.size());
  tryObjectivesForCgs(structVPCs, num_vpcs_per_cgs_T1, num_generated_vpcsT1,
      num_obj_tried, probData, dim, inNBSpace, cgs_ind, 
      cgsName, interPtsAndRays, probData, structSICs, 
      vpcTimeStats, time_T1 + "TOTAL", false, objCut);

  vpcTimeStats.end_timer(time_T1 + std::to_string(unsorted_cgs_ind));
  vpcTimeStats.end_timer(time_T1 + "TOTAL");
} /* generateVPCsT1 */

/**
 * Generate type-2 VPCs
 */
void CglVPC::generateVPCsT2(AdvCuts& structVPCs, const SolutionInfo& probData,
    const std::vector<PointCutsSolverInterface*>& tmpSolver,
    std::vector<std::vector<int> >& tmpVarBasicInRow,
    std::vector<std::vector<int> >& tmpBasicStructVar,
    std::vector<std::vector<int> >& tmpBasicSlackVar,
    std::vector<std::vector<int> >& rowOfTmpVar,
    std::vector<std::vector<int> >& rowOfOrigNBVar,
    std::vector<std::vector<int> >& tmpNonBasicVarIndex,
    std::vector<std::vector<std::vector<double> > >& currBInvACol,
    std::vector<std::vector<std::vector<double> > >& currRay,
    Stats& vpcTimeStats, const AdvCuts& structSICs,
    const std::vector<IntersectionInfo>& interPtsAndRays, int& num_vpcs_per_cgs_T2,
    int& num_generated_vpcsT2, const AdvCut* const objCut,
    const std::vector<std::vector<std::vector<int> > >& termIndices,
    const std::vector<std::vector<std::vector<double> > >& termCoefficients,
    const std::vector<std::vector<double> >& termRHS,
    const std::vector<bool>& calcAndFeasFacet, const int cgs_ind,
    const int unsorted_cgs_ind, const std::string& cgsName, const int num_cgs,
    const int max_num_cgs, const int dim, const bool inNBSpace) {
  const bool splitVarDeleted = param.getParamVal(
      CglGICParamNamespace::ParamIndices::SPLIT_VAR_DELETED_PARAM_IND);
//  const int currFracVar = probData.feasSplitVar[cgs_ind];
  const int num_facets_cgs = tmpSolver.size();
  if (num_facets_cgs > 2) {
    warning_msg(warnstr, "VPCs of type 2 only currently work with 2 facets.\n");
    return;
  }

  vpcTimeStats.start_timer(time_T2 + "TOTAL");
  vpcTimeStats.start_timer(time_T2 + std::to_string(unsorted_cgs_ind));

  /***********************************************************************************
   * Find pivots along each ray
   ***********************************************************************************/
  //        std::vector<std::vector<LiftGICsSolverInterface*> > pivotSolver(2);
  std::vector<std::vector<int> > varOut(2), dirnOut(2), pivotRow(2);
  std::vector<std::vector<double> > distToPivot(2);
  std::vector<std::vector<double> > pivotObj(2);
  //        std::vector<std::vector<int> > varOutNonDegen(2), dirnOutNonDegen(2), pivotRowNonDegen(2);
  //        std::vector<std::vector<double> > distToPivotNonDegen(2);
  for (int f = 0; f < num_facets_cgs; f++) {
    findPivots(varOut[f], dirnOut[f], pivotRow[f], distToPivot[f], pivotObj[f],
        tmpNonBasicVarIndex[f], tmpSolver[f], tmpVarBasicInRow[f], termIndices[f][0][0],
        splitVarDeleted);
  }

  /***********************************************************************************
   * Sort the rays based on cost
   ***********************************************************************************/
  std::vector<std::vector<double> > pivotOpt(num_facets_cgs);
  std::vector<std::vector<int> > sortIndexRays(num_facets_cgs);
  const int numTmpNBVars = tmpNonBasicVarIndex[0].size(); // Same for both
  for (int f = 0; f < num_facets_cgs; f++) {
    sortIndexRays[f].reserve(numTmpNBVars);
    pivotOpt[f].reserve(numTmpNBVars);
    for (int nb_ind = 0; nb_ind < numTmpNBVars; nb_ind++) {
      if (varOut[f][nb_ind] >= 0) {
        sortIndexRays[f].push_back(nb_ind);
        pivotOpt[f].push_back(pivotObj[f][nb_ind]);
      }
    }
    sort(sortIndexRays[f].begin(), sortIndexRays[f].end(),
        index_cmp_asc<double*>(pivotOpt[f].data())); // Since this is a minimization problem
  }

  /***********************************************************************************
   * Generate type-2 points and rays
   ***********************************************************************************/
  if (param.getParamVal(ParamIndices::VPC_DEPTH2_HEUR_PARAM_IND) == 0) { // Pivot procedure
    std::vector<std::vector<int> > currTmpVarBasicInRow(num_facets_cgs);
    std::vector<std::vector<int> > currTmpBasicStructVar(num_facets_cgs),
        currTmpBasicSlackVar(num_facets_cgs);
    std::vector<std::vector<int> > currRowOfTmpVar(num_facets_cgs),
        currRowOfOrigNBVar(num_facets_cgs);
    std::vector<std::vector<int> > currTmpNonBasicVarIndex(num_facets_cgs);
    std::vector<std::vector<std::vector<double> > > currCurrBInvACol(
        num_facets_cgs), currCurrRay(num_facets_cgs);
    std::vector<int> currRayTmpVar(num_facets_cgs);

    const int numRaysToCutParamVal =
        (param.getParamVal(ParamIndices::NUM_RAYS_CUT_PARAM_IND) >= 0) ?
            param.getParamVal(ParamIndices::NUM_RAYS_CUT_PARAM_IND) :
            static_cast<int>(std::sqrt(solver->getNumCols()))
                / (-1 * param.getParamVal(ParamIndices::NUM_RAYS_CUT_PARAM_IND));
    std::vector<int> numRaysToCut(num_facets_cgs);
    for (int f = 0; f < num_facets_cgs; f++) {
      numRaysToCut[f] =
          ((numRaysToCutParamVal > 0)
              && (numRaysToCutParamVal < (int) sortIndexRays[f].size())) ?
              numRaysToCutParamVal : sortIndexRays[0].size();
    }

    for (int r0 = 0; r0 < numRaysToCut[0]; r0++) {
      if (reachedCutLimit(num_vpcs_per_cgs_T2, num_generated_vpcsT2)
          || reachedTimeLimit(vpcTimeStats,
              time_T2 + std::to_string(unsorted_cgs_ind),
              param.getTIMELIMIT() / probData.numFeasSplits)) {
        break;
      }
      const int ray_ind0 = sortIndexRays[0][r0];
      int f = 0;
      // Pivot to the nearest neighbor along this ray
      int ray_ind = ray_ind0;
      if (varOut[0][ray_ind0] < 0) { // This ray and all after it are extreme (or something happened with the pivot)
        break;
      }

      std::vector<IntersectionInfo> currInterPtsAndRays(num_facets_cgs);
      // Reverse ordering so packed matrix is row ordered and set number of columns
      currInterPtsAndRays[f].reverseOrdering();
      currInterPtsAndRays[f].setDimensions(0, dim);
      currInterPtsAndRays[f].resizeInfoByActivationRound(0);

      // Set up pivot
      std::vector<PointCutsSolverInterface*> pivotSolver(num_facets_cgs);
      pivotSolver[f] =
          dynamic_cast<PointCutsSolverInterface*>(tmpSolver[f]->clone());
      pivotSolver[f]->enableSimplexInterface(true);
      const int varIn = tmpNonBasicVarIndex[f][ray_ind];
      const int dirnIn =
          (pivotSolver[f]->getModelPtr()->getStatus(varIn)
              == ClpSimplex::Status::atUpperBound) ? -1 : 1;
      int varOutFloor, dirnOutFloor, pivotRowFloor;
      double distFloor;
      const int return_code = pivot(pivotSolver[f], varIn, dirnIn, varOutFloor,
          dirnOutFloor, pivotRowFloor, distFloor);

      if ((return_code < 0) || (varOutFloor != varOut[f][ray_ind])
          || (dirnOutFloor != dirnOut[f][ray_ind])
          || (pivotRowFloor != pivotRow[f][ray_ind])
          || !isVal(distFloor, distToPivot[f][ray_ind])) {
        error_msg(errorstring,
            "Pivot on term %d ray %d (var %d) does not have same result as before.\n",
            f, ray_ind, varIn);
        writeErrorToLog(errorstring, GlobalVariables::log_file);
        exit(1);
      }

      currRayTmpVar[f] = tmpNonBasicVarIndex[f][ray_ind];
      pivotHelper(currInterPtsAndRays[f], pivotSolver[f],
          currTmpVarBasicInRow[f], currTmpBasicStructVar[f],
          currTmpBasicSlackVar[f], currRowOfTmpVar[f], currRowOfOrigNBVar[f],
          currTmpNonBasicVarIndex[f], currCurrBInvACol[f], currCurrRay[f],
          varOut[f][ray_ind], pivotRow[f][ray_ind], interPtsAndRays[f],
          tmpSolver[f], probData, f, ray_ind, termIndices[f][0][0],
          splitVarDeleted);

      for (int r1 = 0; r1 < numRaysToCut[1]; r1++) {
        if (reachedCutLimit(num_vpcs_per_cgs_T2, num_generated_vpcsT2)
            || reachedTimeLimit(vpcTimeStats,
                time_T2 + std::to_string(unsorted_cgs_ind),
                param.getTIMELIMIT() / probData.numFeasSplits)) {
          break;
        }
        const int ray_ind1 = sortIndexRays[1][r1];
        f = 1;
        ray_ind = ray_ind1;

        if (varOut[1][ray_ind1] < 0) { // This ray and all after it are extreme (or something happened with the pivot)
          break;
        }

        // Reverse ordering so packed matrix is row ordered and set number of columns
        IntersectionInfo tmpInterPtsAndRays;
        tmpInterPtsAndRays.reverseOrdering();
        tmpInterPtsAndRays.setDimensions(0, dim);
        tmpInterPtsAndRays.resizeInfoByActivationRound(0);
        currInterPtsAndRays[f] = tmpInterPtsAndRays;

        // Pivot to the nearest neighbor along this ray
        currRayTmpVar[f] = tmpNonBasicVarIndex[f][ray_ind];

        // Set up pivot
        pivotSolver[f] =
            dynamic_cast<PointCutsSolverInterface*>(tmpSolver[f]->clone());
        pivotSolver[f]->enableSimplexInterface(true);
        const int varIn = tmpNonBasicVarIndex[f][ray_ind];
        const int dirnIn =
            (pivotSolver[f]->getModelPtr()->getStatus(varIn)
                == ClpSimplex::Status::atUpperBound) ? -1 : 1;
        int varOutCeil, dirnOutCeil, pivotRowCeil;
        double distCeil;
        const int return_code = pivot(pivotSolver[f], varIn, dirnIn, varOutCeil,
            dirnOutCeil, pivotRowCeil, distCeil);
        if ((return_code < 0) || (varOutCeil != varOut[f][ray_ind])
            || (dirnOutCeil != dirnOut[f][ray_ind])
            || (pivotRowCeil != pivotRow[f][ray_ind])
            || !isVal(distCeil, distToPivot[f][ray_ind])) {
          error_msg(errorstring,
              "Pivot on term %d ray %d (var %d) does not have same result as before.\n",
              f, ray_ind, varIn);
          writeErrorToLog(errorstring, GlobalVariables::log_file);
          exit(1);
        }

        currRayTmpVar[f] = tmpNonBasicVarIndex[f][ray_ind];
        pivotHelper(currInterPtsAndRays[f], pivotSolver[f],
            currTmpVarBasicInRow[f], currTmpBasicStructVar[f],
            currTmpBasicSlackVar[f], currRowOfTmpVar[f], currRowOfOrigNBVar[f],
            currTmpNonBasicVarIndex[f], currCurrBInvACol[f], currCurrRay[f],
            varOut[f][ray_ind], pivotRow[f][ray_ind], interPtsAndRays[f],
            tmpSolver[f], probData, f, ray_ind, termIndices[f][0][0],
            splitVarDeleted);

        std::vector<bool> calcDisjTerm(num_facets_cgs, true);
        calcDisjTerm[0] = (r1 == 0) && (pivotRow[0][ray_ind0] >= 0);
        calcDisjTerm[1] = (pivotRow[1][ray_ind1] >= 0);
        for (int f = 0; f < 2; f++) {
          if (!calcDisjTerm[f]) {
            continue;
          }
          genDepth1PRCollection(currInterPtsAndRays[f], pivotSolver[f],
              probData, termIndices[f], currTmpVarBasicInRow[f],
              currTmpBasicStructVar[f], currTmpBasicSlackVar[f],
              currRowOfTmpVar[f], currRowOfOrigNBVar[f],
              currTmpNonBasicVarIndex[f], currCurrRay[f], f, splitVarDeleted,
              unsorted_cgs_ind, max_num_cgs);
        }

        /***********************************************************************************
         * Generate cuts from type-2 points and rays
         ***********************************************************************************/
        printf(
            "\n## Generating type-2 point cuts after pivoting (floor ray %d/%d, ceil ray %d/%d) "
                "(in or curr ray, out or curr hplane, status = 1 for ub and -1 for lb):\n"
                "\t(%d,%d,%d) on facet 0 and (%d,%d,%d) on facet 1 of cgs %d/%d. ##\n",
            r0 + 1, numRaysToCut[0], r1 + 1, numRaysToCut[1], currRayTmpVar[0],
            varOut[0][ray_ind0], dirnOut[0][ray_ind0], currRayTmpVar[1],
            varOut[1][ray_ind1], dirnOut[1][ray_ind1], unsorted_cgs_ind + 1,
            max_num_cgs);
        tryObjectivesForCgs(structVPCs, num_vpcs_per_cgs_T2, num_generated_vpcsT2,
            num_obj_tried, probData, dim, inNBSpace, cgs_ind,
            cgsName, currInterPtsAndRays, probData, structSICs,
            vpcTimeStats, time_T2 + "TOTAL", true);

        //            // Pivot back
        //            tmpSolver[f]->enableSimplexInterface(true);
        //            tmpSolver[f]->pivot(hplaneTmpVar[f], currRayTmpVar[f],
        //                backStatus[f]);
        //            tmpSolver[f]->disableSimplexInterface();
        //            tmpSolver[f]->resolve(); // because the values of the variables are wrong sometimes

        // Free the pivot solver data
        if (pivotSolver[1]) {
          pivotSolver[1]->disableSimplexInterface();
          delete pivotSolver[1];
        }
      } /* iterate over second set of rays */
      //          f = 0;
      //          // Pivot back
      //          tmpSolver[f]->enableSimplexInterface(true);
      //          tmpSolver[f]->pivot(hplaneTmpVar[f], currRayTmpVar[f], backStatus[f]);
      //          tmpSolver[f]->disableSimplexInterface();
      //          tmpSolver[f]->resolve(); // because the values of the variables are wrong sometimes
      
      // Free the pivot solver data
      if (pivotSolver[0]) {
        pivotSolver[0]->disableSimplexInterface();
        delete pivotSolver[0];
      }
    } /* iterate over rays to cut */
  } else if (param.getParamVal(ParamIndices::VPC_DEPTH2_HEUR_PARAM_IND) == 1) {  // activate hyperplane procedure
    /***********************************************************************************
     * Find hyperplanes that need to be activated for floor and ceiling
     ***********************************************************************************/
    std::vector<std::vector<Hplane> > hplanesToAct(2); // hyperplanes will be in tmpSolver space
    std::vector<std::vector<int> > firstHplane(2); // index in hplanesToAct of the first hplane to intersect the ray
    std::vector<std::vector<std::vector<double> > > distToHplane(2); // [facet][hplane][ray]
    for (int f = 0; f < 2; f++) {
      chooseHplanesDepth2VPCs(hplanesToAct[f], firstHplane[f], distToHplane[f],
          tmpSolver[f], probData, tmpVarBasicInRow[f], tmpBasicStructVar[f],
          tmpBasicSlackVar[f], rowOfTmpVar[f], rowOfOrigNBVar[f],
          tmpNonBasicVarIndex[f], currRay[f], f, cgs_ind, termIndices[f][0][0],
          splitVarDeleted);
    }

    /***********************************************************************************
     * Sort and filter down the hyperplanes to some reasonable limit
     ***********************************************************************************/
    std::vector<std::vector<int> > numIntersections(2);
    std::vector<std::vector<int> > sortIndex(2);
    for (int f = 0; f < 2; f++) {
      const int numHplanes = hplanesToAct[f].size();
      sortIndex[f].resize(numHplanes);
      numIntersections[f].resize(numHplanes);
      for (int h = 0; h < numHplanes; h++) {
        sortIndex[f][h] = h;
        numIntersections[f][h] =
            hplanesToAct[f][h].rayToBeCutByHplane[0].size();
      }
      sort(sortIndex[f].begin(), sortIndex[f].end(),
          index_cmp_dsc<int*>(numIntersections[f].data()));
    }

    std::vector<IntersectionInfo> currInterPtsAndRays(2);
    for (int f = 0; f < num_facets_cgs; f++) {
      currInterPtsAndRays[f] = interPtsAndRays[f];
    }
    const int numHplanesToChoosePerFacet = param.getParamVal(
        ParamIndices::MAX_HPLANES_PARAM_IND);
    for (int h0_tmp = 0; h0_tmp < numHplanesToChoosePerFacet; h0_tmp++) {
      if (h0_tmp >= (int) hplanesToAct[0].size()) {
        break;
      }

      if (reachedCutLimit(num_vpcs_per_cgs_T2, num_generated_vpcsT2)
          || reachedTimeLimit(vpcTimeStats,
              time_T2 + std::to_string(unsorted_cgs_ind),
              param.getTIMELIMIT() / probData.numFeasSplits)) {
        break;
      }
      const int h0 = sortIndex[0][h0_tmp];
      // Activate hyperplane h1 on the floor side of the split
      int facet_ind = 0;
      activateHplane(currInterPtsAndRays[facet_ind],
          distToHplane[facet_ind][h0], tmpSolver[facet_ind], probData,
          hplanesToAct[facet_ind][h0], h0, rowOfTmpVar[facet_ind],
          rowOfOrigNBVar[facet_ind], tmpNonBasicVarIndex[facet_ind],
          currBInvACol[facet_ind], currRay[facet_ind],
          termIndices[facet_ind][0][0], termRHS[facet_ind][0], facet_ind,
          splitVarDeleted);

      for (int h1_tmp = 0; h1_tmp < numHplanesToChoosePerFacet; h1_tmp++) {
        if (h1_tmp >= (int) hplanesToAct[1].size()) {
          break;
        }
        if (reachedCutLimit(num_vpcs_per_cgs_T2, num_generated_vpcsT2)
            || reachedTimeLimit(vpcTimeStats,
                time_T2 + std::to_string(unsorted_cgs_ind),
                param.getTIMELIMIT() / probData.numFeasSplits)) {
          break;
        }
        const int h1 = sortIndex[1][h1_tmp];
        // Activate hyperplane h2 on the ceil side of the split
        facet_ind = 1;
        activateHplane(currInterPtsAndRays[facet_ind],
            distToHplane[facet_ind][h1], tmpSolver[facet_ind], probData,
            hplanesToAct[facet_ind][h1], h1, rowOfTmpVar[facet_ind],
            rowOfOrigNBVar[facet_ind], tmpNonBasicVarIndex[facet_ind],
            currBInvACol[facet_ind], currRay[facet_ind],
            termIndices[facet_ind][0][0], termRHS[facet_ind][0], facet_ind,
            splitVarDeleted);

        /***********************************************************************************
         * Generate cuts from type-2 points and rays
         ***********************************************************************************/
        printf(
            "\n## Generating point cuts after activating hyperplane %d on facet 0 "
                "and %d (max %d) on facet 1 of term %d/%d. ##\n",
            h0_tmp, h1_tmp, numHplanesToChoosePerFacet, unsorted_cgs_ind + 1,
            max_num_cgs);
        tryObjectivesForCgs(structVPCs, num_vpcs_per_cgs_T2, num_generated_vpcsT2,
            num_obj_tried, probData, dim, inNBSpace, cgs_ind,
            cgsName, currInterPtsAndRays, probData, structSICs, 
            vpcTimeStats, time_T2 + "TOTAL", false);

        // Return intersection points and ray to original state
        currInterPtsAndRays[1] = interPtsAndRays[1];
      }
      // Return intersection points and ray to original state
      currInterPtsAndRays[0] = interPtsAndRays[0];
    }
  }

  vpcTimeStats.end_timer(time_T2 + std::to_string(unsorted_cgs_ind));
  vpcTimeStats.end_timer(time_T2 + "TOTAL");
} /* generateVPCsT2 */

/**********************/
/** POINT GENERATION **/
/**********************/

/**
 * Generate type-1 points and rays
 */
void CglVPC::genDepth1PRCollection(
    IntersectionInfo& interPtsAndRays,
    const PointCutsSolverInterface* tmpSolver,
    const SolutionInfo& origProbData,
    const std::vector<std::vector<int> >& termIndices,
    const std::vector<int>& varBasicInRow,
    const std::vector<int>& basicStructVar,
    const std::vector<int>& basicSlackVar,
    const std::vector<int>& rowOfVar,
    const std::vector<int>& rowOfOrigNBVar,
    const std::vector<int>& nonBasicVarIndex,
    const std::vector<std::vector<double> >& currRay,
    const int f, const bool splitVarDeleted,
    const int cgs_ind, const int num_cgs) {
#ifdef TRACE
  printf(
      "\n## Generating relaxed corner points and rays from term %d of cut-generating set %d/%d. ##\n",
      f, cgs_ind + 1, num_cgs);
#endif
  if (!param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND)) {
    genCorner(interPtsAndRays, termIndices[0][0], tmpSolver, f,
        basicStructVar, rowOfVar, rowOfOrigNBVar, nonBasicVarIndex,
        currRay, splitVarDeleted);
  } else {
    genCornerNB(interPtsAndRays, termIndices[0][0], tmpSolver,
        origProbData, f, basicStructVar, rowOfVar, rowOfOrigNBVar,
        nonBasicVarIndex, currRay, splitVarDeleted);
  }
} /* genDepth1PRCollection */

/**********************************************************/
/**
 * IN STRUCTURAL SPACE
 * Get Point and Rays from corner polyhedron defined by current optimum at solver
 * Assumed to be optimal already
 */
void CglVPC::genCorner(IntersectionInfo& interPtsAndRays, const int split_var,
    const PointCutsSolverInterface* const tmpSolver, const int facet_ind,
    const std::vector<int>& basicStructVar, const std::vector<int>& rowOfVar,
    const std::vector<int>& rowOfOrigNBVar,
    const std::vector<int>& nonBasicVarIndex,
    const std::vector<std::vector<double> >& currRay,
    const bool splitVarDeleted) {
//  const int solverFactorizationStatus = tmpSolver->canDoSimplexInterface();
//  if (solverFactorizationStatus == 0) {
//    tmpSolver->enableFactorization();
//  }
  // Ensure solver is optimal
  if (!tmpSolver->isProvenOptimal()) {
    error_msg(errstr, "Solver is not proven optimal.\n");
    writeErrorToLog(errstr, GlobalVariables::log_file);
    exit(1);
  }

  /***********************************************************************************
   * The first point is simply the first vertex
   ***********************************************************************************/
  std::vector<int> pointIndex;
  std::vector<double> pointVal;
  for (int var = 0; var < tmpSolver->getNumCols(); var++) {
    const double curr_val = tmpSolver->getColSolution()[var];
    const int origVarIndex =
        (!splitVarDeleted || (var < split_var)) ? var : var + 1;
    if (splitVarDeleted && (var == split_var)
        && !isZero(curr_val, param.getEPS())) {
      pointIndex.push_back(split_var);
      pointVal.push_back(curr_val);
    }
    if (!isZero(curr_val, param.getEPS())) { // Note, one day this may cause trouble if values are really close to zero
      pointIndex.push_back(origVarIndex);
      pointVal.push_back(curr_val);
    }
  }
  CoinPackedVector optPoint((int) pointIndex.size(), pointIndex.data(),
      pointVal.data());
  const double beta =
      param.getParamVal(ParamIndices::FLIP_BETA_PARAM_IND) >= 0 ? 1.0 : -1.0;
  interPtsAndRays.addPointOrRayToIntersectionInfo(optPoint, beta, facet_ind,
      tmpSolver->getObjValue());
  if (tmpSolver->getObjValue() < this->min_nb_obj_val) {
    this->min_nb_obj_val = tmpSolver->getObjValue();
  }

  // We now store the packed vector for the corner polyhedron rays.
  // For each non-basic var in nonBasicVarIndex, we get Binv * A.
  // Since we are looking at x_B = \bar a_0 - \bar A_N x_N,
  // the rays should be negative of Binv * A.
  // Exception is when the variable is non-basic at its upper bound.
  // In that case, ray is negated, which is equivalent to taking Binv * A as-is.
  // For free variables, we need both the ray from Binv * A and its negation.
  // For fixed variables, there is no need to get a ray, since we would not be able
  // to increase along that direction anyway.
  // Finally, considering the slack variables, if the row is
  //  ax <= b
  // then Clp stores a slack of the form
  //  ax + s = b, s >= 0
  // in which case we need to again take -Binv * A.
  // If the row is
  //  ax >= b
  // then Clp stores a slack of the form
  //  ax + s = b, s <= 0
  // in which case we already have the negative by using Binv * A.
  // This is because the Clp version is an upper-bounded variable,
  // so we have to negate.
  //
  // For the rows that are >=, if the variable basic in the row is slack,
  // then we have it be a_i - s = b_i, so we move everything to the other side.
  // I.e., we negate the *row* of Binv * A.
  std::vector<int> structRayIndex;
  std::vector<double> structRayElem;
  for (int j = 0; j < (int) nonBasicVarIndex.size(); j++) {
    const int NBVar = nonBasicVarIndex[j];
    const int origNBVarIndex =
        (!splitVarDeleted || (NBVar < split_var)) ? NBVar : NBVar + 1;
    if (origNBVarIndex == split_var) {
      continue; // Skip the ray corresponding to split_var
    }

    // If NBVar is structural, we need to insert it too
    bool toInsert = NBVar < tmpSolver->getNumCols();
    const double rayMult = (isNonBasicUBVar(tmpSolver, NBVar)) ? -1.0 : 1.0;

    // We now loop through the basic structural variables to get their ray components
    for (int c = 0; c < (int) basicStructVar.size(); c++) {
      const int BVar = basicStructVar[c];
      const int origBVarIndex = (!splitVarDeleted || BVar < split_var) ? BVar : BVar + 1;

      // We should insert (in the appropriate place) the NBVar if it is a structural one
      if (toInsert && NBVar < BVar) {
        structRayIndex.push_back(origNBVarIndex);
        structRayElem.push_back(rayMult);
        toInsert = false;
      }

      const int rowOfBVar = rowOfVar[BVar]; // Should not be -1

      // Default is -Binv * A
      // If NBVar is at ub, we have to negate the ray
      const double rayVal = currRay[j][rowOfBVar]; //rayMult * -1.0 * currRay[j][rowOfBVar];
      if (!isZero(rayVal, param.getRAYEPS())) {
        structRayIndex.push_back(origBVarIndex);
        structRayElem.push_back(rayVal);
      }
    }

    // If we have reached the end of the loop and did not insert the struct NBVar, do so now
    if (toInsert) {
      structRayIndex.push_back(origNBVarIndex);
      structRayElem.push_back(rayMult);
    }

    // Now we add the ray
    // The number of non-basic variables is really the number of cols,
    // since we have m + n variables total, counting slacks, and only m basic
    Ray tmpRay;
    tmpRay.setVector(structRayIndex.size(), structRayIndex.data(),
        structRayElem.data());
    interPtsAndRays.addPointOrRayToIntersectionInfo(tmpRay, 0.0, facet_ind,
        tmpRay.getObjViolation());

//    {
//      if (NBVar < tmpSolver->getNumCols())
//        printf(
//            "Ray %d on nb var (col) %d with name %s was added? %d.\n",
//            j, NBVar, tmpSolver->getColName(NBVar).c_str(), added);
//      else
//        printf(
//            "Ray %d on nb var (row) %d with name %s was added? %d.\n",
//            j, NBVar,
//            tmpSolver->getRowName(NBVar - tmpSolver->getNumCols()).c_str(),
//            added);
//    }
//
//    // If the ray corresponds to a free variable, we also need to add its negation
//    if (isNonBasicFreeVar(tmpSolver, NBVar)) {
//      for (int i = 0; i < (int) structRayElem.size(); i++) {
//        structRayElem[i] *= -1.0;
//      }
//      addPackedRay(ray, tmpSolver->getNumCols(), structRayIndex,
//          structRayElem);
//
//      {
//        if (NBVar < tmpSolver->getNumCols())
//          printf(
//              "Negation of ray %d on nb free var (col) %d with name %s was added? %d.\n",
//              j, NBVar, tmpSolver->getColName(NBVar).c_str(), added);
//        else
//          printf(
//              "Negation of ray %d on nb free var (row) %d with name %s was added? %d.\n",
//              j, NBVar,
//              tmpSolver->getRowName(NBVar - tmpSolver->getNumCols()).c_str(),
//              added);
//      }
//    }

    // Clear the data structures for use for the next ray
    structRayIndex.clear();
    structRayElem.clear();
  }
//  if (solverFactorizationStatus == 0) {
//    tmpSolver->disableFactorization();
//  }
} /* genCorner */

/**********************************************************/
/**
 * IN NON-BASIC SPACE
 * Get Point and Rays from corner polyhedron defined by current optimum at solver
 * Assumed to be optimal already
 */
void CglVPC::genCornerNB(IntersectionInfo& interPtsAndRays, const int split_var,
    const PointCutsSolverInterface* const tmpSolver,
    const SolutionInfo & origProbData, const int term_ind,
    const std::vector<int>& basicStructVar, const std::vector<int>& rowOfVar,
    const std::vector<int>& rowOfOrigNBVar,
    const std::vector<int>& tmpNonBasicVarIndex,
    const std::vector<std::vector<double> >& currRay,
    const bool splitVarDeleted) {
//  const int solverFactorizationStatus = tmpSolver->canDoSimplexInterface();
//  if (solverFactorizationStatus == 0) {
//    tmpSolver->enableFactorization();
//  }
  // Ensure solver is optimal
  if (!tmpSolver->isProvenOptimal()) {
    error_msg(errstr, "Solver is not proven optimal.\n");
    writeErrorToLog(errstr, GlobalVariables::log_file);
    exit(1);
  }

  const int cgs_param = param.getParamVal(
      CglGICParamNamespace::ParamIndices::CGS_PARAM_IND);
  const bool use_cross = (cgs_param == 7);

  /***********************************************************************************
   * The first point is simply the first vertex
   ***********************************************************************************/
  // We need to calculate its coefficients in the NB space given in origProbData
  // That is, the NB space defined by v, the solution to the initial LP relaxation
  // *However* we need to work in the complemented NB space, where v is the origin
  //const double* structSolution = tmpSolver->getColSolution();
  // The point will be stored wrt origProbData cobasis,
  // but the cobasis of the point is given by nonBasicVarIndex
  Point optPoint;
  optPoint.setCompNBCoor(tmpSolver, castsolver, origProbData,
      splitVarDeleted ? split_var : -1);
  if (!isVal(optPoint.getObjViolation(),
      tmpSolver->getObjValue() - origProbData.LPopt, 1e-3)) {
    error_msg(errorstring,
        "NB obj value is somehow incorrect. Calculated: %s, should be: %s.\n",
        stringValue(optPoint.getObjViolation()).c_str(),
        stringValue(tmpSolver->getObjValue() - origProbData.LPopt).c_str());
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }
  const double beta =
      param.getParamVal(ParamIndices::FLIP_BETA_PARAM_IND) >= 0 ? 1.0 : -1.0;
  if (beta > 0 && !isZero(optPoint.getObjViolation())) { // check things are still okay after we scale; only applies when beta > 0, as otherwise the obj cut is not valid
    const double activity = optPoint.getObjViolation()
        / (tmpSolver->getObjValue() - origProbData.LPopt);
    if (lessThanVal(activity, beta, param.getEPS())) {
      GlobalVariables::numCutSolverFails[CutSolverFails::NUMERICAL_ISSUES_WARNING_NO_OBJ_FAIL_IND]++;
      // Is it less by a little or by a lot?
      if (lessThanVal(activity, beta, param.getDIFFEPS())) {
        error_msg(errorstring,
            "Term %d (point) does not satisfy the objective cut. Activity: %.8f. RHS: %.8f\n",
            term_ind, activity, beta);
        writeErrorToLog(errorstring, GlobalVariables::log_file);
        exit(1);
      } else {
        warning_msg(warnstring,
            "Term %d (point) does not satisfy the objective cut.\n\tActivity: %.8f\n\tRHS: %.8f.\n" "\tSmall enough violation that we only send a warning.\n",
            term_ind, activity, beta);
      }
    }
  }
  interPtsAndRays.addPointOrRayToIntersectionInfo(optPoint, beta, term_ind,
      optPoint.getObjViolation());
  if (optPoint.getObjViolation() < this->min_nb_obj_val) {
    this->min_nb_obj_val = optPoint.getObjViolation();
  }

  /***********************************************************************************
   * We get the rays corresponding to non-basic variables
   ***********************************************************************************/
  // We now store the packed vector for the corner polyhedron rays.
  // For each non-basic var in nonBasicVarIndex, we get Binv * A.
  // Since we are looking at x_B = \bar a_0 - \bar A_N x_N,
  // the rays should be negative of Binv * A.
  // Exception is when the variable is non-basic at its upper bound.
  // In that case, ray is negated, which is equivalent to taking Binv * A as-is.
  // For free variables, we need both the ray from Binv * A and its negation.
  // For fixed variables, there is no need to get a ray, since we would not be able
  // to increase along that direction anyway.
  // Finally, considering the slack variables, if the row is
  //  ax <= b
  // then Clp stores a slack of the form
  //  ax + s = b, s >= 0
  // in which case we need to again take -Binv * A.
  // If the row is
  //  ax >= b
  // then Clp stores a slack of the form
  //  ax + s = b, s <= 0
  // in which case we already have the negative by using Binv * A.
  // This is because the Clp version is an upper-bounded variable,
  // so we have to negate.
  //
  // For the rows that are >=, if the variable basic in the row is slack,
  // then we have it be a_i - s = b_i, so we move everything to the other side.
  // I.e., we negate the *row* of Binv * A.
  // We iterate through each of the rays
  for (int j = 0; j < (int) tmpNonBasicVarIndex.size(); j++) {
    const int NBVar = tmpNonBasicVarIndex[j]; // This is the current ray
    if (!splitVarDeleted && !use_cross && (NBVar == split_var)) {
        //&& (NBVar >= tmpSolver->getNumRows() + tmpSolver->getNumCols() - 1)) {
      continue; // Skip the ray corresponding to split_var (or the artificial variable)
    }

    // We want to store this ray in the complemented original NB space
    Ray currRayNB(origProbData.numNB);
    currRayNB.setCompNBCoor(currRay[j].data(), tmpSolver, rowOfOrigNBVar,
        origProbData, NBVar, splitVarDeleted ? split_var : -1, false);
    if (currRayNB.getNumElements() > 0) {
      { // For debugging purposes, check obj dot product is correct
        const int numElem = currRayNB.getNumElements();
        const int* ind = currRayNB.getIndices();
        const double* vals = currRayNB.getElements();
        double calcRedCost = 0.;
        for (int el = 0; el < numElem; el++) {
          calcRedCost += vals[el] * origProbData.nonBasicReducedCost[ind[el]];
        }
        calcRedCost /= currRayNB.getScale();
        if (!isVal(currRayNB.getObjViolation(), calcRedCost, 1e-3)) {
          error_msg(errorstring,
              "Calculated reduced cost is somehow incorrect. Calculated: %s, should be: %s.\n",
              stringValue(calcRedCost).c_str(),
              stringValue(currRayNB.getObjViolation()).c_str());
          writeErrorToLog(errorstring, GlobalVariables::log_file);
          exit(1);
        }
        if (lessThanVal(calcRedCost, 0., param.getEPS())) {
          GlobalVariables::numCutSolverFails[CutSolverFails::NUMERICAL_ISSUES_WARNING_NO_OBJ_FAIL_IND]++;
          // Is it less by a little or by a lot?
          if (lessThanVal(calcRedCost, 0., param.getDIFFEPS())) {
            error_msg(errorstring,
                "Term %d ray %d does not satisfy the objective cut. Activity: %.8f. RHS: %.8f\n",
                term_ind, j, calcRedCost, 0.);
            writeErrorToLog(errorstring, GlobalVariables::log_file);
            exit(1);
          } else {
            warning_msg(warnstring,
                "Term %d ray %d does not satisfy the objective cut.\n\tActivity: %.8f\n\tRHS: %.8f.\n" "\tSmall enough violation that we only send a warning.\n",
                term_ind, j, calcRedCost, 0.);
          }
        }
      }
      interPtsAndRays.addPointOrRayToIntersectionInfo(currRayNB, 0.0,
          term_ind, currRayNB.getObjViolation() / currRayNB.twoNorm());
    }
//    Point newPoint;
//    packedShallowVectorSum(newPoint, 1.0, optPoint, 1.0, currRayNB);
//    interPtsAndRays.addPointOrRayToIntersectionInfo(newPoint, 1.0, facet_ind);

    // We also add this ray to the ray indices for the initial vertex
    //point[0].rayIndices.push_back((int) ray.size() - 1);

//    {
//      if (NBVar < solver->getNumCols())
//        printf(
//            "Ray %d on nb var (col) %d with name %s was added? %d.\n",
//            j, NBVar, solver->getColName(NBVar).c_str(), added);
//      else
//        printf(
//            "Ray %d on nb var (row) %d with name %s was added? %d.\n",
//            j, NBVar,
//            solver->getRowName(NBVar - solver->getNumCols()).c_str(),
//            added);
//    }
//
//    // If the ray corresponds to a free variable, we also need to add its negation
//    if (isNonBasicFreeVar(tmpSolver, NBVar)) {
//      currRayNB *= -1.0;
//      added = addRay(ray, currRayNB);
//      if (added) {
//        currRayFullSpace *= -1.0;
//        addRay(rayFullSpace, currRayFullSpace, false);
//
//        // We also add this ray to the ray indices for the initial vertex
//        point[0].rayIndices.push_back((int) ray.size() - 1);
//        pointFullSpace[0].rayIndices.push_back(
//            (int) rayFullSpace.size() - 1);
//      }
//
//      {
//        if (NBVar < solver->getNumCols())
//          printf(
//              "Negation of ray %d on nb free var (col) %d with name %s was added? %d.\n",
//              j, NBVar, solver->getColName(NBVar).c_str(), added);
//        else
//          printf(
//              "Negation of ray %d on nb free var (row) %d with name %s was added? %d.\n",
//              j, NBVar,
//              solver->getRowName(NBVar - solver->getNumCols()).c_str(),
//              added);
//      }
//    }
  }
//  if (solverFactorizationStatus == 0) {
//    tmpSolver->disableFactorization();
//  }
} /* genCornerNB */

/**********************************************************/
/**
 * Return code
 * -1 unsuccessful
 * 1 successful
 * This is the version being used the type~2 code above
 */
int CglVPC::pivotHelper(IntersectionInfo& currInterPtsAndRays,
    const PointCutsSolverInterface* const & pivotSolver,
    std::vector<int>& currTmpVarBasicInRow,
    std::vector<int>& currTmpBasicStructVar,
    std::vector<int>& currTmpBasicSlackVar, std::vector<int>& currRowOfTmpVar,
    std::vector<int>& currRowOfOrigNBVar,
    std::vector<int>& currTmpNonBasicVarIndex,
    std::vector<std::vector<double> >& currCurrBInvACol,
    std::vector<std::vector<double> >& currCurrRay, const int hplaneTmpVar,
    const int hplaneTmpRow, const IntersectionInfo& interPtsAndRays,
    const PointCutsSolverInterface* const tmpSolver,
    const SolutionInfo& probData, const int facet_ind, const int ray_ind,
    const int currFracVar, const bool splitVarDeleted) {
  // Pivoting has already been done

  if (hplaneTmpRow >= 0) {
    // Re-identify cobasis
    identifyCobasis(currTmpVarBasicInRow, currTmpBasicStructVar,
        currTmpBasicSlackVar, currRowOfTmpVar, currRowOfOrigNBVar,
        currTmpNonBasicVarIndex, currCurrBInvACol, currCurrRay, pivotSolver,
        probData, currFracVar, splitVarDeleted, true);
  } else {
    // Hitting opposite bound of the non-basic variable (obviously structural)
    // Only one ray is affected (the current ray)
    // Suffices to replace that ray with the corresponding intersection point
    currInterPtsAndRays = interPtsAndRays;
    std::vector<int> delIndices(1, 1 + ray_ind);
    const double dist = getVarUB(tmpSolver, hplaneTmpVar)
        - getVarLB(tmpSolver, hplaneTmpVar);
    if (!isZero(dist)) {
      CoinPackedVector newPoint;
      packedSortedVectorSum(newPoint, 1.0, currInterPtsAndRays.getVector(0),
          dist, currInterPtsAndRays.getVector(1 + ray_ind));
      currInterPtsAndRays.addPointOrRayToIntersectionInfo(newPoint, 1.0,
          facet_ind, -1.); // TODO add obj depth
    }
    currInterPtsAndRays.deleteRowInfo(delIndices.size(), delIndices.data(),
        false);
  }
  return 1;
} /* pivotHelper (being used) */

/**********************************************************/
void CglVPC::pivotHelper(IntersectionInfo& currInterPtsAndRays,
    PointCutsSolverInterface*& currTmpSolver,
    std::vector<int>& currTmpVarBasicInRow,
    std::vector<int>& currTmpBasicStructVar,
    std::vector<int>& currTmpBasicSlackVar, std::vector<int>& currRowOfTmpVar,
    std::vector<int>& currRowOfOrigNBVar,
    std::vector<int>& currTmpNonBasicVarIndex,
    std::vector<std::vector<double> >& currCurrBInvACol,
    std::vector<std::vector<double> >& currCurrRay, int& hplaneTmpVar,
    int& hplaneTmpRow, int& currRayTmpVar, int& outStatus,
    const IntersectionInfo& interPtsAndRays,
    const PointCutsSolverInterface* const tmpSolver,
    const SolutionInfo& probData, const int facet_ind, const int ray_ind,
    const int currFirstHplaneIndex, const int currFracVar,
    const std::vector<std::vector<double> >& currRay,
    const std::vector<Hplane>& hplanesToAct,
    const std::vector<int>& tmpNonBasicVarIndex, const bool splitVarDeleted) {
  // Pivot to the nearest neighbor along this ray
  hplaneTmpVar = hplanesToAct[currFirstHplaneIndex].var;
  hplaneTmpRow = hplanesToAct[currFirstHplaneIndex].row;
  currRayTmpVar = tmpNonBasicVarIndex[ray_ind];
  if (hplaneTmpRow >= 0) {
    if (hplaneTmpVar < tmpSolver->getNumCols()) {
      outStatus =
          (greaterThanVal(currRay[ray_ind][hplaneTmpRow], 0.0,
              param.getRAYEPS())) ? 1 : -1;
    } else {
      outStatus = (tmpSolver->getRowSense()[hplaneTmpRow] == 'L') ? 1 : -1;
    }

    //          backStatus[f] =
    //              (currRayTmpVar[f] < tmpSolver[f]->getNumCols())
    //                  && isNonBasicUBCol(tmpSolver[f], currRayTmpVar[f]) ? 1 : -1;

    currTmpSolver = dynamic_cast<PointCutsSolverInterface*>(tmpSolver->clone());
    currTmpSolver->enableSimplexInterface(true);
    currTmpSolver->pivot(currRayTmpVar, hplaneTmpVar, outStatus);
    currTmpSolver->disableSimplexInterface();

    // Re-identify cobasis
    identifyCobasis(currTmpVarBasicInRow, currTmpBasicStructVar,
        currTmpBasicSlackVar, currRowOfTmpVar, currRowOfOrigNBVar,
        currTmpNonBasicVarIndex, currCurrBInvACol, currCurrRay, currTmpSolver,
        probData, currFracVar, splitVarDeleted, true);
  } else {
    // Hitting opposite bound of the non-basic variable (obviously structural)
    // Only one ray is affected (the current ray)
    // Suffices to replace that ray with the corresponding intersection point
    currInterPtsAndRays = interPtsAndRays;
    CoinShallowPackedVector optPoint(currInterPtsAndRays.getVector(0));
    std::vector<int> delIndices(1, 1 + ray_ind);
    currInterPtsAndRays.deleteRowInfo(delIndices.size(), delIndices.data(),
        false);
    const double dist = getVarUB(tmpSolver, hplaneTmpVar)
        - getVarLB(tmpSolver, hplaneTmpVar);
    if (!isZero(dist)) {
      const double rayVal =
          currInterPtsAndRays.getVector(1 + ray_ind).getIndices()[0]; // Should only have one index
      std::vector<int> newPointIndex(optPoint.getIndices(),
          optPoint.getIndices() + optPoint.getNumElements());
      std::vector<double> newPointVal(optPoint.getElements(),
          optPoint.getElements() + optPoint.getNumElements());
      bool toInsert = true;
      for (int el = 0; el < optPoint.getNumElements(); el++) {
        const int curr_ind = newPointIndex[el];
        if (curr_ind == ray_ind) { // TODO should be different in struct space
          newPointVal[el] += dist * rayVal;
          toInsert = false;
          break;
        } else if (curr_ind > ray_ind) {
          newPointIndex.insert(newPointIndex.begin() + el, ray_ind);
          newPointVal.insert(newPointVal.begin() + el, dist);
          toInsert = false;
          break;
        }
      }
      if (toInsert) {
        newPointIndex.push_back(ray_ind);
        newPointVal.push_back(dist);
      }
      CoinShallowPackedVector newPoint;
      newPoint.setVector(newPointIndex.size(), newPointIndex.data(),
          newPointVal.data());
      currInterPtsAndRays.addPointOrRayToIntersectionInfo(newPoint, 1.0,
          facet_ind, -1.); // TODO add obj depth
    }
  }
} /* pivotHelper */

/**********************************************************/
void CglVPC::chooseHplanesDepth2VPCs(std::vector<Hplane>& hplanesToAct,
    std::vector<int>& firstHplane,
    std::vector<std::vector<double> >& distToHplane,
    const PointCutsSolverInterface* const tmpSolver,
    const SolutionInfo& origProbData, const std::vector<int>& varBasicInRow,
    const std::vector<int>& basicStructVar,
    const std::vector<int>& basicSlackVar, const std::vector<int>& rowOfVar,
    const std::vector<int>& rowOfOrigNBVar,
    const std::vector<int>& nonBasicVarIndex,
    const std::vector<std::vector<double> >& currRay, const int facet_ind,
    const int split_ind, const int split_var, const bool splitVarDeleted) {
  const int numRays = (int) nonBasicVarIndex.size();
  const std::vector<double> inftyDist(numRays, -1);
  firstHplane.resize(numRays, -1);

  const int cgs_param = param.getParamVal(
      CglGICParamNamespace::ParamIndices::CGS_PARAM_IND);
  const bool use_cross = (cgs_param == 7);

  for (int ray_ind = 0; ray_ind < numRays; ray_ind++) {
    const int rayVarStructIndex = nonBasicVarIndex[ray_ind];
    if (!splitVarDeleted && !use_cross && rayVarStructIndex == split_var) {
        //&& (rayVarStructIndex >= tmpSolver->getNumRows() + tmpSolver->getNumCols() - 1)) {
      continue; // Skip ray corresponding to split_var
    }
#ifdef TRACE
    const std::string nb_var_name =
    (rayVarStructIndex < tmpSolver->getNumCols()) ?
    tmpSolver->getColName(rayVarStructIndex) :
    tmpSolver->getRowName(rayVarStructIndex - tmpSolver->getNumCols());
    printf(
        "CglVPC::chooseHplanesDepth2VPCs: Facet %d of split %d on variable %d, choosing hplane for ray (index %d, variable %d, and name %s).\n",
        facet_ind, split_ind, split_var, ray_ind, rayVarStructIndex,
        nb_var_name.c_str());
#endif
    // Find hyperplane that intersects ray first
    // It returns false if no hyperplane is intersected by the ray
    findHplaneToIntersectRayFirst(firstHplane[ray_ind], rayVarStructIndex,
        tmpSolver, origProbData, currRay[ray_ind], varBasicInRow, hplanesToAct,
        split_var, splitVarDeleted);
  }

  // Find distance to each hyperplane
  for (int hplane_ind = 0; hplane_ind < (int) hplanesToAct.size();
      hplane_ind++) {
    distToHplane.push_back(inftyDist);
  }
  for (int ray_ind = 0; ray_ind < numRays; ray_ind++) {
    // Get the ray
    const int rayVarTmpStructIndex = nonBasicVarIndex[ray_ind];
//    const int tmpRayMult =
//        isNonBasicUBVar(tmpSolver, rayVarTmpStructIndex) ? -1 : 1;
    for (int hplane_ind = 0; hplane_ind < (int) hplanesToAct.size();
        hplane_ind++) {
      const double var_val = getVarVal(tmpSolver,
          hplanesToAct[hplane_ind].var);
      const int hplane_row = hplanesToAct[hplane_ind].row;
      if (hplane_row >= 0) {
//        const int rowMult =
//            (hplanesToAct[hplane_ind].structvar >= tmpSolver->getNumCols())
//                && (tmpSolver->getRowSense()[hplane_row] == 'G') ? -1 : 1; // Slacks complemented
//        const double rayVal = tmpRayMult * -1
//            * currRay[ray_ind][hplane_row];
        const double rayVal = currRay[ray_ind][hplane_row];

        // Check that the ray value is non-zero
        if (isZero(rayVal, param.getRAYEPS())) {
          continue;
        } else if (lessThanVal(rayVal, 0.0, param.getRAYEPS())
            && !isVal(hplanesToAct[hplane_ind].bound,
                getVarLB(tmpSolver, hplanesToAct[hplane_ind].var))) {
          continue;
        } else if (greaterThanVal(rayVal, 0.0, param.getRAYEPS())
            && !isVal(hplanesToAct[hplane_ind].bound,
                getVarUB(tmpSolver, hplanesToAct[hplane_ind].var))) {
          continue;
        }

        // Otherwise, find the distance to the hyperplane, and ensure it is actually intersected
        const double tmpDistToHplane =
            (hplanesToAct[hplane_ind].bound - var_val) / rayVal;
        if (!isInfinity(tmpDistToHplane, tmpSolver->getInfinity())
            && !lessThanVal(tmpDistToHplane, 0.0)) {
          // Ray intersects this hyperplane
          hplanesToAct[hplane_ind].allRaysIntByHplane[0].push_back(ray_ind);
          hplanesToAct[hplane_ind].rayToBeCutByHplane[0].push_back(ray_ind);
          distToHplane[hplane_ind][ray_ind] = tmpDistToHplane;
        }
      } else if (hplanesToAct[hplane_ind].var == rayVarTmpStructIndex) {
        hplanesToAct[hplane_ind].allRaysIntByHplane[0].push_back(ray_ind);
        hplanesToAct[hplane_ind].rayToBeCutByHplane[0].push_back(ray_ind);
        distToHplane[hplane_ind][ray_ind] = getVarUB(tmpSolver,
            rayVarTmpStructIndex) - getVarLB(tmpSolver, rayVarTmpStructIndex);
      }
    }
  }
} /* chooseHplanesDepth2VPCs */

/**********************************************************/
/**
 * Should we instead be using Rays obtained from IntersectionInfo
 * (i.e., dot products of CoinPackedVectors)
 * Is that more efficient?
 */
bool CglVPC::findHplaneToIntersectRayFirst(int& firstHplane,
    const int rayVarStructIndex, const PointCutsSolverInterface* const tmpSolver,
    const SolutionInfo& origProbData, const std::vector<double>& currRay,
    const std::vector<int>& varBasicInRow, std::vector<Hplane>& hplanesToAct,
    const int splitVar, const bool splitVarDeleted) {
  // Initialize values, in case no hplane is found at all
  Hplane tmpHplane(HplaneVarFlag::none, HplaneRowFlag::none,
      HplaneBoundFlag::none, 0); // var, row, ubflag, numsplits
  // Initialize values, in case no hplane is found at all
  Hplane hplane(HplaneVarFlag::none, HplaneRowFlag::none, HplaneBoundFlag::none,
      1); // var, row, ubflag, numsplits

  double minDist = tmpSolver->getInfinity();

  // Get the ray
  const int tmpRayMult = isNonBasicUBVar(tmpSolver, rayVarStructIndex) ? -1 : 1;

  // First, check all the previously activated hyperplanes
  for (int h = 0; h < (int) hplanesToAct.size(); h++) {
    const int r = hplanesToAct[h].row;
    // If it is a basic variable, we check the distance along this ray to the hplane
    // If the hplane corresponds to a previous non-basic variable, then there is no point to check
    // A non-basic variable does not intersect the bounds on any other non=basic variable
    // Nothing needs to be changed in distToHplane since it was initialized to infinity
    if (r >= 0) {
      const double var_val = getVarVal(tmpSolver, hplanesToAct[h].var);
//      const int rowMult = (tmpSolver->getRowSense()[r] == 'G') ? -1 : 1; // Slacks complemented
      const double rayVal = currRay[r]; // tmpRayMult * -1 * currRay[r];
      const bool toLB = (lessThanVal(rayVal, 0.0, param.getRAYEPS())
          && isVal(hplanesToAct[h].bound,
              getVarLB(tmpSolver, hplanesToAct[h].var)));
      const bool toUB = (greaterThanVal(rayVal, 0.0, param.getRAYEPS())
          && isVal(hplanesToAct[h].bound,
              getVarUB(tmpSolver, hplanesToAct[h].var)));
      if (toLB || toUB) {
        const double tmpDistToHplane = (hplanesToAct[h].bound - var_val)
            / rayVal;
        if (!isInfinity(tmpDistToHplane, tmpSolver->getInfinity())
            && !lessThanVal(tmpDistToHplane, 0.0)) {
          // If the distance is closer than what we had before, use this hplane instead
          if (lessThanVal(tmpDistToHplane, minDist)) {
            firstHplane = h;
            minDist = tmpDistToHplane;
          }
        }
      }
    }
  }

  // Check the basic variable in each row to see if it can be activated
  if (ACTIVATE_BASIC_BOUNDS) {
    for (int r = 0; r < tmpSolver->getNumRows(); r++) {
      tmpHplane.var = varBasicInRow[r];
      const bool isStructVar = (tmpHplane.var < tmpSolver->getNumCols());
      tmpHplane.row = r;
      if (tmpSolver->getRowSense()[r] == 'E') {
        continue; // Do not activate equality rows
      }
      const double var_val = getVarVal(tmpSolver, tmpHplane.var);
//    const int rowMult =
//        ((!isStructVar) && (tmpSolver->getRowSense()[r] == 'G')) ? -1 : 1; // Slacks complemented
      const double rayVal = currRay[r]; // tmpRayMult * -1 * currRay[r];

      tmpHplane.bound = 0.0;
      if (lessThanVal(rayVal, 0.0, param.getRAYEPS())) {
        tmpHplane.bound = getVarLB(tmpSolver, tmpHplane.var);
        tmpHplane.ubflag = static_cast<int>(HplaneBoundFlag::toLB);
      } else if (greaterThanVal(rayVal, 0.0, param.getRAYEPS())) {
        tmpHplane.bound = getVarUB(tmpSolver, tmpHplane.var);
        tmpHplane.ubflag = static_cast<int>(HplaneBoundFlag::toUB);
      } else {
        continue; // ray does not intersect this hyperplane
      }

      // If this bound is actually at infinity; we will never intersect it
      if (isInfinity(std::abs(tmpHplane.bound), tmpSolver->getInfinity())) {
        continue;
      }

      // Otherwise, find the distance to the hyperplane, and ensure it is actually intersected
      const double tmpDistToHplane = (tmpHplane.bound - var_val) / rayVal;
      if (isInfinity(tmpDistToHplane, tmpSolver->getInfinity())) {
        continue; // ray does not intersect this hyperplane
      }

      // If the distance is closer than what we had before, use this hplane instead
      if (lessThanVal(tmpDistToHplane, minDist)) {
        hplane.var = tmpHplane.var;
        hplane.row = tmpHplane.row;
        hplane.ubflag = tmpHplane.ubflag;
        hplane.bound = tmpHplane.bound;
        if (!isStructVar) {
          hplane.ubflag = static_cast<int>(HplaneBoundFlag::toLBSlack);
        } else {
          // If this variable was originally NB and at its upper-bound,
          // then we had complemented it to be at its lower-bound
          // Hence, if it is at its upper-bound now, this is the same as its lower-bound
          // in the complemented non-basic space
          const int origVarIndex =
              (!splitVarDeleted || (tmpHplane.var < splitVar)) ?
                  tmpHplane.var : tmpHplane.var + 1;
//        const int NBRowMult = (param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND)
//            && origProbData.isNBUBVar(origVarIndex)
//           ) ? -1 : 1;
          if (param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND)
              && origProbData.isNBUBVar(origVarIndex)) {
            hplane.ubflag *= -1 * tmpHplane.ubflag + 1; // Switch between 0 and 1
          }
        }
        minDist = tmpDistToHplane;
      }
    }
  }

  // Check non-basic bound
  if (ACTIVATE_NB_BOUNDS && rayVarStructIndex < tmpSolver->getNumCols()) {
    // distToHplane is (UB - LB) / 1.0
    const double LB = getVarLB(tmpSolver, rayVarStructIndex);
    const double UB = getVarUB(tmpSolver, rayVarStructIndex);
    if (!isNegInfinity(LB, tmpSolver->getInfinity())
        && !isInfinity(UB, tmpSolver->getInfinity())) {
      if (lessThanVal(UB - LB, minDist)) {
        hplane.var = rayVarStructIndex;
        hplane.row = static_cast<int>(HplaneRowFlag::NB);
        const int origNBVarStructIndex =
            (!splitVarDeleted || (rayVarStructIndex < splitVar)) ?
                rayVarStructIndex : rayVarStructIndex + 1;
        const int origRayMult =
            (param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND)
                && origProbData.isNBUBVar(origNBVarStructIndex)) ? -1 : 1;
        hplane.ubflag = static_cast<int>(HplaneBoundFlag::toUBNB) * tmpRayMult
            * origRayMult;
        if (hplane.ubflag == static_cast<int>(HplaneBoundFlag::toUBNB)) {
          hplane.bound =
              param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND) ?
                  UB - LB : UB;
        } else {
          hplane.bound =
              param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND) ? 0 : LB;
        }
        minDist = UB - LB;
      }
    }
  }

  if (isInfinity(minDist, tmpSolver->getInfinity())) {
    return false;
  } else {
    // Check that this hyperplane has not already been activated for this ray
    if ((hplane.var >= 0)
        && hasHplaneBeenActivated(hplanesToAct, hplane) == -1) {
      firstHplane = hplanesToAct.size();
      hplanesToAct.push_back(hplane);
    }
    return true;
  }
} /* findHplaneToIntersectRayFirst */

/**
 *
 */
void CglVPC::activateHplane(IntersectionInfo& interPtsAndRays,
    const std::vector<double>& distToHplane,
    const PointCutsSolverInterface* const tmpSolver,
    const SolutionInfo& origProbData, const Hplane& hplane,
    const int hplane_ind, const std::vector<int>& rowOfTmpVar,
    const std::vector<int>& rowOfOrigNBVar,
    const std::vector<int>& tmpNonBasicVarIndex,
    const std::vector<std::vector<double> >& currBInvACol,
    const std::vector<std::vector<double> >& currRay, const int splitVar,
    const double splitVal, const int facet_ind, const bool splitVarDeleted) {
  const int numRays = tmpNonBasicVarIndex.size();
  const int numRaysToCut = hplane.rayToBeCutByHplane[0].size();

  // Assumed to be in the same order as they were put into interPtsAndRays
  // ray with index ray_ind is in position ray_ind+1 in interPtsAndRays (the first index is the optimal point)
  std::vector<int> delIndices; // For deleting rows

  // If it is a non-basic variable bound activated, it only cuts one ray, and we specialize the code
  if (hplane.row < 0) {
    if (numRaysToCut > 1) {
      error_msg(errorstring,
          "Number of rays cut by non-basic hyperplane (variable %d) should be 1; instead it is %d.\n",
          hplane.var, numRaysToCut);
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }
    const int ray_ind = hplane.rayToBeCutByHplane[0][0];
    const int rayVarStructIndex = tmpNonBasicVarIndex[ray_ind];
    if (greaterThanVal(distToHplane[ray_ind], 0.0)) {
      Point point;
      calcDepth2Point(point, tmpSolver, origProbData, rayVarStructIndex,
          currBInvACol[ray_ind], distToHplane[ray_ind], rowOfTmpVar,
          rowOfOrigNBVar, splitVar, splitVal, splitVarDeleted);
      if (!duplicatePoint(interPtsAndRays, interPtsAndRays.RHS, point)) {
        interPtsAndRays.addPointOrRayToIntersectionInfo(point, 1.0, ray_ind, -1,
            hplane_ind, -2, 0, facet_ind, 0.0, 0.0, false);
      }

      // Increase multiplicity on all the other rays
      for (int j = 0; j < numRays; j++) {
        if (j != ray_ind) {
          interPtsAndRays.increaseMultiplicity(j + 1);
        }
      }
      delIndices.push_back(ray_ind + 1);
    }
    interPtsAndRays.deleteRowInfo(delIndices.size(), delIndices.data(), true);
    return;
  }

  // Mark rays that will be cut by a hyperplane
  std::vector<bool> raysCutFlag(numRays, false);

  for (int j = 0; j < numRaysToCut; j++) {
    const int ray_ind = hplane.rayToBeCutByHplane[0][j];
    raysCutFlag[ray_ind] = true;
    delIndices.push_back(ray_ind + 1);
  }

  // Deleted indices need to be sorted
//  sort(delIndices.begin(), delIndices.end());
  interPtsAndRays.deleteRowInfo(delIndices.size(), &delIndices[0], true);

  // For each hyperplane, create the set of new rays
  std::vector<int> newRayNBIndices;
  storeNewRayNBIndices(numRays, newRayNBIndices, hplane.allRaysIntByHplane[0],
      raysCutFlag);
  const int numNewRays = newRayNBIndices.size();

  for (int j = 0; j < numRaysToCut; j++) {
    const int ray_ind = hplane.rayToBeCutByHplane[0][j];
    const int rayVarStructIndex = tmpNonBasicVarIndex[ray_ind];
    if (!lessThanVal(distToHplane[ray_ind], 0.0)
        && !isInfinity(distToHplane[ray_ind])) {
//      delIndices.push_back(ray_ind + 1);
      // If it actually goes anywhere along this ray, add a new point
      if (greaterThanVal(distToHplane[ray_ind], 0.0)) {
        Point point;
        calcDepth2Point(point, tmpSolver, origProbData, rayVarStructIndex,
            currBInvACol[ray_ind], distToHplane[ray_ind], rowOfTmpVar,
            rowOfOrigNBVar, splitVar, splitVal, splitVarDeleted);
        if (!duplicatePoint(interPtsAndRays, interPtsAndRays.RHS, point)) {
          interPtsAndRays.addPointOrRayToIntersectionInfo(point, 1.0, ray_ind,
              -2, hplane_ind, -2, -2, facet_ind, 0.0, 0.0, false);
        }
      }

      // We also need to add the new rays
      // These are added regardless of whether the point is added or not
      for (int k = 0; k < numNewRays; k++) {
        const int new_ray_ind = newRayNBIndices[k];
        Ray ray;
        calcDepth2Ray(ray, tmpSolver, origProbData, hplane.row,
            rayVarStructIndex, ray_ind, tmpNonBasicVarIndex[new_ray_ind],
            new_ray_ind, currBInvACol, rowOfTmpVar, rowOfOrigNBVar, splitVar,
            splitVarDeleted);
        const int prev_row = findDuplicateRay(interPtsAndRays,
            interPtsAndRays.RHS, ray);
        if (prev_row < 0) {
          interPtsAndRays.addPointOrRayToIntersectionInfo(ray, 0.0, ray_ind,
              new_ray_ind, hplane_ind, -2, -2, facet_ind, 0.0, 0.0, false);
        } else {
          interPtsAndRays.increaseMultiplicity(prev_row);
        }
      }
    }
  }
} /* activateHplane */

/**********************************************************/
/**
 * The type-2 point is obtained by starting at the optimal point p^f on the facet
 * and proceeding distAlongRay along one of the rays r^k emanating from p^f
 *
 * Note:
 *   rayTmpStructIndex = k
 *   rayMult = M_{N^f}(k)
 *   currBInvACol = the ray (but we need to do the calculation to negate, etc.)
 *     TODO We should probably set it up so the rays are calculated earlier
 *
 * To get the new point's coordinate for x_i, we calculate
 *   p^{fk}_i = p^f_i + distAlongRay * r^k_i
 *
 * 1. In the structural space, with complemented slacks
 *   a. x_i structural + basic at p^f
 *     p^f_i = tmpSolver->getColSolution()[i]
 *     r^k_i = rayMult * (-\bar a_{i,k}) = rayMult * (-currBInvACol[row(i)])
 *   b. x_i structural + nb at p^f
 *     p^f_i = tmpSolver->getColSolution()[i]
 *     r^k_i = rayMult if i = k, 0 otherwise
 *   c. x_i slack + basic at p^f; rowMult = 1 if <= row, -1 if >= row
 *     p^f_i = abs( tmpSolver->getRowActivity()[i] - tmpSolver->getRightHandSide()[i] )
 *     r^k_i = rowMult * rayMult * (-currBInvACol[i])
 *   d. x_i slack + nb at p^f; rowMult = 1 if <= row, -1 if >= row
 *     p^f_i = 0
 *     r^k_i = rowMult * rayMult if i = k, 0 otherwise
 *
 * 2. In the non-basic space (the ORIGINAL non-basic space, defined by Nbar)
 *     \tilde p^{fk}_i = abs( p^{fk}_i - origVal )
 *   where origVal = solver->getColSolution()[i] if i is structural, 0 otherwise
 */
void CglVPC::calcDepth2Point(Point& newPoint,
    const PointCutsSolverInterface* const tmpSolver,
    const SolutionInfo& origProbData, const int rayTmpStructIndex,
    const std::vector<double>& currRay, const double distAlongRay,
    const std::vector<int>& rowOfTmpVar, const std::vector<int>& rowOfOrigNBVar,
    const int splitVar, const double splitVal, const bool splitVarDeleted) {
  const int rayMult = (isNonBasicUBVar(tmpSolver, rayTmpStructIndex)) ? -1 : 1;

  std::vector<int> pointIndex;
  std::vector<double> pointVal;

  if (param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND)) {
    for (int j = 0; j < origProbData.numNB; j++) {
      const int col = origProbData.nonBasicVarIndex[j];
//      const int rowMult =
//          ((col < origProbData.numCols)
//              || (solver->getRowSense()[col - origProbData.numCols] == 'L')) ?
//              1 : -1;
      const int tmp_col =
          (!splitVarDeleted || (col < splitVar)) ? col : col - 1;
      const double tmpVal = getVarVal(tmpSolver, tmp_col);
      const double origVal =
          (col < origProbData.numCols) ? castsolver->getColSolution()[col] : 0.;
      const int row = rowOfOrigNBVar[j];
//      const double rayVal =
//          (row >= 0) ?
//              rayMult * -1 * currBInvACol[row] : rayMult;
      const double rayVal = (row >= 0) ? currRay[row] : rayMult;
      if ((row >= 0) || (tmp_col == rayTmpStructIndex)) {
        const double structNewVal = tmpVal + distAlongRay * rayVal;
        const double NBNewVal = std::abs(structNewVal - origVal);
        if (!isZero(NBNewVal, param.getEPS())) {
          pointIndex.push_back(j);
          pointVal.push_back(NBNewVal);
        }
      }
    }
  } else {
    for (int col = 0; col < castsolver->getNumCols(); col++) {
      if (col == splitVar) {
        if (!isZero(splitVal)) {
          pointIndex.push_back(splitVar);
          pointVal.push_back(splitVal);
        }
      } else {
        const int tmp_col =
            (!splitVarDeleted || (col < splitVar)) ? col : col - 1;
        const int tmpVal = tmpSolver->getColSolution()[tmp_col];
        const int row = rowOfTmpVar[tmp_col];
//        const double rayVal =
//            (row >= 0) ? rayMult * -1 * currBInvACol[row] : rayMult;
        const double rayVal = (row >= 0) ? currRay[row] : rayMult;
        if ((row >= 0) || (tmp_col == rayTmpStructIndex)) {
          const double structNewVal = tmpVal + distAlongRay * rayVal;
          if (!isZero(structNewVal, param.getEPS())) {
            pointIndex.push_back(col);
            pointVal.push_back(structNewVal);
          }
        }
      }
    }
  }

  newPoint.setVector(pointIndex.size(), pointIndex.data(), pointVal.data(),
      false);
} /* calcDepth2Point */

/**********************************************************/
/**
 * Calculate the new ray components starting at a type-2 point,
 * proceeding along ray r^k emanating from p^f,
 * and then finding the component for ray r^q
 *
 * Note:
 *   rayTmpStructIndex = k
 *   rayTmpNBIndex = k_nb
 *   newRayTmpStructIndex = q
 *   newRayTmpNBIndex = q_nb
 *   hplane_row = h
 *   rayMult = M_{N^f}(q)
 *   origRayMult = M_{Nbar}(i)
 *   currBInvACol = tilda = all the rays (but we need to do the calculation to negate, etc.)
 *
 * To get the components of r^{khq}_i
 * (the ray along r^k, hitting hyperplane H_h, in direction r^q), we calculate
 *
 * 1. In the structural space, with complemented slacks
 *   a. h >= 0 (meaning we activated a bound on a variable basic at p^f)
 *     i. x_i basic at p^f, i.e., row(i) >= 0
 *       Let rowMult = -1 if x_i slack and >= row, 1 otherwise
 *
 *       r^{khq}_i = rowMult * rayMult *
 *                   ( tilda[k_nb][row(i)] * tilda[q_nb][h] / tilda[k_nb][h]
 *                     - tilda[q_nb][row(i)] )
 *       Note that if tilda[k_nb][row(i)] - 0, then nothing changes
 *     ii. x_i nb at p^f
 *       Let rowMult = -1 if x_i slack and >= row, 1 otherwise
 *       - i = k
 *         r^{khq}_i = rowMult * rayMult * ( - tilda[q_nb][h] / tilda[k_nb][h] )
 *       - i = q
 *         r^{khq}_i = rowMult * rayMult if i = q
 *       - otherwise, equal to zero
 *   b. h < 0 (activating a bound on a non-basic variable) *** WILL NOT HAPPEN IN THIS FUNCTION
 *     The only ray that changes is increasing the multiplicity on all the "other" rays (other than k)
 *
 * 2. In the non-basic space, we simply multiply r^{khq}_i by origRayMult
 */
void CglVPC::calcDepth2Ray(Ray& newRay,
    const PointCutsSolverInterface* const tmpSolver,
    const SolutionInfo& origProbData, const int hplane_row,
    const int rayTmpStructIndex, const int rayTmpNBIndex,
    const int newRayTmpStructIndex, const int newRayTmpNBIndex,
    const std::vector<std::vector<double> >& currBInvACol,
    const std::vector<int>& rowOfTmpVar, const std::vector<int>& rowOfOrigNBVar,
    const int splitVar, const bool splitVarDeleted) {
  const int rayMult =
      (isNonBasicUBVar(tmpSolver, newRayTmpStructIndex)) ? -1 : 1;

  std::vector<int> packedRayIndex;
  std::vector<double> packedPointVal;
  if (param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND)) {
    for (int j = 0; j < origProbData.numNB; j++) {
      const int col = origProbData.nonBasicVarIndex[j];
      const int origRayMult = (origProbData.isNBUBVar(col)) ? -1 : 1;

      const int tmp_col =
          (!splitVarDeleted || (col < splitVar)) ? col : col - 1;
      const int row = rowOfOrigNBVar[j];

      double newVal = 0.0;
      if (row >= 0) {
        if (!isZero(currBInvACol[rayTmpNBIndex][row], param.getRAYEPS())) {
          newVal = rayMult
              * (currBInvACol[rayTmpNBIndex][row]
                  * currBInvACol[newRayTmpNBIndex][hplane_row]
                  / currBInvACol[rayTmpNBIndex][hplane_row]
                  - currBInvACol[newRayTmpNBIndex][row]);
        }
      } else {
        if (tmp_col == rayTmpStructIndex) {
          newVal = rayMult
              * (-1 * currBInvACol[newRayTmpNBIndex][hplane_row]
                  / currBInvACol[rayTmpNBIndex][hplane_row]);
        } else if (tmp_col == newRayTmpStructIndex) {
          newVal = origRayMult * rayMult;
        }
      }
      if (!isZero(newVal, param.getRAYEPS())) {
        packedRayIndex.push_back(j);
        packedPointVal.push_back(newVal);
      }
    }
  } else {
    for (int col = 0; col < castsolver->getNumCols(); col++) {
      const int tmp_col =
          (!splitVarDeleted || (col < splitVar)) ? col : col - 1;
      const int row = rowOfTmpVar[tmp_col];
      double newVal = 0.0;
      if (row >= 0) {
        if (!isZero(currBInvACol[rayTmpNBIndex][row], param.getRAYEPS())) {
          newVal = rayMult
              * (currBInvACol[rayTmpNBIndex][row]
                  * currBInvACol[newRayTmpNBIndex][hplane_row]
                  / currBInvACol[rayTmpNBIndex][hplane_row]
                  - currBInvACol[newRayTmpNBIndex][row]);
        }
      } else {
        if (tmp_col == rayTmpStructIndex) {
          newVal = rayMult
              * (-currBInvACol[newRayTmpNBIndex][hplane_row]
                  / currBInvACol[rayTmpNBIndex][hplane_row]);
        } else if (tmp_col == newRayTmpStructIndex) {
          newVal = rayMult;
        }
      }
      if (!isZero(newVal, param.getRAYEPS())) {
        packedRayIndex.push_back(col);
        packedPointVal.push_back(newVal);
      }
    }
  }

  newRay.setVector(packedRayIndex.size(), packedRayIndex.data(),
      packedPointVal.data(), false);
} /* calcDepth2Ray */

/********************/
/** CUT GENERATION **/
/********************/

/**********************************************************/
/** 
 * Generate cuts from points and rays for this cgs
 */
void CglVPC::tryObjectivesForCgs(AdvCuts& structVPCs, int& num_cuts_per_cgs,
    int& num_cuts_generated, int& num_obj_tried, const SolutionInfo& probData,
    const int dim, const bool inNBSpace, const int cgs_ind,
    const std::string& cgsName,
    const std::vector<IntersectionInfo>& interPtsAndRays,
    const SolutionInfo& origProbData, const AdvCuts& structSICs,
    Stats& timeStats, const std::string timeName,
    const bool fixFirstPoint, const AdvCut* objCut) {
  if (reachedCutLimit(num_cuts_per_cgs, num_cuts_generated)) {
    return;
  }

#ifdef TRACE
  printf("\n## CglVPC: Trying objectives for cgs %d/%d with name %s. ##\n",
      cgs_ind + 1, param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND),
      cgsName.c_str());
#endif

  // Set up solver with points and rays found
  PointCutsSolverInterface* cutSolver;
  cutSolver = new PointCutsSolverInterface;

  // We can scale the rhs for points by min_nb_obj_val
  const bool useScale = true && !isInfinity(std::abs(this->min_nb_obj_val));
  const double scale = (!useScale || lessThanVal(this->min_nb_obj_val, 1.)) ? 1. : this->min_nb_obj_val;
  double beta = param.getParamVal(ParamIndices::FLIP_BETA_PARAM_IND) >= 0 ? scale : -1. * scale;

  std::vector<double> ortho;
  std::vector<int> nonZeroColIndex;
  const bool isCutSolverPrimalFeas = setupCutSolver(cutSolver, nonZeroColIndex,
      interPtsAndRays, ortho, scale, cgs_ind, fixFirstPoint);

  if (isCutSolverPrimalFeas) {
    if (param.getParamVal(ParamIndices::PRLP_STRATEGY_PARAM_IND) == 0) {
      tightOnPointsRaysCutGeneration(cutSolver, beta, nonZeroColIndex,
          structVPCs, num_cuts_per_cgs, num_cuts_generated, num_obj_tried,
          castsolver, probData, cgs_ind, cgsName, structSICs, timeStats,
          timeName, inNBSpace);

      if (!inNBSpace) {
        //genCutsFromPointsRays(cutSolver, beta, structVPCs,
        //    num_vpcs_per_split[split_ind], num_cuts_generated, num_obj_tried,
        //    dynamic_cast<LiftGICsSolverInterface*>(solver), probData, interPtsAndRays[split_ind],
        //    probData.feasSplitVar[split_ind], structSICs);
      } else {
        // This is in the * complemented * non-basic space,
        // so the origin is the solution to the LP relaxation
        // Moreover, every cut will have right-hand side 1,
        // since we want to separate the origin
        genCutsFromPointsRaysNB(cutSolver, beta, nonZeroColIndex, structVPCs,
            num_cuts_per_cgs, num_cuts_generated, num_obj_tried, castsolver,
            probData, cgs_ind, cgsName, structSICs);
      }
    } else {
      targetStrongAndDifferentCuts(cutSolver, beta, nonZeroColIndex, structVPCs,
          num_cuts_per_cgs, num_cuts_generated, num_obj_tried, castsolver,
          probData, ortho, cgs_ind, cgsName, structSICs, timeStats, timeName,
          inNBSpace);
    }
  }

  // May be worth switching to -1 rhs
  // Even if these are not _cuts_, they may still help in branch-and-bound
  // Though the objectives we use are not attuned to this idea
  const int flipBeta = std::abs(param.getParamVal(ParamIndices::FLIP_BETA_PARAM_IND));
  if (flipBeta == 2 || (!isCutSolverPrimalFeas && flipBeta > 0)) {
#ifdef TRACE
  printf(
      "\n## Flip beta and again check feasibility using all zeroes objective (tentative cgs %d/%d). ##\n",
      cgs_ind + 1, param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND));
#endif
    beta *= -1;
    for (int row_ind = 0; row_ind < cutSolver->getNumRows(); row_ind++) {
      const double rhs = cutSolver->getRightHandSide()[row_ind];
      if (!isZero(rhs)) {
        cutSolver->setRowLower(row_ind, -1. * rhs);
      }
    }

    const bool isNewCutSolverPrimalFeas = setupCutSolverHelper(cutSolver,
        cgs_ind, -1, -1);

    if (isNewCutSolverPrimalFeas) {
      if (param.getParamVal(ParamIndices::PRLP_STRATEGY_PARAM_IND) == 0) {
        tightOnPointsRaysCutGeneration(cutSolver, beta, nonZeroColIndex,
            structVPCs, num_cuts_per_cgs, num_cuts_generated, num_obj_tried,
            castsolver, probData, cgs_ind, cgsName, structSICs, timeStats,
            timeName, inNBSpace);

        if (!inNBSpace) {
          //genCutsFromPointsRays(cutSolver, beta, structVPCs,
          //    num_vpcs_per_split[split_ind], num_cuts_generated, num_obj_tried,
          //    dynamic_cast<LiftGICsSolverInterface*>(solver), probData, interPtsAndRays[split_ind],
          //    probData.feasSplitVar[split_ind], structSICs);
        } else {
          // This is in the * complemented * non-basic space,
          // so the origin is the solution to the LP relaxation
          // Moreover, every cut will have right-hand side 1,
          // since we want to separate the origin
          genCutsFromPointsRaysNB(cutSolver, beta, nonZeroColIndex, structVPCs,
              num_cuts_per_cgs, num_cuts_generated, num_obj_tried, castsolver,
              probData, cgs_ind, cgsName, structSICs);
        }
      } else {
        targetStrongAndDifferentCuts(cutSolver, beta, nonZeroColIndex,
            structVPCs, num_cuts_per_cgs, num_cuts_generated, num_obj_tried,
            castsolver, probData, ortho, cgs_ind, cgsName, structSICs,
            timeStats, timeName, inNBSpace);
      }
    }
  }

  // Free cutSolver
  if (cutSolver) {
    delete cutSolver;
  }
} /* tryObjectivesForCgs */

/********************/
/** USEFUL METHODS **/
/********************/

/**
 * Count the number of points and rays for a specific cgs
 */
void CglVPC::analyzePRCollection(std::vector<IntersectionInfo>& interPtsAndRays,
    const AdvCut* const objCut, const std::vector<bool>& calcAndFeasFacet) {
//  for (int f = 0; f < (int) interPtsAndRays.size(); f++) {
//    if (!calcAndFeasFacet[f]) {
//      continue;
//    }
//    const int numIntPointsOrRaysOnFacet = interPtsAndRays[f].getNumRows();
////    interPtsAndRays[f].pointRowIndex.reserve(numIntPointsOrRaysOnFacet / 2);
////    interPtsAndRays[f].objDepth.reserve(numIntPointsOrRaysOnFacet / 2);
////    interPtsAndRays[f].SICDepth.reserve(numIntPointsOrRaysOnFacet / 2);
////    interPtsAndRays[f].finalFlag.resize(numIntPointsOrRaysOnFacet, true);
////    for (int row_ind = 0; row_ind < numIntPointsOrRaysOnFacet; row_ind++) {
//      // If it's a point get it, o/w count the ray
////      if (isZero(interPtsAndRays[f].RHS[row_ind])) {
////        interPtsAndRays[f].numRays++;
////      } else {
////        interPtsAndRays[f].numPoints++;
////        interPtsAndRays[f].pointRowIndex.push_back(row_ind);
////      }
////    }
//
////#ifdef SHOULD_CALC_POINT_OBJ_DEPTH
////    // We will calculate the violation from the objective
////    for (int ind = 0; ind < interPtsAndRays[f].numPoints; ind++) {
//////      const int point_ind = interPtsAndRays[f].pointRowIndex[ind];
////      const double objActivity = objCut->getActivity(
////          interPtsAndRays[f][point_ind]);
////      double curr_obj_depth = (objActivity - objCut->rhs())
////      / objCut->row().twoNorm();
////      if (isZero(curr_obj_depth)) {
////        curr_obj_depth = +0.0;
////      }
////      interPtsAndRays[f].objDepth.push_back(curr_obj_depth);
////    }
////#endif
//  }
} /* analyzePRCollection */

/**
 * Check that the points and rays we have generated satisfy the objective cut
 * There may be some slight violations (similar to when the non-basic reduced cost is slightly negative)
 * This in general will _probably_ not cause invalid cuts,
 * but the cutSolver might end up being infeasible (which otherwise should not happen).
 */
void CglVPC::checkPRCollectionForFeasibility(const int term_ind,
    const IntersectionInfo& interPtsAndRays, const double nb_obj_val,
    const AdvCut* const objCut) {
  if (isZero(nb_obj_val)) {
    return;
  }
  // Check that the points and rays we have generated satisfy the objective cut
  // There may be some slight violations, such as when the non-basic reduced cost is slightly negative
  // This in general will _probably_ not cause invalid cuts,
  // but the cutSolver might end up being infeasible (which otherwise should not happen).
  const CoinPackedVector objVec = objCut->row();
  for (int r = 0; r < (int) interPtsAndRays.getNumRows(); r++) {
    const CoinShallowPackedVector vec = interPtsAndRays.getVector(r);
    const double rhs = interPtsAndRays.RHS[r];
    const double scale = (isZero(rhs)) ? 1. : nb_obj_val;
   const double activity = dotProduct(vec, objVec) / scale;
    //objCut->getActivity(interPtsAndRays[t][r])
    if (lessThanVal(activity, rhs, param.getEPS())) {
      // Is it less by a little or by a lot?
      if (lessThanVal(activity, rhs, param.getDIFFEPS())) {
        error_msg(errorstring,
            "Term %d row %d does not satisfy the objective cut. Activity: %.8f. RHS: %.8f\n",
            term_ind, r, activity, rhs);
        writeErrorToLog(errorstring, GlobalVariables::log_file);
        exit(1);
      } else {
        warning_msg(warnstring,
            "Term %d row %d does not satisfy the objective cut.\n\tActivity: %.8f\n\tRHS: %.8f.\n" "\tSmall enough violation that we only send a warning.\n",
            term_ind, r, activity, rhs);
        GlobalVariables::numCutSolverFails[CutSolverFails::NUMERICAL_ISSUES_WARNING_NO_OBJ_FAIL_IND]++;
      }
    }
  }
} /* checkPRCollectionForFeasibility */

/**********************************************************/
/**
 * We get the rays corresponding to non-basic variables
 */
void CglVPC::identifyCobasis(std::vector<int>& varBasicInRow,
    std::vector<int>& basicStructVar, std::vector<int>& basicSlackVar,
    std::vector<int>& rowOfVar, std::vector<int>& rowOfOrigNBVar,
    std::vector<int>& nonBasicVarIndex,
    std::vector<std::vector<double> >& currBInvACol,
    std::vector<std::vector<double> >& currRay,
    const PointCutsSolverInterface* const tmpSolver,
    const SolutionInfo& origProbData, const int splitVar,
    const bool splitVarDeleted, const bool calcRays) {
//  const int solverFactorizationStatus = tmpSolver->canDoSimplexInterface();
//  if (solverFactorizationStatus == 0) {
//    tmpSolver->enableFactorization();
//  }
  // Ensure solver is optimal
//  if (!tmpSolver->isProvenOptimal()) {
//    error_msg(errstr, "Solver is not proven optimal.\n");
//    writeErrorToII(errstr, GlobalVariables::inst_info_out);
//    exit(1);
//  }

  // Clear things
  varBasicInRow.clear();
  basicStructVar.clear();
  basicSlackVar.clear();
  rowOfVar.clear();
  rowOfOrigNBVar.clear();
  nonBasicVarIndex.clear();

  // We could get this information by creating a SolutionInfo for this solver, but that seems too expensive
  // All we need is the number of rays we will have
  // This is the number of non-basic variables at their upper or lower bounds, including slack variables
  // Also free variables are not a problem because we have preprocessed
  varBasicInRow.resize(tmpSolver->getNumRows());
  tmpSolver->getBasics(&varBasicInRow[0]);

  // We also will want to know what row each *orig* non-basic var is basic in, *if any*
  // This will be useful to extract the coefficients of the rays quickly
  rowOfVar.resize(tmpSolver->getNumCols() + tmpSolver->getNumRows(), -1);
  // The vector will hold the row that nbvar i is basic in, if any.
  // If none, it will hold -1.
  rowOfOrigNBVar.resize(origProbData.numNB, -1);

  // Gather which are the non-basic variables for this solver
  //  int numRays = 0, numNBFree;
  nonBasicVarIndex.reserve(tmpSolver->getNumCols()); // There may be fewer if we have equality constraints
  for (int var = 0; var < tmpSolver->getNumCols() + tmpSolver->getNumRows();
      var++) {
    // If this is a basic structural variable, store its index
    if (var < tmpSolver->getNumCols()) {
      if (isBasicCol(tmpSolver, var)) {
        basicStructVar.push_back(var);
      } else {
        rowOfVar[var] -= nonBasicVarIndex.size();
        nonBasicVarIndex.push_back(var);
      }
    } else { // If this is a row and the basic var is structural, store its row
      const int row = var - tmpSolver->getNumCols();
      const int basicVar = varBasicInRow[row];

      rowOfVar[basicVar] = row;
      if (basicVar >= tmpSolver->getNumCols()) { // Should be equal to var
        basicSlackVar.push_back(var);
      } else {
        rowOfVar[var] -= nonBasicVarIndex.size();
        nonBasicVarIndex.push_back(var);
      }

      // If this basicVar was non-basic in the basis at v,
      // we need to add this row in the right spot in rowOfOrigNBVar
      // Account for the deleted variable corresponding to split
      const int origBasicVarIndex =
          (!splitVarDeleted || (basicVar < splitVar)) ? basicVar : basicVar + 1;
      const int NBIndexOfBasicVar = origProbData.getVarNBIndex(
          origBasicVarIndex);
      if (NBIndexOfBasicVar >= 0) {
        rowOfOrigNBVar[NBIndexOfBasicVar] = row;
      }
    }
  }

  if (calcRays) {
    currBInvACol.resize(nonBasicVarIndex.size());
    currRay.resize(nonBasicVarIndex.size());
    for (int ray_ind = 0; ray_ind < (int) nonBasicVarIndex.size(); ray_ind++) {
      currBInvACol[ray_ind].resize(tmpSolver->getNumRows());
      tmpSolver->getBInvACol(nonBasicVarIndex[ray_ind],
          &(currBInvACol[ray_ind][0]));
      currRay[ray_ind] = currBInvACol[ray_ind];
      if (isNonBasicLBVar(tmpSolver, nonBasicVarIndex[ray_ind])) {
        for (int row = 0; row < tmpSolver->getNumRows(); row++) {
//      const int rowMult =
//          ((varBasicInRow[row] >= tmpSolver->getNumCols())
//              && (tmpSolver->getRowSense()[row] == 'G')) ? -1 : 1;
          currRay[ray_ind][row] *= -1.;
        }
      }
    }
  }
//  if (solverFactorizationStatus == 0) {
//    tmpSolver->disableFactorization();
//  }
} /* identifyCobasis */

/**********************************************************/
/**
 * Version of finding pivots without non-degen information or saving solvers
 */
void CglVPC::findPivots(std::vector<int>& varOut, std::vector<int>& dirnOut,
    std::vector<int>& pivotRow, std::vector<double>& dist,
    std::vector<double>& obj, const std::vector<int>& tmpNonBasicVarIndex,
    const OsiClpSolverInterface* const tmpSolver,
    const std::vector<int>& varBasicInRow, const int split_var,
    const bool splitVarDeleted) {
  const int numNB = tmpNonBasicVarIndex.size();

  // Set up vectors
  varOut.clear();
  varOut.resize(numNB, -1);
  dirnOut.clear();
  dirnOut.resize(numNB, 0);
  pivotRow.clear();
  pivotRow.resize(numNB, -1);
  dist.clear();
  dist.resize(numNB, -1);
  obj.clear();
  obj.resize(numNB, -1 * tmpSolver->getInfinity());

  // For each non-basic variable, try to pivot along the ray
  OsiClpSolverInterface* copySolver;
  for (int ray_ind = 0; ray_ind < numNB; ray_ind++) {
    const int varIn = tmpNonBasicVarIndex[ray_ind];
    if (!splitVarDeleted && (varIn == split_var)) {
      continue; // skip the ray corresponding to the split variable
    }

    copySolver = dynamic_cast<PointCutsSolverInterface*>(tmpSolver->clone());
    copySolver->enableSimplexInterface(true);
    ClpSimplex* model = copySolver->getModelPtr();

    // Check to make sure this variable is still non-basic
    if (isBasicVar(copySolver, varIn)) {
      error_msg(errorstring,
          "Previously non-basic variable %d (nb index %d) is now basic.\n",
          varIn, ray_ind);
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }
//    const int varIn_row =
//        (varIn < copySolver->getNumCols()) ?
//            -1 : varIn - copySolver->getNumCols();
    const int in_stat_old = model->getStatus(varIn);
    const int dirnIn =
        (in_stat_old == ClpSimplex::Status::atUpperBound) ? -1 : 1;
//    const int return_code = copySolver->primalPivotResult(varIn, dirnIn,
//        varOut[ray_ind], dirnOut[ray_ind], dist[ray_ind],
//        NULL);
    const int return_code = pivot(copySolver, varIn, dirnIn, varOut[ray_ind],
        dirnOut[ray_ind], pivotRow[ray_ind], dist[ray_ind]);

    if (return_code >= 0) {
      if (pivotRow[ray_ind] >= 0) {
        if (varBasicInRow[pivotRow[ray_ind]] != varOut[ray_ind]) {
          error_msg(errorstring,
              "While processing ray %d (var %d), variable basic in row %d is supposed to be %d but copySolver says it is %d.\n",
              ray_ind, varIn, pivotRow[ray_ind], varBasicInRow[ray_ind],
              varOut[ray_ind]);
          writeErrorToLog(errorstring, GlobalVariables::log_file);
          exit(1);
        }
      } else {
        if (pivotRow[ray_ind] != -2 && varIn != varOut[ray_ind]) {
          error_msg(errorstring,
              "While processing ray %d (var %d), pivot says nb variable is closest, so pivotRow should be -2 (is %d) and varIn (%d) == varIn (%d).\n",
              ray_ind, varIn, pivotRow[ray_ind], varIn, varOut[ray_ind]);
          writeErrorToLog(errorstring, GlobalVariables::log_file);
          exit(1);
        }
      }
      obj[ray_ind] = copySolver->getObjValue();
    }
    if (copySolver) {
      delete copySolver;
    }
  }
}

/**********************************************************/
/**
 * Version of finding pivots without non-degen information
 */
void CglVPC::findPivots(std::vector<OsiClpSolverInterface*>& copySolver,
    std::vector<int>& varOut, std::vector<int>& dirnOut,
    std::vector<int>& pivotRow, std::vector<double>& dist,
    const std::vector<int>& tmpNonBasicVarIndex,
    const OsiClpSolverInterface* const tmpSolver,
    const std::vector<int>& varBasicInRow, const int split_var,
    const bool splitVarDeleted) {
  const int numNB = tmpNonBasicVarIndex.size();

  // Set up vectors
  copySolver.clear();
  copySolver.resize(numNB);
  varOut.clear();
  varOut.resize(numNB, -1);
  dirnOut.clear();
  dirnOut.resize(numNB, 0);
  pivotRow.clear();
  pivotRow.resize(numNB, -1);
  dist.clear();
  dist.resize(numNB, -1);

  // For each non-basic variable, try to pivot along the ray
  for (int ray_ind = 0; ray_ind < numNB; ray_ind++) {
    const int varIn = tmpNonBasicVarIndex[ray_ind];
    if (!splitVarDeleted && (varIn == split_var)) {
      continue; // skip if non-basic variable is split_var
    }

    copySolver[ray_ind] =
        dynamic_cast<OsiClpSolverInterface*>(tmpSolver->clone());
    copySolver[ray_ind]->enableSimplexInterface(true);
    ClpSimplex* model = copySolver[ray_ind]->getModelPtr();

    // Check to make sure this variable is still non-basic
    if (isBasicVar(copySolver[ray_ind], varIn)) {
      error_msg(errorstring,
          "Previously non-basic variable %d (nb index %d) is now basic.\n",
          varIn, ray_ind);
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }
//    const int varIn_row =
//        (varIn < copySolver[ray_ind]->getNumCols()) ?
//            -1 : varIn - copySolver[ray_ind]->getNumCols();
    const int in_stat_old = model->getStatus(varIn);
    const int dirnIn =
        (in_stat_old == ClpSimplex::Status::atUpperBound) ? -1 : 1;
//    const int return_code = copySolver[ray_ind]->primalPivotResult(varIn,
//        dirnIn, varOut[ray_ind], dirnOut[ray_ind], dist[ray_ind],
//        NULL);
    const int return_code = pivot(copySolver[ray_ind], varIn, dirnIn,
        varOut[ray_ind], dirnOut[ray_ind], pivotRow[ray_ind], dist[ray_ind]);

    if (return_code >= 0) {
      if (pivotRow[ray_ind] >= 0) {
        if (varBasicInRow[pivotRow[ray_ind]] != varOut[ray_ind]) {
          error_msg(errorstring,
              "While processing ray %d (var %d), variable basic in row %d is supposed to be %d but copySolver says it is %d.\n",
              ray_ind, varIn, pivotRow[ray_ind], varBasicInRow[ray_ind],
              varOut[ray_ind]);
          writeErrorToLog(errorstring, GlobalVariables::log_file);
          exit(1);
        }
      } else {
        if (pivotRow[ray_ind] != -2 && varIn != varOut[ray_ind]) {
          error_msg(errorstring,
              "While processing ray %d (var %d), pivot says nb variable is closest, so pivotRow should be -2 (is %d) and varIn (%d) == varIn (%d).\n",
              ray_ind, varIn, pivotRow[ray_ind], varIn, varOut[ray_ind]);
          writeErrorToLog(errorstring, GlobalVariables::log_file);
          exit(1);
        }
      }
    }
  }
}

/**********************************************************/
/**
 * Version of finding pivots that saves the solvers
 */
void CglVPC::findPivots(std::vector<OsiClpSolverInterface*>& copySolver,
    std::vector<int>& varOut, std::vector<int>& dirnOut,
    std::vector<int>& pivotRow, std::vector<double>& dist,
    std::vector<int>& varOutNonDegen, std::vector<int>& dirnOutNonDegen,
    std::vector<int>& pivotRowNonDegen, std::vector<double>& distNonDegen,
    const std::vector<int>& tmpNonBasicVarIndex,
    const OsiClpSolverInterface* const tmpSolver,
    const std::vector<std::vector<double> >& rayVal,
    const std::vector<int>& varBasicInRow, const int split_var,
    const bool splitVarDeleted) {
  const int numNB = tmpNonBasicVarIndex.size();

  // Set up vectors
  copySolver.clear();
  copySolver.resize(numNB);
  varOut.clear();
  varOut.resize(numNB, -1);
  dirnOut.clear();
  dirnOut.resize(numNB, 0);
  pivotRow.clear();
  pivotRow.resize(numNB, -1);
  dist.clear();
  dist.resize(numNB, -1);
  varOutNonDegen.clear();
  varOutNonDegen.resize(numNB, -1);
  dirnOutNonDegen.clear();
  dirnOutNonDegen.resize(numNB, 0);
  pivotRowNonDegen.clear();
  pivotRowNonDegen.resize(numNB, -1);
  distNonDegen.clear();
  distNonDegen.resize(numNB, -1);

  // For each non-basic variable, try to pivot along the ray
  for (int ray_ind = 0; ray_ind < numNB; ray_ind++) {
    const int varIn = tmpNonBasicVarIndex[ray_ind];
    if (!splitVarDeleted && (varIn == split_var)) {
      continue; // skip if non-basic variable is split_var
    }

    copySolver[ray_ind] =
        dynamic_cast<PointCutsSolverInterface*>(tmpSolver->clone());
    copySolver[ray_ind]->enableSimplexInterface(true);
    ClpSimplex* model = copySolver[ray_ind]->getModelPtr();

    // Check to make sure this variable is still non-basic
    if (isBasicVar(copySolver[ray_ind], varIn)) {
      error_msg(errorstring,
          "Previously non-basic variable %d (nb index %d) is now basic.\n",
          varIn, ray_ind);
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }
//    const int varIn_row =
//        (varIn < copySolver[ray_ind]->getNumCols()) ?
//            -1 : varIn - copySolver[ray_ind]->getNumCols();
    const int in_stat_old = model->getStatus(varIn);
    const int dirnIn =
        (in_stat_old == ClpSimplex::Status::atUpperBound) ? -1 : 1;
//    const int return_code = copySolver[ray_ind]->primalPivotResult(varIn,
//        dirnIn, varOut[ray_ind], dirnOut[ray_ind], dist[ray_ind],
//        NULL);
    const int return_code = pivot(copySolver[ray_ind], varIn, dirnIn,
        varOut[ray_ind], dirnOut[ray_ind], pivotRow[ray_ind], dist[ray_ind]);

    if (return_code >= 0) {
      if (pivotRow[ray_ind] >= 0) {
        if (varBasicInRow[pivotRow[ray_ind]] != varOut[ray_ind]) {
          error_msg(errorstring,
              "While processing ray %d (var %d), variable basic in row %d is supposed to be %d but copySolver says it is %d.\n",
              ray_ind, varIn, pivotRow[ray_ind], varBasicInRow[ray_ind],
              varOut[ray_ind]);
          writeErrorToLog(errorstring, GlobalVariables::log_file);
          exit(1);
        }
      } else {
        if (pivotRow[ray_ind] != -2 && varIn != varOut[ray_ind]) {
          error_msg(errorstring,
              "While processing ray %d (var %d), pivot says nb variable is closest, so pivotRow should be -2 (is %d) and varIn (%d) == varIn (%d).\n",
              ray_ind, varIn, pivotRow[ray_ind], varIn, varOut[ray_ind]);
          writeErrorToLog(errorstring, GlobalVariables::log_file);
          exit(1);
        }
      }

      if (isZero(dist[ray_ind])) {
        double valueOut;
        distNonDegen[ray_ind] = manualPivot(varOutNonDegen[ray_ind],
            dirnOutNonDegen[ray_ind], pivotRowNonDegen[ray_ind], valueOut,
            varIn, tmpSolver, rayVal[ray_ind], varBasicInRow, true);
      } else {
        varOutNonDegen[ray_ind] = varOut[ray_ind];
        dirnOutNonDegen[ray_ind] = dirnOut[ray_ind];
        pivotRowNonDegen[ray_ind] = pivotRow[ray_ind];
        distNonDegen[ray_ind] = dist[ray_ind];
      }
    } else {
      // This could be a bunch of things, but one option is that it is an extreme ray; we simply skip it
      varOutNonDegen[ray_ind] = -1;
      dirnOutNonDegen[ray_ind] = 0;
      pivotRowNonDegen[ray_ind] = -1;
      distNonDegen[ray_ind] = -1;
    }
  }
} /* findPivots */

/**********************************************************/
/**
 * This is useful in case we want to find a non-zero pivot,
 * or to check the internal pivoting mechanism
 */
double CglVPC::manualPivot(const int varIn,
    const PointCutsSolverInterface* const tmpSolver,
    const std::vector<double>& currRay, const std::vector<int>& varBasicInRow,
    const bool onlyNonDegenPivot) {
  int varOut_manual = -1, pivotRow_manual = -1, dirnOut_manual = -1;
  double value_new_manual;
  return manualPivot(varOut_manual, dirnOut_manual, pivotRow_manual,
      value_new_manual, varIn, tmpSolver, currRay, varBasicInRow,
      onlyNonDegenPivot);
} /* manualPivot */

/**********************************************************/
/**
 * This is useful in case we want to find a non-zero pivot,
 * or to check the internal pivoting mechanism
 */
double CglVPC::manualPivot(int& varOut_manual, int& dirnOut_manual,
    int& pivotRow_manual, double& valueOut_manual, const int varIn,
    const PointCutsSolverInterface* const tmpSolver,
    const std::vector<double>& currRay, const std::vector<int>& varBasicInRow,
    const bool onlyNonDegenPivot) {
//  const int solverFactorizationStatus = tmpSolver->canDoSimplexInterface();
//  if (solverFactorizationStatus == 0) {
//    tmpSolver->enableFactorization();
//  }
  const int in_stat_old = tmpSolver->getModelPtr()->getStatus(varIn);
  if (in_stat_old == ClpSimplex::Status::isFixed) {
    varOut_manual = varIn;
    dirnOut_manual = 0;
    pivotRow_manual = -2;
    valueOut_manual = getVarVal(tmpSolver, varIn);
    return 0;
  }

  double theta_manual = tmpSolver->getInfinity();
  for (int row = 0; row < tmpSolver->getNumRows(); row++) {
    const int candidate = varBasicInRow[row];
    if (candidate != tmpSolver->getModelPtr()->pivotVariable()[row]) {
      error_msg(errorstring,
          "Var basic in row %d is supposed to be %d but is actually %d.\n", row,
          candidate, tmpSolver->getModelPtr()->pivotVariable()[row]);
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }
//    const int candidate_row = (candidate < tmpSolver->getNumCols()) ? -1 : candidate - tmpSolver->getNumCols();
    const double LB = getVarLB(tmpSolver, candidate);
    const double UB = getVarUB(tmpSolver, candidate);

    // Get the ray value
    // Usually it is -1 * abar_{ij}, but we negate if it is an ub var
    const double rayVal = currRay[row];

    if (isZero(rayVal, param.getRAYEPS())) {
      continue;
    }

    const double bound =
        (lessThanVal(rayVal, 0.0, param.getRAYEPS())) ? LB : UB;
    const int tmp_stat =
        (lessThanVal(rayVal, 0.0, param.getRAYEPS())) ?
            ClpSimplex::Status::atLowerBound : ClpSimplex::Status::atUpperBound;

    // Check that it exists
    if (isInfinity(std::abs(bound), tmpSolver->getInfinity())) {
      continue;
    }

    const double dist = (bound - getVarVal(tmpSolver, candidate)) / rayVal;

    if (dist < theta_manual) {
      if (onlyNonDegenPivot && isZero(dist, param.getEPS())) {
        continue;
      }
      theta_manual = dist;
      varOut_manual = candidate;
      pivotRow_manual = row;
      dirnOut_manual = (tmp_stat == ClpSimplex::Status::atLowerBound) ? -1 : 1;
      valueOut_manual = bound;
    }
  }

  // Could also intersect the bound on the ray first
  if (varIn < tmpSolver->getNumCols()) {
    double bound;
    if (isNonBasicUBVar(tmpSolver, varIn) > 0) {
      bound = tmpSolver->getColUpper()[varIn];
    } else {
      bound = tmpSolver->getColLower()[varIn];
    }

    if (!isInfinity(std::abs(bound), tmpSolver->getInfinity())) {
      double dist = std::abs(bound - getVarVal(tmpSolver, varIn));
      if (dist < theta_manual) {
        if (!onlyNonDegenPivot || !isZero(dist)) {
          theta_manual = dist;
          varOut_manual = varIn;
          pivotRow_manual = -2;
          // Moves to other bound
          const int tmp_stat = (-1 * (in_stat_old - 2)) + 1;
          dirnOut_manual =
              (tmp_stat == ClpSimplex::Status::atLowerBound) ? -1 : 1;
          valueOut_manual = bound;
        }
      }
    }
  }

//  if (solverFactorizationStatus == 0) {
//    tmpSolver->disableFactorization();
//  }
  return theta_manual;
} /* manualPivot */

/**********************************************************/
void CglVPC::generateSICs(const OsiSolverInterface* si, AdvCuts& structSICs, SolutionInfo& solnInfo) {
  // Only feasible ones are returned
  CglSIC SICGen(param);
  AdvCuts NBSICs(true);
  SICGen.generateCuts(*si, NBSICs, structSICs, solnInfo);
#ifdef TRACE
  printf("***** SICs generated: %d.\n", structSICs.sizeCuts());
#endif
} /* generateSICs */

/*********************************************************************/
// Create C++ lines to get to current state
std::string CglVPC::generateCpp(FILE * fp) {
  return "CglVPC";
} /* generateCpp */

/***********************************************************************/
void CglVPC::setParam(const CglGICParam &source) {
  param = source;
} /* setParam */

/*********************************************************************/
// Returns true if needs optimal basis to do cuts
/**********************************************************/
bool CglVPC::needsOptimalBasis() const {
  return true;
} /* neesdOptimalBasis */

/**********************************************************/
void CglVPC::printvecINT(const char *vecstr, const int *x, int n) const {
  int num, fromto, upto;

  num = (n / 10) + 1;
  printf("%s :\n", vecstr);
  for (int j = 0; j < num; ++j) {
    fromto = 10 * j;
    upto = 10 * (j + 1);
    if (n <= upto)
      upto = n;
    for (int i = fromto; i < upto; ++i)
      printf(" %4d", x[i]);
    printf("\n");
  }
  printf("\n");
} /* printvecINT */

/**********************************************************/
void CglVPC::printvecDBL(const char *vecstr, const double *x, int n) const {
  int num, fromto, upto;

  num = (n / 10) + 1;
  printf("%s :\n", vecstr);
  for (int j = 0; j < num; ++j) {
    fromto = 10 * j;
    upto = 10 * (j + 1);
    if (n <= upto)
      upto = n;
    for (int i = fromto; i < upto; ++i)
      printf(" %7.3f", x[i]);
    printf("\n");
  }
  printf("\n");
} /* printvecDBL */

/**********************************************************/
void CglVPC::printvecDBL(const char *vecstr, const double *elem,
    const int * index, int nz) const {
  printf("%s\n", vecstr);
  int written = 0;
  for (int j = 0; j < nz; ++j) {
    written += printf("%d:%.3f ", index[j], elem[j]);
    if (written > 70) {
      printf("\n");
      written = 0;
    }
  }
  if (written > 0) {
    printf("\n");
  }

} /* printvecDBL */
