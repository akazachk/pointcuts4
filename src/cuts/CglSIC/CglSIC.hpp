// Last edit: 01/10/2015
//
// Name:     CglSIC.hpp
// Author:   F. Margot (edited by A. M. Kazachkov)
//           Tepper School of Business
//           email: fmargot@andrew.cmu.edu
// Date:     01/10/2015
//-----------------------------------------------------------------------------

#pragma once

#include "CglGICParam.hpp"
#include "CglCutGenerator.hpp"
#include "CoinWarmStartBasis.hpp"
#include "CoinFactorization.hpp"

#include "SolutionInfo.hpp"
// (the AdvCut and AdvCuts classes could probably be removed eventually,
// but they have some useful features wrt OsiCuts)
#include "AdvCut.hpp"

#include "OsiRowCut.hpp"
#include "OsiCuts.hpp"

/* Debug output */
//#define NEWGEN_TRACE

/* Debug output: print optimal tableau */
//#define NEWGEN_TRACETAB

class CglSIC : public CglCutGenerator {

  friend void CglSICUnitTest(const OsiSolverInterface * siP,
				const std::string mpdDir);
public:
  /**@name generateCuts */
  //@{
  /** Generate cuts for the model of the solver
      interface si.

      Insert the generated cuts into OsiCuts cs.
  */
  virtual void generateCuts(const OsiSolverInterface & si, OsiCuts & cs,
			    const CglTreeInfo info = CglTreeInfo());
  virtual void generateCuts(const OsiSolverInterface & si, 
      OsiCuts& nb_cs, OsiCuts& struct_cs,
      SolutionInfo& probData,
      const CglTreeInfo info = CglTreeInfo());

  /**
   * Add each SIC and see if the problem is feasible after adding it.
   * In the subspace, infeasibility may sometimes happen
   * (in the full space, it would prove infeasibility of the problem)
   */
  void checkSICFeasibility(AdvCuts& structSIC,
      SolutionInfo& probData, const bool in_subspace); 

  /// For compatibility with CglCutGenerator (const method)
  virtual void generateCuts(const OsiSolverInterface & si, OsiCuts & cs,
			    const CglTreeInfo info = CglTreeInfo()) const;

  /// Return true if needs optimal basis to do cuts (will return true)
  virtual bool needsOptimalBasis() const;
  //@}
  
  /**@name Public Methods */
  //@{

  // Set the parameters to the values of the given CglGMIParam object.
  void setParam(const CglGICParam &source); 
  // Return the CglGICParam object of the generator. 
  inline CglGICParam getParam() const {return param;}

  //@}

  /**@name Constructors and destructors */
  //@{
  /// Default constructor 
  CglSIC();

  // Constructor with specified parameters
  CglSIC(const CglGICParam&);

  /// Copy constructor 
  CglSIC(const CglSIC &);

  /// Clone
  virtual CglCutGenerator * clone() const;

  /// Assignment operator 
  CglSIC & operator=(const CglSIC& rhs);
  
  /// Destructor 
  virtual ~CglSIC();
  /// Create C++ lines to get to current state
  virtual std::string generateCpp( FILE * fp);

  inline void setSolution(std::vector<double>* solution) {
    this->solution = solution;
  }

//  inline void clearCuts() {
//    for (int i = nb_advcs.sizeCuts() - 1; i >= 0; i--) {
//      nb_advcs.eraseRowCut(i);
//    }
//    nb_advcs.cuts.resize(0);
//
//    for (int i = struct_advcs.sizeCuts() - 1; i >= 0; i--) {
//      struct_advcs.eraseRowCut(i);
//    }
//    struct_advcs.cuts.resize(0);
//  }

  //@}
    
private:
  
  // Private member methods

/**@name Private member methods */

  //@{
  
  // Method generating the cuts after all CglSIC members are properly set.
  void genStructSICs(AdvCuts& structSICs, AdvCuts& NBSICs, 
      SolutionInfo& probData);
  void genNBSICs(AdvCuts& NBSIC, const SolutionInfo& probData);
  void genNBSIC(AdvCut& NBSIC, double& absSumCoeffs,
      const std::vector<int>& nonBasicVarIndex,
          const int numNBOrig, const int splitVarIndex,
      const int splitVarRowIndex, const double EPS);
  void createMIGs(AdvCuts& structGMICs, const SolutionInfo& probData);
  void createMIG(AdvCut &cut, const std::vector<int>& nonBasicVarIndex,
      const int numNBOrig, const int splitVarIndex,
      const int splitVarRowIndex) const;
  void eliminate_slacks(std::vector<double>& vec) const;

  /// print a vector of integers
  void printvecINT(const char *vecstr, const int *x, int n) const;
  /// print a vector of doubles: dense form
  void printvecDBL(const char *vecstr, const double *x, int n) const;
  /// print a vector of doubles: sparse form
  void printvecDBL(const char *vecstr, const double *elem, const int * index, 
		   int nz) const;

  //@}
  
  // Private member data

/**@name Private member data */

  //@{

  /// Object with CglGICParam members. 
  CglGICParam param;
  
  /// Pointer on solver. Reset by each call to generateCuts().
  OsiSolverInterface *solver;

  /// Pointer on point to separate. Reset by each call to generateCuts().
  const double *xlp;

  /// Pointer on matrix of coefficient ordered by rows. 
  /// Reset by each call to generateCuts().
  const CoinPackedMatrix *byRow;

  /// Pointer on matrix of coefficient ordered by columns. 
  /// Reset by each call to generateCuts().
  const CoinPackedMatrix *byCol;

  // AdvCuts class
//  AdvCuts nb_advcs, struct_advcs;
  
  /// IP feasible solution that we can check against
  std::vector<double>* solution;

  //@}
};

//#############################################################################
/** A function that tests the methods in the CglSIC class. The
    only reason for it not to be a member method is that this way it doesn't
    have to be compiled into the library. And that's a gain, because the
    library should be compiled with optimization on, but this method should be
    compiled with debugging. */
void CglSICUnitTest(const OsiSolverInterface * siP,
			 const std::string mpdDir );
