// Last edit: 07/14/2017
//
// Name:     CglSIC.hpp
// Author:   F. Margot and A. M. Kazachkov
//           Tepper School of Business
// Date:     07/10/2015 (latest update in git commits)
//-----------------------------------------------------------------------------

#if defined(_MSC_VER)
// Turn off compiler warning about long names
#  pragma warning(disable:4786)
#endif
#include <cstdlib>
#include <cstdio>
#include <cmath>  // Remember to put std::abs
#include <cfloat>
#include <cassert>
#include <iostream>
#include <fenv.h>
#include <climits>

#include "CglSIC.hpp"
#include "OsiSolverInterface.hpp"

#include "CoinHelperFunctions.hpp"
#include "CoinPackedVector.hpp"
#include "CoinPackedMatrix.hpp"
#include "CoinIndexedVector.hpp"
#include "OsiRowCutDebugger.hpp"
#include "CoinFactorization.hpp"
#include "CoinFinite.hpp"

#include "GlobalConstants.hpp"
#include "Utility.hpp"
#include "CutHelper.hpp"

/***************************************************************************/
CglSIC::CglSIC() :
  CglCutGenerator(), solver(NULL), xlp(NULL), byRow(NULL), byCol(NULL), solution(NULL)
{
  this->param.setParams();
}

/***************************************************************************/
CglSIC::CglSIC(const CglGICParam& parameters) : 
  CglCutGenerator(), param(parameters), solver(NULL), xlp(NULL), byRow(NULL), byCol(NULL), solution(NULL)
{
  // Nothing to do here
}

/***************************************************************************/
CglSIC::CglSIC(const CglSIC& rhs) :
  CglCutGenerator(rhs), param(rhs.param), solver(rhs.solver), xlp(rhs.xlp), byRow(
      rhs.byRow), byCol(rhs.byCol), solution(rhs.solution)
{
  // Nothing to do here
}

/***************************************************************************/
CglSIC & CglSIC::operator=(const CglSIC& rhs) {
  if (this != &rhs) {
    CglCutGenerator::operator=(rhs);
    this->param = rhs.param;
    solver = rhs.solver;
    xlp = rhs.xlp;
    byRow = rhs.byRow;
    byCol = rhs.byCol;
    solution = rhs.solution;
  }
  return *this;
}

/***************************************************************************/
CglSIC::~CglSIC() {

}

/*********************************************************************/
CglCutGenerator *
CglSIC::clone() const {
  return new CglSIC(*this);
}

/************************************************************************/
/**
 * Kludge to be able to modify the CglSIC object if it is const
 */
void CglSIC::generateCuts(const OsiSolverInterface & si, OsiCuts & cs,
    const CglTreeInfo info) const {
  CglSIC temp(*this);
  temp.generateCuts(si, cs, info);
} /* generateCuts */

/************************************************************************/
/**
 * When SolutionInfo not provided, need to make it ourselves
 */
void CglSIC::generateCuts(const OsiSolverInterface &si, OsiCuts & cs,
    const CglTreeInfo info) {
  // Assuming we are in the structural space
  solver = const_cast<OsiSolverInterface *>(&si); // Removes const-ness
  SolutionInfo tmpSolnInfo(solver,
      this->param.getParamVal(ParamIndices::MAX_FRAC_VAR_PARAM_IND),
      this->param.getParamVal(ParamIndices::SUBSPACE_PARAM_IND), true); // we can be brief
  if (tmpSolnInfo.numSplits == 0) {
    return;
  }
  if (!this->param.finalizeParams(tmpSolnInfo.numNB, tmpSolnInfo.numSplits)) {
    error_msg(errstr,
        "Issue finalizing parameters (no rays to cut).\n");
    writeErrorToLog(errstr, GlobalVariables::log_file);
    exit(1);
  }
  if (tmpSolnInfo.numFeasSplits == 0) {
    return;
  }
  if (this->param.getParamVal(ParamIndices::MAX_FRAC_VAR_PARAM_IND)
      > tmpSolnInfo.numFeasSplits) {
    warning_msg(warnstring,
        "Number of requested cut generating sets is %d, but the maximum number we can choose is %d. Reducing the requested number of splits.\n",
        this->param.getParamVal(ParamIndices::MAX_FRAC_VAR_PARAM_IND),
        tmpSolnInfo.numFeasSplits);
    this->param.setParamVal(ParamIndices::MAX_FRAC_VAR_PARAM_IND,
        tmpSolnInfo.numFeasSplits);
  }
  AdvCuts nb_advcs(true);
  generateCuts(si, nb_advcs, cs, tmpSolnInfo, info);
} /* generateCuts */

/************************************************************************/
/**
 * Generate cuts when SolutionInfo is given
 */
void CglSIC::generateCuts(const OsiSolverInterface &si, OsiCuts& nb_cs,
    OsiCuts& struct_cs, SolutionInfo& probData, const CglTreeInfo info) {
#ifdef TRACE
  printf(
      "\n############### Starting SIC generation. ###############\n");
#endif
  solver = const_cast<OsiSolverInterface *>(&si); // Removes const-ness
  if (solver == NULL) {
    printf("### WARNING: CglSIC::generateCuts(): no solver available.\n");
    return;
  }

  if (!solver->optimalBasisIsAvailable()) {
    printf(
        "### WARNING: CglSIC::generateCuts(): no optimal basis available.\n");
    return;
  }

  xlp = solver->getColSolution();
  
  bool use_advcuts = true;
  AdvCuts *nb_advcs, *struct_advcs;
  try {
    // Try casting
    nb_advcs = dynamic_cast<AdvCuts*>(&nb_cs);
    struct_advcs = dynamic_cast<AdvCuts*>(&struct_cs);
  } catch (std::exception& e) {
//    warning_msg(errorstr,
//      "Not able to cast OsiCuts as AdvCuts, used to keep extra information.\n");
    use_advcuts = false;
  }

  if (use_advcuts && nb_advcs != NULL && struct_advcs != NULL) {
//  try {
//    if (param.getParamVal(ParamIndices::STRENGTHEN_PARAM_IND)) {
//      createMIGs(dynamic_cast<AdvCuts&>(struct_cs), probData);
//    } else {

      // Generate SICs
      genNBSICs(*nb_advcs, probData);

      // Use the non-basic SICs to get struct SICs
      genStructSICs(*struct_advcs, *nb_advcs, probData);
//    }
    struct_advcs->setOsiCuts();

    // Set the internal data from the returned data
//    nb_advcs = dynamic_cast<AdvCuts&>(nb_cs);
//    struct_advcs = dynamic_cast<AdvCuts&>(struct_cs);
  } else {
//  } catch (std::exception& e) {
      // Generate cuts into the internal data first
      AdvCuts nb_advcs(true), struct_advcs(false);
      genNBSICs(nb_advcs, probData);
      genStructSICs(struct_advcs, nb_advcs, probData);

      // Set the return data from the internal data
      nb_advcs.setOsiCuts();
      struct_advcs.setOsiCuts();
      nb_cs = nb_advcs;
      struct_cs = struct_advcs;
//    }
  }
} /* generateCuts */

/***********************************************************************/
/**
 * @brief Generate structSICs, after checking feasibility
 */
void CglSIC::genStructSICs(AdvCuts& structSICs, AdvCuts& NBSICs, 
   SolutionInfo& probData) {
  // Now we set structural cuts from the non-basic ones
  // Convert cuts to structural space, and print them
  structSICs.resize(NBSICs.size());
  for (int currcut = 0; currcut < (int) NBSICs.size(); currcut++) {
    // NBSICs are in complemented NB space
    try {
      AdvCut::convertCutFromJSpaceToStructSpace(structSICs.cuts[currcut],
          dynamic_cast<PointCutsSolverInterface*>(solver),
          probData.nonBasicVarIndex, NBSICs[currcut], true,
          CoinMin(probData.EPS, param.getEPS()));
    } catch (std::exception& e) {
      error_msg(errstr,
        "Failure to convert structSICs to NBSICs at iter %d, with message %s.\n",
        currcut, e.what());
    }
  }

  // The generated NBSICs may be proving "infeasibility" if we have both
  // sides of the split empty and no rays intersect either side of the split.
  // This *could* happen when we are working in the subspace
  // (in the full space, this indeed would be a certificate of infeasibility).
  // So, in the subspace, we add each of the SICs, see if it causes infeasibility,
  // and if it does, we no longer consider that split
  checkSICFeasibility(structSICs, probData, param.paramVal[ParamIndices::SUBSPACE_PARAM_IND]);

  // Remove infeasible cuts
  int num_del = 0;
  for (int cut_ind = 0; cut_ind < (int) structSICs.size(); cut_ind++) {
    if (!probData.splitFeasFlag[cut_ind]) {
      NBSICs.cuts.erase(NBSICs.cuts.begin() + cut_ind - num_del);
      structSICs.cuts.erase(structSICs.cuts.begin() + cut_ind - num_del);
      num_del++;
    }
  }
}

/***********************************************************************/
/**
 * @brief Generate SICs, except only generate at most numCutGenSets splits.
 */
void CglSIC::genNBSICs(AdvCuts& NBSICs, const SolutionInfo& probData) {
  if (!NBSICs.NBSpace) {
    error_msg(errorstr,
      "NBSICs parameter expected to be in non-basic space, but it is not.\n");
  }
  // Although we may not use all these splits,
  // since some may be lopsided, better to calculate all.
  // Much better to calculate one at a time, decide if it is good,
  // then stop when we have reached our limit...
  int numsplits = probData.fractionalCore.size();

  // Resize NBSICs to account for all possible splits
  NBSICs.resize(numsplits);
  std::vector<double> absSumCoeffs(numsplits);

  // Generate a SIC per each split in the subspace
  for (int split = 0; split < numsplits; split++) {
    int s = probData.fractionalCore[split];
    int s_row = probData.rowOfVar[s]; // Clearly will be basic since it is in the fractional core
#ifdef TRACE
    printf(
        "\n## Generating NBSIC for split %d (%d/%d) on variable %s (strengthened? %d). ##\n",
        s, split + 1, numsplits, solver->getColName(s).c_str(),
        (param.getParamVal(ParamIndices::STRENGTHEN_PARAM_IND) > 0));
#endif
    genNBSIC(NBSICs.cuts[split], absSumCoeffs[split], 
        probData.nonBasicVarIndex, probData.numNBOrig,
        s, s_row, CoinMin(probData.EPS, param.getEPS()));
  }
}

/***********************************************************************/
/**
 * @brief Generate a single SIC in the NB space of the form alpha x \ge beta.
 *      Beta is 1 unless there is some complementing.
 */
void CglSIC::genNBSIC(AdvCut& NBSIC, double& absSumCoeffs,
    const std::vector<int>& nonBasicVarIndex,
    const int numNBOrig, const int splitVarIndex,
    const int splitVarRowIndex, const double EPS) {
  const int numcols = solver->getNumCols();
  const int numrows = solver->getNumRows();
  const int numNB = nonBasicVarIndex.size();

  const double splitVarVal = solver->getColSolution()[splitVarIndex];
//  const double pi0 = std::floor(splitVarVal);
  const double f0 = splitVarVal - std::floor(splitVarVal);

  NBSIC.NBSpace = true;
  std::vector<double> coeff(numNB);
  NBSIC.distAlongRay.resize(numNB);
  NBSIC.cgsIndex = splitVarIndex;
  NBSIC.cgsName = solver->getColName(splitVarIndex);
  NBSIC.splitVarIndex[0] = splitVarIndex;
  NBSIC.cutHeur = CutHeuristics::SIC_CUT_GEN;
  double rhs = 1.0;

//  double rayMoveSize;
//  std::vector<int> cstat(numcols), rstat(numrows);

//  std::vector<double> basisCol(numrows);
  const char* rowSense = solver->getRowSense();

  // In case something disabled it before
  // Actually we are passing it as const here, so this will not work
  // Just ensure it is not disabled before
  bool mustDelete = false;
   const CoinWarmStartBasis* basis_ =
       dynamic_cast<CoinWarmStartBasis*>(solver->getPointerToWarmStart(
           mustDelete));

  std::vector<double> basisRowStruct(numcols), basisRowSlack(numrows);
  solver->enableFactorization(); // Even works if const?
//  solver->getBasisStatus(&cstat[0], &rstat[0]);
  { // DEBUG ensure that splitVar is actually basic in this row still
    const int varBasicInRow = dynamic_cast<OsiClpSolverInterface*>(solver)->getModelPtr()->pivotVariable()[splitVarRowIndex];
    if (varBasicInRow != splitVarIndex) {
      error_msg(errorstring,
          "Basic variable in row %d should be %d, but it is %d.\n",
          splitVarRowIndex, splitVarIndex, varBasicInRow);
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }
  }
  solver->getBInvARow(splitVarRowIndex, &basisRowStruct[0], &basisRowSlack[0]);
  solver->disableFactorization();

  for (int col = 0; col < numNB; col++) {
    int currvar = nonBasicVarIndex[col];

    double ray_coeff = (currvar < numcols) ? basisRowStruct[currvar] : basisRowSlack[currvar - numcols];
//    solver->getBInvACol(currvar, &basisCol[0]);
//    double ray_coeff = basisCol[splitVarRowIndex];

    // If the non-basic variable is at its upper bound, flip the sign of the ray
    if (currvar < numcols) {
      if (basis_->getStructStatus(currvar)
          == CoinWarmStartBasis::atUpperBound) { //(cstat[currvar] == 2) {
        ray_coeff *= -1;
      }
    } else {
      if (rowSense[currvar - numcols] == 'G') {
        ray_coeff *= -1;
      }
    }

    coeff[col] = intersectionCutCoeff(ray_coeff, f0, solver, currvar,
        (param.getParamVal(ParamIndices::STRENGTHEN_PARAM_IND) > 0));

    if (isZero(coeff[col], EPS)) {
      coeff[col] = 0.0;
      NBSIC.distAlongRay[col] = solver->getInfinity();
    } else {
      NBSIC.distAlongRay[col] = 1 / coeff[col];
    }
  }
//#ifdef TRACE
  /*
  printf("In the complemented NB space (LP relaxation is origin).\n");
  printf("\tSplit var: %d\n", NBSIC.splitVarIndex[0]);
  printf("\tSplit var name: %s\n",
      solver->getColName(NBSIC.splitVarIndex[0]).c_str());
  printf("\tRHS: %f\n", rhs);
  for (int i = 0; i < (int) coeff.size(); i++) {
    printf("\tCoeff %d: %f\n", i, coeff[i]);
  }
  printf("\n");
  */
//#endif

  // Set packed vector
  NBSIC.setOsiRowCut(coeff, rhs, EPS);

  if (mustDelete && basis_) {
    delete basis_;
  }
} /* genNBSIC */

/**
 * Add each SIC and see if the problem is feasible after adding it.
 * In the subspace, infeasibility may sometimes happen
 * (in the full space, it would prove infeasibility of the problem)
 */
void CglSIC::checkSICFeasibility(AdvCuts& structSIC,
    SolutionInfo& probData, const bool in_subspace) {
#ifdef TRACE
  printf("\n## Checking SIC Feasibility. ##\n");
#endif
  const int numSplits = structSIC.size();

  // If solution is available, just check that none of the cuts violate it (by something unacceptable)
  if (solution) {
    // Check cuts
    for (int cut_ind = 0; cut_ind < (int) structSIC.cuts.size(); cut_ind++) {
      const double rhs = structSIC.cuts[cut_ind].rhs();
      const int num_el = structSIC.cuts[cut_ind].row().getNumElements();
      const int* ind = structSIC.cuts[cut_ind].row().getIndices();
      const double* el = structSIC.cuts[cut_ind].row().getElements();
      const double activity = dotProduct(num_el, ind, el, solution->data());

      if (lessThanVal(activity, rhs, 1e-7)) {
        if (lessThanVal(activity, rhs, 1e-3)) {
          error_msg(errorstring,
              "Cut %d removes optimal solution. Activity: %.10f. Rhs: %.10f.\n",
              cut_ind, activity, rhs);
          writeErrorToLog(errorstring, GlobalVariables::log_file);
          exit(1);
        } else {
          warning_msg(warnstring,
              "Cut %d removes optimal solution. Activity: %.10f. Rhs: %.10f.\n",
              cut_ind, activity, rhs);
        }
      }
    }
  }

  if (param.getParamVal(ParamIndices::SUBSPACE_PARAM_IND) == 0) {
    probData.numFeasSplits = numSplits;
    probData.feasSplitFracCoreIndex.resize(numSplits);
    probData.feasSplitVar.resize(numSplits);
    probData.splitFeasFlag.resize(0);
    probData.splitFeasFlag.resize(numSplits, true);
    probData.infeasSplitVar.clear();
    probData.infeasSplitVar.resize(0);
    for (int cut_ind = 0; cut_ind < numSplits; cut_ind++) {
      probData.feasSplitFracCoreIndex[cut_ind] = cut_ind;
      probData.feasSplitVar[cut_ind] = structSIC[cut_ind].splitVarIndex[0];
    }
    return;
  }

  // In case we previously had some variables set
  probData.numFeasSplits = 0;
  probData.feasSplitFracCoreIndex.clear();
  probData.feasSplitVar.clear();
  probData.splitFeasFlag.clear();
  probData.splitFeasFlag.resize(numSplits, false);
  probData.infeasSplitVar.clear();

  PointCutsSolverInterface* SICLP = dynamic_cast<PointCutsSolverInterface*>(solver->clone());
  SICLP->disableFactorization();
  setClpParameters(SICLP);
  SICLP->initialSolve();
  for (int cut_ind = 0; cut_ind < numSplits; cut_ind++) {
    const int split_var = structSIC[cut_ind].splitVarIndex[0];

    SICLP->applyRowCuts(1, &structSIC.cuts[cut_ind]);
    SICLP->resolve();

    if (!SICLP->isProvenOptimal()) {
#ifdef TRACE
      printf("SICLP not proven optimal after adding SIC %d on var %d.\n", cut_ind, split_var);
#endif
      if (!in_subspace) {
        error_msg(errorstring,
            "SIC %d on var %d proves infeasibility of the problem.\n",
            cut_ind, split_var);
        writeErrorToLog(errorstring, GlobalVariables::log_file);
        exit(1);
      }

      probData.infeasSplitVar.push_back(split_var);
      probData.splitFeasFlag[cut_ind] = false;
      structSIC.cuts[cut_ind].postCutObj = solver->getInfinity();
    } else {
#ifdef TRACE
      printf("SICLP proven optimal after adding SIC %d on var %d.\n", cut_ind, split_var);
#endif
      // If the cut has both sides empty, then we should not consider the cut
      // Depending on YESLOP, may also not consider splits with one side empty
      bool lopBool = false;
      // If we are okay using splits in which both sides are infeasible,
      // then we can consider lopBool = true always.
      if (!in_subspace || USE_EMPTY) {
        lopBool = true;
      } else if (YESLOP) {
        lopBool = probData.fracCoreFloorFeasFlag[cut_ind]
            || probData.fracCoreCeilFeasFlag[cut_ind];
      } else {
        lopBool = probData.fracCoreFloorFeasFlag[cut_ind]
            && probData.fracCoreCeilFeasFlag[cut_ind];
      }
      if (lopBool
          && ((int) probData.feasSplitVar.size()
              < param.getParamVal(ParamIndices::MAX_FRAC_VAR_PARAM_IND))) {
        // And we add it to feasSplit
        probData.feasSplitFracCoreIndex.push_back(cut_ind);
        probData.feasSplitVar.push_back(split_var);
        probData.splitFeasFlag[cut_ind] = true;
      } else {
        probData.infeasSplitVar.push_back(split_var);
        probData.splitFeasFlag[cut_ind] = false;
      }
      structSIC.cuts[cut_ind].postCutObj = SICLP->getObjValue();

#ifdef TRACE
      printf(
          "Cut %d on var %d. LP relaxation optimal value: %f. After cut optimal value: %f.\n",
          cut_ind, split_var, solver->getObjValue(), SICLP->getObjValue());
#endif
    }

    // Don't forget to remove the cut
    std::vector<int> rowsToDelete(1);
    rowsToDelete[0] = probData.numRows;
    SICLP->deleteRows(1, rowsToDelete.data());
  }

  probData.numFeasSplits = probData.feasSplitVar.size();
        
  // Free
  if (SICLP) {
    delete SICLP;
  }
}

/***********************************************************************/
/**
 * @brief Generate GMICs, except only generate at most numCutGenSets splits.
 */
void CglSIC::createMIGs(AdvCuts& structGMICs, const SolutionInfo& probData) {
  // Although we may not use all these splits,
  // since some may be lopsided, better to calculate all.
  // Much better to calculate one at a time, decide if it is good,
  // then stop when we have reached our limit...
  int numsplits = probData.fractionalCore.size();

  // Resize NBSICs to account for all possible splits
  structGMICs.resize(numsplits);

  // Generate a SIC per each split in the subspace
  for (int split = 0; split < numsplits; split++) {
    int s = probData.fractionalCore[split];
    int s_row = probData.rowOfVar[s]; // Clearly will be basic since it is in the fractional core
#ifdef TRACE
    printf("\n## Generating structGMIC for split %d on variable %s. ##\n", s,
        solver->getColName(s).c_str());
#endif
    createMIG(structGMICs.cuts[split], probData.nonBasicVarIndex,
        probData.numNBOrig, s, s_row);
  }
}

/***********************************************************************/
/** Adapted from CglLandP **/
void CglSIC::createMIG(AdvCut &cut, const std::vector<int>& nonBasicVarIndex,
    const int numNBOrig, const int splitVarIndex,
    const int splitVarRowIndex) const {
  const int numcols = solver->getNumCols();
  const int numrows = solver->getNumRows();
  const int numNB = nonBasicVarIndex.size();
  const int numNBSlack = numNB - numNBOrig;

  double splitVarVal = solver->getColSolution()[splitVarIndex];

  cut.NBSpace = false;
  std::vector<double> coeff(numNB);
  cut.distAlongRay.resize(numNB);
  cut.cgsIndex = splitVarIndex;
  cut.cgsName = solver->getColName(splitVarIndex);
  cut.splitVarIndex[0] = splitVarIndex;
  cut.cutHeur = CutHeuristics::SIC_CUT_GEN;
  cut.num_coeff = numcols;
  //double rhs = 1.0;

  const double * colLower = solver->getColLower();
  const double * rowLower = solver->getRowLower();
  const double * colUpper = solver->getColUpper();
  const double * rowUpper = solver->getRowUpper();

//  const CoinWarmStartBasis* basis_ =
//      dynamic_cast<LiftGICsSolverInterface*>(solver)->getPointerToWarmStart();
  bool mustDelete = false;
  const CoinWarmStartBasis* basis_ =
      dynamic_cast<CoinWarmStartBasis*>(solver->getPointerToWarmStart(
          mustDelete));

  std::vector<double> basisRowStruct(numcols), basisRowSlack(numrows);
  solver->enableFactorization();
  solver->getBInvARow(splitVarRowIndex, &basisRowStruct[0], &basisRowSlack[0]);
  solver->disableFactorization();

  double f0 = splitVarVal - std::floor(splitVarVal);

  cut.setUb(COIN_DBL_MAX);
  std::vector<double> vec(numcols + numrows, 0.);
//  double cutRhs = f0;
//  cutRhs = cutRhs * (1 - cutRhs);
  double cutRhs = 1.;
  assert(std::abs(cutRhs) < 1e100); // TODO assert gracefully
  for (int col = 0; col < numNBOrig; col++) {
    int currvar = nonBasicVarIndex[col];
    if (std::abs(basisRowStruct[currvar]) <= 0.) { // TODO why checking exactly zero?
      continue;
    }
    const CoinWarmStartBasis::Status status = basis_->getStructStatus(currvar);
    double value;
    if (status == CoinWarmStartBasis::atUpperBound) { // TODO I think this might be wrong for ub vars
      value = -intersectionCutCoeff(-basisRowStruct[currvar], f0, solver, currvar, true);
      cutRhs += value * colUpper[currvar];
    } else if (status == CoinWarmStartBasis::atLowerBound) {
      value = intersectionCutCoeff(basisRowStruct[currvar], f0, solver, currvar, true);
      cutRhs += value * colLower[currvar];
    } else {
      error_msg(errorstring, "Invalid basis\n");
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      if (mustDelete && basis_) {
        delete basis_;
      }
      exit(1); // probably better to throw than exit
    }

    assert(std::abs(cutRhs) < 1e100); // TODO assert gracefully
    vec[currvar] = value; // had "original index" but we do not work in subspace
  }

  for (int var = 0; var < numNBSlack; var++) {
    int currvar = nonBasicVarIndex[var + numNBOrig];
    int iRow = currvar - numcols;
    if (std::abs(basisRowSlack[iRow]) <= 0.) {
      continue;
    }
    double value;
    if (!isInfinity(rowUpper[iRow])) {
      value = intersectionCutCoeff(basisRowSlack[iRow], f0, solver, currvar, true);
      cutRhs -= value * rowUpper[iRow];
    } else {
      value = -intersectionCutCoeff(-basisRowSlack[iRow], f0, solver, currvar, true);
      cutRhs -= value * rowLower[iRow];
      assert(
          basis_->getArtifStatus(iRow) == CoinWarmStartBasis::atUpperBound
              || !isInfinity(rowUpper[iRow])); // TODO assert gracefully
    }
    vec[currvar] = value; // This had "original index" but we do not work in subspace
    assert(std::abs(cutRhs) < 1e100); // TODO assert gracefully
  }

  //Eliminate slacks
  eliminate_slacks(vec);

  //Pack vec into the cut
  std::vector<int> inds(numcols);
  int nelem = 0;
  for (int i = 0; i < numcols; i++) {
    if (std::abs(vec[i]) > COIN_INDEXED_TINY_ELEMENT) {
      vec[nelem] = vec[i];
      inds[nelem++] = i;
    }
  }

  cut.setLb(cutRhs);
  cut.setRow(nelem, inds.data(), vec.data(), false);
  if (mustDelete && basis_) {
    delete basis_;
  }
} /* createMIG */

void
CglSIC::eliminate_slacks(std::vector<double>& vec) const
{
    const CoinPackedMatrix * mat = solver->getMatrixByCol();
    const CoinBigIndex * starts = mat->getVectorStarts();
    const int * lengths = mat->getVectorLengths();
    const double * values = mat->getElements();
    const CoinBigIndex * indices = mat->getIndices();
    const double * vecSlacks = vec.data() + solver->getNumCols();
    for (int j = 0 ; j < solver->getNumCols() ; j++)
    {
        const CoinBigIndex& start = starts[j];
        CoinBigIndex end = start + lengths[j];
        double & val = vec[j]; //vec[original_index_[j]];
        for (CoinBigIndex k = start ; k < end ; k++)
        {
            val -= vecSlacks[indices[k]] * values[k];
        }
    }
} /* eliminate_slacks */

/***********************************************************************/
void CglSIC::setParam(const CglGICParam &source) {
  param = source;
} /* setParam */

/*********************************************************************/
// Returns true if needs optimal basis to do cuts
bool CglSIC::needsOptimalBasis() const {
  return true;
}

/*********************************************************************/
// Create C++ lines to get to current state
std::string CglSIC::generateCpp(FILE * fp) {
  return "CglSIC";
}

/**********************************************************/
void CglSIC::printvecINT(const char *vecstr, const int *x, int n) const {
  int num, fromto, upto;

  num = (n / 10) + 1;
  printf("%s :\n", vecstr);
  for (int j = 0; j < num; ++j) {
    fromto = 10 * j;
    upto = 10 * (j + 1);
    if (n <= upto)
      upto = n;
    for (int i = fromto; i < upto; ++i)
      printf(" %4d", x[i]);
    printf("\n");
  }
  printf("\n");
} /* printvecINT */

/**********************************************************/
void CglSIC::printvecDBL(const char *vecstr, const double *x, int n) const {
  int num, fromto, upto;

  num = (n / 10) + 1;
  printf("%s :\n", vecstr);
  for (int j = 0; j < num; ++j) {
    fromto = 10 * j;
    upto = 10 * (j + 1);
    if (n <= upto)
      upto = n;
    for (int i = fromto; i < upto; ++i)
      printf(" %7.3f", x[i]);
    printf("\n");
  }
  printf("\n");
} /* printvecDBL */

/**********************************************************/
void CglSIC::printvecDBL(const char *vecstr, const double *elem,
    const int * index, int nz) const {
  printf("%s\n", vecstr);
  int written = 0;
  for (int j = 0; j < nz; ++j) {
    written += printf("%d:%.3f ", index[j], elem[j]);
    if (written > 70) {
      printf("\n");
      written = 0;
    }
  }
  if (written > 0) {
    printf("\n");
  }

} /* printvecDBL */

