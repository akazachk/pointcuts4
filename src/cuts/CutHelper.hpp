//============================================================================
// Name        : CutHelper.hpp
// Author      : akazachk
// Version     : 0.2014.03.20
// Copyright   : Your copyright notice
// Description : Helper functions of cuts
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#pragma once

#include "Point.hpp"
#include "Vertex.hpp"
#include "Ray.hpp"
#include "AdvCut.hpp"
#include "SolutionInfo.hpp"

#include "typedefs.hpp"

#include "IntersectionInfo.hpp"

void setConstantObjectiveFromPackedVector(
    PointCutsSolverInterface* const cutSolver, const double val = 0.,
    const int numIndices = 0, const int* indices = NULL);

/***********************************************************************/
/***********************************************************************/
/** Methods related to generating cuts from non-basic space */
/***********************************************************************/
/***********************************************************************/

/**
 * Generates cuts based on points and rays, in the *non-basic* space
 * @return Number of cuts generated
 */
int genCutsFromPointsRaysNB(PointCutsSolverInterface* const cutSolver,
    const double beta, const std::vector<int>& nonZeroColIndex, AdvCuts & cuts,
    int& num_cuts_this_cgs, int& num_cuts_total, int& num_obj_tried,
    const PointCutsSolverInterface* const solver, const SolutionInfo & solnInfo,
//    const IntersectionInfo& interPtsAndRays,
    const int cgsIndex, const std::string& cgsName, const AdvCuts& structSICs);

/**
 * Set the cut solver objective based on option, in the *non-basic* space
 * Options are:
 *  1. max_eff
 */
void setCutSolverObjectiveFromVertexNB(const int vert_ind,
    PointCutsSolverInterface* const  cutSolver,
    const std::vector<Vertex>& vertexStore);

/***********************************************************************/
/***********************************************************************/
/** Methods related to generating cuts from structural space */
/***********************************************************************/
/***********************************************************************/

///**
// * Generates cuts based on points and rays
// * @return Number of cuts generated
// */
//int genCutsFromPointsRays(AdvCuts & pcuts, const LiftGICsSolverInterface* const  solver,
//    const SolutionInfo & solnInfo, const IntersectionInfo& interPtsAndRays,
//    const int splitVar, const int split_ind, const AdvCuts& structSICs);

bool setupCutSolverHelper(PointCutsSolverInterface* const cutSolver,
    const int cgsIndex, const int numPoints, const int numRays);

/**
 * Set up solver from points and rays provided (VPC version)
 */
bool setupCutSolver(PointCutsSolverInterface* const cutSolver,
    std::vector<int>& nonZeroColIndex,
    const std::vector<IntersectionInfo>& interPtsAndRays,
    std::vector<double>& ortho, const double scale, const int cgsIndex,
    const bool fixFirstPoint);
/**
 * Set up solver from points and rays provided (PHA version)
 */
bool setupCutSolver(PointCutsSolverInterface* const cutSolver,
    const IntersectionInfo& interPtsAndRays, /*const double beta,*/
    const int cgsIndex);

/**
 * Set the cut solver objective based on option
 * Options are:
 *  1. max_eff
 */
/*
void setCutSolverObjective(const std::string option,
    PointCutsSolverInterface* const cutSolver, const double * objVertex);
*/

/**
 * Change row index rhs to beta (so becomes >= beta for these rows)
 */
void changeCutSolverBeta(PointCutsSolverInterface* const  cutSolver,
    const IntersectionInfo& interPtsAndRays, const double beta);

/***********************************************************************/
/***********************************************************************/
/** Cut heuristics **/
/***********************************************************************/
/***********************************************************************/

/**
 * @brief Try the (1,...,1) obj, all points, then select among rays ``intelligently''
 * @return Number generated cuts
 */
int targetStrongAndDifferentCuts(PointCutsSolverInterface* const cutSolver,
    const double beta, const std::vector<int>& nonZeroColIndex, AdvCuts& cuts,
    int& num_cuts_this_cgs, int& num_cuts_total, int& num_obj_tried,
    const PointCutsSolverInterface* const solver, const SolutionInfo & probData,
    const std::vector<double>& ortho,
//    const IntersectionInfo& interPtsAndRays,
//    const AdvCut* const objCut,
    const int cgsIndex, const std::string& cgsName, const AdvCuts& structSICs,
    const Stats& timeStats, const std::string& timeName, const bool inNBSpace);

/**
 * @brief Try being tight on each of the intersection points/rays
 */
int tightOnPointsRaysCutGeneration(PointCutsSolverInterface* const cutSolver,
    const double beta, const std::vector<int>& nonZeroColIndex, AdvCuts& cuts, int& num_cuts_this_cgs,
    int& num_cuts_total, int& num_obj_tried,
    const PointCutsSolverInterface* const solver, const SolutionInfo & probData,
    //const IntersectionInfo& interPtsAndRays,
    const int cgsIndex, const std::string& cgsName, const AdvCuts& structSICs,
    const Stats& timeStats, const std::string& timeName,
    const bool inNBSpace = true);

/**
 * Generate a cut, if possible, from the solver
 * Rhs is given as beta
 */
int genCutsFromCutSolver(AdvCuts & cuts,
    PointCutsSolverInterface* const cutSolver,
    const std::vector<int>& nonZeroColIndex,
    const PointCutsSolverInterface* const origSolver,
    const SolutionInfo & origProbData, const double beta, const int cgsIndex,
    const std::string& cgsName, const AdvCuts& structSICs,
    int& num_cuts_this_cgs, int& num_cuts_total, const bool inNBSpace,
    const CutHeuristics cutHeur,
    const bool tryExtraHard = false,
    const bool doPivoting = param.getParamVal(ParamIndices::PIVOT_PARAM_IND));
int genCutsHelper(AdvCuts & cuts,
    PointCutsSolverInterface* const cutSolver,
    const std::vector<int>& nonZeroColIndex,
    const PointCutsSolverInterface* const origSolver,
    const SolutionInfo & origProbData, const double beta, const int cgsIndex,
    const std::string& cgsName, const AdvCuts& structSICs,
    int& num_cuts_this_cgs, int& num_cuts_total, const bool inNBSpace,
    const CutHeuristics cutHeur);
int exitGenCutsFromCutSolver(PointCutsSolverInterface* const cutSolver,
    const int num_cuts_generated);

/**********************************************************/
/**
 * Pivot to neighbor of solution with highest Euclidean distance
 * Corresponds to lower 2-norm
 */
void pivotToNbrHigherEuclDist(OsiClpSolverInterface* cutSolver,
    std::vector<double>& best_sol);

/**
 * @brief Try cutting off each of the vertices generated in the intermediate stages
 */
int cutVerticesCutGeneration(PointCutsSolverInterface* const cutSolver,
    const double beta, const std::vector<int>& nonZeroColIndex, AdvCuts & cuts,
    int& num_cuts_this_cgs, int& num_cuts_total, int& num_obj_tried,
    const PointCutsSolverInterface* const solver, const SolutionInfo & probData,
    //const IntersectionInfo& interPtsAndRays,
    const std::vector<Vertex>& vertexStore, const int cgsIndex,
    const std::string& cgsName, const AdvCuts& structSICs);

/**
 * For each ray, look at set of intersection points (across all splits) generated by this ray
 * Call this set P(r), corresponding to ray r
 * Let p(r,s) be the point generated by ray r for split s
 * Let d(r,s) = distance along ray r to split s
 * Then for any split s, we can try to cut points p(r,s') for all s' : d(r,s') < d(r,s)
 */
void splitSharingCutGeneration(std::vector<PointCutsSolverInterface*>& cutSolver,
    const std::vector<int>& nonZeroColIndex,
    AdvCuts & cuts, std::vector<int>& num_cuts_per_split,
    int& num_cuts_total, int& num_obj_tried,
    const std::vector<IntersectionInfo>& interPtsAndRays,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
//    const std::vector<Ray>& rayStore,
    const std::vector<Vertex>& vertexStore,
    const std::vector<Hplane>& hplaneStore, const AdvCuts& structSICs,
    const std::vector<bool>& isCutSolverPrimalFeasible);

/**
 * @brief For the ray that generated the given point, find the deepest split the ray intersects
 */
int findDeepestSplit(const IntersectionInfo& interPtsAndRays, const int pt_ind,
    const int split_ind, const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo,
//    const std::vector<Ray>& rayStore,
    const std::vector<Vertex>& vertexStore,
    const std::vector<Hplane>& hplaneStore);

/**
 * We want to solve the bilinear program
 * min <\alpha, x>
 * s.t.
 * <\alpha, p> \ge 1 \forall p \in \pointset
 * <\alpha, r> \ge 0 \forall r \in \rayset
 * x \in Pbar (P + all SICs)
 */
int iterateDeepestCutPostMSIC(AdvCuts & cuts, int& num_cuts_this_cgs,
    int& num_cuts_total, int& num_obj_tried,
    PointCutsSolverInterface* const cutSolver,
    const std::vector<int>& nonZeroColIndex,
    const PointCutsSolverInterface* const origSolver,
    const SolutionInfo & origSolnInfo, const double beta, const int cgsIndex,
    const std::string& cgsName, const AdvCuts& structSICs,
    const bool inNBSpace);

int exitFromIterateDeepestCutPostMSIC(const int num_cuts_gen,
    PointCutsSolverInterface* const cutSolver,
    PointCutsSolverInterface* const MSICSolver);

/**
 * Removes the fixed variable var from the system defining tmpSolver
 * and adjusts the constant side appropriately using fracCol
 * (the column of A corresponding to var)
 */
bool removeFixedVar(OsiSolverInterface* tmpSolver, const int var,
    const double fixed_val, const CoinPackedVectorBase& fracCol);

/**********************************************************/
/**
 * Adds constraint to tmpSolver to fix to a facet of S
 */
bool fixDisjTerm(OsiClpSolverInterface* tmpSolver,
    const std::vector<std::vector<int> >& termIndices,
    const std::vector<std::vector<double> >& termCoeff,
    const std::vector<double>& termRHS, const bool set_equal = false);
bool fixDisjTermUsingBounds(OsiClpSolverInterface* tmpSolver,
    const std::vector<std::vector<int> >& termIndices,
    const std::vector<std::vector<double> >& termCoeff,
    const std::vector<double>& termRHS, const bool set_equal = false);
bool fixDisjTerm(OsiClpSolverInterface* tmpSolver,
    const std::vector<int>& termIndices, const std::vector<double>& termCoeff,
    const double termRHS, const bool set_equal = false);

/**********************************************************/
/**
 * Fixes bound on variable to fixed_val
 * (should be more efficient than adding a constraint)
 */
bool fixFacetOfS(OsiClpSolverInterface* tmpSolver, const int var,
    const double fixed_val);
/**
 * @return Whether the solver was feasible and give objective value if so
 */
//bool checkOptimality(double & objVal, const OsiSolverInterface* const solver,
//    const int col);

/**
 * @return Whether the solver was feasible
 */
bool solveFloor(double & objVal, const OsiSolverInterface* const solver, const int col);

/**
 * @return Whether the solver was feasible
 */
bool solveCeil(double & objVal, const OsiSolverInterface* const solver,
    const int col);

/**
 * If fixFloor, we add the cut xk <= floorxk. Otherwise, we add xk >= ceilxk.
 * Will not be added if this is a cut that already exists in the pcuts.
 */
bool addOneSidedCut(AdvCuts & pcuts, bool fixFloor,
    const OsiSolverInterface* const solver, const int splitVar);
bool addOneSidedCut(AdvCuts & pcuts, const OsiSolverInterface* const solver,
    const int num_coeff, const int* ind, const double* coeff, const double lb,
    const int cgs_ind, const std::string& cgsName);
int fixToDisjTerm(AdvCuts & pcuts, const OsiSolverInterface* const solver,
    const std::vector<std::vector<int> >& termIndices,
    const std::vector<std::vector<double> >& termCoeff,
    const std::vector<double>& termRHS, const int cgs_ind,
    const std::string& cgsName);

/**
 * If fixFloor, we create the cut xk <= floorxk.
 * Otherwise, we create xk >= ceilxk.
 */
void genOneSidedOsiRowCut(OsiRowCut & currCut, const bool fixFloor,
    const OsiSolverInterface* const solver, const int splitVar);

/**
 * @brief Checks to make sure there is no big disparity among cut coefficients
 */
bool badlyScaled(int num_cols, const double* colSolution, const double rhs,
    const double SENSIBLE_MAX = param.getMAXDYN(), const double EPS = 1e-20); // TODO remove hardcoded eps

/**
 * Scales ray generated.
 * If max is not too big, then we do not bother rescaling
 * If min / norm too small, then we do not do that scaling
 *
 * @return status: -1 is bad ray, don't add it; 0 is no scaling; 1 is okay scaling
 */
int scalePrimalRay(int numCols, double** rayToScale, const double SENSIBLE_MAX =
    100.0);

/**
 * Compute score as in SCIP from initialized orthogonalities
 */
void computeScore(AdvCuts& cs, const int pos);

/**
 * As in SCIP
 */
void updateOrthogonolaties(AdvCuts& cs, const OsiRowCut& cut,
    const double min_cut_orthogononality,
    const std::vector<int>& roundWhenCutApplied);

/**
 * As in SCIP, returns the position of the best non-forced cut in the cuts array
 * Currently not forcing any cuts
 * */
int getBestCutIndex(const AdvCuts& cs,
    const std::vector<int>& roundWhenCutApplied);

/**
 * Generic way to apply a set of cuts to a solver
 * @return Solver value after adding the cuts, if they were added successfully
 */
double applyCuts(PointCutsSolverInterface* solver, const AdvCuts& cs,
   const int startCutIndex, const int numCutsOverload, double compare_obj);
double applyCuts(PointCutsSolverInterface* solver, const AdvCuts& cs, 
    const int numCutsOverload = -1, 
    double compare_obj = std::numeric_limits<double>::lowest());

/**
 * Generic way to apply a set of cuts to a solver in rounds (simple version)
 * @return Solver value after adding the cuts, if they were added successfully
 */
double applyCutsInRounds(PointCutsSolverInterface* solver, AdvCuts& cs,
    const int numCutsToAddPerRound, const int maxRounds,
    double compare_obj = std::numeric_limits<double>::lowest());

/**
 * Generic way to apply a set of cuts to a solver (complex version)
 * @return Solver value after adding the cuts, if they were added successfully
 */
double applyCutsInRounds(PointCutsSolverInterface* solver, AdvCuts& cs,
    std::vector<int>& numCutsByRound, std::vector<double>& boundByRound,
    std::vector<double>& basisCond, const int numCutsToAddPerRound,
    std::vector<int>* const oldRoundWhenCutApplied = NULL, 
    bool populateRoundWhenCutApplied = false,
    const int maxRounds = 5,
    double compare_obj = std::numeric_limits<double>::lowest());

//void compareCuts(const int numSimpleClosedPCuts, const int numOSICs,
//    const int numPCuts, const LiftGICsSolverInterface* const  solver,
//    const LiftGICsSolverInterface* const  OSICSolver,
//    const LiftGICsSolverInterface* const  simpleClosedPCutsSolver,
//    const LiftGICsSolverInterface* const  PCutSolver, const AdvCuts & structOSICs,
//    const AdvCuts & simpleClosedPCuts, const AdvCuts & structPCuts);
/**
 * Get bound from applying
 * 1. SICs
 * 2. ClosedPCuts
 * 3. PCuts
 * 4. GICs
 * 5. SICs + ClosedPCuts
 * 6. SICs + ClosedPCuts + PCuts
 * 7. SICs + ClosedPCuts + PCuts + GICs
 * 8. ClosedPCuts + PCuts + GICs
 */
void compareCuts(const int num_obj_tried,
    const OsiSolverInterface* const solver, AdvCuts& structSICs,
    AdvCuts& structGMICuts, AdvCuts& structLandPCuts, AdvCuts& structTilted,
    AdvCuts& structVPCs, AdvCuts& structPHAs,
    const double strongBranchingLB, const double strongBranchingUB,
    const int numCutsToAddPerRound, std::string& output);

//void compareCuts(const int num_obj_tried, const LiftGICsSolverInterface* const  solver,
//    const double postOSICObj, const double postGICObj, const AdvCuts & structOSICs,
//    const AdvCuts& structGICs);

/***********************************************************************/
/***********************************************************************/
/** Methods related to adding points and rays */
/***********************************************************************/
/***********************************************************************/

/**
 * @return True if point added; false otherwise.
 */
bool addPoint(std::vector<Point>& point, const Point & tmpPt,
    const bool checkForDuplicate = true);

/**
 * vec is the full (non-compressed) point
 * @return True if point added; false otherwise.
 */
bool addPoint(std::vector<Point>& point, const std::vector<double> &vec,
    const bool checkForDuplicate = true);

/**
 * vec is the full (non-compressed) point
 * @return True if point added; false otherwise.
 */
bool addPoint(std::vector<Point>& point, int numElem, const double* vec,
    const bool checkForDuplicate = true);

/**
 * vec is the packed point
 * @return True if point added; false otherwise.
 */
bool addPackedPoint(std::vector<Point>& point, int coBasisSize,
    const std::vector<int> &index, const std::vector<double> &vec,
    const bool checkForDuplicate = true);

/**
 * vec is the packed ray
 * @return True if ray added; false otherwise.
 */
bool addPackedRay(std::vector<Ray>& ray, //int coBasisSize,
    const std::vector<int> &index, const std::vector<double> &vec,
    const bool checkForDuplicate = true);

/**
 * @return True if ray added; false otherwise.
 */
bool addRay(std::vector<Ray>& ray, const Ray & tmpRay,
    const bool checkForDuplicate = true);

/**
 * vec is the full (non-compressed) ray
 * @return True if ray added; false otherwise.
 */
bool addRay(std::vector<Ray>& ray, const std::vector<double> &vec,
    const bool checkForDuplicate = true);

/**
 * vec is the full (non-compressed) ray
 * @return True if ray added; false otherwise.
 */
bool addRay(std::vector<Ray>& ray, int numElem, const double* vec,
    const bool checkForDuplicate = true);

/**
 * Check if point has tmpPoint
 */
bool hasPoint(const std::vector<Point> & point, const Point & tmpPoint);

/**
 * Check if ray has tmpRay
 */
bool hasRay(const std::vector<Ray> & ray, const Ray & tmpRay);

/***********************************************************************/
/***********************************************************************/
/** Methods related to adding an OsiRowCut cut */
/***********************************************************************/
/***********************************************************************/
//int duplicateCutIndex(const OsiRowCut & tmpCut);

/***********************************************************************/
/***********************************************************************/
/** Methods to help with generating points and rays */
/***********************************************************************/
/***********************************************************************/
//
//bool findHplaneToIntersectRay(int& varIn, double& rayDist,
//    const Point & origPoint, const Ray & rayOut,
//    const LiftGICsSolverInterface* const  solver, const SolutionInfo & solnInfo);
//
//void addRaysFromActivation(const int rayOut, const Point & origPoint,
//    const Point & origPointFullSpace, std::vector<Ray>& ray,
//    std::vector<Ray>& rayFullSpace, const LiftGICsSolverInterface* const  tmpSolver,
//    const SolutionInfo & origProbData);
//
//void calculateA0FullInCompNBSpace(std::vector<double>& a0,
//    const Point & pointFullSpace, const std::vector<Ray>& rayFullSpace,
//    const LiftGICsSolverInterface* const  solver,
//    const SolutionInfo & origProbData);
