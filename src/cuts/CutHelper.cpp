//============================================================================
// Name        : CutHelper.cpp
// Author      : akazachk
// Version     : 0.2014.03.20
// Description : Helper functions of cuts
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#include <cmath> // preferred over math.h
#include <numeric> // inner_prod
#include "GlobalConstants.hpp"
#include "Utility.hpp"
#include "Output.hpp"
#include "CutHelper.hpp"
#include "CoinTime.hpp"
#include "CglSIC.hpp"
#include "ClpPresolve.hpp"

//#include <queue> // for priority_queue, i.e., min-heap

/**
 * Set the cut solver objective coefficients based on a packed vector
 * We do not zero out the other coefficients unless requested
 */
void addToObjectiveFromPackedVector(PointCutsSolverInterface* const cutSolver,
    const CoinPackedVectorBase* vec, const bool zeroOut = false,
    const double mult = 1.) {
  if (zeroOut) {
    for (int i = 0; i < cutSolver->getNumCols(); i++) {
      cutSolver->setObjCoeff(i, 0.);
    }
  }

  if (vec) {
    const double twoNorm = vec->twoNorm() / cutSolver->getNumCols();
    for (int i = 0; i < (*vec).getNumElements(); i++) {
      const int col = (*vec).getIndices()[i];
      const double elem = (*vec).getElements()[i];
      const double coeff = cutSolver->getObjCoefficients()[col];
      cutSolver->setObjCoeff(col, coeff + elem * mult / twoNorm);
    }
  }
} /* addToObjectiveFromPackedVector */

/*
void setConstantObjectiveFromPackedVector(
    PointCutsSolverInterface* const cutSolver, const double val = 0.,
    const CoinPackedVectorBase* vec = NULL) {
  if (vec) {
    for (int i = 0; i < (*vec).getNumElements(); i++) {
      cutSolver->setObjCoeff((*vec).getIndices()[i], val);
    }
  } else {
    for (int i = 0; i < cutSolver->getNumCols(); i++) {
      cutSolver->setObjCoeff(i, val);
    }
  }
}*/ /* setObjectiveFromPackedVector */

void setConstantObjectiveFromPackedVector(
    PointCutsSolverInterface* const cutSolver, const double val,
    const int numIndices, const int* indices) {
  if (numIndices > 0 && indices) {
    for (int i = 0; i < numIndices; i++) {
      cutSolver->setObjCoeff(indices[i], val);
    }
  } else {
    for (int i = 0; i < cutSolver->getNumCols(); i++) {
      cutSolver->setObjCoeff(i, val);
    }
  }
} /* setConstantObjectiveFromPackedVector */

/***********************************************************************/
/***********************************************************************/
/** Methods related to generating cuts from non-basic space */
/***********************************************************************/
/***********************************************************************/

/**
 * Generates cuts based on points and rays in IntersectionInfo, which are in the *non-basic* space
 * Cuts are generated in the structural space however
 * @return Number of cuts generated
 */
int genCutsFromPointsRaysNB(PointCutsSolverInterface* const cutSolver,
    const double beta, const std::vector<int>& nonZeroColIndex, AdvCuts & cuts,
    int& num_cuts_this_cgs, int& num_cuts_total, int& num_obj_tried,
    const PointCutsSolverInterface* const solver, const SolutionInfo & probData,
    //const IntersectionInfo& interPtsAndRays,
    const int cgsIndex, const std::string& cgsName, const AdvCuts& structSICs) {
  if (reachedCutLimit(num_cuts_this_cgs, num_cuts_total)) {
    return 0;
  }
  std::vector<double> obj(cutSolver->getNumCols(), 0.0);

  // Generate cuts from this solver
  const int init_num_cuts = (int) cuts.size();

  if (param.paramVal[NUM_CUTS_ITER_BILINEAR_PARAM_IND] >= 1) {
    // Try the bilinear optimization
    GlobalVariables::timeStats.start_timer(
        CutHeuristicsName[CutHeuristics::ITER_BILINEAR_CUT_HEUR] + "_TIME");
#ifdef TRACE
    printf(
        "\n## Using iterative procedure to get deepest cut post SICs (cgs %d/%d). ##\n",
        cgsIndex + 1, param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND));
#endif

    iterateDeepestCutPostMSIC(cuts, num_cuts_this_cgs, num_cuts_total,
        num_obj_tried, cutSolver, nonZeroColIndex, solver, probData, beta,
        cgsIndex, cgsName, structSICs, true);
    GlobalVariables::timeStats.end_timer(
        CutHeuristicsName[CutHeuristics::ITER_BILINEAR_CUT_HEUR] + "_TIME");
  }

  if (reachedCutLimit(num_cuts_this_cgs, num_cuts_total)) {
    return (int) cuts.size() - init_num_cuts;
  }

  if (param.getParamVal(ParamIndices::USE_UNIT_VECTORS_HEUR_PARAM_IND) >= 1) {
    // Try each of the axis directions
    GlobalVariables::timeStats.start_timer(
        CutHeuristicsName[CutHeuristics::UNIT_VECTORS_CUT_HEUR] + "_TIME");
#ifdef TRACE
    printf(
        "\n## Try finding deepest cut with respect to each of the non-basic axis directions (cgs %d/%d). ##\n",
        cgsIndex + 1, param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND));
#endif
    int nb_dir = 0;
    while (!reachedCutLimit(num_cuts_this_cgs, num_cuts_total)
        && (nb_dir < cutSolver->getNumCols())) {
      obj[nb_dir] = 1.0;
      cutSolver->setObjective(obj.data());
      num_obj_tried++;
      genCutsFromCutSolver(cuts, cutSolver, nonZeroColIndex, solver, probData,
          beta, cgsIndex, cgsName, structSICs, num_cuts_this_cgs, num_cuts_total,
          true, CutHeuristics::UNIT_VECTORS_CUT_HEUR);
      obj[nb_dir] = 0.0;
      nb_dir++;
    }
    GlobalVariables::timeStats.end_timer(
        CutHeuristicsName[CutHeuristics::UNIT_VECTORS_CUT_HEUR] + "_TIME");
  } /* try unit vectors */

  return (int) cuts.size() - init_num_cuts;
} /* genCutsFromPointsRaysNB */

/**
 * Set the cut solver objective based on option, in the *non-basic* space
 * Options are:
 *  1. max_eff
 */
void setCutSolverObjectiveFromVertexNB(const int vert_ind,
    PointCutsSolverInterface* const cutSolver,
    const std::vector<Vertex>& vertexStore) {
  // We set the objective function to be the one to maximize "effectiveness"
  // That is, we want to cut off objVertex by as much as possible
  // To do this we *minimize* alpha * objVertex
  // If < beta, then alpha x >= beta valid and cuts off objVertex
  // However, objVertex is just the origin, which will not lead to any meaningful cut.
  // We instead optimize in the direction (1,1,...,1), which may actually be feasible,
  // but certainly for some t, t * (1,1,...,1) is cut by, say, the SIC.
  cutSolver->setObjSense(1.0);

  const int numElem = vertexStore[vert_ind].getNumElements();
  if (numElem == 0) {
    std::vector<double> obj(cutSolver->getNumCols(), 1.0);
    cutSolver->setObjective(obj.data());
  } else {
    std::vector<double> obj(cutSolver->getNumCols(), 0.0);
    setFullVecFromPackedVec(obj, vertexStore[vert_ind]);
    cutSolver->setObjective(obj.data());
  }
} /* setCutSolverObjectiveFromVertexNB */

/***********************************************************************/
/***********************************************************************/
/** Methods related to generating cuts */
/***********************************************************************/
/***********************************************************************/

bool setupCutSolverHelper(PointCutsSolverInterface* const cutSolver,
    const int cgsIndex, const int numPoints, const int numRays) {
  if (numPoints >= 0 && numRays >= 0) {
    // Store statistics about the PRLP
    const double density =
        (double) cutSolver->getMatrixByCol()->getNumElements()
            / (cutSolver->getNumRows() * cutSolver->getNumCols());
    if (density < GlobalVariables::minDensityPRLP) {
      GlobalVariables::minDensityPRLP = density;
    }
    if (density > GlobalVariables::maxDensityPRLP) {
      GlobalVariables::maxDensityPRLP = density;
    }
    if (cutSolver->getNumRows() < GlobalVariables::minNumRowsPRLP) {
      GlobalVariables::minNumRowsPRLP = cutSolver->getNumRows();
    }
    if (cutSolver->getNumRows() > GlobalVariables::maxNumRowsPRLP) {
      GlobalVariables::maxNumRowsPRLP = cutSolver->getNumRows();
    }
    if (cutSolver->getNumCols() < GlobalVariables::minNumColsPRLP) {
      GlobalVariables::minNumColsPRLP = cutSolver->getNumCols();
    }
    if (cutSolver->getNumCols() > GlobalVariables::maxNumColsPRLP) {
      GlobalVariables::maxNumColsPRLP = cutSolver->getNumCols();
    }
    GlobalVariables::totalNumPointsPRLP += numPoints;
    if (numPoints < GlobalVariables::minNumPointsPRLP) {
      GlobalVariables::minNumPointsPRLP = numPoints;
    }
    if (numPoints > GlobalVariables::maxNumPointsPRLP) {
      GlobalVariables::maxNumPointsPRLP = numPoints;
    }
    GlobalVariables::totalNumRaysPRLP += numRays;
    if (numRays < GlobalVariables::minNumRaysPRLP) {
      GlobalVariables::minNumRaysPRLP = numRays;
    }
    if (numRays > GlobalVariables::maxNumRaysPRLP) {
      GlobalVariables::maxNumRaysPRLP = numRays;
    }
  }

  GlobalVariables::timeStats.start_timer(GlobalConstants::SETUP_PRLP_TIME);
  setClpParameters(cutSolver);
  cutSolver->getModelPtr()->setMaximumIterations(param.getCUTSOLVER_MAX_ITER());
  cutSolver->getModelPtr()->setNumberIterations(0);
  cutSolver->getModelPtr()->setMaximumSeconds(CoinMax(60., 10 * param.getCUTSOLVER_TIMELIMIT()));
  cutSolver->setHintParam(OsiDoPresolveInInitial,
      (param.getParamVal(ParamIndices::CUT_PRESOLVE_PARAM_IND) > 0) ?
          true : false);
  cutSolver->setHintParam(OsiDoPresolveInResolve,
      (param.getParamVal(ParamIndices::CUT_PRESOLVE_PARAM_IND) > 1) ?
          true : false);

  // Check that the cutSolver is feasible for the zero objective function
  GlobalVariables::timeStats.start_timer(
      CutHeuristicsName[CutHeuristics::DUMMY_OBJ_CUT_HEUR] + "_TIME");

  const double start = CoinCpuTime();
  cutSolver->initialSolve();
  const double end = CoinCpuTime();

  // Set PRLP time limit based on this initial solve, if the time is not yet set and it is not permanently set to infinity
  if (isVal(param.getCUTSOLVER_TIMELIMIT(), -1.)) {
    param.setCUTSOLVER_TIMELIMIT(
        CoinMax(param.getMIN_CUTSOLVER_TIMELIMIT(), (end - start)));
  }

//  checkSolverOptimality(cutSolver, false, param.getCUTSOLVER_TIMELIMIT(), false); // do not do reduced cost based changes

  GlobalVariables::timeStats.end_timer(
      CutHeuristicsName[CutHeuristics::DUMMY_OBJ_CUT_HEUR] + "_TIME");

  // Check that cutSolver is not primal infeasible
  // This can happen if 0 \in cone(\rayset), for instance, in the non-basic space.
  // We might also get that the cut LP is "bad" and does not solve quickly
  // in which case we abort with numerical issues as the error.
  bool retval;
  if (cutSolver->isProvenOptimal()) {
    retval = true;
  } else if (cutSolver->isProvenPrimalInfeasible() && !cutSolver->isAbandoned()) {
    warning_msg(errorstring,
        "PRLP (cgs %d/%d) is primal infeasible; giving up on this point-ray collection.\n",
        cgsIndex + 1, param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND));
    GlobalVariables::numCutSolverFails[CutSolverFails::PRIMAL_CUTSOLVER_NO_OBJ_FAIL_IND]++;
    retval = false;
  } else if (cutSolver->isIterationLimitReached()
      || cutSolver->getModelPtr()->hitMaximumIterations()) {
    warning_msg(errorstring,
        "PRLP (cgs %d/%d) is having numerical issues; giving up on this point-ray collection.\n",
        cgsIndex + 1, param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND));
    GlobalVariables::numCutSolverFails[CutSolverFails::NUMERICAL_ISSUES_NO_OBJ_FAIL_IND]++;
    retval = false;
  } else if (cutSolver->isProvenDualInfeasible() && !cutSolver->isAbandoned()) {
    // This should never happen, but we are going to chock it up to numerical problems
    warning_msg(errorstring,
        "PRLP (cgs %d/%d) is having numerical issues; giving up on this point-ray collection.\n",
        cgsIndex + 1, param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND));
    GlobalVariables::numCutSolverFails[CutSolverFails::NUMERICAL_ISSUES_NO_OBJ_FAIL_IND]++;
    retval = false;
  } else {
    // Something else happened; still bad...
    warning_msg(errorstring,
        "PRLP (cgs %d/%d) is having numerical issues; giving up on this point-ray collection.\n",
        cgsIndex + 1, param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND));
    GlobalVariables::numCutSolverFails[CutSolverFails::NUMERICAL_ISSUES_NO_OBJ_FAIL_IND]++;
    retval = false;
  }
  GlobalVariables::timeStats.end_timer(GlobalConstants::SETUP_PRLP_TIME);
  return retval;
} /* setupCutSolverHelper */

/**
 * Set up solver from points and rays provided.
 * The rows are \alpha (p or r) \ge \beta = 1 (or 0 or -1)
 * VPC VERSION
 * @return Is cutSolver primal feasible?
 */
bool setupCutSolver(PointCutsSolverInterface* const cutSolver,
    std::vector<int>& nonZeroColIndex,
    const std::vector<IntersectionInfo>& interPtsAndRays,
    std::vector<double>& ortho, const double scale, const int cgsIndex,
    const bool fixFirstPoint) {
  GlobalVariables::timeStats.start_timer(GlobalConstants::SETUP_PRLP_TIME);
  const int num_disj_terms = interPtsAndRays.size();
  int first_term;
  for (first_term = 0; first_term < num_disj_terms; first_term++) {
    if (interPtsAndRays[first_term].getNumRows() > 0) {
      break;
    }
  }

  // Build the matrix as well as col lower/upper bounds
  const int num_cols = interPtsAndRays[first_term].getNumCols();
  std::vector<double> colLB(num_cols, -1 * cutSolver->getInfinity());
  std::vector<double> colUB(num_cols, cutSolver->getInfinity());
  std::vector<double> rowLB;
  rowLB.reserve(num_disj_terms * (num_cols + 1));
  int numElementsEstimate = 0; // estimate max size of sparse coefficient matrix, for reserve

  // We will keep the matrix sorted from least to greatest orthogonality with the objective
  // The points will be first, and then the rays
  std::vector<int> disjTermOfPoint, disjTermOfRay;
  std::vector<int> rowOfPoint, rowOfRay;
  std::vector<double> objDepthPoints, objDepthRays;
  disjTermOfPoint.reserve(num_disj_terms);
  rowOfPoint.reserve(num_disj_terms);
  objDepthPoints.reserve(num_disj_terms);
  disjTermOfRay.reserve(num_disj_terms * num_cols);
  rowOfRay.reserve(num_disj_terms * num_cols);
  objDepthRays.reserve(num_disj_terms * num_cols);
//  std::vector<int> disjTermStart(num_disj_terms, 0), rowToDelete;

  // First pass is just to get the orthogonalities and fill the vectors,
  // but we will also remove any singleton rows by setting them as bounds
  for (int t = first_term; t < num_disj_terms; t++) {
//    disjTermStart[t] = mx.getNumRows();
//    mx.bottomAppendPackedMatrix(interPtsAndRays[t]);
    for (int r = 0; r < interPtsAndRays[t].getNumRows(); r++) {
      const int num_vec_el = interPtsAndRays[t].getVectorSize(r);
      const double vec_rhs =
          (interPtsAndRays[t].RHS[r] != 0.) ?
              interPtsAndRays[t].RHS[r] * scale : interPtsAndRays[t].RHS[r];
      // Check if it is a bound
      if (num_vec_el == 1) {
        // row is coeff * var >= rhs
        const int start_ind = interPtsAndRays[t].getVectorFirst(r);
        const double coeff = interPtsAndRays[t].getElements()[start_ind];
        const int var = interPtsAndRays[t].getIndices()[start_ind];
        const double old_lb = colLB[var];
        const double old_ub = colUB[var];
        if (isZero(vec_rhs)) {
          if (lessThanVal(coeff, 0.)) {
            if (old_ub > 0.) {
              colUB[var] = 0.;
            }
          } else if (greaterThanVal(coeff, 0.)) {
            if (old_lb < 0.) {
              colLB[var] = 0.;
            }
          } else {
            // Do nothing, because this is a 0 >= 0 row (shouldn't happen... but who knows)
          }
        } /* if zero rhs */
        else {
          if (isVal(coeff, 0.)) {
            // Do nothing, because this is 0 >= something
            if (lessThanVal(vec_rhs, 0.)) {
              error_msg(errorstring,
                  "We have an infeasible row: %.2f * x%d >= %.2f.\n", coeff,
                  var, vec_rhs);
              writeErrorToLog(errorstring, GlobalVariables::log_file);
              exit(1);
            }
          } else {
            const double new_bound = vec_rhs / coeff;
            if (lessThanVal(coeff, 0.)) {
              if (old_ub > new_bound) {
                colUB[var] = new_bound;
              }
            } else {
              if (old_lb < new_bound) {
                colLB[var] = new_bound;
              }
            }
          }
        } /* else it is a non-zero rhs */
      } /* if num_vec_el == 1, it is a bound */

      // When rhs non-zero, we input it anyway, to keep for objective function purposes
      if (num_vec_el > 1 || !isZero(vec_rhs)) {
        numElementsEstimate += num_vec_el;
        if (!isZero(vec_rhs)) {
          disjTermOfPoint.push_back(t);
          rowOfPoint.push_back(r);
          objDepthPoints.push_back(interPtsAndRays[t].objDepth[r]);
        } else {
          disjTermOfRay.push_back(t);
          rowOfRay.push_back(r);
          objDepthRays.push_back(interPtsAndRays[t].objDepth[r]);
        }
      } /* we add it for processing */
//      else {
//        rowToDelete.push_back(disjTermStart[t] + r);
//      }
    } /* iterate over the rows in the matrix */
  } /* iterate over the disjunctive terms we are adding in */

  // Sort by orthogonality
  const int numPointsToProcess = objDepthPoints.size();
  std::vector<int> sortIndexPoints(numPointsToProcess);
  for (int i = 0; i < numPointsToProcess; i++) {
    sortIndexPoints[i] = i;
  }
  sort(sortIndexPoints.begin(), sortIndexPoints.end(),
      index_cmp_asc<const double*>(objDepthPoints.data())); // ascending order

  const int numRaysToProcess = objDepthRays.size();
  std::vector<int> sortIndexRays(numRaysToProcess);
  for (int i = 0; i < numRaysToProcess; i++) {
    sortIndexRays[i] = i;
  }
  sort(sortIndexRays.begin(), sortIndexRays.end(),
      index_cmp_asc<const double*>(objDepthRays.data())); // ascending order

  // Should we check for duplicates?
  const bool checkForDuplicates = true;
  std::vector<bool> isDuplicatePoint, isDuplicateRay;
  if (checkForDuplicates) {
    // Check points for duplicates
    isDuplicatePoint.resize(numPointsToProcess, false);
    for (int p1 = 1; p1 < numPointsToProcess; p1++) {
      const int ind1 = sortIndexPoints[p1];
      const int t1 = disjTermOfPoint[ind1];
      const int r1 = rowOfPoint[ind1];
      const double ortho1 = objDepthPoints[ind1];
      if (!isVal(objDepthPoints[sortIndexPoints[p1-1]], ortho1)) {
        continue;
      }
      int howDissimilar = 2;
      const CoinShallowPackedVector vec1 = interPtsAndRays[t1].getVector(r1);
      const double rhs1 = interPtsAndRays[t1].RHS[r1];
      // Iterate through previous points until bottom or the orthogonality is different
      for (int p2 = p1 - 1; p2 >= 0; p2--) {
        if (isDuplicatePoint[p2]) {
          continue;
        }
        const int ind2 = sortIndexPoints[p2];
        const double ortho2 = objDepthPoints[ind2];
        if (!isVal(ortho1, ortho2)) {
          break;
        }
        const int t2 = disjTermOfPoint[ind2];
        const int r2 = rowOfPoint[ind2];
        const double rhs2 = interPtsAndRays[t2].RHS[r2];
        const CoinShallowPackedVector vec2 = interPtsAndRays[t2].getVector(r2);
        // Ensure constants line up
        if (!((greaterThanVal(rhs1, 0.) && greaterThanVal(rhs2, 0.))
            || (lessThanVal(rhs1, 0.) && lessThanVal(rhs2, 0.)))) {
          continue;
        }
        howDissimilar = isRowDifferent(vec1, rhs1, vec2, rhs2, param.getDIFFEPS());
        if (howDissimilar == -1) {
          // This means the current row is better somehow, and we should replace r2
          isDuplicatePoint[p2] = true;
          numElementsEstimate -= vec2.getNumElements();
//          rowToDelete.push_back(disjTermStart[t2] + r2);
        }
        if (howDissimilar != 2) {
          isDuplicatePoint[p1] = true;
          numElementsEstimate -= vec1.getNumElements();
//          rowToDelete.push_back(disjTermStart[t1] + r1);
          break;
        }
      } /* iterate over previously added points that have same objDepth */
    } /* iterate over points to find duplicates */

    // Check rays for duplicates
    isDuplicateRay.resize(numRaysToProcess, false);
    for (int p1 = 1; p1 < numRaysToProcess; p1++) {
      const int ind1 = sortIndexRays[p1];
      const int t1 = disjTermOfRay[ind1];
      const int r1 = rowOfRay[ind1];
      const CoinShallowPackedVector vec1 = interPtsAndRays[t1].getVector(r1);
      const double rhs1 = interPtsAndRays[t1].RHS[r1];
      const double ortho1 = objDepthRays[ind1];
      int howDissimilar = 2;
      // Iterate through previous rays until bottom or the orthogonality is different
      for (int p2 = p1 - 1; p2 >= 0; p2--) {
        if (isDuplicateRay[p2]) {
          continue;
        }
        const int ind2 = sortIndexRays[p2];
        const double ortho2 = objDepthRays[ind2];
        if (!isVal(ortho1, ortho2)) {
          break;
        }
        const int t2 = disjTermOfRay[ind2];
        const int r2 = rowOfRay[ind2];
        const CoinShallowPackedVector vec2 = interPtsAndRays[t2].getVector(r2);
        const double rhs2 = interPtsAndRays[t2].RHS[r2];
        howDissimilar = isRowDifferent(vec1, rhs1, vec2, rhs2, param.getDIFFEPS());
        if (howDissimilar == -1) {
          // This means the current row is better somehow, and we should replace r2
          isDuplicateRay[p2] = true;
          numElementsEstimate -= vec2.getNumElements();
//          rowToDelete.push_back(disjTermStart[t2] + r2);
        }
        if (howDissimilar != 2) {
          isDuplicateRay[p1] = true;
          numElementsEstimate -= vec1.getNumElements();
//          rowToDelete.push_back(disjTermStart[t1] + r1);
          break;
        }
      } /* iterate over previously added rays that have same objDepth */
    } /* iterate over rays to find duplicates */
  } /* check for duplicates */

//  // Sort rowToDelete and delete the corresponding rows from mx
//  std::sort(rowToDelete.begin(), rowToDelete.end());
//  mx.deleteRows(rowToDelete.size(), rowToDelete.data());

  // Now input into the matrix
  CoinPackedMatrix mx; // create a new col-ordered matrix
  mx.reverseOrdering(); // make it row-ordered
  mx.setDimensions(0, num_cols);
  mx.reserve(numPointsToProcess + numRaysToProcess, numElementsEstimate, false);
  //    (numPointsToProcess + numRaysToProcess) * num_cols, false);
  int numPoints = 0, numRays = 0;

  // First the points
  std::vector<int> rowInMxOfPoint;
  if (fixFirstPoint) {
    rowInMxOfPoint.resize(numPointsToProcess, -1);
  }
  ortho.clear();
  ortho.reserve(numPointsToProcess + numRaysToProcess);
  for (int p = 0; p < numPointsToProcess; p++) {
    if (checkForDuplicates && isDuplicatePoint[p]) {
      continue;
    }
    const int ind = sortIndexPoints[p];
    const int t = disjTermOfPoint[ind];
    const int r = rowOfPoint[ind];
    const double rhs = interPtsAndRays[t].RHS[r] * scale;
    //const CoinShallowPackedVector vec = interPtsAndRays[t].getVector(r);
    //mx.appendRow(vec);
    mx.appendRow(interPtsAndRays[t].getVectorSize(r),
        interPtsAndRays[t].getIndices() + interPtsAndRays[t].getVectorFirst(r),
        interPtsAndRays[t].getElements()
            + interPtsAndRays[t].getVectorFirst(r));
    rowLB.push_back(rhs);
    numPoints++;
    ortho.push_back(objDepthPoints[ind]);

    if (fixFirstPoint) {
      rowInMxOfPoint[p] = mx.getNumRows() - 1; // index of point whose rhs we will fix
    }
  } /* input points */

  // Then the rays
  for (int p = 0; p < numRaysToProcess; p++) {
    if (checkForDuplicates && isDuplicateRay[p]) {
      continue;
    }
    const int ind = sortIndexRays[p];
    const int t = disjTermOfRay[ind];
    const int r = rowOfRay[ind];
    const double rhs = interPtsAndRays[t].RHS[r];
    //const CoinShallowPackedVector vec = interPtsAndRays[t].getVector(r);
    //mx.appendRow(vec);
    mx.appendRow(interPtsAndRays[t].getVectorSize(r),
        interPtsAndRays[t].getIndices() + interPtsAndRays[t].getVectorFirst(r),
        interPtsAndRays[t].getElements()
            + interPtsAndRays[t].getVectorFirst(r));
    rowLB.push_back(rhs);
    numRays++;
    ortho.push_back(objDepthRays[ind]);
  } /* input rays */

  // Clean matrix
  mx.cleanMatrix();

  // Filter out columns that are empty
  const bool shouldDelete = true;
  if (shouldDelete) {
    std::vector<int> deleteColIndex;
    const int* length = mx.countOrthoLength();
    for (int col_ind = 0; col_ind < mx.getNumCols(); col_ind++) {
      if (length[col_ind] == 0) {
        const bool ignore_lb = isNegInfinity(colLB[col_ind]) || isZero(colLB[col_ind]);
        const bool ignore_ub = isInfinity(colUB[col_ind]) || isZero(colUB[col_ind]);
        if (!ignore_lb || !ignore_ub) {
          nonZeroColIndex.push_back(col_ind);
        } else {
          deleteColIndex.push_back(col_ind);
        }
      } else {
        nonZeroColIndex.push_back(col_ind);
      }
    }
    if (length) {
      delete[] length;
      length = NULL;
    }
    // Delete the columns from mx, as well as from colLB and colUB
    mx.deleteCols(deleteColIndex.size(), deleteColIndex.data());
    for (int pos = deleteColIndex.size() - 1; pos >= 0; pos--) {
      colLB.erase(colLB.begin() + deleteColIndex[pos]);
      colUB.erase(colUB.begin() + deleteColIndex[pos]);
    }
  }

  // Load problem into cutSolver
  // Defaults (in COIN-OR):
  // colLB: 0 **** This should be -inf (except when changed above)
  // colUB: inf
  // obj coeff: 0
  // rowSense: >=
  // rhs: 0 **** We want to change this to beta for the points
  // rowRange: 0
  cutSolver->loadProblem(mx, colLB.data(), colUB.data(), NULL, NULL,
      rowLB.data(), NULL);
  cutSolver->disableFactorization();
  //cutSolver->getModelPtr()->cleanMatrix();

  // Set to equality the first point if so desired
  if (fixFirstPoint) {
    std::vector<int> haveFixedPoint(num_disj_terms, false);
    for (int p = 0; p < numPointsToProcess; p++) {
      const int row_ind = rowInMxOfPoint[p];
      if (row_ind < 0) {
        continue;
      }
      const int ind = sortIndexPoints[p];
      const int t = disjTermOfPoint[ind];
      if (haveFixedPoint[t]) {
        continue;
      }
      const double curr_rhs = rowLB[row_ind];
      if (!isZero(curr_rhs)) {
        cutSolver->setRowUpper(row_ind, curr_rhs);
      }
      haveFixedPoint[t] = true;
    }
  } /* fix first point in each disj term */

  GlobalVariables::timeStats.end_timer(GlobalConstants::SETUP_PRLP_TIME);

#ifdef TRACE
  printf(
      "\n## Check feasibility using all zeroes objective (tentative cgs %d/%d). ##\n",
      cgsIndex + 1, param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND));
#endif

  return setupCutSolverHelper(cutSolver, cgsIndex, numPoints, numRays);
} /* setupCutSolver (VPC) */

/**
 * Set up solver from points and rays provided.
 * The rows are \alpha (p or r) \ge \beta = 1 (or 0 or -1)
 * PHA version
 * We do not check for duplicate points / rays here because we already did this before when creating interPtsAndRays
 * However, we need to check for bounds, to make solving easier
 */
bool setupCutSolver(PointCutsSolverInterface* const cutSolver,
    const IntersectionInfo& interPtsAndRays, /*const double beta,*/
    const int cgsIndex) {
  const double scale = 1.;

  GlobalVariables::timeStats.start_timer(GlobalConstants::SETUP_PRLP_TIME);
  // Build the matrix as well as col lower/upper bounds
  CoinPackedMatrix mx; // create a new col-ordered matrix
  mx.reverseOrdering(); // make it row-ordered
  const int num_cols = interPtsAndRays.getNumCols();
  mx.setDimensions(0, num_cols);
  std::vector<double> colLB(num_cols, -1.0 * cutSolver->getInfinity());
  std::vector<double> colUB(num_cols, cutSolver->getInfinity());
  std::vector<double> rowLB;
  rowLB.reserve(interPtsAndRays.getNumRows());

  // We will keep the matrix sorted from least to greatest orthogonality with the objective
  // The points will be first, and then the rays
  const int numPointsToReserve = interPtsAndRays.numPoints > 0 ? interPtsAndRays.numPoints : 10;
  const int numRaysToReserve = interPtsAndRays.numRays > 0 ? interPtsAndRays.numRays : 10;
  std::vector<int> rowOfPoint, rowOfRay;
  std::vector<double> objDepthPoints, objDepthRays;
  rowOfPoint.reserve(numPointsToReserve);
  objDepthPoints.reserve(numPointsToReserve);
  rowOfRay.reserve(numRaysToReserve);
  objDepthRays.reserve(numRaysToReserve);

  // First pass is just to get the orthogonalities and fill the vectors,
  // but we will also remove any singleton rows by setting them as bounds
  for (int r = 0; r < interPtsAndRays.getNumRows(); r++) {
    const int num_vec_el = interPtsAndRays.getVectorSize(r);
    const double vec_rhs =
        (interPtsAndRays.RHS[r] > 0) ?
            interPtsAndRays.RHS[r] * scale : interPtsAndRays.RHS[r];
    // Check if it is a bound
    if (num_vec_el == 1) {
      // row is coeff * var >= rhs
      const CoinShallowPackedVector vec = interPtsAndRays.getVector(r);
      const double coeff = vec.getElements()[0];
      const int var = vec.getIndices()[0];
      const double old_lb = colLB[var];
      const double old_ub = colUB[var];
      if (isZero(vec_rhs)) {
        if (lessThanVal(coeff, 0.)) {
          if (old_ub > 0.) {
            colUB[var] = 0.;
          }
        } else if (greaterThanVal(coeff, 0.)) {
          if (old_lb < 0.) {
            colLB[var] = 0.;
          }
        } else {
          // Do nothing, because this is a 0 >= 0 row (shouldn't happen... but who knows)
        }
      } /* if zero rhs */
      else {
        if (isVal(coeff, 0.)) {
          // Do nothing, because this is 0 >= something
          if (lessThanVal(vec_rhs, 0.)) {
            error_msg(errorstring,
                "We have an infeasible row: %.2f * x%d >= %.2f.\n", coeff, var,
                vec_rhs);
            writeErrorToLog(errorstring, GlobalVariables::log_file);
            exit(1);
          }
        } else {
          const double new_bound = vec_rhs / coeff;
          if (lessThanVal(coeff, 0.)) {
            if (old_ub > new_bound) {
              colUB[var] = new_bound;
            }
          } else {
            if (old_lb < new_bound) {
              colLB[var] = new_bound;
            }
          }
        }
      } /* else it is a non-zero rhs */
    } /* if num_vec_el == 1, it is a bound */

    // When rhs non-zero, we input it anyway, to keep for objective function purposes
    if (num_vec_el > 1 || !isZero(vec_rhs)) {
      if (!isZero(vec_rhs)) {
        rowOfPoint.push_back(r);
        objDepthPoints.push_back(interPtsAndRays.objDepth[r]);
      } else {
        rowOfRay.push_back(r);
        objDepthRays.push_back(interPtsAndRays.objDepth[r]);
      }
    } /* else, we add it for processing */
  } /* iterate over the rows in the matrix */

  // Sort by orthogonality
  const int numPointsToProcess = objDepthPoints.size();
  std::vector<int> sortIndexPoints(numPointsToProcess);
  for (int i = 0; i < numPointsToProcess; i++) {
    sortIndexPoints[i] = i;
  }
  sort(sortIndexPoints.begin(), sortIndexPoints.end(),
      index_cmp_dsc<const double*>(objDepthPoints.data())); // descending order

  const int numRaysToProcess = objDepthRays.size();
  std::vector<int> sortIndexRays(numRaysToProcess);
  for (int i = 0; i < numRaysToProcess; i++) {
    sortIndexRays[i] = i;
  }
  sort(sortIndexRays.begin(), sortIndexRays.end(),
      index_cmp_asc<const double*>(objDepthRays.data())); // ascending order

  // Should we check for duplicates?
  const bool checkForDuplicates = true;
  std::vector<bool> isDuplicatePoint, isDuplicateRay;
  if (checkForDuplicates) {
    // Check points for duplicates
    isDuplicatePoint.resize(numPointsToProcess, false);
    for (int p1 = 1; p1 < numPointsToProcess; p1++) {
      const int ind1 = sortIndexPoints[p1];
      const int r1 = rowOfPoint[ind1];
      const double ortho1 = objDepthPoints[ind1];
      if (!isVal(objDepthPoints[sortIndexPoints[p1 - 1]], ortho1)) {
        continue;
      }
      int howDissimilar = 2;
      const CoinShallowPackedVector vec1 = interPtsAndRays.getVector(r1);
      const double rhs1 = interPtsAndRays.RHS[r1];
      // Iterate through previous points until bottom or the orthogonality is different
      for (int p2 = p1 - 1; p2 >= 0; p2--) {
        if (isDuplicatePoint[p2]) {
          continue;
        }
        const int ind2 = sortIndexPoints[p2];
        const double ortho2 = objDepthPoints[ind2];
        if (!isVal(ortho1, ortho2)) {
          break;
        }
        const int r2 = rowOfPoint[ind2];
        const double rhs2 = interPtsAndRays.RHS[r2];
        const CoinShallowPackedVector vec2 = interPtsAndRays.getVector(r2);
        // Ensure constants line up
        if (!((greaterThanVal(rhs1, 0.) && greaterThanVal(rhs2, 0.))
            || (lessThanVal(rhs1, 0.) && lessThanVal(rhs2, 0.)))) {
          continue;
        }
        howDissimilar = isRowDifferent(vec1, rhs1, vec2, rhs2,
            param.getDIFFEPS());
        if (howDissimilar == -1) {
          // This means the current row is better somehow, and we should replace r2
          isDuplicatePoint[p2] = true;
        }
        if (howDissimilar != 2) {
          isDuplicatePoint[p1] = true;
          break;
        }
      } /* iterate over previously added points that have same objDepth */
    } /* iterate over points to find duplicates */

    // Check rays for duplicates
    isDuplicateRay.resize(numRaysToProcess, false);
    for (int p1 = 1; p1 < numRaysToProcess; p1++) {
      const int ind1 = sortIndexRays[p1];
      const int r1 = rowOfRay[ind1];
      const CoinShallowPackedVector vec1 = interPtsAndRays.getVector(r1);
      const double rhs1 = interPtsAndRays.RHS[r1];
      const double ortho1 = objDepthRays[ind1];
      int howDissimilar = 2;
      // Iterate through previous rays until bottom or the orthogonality is different
      for (int p2 = p1 - 1; p2 >= 0; p2--) {
        if (isDuplicateRay[p2]) {
          continue;
        }
        const int ind2 = sortIndexRays[p2];
        const double ortho2 = objDepthRays[ind2];
        if (!isVal(ortho1, ortho2)) {
          break;
        }
        const int r2 = rowOfRay[ind2];
        const CoinShallowPackedVector vec2 = interPtsAndRays.getVector(r2);
        const double rhs2 = interPtsAndRays.RHS[r2];
        howDissimilar = isRowDifferent(vec1, rhs1, vec2, rhs2,
            param.getDIFFEPS());
        if (howDissimilar == -1) {
          // This means the current row is better somehow, and we should replace r2
          isDuplicateRay[p2] = true;
        }
        if (howDissimilar != 2) {
          isDuplicateRay[p1] = true;
          break;
        }
      } /* iterate over previously added rays that have same objDepth */
    } /* iterate over rays to find duplicates */
  } /* check for duplicates */

  // Now input into the matrix
  int numPoints = 0, numRays = 0;

  // First the points
  for (int p = 0; p < numPointsToProcess; p++) {
    if (isDuplicatePoint[p]) {
      continue;
    }
    const int ind = sortIndexPoints[p];
    const int r = rowOfPoint[ind];
    const CoinShallowPackedVector vec = interPtsAndRays.getVector(r);
    const double rhs = interPtsAndRays.RHS[r] * scale;
    mx.appendRow(vec);
    rowLB.push_back(rhs);
    numPoints++;
  } /* input points */

  // Then the rays
  for (int p = 0; p < numRaysToProcess; p++) {
    if (isDuplicateRay[p]) {
      continue;
    }
    const int ind = sortIndexRays[p];
    const int r = rowOfRay[ind];
    const CoinShallowPackedVector vec = interPtsAndRays.getVector(r);
    const double rhs = interPtsAndRays.RHS[r];
    mx.appendRow(vec);
    rowLB.push_back(rhs);
    numRays++;
  } /* input rays */

  // Load problem into cutSolver
  // Defaults:
  // colLB: 0 **** This should be -inf (except when changed above)
  // colUB: inf
  // obj coeff: 0
  // rowSense: >=
  // rhs: 0 **** We want to change this to beta for the points
  // rowRange: 0
  cutSolver->loadProblem(mx, colLB.data(), colUB.data(), NULL, NULL,
      rowLB.data(), NULL);

  cutSolver->disableFactorization();
  cutSolver->getModelPtr()->cleanMatrix();
  GlobalVariables::timeStats.end_timer(GlobalConstants::SETUP_PRLP_TIME);

#ifdef TRACE
  printf(
      "\n## Check feasibility with all zeroes objective (cgs %d/%d). ##\n",
      cgsIndex + 1, param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND));
#endif
  return setupCutSolverHelper(cutSolver, cgsIndex, numPoints, numRays);
} /* setupCutSolver (PHA) */

/**
 * Set the cut solver objective based on option
 * Options are:
 *  1. max_eff
 */
/*
void setCutSolverObjective(const std::string option,
    PointCutsSolverInterface* const cutSolver, const double * objVertex) {
  // We set the objective function to be the one to maximize "effectiveness"
  // That is, we want to cut off objVertex by as much as possible
  // To do this we *minimize* alpha * objVertex
  // If < beta, then alpha x >= beta valid and cuts off objVertex
  cutSolver->setObjSense(1.0);
  cutSolver->setObjective(objVertex);
}*/ /* setCutSolverObjective */

/**
 * Change row index rhs to beta (so becomes >= beta for these rows)
 */
void changeCutSolverBeta(PointCutsSolverInterface* const cutSolver,
    const IntersectionInfo& interPtsAndRays, const double beta) {
  for (int i = 0; i < interPtsAndRays.getNumRows(); i++) {
    if (!isZero(interPtsAndRays.RHS[i]))
      cutSolver->setRowLower(i, beta);
  }
} /* changeCutSolverBeta */

/**
 * @brief Generate a cut, if possible, from the solver
 * Rhs is given as beta
 *
 * @return -1 * (fail index + 1) if it is something that means to discontinue this solver
 */
int genCutsFromCutSolver(AdvCuts & cuts,
    PointCutsSolverInterface* const cutSolver,
    const std::vector<int>& nonZeroColIndex,
    const PointCutsSolverInterface* const origSolver,
    const SolutionInfo & origProbData, const double beta, const int cgsIndex,
    const std::string& cgsName, const AdvCuts& structSICs,
    int& num_cuts_this_cgs, int& num_cuts_total, const bool inNBSpace,
    const CutHeuristics cutHeur, const bool tryExtraHard, const bool doPivoting) {
  GlobalVariables::timeStats.start_timer(GlobalConstants::SOLVE_PRLP_TIME);
  GlobalVariables::numObjFromHeur[cutHeur]++;
  if (reachedCutLimit(num_cuts_this_cgs, num_cuts_total)) {
    return exitGenCutsFromCutSolver(cutSolver,
        -1 * (CutSolverFails::CUT_LIMIT_FAIL_IND + 1));
  }

  // First we save the old solution; this is just to have a quick way to check that the same solution was found
  std::vector<double> old_solution; 
  if (cuts.size() > 0 && cutSolver->isProvenOptimal()) {
    old_solution.assign(cutSolver->getColSolution(), cutSolver->getColSolution() + cutSolver->getNumCols());
  }

  // Resolve
  cutSolver->getModelPtr()->setNumberIterations(0);
  const double timeLimit = (tryExtraHard) ?  -1. : param.getCUTSOLVER_TIMELIMIT(); // maybe go unlimited time this round
  cutSolver->getModelPtr()->setMaximumSeconds(timeLimit);
  cutSolver->resolve();

//  // If the objective value seems weird, we may want to try an initial solve
//  if (cutSolver->isProvenOptimal()) {
//    const double objValue = cutSolver->getObjValue();
//    if (isZero(objValue, param.getEPS())
//        && !isZero(objValue, param.getDIFFEPS())) {
//      cutSolver->getModelPtr()->setNumberIterations(0);
//      cutSolver->getModelPtr()->setMaximumSeconds(
//          param.getCUTSOLVER_TIMELIMIT());
//      cutSolver->initialSolve();
//    }
//  }

  if (!checkSolverOptimality(cutSolver, false, timeLimit, true)) {
//    if (cutSolver->getModelPtr()->hitMaximumIterations()) {
//      // Sometimes Clp gets stuck
//      cutSolver->getModelPtr()->setMaximumSeconds(timeLimit);
//      cutSolver->initialSolve();
//      checkSolverOptimality(cutSolver, false, timeLimit, true);
//    }
    if (cutSolver->isIterationLimitReached()) {
      return exitGenCutsFromCutSolver(cutSolver,
          -1 * (CutSolverFails::ITERATION_CUTSOLVER_FAIL_IND + 1));
    }
    if (cutSolver->getModelPtr()->hitMaximumIterations()) {
      return exitGenCutsFromCutSolver(cutSolver,
          -1 * (CutSolverFails::TIMELIMIT_CUTSOLVER_FAIL_IND + 1));
    }
    if (cutSolver->isAbandoned()) {
      return exitGenCutsFromCutSolver(cutSolver,
          -1 * (CutSolverFails::ABANDONED_CUTSOLVER_FAIL_IND + 1));
    }
  }

  // If "nan" objective, then something went terribly wrong; try again
  if (std::isnan(cutSolver->getObjValue())) {
#ifdef TRACE
    warning_msg(warnstr,
        "Encountered NaN objective value on cgs %d/%d.\n",
        cgsIndex + 1, param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND));
#endif
    cutSolver->getModelPtr()->setNumberIterations(0);
    cutSolver->getModelPtr()->setMaximumSeconds(timeLimit);
    cutSolver->initialSolve();
    checkSolverOptimality(cutSolver, false, timeLimit, true);
  }

  // Too expensive
//  if (!cutSolver->isProvenOptimal() && !cutSolver->isProvenDualInfeasible()) {
//    // Try resolving since this seems to help sometimes
//    cutSolver->getModelPtr()->setMaximumSeconds(timeLimit);
//    cutSolver->initialSolve();
//    checkSolverOptimality(cutSolver, false, timeLimit, true);
//  }

  if (cutSolver->isProvenOptimal()
      && isNegInfinity(cutSolver->getObjValue(), param.getINFINITY())) {
    // We essentially have a ray, though it is not being reported as such
    // Try resolving because this fixes the issue sometimes
    cutSolver->getModelPtr()->setMaximumSeconds(timeLimit);
    cutSolver->initialSolve();
    checkSolverOptimality(cutSolver, false, timeLimit, true);
  }

  if (cutSolver->isProvenDualInfeasible()) {
//    // Is it really true?
//    cutSolver->getModelPtr()->setMaximumSeconds(param.getCUTSOLVER_TIMELIMIT());
//    cutSolver->resolve();
//
//    // This may stop it from being dual infeasible
//    if (!cutSolver->isProvenOptimal()
//        && !cutSolver->isProvenDualInfeasible()) {
//      // Sometimes Clp gets stuck
//      cutSolver->getModelPtr()->setMaximumSeconds(param.getCUTSOLVER_TIMELIMIT());
//      cutSolver->initialSolve();
//    }
//
    // Homogeneous cuts never cut the LP optimum in NB space so no point trying rays
    if (cutSolver->isProvenDualInfeasible() && inNBSpace) {
      return exitGenCutsFromCutSolver(cutSolver,
          -1 * (CutSolverFails::DUAL_CUTSOLVER_FAIL_IND + 1));
    }
  }

  int num_cuts_generated = 0;
  const int num_rays_to_get = 1; // Actually this is the only possibility currently

  // If it is proven optimal, we can simply obtain the solution
  // If it is dual infeasible, then it is unbounded (or infeasible, but it would say so)
  // in which case we need to get the ray and use that as the solution
  if (cutSolver->isProvenOptimal()) {
//    // Ensure the solution is not poorly scaled (a ray that is being treated as a point, for example)
//    if (badlyScaled(cutSolver->getNumCols(), cutSolver->getColSolution(), beta,
//        param.getMAXDYN(), origProbData.EPS)) {
//      cutSolver->getModelPtr()->setMaximumSeconds(param.getCUTSOLVER_TIMELIMIT());
//      cutSolver->resolve(); // 8/9/18 AMK: there is trouble with some settings, e.g., -i ./bell3a.mps.gz -o . --vpc_depth=1 --cgs=7 --num_cgs=0 --overload_max_cuts=10000 --use_tight_points_heur=1000 --strengthen=2
//      checkSolverOptimality(cutSolver, false, param.getCUTSOLVER_TIMELIMIT(), true);
//      if (cutSolver->isProvenOptimal()
//          && badlyScaled(cutSolver->getNumCols(), cutSolver->getColSolution(),
//              beta, param.getMAXDYN(), origProbData.EPS)) {
//        return exitGenCutsFromCutSolver(cutSolver,
//            -1 * (CutSolverFails::SCALING_CUTSOLVER_FAIL_IND + 1));
//      }
//    }

    // Try to generate the cut (but first, test against previous solution)
    if (!old_solution.empty()) {
      double maxDiff = 0.;
      for (int ind = 0; ind < cutSolver->getNumCols(); ind++) {
        const double diff = std::abs(cutSolver->getColSolution()[ind] - old_solution[ind]);
        if (diff > maxDiff) {
          maxDiff = diff;
        }
      }
      //const double norm_old = std::sqrt(std::inner_product(old_solution.data(), old_solution.data() + cutSolver->getNumCols(), old_solution.data(), 0.0L));
      //const double norm_new = std::sqrt(std::inner_product(cutSolver->getColSolution(), cutSolver->getColSolution() + cutSolver->getNumCols(), cutSolver->getColSolution(), 0.0L));
      //const double scalarprod = dotProduct(old_solution.data(), cutSolver->getColSolution(), cutSolver->getNumCols());
      //const double ortho = 1. - scalarprod / (norm_new * norm_old);
      //if (isZero(ortho, param.getMINORTHOGONALITY())) {
      //if (isZero(ortho, 1e-20)) {
      if (isZero(maxDiff, origProbData.EPS)) {
        return exitGenCutsFromCutSolver(cutSolver,
            -1 * (CutSolverFails::DUPLICATE_GIC_CUTSOLVER_FAIL_IND + 1));
      }
      //      else {
      //        return exitGenCutsFromCutSolver(cutSolver,
      //            -1 * (CutSolverFails::ORTHOGONALITY_GIC_CUTSOLVER_FAIL_IND + 1));
      //      }
      //}
    } /* check whether it is the same solution as before */

    const int return_code = genCutsHelper(cuts, cutSolver, nonZeroColIndex,
        origSolver, origProbData, beta, cgsIndex, cgsName, structSICs,
        num_cuts_this_cgs, num_cuts_total, inNBSpace, cutHeur);
    if (return_code > 0) {
      num_cuts_generated += return_code;
    } else {
      return exitGenCutsFromCutSolver(cutSolver, return_code);
    }

    if (cutSolver->isProvenOptimal() && doPivoting) {
      const int numCols = cutSolver->getNumCols();
      const int numRows = cutSolver->getNumRows();
      //const double* sol = cutSolver->getColSolution();
      //double min_sqnorm = std::inner_product(sol, sol + numCols, sol, 0.0L);
      //  int min_index = -1;

      // Set up vectors
      int varOut = -1;
      int dirnOut = 0;
      int pivotRow = -1;
      double dist = -1.;
      std::vector<int> varBasicInRow(numRows);
      cutSolver->enableSimplexInterface(true);
      cutSolver->getBasics(&varBasicInRow[0]);
      cutSolver->disableSimplexInterface();

      // For each non-basic variable, try to pivot along the ray
      OsiClpSolverInterface* copySolver;
      int ray_ind = -1;
      for (int varIn = 0; varIn < numCols + numRows; varIn++) {
        // Check if it is non-basic
        if (!isRayVar(cutSolver, varIn)) {
          continue;
        }

        // Else, increment ray index
        ray_ind++;

        copySolver = dynamic_cast<OsiClpSolverInterface*>(cutSolver->clone());
        copySolver->enableSimplexInterface(true);
        ClpSimplex* model = copySolver->getModelPtr();

        const int in_stat_old = model->getStatus(varIn);
        const int dirnIn =
          (in_stat_old == ClpSimplex::Status::atUpperBound) ? -1 : 1;
        const int return_code = pivot(copySolver, varIn, dirnIn, varOut,
            dirnOut, pivotRow, dist);

        if (return_code >= 0) {
          if (pivotRow >= 0) {
            if (varBasicInRow[pivotRow] != varOut) {
              error_msg(errorstring,
                  "While processing ray %d (var %d), variable basic in row %d is supposed to be %d but copySolver says it is %d.\n",
                  ray_ind, varIn, pivotRow, varBasicInRow[ray_ind],
                  varOut);
              writeErrorToLog(errorstring, GlobalVariables::log_file);
              exit(1);
            }
          } else {
            if (pivotRow != -2 && varIn != varOut) {
              error_msg(errorstring,
                  "While processing ray %d (var %d), pivot says nb variable is closest, so pivotRow should be -2 (is %d) and varIn (%d) == varIn (%d).\n",
                  ray_ind, varIn, pivotRow, varIn, varOut);
              writeErrorToLog(errorstring, GlobalVariables::log_file);
              exit(1);
            }
          }
          //double curr_min_sqnorm = std::inner_product(curr_sol, curr_sol + numCols,
          //    curr_sol, 0.0L);
          //if (curr_min_sqnorm < min_sqnorm) {
          //        min_index = ray_ind;
          //  min_sqnorm = curr_min_sqnorm;
          const int return_code = genCutsHelper(cuts, cutSolver,
              nonZeroColIndex, origSolver, origProbData, beta, cgsIndex,
              cgsName, structSICs, num_cuts_this_cgs, num_cuts_total, inNBSpace,
              cutHeur);
          if (return_code > 0) {
            num_cuts_generated += return_code;
          } else {
            if (copySolver) {
              delete copySolver;
            }
            return exitGenCutsFromCutSolver(cutSolver, return_code);
          }
          //}
        }
        
        if (copySolver) {
          delete copySolver;
        }
      }
    } /* end pivoting */
  } else if (cutSolver->isProvenDualInfeasible() && !inNBSpace) {
    // For the time being this seems to not be working well
    // For bell3a for instance, when cutting 64 rays, there is a cut that appears
    // both as a primal ray and as a feasible solution, so it has both >= 0 and >= -1
    // constant sides, and it causes infeasibility of the original problem when added
    return exitGenCutsFromCutSolver(cutSolver,
        -1 * (CutSolverFails::DUAL_CUTSOLVER_FAIL_IND + 1));

    // Resolve because with presolve turned on, there may be issues getting rays
    AdvCut currCut(false, 1);
    // We need to get primal rays
    // In case of dual infeasibility, there should be at least one
    std::vector<double*> primalRay(num_rays_to_get);
    for (int i = 0; i < num_rays_to_get; i++) {
      primalRay[i] = new double[cutSolver->getNumCols()];
    }
    primalRay = cutSolver->getPrimalRays(num_rays_to_get); // There may be more than one, which all lead to cuts

    const int scaleStatus = scalePrimalRay(cutSolver->getNumCols(), &primalRay[0]);

    if (scaleStatus >= 0) {
      double obj_val;
      if (inNBSpace) {
        AdvCut currCutNB(true, 1);
        currCutNB.cgsIndex = cgsIndex;
        currCutNB.cgsName = cgsName;
        currCutNB.splitVarIndex[0] = cgsIndex;
        currCutNB.setOsiRowCut(nonZeroColIndex, cutSolver->getNumCols(), primalRay[0], 0.0,
            origProbData.EPS); // This is an extreme ray, so rhs is 0
        obj_val = dotProduct(currCutNB.row(), cutSolver->getObjCoefficients());
        AdvCut::convertCutFromJSpaceToStructSpace(currCut, origSolver,
            origProbData.nonBasicVarIndex, currCutNB, true, origProbData.EPS);
      } else {
        obj_val = std::inner_product(origSolver->getObjCoefficients(),
            origSolver->getObjCoefficients()
                + origSolver->getNumCols(), primalRay[0], 0.0);
        currCut.cgsIndex = cgsIndex;
        currCut.cgsName = cgsName;
        currCut.splitVarIndex[0] = cgsIndex;
        currCut.setOsiRowCut(nonZeroColIndex, cutSolver->getNumCols(), primalRay[0], 0.0,
            origProbData.EPS); // This is an extreme ray, so rhs is 0
      }

      const bool cuttingSolutionFlag = lessThanVal(obj_val, 0.0);
      const bool duplicateSICFlag = structSICs.isDuplicateCut(currCut);
      const bool duplicateGICFlag = cuts.isDuplicateCut(currCut);
      const bool toAdd = cuttingSolutionFlag //&& !duplicateSICFlag
          && !duplicateGICFlag
          && !reachedCutLimit(num_cuts_this_cgs, num_cuts_total);
      if (toAdd) {
        currCut.cutHeur = cutHeur;
        GlobalVariables::numCutsFromHeur[cutHeur]++;
        cuts.addNonDuplicateCut(currCut, false);
        num_cuts_generated++;
        num_cuts_this_cgs++;
        num_cuts_total++;
      } 
      GlobalVariables::numCutSolverFails[CutSolverFails::NON_CUTTING_CUTSOLVER_FAIL_IND] +=
        !cuttingSolutionFlag;
      GlobalVariables::numCutSolverFails[CutSolverFails::DUPLICATE_SIC_CUTSOLVER_FAIL_IND] +=
        duplicateSICFlag;
      GlobalVariables::numCutSolverFails[CutSolverFails::DUPLICATE_GIC_CUTSOLVER_FAIL_IND] +=
        duplicateGICFlag;
      for (int i = 0; i < num_rays_to_get; i++) {
        delete[] primalRay[i];
      }
      primalRay.clear();
    } else {
      for (int i = 0; i < num_rays_to_get; i++) {
        delete[] primalRay[i];
      }
      primalRay.clear();
      return exitGenCutsFromCutSolver(cutSolver,
          -1 * (CutSolverFails::DUAL_CUTSOLVER_FAIL_IND + 1));
    }
//    primalRay.clear();
//    std::vector<double*>().swap(primalRay);
  } /* generate rays? (currently non-functional) */

  if (!cutSolver->isProvenOptimal() && !cutSolver->isProvenDualInfeasible()) {
    warning_msg(errorstring,
        "cutSolver is not proven optimal or dual infeasible for cgs %d/%d. Primal infeasible? %d.\n",
        cgsIndex + 1, param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND),
        cutSolver->isProvenPrimalInfeasible());
  }
  return exitGenCutsFromCutSolver(cutSolver, num_cuts_generated);
} /* genCutsFromCutSolver */

int genCutsHelper(AdvCuts & cuts,
    PointCutsSolverInterface* const cutSolver,
    const std::vector<int>& nonZeroColIndex,
    const PointCutsSolverInterface* const origSolver,
    const SolutionInfo & origProbData, const double beta, const int cgsIndex,
    const std::string& cgsName, const AdvCuts& structSICs,
    int& num_cuts_this_cgs, int& num_cuts_total, const bool inNBSpace,
    const CutHeuristics cutHeur) {
  // We might get here without cutSolver being optimal
  if (!cutSolver->isProvenOptimal()) {
    return 0; // errors are tabulated in exitGenCutsFromCutSolver
  } /* may have run into issues */
  if (reachedCutLimit(num_cuts_this_cgs, num_cuts_total)) {
//    GlobalVariables::numCutSolverFails[CutSolverFails::CUT_LIMIT_FAIL_IND]++;
//    return 0;
    return -1 * (CutSolverFails::CUT_LIMIT_FAIL_IND + 1);
  }

  int num_cuts_generated = 0;
  AdvCut currCut(false, 1);
  currCut.cgsIndex = cgsIndex;
  currCut.cgsName = cgsName;
  currCut.splitVarIndex[0] = cgsIndex;
  if (inNBSpace) {
    currCut.setCutFromJSpaceCoefficients(nonZeroColIndex,
        cutSolver->getColSolution(), beta, origSolver,
        origProbData.nonBasicVarIndex, true, origProbData.EPS);
  } else {
    currCut.setOsiRowCut(nonZeroColIndex, cutSolver->getNumCols(),
        cutSolver->getColSolution(), beta, origProbData.EPS);
  }

  // Clean
  const int clean_error = currCut.cleanCut(origSolver, origProbData, beta);
  if (clean_error != 0) {
//    GlobalVariables::numCutSolverFails[clean_error]++;
//    return 0;
    return clean_error;
  }

  // Ensure the cut is not a duplicate, and some other checks
  const bool generateAnywayIfDuplicateSIC = true; // in principle, when this is true, we do not need to check ortho with SICs; in our tests, we might anyway if we want to get the relevant stats
  const bool checkIfDuplicateSIC = false;
  const double currCutNorm = currCut.getTwoNorm();
  const double violation =
      //(param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND)) ?
      //    beta :
          currCut.violated(origSolver->getColSolution()); // beta normalization fixes the violation in nb-space
  int duplicateSICIndex = -1, duplicateGICIndex = -1;
  int minOrthogonalityIndex = -1;
  double orthogonalityWithSICs = 1., orthogonalityWithGICs = 1.;
  if (checkIfDuplicateSIC) {
    structSICs.howDuplicate(currCut, duplicateSICIndex, minOrthogonalityIndex,
        orthogonalityWithSICs, origProbData.EPS);
  }
  const bool duplicateSICFlag = (duplicateSICIndex >= 0);
  const bool orthogonalitySICFailFlag = !duplicateSICFlag
      && (orthogonalityWithSICs < param.getMINORTHOGONALITY());
  if (generateAnywayIfDuplicateSIC || (!duplicateSICFlag && !orthogonalitySICFailFlag)) {
    cuts.howDuplicate(currCut, duplicateGICIndex, minOrthogonalityIndex,
        orthogonalityWithGICs, origProbData.EPS);
  }
  const bool duplicateGICFlag = (duplicateGICIndex >= 0);
  bool orthogonalityGICFailFlag = !duplicateGICFlag
      && (orthogonalityWithGICs < param.getMINORTHOGONALITY());
  if (orthogonalityGICFailFlag) {
    // If rhs is better or sparser than existing cut, replace the cut
    const int cut_num_el =
        cuts.cuts[minOrthogonalityIndex].row().getNumElements();
//    const int* fpc_indices =
//        fpcs.cuts[minOrthogonalityIndex].row().getIndices();
//    const double* fpc_vals =
//        fpcs.cuts[minOrthogonalityIndex].row().getElements();
    const bool isSparser = currCut.row().getNumElements() < cut_num_el;
    const double old_effectiveness =
        cuts.cuts[minOrthogonalityIndex].effectiveness();
    const bool isMoreEffective = (violation / currCutNorm) > old_effectiveness;
//    double ratio = -1.;
//    int fpc_i = 0;
//    if (!isSparser) {
//      for (int i = 0; i < currCut.row().getNumElements(); i++) {
//        if (fpc_i >= fpc_num_el) {
//          break;
//        }
//        const int curr_ind = currCut.row().getIndices()[i];
//        int fpc_ind = fpc_indices[fpc_i];
//        if (curr_ind == fpc_ind) {
//          const double curr_val = currCut.row().getElements()[i];
//          const double fpc_val = fpc_vals[fpc_i];
//          if (!isZero(curr_val) && !isZero(fpc_val)) {
//            ratio = curr_val / fpc_val;
//            break;
//          }
//        } else {
//          while (curr_ind > fpc_ind) {
//            fpc_i++;
//            if (fpc_i < fpc_num_el) {
//              fpc_ind = fpc_indices[fpc_i];
//            } else {
//              break;
//            }
//          }
//          if (curr_ind == fpc_ind) {
//            const double curr_val = currCut.row().getElements()[i];
//            const double fpc_val = fpc_vals[fpc_i];
//            if (!isZero(curr_val) && !isZero(fpc_val)) {
//              ratio = curr_val / fpc_val;
//              break;
//            }
//          }
//        }
//      }
//      if (ratio < 0) {
//        error_msg(errorstr,
//            "Ratio needs to be positive, but found value of %f.\n", ratio);
//        writeErrorToII(errorstr, GlobalVariables::inst_info_out);
//        exit(1);
//      }
//    }
//    const bool rhsIsBetter = (ratio >= 0)
//        && greaterThanVal(currCut.rhs(),
//            ratio * fpcs.getCutPointer(minOrthogonalityIndex)->rhs());
    if (isSparser || isMoreEffective) {
      // Adjust statistics
      GlobalVariables::numCutsFromHeur[cuts.cuts[minOrthogonalityIndex].cutHeur]--;
      GlobalVariables::numCutsFromHeur[cutHeur]++;

      // Replace old cut
      currCut.cutHeur = cutHeur;
      currCut.setEffectiveness(violation / currCutNorm);
      cuts.cuts[minOrthogonalityIndex] = currCut;

      orthogonalityGICFailFlag = true;
    }
  } /* orthogonalityGICFail (replace cut possibly) */
  const bool toAdd = (generateAnywayIfDuplicateSIC || (!duplicateSICFlag && !orthogonalitySICFailFlag)) 
      && !duplicateGICFlag && !orthogonalityGICFailFlag;
  if (toAdd) {
    currCut.cutHeur = cutHeur;
    currCut.setEffectiveness(violation / currCutNorm);
    GlobalVariables::numCutsFromHeur[cutHeur]++;
    cuts.addNonDuplicateCut(currCut, false);
    num_cuts_generated++;
    num_cuts_this_cgs++;
    num_cuts_total++;
    GlobalVariables::numCutSolverFails[CutSolverFails::DUPLICATE_SIC_CUTSOLVER_FAIL_IND] +=
        duplicateSICFlag;
    GlobalVariables::numCutSolverFails[CutSolverFails::ORTHOGONALITY_SIC_CUTSOLVER_FAIL_IND] +=
        orthogonalitySICFailFlag;
    return num_cuts_generated;
  } else {
    // Note that these are all mutually exclusive IF we are checking for duplicates
    if (generateAnywayIfDuplicateSIC) {
      GlobalVariables::numCutSolverFails[CutSolverFails::DUPLICATE_SIC_CUTSOLVER_FAIL_IND] +=
          duplicateSICFlag;
      GlobalVariables::numCutSolverFails[CutSolverFails::ORTHOGONALITY_SIC_CUTSOLVER_FAIL_IND] +=
          orthogonalitySICFailFlag;
    } else if (duplicateSICFlag) {
      return -1 * (CutSolverFails::DUPLICATE_SIC_CUTSOLVER_FAIL_IND + 1);
    } else if (orthogonalitySICFailFlag) {
      return -1 * (CutSolverFails::ORTHOGONALITY_SIC_CUTSOLVER_FAIL_IND + 1);
    }
    if (duplicateGICFlag) {
      return -1 * (CutSolverFails::DUPLICATE_GIC_CUTSOLVER_FAIL_IND + 1);
    }
    if (orthogonalityGICFailFlag) {
      return -1 * (CutSolverFails::ORTHOGONALITY_GIC_CUTSOLVER_FAIL_IND + 1);
    }
  }
  return num_cuts_generated;
} /* genCutsHelper */

/**
 * Uses a cut as a row and generates a Gomory cut from it,
 * by using the ``most fractional'' integer variable.
 * That is, say ax \ge b is the row given.
 * Divide by a1 to get
 *   x1 = b/a1 - \sum_j aj/a0 xj
 * The GMI cut from this row is
 *
 */
/*
void generateGomoryCutFromCut(const std::vector<double>& oldCutCoeff,
    const double oldRhs, AdvCut& newCut) {

}*/ /* generateGomoryCutFromCut */

/**
 * Returns error status or num_cuts_generated
 */
int exitGenCutsFromCutSolver(PointCutsSolverInterface* const cutSolver,
    const int num_cuts_generated) {
  GlobalVariables::timeStats.end_timer(GlobalConstants::SOLVE_PRLP_TIME);
  int return_status = num_cuts_generated;
  if (return_status >= 0 && !cutSolver->isProvenOptimal()) {
    // Sometimes it simply takes too long to solve optimally
    if (cutSolver->isIterationLimitReached()) {
      return_status = -1 * (CutSolverFails::ITERATION_CUTSOLVER_FAIL_IND + 1);
    }
    // The next will be true if either # iterations or time limit reached
    else if (cutSolver->getModelPtr()->hitMaximumIterations()) {
      return_status = -1 * (CutSolverFails::TIMELIMIT_CUTSOLVER_FAIL_IND + 1);
    }
    // Numerical difficulties may have been encountered
    else if (cutSolver->isAbandoned()) {
      return exitGenCutsFromCutSolver(cutSolver,
          -1 * (CutSolverFails::ABANDONED_CUTSOLVER_FAIL_IND + 1));
    }
    // The problem may be unbounded
    else if (cutSolver->isProvenDualInfeasible()) {
      return_status = -1 * (CutSolverFails::DUAL_CUTSOLVER_FAIL_IND + 1);
    } 
    // We may get an infeasible problem
    else if (cutSolver->isProvenPrimalInfeasible()) {
      return_status = -1 * (CutSolverFails::PRIMAL_CUTSOLVER_FAIL_IND + 1);
    } 
    // Something super strange may have happened
    else {
      return_status = -1 * (CutSolverFails::UNSPECIFIED_CUTSOLVER_FAIL_IND + 1);
    }

    // Make sure we did not generate cuts
    if (num_cuts_generated  > 0) {
      error_msg(errorstring, "Somehow we generated %d cuts from a non-optimal cutSolver; not possible. Error: %d.\n", num_cuts_generated, -1 * (return_status + 1));
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }
  } /* may have run into issues */

  if (return_status < 0) {
    const int error_index = -1 * (return_status + 1);
    GlobalVariables::numCutSolverFails[error_index]++;
  }

  // Reset timer and number iterations
  cutSolver->getModelPtr()->setNumberIterations(0);
  cutSolver->getModelPtr()->setMaximumSeconds(param.getCUTSOLVER_TIMELIMIT());
  return return_status;
} /* exitGenCutsFromCutSolver */

/**********************************************************/
/**
 * Pivot to neighbor of solution with highest Euclidean distance
 * Corresponds to lower 2-norm
 */
void pivotToNbrHigherEuclDist(OsiClpSolverInterface* cutSolver,
    std::vector<double>& best_sol) {
  if (!cutSolver->isProvenOptimal()) {
    return;
  }

  // Current solution's 2-norm
  const int numCols = cutSolver->getNumCols();
  const int numRows = cutSolver->getNumRows();
  const double* sol = cutSolver->getColSolution();
  double min_sqnorm = std::inner_product(sol, sol + numCols, sol, 0.0L);
//  int min_index = -1;

  // Set up vectors
  std::vector<int> varOut(numCols, -1);
  std::vector<int> dirnOut(numCols, 0);
  std::vector<int> pivotRow(numCols, -1);
  std::vector<double> dist(numCols, -1);
  std::vector<int> varBasicInRow(numRows);
  cutSolver->enableFactorization();
  cutSolver->getBasics(&varBasicInRow[0]);
  cutSolver->disableFactorization();

  // For each non-basic variable, try to pivot along the ray
  OsiClpSolverInterface* copySolver;
  int ray_ind = -1;
  for (int varIn = 0; varIn < numCols + numRows; varIn++) {
    // Check if it is non-basic
    if (!isRayVar(cutSolver, varIn)) {
      continue;
    }

    // Else, increment ray index
    ray_ind++;

    copySolver = dynamic_cast<OsiClpSolverInterface*>(cutSolver->clone());
    copySolver->enableSimplexInterface(true);
    ClpSimplex* model = copySolver->getModelPtr();

//    const int varIn_row =
//        (varIn < copySolver->getNumCols()) ?
//            -1 : varIn - copySolver->getNumCols();
    const int in_stat_old = model->getStatus(varIn);
    const int dirnIn =
        (in_stat_old == ClpSimplex::Status::atUpperBound) ? -1 : 1;
//    const int return_code = copySolver->primalPivotResult(varIn, dirnIn,
//        varOut[ray_ind], dirnOut[ray_ind], dist[ray_ind],
//        NULL);
    const int return_code = pivot(copySolver, varIn, dirnIn, varOut[ray_ind],
        dirnOut[ray_ind], pivotRow[ray_ind], dist[ray_ind]);

    if (return_code >= 0) {
      if (pivotRow[ray_ind] >= 0) {
        if (varBasicInRow[pivotRow[ray_ind]] != varOut[ray_ind]) {
          error_msg(errorstring,
              "While processing ray %d (var %d), variable basic in row %d is supposed to be %d but copySolver says it is %d.\n",
              ray_ind, varIn, pivotRow[ray_ind], varBasicInRow[ray_ind],
              varOut[ray_ind]);
          writeErrorToLog(errorstring, GlobalVariables::log_file);
          exit(1);
        }
      } else {
        if (pivotRow[ray_ind] != -2 && varIn != varOut[ray_ind]) {
          error_msg(errorstring,
              "While processing ray %d (var %d), pivot says nb variable is closest, so pivotRow should be -2 (is %d) and varIn (%d) == varIn (%d).\n",
              ray_ind, varIn, pivotRow[ray_ind], varIn, varOut[ray_ind]);
          writeErrorToLog(errorstring, GlobalVariables::log_file);
          exit(1);
        }
      }
      const double* curr_sol = copySolver->getColSolution();
      double curr_min_sqnorm = std::inner_product(curr_sol, curr_sol + numCols,
          curr_sol, 0.0L);
      if (curr_min_sqnorm < min_sqnorm) {
//        min_index = ray_ind;
        min_sqnorm = curr_min_sqnorm;
        best_sol.assign(curr_sol, curr_sol + numCols);
      }
    }
  }
} /* pivotToNbrHigherEuclDist */

/***********************************************************************/
/***********************************************************************/
/** Cut heuristics **/
/***********************************************************************/
/***********************************************************************/

struct rowAndActivity {
    int row;
    double activity;
};

struct by_activity_desc {
    bool operator()(rowAndActivity const &a, rowAndActivity const &b) {
      return a.activity > b.activity;
    }
};

struct by_activity_asc {
    bool operator()(rowAndActivity const &a, rowAndActivity const &b) {
      return a.activity < b.activity;
    }
};

struct compareRayInfo {
    int index;
    double min_ortho;
    double avg_ortho;

    bool operator<(const compareRayInfo& rhs) const {
//      return this->min_ortho + this->avg_ortho > rhs.min_ortho + rhs.avg_ortho;
      return (this->min_ortho > rhs.min_ortho
          || (isVal(this->min_ortho, rhs.min_ortho, 0.1)
              && this->avg_ortho > rhs.avg_ortho));
    }
}; /* compareRayInfo struct */

/**
 * Collect all the points and rays and sort them by the objective
 */
void setupForTargetedCutGeneration(std::vector<rowAndActivity>& pointIndex,
    std::vector<rowAndActivity>& rayIndex,
//    std::set<compareRayInfo>& sortedRays,
    const std::vector<double>& ortho,
    //const AdvCut* const objCut,
    const PointCutsSolverInterface* const cutSolver) {
  pointIndex.clear();
  rayIndex.clear();
  for (int row_ind = 0; row_ind < cutSolver->getNumRows(); row_ind++) {
    if (isZero(cutSolver->getRightHandSide()[row_ind])) {
      rowAndActivity tmp;
      tmp.row = row_ind;
      tmp.activity = ortho[row_ind];
//      sortedRays.insert(compareRayInfo { (int) rayIndex.size(), 1., 0. });
      rayIndex.push_back(tmp);
    } else {
      rowAndActivity tmp;
      tmp.row = row_ind;
      tmp.activity = ortho[row_ind];
      pointIndex.push_back(tmp);
    }
  }

  // Moved the sorting into the setupCutSolver code, i.e., the points and rays are presorted
//  std::sort(pointIndex.begin(), pointIndex.end(), by_activity_desc());
//  std::sort(rayIndex.begin(), rayIndex.end(), by_activity_asc());

//  for (int i = 0; i < pointIndex.size(); i++) {
//    printf("(%d, %f), ", pointIndex[i].row, pointIndex[i].activity);
//  }
//  printf("\n");
//  for (int i = 0; i < rayIndex.size(); i++) {
//    printf("(%d, %f), ", rayIndex[i].row, rayIndex[i].activity);
//  }
//  printf("\n");
//  exit(1);
} /* setupForTargetedCutGeneration */

/**
 * After a cut has been generated, mark all the tight rows
// * For the rays, combine them into one and add it to compareRays
 * In addition, update the set of rays to be considered
 */
int updateStepForTargetedCutGeneration(std::vector<int>& numTimesTightRow,
    std::vector<int>& numTimesTightColLB, std::vector<int>& numTimesTightColUB,
//    std::vector<int>& nonZeroColIndex,
//    std::vector<rowAndActivity>& pointIndex,
//    std::vector<rowAndActivity>& rayIndex,
    PointCutsSolverInterface* const cutSolver) {
  if (!cutSolver->isProvenOptimal()) {
    return 0;
  }

  int num_changed = 0;
//  double find_max = -1.; // change to +1/-1 depending on what you wish to try
//  double best_reduced_cost = (find_max > 0) ? 0. : std::numeric_limits<double>::max();
//  int row_with_best_reduced_cost = -1;
  // Check each row
  for (int row_ind = 0; row_ind < cutSolver->getNumRows(); row_ind++) {
    const double activity = cutSolver->getRowActivity()[row_ind];
    const double rhs = cutSolver->getRightHandSide()[row_ind];
    if (isVal(activity, rhs)) {
//    if (!isBasicSlack(cutSolver, row_ind)) {
      if (numTimesTightRow[row_ind] == 0) {
        num_changed++;
      }
      if (numTimesTightRow[row_ind] >= 0) {
        numTimesTightRow[row_ind]++;
      } else {
        numTimesTightRow[row_ind]--;
      }
//      if (isZero(rhs) && updateObjective) { // update, but only for the rays, to not negate the strong branching lb point
//        const double reduced_cost = std::abs(cutSolver->getRowPrice()[row_ind]);
//        if (find_max * reduced_cost > find_max * best_reduced_cost) {
//          best_reduced_cost = reduced_cost;
//          row_with_best_reduced_cost = row_ind;
//        }
//      }
    }
  } /* check rows */

  // Also process the axis directions
  for (int col_ind = 0; col_ind < cutSolver->getNumCols(); col_ind++) {
//    const int col_ind = nonZeroColIndex[ind];
    const double val = cutSolver->getColSolution()[col_ind];
    const double lb = cutSolver->getColLower()[col_ind];
    const double ub = cutSolver->getColUpper()[col_ind];
    if (!isNegInfinity(lb) && isVal(val, lb)) {
      if (numTimesTightColLB[col_ind] == 0) {
        num_changed++;
      }
      if (numTimesTightColLB[col_ind] >= 0) {
        numTimesTightColLB[col_ind]++;
      } else {
        numTimesTightColLB[col_ind]--;
      }
    }
    if (!isInfinity(ub) && isVal(val, ub)) {
      if (numTimesTightColUB[col_ind] == 0) {
        num_changed++;
      }
      if (numTimesTightColUB[col_ind] >= 0) {
        numTimesTightColUB[col_ind]++;
      } else {
        numTimesTightColUB[col_ind]--;
      }
    }
//    else if (isVal(cutSolver->getColSolution()[j], 0.)) {
//      // I think we should skip all such columns
//      // They may either be unbounded in that direction,
//      // or they are already optimal at 0
//      if (numTimesTight[ind] >= 0) {
//        numTimesTight[ind]++;
//        numTimesTight[ind] *= -1;
//      } else {
//        numTimesTight[ind]--;
//      }
//    }
  } /* check columns */

  // The previous objective was alpha * p (say)
  // We have alpha * q = b_q, where q is the row (point or ray) with max reduced cost
  // If we instead optimize alpha * (p - q),
  // then if there exists alpha' with alpha' * q > b_q,
  // alpha will no longer be optimal
//  if (row_with_best_reduced_cost >= 0) {
//    const CoinShallowPackedVector vec = cutSolver->getMatrixByRow()->getVector(row_with_best_reduced_cost);
//    addToObjectiveFromPackedVector(cutSolver, &vec, false, -1.);
//  }
//  for (int point_ind = 0; point_ind < (int) pointIndex.size(); point_ind++) {
//    const int row_ind = pointIndex[point_ind].row;
////        (pointIndex[point_ind].row < 0) ?
////            (-1 * (pointIndex[point_ind].row + 1)) : pointIndex[point_ind].row;
//
//    if (!isBasicSlack(cutSolver, row_ind)) {
////      if (pointIndex[point_ind].row >= 0) {
////        pointIndex[point_ind].row = -1 * (row_ind + 1);
//      if (numTimesTight[row_ind] == 0) {
//        num_changed++;
//      }
//      numTimesTight[row_ind]++;
//    }
//  } /* process points */
//
//  for (int ray_ind = 0; ray_ind < (int) rayIndex.size(); ray_ind++) {
//    const int row_ind = rayIndex[ray_ind].row;
////        (rayIndex[ray_ind].row < 0) ?
////            (-1 * (rayIndex[ray_ind].row + 1)) : rayIndex[ray_ind].row;
//
//    if (!isBasicSlack(cutSolver, row_ind)) {
////      if (rayIndex[ray_ind].row >= 0) {
////        rayIndex[ray_ind].row = -1 * (row_ind + 1);
//      if (numTimesTight[row_ind] == 0) {
//        num_changed++;
//      }
//      numTimesTight[row_ind]++;
//    }
//  } /* process rays */

  return num_changed;
} /* updateStepForTargetedCutGeneration */

/**
 * After a new cut, we need to update the sorted list of rays that we are going to use
 * We need to remove all the rays that are now tight and we need to update the orthogonalities
 * with respect to the new ray (we calculate with respect to the cut norm actually)
 */
void updateRaySetForTargetedCutGeneration(std::set<compareRayInfo>& sortedRays,
    const std::vector<rowAndActivity>& rayIndex, const int init_num_cuts, int& num_old_cuts,
    const AdvCuts& cuts, const PointCutsSolverInterface* const cutSolver) {
  const int num_cuts = cuts.size();
//  const int num_new_cuts = cuts.size() - num_old_cuts;
//  const int num_cols = cutSolver->getNumCols();
//  const double* newOrthogonalRay = cutSolver->getColSolution();

  std::set<compareRayInfo> newSortedRays;
  for (compareRayInfo curr_ray_info : sortedRays) {
    const int ray_ind = curr_ray_info.index;
    const int row_ind = rayIndex[ray_ind].row;

    // If this ray has a negative row, we skip it
    if (row_ind < 0) {
      continue;  // not a candidate; already has been tight
    }

//    double this_min_ortho = 1.;
    double this_total_ortho = 0.;
    const CoinShallowPackedVector vec = cutSolver->getMatrixByRow()->getVector(row_ind);
    for (int cut_ind = num_old_cuts; cut_ind < num_cuts; cut_ind++) {
      const CoinPackedVector newOrthogonalRay = cuts[cut_ind].row();

      // The norm to the cut is orthogonal to the rays we would actually like to be orthogonal to
      // In other words, we would like to be parallel to the cut norm
      const double this_ortho = std::abs(getParallelism(vec, newOrthogonalRay));

      if (this_ortho < curr_ray_info.min_ortho) {
        curr_ray_info.min_ortho = this_ortho;
      }
      this_total_ortho += this_ortho;
    }
    // We redo the average
    // (We use num_old_cuts + 1 not num_old_cuts because we initialized with ortho wrt objective)
    curr_ray_info.avg_ortho =
        (curr_ray_info.avg_ortho
        * (num_old_cuts - init_num_cuts + 1) + this_total_ortho)
        / (num_cuts - init_num_cuts + 1);
    newSortedRays.insert(curr_ray_info);
  } /* update the sorted rays set */
  sortedRays.swap(newSortedRays);
  num_old_cuts = num_cuts;

//  for (compareRayInfo curr_ray_info : sortedRays) {
//    printf("(%d, %1.3f, %1.3f)\n", curr_ray_info.index, curr_ray_info.min_ortho, curr_ray_info.avg_ortho);
//  }
//  printf("\n");
//  exit(1);
} /* updateRaySetForTargetedCutGeneration */

////std::vector<int>&
//void chooseRayForTargetedCutGeneration(
//    std::set<compareRayInfo>& sortedRays,
//    const std::vector<rowAndActivity>& rayIndex,
//    const std::vector<CoinPackedVector>& compareRays,
//    const PointCutsSolverInterface* const cutSolver) {
//  double best_min_orthogonality = 0., best_avg_orthogonality = 0.;
//
//  for (int ray_ind = 0; ray_ind < (int) rayIndex.size(); ray_ind++) {
//    const int row_ind = rayIndex[ray_ind].row;
//    if (row_ind < 0) {
//      continue;  // not a candidate; already has been tight
//    }
//    double this_min_orthogonality = 0., this_avg_orthogonality = 0.;
//    for (int compare_ind = 0; compare_ind < (int) compareRays.size(); compare_ind++) {
//      const double this_ortho = getOrthogonality(
//          cutSolver->getMatrixByRow()->getVector(row_ind),
//          compareRays[compare_ind]);
//
//      if (this_ortho < this_min_orthogonality) {
//        this_min_orthogonality = this_ortho;
//      }
//      this_avg_orthogonality += this_ortho;
//    }
//    this_avg_orthogonality /= compareRays.size();
//
//    if (this_min_orthogonality > best_min_orthogonality
//        || (isVal(this_min_orthogonality, best_min_orthogonality, 0.1)
//            && this_avg_orthogonality > best_avg_orthogonality)) {
//      return_index = ray_ind;
//      best_min_orthogonality = this_min_orthogonality;
//      best_avg_orthogonality = this_avg_orthogonality;
//    }
//  }
//} /* chooseRayForTargetedCutGeneration */

int tryOneObjective(int& num_failures, std::vector<int>& numTimesTightRow,
    std::vector<int>& numTimesTightColLB, std::vector<int>& numTimesTightColUB,
    const std::vector<int>& nonZeroColIndex, const CutHeuristics& cutHeur,
    PointCutsSolverInterface* const cutSolver, const double beta, AdvCuts& cuts,
    int& num_cuts_this_cgs, int& num_cuts_total, int& num_obj_tried,
    const PointCutsSolverInterface* const origSolver,
    const SolutionInfo & probData, const int cgsIndex,
    const std::string& cgsName, const AdvCuts& structSICs, const bool inNBSpace,
    const bool tryExtraHard = false) {
  num_obj_tried++;
  int return_code = genCutsFromCutSolver(cuts, cutSolver, nonZeroColIndex,
      origSolver, probData, beta, cgsIndex, cgsName, structSICs,
      num_cuts_this_cgs, num_cuts_total, inNBSpace, cutHeur, tryExtraHard);
//  if (return_code > 0) {
  if (cutSolver->isProvenOptimal()) {
    updateStepForTargetedCutGeneration(numTimesTightRow, numTimesTightColLB,
        numTimesTightColUB, cutSolver);
  }
  if (return_code <= 0){
    num_failures++;
  }
  return return_code;
} /* tryOneObjective */

/**
 * Try getting cuts tight on the branching lb
 * Returns 0 if error, 1 if no error
 */
int findCutsTightOnPoint(int& num_failures, std::vector<int>& numTimesTightRow,
    std::vector<int>& numTimesTightColLB, std::vector<int>& numTimesTightColUB,
    const std::vector<int>& nonZeroColIndex,
    const int point_row_ind, const int init_num_cuts,
    const CutHeuristics& cutHeur, PointCutsSolverInterface* const cutSolver,
    const double beta, AdvCuts& cuts, int& num_cuts_this_cgs,
    int& num_cuts_total, int& num_obj_tried,
    const PointCutsSolverInterface* const origSolver,
    const SolutionInfo & probData, const std::vector<double>& ortho,
    const int cgsIndex, const std::string& cgsName, const AdvCuts& structSICs,
    const Stats& timeStats, const std::string& timeName, const bool inNBSpace,
    const int MAX_NUM_OBJ_PER_POINT) {
  const int num_rows = cutSolver->getNumRows();
  const int num_cols = cutSolver->getNumCols();
  int num_cuts_per_point = 0;
  int check_ind = -1; //strong_lb_row_ind; // row that we last _subtracted_ from the objective
  const int BAD_RETURN_CODE = -1 * (CutSolverFails::PRIMAL_CUTSOLVER_FAIL_IND + 1);
  int ret_val = 1;

#ifdef TRACE_CUT_BOUND
  // Add a debug solver in order to track the bound after adding the cuts we have generated
  PointCutsSolverInterface* tmpSolver =
      dynamic_cast<PointCutsSolverInterface*>(origSolver->clone());
  AdvCuts tmpCuts = cuts;
  tmpCuts.setOsiCuts();
  applyCuts(tmpSolver, tmpCuts, 0, tmpCuts.size(), origSolver->getObjValue());
#endif

  const CoinPackedMatrix* mat = cutSolver->getMatrixByRow();
  const CoinShallowPackedVector orig_point = mat->getVector(point_row_ind);
  CoinPackedVector* vec = NULL; // ray that will be added/subtracted
  addToObjectiveFromPackedVector(cutSolver, &orig_point, false);

  int tmp_return_code = tryOneObjective(num_failures,
      numTimesTightRow, numTimesTightColLB, numTimesTightColUB, nonZeroColIndex, cutHeur,
      cutSolver, beta, cuts, num_cuts_this_cgs, num_cuts_total, num_obj_tried,
      origSolver, probData, cgsIndex, cgsName, structSICs, inNBSpace, true);
  if (tmp_return_code > 0) {
    num_cuts_per_point += tmp_return_code;
#ifdef TRACE_CUT_BOUND
    for (int add_ind = 0; add_ind < tmp_return_code; add_ind++) {
      tmpCuts.insert(cuts.cuts[tmpCuts.sizeCuts() + add_ind]);
    }
    applyCuts(tmpSolver, tmpCuts, tmpCuts.sizeCuts() - tmp_return_code,
        tmp_return_code, tmpSolver->getObjValue());
#endif
  }
  setConstantObjectiveFromPackedVector(cutSolver, 0., orig_point.getNumElements(), orig_point.getIndices());

  // There better exist a cut that is tight on the point, or we give up
  if (!cutSolver->isProvenOptimal()
      || !isVal(cutSolver->getRowActivity()[point_row_ind],
          cutSolver->getRightHandSide()[point_row_ind])
      || MAX_NUM_OBJ_PER_POINT <= 1) {
//    if (vec) {
//      delete vec;
//    }
#ifdef TRACE_CUT_BOUND
    if (tmpSolver) {
      delete tmpSolver;
    }
#endif
    return 1;
  }

  // Setup; there are different options for how to choose objectives here
  const int mode_param = param.getParamVal(ParamIndices::MODE_OBJ_PER_POINT_PARAM_IND);
  // 0 = all of the rows that are not tight, and subtract as they get tight
  // 1 = one point/ray at time
  // 2 = keep trying even if the point/ray has become tight in the process?
  // 0 is more expensive at each step and not clear that it would be better
  const int mode_ones = mode_param % 10;
  // 0x: only rays
  // 1x: points+rays
  // 2x: points+rays+variables
  const int mode_param_tens = (mode_param % 100 - (mode_param % 10)) / 10;
  // 0xx: small to large angle with obj (ascending)
  // 1xx: large to small angle with obj (descending)
  // 2xx: small to large slack (ascending)
  // 3xx: large to small slack (descending)
  const int mode_param_hundreds = (mode_param % 1000 - (mode_param % 100)) / 100;
  const bool usePoints = mode_param_tens > 0; // use points as objectives
  const bool useVariables = mode_param_tens > 1; // use variables as objectives
  const int shouldSort = (mode_ones > 0) * mode_param_hundreds; // sort by row activity (no need for mode 0)

  // Trying to find (more) cuts tight on the given point, so set the row to equality temporarily
  const double orig_lb = cutSolver->getRowLower()[point_row_ind];
  const double orig_ub = cutSolver->getRowUpper()[point_row_ind];
  cutSolver->setRowUpper(point_row_ind, orig_lb);

  // Set up vectors to be used in setting the objective
  std::vector<int> indexToCheck; // index order is first rows, then col lb, then col ub
  std::vector<double> sortCriterion;
  std::vector<int> savedNumTimesTightRow(numTimesTightRow);
  std::vector<int> savedNumTimesTightColLB(numTimesTightColLB);
  std::vector<int> savedNumTimesTightColUB(numTimesTightColUB);
//  for (int i = 0; i < numToCheckFull; i++) {
//    const int var = (i < num_rows) ? i : i - num_rows;
//    const double curr_activity =
//        (i < num_rows) ?
//            cutSolver->getRowActivity()[var] : cutSolver->getColSolution()[var];
//    double curr_rhs = 0.;
//    if (i < num_rows) {
//      curr_rhs = cutSolver->getRightHandSide()[var];
//      if (!usePoints && !isZero(curr_rhs)) {
//        continue; // skip points if desired
//      }
//    } else {
//      if (!isNegInfinity(cutSolver->getColLower()[var])) {
//        curr_rhs = cutSolver->getColLower()[var];
//      } else if (!isInfinity(cutSolver->getColUpper()[var])) {
//        curr_rhs = cutSolver->getColUpper()[var];
//      } else {
//        continue; // free variable
//      }
//    }
//    if (!isZero(curr_activity - curr_rhs)) {
//      rowIndexToCheck.push_back(i);
//      slack.push_back(curr_activity - curr_rhs);
//    }
//  } /* set up slack and rhs vectors */

  // Set up vectors to be used in setting the objective
//  std::vector<int> rowIndexToCheck2;
//  std::vector<double> slack2;
  cutSolver->enableFactorization(); // note that this changes things slightly sometimes
  std::vector<int> varBasicInRow(num_rows);
  cutSolver->getBasics(varBasicInRow.data());
  for (int row_ind = 0; row_ind < num_rows; row_ind++) {
    const int var = varBasicInRow[row_ind];
    double curr_activity = 0., curr_rhs = 0.;
    if (var < num_cols) {
      if (!useVariables) {
        continue; // skip columns if desired
      }
      const double lb = cutSolver->getColLower()[var];
      const double ub = cutSolver->getColUpper()[var];
      curr_activity = cutSolver->getColSolution()[var];
      if (!isNegInfinity(lb) && numTimesTightColLB[var] >= 0) {
        curr_rhs = lb;
        if (!isZero(curr_activity - curr_rhs)) {
          indexToCheck.push_back(var + num_rows);
          if (shouldSort == 0 || shouldSort == 1) {
            sortCriterion.push_back(
                param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND) ?
                    probData.nonBasicReducedCost[var] :
                    std::abs(origSolver->getReducedCost()[var]));
          } else if (shouldSort == 2 || shouldSort == 3) {
            sortCriterion.push_back(curr_activity - curr_rhs);
          }
        }
      }
      if (!isInfinity(ub) && numTimesTightColUB[var] >= 0) {
        curr_rhs = ub;
        if (!isZero(curr_activity - curr_rhs)) {
          indexToCheck.push_back(var + num_rows + num_cols);
          if (shouldSort == 0 || shouldSort == 1) {
            sortCriterion.push_back(
                param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND) ?
                    probData.nonBasicReducedCost[var] :
                    std::abs(origSolver->getReducedCost()[var]));
          } else if (shouldSort == 2 || shouldSort == 3) {
            sortCriterion.push_back(curr_rhs - curr_activity); // stay positive
          }
        }
      }
    } else {
      const int row = var - num_cols; // should actually be same as row_ind, but just in case...
      curr_rhs = cutSolver->getRightHandSide()[row];
      if (!usePoints && !isZero(curr_rhs) && numTimesTightRow[row] >= 0) {
        continue; // skip points if desired
      }
      curr_activity = cutSolver->getRowActivity()[row];
      if (!isZero(curr_activity - curr_rhs)) {
        indexToCheck.push_back(row);
        if (shouldSort == 0 || shouldSort == 1) {
          if (isZero(curr_rhs)) {
            sortCriterion.push_back(ortho[row]); // already normalized
          } else {
            // ortho[row] has c * p2 (current point)
            // We wish to add c * (p2 - p1) / norm(p2 - p1)
            if (vec) {
              delete vec;
            }
            vec = new CoinPackedVector(mat->getVectorSize(row),
                mat->getIndices() + mat->getVectorFirst(row),
                mat->getElements() + mat->getVectorFirst(row), false);
            CoinPackedVector tmpVec;
            packedSortedVectorSum(tmpVec, 1., (*vec), -1., orig_point, 1e-20);
            sortCriterion.push_back(
                (ortho[row] - ortho[point_row_ind]) / tmpVec.twoNorm());
          }
        } else if (shouldSort == 2 || shouldSort == 3) {
          sortCriterion.push_back((curr_activity - curr_rhs) / getRowTwoNorm(row, mat));
        }
      }
    }
  } /* set up slack and rhs vectors */
  cutSolver->disableFactorization();

  // The real # to check
  const int numToCheck = indexToCheck.size();

  // Sort by descending/ascending activity, if requested
  std::vector<int> sortIndex(numToCheck);
  for (int i = 0; i < numToCheck; i++) {
    sortIndex[i] = i;
  }
  if (shouldSort == 0 || shouldSort == 2) {
    std::sort(sortIndex.begin(), sortIndex.end(),
        index_cmp_asc<const std::vector<double>&>(sortCriterion)); // ascending
  } else if (shouldSort == 1 || shouldSort == 3) {
    std::sort(sortIndex.begin(), sortIndex.end(),
        index_cmp_dsc<const std::vector<double>&>(sortCriterion)); // descending
  }

  // Prepare first new objective
  std::vector<int> indexAddedToObjective;
  while (check_ind < numToCheck - 1) {
    check_ind++;
    const int curr_index = indexToCheck[sortIndex[check_ind]];
    if (vec) {
      delete vec;
      vec = NULL;
    }
    if (curr_index < num_rows) {
//      if (numTimesTightRow[curr_index] != 0) {
//        continue;
//      }
      vec = new CoinPackedVector(mat->getVectorSize(curr_index),
          mat->getIndices() + mat->getVectorFirst(curr_index),
          mat->getElements() + mat->getVectorFirst(curr_index), false);
    } else if (curr_index < num_rows + num_cols) {
      const int col = curr_index - num_rows;
//      if (numTimesTightColLB[col] != 0) {
//        continue;
//      }
      const int index[1] = { col };
      const double value[1] = { 1. };
      vec = new CoinPackedVector(1, index, value);
    } else {
      const int col = curr_index - num_rows - num_cols;
//      if (numTimesTightColUB[col] != 0) {
//        continue;
//      }
      const int index[1] = { col };
      const double value[1] = { -1. };
      vec = new CoinPackedVector(1, index, value);
    }
    addToObjectiveFromPackedVector(cutSolver, vec, false, 1.);
    if (mode_ones > 0) {
      break;
    } else {
      indexAddedToObjective.push_back(check_ind);
    }
  } /* prepare first new objective */

  if ((mode_ones > 0 && check_ind >= numToCheck - 1)
      || (mode_ones == 0 && indexAddedToObjective.empty())) {
    setConstantObjectiveFromPackedVector(cutSolver, 0.);
    cutSolver->setRowUpper(point_row_ind, orig_ub);
    if (vec) {
      delete vec;
    }
#ifdef TRACE_CUT_BOUND
    if (tmpSolver) {
      delete tmpSolver;
    }
#endif
    return 1;
  }

  for (int try_ind = 1; try_ind < MAX_NUM_OBJ_PER_POINT; try_ind++) {
    if (tmp_return_code == BAD_RETURN_CODE
        || reachedCutLimit(num_cuts_this_cgs, num_cuts_total)
        || reachedTimeLimit(timeStats, timeName, param.getTIMELIMIT())
        || reachedFailureLimit(cuts.size() - init_num_cuts, num_failures,
            GlobalVariables::timeStats.get_total_time(GlobalConstants::SOLVE_PRLP_TIME))) {
      ret_val = 0;
      break;
    }
    if (mode_ones > 0 && check_ind >= numToCheck - 1) {
      break;
    }
    tmp_return_code = tryOneObjective(num_failures, numTimesTightRow,
        numTimesTightColLB, numTimesTightColUB, nonZeroColIndex, cutHeur, cutSolver, beta, cuts,
        num_cuts_this_cgs, num_cuts_total, num_obj_tried, origSolver, probData,
        cgsIndex, cgsName, structSICs, inNBSpace);
    if (tmp_return_code > 0) {
      num_cuts_per_point += tmp_return_code;
#ifdef TRACE_CUT_BOUND
      for (int add_ind = 0; add_ind < tmp_return_code; add_ind++) {
        tmpCuts.insert(cuts.cuts[tmpCuts.sizeCuts() + add_ind]);
      }
      applyCuts(tmpSolver, tmpCuts, tmpCuts.sizeCuts() - tmp_return_code,
          tmp_return_code, tmpSolver->getObjValue());
#endif
    }
    if (try_ind + 1 < MAX_NUM_OBJ_PER_POINT) { //&& cutSolver->isProvenOptimal()) {
      //    for (int row_ind = 0; row_ind < cutSolver->getNumRows(); row_ind++) {
//      if (return_code == -1 * (CutSolverFails::DUAL_CUTSOLVER_FAIL_IND + 1)) {
        // Last vector caused dual infeasibility, so scrap it
      if (mode_ones > 0) {
        addToObjectiveFromPackedVector(cutSolver, vec, false, -1.);
        while (check_ind < numToCheck - 1) {
          check_ind++;
          const int curr_index = indexToCheck[sortIndex[check_ind]];
          if (vec) {
            delete vec;
            vec = NULL;
          }
          if (curr_index < num_rows) {
            if ((mode_ones == 1)
                && (numTimesTightRow[curr_index]
                    != savedNumTimesTightRow[curr_index])) {
              continue;
            }
            vec = new CoinPackedVector(mat->getVectorSize(curr_index),
                mat->getIndices() + mat->getVectorFirst(curr_index),
                mat->getElements() + mat->getVectorFirst(curr_index), false);
          } else if (curr_index < num_rows + num_cols) {
            const int col = curr_index - num_rows;
            if ((mode_ones == 1)
                && (numTimesTightColLB[col] != savedNumTimesTightColLB[col])) {
              continue;
            }
            const int index[1] = { col };
            const double value[1] = { 1. };
            vec = new CoinPackedVector(1, index, value);
          } else {
            const int col = curr_index - num_rows - num_cols;
            if ((mode_ones == 1)
                && (numTimesTightColUB[col] != savedNumTimesTightColUB[col])) {
              continue;
            }
            const int index[1] = { col };
            const double value[1] = { -1. };
            vec = new CoinPackedVector(1, index, value);
          }
          addToObjectiveFromPackedVector(cutSolver, vec, false, 1.);
          break;
        }
      } /* mode = 0 */
      else {
        // Remove all rows that are tight but were not before
        int num_removed = 0, num_to_remove = 0;
        for (int tmp_ind = 0; tmp_ind < (int) indexAddedToObjective.size(); tmp_ind++) {
          check_ind = indexAddedToObjective[tmp_ind];
          if (check_ind < 0) {
            continue;
          }
          num_to_remove++;
          const int curr_index = indexToCheck[sortIndex[check_ind]];
          double curr_activity = 0., curr_rhs = 0.;
          if (curr_index < num_rows) {
            curr_activity = cutSolver->getRowActivity()[curr_index];
            curr_rhs = cutSolver->getRightHandSide()[curr_index];
          } else if (curr_index < num_rows + num_cols) {
            const int var = curr_index - num_rows;
            curr_activity = cutSolver->getColSolution()[var];
            curr_rhs = cutSolver->getColLower()[var];
          } else {
            const int var = curr_index - num_rows - num_cols;
            curr_activity = cutSolver->getColSolution()[var];
            curr_rhs = cutSolver->getColUpper()[var];
          }
          if (isVal(curr_activity, curr_rhs)) {
            num_removed++;
            indexAddedToObjective[tmp_ind] += 1;
            indexAddedToObjective[tmp_ind] *= -1;
            if (vec) {
              delete vec;
              vec = NULL;
            }
            if (curr_index < num_rows) {
              vec = new CoinPackedVector(mat->getVectorSize(curr_index),
                  mat->getIndices() + mat->getVectorFirst(curr_index),
                  mat->getElements() + mat->getVectorFirst(curr_index), false);
            } else if (curr_index < num_rows + num_cols) {
              const int col = curr_index - num_rows;
              const int index[1] = { col };
              const double value[1] = { 1. };
              vec = new CoinPackedVector(1, index, value);
            } else {
              const int col = curr_index - num_rows - num_cols;
              const int index[1] = { col };
              const double value[1] = { -1. };
              vec = new CoinPackedVector(1, index, value);
            }
            addToObjectiveFromPackedVector(cutSolver, vec, false, -1.);
          }
        }
        if (num_removed * num_to_remove == 0 || (num_to_remove - num_removed == 0)) {
          break;
        }
      } /* mode = 1 */
    } /* try_ind = 1 ... MAX_NUM_TRIES */
  } /* try max_depth times */
  if (ret_val) {
    setConstantObjectiveFromPackedVector(cutSolver, 0.);
  }
  cutSolver->setRowUpper(point_row_ind, orig_ub);
  if (vec) {
    delete vec;
  }
#ifdef TRACE_CUT_BOUND
  if (tmpSolver) {
    delete tmpSolver;
  }
#endif
  return ret_val;
} /* findCutsTightOnPoint */

/**
 * @brief Try the (1,...,1) obj, all points, then select among rays ``intelligently''
 * @return Number generated cuts
 */
int targetStrongAndDifferentCuts(PointCutsSolverInterface* const cutSolver,
    const double beta, const std::vector<int>& nonZeroColIndex, AdvCuts& cuts,
    int& num_cuts_this_cgs, int& num_cuts_total, int& num_obj_tried,
    const PointCutsSolverInterface* const origSolver,
    const SolutionInfo & probData, const std::vector<double>& ortho,
//    const IntersectionInfo& interPtsAndRays,
//    const AdvCut* const objCut,
    const int cgsIndex, const std::string& cgsName, const AdvCuts& structSICs,
    const Stats& timeStats, const std::string& timeName, const bool inNBSpace) {
  const int init_num_cuts = (int) cuts.size();
  if (reachedCutLimit(num_cuts_this_cgs, num_cuts_total)
      || reachedTimeLimit(timeStats, timeName, param.getTIMELIMIT())) {
    return 0;
  }

  // Setup step
//  std::set<compareRayInfo> sortedRays;
  std::vector<rowAndActivity> pointIndex, rayIndex;
  setupForTargetedCutGeneration(pointIndex, rayIndex, ortho, cutSolver);

  // numTimesTight will keep how frequently that row (or column) was optimal for some objective
  // If the value is < 0, then we should not ever re-try that objective
//  std::vector<int> numTimesTight(cutSolver->getNumRows() + cutSolver->getNumCols(), 0);
  std::vector<int> numTimesTightRow(cutSolver->getNumRows(), 0);
  std::vector<int> numTimesTightColLB(cutSolver->getNumCols(), 0);
  std::vector<int> numTimesTightColUB(cutSolver->getNumCols(), 0);

  // Filter out columns that are empty
//  std::vector<int> nonZeroColIndex;
//  for (int col_ind = 0; col_ind < cutSolver->getNumCols(); col_ind++) {
//    if (cutSolver->getMatrixByCol()->getVectorLengths()[col_ind] == 0) {
//      numTimesTightColLB[col_ind] = -1;
//      numTimesTightColUB[col_ind] = -1;
//    } else {
//      nonZeroColIndex.push_back(col_ind);
//    }
//  }

  int num_points_tried = 0, num_rays_tried = 0;
  int num_failures = 0;
  int return_code = 0;
  const int BAD_RETURN_CODE = -1 * (CutSolverFails::PRIMAL_CUTSOLVER_FAIL_IND + 1);
  const int MAX_NUM_POINTS_TO_TRY =
        param.getParamVal(ParamIndices::USE_TIGHT_POINTS_HEUR_PARAM_IND);
  const int MAX_NUM_OBJ_PER_POINT =
      param.getParamVal(ParamIndices::NUM_OBJ_PER_POINT_PARAM_IND) < 0 ?
          -1 * param.getParamVal(ParamIndices::NUM_OBJ_PER_POINT_PARAM_IND)
              * getCutLimit() :
          param.getParamVal(ParamIndices::NUM_OBJ_PER_POINT_PARAM_IND);
//      10000;
//      (GlobalVariables::maxNodeDepth > 0) ? GlobalVariables::maxNodeDepth : 1;
  const int MAX_NUM_RAYS_TO_TRY =
      param.getParamVal(ParamIndices::USE_TIGHT_RAYS_HEUR_PARAM_IND) < 0 ?
          -1 * (param.getParamVal(ParamIndices::USE_TIGHT_RAYS_HEUR_PARAM_IND))
              * std::ceil(std::sqrt(rayIndex.size())) :
          param.getParamVal(ParamIndices::USE_TIGHT_RAYS_HEUR_PARAM_IND);
  const int MAX_NUM_UNIT_VECTORS_TO_TRY =
      param.getParamVal(ParamIndices::USE_UNIT_VECTORS_HEUR_PARAM_IND) < 0 ?
          -1 * (param.getParamVal(ParamIndices::USE_UNIT_VECTORS_HEUR_PARAM_IND))
              * std::ceil(std::sqrt(probData.numCols)) :
          std::min(cutSolver->getNumCols(), param.getParamVal(ParamIndices::USE_UNIT_VECTORS_HEUR_PARAM_IND));
//  std::vector<double> obj(cutSolver->getNumCols(), 1.0); // Start with the all ones objective

  // First, the all ones objective
  if (param.getParamVal(ParamIndices::USE_ALL_ONES_HEUR_PARAM_IND) == 1) {
    GlobalVariables::timeStats.start_timer(
        CutHeuristicsName[CutHeuristics::ALL_ONES_CUT_HEUR] + "_TIME");
#ifdef TRACE
    printf("\n## Try all ones objective; cuts so far: %d. (cgs %d/%d). ##\n",
        (int) cuts.size(), cgsIndex + 1,
        param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND));
#endif
    setConstantObjectiveFromPackedVector(cutSolver, 1.);
    return_code = tryOneObjective(num_failures, numTimesTightRow,
        numTimesTightColLB, numTimesTightColUB, nonZeroColIndex,
        CutHeuristics::ALL_ONES_CUT_HEUR, cutSolver, beta, cuts,
        num_cuts_this_cgs, num_cuts_total, num_obj_tried, origSolver, probData,
        cgsIndex, cgsName, structSICs, inNBSpace);
    setConstantObjectiveFromPackedVector(cutSolver, 0.);
    GlobalVariables::timeStats.end_timer(
        CutHeuristicsName[CutHeuristics::ALL_ONES_CUT_HEUR] + "_TIME");

    if (return_code == BAD_RETURN_CODE
        || reachedCutLimit(num_cuts_this_cgs, num_cuts_total)
        || reachedTimeLimit(timeStats, timeName, param.getTIMELIMIT())
        || reachedFailureLimit(cuts.size() - init_num_cuts, num_failures,
            GlobalVariables::timeStats.get_total_time(GlobalConstants::SOLVE_PRLP_TIME))) {
      return (int) cuts.size() - init_num_cuts; // abandon
    }
  } /* all ones heuristic */

  // Next we try the iterative bilinear heuristic
  if (param.getParamVal(ParamIndices::NUM_CUTS_ITER_BILINEAR_PARAM_IND) >= 1) {
    // Try the bilinear optimization; this usually does not add many cuts
    GlobalVariables::timeStats.start_timer(
        CutHeuristicsName[CutHeuristics::ITER_BILINEAR_CUT_HEUR] + "_TIME");
#ifdef TRACE
    printf(
        "\n## Using iterative procedure to get deepest cut post SICs; cuts so far: %d. (cgs %d/%d). ##\n",
        (int) cuts.size(), cgsIndex + 1,
        param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND));
#endif
    const int init_num_iter_bil_obj = num_obj_tried;
    const int init_num_iter_bil_cuts = cuts.size();
    iterateDeepestCutPostMSIC(cuts, num_cuts_this_cgs, num_cuts_total,
        num_obj_tried, cutSolver, nonZeroColIndex, origSolver, probData, beta,
        cgsIndex, cgsName, structSICs, inNBSpace);
    setConstantObjectiveFromPackedVector(cutSolver, 0.);
    const int final_num_iter_bil_obj = num_obj_tried - init_num_iter_bil_obj;
    const int final_num_iter_bil_cuts = cuts.size() - init_num_iter_bil_cuts;
    num_failures += (final_num_iter_bil_obj - final_num_iter_bil_cuts);
    GlobalVariables::timeStats.end_timer(
        CutHeuristicsName[CutHeuristics::ITER_BILINEAR_CUT_HEUR] + "_TIME");

    if (return_code == BAD_RETURN_CODE
        || reachedCutLimit(num_cuts_this_cgs, num_cuts_total)
        || reachedTimeLimit(timeStats, timeName, param.getTIMELIMIT())
        || reachedFailureLimit(cuts.size() - init_num_cuts, num_failures,
            GlobalVariables::timeStats.get_total_time(GlobalConstants::SOLVE_PRLP_TIME))) {
      return (int) cuts.size() - init_num_cuts; // abandon
    }
  } /* iterative bilinear procedure */

  // Reset things with unlimited time
  GlobalVariables::timeStats.start_timer(GlobalConstants::SOLVE_PRLP_TIME);
  cutSolver->getModelPtr()->setNumberIterations(0);
  cutSolver->getModelPtr()->setMaximumSeconds(-1.);
  cutSolver->initialSolve();
  GlobalVariables::timeStats.end_timer(GlobalConstants::SOLVE_PRLP_TIME);

  // Let us try to target the strong branching lower bound
  // This means we find something tight on the point with the lowest objective value
  // Since that point cannot be cut away, that is the best bound we can hope for
  const int strong_lb_row_ind =
      (pointIndex[0].row < 0) ?
          (-1 * (pointIndex[0].row + 1)) : pointIndex[0].row;
  if (param.getParamVal(ParamIndices::MODE_OBJ_PER_POINT_PARAM_IND) >= 0) {
    GlobalVariables::timeStats.start_timer(
        CutHeuristicsName[CutHeuristics::STRONG_LB_CUT_HEUR] + "_TIME");
#ifdef TRACE
    printf(
        "\n## Try finding a cut tight on the branching lb point; cuts so far: %d. (cgs %d/%d). ##\n",
        (int) cuts.size(), cgsIndex + 1,
        param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND));
#endif
//  const int strong_lb_row_ind =
//      (pointIndex[pointIndex.size() - 1].row < 0) ?
//          (-1 * (pointIndex[pointIndex.size() - 1].row)) :
//          pointIndex[pointIndex.size() - 1].row;
    if (!findCutsTightOnPoint(num_failures, numTimesTightRow,
        numTimesTightColLB, numTimesTightColUB, nonZeroColIndex, strong_lb_row_ind,
        init_num_cuts, CutHeuristics::STRONG_LB_CUT_HEUR, cutSolver, beta, cuts,
        num_cuts_this_cgs, num_cuts_total, num_obj_tried, origSolver, probData,
        ortho, cgsIndex, cgsName, structSICs, timeStats, timeName, inNBSpace,
        MAX_NUM_OBJ_PER_POINT)) {
      GlobalVariables::timeStats.end_timer(
          CutHeuristicsName[CutHeuristics::STRONG_LB_CUT_HEUR] + "_TIME");
      return (int) cuts.size() - init_num_cuts; // abandon
    }
    GlobalVariables::timeStats.end_timer(
        CutHeuristicsName[CutHeuristics::STRONG_LB_CUT_HEUR] + "_TIME");

    if (return_code == BAD_RETURN_CODE
        || reachedCutLimit(num_cuts_this_cgs, num_cuts_total)
        || reachedTimeLimit(timeStats, timeName, param.getTIMELIMIT())
        || reachedFailureLimit(cuts.size() - init_num_cuts, num_failures,
            GlobalVariables::timeStats.get_total_time(GlobalConstants::SOLVE_PRLP_TIME))) {
      return (int) cuts.size() - init_num_cuts; // abandon
    }
  } /* branching lb heuristic */

//  // Zero out the objective
//  std::fill(obj.begin(), obj.end(), 0.);
//
//  // These are the rays emanating from that strong lb point
//  for (int row_ind = strong_lb_row_ind + 1; row_ind < cutSolver->getNumRows();
//      row_ind++) {
//    if (!isZero(cutSolver->getRightHandSide()[row_ind])
//        || return_code == BAD_RETURN_CODE
//        || reachedCutLimit(num_cuts_this_cgs, num_cuts_total)
//        || reachedTimeLimit(timeStats, timeName, param.getTIMELIMIT())
//        || reachedFailureLimit(cuts.size() - init_num_cuts, num_failures)) {
//      break;
//    }
//    setFullVecFromPackedVec(obj, mat->getVector(row_ind));
//    cutSolver->setObjective(obj.data());
//    num_obj_tried++;
//    return_code = genCutsFromCutSolver(cuts, cutSolver, solver, probData, beta,
//        cgsIndex, cgsName, structSICs, num_cuts_this_cgs, num_cuts_total,
//        inNBSpace, CutHeuristics::STRONG_LB_CUT_HEUR);
//    if (return_code > 0) {
//        updateStepForTargetedCutGeneration(numTimesTight, //pointIndex, rayIndex,
//            cutSolver);
//    } else {
//      num_failures++;
//    }
//
//    // Zero out the objective
//    std::fill(obj.begin(), obj.end(), 0.);
//  }

  // For each of the points that has not been tight so far, try it
  if (MAX_NUM_POINTS_TO_TRY > 0) {
    GlobalVariables::timeStats.start_timer(
        CutHeuristicsName[CutHeuristics::TIGHT_POINTS_CUT_HEUR] + "_TIME");
#ifdef TRACE
    printf(
        "\n## Try finding a cut tight on each intersection point; cuts so far: %d. (cgs %d/%d): %d total points to try, %d objectives left. ##\n",
        (int) cuts.size(), cgsIndex + 1,
        param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND),
        (int) pointIndex.size(), MAX_NUM_POINTS_TO_TRY);
#endif
    for (int ind = 0; ind < (int) pointIndex.size() - 1; ind++) {
      if (return_code == BAD_RETURN_CODE
          || reachedCutLimit(num_cuts_this_cgs, num_cuts_total)
          || reachedTimeLimit(timeStats, timeName, param.getTIMELIMIT())
          || reachedFailureLimit(cuts.size() - init_num_cuts, num_failures,
            GlobalVariables::timeStats.get_total_time(GlobalConstants::SOLVE_PRLP_TIME))) {
        GlobalVariables::timeStats.end_timer(
            CutHeuristicsName[CutHeuristics::TIGHT_POINTS_CUT_HEUR] + "_TIME");
        return (int) cuts.size() - init_num_cuts; // abandon
      }
      if (num_points_tried >= MAX_NUM_POINTS_TO_TRY) {
        break;
      }
      const int row_ind = pointIndex[ind].row;
      if (row_ind < 0 || numTimesTightRow[row_ind] != 0) {
        continue;
      }
      num_points_tried++;
      pointIndex[ind].row = -1 * (row_ind + 1);
      if (!findCutsTightOnPoint(num_failures, numTimesTightRow,
            numTimesTightColLB, numTimesTightColUB, nonZeroColIndex, row_ind, init_num_cuts,
            CutHeuristics::TIGHT_POINTS_CUT_HEUR, cutSolver, beta, cuts,
            num_cuts_this_cgs, num_cuts_total, num_obj_tried, origSolver, probData,
            ortho, cgsIndex, cgsName, structSICs, timeStats, timeName, inNBSpace,
            MAX_NUM_OBJ_PER_POINT)) {
        GlobalVariables::timeStats.end_timer(
            CutHeuristicsName[CutHeuristics::TIGHT_POINTS_CUT_HEUR] + "_TIME");
        return (int) cuts.size() - init_num_cuts; // abandon
      }
    } /* tight on points */
    GlobalVariables::timeStats.end_timer(
        CutHeuristicsName[CutHeuristics::TIGHT_POINTS_CUT_HEUR] + "_TIME");
  } /* MAX_NUM_POINTS_TO_TRY > 0 */

  if (return_code == BAD_RETURN_CODE
      || reachedCutLimit(num_cuts_this_cgs, num_cuts_total)
      || reachedTimeLimit(timeStats, timeName, param.getTIMELIMIT())
      || reachedFailureLimit(cuts.size() - init_num_cuts, num_failures,
          GlobalVariables::timeStats.get_total_time(GlobalConstants::SOLVE_PRLP_TIME))) {
    return (int) cuts.size() - init_num_cuts; // abandon
  }

  // Next, we should try the non-basic directions individually, because these have worked well before
  if (MAX_NUM_UNIT_VECTORS_TO_TRY > 0) {
    GlobalVariables::timeStats.start_timer(
        CutHeuristicsName[CutHeuristics::UNIT_VECTORS_CUT_HEUR] + "_TIME");
#ifdef TRACE
    printf(
        "\n## Try finding deepest cut with respect to the axis directions; cuts so far: %d. (cgs %d/%d): %d dirns, %d to try. ##\n",
        (int) cuts.size(), cgsIndex + 1,
        param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND),
        cutSolver->getNumCols(), MAX_NUM_UNIT_VECTORS_TO_TRY);
#endif
    const int numNonZero = nonZeroColIndex.size();
    std::vector<int> sortIndex(numNonZero);
    std::vector<double> sortCriterion(numNonZero);
    for (int i = 0; i < numNonZero; i++) {
      sortIndex[i] = i;
      sortCriterion[i] = (param.getParamVal(ParamIndices::NB_SPACE_PARAM_IND)) ? probData.nonBasicReducedCost[nonZeroColIndex[i]] : origSolver->getReducedCost()[nonZeroColIndex[i]];
    }
    std::sort(sortIndex.begin(), sortIndex.end(),
        index_cmp_dsc<const std::vector<double>&>(sortCriterion)); // descending
    int num_unit_vectors_tried = 0;
    for (int tmp_ind = 0; tmp_ind < (int) sortIndex.size(); tmp_ind++) {
      if (num_unit_vectors_tried >= MAX_NUM_UNIT_VECTORS_TO_TRY) {
        break;
      }
      if (return_code == BAD_RETURN_CODE
          || reachedCutLimit(num_cuts_this_cgs, num_cuts_total)
          || reachedTimeLimit(timeStats, timeName, param.getTIMELIMIT())
          || reachedFailureLimit(cuts.size() - init_num_cuts, num_failures,
              GlobalVariables::timeStats.get_total_time(GlobalConstants::SOLVE_PRLP_TIME))) {
        GlobalVariables::timeStats.end_timer(
            CutHeuristicsName[CutHeuristics::UNIT_VECTORS_CUT_HEUR] + "_TIME");
        return (int) cuts.size() - init_num_cuts; // abandon
      }
      const int nb_dir = sortIndex[tmp_ind];
      if (numTimesTightColLB[nb_dir] != 0) {
        continue;
      }
      cutSolver->setObjCoeff(nb_dir, 1.);
      num_unit_vectors_tried++;
      return_code = tryOneObjective(num_failures, numTimesTightRow,
          numTimesTightColLB, numTimesTightColUB, nonZeroColIndex,
          CutHeuristics::UNIT_VECTORS_CUT_HEUR, cutSolver, beta, cuts,
          num_cuts_this_cgs, num_cuts_total, num_obj_tried, origSolver,
          probData, cgsIndex, cgsName, structSICs, inNBSpace);
      if (cutSolver->isProvenOptimal()) {
        if (greaterThanVal(cutSolver->getColSolution()[nb_dir],
            cutSolver->getColLower()[nb_dir])) {
          cutSolver->setColLower(nb_dir, cutSolver->getObjValue());
        }
      }
      cutSolver->setObjCoeff(nb_dir, 0.);
    } /* nb directions */
    GlobalVariables::timeStats.end_timer(
        CutHeuristicsName[CutHeuristics::UNIT_VECTORS_CUT_HEUR] + "_TIME");
  } /* try axis directions in the PRLP */

  if (return_code == BAD_RETURN_CODE
      || reachedCutLimit(num_cuts_this_cgs, num_cuts_total)
      || reachedTimeLimit(timeStats, timeName, param.getTIMELIMIT())
      || reachedFailureLimit(cuts.size() - init_num_cuts, num_failures,
          GlobalVariables::timeStats.get_total_time(GlobalConstants::SOLVE_PRLP_TIME))) {
    return (int) cuts.size() - init_num_cuts; // abandon
  }

  const CoinPackedMatrix* mat = cutSolver->getMatrixByRow();
  if (MAX_NUM_RAYS_TO_TRY > 0) {
    // Continue with rays
    GlobalVariables::timeStats.start_timer(
        CutHeuristicsName[CutHeuristics::TIGHT_RAYS_CUT_HEUR] + "_TIME");
  #ifdef TRACE
    printf(
        "\n## Try finding a cut tight on the intersection rays; cuts so far: %d. (cgs %d/%d): %d total rays to try, %d objectives left. ##\n",
        (int) cuts.size(), cgsIndex + 1,
        param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND), 
        (int) rayIndex.size(), MAX_NUM_RAYS_TO_TRY);
  #endif
  //  const int MAX_NUM_RAYS_PER_ROUND = 5;
  //  int first_update_index = init_num_cuts;
  //  while (!sortedRays.empty() && num_pr_tried < MAX_NUM_PR_TO_TRY) {
    for (int ind = 0; ind < (int) rayIndex.size(); ind++) {
  //    if (return_code == BAD_RETURN_CODE
  //        || reachedCutLimit(num_cuts_this_cgs, num_cuts_total)
  //        || reachedTimeLimit(timeStats, timeName, param.getTIMELIMIT())
  //        || reachedFailureLimit(cuts.size() - init_num_cuts, num_failures)) {
  //      return (int) cuts.size() - init_num_cuts; // abandon
  //    }
  ////    // First update the sortedRays set with the recent cuts and removed rays
  //    updateRaySetForTargetedCutGeneration(sortedRays, rayIndex,
  //        init_num_cuts, first_update_index, cuts, cutSolver);
  //    first_update_index = cuts.size();
  //
  //    int num_tried = 0;
  //    for (compareRayInfo curr_ray_info : sortedRays) {
  //      if (num_tried >= MAX_NUM_RAYS_PER_ROUND) {
  //        break;
  //      }
        if (num_rays_tried >= MAX_NUM_RAYS_TO_TRY) {
          break;
        }
        if (return_code == BAD_RETURN_CODE
            || reachedCutLimit(num_cuts_this_cgs, num_cuts_total)
            || reachedTimeLimit(timeStats, timeName, param.getTIMELIMIT())
            || reachedFailureLimit(cuts.size() - init_num_cuts, num_failures,
                GlobalVariables::timeStats.get_total_time(GlobalConstants::SOLVE_PRLP_TIME))) {
          GlobalVariables::timeStats.end_timer(
              CutHeuristicsName[CutHeuristics::TIGHT_RAYS_CUT_HEUR] + "_TIME");
          return (int) cuts.size() - init_num_cuts;  // abandon
        }
        const int curr_index = ind; //curr_ray_info.index
        const int row_ind = rayIndex[curr_index].row;
        if (row_ind < 0 || numTimesTightRow[row_ind] != 0) {
          continue;
        }
        rayIndex[curr_index].row = -1 * (row_ind + 1);
  //      setFullVecFromPackedVec(obj, mat->getVector(row_ind));
  //      cutSolver->setObjective(obj.data());
        const CoinShallowPackedVector vec = mat->getVector(row_ind);
        addToObjectiveFromPackedVector(cutSolver, &vec, false);
        num_rays_tried++;
        return_code = tryOneObjective(num_failures, numTimesTightRow,
            numTimesTightColLB, numTimesTightColUB, nonZeroColIndex,
            CutHeuristics::TIGHT_RAYS_CUT_HEUR, cutSolver, beta, cuts,
            num_cuts_this_cgs, num_cuts_total, num_obj_tried, origSolver, probData,
            cgsIndex, cgsName, structSICs, inNBSpace);
        if (return_code < 0) {
          numTimesTightRow[row_ind]++;
          numTimesTightRow[row_ind] *= -1;
        }
        setConstantObjectiveFromPackedVector(cutSolver, 0., vec.getNumElements(), vec.getIndices());
  //      std::fill(obj.begin(), obj.end(), 0.); // zero out the objective
  //    }
    } /* tight on rays */
    GlobalVariables::timeStats.end_timer(
        CutHeuristicsName[CutHeuristics::TIGHT_RAYS_CUT_HEUR] + "_TIME");
  } /* MAX_NUM_RAYS_TO_TRY > 0 */

  if (return_code == BAD_RETURN_CODE
      || reachedCutLimit(num_cuts_this_cgs, num_cuts_total)
      || reachedTimeLimit(timeStats, timeName, param.getTIMELIMIT())
      || reachedFailureLimit(cuts.size() - init_num_cuts, num_failures,
          GlobalVariables::timeStats.get_total_time(GlobalConstants::SOLVE_PRLP_TIME))) {
    return (int) cuts.size() - init_num_cuts;  // abandon
  }

  if (num_points_tried < MAX_NUM_POINTS_TO_TRY) {
    // We may still not hit the cut limit but have exhausted all points and rays
    // In this case, we will try the points then the rays in the order they are in
    // (sorted by objective value)
    // and hope that we get new cuts along the way?
    GlobalVariables::timeStats.start_timer(
        CutHeuristicsName[CutHeuristics::TIGHT_POINTS2_CUT_HEUR] + "_TIME");
#ifdef TRACE
    printf(
        "\n## Try being tight on the intersection points _again_; cuts so far: %d. (cgs %d/%d): %d total points to try, %d objectives left. ##\n",
        (int) cuts.size(), cgsIndex + 1,
        param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND), 
        (int) pointIndex.size(), MAX_NUM_POINTS_TO_TRY - num_points_tried);
#endif
    for (int ind = 0; ind < (int) pointIndex.size(); ind++) { // skip the first point (same as the strong lb point)
      if (return_code == BAD_RETURN_CODE
          || reachedCutLimit(num_cuts_this_cgs, num_cuts_total)
          || reachedTimeLimit(timeStats, timeName, param.getTIMELIMIT())
          || reachedFailureLimit(cuts.size() - init_num_cuts, num_failures,
            GlobalVariables::timeStats.get_total_time(GlobalConstants::SOLVE_PRLP_TIME))) {
        GlobalVariables::timeStats.end_timer(
            CutHeuristicsName[CutHeuristics::TIGHT_POINTS2_CUT_HEUR] + "_TIME");
        return (int) cuts.size() - init_num_cuts; // abandon
      }
      if (num_points_tried >= MAX_NUM_POINTS_TO_TRY) {
        break;
      }
      const int row_ind =
        (pointIndex[ind].row < 0) ?
        -1 * (pointIndex[ind].row + 1) : pointIndex[ind].row;
      if (numTimesTightRow[row_ind] < 0) {
        continue;
      }
      if (row_ind == strong_lb_row_ind) {
        continue;
      }
      num_points_tried++;
      if (!findCutsTightOnPoint(num_failures, numTimesTightRow,
            numTimesTightColLB, numTimesTightColUB, nonZeroColIndex, row_ind, init_num_cuts,
            CutHeuristics::TIGHT_POINTS2_CUT_HEUR, cutSolver, beta, cuts,
            num_cuts_this_cgs, num_cuts_total, num_obj_tried, origSolver, probData,
            ortho, cgsIndex, cgsName, structSICs, timeStats, timeName, inNBSpace,
            MAX_NUM_OBJ_PER_POINT)) {
        GlobalVariables::timeStats.end_timer(
            CutHeuristicsName[CutHeuristics::TIGHT_POINTS2_CUT_HEUR] + "_TIME");
        return (int) cuts.size() - init_num_cuts; // abandon
      }
      //    setFullVecFromPackedVec(obj, mat->getVector(row_ind));
      //    cutSolver->setObjective(obj.data());
      //    const CoinShallowPackedVector vec = mat->getVector(row_ind);
      //    setObjectiveFromPackedVector(cutSolver, &vec, false);
      //    num_obj_tried++;
      //    num_points_tried++;
      //    return_code = genCutsFromCutSolver(cuts, cutSolver, solver, probData, beta,
      //        cgsIndex, cgsName, structSICs, num_cuts_this_cgs, num_cuts_total,
      //        inNBSpace, CutHeuristics::TIGHT_POINTS2_CUT_HEUR);
      //    if (return_code > 0) {
      ////      updateStepForTargetedCutGeneration(numTimesTight, //pointIndex, rayIndex,
      ////          cutSolver);
      //    } else {
      //      num_failures++;
      //    }
      //    setConstantObjectiveFromPackedVector(cutSolver, 0., &vec);
      //      std::fill(obj.begin(), obj.end(), 0.); // zero out the objective
    } /* tight on points */
    GlobalVariables::timeStats.end_timer(
        CutHeuristicsName[CutHeuristics::TIGHT_POINTS2_CUT_HEUR] + "_TIME");
  } /* any objectives left for points? */

  if (return_code == BAD_RETURN_CODE
      || reachedCutLimit(num_cuts_this_cgs, num_cuts_total)
      || reachedTimeLimit(timeStats, timeName, param.getTIMELIMIT())
      || reachedFailureLimit(cuts.size() - init_num_cuts, num_failures,
          GlobalVariables::timeStats.get_total_time(GlobalConstants::SOLVE_PRLP_TIME))) {
    return (int) cuts.size() - init_num_cuts; // abandon
  }

  if (num_rays_tried < MAX_NUM_RAYS_TO_TRY) {
    // Continue with rays
    GlobalVariables::timeStats.start_timer(
        CutHeuristicsName[CutHeuristics::TIGHT_RAYS2_CUT_HEUR] + "_TIME");
#ifdef TRACE
    printf(
        "\n## Try the intersection rays _again_; cuts so far: %d. (cgs %d/%d): %d total rays to try, %d objectives left. ##\n",
        (int) cuts.size(), cgsIndex + 1,
        param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND),
        (int) rayIndex.size(), MAX_NUM_RAYS_TO_TRY - num_rays_tried);
#endif
    for (int ind = 0; ind < (int) rayIndex.size(); ind++) {
      if (return_code == BAD_RETURN_CODE
          || reachedCutLimit(num_cuts_this_cgs, num_cuts_total)
          || reachedTimeLimit(timeStats, timeName, param.getTIMELIMIT())
          || reachedFailureLimit(cuts.size() - init_num_cuts, num_failures,
            GlobalVariables::timeStats.get_total_time(GlobalConstants::SOLVE_PRLP_TIME))) {
        GlobalVariables::timeStats.end_timer(
            CutHeuristicsName[CutHeuristics::TIGHT_RAYS2_CUT_HEUR] + "_TIME");
        return (int) cuts.size() - init_num_cuts; // abandon
      }
      if (num_rays_tried >= MAX_NUM_RAYS_TO_TRY) {
        break;
      }
      const int row_ind =
        (rayIndex[ind].row < 0) ?
        -1 * (rayIndex[ind].row + 1) : rayIndex[ind].row;
      if (numTimesTightRow[row_ind] < 0) {
        continue;
      }
      ////    setFullVecFromPackedVec(obj, mat->getVector(row_ind));
      ////    cutSolver->setObjective(obj.data());
      const CoinShallowPackedVector vec = mat->getVector(row_ind);
      addToObjectiveFromPackedVector(cutSolver, &vec, false);
      num_obj_tried++;
      num_rays_tried++;
      return_code = tryOneObjective(num_failures, numTimesTightRow,
          numTimesTightColLB, numTimesTightColUB, nonZeroColIndex,
          CutHeuristics::TIGHT_RAYS2_CUT_HEUR, cutSolver, beta, cuts,
          num_cuts_this_cgs, num_cuts_total, num_obj_tried, origSolver, probData,
          cgsIndex, cgsName, structSICs, inNBSpace);
      //    if (return_code > 0) {
      ////      updateStepForTargetedCutGeneration(numTimesTight, //pointIndex, rayIndex,
      ////          cutSolver);
      //    } else {
      //      num_failures++;
      //    }
      setConstantObjectiveFromPackedVector(cutSolver, 0., vec.getNumElements(), vec.getIndices());
      //    //      std::fill(obj.begin(), obj.end(), 0.); // zero out the objective
    } /* tight on rays again */
    GlobalVariables::timeStats.end_timer(
        CutHeuristicsName[CutHeuristics::TIGHT_RAYS2_CUT_HEUR] + "_TIME");
  } /* any objectives left for rays? */

  return (int) cuts.size() - init_num_cuts;
} /* targetStrongAndDifferentCuts */

/**
 * @brief Try being tight on each of the intersection points/rays
 *
 * This is an expensive procedure, so we check the time limit is not reached
 */
int tightOnPointsRaysCutGeneration(PointCutsSolverInterface* const cutSolver,
    const double beta, const std::vector<int>& nonZeroColIndex, AdvCuts& cuts,
    int& num_cuts_this_cgs, int& num_cuts_total, int& num_obj_tried,
    const PointCutsSolverInterface* const solver, const SolutionInfo & probData,
    //const IntersectionInfo& interPtsAndRays,
    const int cgsIndex, const std::string& cgsName, const AdvCuts& structSICs,
    const Stats& timeStats, const std::string& timeName, const bool inNBSpace) {
  const int init_num_cuts = (int) cuts.size();

  GlobalVariables::timeStats.start_timer(
      CutHeuristicsName[CutHeuristics::TIGHT_POINTS_CUT_HEUR]
      + "_TIME");
#ifdef TRACE
  printf(
      "\n## Try finding a cut tight on each intersection point or ray (cgs %d/%d): %d total. ##\n",
      cgsIndex + 1, param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND),
      cutSolver->getNumRows());
#endif
  const CoinPackedMatrix* mat = cutSolver->getMatrixByRow();
  const int MAX_NUM_POINTS_TO_TRY =
          param.getParamVal(ParamIndices::USE_TIGHT_POINTS_HEUR_PARAM_IND);
  const int MAX_NUM_RAYS_TO_TRY =
      param.getParamVal(ParamIndices::USE_TIGHT_RAYS_HEUR_PARAM_IND) >= 0 ?
          param.getParamVal(ParamIndices::USE_TIGHT_RAYS_HEUR_PARAM_IND) :
          -1 * param.getParamVal(ParamIndices::USE_TIGHT_RAYS_HEUR_PARAM_IND)
              * std::sqrt(mat->getNumRows()); // should be # rays, but this is quicker and off only by a little due to number of points being small
  int numPointsTried = 0, numRaysTried = 0;
  bool hitPointLimit = (MAX_NUM_POINTS_TO_TRY == 0);
  bool hitRayLimit = (MAX_NUM_RAYS_TO_TRY == 0);
  std::vector<double> obj(cutSolver->getNumCols(), 0.0);
  for (int row_ind = 0; row_ind < mat->getNumRows(); row_ind++) {
    if (reachedCutLimit(num_cuts_this_cgs, num_cuts_total)) {
      break; // cut limit reached
    }
    if (reachedTimeLimit(timeStats, timeName, param.getTIMELIMIT())) {
      break; // time limit reached
    }
    if (hitPointLimit && hitRayLimit) {
      break; // hit point and ray limit
    }
    const double rhs = cutSolver->getRightHandSide()[row_ind];
    // Check we have not hit the point or ray limit
    if (isZero(rhs)) {
      if (hitRayLimit) {
        continue;
      }
      numRaysTried++;
      if (numRaysTried >= MAX_NUM_RAYS_TO_TRY) {
        hitRayLimit = true;
      }
    } else {
      if (hitPointLimit) {
        continue;
      }
      numPointsTried++;
      if (numPointsTried >= MAX_NUM_POINTS_TO_TRY) {
        hitRayLimit = true;
      }
    }
    setFullVecFromPackedVec(obj, mat->getVector(row_ind));
    cutSolver->setObjective(obj.data());
    num_obj_tried++;
    const int return_code = genCutsFromCutSolver(cuts, cutSolver,
        nonZeroColIndex, solver, probData, beta, cgsIndex, cgsName, structSICs,
        num_cuts_this_cgs, num_cuts_total, inNBSpace,
        CutHeuristics::TIGHT_POINTS_CUT_HEUR);
    if (return_code == -1 * (CutSolverFails::PRIMAL_CUTSOLVER_FAIL_IND + 1)) {
      break; // abandon, may be primal infeasible
    }
    // Zero out the objective
    std::fill(obj.begin(), obj.end(), 0.);
  }
  GlobalVariables::timeStats.end_timer(
      CutHeuristicsName[CutHeuristics::TIGHT_POINTS_CUT_HEUR]
      + "_TIME");

  return (int) cuts.size() - init_num_cuts;
} /* tightOnPointsAndRaysGeneration */

/**
 * @brief Try cutting off each of the vertices generated in the intermediate stages
 */
int cutVerticesCutGeneration(PointCutsSolverInterface* const cutSolver,
    const double beta, const std::vector<int>& nonZeroColIndex, AdvCuts & cuts,
    int& num_cuts_this_cgs, int& num_cuts_total, int& num_obj_tried,
    const PointCutsSolverInterface* const solver, const SolutionInfo & probData,
    //const IntersectionInfo& interPtsAndRays,
    const std::vector<Vertex>& vertexStore, const int cgsIndex,
    const std::string& cgsName, const AdvCuts& structSICs) {
  const int init_num_cuts = (int) cuts.size();

  GlobalVariables::timeStats.start_timer(
      CutHeuristicsName[CutHeuristics::CUT_VERTICES_CUT_HEUR] + "_TIME");
#ifdef TRACE
  printf(
      "\n## Try cutting each of the generated vertices by as much as possible (cgs %d/%d). ##\n",
      cgsIndex + 1, param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND));
#endif
  for (int vert_ind = 0; vert_ind < (int) vertexStore.size(); vert_ind++) {
    setCutSolverObjectiveFromVertexNB(vert_ind, cutSolver, vertexStore);
//    cutSolver->resolve();
    num_obj_tried++;
    genCutsFromCutSolver(cuts, cutSolver, nonZeroColIndex, solver, probData, beta, cgsIndex,
        cgsName, structSICs, num_cuts_this_cgs, num_cuts_total, true,
        CutHeuristics::CUT_VERTICES_CUT_HEUR);
  }
  GlobalVariables::timeStats.end_timer(
      CutHeuristicsName[CutHeuristics::CUT_VERTICES_CUT_HEUR] + "_TIME");

  return (int) cuts.size() - init_num_cuts;
} /* cutVerticesCutGeneration */

/**
 * For each ray, look at set of intersection points (across all splits) generated by this ray
 * Call this set P(r), corresponding to ray r
 * Let p(r,s) be the point generated by ray r for split s
 * Let d(r,s) = distance along ray r to split s
 * Then for any split s, we can try to cut points p(r,s') for all s' : d(r,s') < d(r,s)
 */
void splitSharingCutGeneration(
    std::vector<PointCutsSolverInterface*>& cutSolver,
    const std::vector<int>& nonZeroColIndex, AdvCuts & cuts,
    std::vector<int>& num_cuts_per_cgs, int& num_cuts_total, int& num_obj_tried,
    const std::vector<IntersectionInfo>& interPtsAndRays,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
//    const std::vector<Ray>& rayStore,
    const std::vector<Vertex>& vertexStore,
    const std::vector<Hplane>& hplaneStore, const AdvCuts& structSICs,
    const std::vector<bool>& isCutSolverPrimalFeasible) {
  GlobalVariables::timeStats.start_timer(
      CutHeuristicsName[CutHeuristics::SPLIT_SHARE_CUT_HEUR] + "_TIME");
  // point analysis must have been done prior to entering this function
  // (this fills the objective bound vector and identifies points)
  const double beta = 1.0; // We are in the complemented nb space
  for (int s = 0; s < solnInfo.numFeasSplits; s++) {
    if (reachedCutLimit(num_cuts_per_cgs[s], num_cuts_total)) {
      continue;
    }
    std::vector<double> obj(interPtsAndRays[s].getNumCols(), 0.0);

    // Sort points based on obj cost
    std::vector<int> sortIndex(interPtsAndRays[s].getNumRows());
    for (int i = 0; i < interPtsAndRays[s].getNumRows(); i++) {
      sortIndex[i] = i;
    }

//    std::vector<double> objDepth(interPtsAndRays[s].objDepth);
    std::sort(sortIndex.begin(), sortIndex.end(),
        index_cmp_asc<const std::vector<double>&>(interPtsAndRays[s].objDepth));

//    const int num_points = interPtsAndRays[s].pointRowIndex.size();
    int num_points_tried = 0;
    for (int ind = 0;
        (ind < interPtsAndRays[s].getNumRows())
            && (num_points_tried
                < param.paramVal[ParamIndices::MAX_POINTS_PER_SPLIT_SPLIT_SHARE_PARAM_IND]);
        ind++) {
      if (reachedCutLimit(num_cuts_per_cgs[s], num_cuts_total)) {
        break;
      }
      const int row_ind = sortIndex[ind];
      if (isZero(interPtsAndRays[s].RHS[row_ind])) {
        continue;
      }
      num_points_tried++;
      const int deep_split_index = findDeepestSplit(interPtsAndRays[s],
          row_ind, s, solver, solnInfo, //rayStore,
          vertexStore, hplaneStore);
      if ((deep_split_index == -1) || !isCutSolverPrimalFeasible[s]
          || reachedCutLimit(num_cuts_per_cgs[deep_split_index],
              num_cuts_total)) {
        continue;
      }

      // Cut this point using the deepest split
      setFullVecFromPackedVec(obj, interPtsAndRays[s].getVector(row_ind));

      // Set the objective for the deepest split solver
      cutSolver[deep_split_index]->setObjective(obj.data());
//      cutSolver[deep_split_index]->initialSolve();
//      cutSolver[deep_split_index]->resolve();
      num_obj_tried++;
      genCutsFromCutSolver(cuts, cutSolver[deep_split_index], nonZeroColIndex,
          solver, solnInfo, beta, solnInfo.feasSplitVar[deep_split_index],
          solver->getColName(solnInfo.feasSplitVar[deep_split_index]),
          structSICs, num_cuts_per_cgs[deep_split_index], num_cuts_total, true,
          CutHeuristics::SPLIT_SHARE_CUT_HEUR);

      // Clear the objective vector back to zeroes
      clearFullVecFromPackedVec(obj,
          interPtsAndRays[s].getVector(row_ind));
    }
  }
  GlobalVariables::timeStats.end_timer(
      CutHeuristicsName[CutHeuristics::SPLIT_SHARE_CUT_HEUR] + "_TIME");
} /* splitSharing */

/**
 * @brief For the ray that generated the given point, find the deepest split the ray intersects
 */
int findDeepestSplit(const IntersectionInfo& interPtsAndRays, const int pt_ind,
    const int split_ind, const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo,
//    const std::vector<Ray>& rayStore,
    const std::vector<Vertex>& vertexStore,
    const std::vector<Hplane>& hplaneStore) {
  const int hplane_ind =
      (interPtsAndRays.hplane[pt_ind] >= 0) ?
          interPtsAndRays.hplane[pt_ind] : 0;

  Ray newRay;
  calcNewRayCoordinatesRank1(newRay, /*solver,*/ solnInfo,
      interPtsAndRays.cutRay[pt_ind], interPtsAndRays.newRay[pt_ind], /*rayStore,*/
      hplaneStore[hplane_ind], param.getRAYEPS());

  // Calculate deepest split
  int max_split_ind = -1;
  double maxDistToSplit = -1.;
  double distToThisSplit = -1.;
  for (int s = 0; s < solnInfo.numFeasSplits; s++) {
    const double distToSplit = distanceToSplit(
        vertexStore[interPtsAndRays.vertex[pt_ind]], newRay,
        solnInfo.feasSplitVar[s], solver, solnInfo);
    if (!isInfinity((distToSplit))
        && greaterThanVal(distToSplit, maxDistToSplit)) {
      maxDistToSplit = distToSplit;
      max_split_ind = s;
    }
    if (s == split_ind) {
      distToThisSplit = distToSplit;
    }
  }

  if (lessThanVal(distToThisSplit, maxDistToSplit))
    return max_split_ind;
  else
    return -1;
} /* findDeepestSplit */

/**
 * We want to solve the bilinear program
 * min <\alpha, x>
 * s.t.
 * <\alpha, p> \ge 1 \forall p \in \pointset
 * <\alpha, r> \ge 0 \forall r \in \rayset
 * x \in Pbar (P + all SICs)
 */
int iterateDeepestCutPostMSIC(AdvCuts & cuts, int& num_cuts_this_cgs,
    int& num_cuts_total, int& num_obj_tried,
    PointCutsSolverInterface* const cutSolver,
    const std::vector<int>& nonZeroColIndex,
    const PointCutsSolverInterface* const origSolver,
    const SolutionInfo & origSolnInfo, const double beta, const int cgsIndex,
    const std::string& cgsName, const AdvCuts& structSICs,
    const bool inNBSpace) {
  if (reachedCutLimit(num_cuts_this_cgs, num_cuts_total)
      || (param.paramVal[NUM_CUTS_ITER_BILINEAR_PARAM_IND] == 0)) {
    return 0;
  }

  bool stationary_point_flag = false;
  const int numCutSolverCols =
      (nonZeroColIndex.size() > 0) ?
          nonZeroColIndex.size() : origSolver->getNumCols();

  // Get solution after adding all structSICs
  PointCutsSolverInterface* MSICSolver = dynamic_cast<PointCutsSolverInterface*>(origSolver->clone());
  setClpParameters(MSICSolver);
  if (isInfinity(applyCuts(MSICSolver, structSICs))) {
    error_msg(error_str,
      "Adding SICs causes infeasible linear program. This should not happen!\n");  // TODO could actually happen in subspace?
    exit(1);
  }

  std::vector<double> NBSICSolution;
  if (inNBSpace) {
    compNBCoor(NBSICSolution, MSICSolver->getColSolution(), MSICSolver,
        origSolver, origSolnInfo);
    if (nonZeroColIndex.size() > 0) {
      for (int i = 0; i < numCutSolverCols; i++) {
        cutSolver->setObjCoeff(i, NBSICSolution[nonZeroColIndex[i]]);
      }
    } else {
      cutSolver->setObjective(NBSICSolution.data());
    }
  } else {
    if ((int) nonZeroColIndex.size() > 0) {
      for (int i = 0; i < numCutSolverCols; i++) {
        cutSolver->setObjCoeff(i,
            MSICSolver->getColSolution()[nonZeroColIndex[i]]);
      }
    } else {
      cutSolver->setObjective(MSICSolver->getColSolution());
    }
  }

  num_obj_tried++;
  int num_cuts_gen = genCutsFromCutSolver(cuts, cutSolver, nonZeroColIndex,
      origSolver, origSolnInfo, beta, cgsIndex, cgsName, structSICs,
      num_cuts_this_cgs, num_cuts_total, inNBSpace,
      CutHeuristics::ITER_BILINEAR_CUT_HEUR);

  if (num_cuts_gen < 0) {
    if (MSICSolver) {
      delete MSICSolver;
    }
    return 0;
  }

  // We may need to get primal rays
  // In case of dual infeasibility, there should be at least one
  // During this procedure, since we are using dual rays, do not use presolve
  cutSolver->setHintParam(OsiDoPresolveInInitial, false);
  cutSolver->setHintParam(OsiDoPresolveInResolve, false);

  // During this procedure, since we are using dual rays, do not use presolve
  MSICSolver->setHintParam(OsiDoPresolveInInitial, false);
  MSICSolver->setHintParam(OsiDoPresolveInResolve, false);

  std::vector<double> SICSolution(MSICSolver->getColSolution(),
      MSICSolver->getColSolution() + MSICSolver->getNumCols());
  std::vector<double> structSpaceCut;
  for (int j = 1;
      (j < param.getParamVal(ParamIndices::NUM_CUTS_ITER_BILINEAR_PARAM_IND))
          && !reachedCutLimit(num_cuts_this_cgs, num_cuts_total); j++) {
    for (int i = 0; i < param.getITER_PER_CUT_BILINEAR(); i++) {
      if (cutSolver->isProvenOptimal()) {
        // Recall that iterativeSICSolver is in the structural space,
        // while cutSolver is in the NB space potentially
        if (inNBSpace) {
          AdvCut::convertCutCoeffFromJSpaceToStructSpaceNonPacked(
              structSpaceCut, origSolver, origSolnInfo.nonBasicVarIndex,
              cutSolver->getColSolution(), 1.0, nonZeroColIndex, true);
          MSICSolver->setObjective(structSpaceCut.data());
        } else {
          if ((int) nonZeroColIndex.size() > 0) {
            for (int i = 0; i < (int) nonZeroColIndex.size(); i++) {
              MSICSolver->setObjCoeff(nonZeroColIndex[i],
                  cutSolver->getColSolution()[i]);
            }
          } else {
            MSICSolver->setObjective(cutSolver->getColSolution());
          }
        }
      } else if (cutSolver->isProvenDualInfeasible()) {
        //cutSolverPrimalRay.resize(1);
        //cutSolverPrimalRay[0] = new double[cutSolver->getNumCols()]; // valgrind says this isn't getting freed for some reason
        //cutSolverPrimalRay = cutSolver->getPrimalRays(1); // There may be more than one, which all lead to cuts
        std::vector<double*> cutSolverPrimalRay = cutSolver->getPrimalRays(1); // There may be more than one, which all lead to cuts
        
        // It has happened (e.g., instance rlp2)
        if (cutSolverPrimalRay[0] == NULL) {
          cutSolver->getModelPtr()->setNumberIterations(0);
          cutSolver->getModelPtr()->setMaximumSeconds(param.getCUTSOLVER_TIMELIMIT());
          cutSolver->initialSolve();
          checkSolverOptimality(cutSolver, false, param.getCUTSOLVER_TIMELIMIT(), true);
          if (cutSolver->isProvenDualInfeasible()) {
            cutSolverPrimalRay = cutSolver->getPrimalRays(1);
          }
          if (!cutSolver->isProvenDualInfeasible() || !cutSolverPrimalRay[0]) {
            warning_msg(warnstring,
                "Issues obtaining ray when cut solver is unbounded on cgs %d/%d when trying to iterate deepest cut post SICs to get cut %d in iteration %d. Abandoning this function.\n",
                cgsIndex + 1,
                param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND),
                j, i);
            // Free memory
            if (cutSolverPrimalRay.size() > 0 && cutSolverPrimalRay[0]) {
              delete[] cutSolverPrimalRay[0];
            }
            cutSolverPrimalRay.clear();
            return exitFromIterateDeepestCutPostMSIC(num_cuts_gen, cutSolver,
                MSICSolver);
          }
        }

        scalePrimalRay(cutSolver->getNumCols(), &cutSolverPrimalRay[0]);
        if (inNBSpace) {
          AdvCut::convertCutCoeffFromJSpaceToStructSpaceNonPacked(
              structSpaceCut, origSolver,
              origSolnInfo.nonBasicVarIndex,
              cutSolverPrimalRay[0], 1.0, nonZeroColIndex, true);
          MSICSolver->setObjective(structSpaceCut.data());
        } else {
          if ((int) nonZeroColIndex.size() > 0) {
            for (int i = 0; i < (int) nonZeroColIndex.size(); i++) {
              MSICSolver->setObjCoeff(nonZeroColIndex[i],
                  cutSolverPrimalRay[0][i]);
            }
          } else {
            MSICSolver->setObjective(cutSolverPrimalRay[0]);
          }
        }

        // Free memory
        if (cutSolverPrimalRay.size() > 0 && cutSolverPrimalRay[0]) {
          delete[] cutSolverPrimalRay[0];
        }
        cutSolverPrimalRay.clear();
      } /* check if PRLP is bounded or not */

      MSICSolver->initialSolve();
      checkSolverOptimality(MSICSolver, false, param.getCUTSOLVER_TIMELIMIT(), true);
      //MSICSolver->resolve(); // Because in pk1, using fpcs, we get a time in which the iterSICSolver is dual infeasible but does not say it is proven so
      if (!MSICSolver->isProvenOptimal()
          && !MSICSolver->isProvenDualInfeasible()) {
        error_msg(errorstring,
            "cgs %d/%d: Iterative SIC solver not proven optimal or dual infeasible (j: %d, iter: %d).\n",
            cgsIndex + 1, param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND),
            j, i);
        writeErrorToLog(errorstring, GlobalVariables::log_file);
        exit(1);
      }

      if (isNegInfinity(MSICSolver->getObjValue(), param.getINFINITY())) {
        // We essentially have a ray, though it is not being reported as such
        // Try resolving because this fixes the issue sometimes
        MSICSolver->resolve();
        checkSolverOptimality(MSICSolver, false, param.getCUTSOLVER_TIMELIMIT(), true);

        if (!MSICSolver->isProvenDualInfeasible()) {
          warning_msg(warnstring,
              "Scaling issues in MSICSolver for split %d when trying to iterate deepest cut post SICs to get cut %d in iteration %d. Abandoning this function.\n",
              cgsIndex, j, i);
          return exitFromIterateDeepestCutPostMSIC(num_cuts_gen, cutSolver,
              MSICSolver);
        }
      }

      if (MSICSolver->isProvenOptimal()) {
        // Check if we have reached a stationary point
        if (vectorsEqual(MSICSolver->getNumCols(), SICSolution.data(),
            MSICSolver->getColSolution())) {
          stationary_point_flag = true;
          break;
        }

        // Otherwise, reset SICSolution to be current solution and set objective
        SICSolution.assign(MSICSolver->getColSolution(),
            MSICSolver->getColSolution() + MSICSolver->getNumCols());
      } else if (MSICSolver->isProvenDualInfeasible()) {
        // Obtain the dual ray
        std::vector<double*> SICSolverPrimalRay = MSICSolver->getPrimalRays(1);
        
        // It has happened (e.g., instance rlp2)
        if (SICSolverPrimalRay[0] == NULL) {
          MSICSolver->initialSolve();
          checkSolverOptimality(MSICSolver, false, param.getCUTSOLVER_TIMELIMIT(), true);
          if (MSICSolver->isProvenDualInfeasible()) {
            SICSolverPrimalRay = MSICSolver->getPrimalRays(1);
          }
          if (!MSICSolver->isProvenDualInfeasible()
              || !SICSolverPrimalRay[0]) {
            warning_msg(warnstring,
                "Issues obtaining ray when iterative SIC solver is unbounded on cgs %d/%d when trying to iterate deepest cut post SICs to get cut %d in iteration %d. Abandoning this function.\n",
                cgsIndex + 1,
                param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND),
                j, i);
            // Free memory
            if (SICSolverPrimalRay.size() > 0 && SICSolverPrimalRay[0]) {
              delete[] SICSolverPrimalRay[0];
            }
            SICSolverPrimalRay.clear();
            return exitFromIterateDeepestCutPostMSIC(num_cuts_gen, cutSolver,
                MSICSolver);
          }
        }
        scalePrimalRay(MSICSolver->getNumCols(), &SICSolverPrimalRay[0]);

        // Check if we have reached a stationary point
        if (vectorsEqual(MSICSolver->getNumCols(),
            SICSolution.data(), SICSolverPrimalRay[0])) {
          stationary_point_flag = true;
          // Free memory
          if (SICSolverPrimalRay.size() > 0 && SICSolverPrimalRay[0]) {
            delete[] SICSolverPrimalRay[0];
          }
          SICSolverPrimalRay.clear();
          break;
        }

        // Otherwise, reset SICSolution to be current solution and set objective
        SICSolution.assign(SICSolverPrimalRay[0],
            SICSolverPrimalRay[0] + MSICSolver->getNumCols());
        
        // Free memory
        if (SICSolverPrimalRay.size() > 0 && SICSolverPrimalRay[0]) {
          delete[] SICSolverPrimalRay[0];
        }
        SICSolverPrimalRay.clear();
      } /* check whether MSICSolver is optimal or unbdd */

      if (inNBSpace) {
        compNBCoor(NBSICSolution, SICSolution.data(), MSICSolver,
            origSolver, origSolnInfo);
        if (nonZeroColIndex.size() > 0) {
          for (int i = 0; i < numCutSolverCols; i++) {
            cutSolver->setObjCoeff(i, NBSICSolution[nonZeroColIndex[i]]);
          }
        } else {
          cutSolver->setObjective(NBSICSolution.data());
        }
      } else {
        if (nonZeroColIndex.size() > 0) {
          for (int i = 0; i < numCutSolverCols; i++) {
            cutSolver->setObjCoeff(i, SICSolution[nonZeroColIndex[i]]);
          }
        } else {
          cutSolver->setObjective(SICSolution.data());
        }
      }

      // Resolve PRLP without generating a cut
      cutSolver->getModelPtr()->setNumberIterations(0);
      cutSolver->getModelPtr()->setMaximumSeconds(param.getCUTSOLVER_TIMELIMIT());
      cutSolver->resolve();
      checkSolverOptimality(cutSolver, false, param.getCUTSOLVER_TIMELIMIT(), true);
      // Sometimes it simply takes too long to solve optimally
      if (cutSolver->isIterationLimitReached() || cutSolver->getModelPtr()->hitMaximumIterations()) {
//        GlobalVariables::numCutSolverFails[CutSolverFails::ITERATION_CUTSOLVER_FAIL_IND]++;
        return exitFromIterateDeepestCutPostMSIC(num_cuts_gen, cutSolver,
            MSICSolver);
      }
      if (!cutSolver->isProvenOptimal()
          && !cutSolver->isProvenDualInfeasible()) {
        error_msg(errorstring,
            "cgs %d/%d: cutSolver issue in iter bilinear: not proven optimal and not dual infeasible (unbounded) (j: %d, iter: %d).\n",
            cgsIndex + 1, param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND),
            j, i);
        writeErrorToLog(errorstring, GlobalVariables::log_file);
        exit(1);
      }

      if (isNegInfinity(cutSolver->getObjValue(), param.getINFINITY())) {
        // We essentially have a ray, though it is not being reported as such
        // Try resolving because this fixes the issue sometimes
        cutSolver->resolve();
        checkSolverOptimality(cutSolver, false, param.getCUTSOLVER_TIMELIMIT(), true);

        // Sometimes it simply takes too long to solve optimally
        if (cutSolver->isIterationLimitReached()
            || cutSolver->getModelPtr()->hitMaximumIterations()) {
//          GlobalVariables::numCutSolverFails[CutSolverFails::ITERATION_CUTSOLVER_FAIL_IND]++;
          return exitFromIterateDeepestCutPostMSIC(num_cuts_gen, cutSolver,
              MSICSolver);
        }
        if (!cutSolver->isProvenDualInfeasible()) {
          warning_msg(warnstring,
              "Scaling issues for cgs %d/%d when trying to iterate deepest cut post SICs to get cut %d in iteration %d. Abandoning this function.\n",
              cgsIndex + 1, param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND),
              j, i);
          return exitFromIterateDeepestCutPostMSIC(num_cuts_gen, cutSolver,
              MSICSolver);
        }
      }
    } /* do this many iterations per obj */
    num_obj_tried++;
    const int return_code = genCutsFromCutSolver(cuts, cutSolver,
        nonZeroColIndex, origSolver, origSolnInfo, beta, cgsIndex, cgsName,
        structSICs, num_cuts_this_cgs, num_cuts_total, inNBSpace,
        CutHeuristics::ITER_BILINEAR_CUT_HEUR);
    if (return_code >= 0) {
      num_cuts_gen += return_code;
    } else {
      return exitFromIterateDeepestCutPostMSIC(num_cuts_gen, cutSolver,
          MSICSolver);
    }
    if (stationary_point_flag)
      break;
  } /* loop at most num_cuts_iter_bilinear times */
        
  return exitFromIterateDeepestCutPostMSIC(num_cuts_gen, cutSolver, MSICSolver);
} /* iterateDeepestCutPostMSIC */

int exitFromIterateDeepestCutPostMSIC(const int num_cuts_gen,
    PointCutsSolverInterface* const cutSolver,
    PointCutsSolverInterface* const MSICSolver) {
  // Delete solvers
  if (MSICSolver) {
    delete MSICSolver;
  }

  cutSolver->getModelPtr()->setMaximumIterations(param.getCUTSOLVER_MAX_ITER());
  cutSolver->getModelPtr()->setMaximumSeconds(param.getCUTSOLVER_TIMELIMIT());

  cutSolver->setHintParam(OsiDoPresolveInInitial,
      (param.getParamVal(ParamIndices::CUT_PRESOLVE_PARAM_IND) > 0) ?
          true : false);
  cutSolver->setHintParam(OsiDoPresolveInResolve,
      (param.getParamVal(ParamIndices::CUT_PRESOLVE_PARAM_IND) > 1) ?
          true : false);

  return num_cuts_gen;
} /* exitFromIterateDeepestCutPostMSIC */

/**********************************************************/
/**
 * Removes the fixed variable var from the system defining tmpSolver
 * and adjusts the constant side appropriately using fracCol
 * (the column of A corresponding to var)
 */
bool removeFixedVar(OsiSolverInterface* tmpSolver, const int var,
    const double fixed_val, const CoinPackedVectorBase& fracCol) {
  const int fracColNumElements = fracCol.getNumElements();
  const int* fracColIndices = fracCol.getIndices();
  const double* fracColElements = fracCol.getElements();

  tmpSolver->deleteCols(1, &var);
  if (!isZero(fixed_val)) {
    for (int el = 0; el < fracColNumElements; el++) {
      const int row = fracColIndices[el];
      const double col_val = fracColElements[el];
      const char row_sense = tmpSolver->getRowSense()[row];
      if (row_sense == 'L' || row_sense == 'E') {
        tmpSolver->setRowUpper(row,
            tmpSolver->getRowUpper()[row] - col_val * fixed_val);
      }
      if (row_sense == 'G' || row_sense == 'E') {
        tmpSolver->setRowLower(row,
            tmpSolver->getRowLower()[row] - col_val * fixed_val);
      }
    }
  }
  tmpSolver->initialSolve();

  return checkSolverOptimality(tmpSolver, true);
} /* removeFixedVar */

/**********************************************************/
/**
 * Adds constraint to tmpSolver to fix to a facet of S
 */
bool fixDisjTerm(OsiClpSolverInterface* tmpSolver,
    const std::vector<std::vector<int> >& termIndices,
    const std::vector<std::vector<double> >& termCoeff,
    const std::vector<double>& termRHS, const bool set_equal) {
//
//      const int* ind, const double* coeff, const double fixed_val
  for (int ineq_ind = 0; ineq_ind < (int) termIndices.size(); ineq_ind++) {
    const int num_coeff = termIndices[ineq_ind].size();
    tmpSolver->addRow(num_coeff, termIndices[ineq_ind].data(),
        termCoeff[ineq_ind].data(), termRHS[ineq_ind],
        (set_equal) ? termRHS[ineq_ind] : tmpSolver->getInfinity());
  }

  tmpSolver->resolve();

  return checkSolverOptimality(tmpSolver, true);

//  for (int ineq_ind = 0; ineq_ind < (int) termIndices.size(); ineq_ind++) {
//    const int num_coeff = termIndices[ineq_ind].size();
//    if (num_coeff == 0) {
//      const int varToBound = termIndices[ineq_ind][0];
//
//      // Ensure new row is non-basic
//      if (!isRayVar(tmpSolver,
//          tmpSolver->getNumCols() + tmpSolver->getNumRows() - 1)) {
//        error_msg(errstr,
//            "Added equality row but corresponding artificial (slack) variable is basic!\n");
//        writeErrorToII(errstr, GlobalVariables::inst_info_out);
//        exit(1);
//      }
//    }
//  }
} /* fixDisjTerm */

/**********************************************************/
/**
 * Changes bounds on variables to restrict to a disjunctive term
 * (should be more efficient than adding constraints)
 */
bool fixDisjTermUsingBounds(OsiClpSolverInterface* tmpSolver,
    const std::vector<std::vector<int> >& termIndices,
    const std::vector<std::vector<double> >& termCoeff,
    const std::vector<double>& termRHS, const bool set_equal) {
//
//      const int* ind, const double* coeff, const double fixed_val
  for (int ineq_ind = 0; ineq_ind < (int) termIndices.size(); ineq_ind++) {
    const int num_coeff = termIndices[ineq_ind].size();
    if (num_coeff > 1) {
      tmpSolver->addRow(num_coeff, termIndices[ineq_ind].data(),
          termCoeff[ineq_ind].data(), termRHS[ineq_ind],
          (set_equal) ? termRHS[ineq_ind] : tmpSolver->getInfinity());
    } else {
      const int var = termIndices[ineq_ind][0];
      if (termCoeff[ineq_ind][0] > 0) {
        if (isVal(termCoeff[ineq_ind][0], 1.)) {
          tmpSolver->setColLower(var, termRHS[ineq_ind]);
        } else {
          tmpSolver->setColLower(var,
              termRHS[ineq_ind] / termCoeff[ineq_ind][0]);
        }
      } else {
        if (isVal(termCoeff[ineq_ind][0], -1.)) {
          tmpSolver->setColUpper(var, -1. * termRHS[ineq_ind]);
        } else {
          tmpSolver->setColUpper(var,
              -1. * termRHS[ineq_ind] / termCoeff[ineq_ind][0]);
        }
      }
    }
  }

  const double origVal = tmpSolver->getObjValue(); // check against this to prevent situation of false feasibility (e.g., with glass4 -64)
  tmpSolver->resolve();
  const double newVal = tmpSolver->getObjValue();
  const double ratio = newVal / origVal;
  if (tmpSolver->isProvenOptimal() && ratio > 1e3) {
    tmpSolver->getModelPtr()->initialSolve(); // this seems to work better than OsiClp->initialSolve() for some reason
  }

  return checkSolverOptimality(tmpSolver, true);

//  for (int ineq_ind = 0; ineq_ind < (int) termIndices.size(); ineq_ind++) {
//    const int num_coeff = termIndices[ineq_ind].size();
//    if (num_coeff == 0) {
//      const int varToBound = termIndices[ineq_ind][0];
//
//      // Ensure new row is non-basic
//      if (!isRayVar(tmpSolver,
//          tmpSolver->getNumCols() + tmpSolver->getNumRows() - 1)) {
//        error_msg(errstr,
//            "Added equality row but corresponding artificial (slack) variable is basic!\n");
//        writeErrorToII(errstr, GlobalVariables::inst_info_out);
//        exit(1);
//      }
//    }
//  }
} /* fixDisjTermUsingBounds */

/**********************************************************/
/**
 * Adds constraint to tmpSolver to fix to a facet of S
 */
bool fixDisjTerm(OsiClpSolverInterface* tmpSolver,
    const std::vector<int>& termIndices, const std::vector<double>& termCoeff,
    const double termRHS, const bool set_equal) {

  const int num_coeff = termIndices.size();
  tmpSolver->addRow(num_coeff, termIndices.data(), termCoeff.data(), termRHS,
      (set_equal) ? termRHS : tmpSolver->getInfinity());

  tmpSolver->resolve();

  return checkSolverOptimality(tmpSolver, true);

//  // Ensure new row is non-basic
//  if (!isRayVar(tmpSolver,
//      tmpSolver->getNumCols() + tmpSolver->getNumRows() - 1)) {
//    error_msg(errstr,
//        "Added equality row but corresponding artificial (slack) variable is basic!\n");
//    writeErrorToII(errstr, GlobalVariables::inst_info_out);
//    exit(1);
//  }

  return true;
} /* fixDisjTerm */

/**********************************************************/
/**
 * Fixes bound on variable to fixed_val
 */
bool fixFacetOfS(OsiClpSolverInterface* tmpSolver, const int var,
    const double fixed_val) {
  tmpSolver->setColLower(var, fixed_val);
  tmpSolver->setColUpper(var, fixed_val);
  tmpSolver->resolve();

  if (!checkSolverOptimality(tmpSolver, true)) {
    return false;
  }

  // Ensure that var is non-basic
  if (!isRayVar(tmpSolver, var)) {
    // Perform dual pivot
    tmpSolver->enableSimplexInterface(true);

    const int dirnOut = 1; // Should not matter since this variable is fixed
    int varIn, dirnIn;
    double dist;
    const int return_code = tmpSolver->dualPivotResult(varIn, dirnIn, var,
        dirnOut, dist,
        NULL);
    if ((return_code < 0) || !isZero(dist) || !isRayVar(tmpSolver, var)) {
      error_msg(errorstring,
          "Error making the fixed variable non-basic. Return code: %d. varIn: %d. dirnIn: %d. var: %d. dirnOut: %d. dist: %s.\n",
          return_code, varIn, dirnIn, var, dirnOut, stringValue(dist).c_str());
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }

    tmpSolver->disableSimplexInterface();
  }

  return true;
} /* fixFacetOfS */

///**********************************************************/
///**
// * @return Whether the solver was feasible and give objective value if so
// */
//bool checkOptimality(double & objVal, const OsiSolverInterface* const solver,
//    const int col) {
//  bool returncode = false;
//
//  if (solver->isProvenOptimal()) {
//    objVal = solver->getObjValue();
//    returncode = true;
//  } else if (solver->isProvenPrimalInfeasible()) {
//    // For the splits that are infeasible on one side, just add the strongest cut available,
//    // which fixes the integer variable to the other side.
////    error_msg(errstr,
////        "Solver is primal infeasible when variable %d with name %s and value %f is fixed to floor %f.\n",
////        col, solver->getColName(col).c_str(), xk, floorxk);
////    writeErrorToII(errstr, LiftGICsParam::inst_info_out);
////    exit(1);
//    returncode = false;
//  } else if (solver->isProvenDualInfeasible()) {
//    // It is then unbounded in this direction?
//    // Shouldn't happen.
//    error_msg(errstr,
//        "Solver is dual infeasible when variable %d with name %s is fixed to %f.\n",
//        col, solver->getColName(col).c_str(), solver->getColSolution()[col]);
//    writeErrorToII(errstr, GlobalVariables::inst_info_out);
//    exit(1);
//  } else {
//    error_msg(errstr,
//        "Solver is not proven optimal when variable %d with name %s is fixed to %f.\n",
//        col, solver->getColName(col).c_str(), solver->getColSolution()[col]);
//    writeErrorToII(errstr, GlobalVariables::inst_info_out);
//    exit(1);
//  }
//
//  return returncode;
//} /* checkOptimality */

/**********************************************************/
/**
 * @return Whether the solver was feasible
 */
bool solveFloor(double & objVal, const OsiSolverInterface* const solver, const int col) {
  bool returncode = true;
  const double xk = solver->getColSolution()[col];
  const double floorxk = std::floor(xk);

  //LiftGICsSolverInterface* tmpSolver = new LiftGICsSolverInterface(*solver);
  PointCutsSolverInterface* tmpSolver = dynamic_cast<PointCutsSolverInterface*>(solver->clone());
  setClpParameters(tmpSolver);

  tmpSolver->setColLower(col, floorxk);
  tmpSolver->setColUpper(col, floorxk);
  tmpSolver->initialSolve();

  if (checkSolverOptimality(tmpSolver, true)) {
    objVal = tmpSolver->getObjValue();
    returncode = true;
  } else {
    returncode = false;
  }

  // Free
  if (tmpSolver) {
    delete tmpSolver;
  }

  return returncode;
} /* solveFloor */

/**
 * @return Whether the solver was feasible
 */
bool solveCeil(double & objVal, const OsiSolverInterface* const solver, const int col) {
  bool returncode = true;
  const double xk = solver->getColSolution()[col];
  const double ceilxk = std::ceil(xk);

  //LiftGICsSolverInterface* tmpSolver = new LiftGICsSolverInterface(*solver);
  PointCutsSolverInterface* tmpSolver = dynamic_cast<PointCutsSolverInterface*>(solver->clone());
  setClpParameters(tmpSolver);

  tmpSolver->setColLower(col, ceilxk);
  tmpSolver->setColUpper(col, ceilxk);
  tmpSolver->initialSolve();

  if (checkSolverOptimality(tmpSolver, true)) {
    objVal = tmpSolver->getObjValue();
    returncode = true;
  } else {
    returncode = false;
  }

  // Free
  if (tmpSolver) {
    delete tmpSolver;
  }

  return returncode;
} /* solveCeil */

/**
 * If fixFloor, we add the cut xk <= floorxk. Otherwise, we add xk >= ceilxk.
 * Will not be added if this is a cut that already exists in the fpcs.
 */
bool addOneSidedCut(AdvCuts & fpcs, bool fixFloor,
    const OsiSolverInterface* const solver, const int splitVar) {
  AdvCut currCut(false, 1);
  currCut.cgsIndex = splitVar;
  currCut.cgsName = solver->getColName(splitVar);
  currCut.splitVarIndex[0] = splitVar;
  currCut.num_coeff = solver->getNumCols();
  genOneSidedOsiRowCut(currCut, fixFloor, solver, splitVar);
  currCut.cutHeur = CutHeuristics::ONE_SIDED_CUT_HEUR;
  GlobalVariables::numCutsFromHeur[CutHeuristics::ONE_SIDED_CUT_HEUR]++;
  const double currCutNorm = currCut.row().twoNorm();
  const double violation = currCut.violated(solver->getColSolution());
  currCut.setEffectiveness(violation / currCutNorm);
  bool added = fpcs.addNonDuplicateCut(currCut);
  return added;
} /* addOneSidedCut */

/**
 * Will not be added if this is a cut that already exists in the fpcs.
 */
bool addOneSidedCut(AdvCuts & fpcs, const OsiSolverInterface* const solver,
    const int num_coeff, const int* ind, const double* coeff, const double lb,
    const int cgs_ind, const std::string& cgsName) {
  AdvCut currCut(false, num_coeff);
  currCut.cgsIndex = cgs_ind;
  for (int i = 0; i < num_coeff; i++) {
    currCut.splitVarIndex[i] = ind[i];
  }
  currCut.cgsName = cgsName;
  currCut.num_coeff = solver->getNumCols();
  currCut.setRow(num_coeff, ind, coeff);
  currCut *= -1.; // facet was fx >= b_f; adding fx <= b_f - 1, in >= form
  currCut.setLb(1 - lb);
  currCut.cutHeur = CutHeuristics::ONE_SIDED_CUT_HEUR;
  const double currCutNorm = currCut.row().twoNorm();
  const double violation = currCut.violated(solver->getColSolution());
  currCut.setEffectiveness(violation / currCutNorm);
  if (fpcs.addNonDuplicateCut(currCut)) {
    GlobalVariables::numCutsFromHeur[CutHeuristics::ONE_SIDED_CUT_HEUR]++;
    return true;
  } else {
    return false;
  }
} /* addOneSidedCut */

/**
 * Will not be added if this is a cut that already exists in the fpcs.
 */
int fixToDisjTerm(AdvCuts & fpcs, const OsiSolverInterface* const solver,
    const std::vector<std::vector<int> >& termIndices,
    const std::vector<std::vector<double> >& termCoeff,
    const std::vector<double>& termRHS, const int cgs_ind,
    const std::string& cgsName) {
  int num_added = 0;
  for (int ineq_ind = 0; ineq_ind < (int) termIndices.size(); ineq_ind++) {
    const int num_coeff = termIndices[ineq_ind].size();
    const int* ind = termIndices[ineq_ind].data();
    const double* coeff = termCoeff[ineq_ind].data();

    AdvCut currCut(false, num_coeff);
    currCut.cgsIndex = cgs_ind;
    for (int i = 0; i < num_coeff; i++) {
      currCut.splitVarIndex[i] = ind[i];
    }
    currCut.cgsName = cgsName;
    currCut.num_coeff = solver->getNumCols();
    currCut.setRow(num_coeff, ind, coeff);
    currCut.setLb(termRHS[ineq_ind]);
//    currCut *= -1.; // facet was fx >= b_f; adding fx <= b_f - 1, in >= form
//    currCut.setLb(1 - termRHS[ineq_ind]);
    currCut.cutHeur = CutHeuristics::ONE_SIDED_CUT_HEUR;
    if (fpcs.addNonDuplicateCut(currCut)) {
      GlobalVariables::numCutsFromHeur[CutHeuristics::ONE_SIDED_CUT_HEUR]++;
      num_added++;
    }
  }
  return num_added;
} /* fixToDijsTerm */

/**
 * If fixFloor, we create the cut xk <= floorxk.
 * Otherwise, we create xk >= ceilxk.
 */
void genOneSidedOsiRowCut(OsiRowCut & currCut, const bool fixFloor,
    const OsiSolverInterface* const solver, const int splitVar) {
  double xk = solver->getColSolution()[splitVar];
  std::vector<int> index(1);
  std::vector<double> elem(1);
  index[0] = splitVar;

  if (fixFloor) {
    elem[0] = -1.0;
    currCut.setLb(-1.0 * std::floor(xk));
  } else {
    elem[0] = 1.0;
    currCut.setLb(ceil(xk));
  }

  currCut.setRow(1, index.data(), elem.data());
//  currCut.cutHeur = CutHeuristics::ONE_SIDED_CUT_HEUR;
  const double currCutNorm = currCut.row().twoNorm();
  const double violation = currCut.violated(solver->getColSolution());
  currCut.setEffectiveness(violation / currCutNorm);
} /* genOneSidedOsiRowCut */

/**
 * @brief Checks to make sure there is no big disparity among cut coefficients
 */
bool badlyScaled(int num_cols, const double* colSolution, const double rhs,
    const double SENSIBLE_MAX, const double EPS) {
  double minAbsElem = 0.0, maxAbsElem = 0.0;
  for (int i = 0; i < num_cols; i++) {
    double absCurr = std::abs(colSolution[i]);
    if (isZero(absCurr, EPS)) { // make sure min and max are not zero
      continue;
    }

    if ((minAbsElem > absCurr) || isZero(minAbsElem, EPS)) {
      minAbsElem = absCurr;
    }
    if (maxAbsElem < absCurr) {
      maxAbsElem = absCurr;
    }
  }
  const double absCurr = std::abs(rhs);
  if (absCurr > maxAbsElem) {
    maxAbsElem = absCurr;
  }
  if (isZero(maxAbsElem, EPS)
  // || isZero(minAbsElem / maxAbsElem)
      || greaterThanVal(maxAbsElem / minAbsElem, SENSIBLE_MAX, EPS)) {
    return true;
  }
  return false;
} /* badlyScaled */

/**
 * Scales ray generated.
 * If max is not too big, then we do not bother rescaling
 * If min / norm too small, then we do not do that scaling
 *
 * @return status: -1 is bad ray, don't add it; 0 is no scaling; 1 is okay scaling
 */
int scalePrimalRay(int num_cols, double** rayToScale,
    const double SENSIBLE_MAX) {
  double totalSOS = 0.0, minAbsElem = 0.0, maxAbsElem = 0.0;
  for (int i = 0; i < num_cols; i++) {
    double curr = (*rayToScale)[i];
    if (isZero(curr, param.getEPS())) {
      curr = +0.0;
      (*rayToScale)[i] = +0.0;
      continue;
    }

    const double absCurr = std::abs(curr);
    totalSOS += absCurr * absCurr;

    if ((minAbsElem > absCurr) || isZero(minAbsElem)) {
      minAbsElem = absCurr;
    }
    if (maxAbsElem < absCurr) {
      maxAbsElem = absCurr;
    }
  }

  // If we already have a sensible max or if the smallest element is really small...
  // Should we maybe check that it is numerically unstable?
  // If it has elements that are too small, we simply reject it
  // We can check this by ensuring the ratio minAbsElem / maxAbsElem is not too small
  if (lessThanVal(maxAbsElem, SENSIBLE_MAX)) {
    return 0;
  }
  if (isZero(maxAbsElem) || isZero(minAbsElem, 1000 * param.getEPS())
      || isZero(minAbsElem / maxAbsElem)) {
    return -1;
  }

  double norm = std::sqrt(totalSOS);
  double divFactor;

  double newAbsMin = minAbsElem / norm; // Do not want this to be too small
  if (isZero(newAbsMin)) {
    divFactor = minAbsElem / (100 * param.getEPS());
  } else {
    divFactor = norm;
  }

  for (int i = 0; i < num_cols; i++) {
    (*rayToScale)[i] = (*rayToScale)[i] / divFactor;
  }
  return 1;
} /* scalePrimalRay */

///**
// * Sort cuts based on secret sauce (primarily efficacy, i.e., Euclidean distance)
// */
//void sortCuts(const LiftGICsSolverInterface* const solver, const AdvCuts& cs,
//    std::vector<int>& sortedCutIndex) {
//  const int numCuts = cs.sizeCuts();
//  std::vector<double> efficacy(numCuts);
//  sortedCutIndex.clear();
//  sortedCutIndex.resize(numCuts);
//  for (int cut_ind = 0; cut_ind < numCuts; cut_ind++) {
//    sortedCutIndex[cut_ind] = cut_ind;
//    efficacy[cut_ind] = cs.cuts[cut_ind].effectiveness();
//  }
//  std::sort(sortedCutIndex.begin(), sortedCutIndex.end(),
//      index_cmp<const std::vector<double>&>(efficacy));
////  // Also calculate score for each cut a la SCIP
////  // given a fixed orthogonality threshold
////  const double orth_thresh = 0.5;
////  OsiRowCut cut1 = cs.rowCut(1);
////  OsiRowCut cut2 = cs.rowCut(2);
////  cut1.row().normSquare();
////  cut1.row().normSquare();
//}

/**
 * Compute score as in SCIP from effectiveness and orthogonalities
 */
void computeScore(AdvCuts& cs, const int pos) {
  cs.score[pos] = cs.cuts[pos].effectiveness() + cs.orthogonality[pos];
} /* computeScore */

/**
 * As in SCIP
 */
void updateOrthogonolaties(AdvCuts& cs, const OsiRowCut& cut,
    const double min_cut_orthogononality,
    const std::vector<int>& roundWhenCutApplied) {
  for (int pos = 0; pos < cs.sizeCuts(); pos++) {
    if (roundWhenCutApplied[pos] >= 0) {
      continue;
    }
    // Update orthogonality
    const double this_ortho = getOrthogonality(cut.row(), cs.cuts[pos].row());
    if (this_ortho < cs.orthogonality[pos]) {
      if (this_ortho < min_cut_orthogononality) {
        /* cut is too parallel: release the row and delete the cut */
        //SCIPdebugMessage("    -> deleting parallel cut <%s> after adding <%s> (pos=%d, len=%d, orthogonality=%g, score=%g)\n",
        //SCIProwGetName(sepastore->cuts[pos]), SCIProwGetName(cut), pos, SCIProwGetNNonz(cut), thisortho, sepastore->scores[pos]);
        //SCIP_CALL( sepastoreDelCut(sepastore, blkmem, set, eventqueue, eventfilter, lp, pos) );
        cs.orthogonality[pos] = -1;
        computeScore(cs, pos); // we actually only penalize it a lot
      } else {
        /* recalculate score */
        cs.orthogonality[pos] = this_ortho;
        computeScore(cs, pos);
      }
    }
  }
} /* updateOrthogonalities */

/**
 * As in SCIP, returns the position of the best non-forced cut in the cuts array
 * Currently not forcing any cuts
 * */
int getBestCutIndex(const AdvCuts& cs,
    const std::vector<int>& roundWhenCutApplied) {
  int bestpos = -1;
  double bestscore = -1;
  for (int pos = 0; pos < cs.sizeCuts(); pos++) {
    // Check if cut has not been applied and is current best cut
    if ((roundWhenCutApplied[pos] < 0) && (cs.score[pos] > bestscore)) {
      bestscore = cs.score[pos];
      bestpos = pos;
    }
  }
  return bestpos;
} /* getBestCutIndex */

/**
 * Generic way to apply a set of cuts to a solver
 * @return Solver value after adding the cuts, if they were added successfully
 */
double applyCuts(PointCutsSolverInterface* solver, const AdvCuts& cs, 
    const int startCutIndex, const int numCutsOverload, double compare_obj) {
  const int numCuts = (numCutsOverload >= 0) ? numCutsOverload : cs.sizeCuts();
  if (solver && !solver->isProvenOptimal()) {
    solver->resolve();
    checkSolverOptimality(solver, false);
  }
  if (solver == NULL || !solver->isProvenOptimal()) {
    return solver->getInfinity();
  }
  
  const double initObjValue = solver->getObjValue();
  if (isNegInfinity(compare_obj)) {
    compare_obj = initObjValue;
  }

  // If the number of cuts is 0, we simply get the value without these cuts applied
  if (numCuts > 0) {
#ifdef TRACE
    OsiSolverInterface::ApplyCutsReturnCode code;
    int num_applied = 0;
#endif
    if ((startCutIndex == 0) && (numCuts == cs.sizeCuts())) {
#ifdef TRACE
      code = solver->applyCuts(cs);
      num_applied = code.getNumApplied();
#else
      solver->applyCuts(cs);
#endif
    } else {
#ifdef TRACE
      const int old_num_rows = solver->getNumRows();
#endif
      OsiRowCut* cuts;
      if (numCuts > 0) {
        cuts = new OsiRowCut[numCuts];

        for (int i = startCutIndex; i < startCutIndex + numCuts; i++) {
          cuts[i - startCutIndex] = cs.rowCut(i);
        }
        solver->applyRowCuts(numCuts, cuts);
        delete[] cuts;
      }
#ifdef TRACE
      num_applied = (solver->getNumRows() - old_num_rows);
#endif
    }

#ifdef TRACE
    printf("Cuts applied: %d (of the %d possible).\n",
        num_applied, numCuts);
#endif
  } /* check if num cuts > 0 */

  const bool useResolve = solver->isProvenOptimal();
  if (useResolve) {
    solver->resolve();
  } else {
    solver->initialSolve();
  }
  checkSolverOptimality(solver, false);

  // At this point it is useful to check for integer feasibility quickly
  // If we are in the subspace, then this is not necessary
  if (!param.paramVal[ParamIndices::SUBSPACE_PARAM_IND]) {
    if (!solver->isProvenOptimal()) {
      error_msg(errstr,
          "Not in subspace and solver not optimal after adding cuts, so we may have an integer infeasible problem. Exit status: %d.\n",
          solver->getModelPtr()->status());
#ifdef SAVE_INFEASIBLE_LP
      std::string infeas_f_name = GlobalVariables::out_f_name_stub
          + "_infeasible";
      solver->writeMps(infeas_f_name.c_str(), "mps", 1);
#endif
      if (param.getEXIT_ON_INFEAS()) {
        writeErrorToLog(errstr, GlobalVariables::log_file);
        exit(1);
      }
    }
  }
  
  // Sometimes there are slight inaccuracies at the end that we can get rid of
  if (solver->isProvenOptimal() && solver->getObjValue() < compare_obj) {
    solver->resolve();
    checkSolverOptimality(solver, false);
  }

  if (solver->isProvenOptimal()) {
    return solver->getObjValue();
  } else {
    return solver->getInfinity();  // Minimization problem, so this means infeas
  }
} /* applyCuts */

/**
 * Generic way to apply a set of cuts to a solver
 * @return Solver value after adding the cuts, if they were added successfully
 */
double applyCuts(PointCutsSolverInterface* solver, const AdvCuts& cs,
    const int numCutsOverload, double compare_obj) {
  const int numCuts = (numCutsOverload >= 0) ? numCutsOverload : cs.sizeCuts();
  return applyCuts(solver, cs, 0, numCuts, compare_obj);
} /* applyCuts */

/**
 * Generic way to apply a set of cuts to a solver in rounds (simple version)
 * @return Solver value after adding the cuts, if they were added successfully
 */
double applyCutsInRounds(PointCutsSolverInterface* solver, AdvCuts& cs,
    const int numCutsToAddPerRound, const int maxRounds, double compare_obj) {
  if (solver && !solver->isProvenOptimal()) {
    solver->resolve();
    checkSolverOptimality(solver, false);
  }
  if (solver == NULL || !solver->isProvenOptimal()) {
    return solver->getInfinity();
  }

  const int numCuts = cs.sizeCuts();
  const int numRounds =
      std::ceil((double) numCuts / numCutsToAddPerRound) <= maxRounds ?
          std::ceil((double) numCuts / numCutsToAddPerRound) : maxRounds;
  int numCutsByRound = 0;
  const bool updateScores = true;

  const double initObjValue = solver->getObjValue();
  if (isNegInfinity(compare_obj)) {
    compare_obj = initObjValue;
  }

#ifdef TRACE
    int numOrigRows = solver->getNumRows();
#endif

  OsiRowCut* cuts = NULL;
  if (numCuts > 0) {
    cuts = new OsiRowCut[numCuts];

//    if (numRounds > 1) { // Sort and compute scores with initialized orthogonalities
    if (updateScores) {
      cs.score.clear();
      cs.score.resize(numCuts);
      cs.orthogonality.clear();
      cs.orthogonality.resize(numCuts, 1.); // initialized orthogonality
    }
    for (int pos = 0; pos < numCuts; pos++) {
      if (updateScores) {
        computeScore(cs, pos);
      }
    }

    for (int i = 0; i < numCuts; i++) {
      cuts[i] = cs.rowCut(i);
    }
  }

  // If the number of cuts is 0, we simply get the value without these cuts applied
  std::vector<int> roundWhenCutApplied(numCuts, -1);
  int numCutsAlreadyApplied = 0;
  for (int round_ind = 0; round_ind < numRounds; round_ind++) {
    numCutsByRound = 0;

    // Re-start scores
    if (updateScores) {
      for (int pos = 0; pos < numCuts; pos++) {
        if (roundWhenCutApplied[pos] < 0) {
          cs.orthogonality[pos] = 1.; // re-start orthogonality
          computeScore(cs, pos);
        }
      }
    }
    const int numCutsLeftToApply = numCuts - numCutsAlreadyApplied;
    const int numCutsToAddInThisRound =
        (numCuts - numCutsAlreadyApplied >= numCutsToAddPerRound) ?
            numCutsToAddPerRound : numCutsLeftToApply;

    if (updateScores) {
//      if (round_ind < numRounds - 1) {
        for (int i = 0; i < numCutsToAddInThisRound; i++) {
          const int pos = getBestCutIndex(cs, roundWhenCutApplied);
          if (pos >= 0) {
            solver->applyRowCuts(1, &(cuts[pos]));
            roundWhenCutApplied[pos] = round_ind;
            numCutsByRound++;
            numCutsAlreadyApplied++;
            if (updateScores) {
              updateOrthogonolaties(cs, cs.cuts[pos], 0, roundWhenCutApplied);
            }
          } else {
            break;
          }
        }
//      } else {
//        for (int pos = 0; pos < numCuts; pos++) {
//          if (roundWhenCutApplied[pos] < 0) {
//            solver->applyRowCuts(1, &(cuts[pos]));
//            roundWhenCutApplied[pos] = round_ind;
//            numCutsByRound++;
//            numCutsAlreadyApplied++;
//            if (numCutsByRound == numCutsToAddInThisRound) {
//              break;
//            }
//          }
//        }
//      }
    }

    const bool useResolve = solver->isProvenOptimal();
    if (useResolve) {
      solver->resolve();
    } else {
      solver->initialSolve();
    }
    checkSolverOptimality(solver, false);

    // At this point it is useful to check for integer feasibility quickly
    // If we are in the subspace, then this is not necessary
    if (!param.paramVal[ParamIndices::SUBSPACE_PARAM_IND]) {
      if (!solver->isProvenOptimal()) {
        error_msg(errstr,
            "Not in subspace and solver not optimal after adding cuts, so we may have an integer infeasible problem. Exit status: %d.\n", solver->getModelPtr()->status());
#ifdef SAVE_INFEASIBLE_LP
        std::string infeas_f_name = GlobalVariables::out_f_name_stub + "_infeasible";
        solver->writeMps(infeas_f_name.c_str(), "mps", 1);
#endif
        if (param.getEXIT_ON_INFEAS()) {
          if (numCuts && cuts) {
            delete[] cuts;
          }
          writeErrorToLog(errstr, GlobalVariables::log_file);
          exit(1);
        }
      }
    }

    if (!(solver->isProvenOptimal())) {
      if (numCuts && cuts) {
        delete[] cuts;
      }
      return (solver->getInfinity()); // minimization problem, so this means infeas
    }
  } /* end iterating over rounds */

  if (numCuts && cuts) {
    delete[] cuts;
  }

  // Sometimes there are slight inaccuracies at the end that we can get rid of
  if (solver->isProvenOptimal() && solver->getObjValue() < compare_obj) {
    solver->resolve();
    checkSolverOptimality(solver, false);
  }

#ifdef TRACE
  printf("Cuts applied in rounds: %d (of the %d possible).\n",
      solver->getNumRows() - numOrigRows, numCuts);
  if (solver->isProvenOptimal()) {
    printf("\tInitial value: %f.\n\tFinal value: %f.\n", initObjValue,
        solver->getObjValue());
  }
#endif
  return (solver->getObjValue());
} /* applyCutsInRounds (simple) */

/**
 * Generic way to apply a set of cuts to a solver in rounds (complex version)
 * @return Solver value after adding the cuts, if they were added successfully
 */
double applyCutsInRounds(PointCutsSolverInterface* solver, AdvCuts& cs,
    std::vector<int>& numCutsByRound, std::vector<double>& boundByRound,
    std::vector<double>& basisCond, const int numCutsToAddPerRound,
    std::vector<int>* const oldRoundWhenCutApplied,
    bool populateRoundWhenCutApplied, 
    const int maxRounds, double compare_obj) {
  if (solver && !solver->isProvenOptimal()) {
    solver->resolve();
    checkSolverOptimality(solver, false);
  }
  if (solver == NULL || !solver->isProvenOptimal()) {
    return solver->getInfinity();
  }
  
  const double initObjValue = solver->getObjValue();
  if (isNegInfinity(compare_obj)) {
    compare_obj = initObjValue;
  }

//  const int numCuts = (numCutsOverload >= 0) ? numCutsOverload : cs.sizeCuts();
  const int numCuts = cs.sizeCuts();
  const int numRounds =
      std::ceil((double) numCuts / numCutsToAddPerRound) <= maxRounds ?
          std::ceil((double) numCuts / numCutsToAddPerRound) : maxRounds + 1;
  populateRoundWhenCutApplied = populateRoundWhenCutApplied
      && (oldRoundWhenCutApplied != NULL);
  const bool updateScores = (oldRoundWhenCutApplied == NULL)
      || populateRoundWhenCutApplied;
  if (populateRoundWhenCutApplied) {
    numCutsByRound.resize(numRounds);
  }
  basisCond.resize(numRounds);
  boundByRound.resize(numRounds);

#ifdef TRACE
    int numOrigRows = solver->getNumRows();
#endif

  OsiRowCut* cuts = NULL;
//  std::vector<int> sortedCutIndex(numCuts);
  if (numCuts > 0) {
    cuts = new OsiRowCut[numCuts];

//    if (numRounds > 1) { // Sort and compute scores with initialized orthogonalities
    if (updateScores) {
      cs.score.clear();
      cs.score.resize(numCuts);
      cs.orthogonality.clear();
      cs.orthogonality.resize(numCuts, 1.); // initialized orthogonality
    }
    for (int pos = 0; pos < numCuts; pos++) {
      if (updateScores) {
        computeScore(cs, pos);
      }
//        sortedCutIndex[pos] = pos;
    }
//      std::sort(sortedCutIndex.begin(), sortedCutIndex.end(),
//          index_cmp<const std::vector<double>&>(cs.score));
//    }

    for (int i = 0; i < numCuts; i++) {
//      cuts[i] = cs.rowCut(sortedCutIndex[i]);
      cuts[i] = cs.rowCut(i);
    }
  }

  // If the number of cuts is 0, we simply get the value without these cuts applied
  //const std::vector<OsiRowCut> cutVec = cs.cuts;
  std::vector<int> roundWhenCutApplied(numCuts, -1);
  for (int round_ind = 0; round_ind < numRounds; round_ind++) {
    // Re-start scores
    if (updateScores) {
      for (int pos = 0; pos < numCuts; pos++) {
        if (roundWhenCutApplied[pos] < 0) {
          cs.orthogonality[pos] = 1.; // re-start orthogonality
          computeScore(cs, pos);
        }
      }
    }
    const int start_ind = round_ind * numCutsToAddPerRound;
    const int numCutsToAddInThisRound =
        (round_ind < numRounds - 1) ?
            numCutsToAddPerRound : numCuts - start_ind;

//    if (numRounds > 1) {
    if (updateScores) {
      if (round_ind < numRounds - 1) {
        for (int i = 0; i < numCutsToAddInThisRound; i++) {
          const int pos = getBestCutIndex(cs, roundWhenCutApplied);
          if (pos >= 0) {
            solver->applyRowCuts(1, &(cuts[pos]));
            roundWhenCutApplied[pos] = round_ind;
            numCutsByRound[round_ind]++;
            if (updateScores) {
              updateOrthogonolaties(cs, cs.cuts[pos], 0, roundWhenCutApplied);
            }
          } else {
            break;
          }
        }
      } else {
        for (int pos = 0; pos < numCuts; pos++) {
          if (roundWhenCutApplied[pos] < 0) {
            solver->applyRowCuts(1, &(cuts[pos]));
            roundWhenCutApplied[pos] = round_ind;
            numCutsByRound[round_ind]++;
            if (numCutsByRound[round_ind] == numCutsToAddInThisRound) {
              break;
            }
          }
        }
      }
    } else {
      int numCutsAdded = 0;
      for (int pos = 0; pos < numCuts; pos++) {
        if ((*oldRoundWhenCutApplied)[pos] == round_ind) {
          solver->applyRowCuts(1, &(cuts[pos]));
          numCutsAdded++;
          if (numCutsAdded == numCutsToAddInThisRound) {
            break;
          }
        }
      }
    }
//    } else {
//      solver->applyRowCuts(numCutsToAddInThisRound, &(cuts[start_ind]));
//    }

    const bool useResolve = solver->isProvenOptimal();
    if (useResolve) {
      solver->resolve();
    } else {
      solver->initialSolve();
    }
    checkSolverOptimality(solver, false);

    // At this point it is useful to check for integer feasibility quickly
    // If we are in the subspace, then this is not necessary
    if (!param.paramVal[ParamIndices::SUBSPACE_PARAM_IND]) {
      if (!solver->isProvenOptimal()) {
        error_msg(errstr,
            "Not in subspace and solver not optimal after adding cuts, so we may have an integer infeasible problem. Exit status: %d.\n", solver->getModelPtr()->status());
#ifdef SAVE_INFEASIBLE_LP
        std::string infeas_f_name = GlobalVariables::out_f_name_stub + "_infeasible";
        solver->writeMps(infeas_f_name.c_str(), "mps", 1);
#endif
        if (param.getEXIT_ON_INFEAS()) {
          if (numCuts && cuts) {
            delete[] cuts;
          }
          writeErrorToLog(errstr, GlobalVariables::log_file);
          exit(1);
        }
      }
    }
    
    // Sometimes there are slight inaccuracies at the end that we can get rid of
    if (solver->isProvenOptimal() && solver->getObjValue() < compare_obj) {
      solver->resolve();
      checkSolverOptimality(solver, false);
    }

    if (solver->isProvenOptimal()) {
      boundByRound[round_ind] = solver->getObjValue();
      basisCond[round_ind] = compute_condition_number_norm2(solver);
    } else {
      for (int round_ind2 = round_ind; round_ind2 < numRounds; round_ind2++) {
        boundByRound[round_ind2] = solver->getInfinity(); // minimization problem, so this means infeas
        basisCond[round_ind2] = -1;
      }
      if (numCuts && cuts) {
        delete[] cuts;
      }
      return (solver->getInfinity()); // minimization problem, so this means infeas
    }
  } /* end iterating over rounds */

  if (numCuts && cuts) {
    delete[] cuts;
  }
  if (populateRoundWhenCutApplied) {
    for (int pos = 0; pos < numCuts; pos++) {
      (*oldRoundWhenCutApplied)[pos] = roundWhenCutApplied[pos];
    }
  }

#ifdef TRACE
  printf("Cuts applied in rounds: %d (of the %d possible).\n",
      solver->getNumRows() - numOrigRows, numCuts);
#endif
  return (solver->getObjValue());
} /* applyCutsInRounds (complex) */

/**
 * Get bound from applying
 * 1. SICs
 * 2. Tilted
 * 3. VPCs
 * 4. PHA
 * 5. SICs + Tilted
 * 6. SICs + VPCs
 * 7. SICs + PHA
 * 6. SICs + Tilted + VPCs
 * 7. SICs + Tilted + VPCs + PHA
 * 8. Tilted + VPCs + PHA
 */
void compareCuts(const int num_obj_tried,
    const OsiSolverInterface* const solver, AdvCuts& structSICs,
    AdvCuts& structGMICuts, AdvCuts& structLandPCuts, AdvCuts& structTilted,
    AdvCuts& structVPCs, AdvCuts& structPHAs,
    const double strongBranchingLB, const double strongBranchingUB,
    const int numCutsToAddPerRound, std::string& output) {
#ifdef TRACE
  printf("\n## Comparing cuts. ##\n");
#endif
  const int numSICs = structSICs.sizeCuts();
  const int numGMICuts = structGMICuts.sizeCuts();
  const int numLandPCuts = structLandPCuts.sizeCuts();
  const int numTilted = structTilted.sizeCuts();
  const int numVPCs = structVPCs.sizeCuts();
  const int numPHA = structPHAs.sizeCuts();
  const double lp_opt = solver->getObjValue();
  double sic_opt = 0.0, gmi_opt = 0.0, landp_opt = 0.0, tilted_opt = 0.0,
      vpc_opt = 0.0, pha_opt = 0.0;
  double sic_tilted_opt = 0.0, sic_vpc_opt = 0.0, sic_pha_opt = 0.0,
      sic_tilted_vpc_opt = 0.0, sic_tilted_vpc_pha_opt = 0.0;
  double gmi_tilted_opt = 0.0, gmi_vpc_opt = 0.0, gmi_pha_opt = 0.0,
      gmi_tilted_vpc_opt = 0.0, gmi_tilted_vpc_pha_opt = 0.0;
  double tilted_vpc_opt = 0.0, tilted_vpc_pha_opt = 0.0;
  std::vector<int> numCutsByRoundSIC, numCutsByRoundTilted, numCutsByRoundVPC,
      numCutsByRoundPHA;
  std::vector<double> boundByRoundSIC, boundByRoundTilted, boundByRoundVPC,
      boundByRoundPHA;
  std::vector<double> boundByRoundTiltedPlus, boundByRoundVPCPlus,
      boundByRoundPHAPlus;
  std::vector<double> basisCondByRoundSIC, basisCondByRoundTilted,
      basisCondByRoundVPC, basisCondByRoundPHA;
  std::vector<double> basisCondByRoundTiltedPlus, basisCondByRoundVPCPlus,
      basisCondByRoundPHAPlus;
  int max_rounds = 0;
  double minSICOrthogonalityInit = -1.;
  double maxSICOrthogonalityInit = -1.;
  double avgSICOrthogonalityInit = -1.;
  double minPHAOrthogonalityInit = -1.;
  double maxPHAOrthogonalityInit = -1.;
  double avgPHAOrthogonalityInit = -1.;
  double minVPCOrthogonalityInit = -1.;
  double maxVPCOrthogonalityInit = -1.;
  double avgVPCOrthogonalityInit = -1.;
  double minSICOrthogonalityFinal = -1.;
  double maxSICOrthogonalityFinal = -1.;
  double avgSICOrthogonalityFinal = -1.;
  double minPHAOrthogonalityFinal = -1.;
  double maxPHAOrthogonalityFinal = -1.;
  double avgPHAOrthogonalityFinal = -1.;
  double minVPCOrthogonalityFinal = -1.;
  double maxVPCOrthogonalityFinal = -1.;
  double avgVPCOrthogonalityFinal = -1.;
  double minOrthoWithObj = 1., maxOrthoWithObj = 0., totalOrthoWithObj = 0.,
      avgOrthoWithObj = 0.;

  // SICS
#ifdef TRACE
  if (numSICs > 0) {
    printf("\nGetting SICs bound.\n");
  } else {
    printf("\nNo SICs to apply.\n");
  }
#endif
  // TODO If in subspace should be SSICS_TIME
  GlobalVariables::timeStats.start_timer(APPLY_MSICS_TIME);
  PointCutsSolverInterface* SICSolver;
  SICSolver = dynamic_cast<PointCutsSolverInterface*>(solver->clone());
  setClpParameters(SICSolver);
  sic_opt = applyCuts(SICSolver, structSICs);
  PointCutsSolverInterface* copySICSolver = NULL;
  if (numSICs > 0) {
    copySICSolver = dynamic_cast<PointCutsSolverInterface*>(SICSolver->clone());
  }
  GlobalVariables::timeStats.end_timer(APPLY_MSICS_TIME);

  // GMI CUTS
#ifdef TRACE
  if (numGMICuts > 0) {
    printf("\nGetting GMI cuts bound.\n");
  }
#endif
  GlobalVariables::timeStats.start_timer(APPLY_GMI_TIME);
  PointCutsSolverInterface* GMISolver;
  GMISolver = dynamic_cast<PointCutsSolverInterface*>(solver->clone());
  setClpParameters(GMISolver);
  gmi_opt = applyCuts(GMISolver, structGMICuts);
  GlobalVariables::timeStats.end_timer(APPLY_GMI_TIME);

  // LANDP CUTS
 #ifdef TRACE
   if (numLandPCuts > 0) {
     printf("\nGetting lift-and-project cuts bound.\n");
   }
 #endif
   GlobalVariables::timeStats.start_timer(APPLY_LANDP_TIME);
   PointCutsSolverInterface* LandPSolver;
   LandPSolver = dynamic_cast<PointCutsSolverInterface*>(solver->clone());
   setClpParameters(LandPSolver);
   landp_opt = applyCuts(LandPSolver, structLandPCuts);
   GlobalVariables::timeStats.end_timer(APPLY_LANDP_TIME);

  // TILTED CUTS
  GlobalVariables::timeStats.start_timer(APPLY_TILTED_TIME);
  if (numTilted > 0) {
#ifdef TRACE
    printf("\nGetting tilted cuts bound.\n");
#endif
  }
  PointCutsSolverInterface* TiltedSolver;
  TiltedSolver = dynamic_cast<PointCutsSolverInterface*>(solver->clone());
  setClpParameters(TiltedSolver);
  std::vector<int> roundWhenCutAppliedTilted(numTilted);
  tilted_opt = applyCutsInRounds(TiltedSolver, structTilted,
      numCutsByRoundTilted, boundByRoundTilted, basisCondByRoundTilted,
      numCutsToAddPerRound, &(roundWhenCutAppliedTilted), true, 5, lp_opt);
  max_rounds = boundByRoundTilted.size();

  if (numTilted > 0) {
#ifdef TRACE
    printf("\nGetting SIC+Tilted bound.\n");
#endif
  }
  // Also add Tilted to SIC solver
  sic_tilted_opt = applyCutsInRounds(SICSolver, structTilted,
      numCutsByRoundTilted, boundByRoundTiltedPlus, basisCondByRoundTiltedPlus,
      numCutsToAddPerRound, &(roundWhenCutAppliedTilted), false);

  if (numTilted > 0) {
#ifdef TRACE
    printf("\nGetting GMI+Tilted bound.\n");
#endif
  }
  // Also add Tilted to GMI solver
  gmi_tilted_opt = applyCuts(GMISolver, structTilted);
  GlobalVariables::timeStats.end_timer(APPLY_TILTED_TIME);

  GlobalVariables::timeStats.start_timer(APPLY_VPC_TIME);
  if (numVPCs > 0) {
#ifdef TRACE
    printf("\nGetting VPCs bound.\n");
#endif
    PointCutsSolverInterface* VPCSolver;
    VPCSolver = dynamic_cast<PointCutsSolverInterface*>(solver->clone());
    setClpParameters(VPCSolver);
    std::vector<int> roundWhenCutAppliedVPC(numVPCs);
    vpc_opt = applyCutsInRounds(VPCSolver, structVPCs, numCutsByRoundVPC,
        boundByRoundVPC, basisCondByRoundVPC, numCutsToAddPerRound,
        &(roundWhenCutAppliedVPC), true);
#ifdef WRITE_LP_WITH_CUTS
    char filename[500];
    snprintf(filename, sizeof(filename) / sizeof(char), "%s%s",
        GlobalVariables::out_f_name_stub.c_str(), "WithVPCs");
    VPCSolver->writeMps(filename, "mps", VPCSolver->getObjSense());
#endif

//    if (structVPCs.sizeCuts() <= 200 || param.getTEMP() == 3) {
    if (param.getTEMP() == 3) {
      // Orthogonality checking can be expensive, so let's limit when it happens
      // Get max orthogonality for round 1
      getOrthogonalityStatsForRound(structVPCs, minVPCOrthogonalityInit,
          maxVPCOrthogonalityInit, avgVPCOrthogonalityInit,
          &roundWhenCutAppliedVPC, 0);
      // Get max orthogonality for all VPCs
      getOrthogonalityStatsForRound(structVPCs, minVPCOrthogonalityFinal,
          maxVPCOrthogonalityFinal, avgVPCOrthogonalityFinal);
    }

    if (numGMICuts > 0) {
#ifdef TRACE
      printf("\nGetting GMI+VPCs bound.\n");
#endif
      gmi_vpc_opt = applyCuts(VPCSolver, structGMICuts);
    } else {
      gmi_vpc_opt = vpc_opt;
    }

    if (numTilted > 0) {
#ifdef TRACE
      printf("\nGetting Tilted+VPCs bound.\n");
#endif
    }
    // Also apply VPCs to TiltedSolver (even if there were no tilted cuts, to have this value)
    tilted_vpc_opt = applyCuts(TiltedSolver, structVPCs);

    if (numSICs > 0) {
#ifdef TRACE
      printf("\nGetting SIC+VPCs bound.\n");
#endif
      sic_vpc_opt = applyCutsInRounds(copySICSolver, structVPCs,
          numCutsByRoundVPC, boundByRoundVPCPlus, basisCondByRoundVPCPlus,
          numCutsToAddPerRound, &(roundWhenCutAppliedVPC), false);
      copySICSolver->restoreBaseModel(solver->getNumRows() + numSICs);
      if (max_rounds < (int) boundByRoundVPCPlus.size()) {
        max_rounds = boundByRoundVPCPlus.size();
      }
    } else {
      sic_vpc_opt = vpc_opt;
    }

    if (numSICs > 0 && numTilted > 0) {
#ifdef TRACE
      printf("\nGetting SIC+Tilted+VPCs bound.\n");
#endif
    }
    // Also apply VPCs to SIC+Tilted solver (even if there are none of these other cuts)
    sic_tilted_vpc_opt = applyCuts(SICSolver, structVPCs);

    if (numGMICuts > 0 && numTilted > 0) {
#ifdef TRACE
      printf("\nGetting GMI+Tilted+VPCs bound.\n");
#endif
    }
    // Also apply VPCs to GMI solver
    gmi_tilted_vpc_opt = applyCuts(GMISolver, structVPCs);

    if (VPCSolver) {
      delete VPCSolver;
    }

    // Get min/max/avg orthogonality of cuts with objective cut
    for (int cut_ind = 0; cut_ind < numVPCs; cut_ind++) {
      const CoinPackedVector cut_row = structVPCs.rowCutPtr(cut_ind)->row();
      const double curr_ortho = getOrthogonality(cut_row, solver->getNumCols(), solver->getObjCoefficients());
      if (curr_ortho < minOrthoWithObj) {
        minOrthoWithObj = curr_ortho;
      }
      if (curr_ortho > maxOrthoWithObj) {
        maxOrthoWithObj = curr_ortho;
      }
      totalOrthoWithObj += curr_ortho;
    }
    avgOrthoWithObj = totalOrthoWithObj / numVPCs;
  } else {
    vpc_opt = lp_opt;
    sic_vpc_opt = sic_opt;
    gmi_vpc_opt = gmi_opt;
    sic_tilted_vpc_opt = sic_tilted_opt;
    gmi_tilted_vpc_opt = gmi_tilted_opt;
    tilted_vpc_opt = tilted_opt;
  }
  GlobalVariables::timeStats.end_timer(APPLY_VPC_TIME);

  GlobalVariables::timeStats.start_timer(APPLY_MPHA_TIME);
  if (numPHA > 0) {
#ifdef TRACE
    printf("\nGetting PHA bound.\n");
#endif
    PointCutsSolverInterface* PHASolver;
    PHASolver = dynamic_cast<PointCutsSolverInterface*>(solver->clone());
    setClpParameters(PHASolver);
    std::vector<int> roundWhenCutAppliedPHA(numPHA);
    pha_opt = applyCutsInRounds(PHASolver, structPHAs, numCutsByRoundPHA,
        boundByRoundPHA, basisCondByRoundPHA, numCutsToAddPerRound,
        &(roundWhenCutAppliedPHA), true);

    // Get max orthogonality for round 1
    getOrthogonalityStatsForRound(structPHAs,
        minPHAOrthogonalityInit, maxPHAOrthogonalityInit,
        avgPHAOrthogonalityInit, &roundWhenCutAppliedPHA, 0);
    // Get max orthogonality for all PHA cuts
    getOrthogonalityStatsForRound(structPHAs,
        minPHAOrthogonalityFinal, maxPHAOrthogonalityFinal,
        avgPHAOrthogonalityFinal);

    if (numGMICuts > 0) {
#ifdef TRACE
      printf("\nGetting GMI+PHA bound.\n");
#endif
      gmi_pha_opt = applyCuts(PHASolver, structGMICuts);
    } else {
      gmi_pha_opt = pha_opt;
    }
    if (PHASolver) {
      delete PHASolver;
    }
    
    if (numSICs > 0) {
#ifdef TRACE
      printf("\nGetting SIC+PHA bound.\n");
#endif
      sic_pha_opt = applyCutsInRounds(copySICSolver, structPHAs,
          numCutsByRoundPHA, boundByRoundPHAPlus, basisCondByRoundPHAPlus,
          numCutsToAddPerRound, &(roundWhenCutAppliedPHA), false);
      copySICSolver->restoreBaseModel(solver->getNumRows() + numSICs);
      if (max_rounds < (int) boundByRoundPHAPlus.size()) {
        max_rounds = boundByRoundPHAPlus.size();
      }
    } else {
      sic_pha_opt = pha_opt;
    }

#ifdef TRACE
    printf("\nGetting SIC+Tilted+VPCs+PHA bound.\n");
#endif
    // Also apply PHAs to SIC solver
    sic_tilted_vpc_pha_opt = applyCuts(SICSolver, structPHAs);

#ifdef TRACE
    printf("\nGetting GMI+Tilted+VPCs+PHA bound.\n");
#endif
    // Also apply VPCs to GMI solver
    gmi_tilted_vpc_pha_opt = applyCuts(GMISolver, structPHAs);

#ifdef TRACE
    printf("\nGetting Tilted+VPCs+PHA bound.\n");
#endif
    // Also apply VPCs to Tilted solver
    tilted_vpc_pha_opt = applyCuts(TiltedSolver, structPHAs);
  } else {
    pha_opt = lp_opt;
    sic_pha_opt = sic_opt;
    gmi_pha_opt = gmi_opt;
    sic_tilted_vpc_pha_opt = sic_tilted_vpc_opt;
    gmi_tilted_vpc_pha_opt = gmi_tilted_vpc_opt;
    tilted_vpc_pha_opt = tilted_vpc_opt;
  }
  GlobalVariables::timeStats.end_timer(APPLY_MPHA_TIME);

  // Get number rounds of SICs needed to meet bound from GICs+SICs
#ifdef TRACE
  printf("\nGetting number rounds of SICs req'd to get bound.\n");
#endif
  int total_num_sics = 0;
  double final_sic_bound = 0.;
  const int min_sic_rounds = (param.getParamVal(ParamIndices::STRENGTHEN_PARAM_IND) == 2) ? 2 : 0;
  int num_sic_rounds = 0;
  if (numSICs > 0 && (param.getTEMP() == 2 || param.getParamVal(ParamIndices::STRENGTHEN_PARAM_IND) >= 2)) {
    copySICSolver->resolve();
    checkSolverOptimality(copySICSolver, false);
    double curr_sic_opt = sic_opt;
    num_sic_rounds++;
    boundByRoundSIC.push_back(sic_opt);
    basisCondByRoundSIC.push_back(
        compute_condition_number_norm2(copySICSolver));
    numCutsByRoundSIC.push_back(numSICs);
    getOrthogonalityStatsForRound(structSICs, minSICOrthogonalityInit,
        maxSICOrthogonalityInit, avgSICOrthogonalityInit);
    total_num_sics += numSICs;
    OsiCuts allSICs = structSICs;
    while (num_sic_rounds < min_sic_rounds || 
        (lessThanVal(curr_sic_opt, sic_tilted_vpc_pha_opt)
          && total_num_sics < numSICs + numTilted + numVPCs + numPHA)) {
      AdvCuts currStructSICs;
      CglGICParam tmpParam(param);
      tmpParam.setParamVal(ParamIndices::MAX_FRAC_VAR_PARAM_IND, 0);
      CglSIC SICGen(tmpParam);
      SICGen.generateCuts(*copySICSolver, currStructSICs);
      // Check if no SICs generated
      if (currStructSICs.sizeCuts() == 0) {
        break;
      }

      num_sic_rounds++;
      curr_sic_opt = applyCuts(copySICSolver, currStructSICs);
      boundByRoundSIC.push_back(curr_sic_opt);
      basisCondByRoundSIC.push_back(
          compute_condition_number_norm2(copySICSolver));
      numCutsByRoundSIC.push_back(currStructSICs.sizeCuts());
      total_num_sics += currStructSICs.sizeCuts();
      allSICs.insert(currStructSICs);

      // Other stopping conditions:
      // Bound does not improve at all after one round
      if (!greaterThanVal(curr_sic_opt, boundByRoundSIC[num_sic_rounds - 2])) {
        break;
      }
      // Bound does not significantly improve after five rounds
      if (num_sic_rounds > 4) {
        const double delta = curr_sic_opt - boundByRoundSIC[num_sic_rounds - 4];
        if (!greaterThanVal(delta, 1e-3)) {
          break;
        }
      }
    } /* one round of Gomory cuts */
    final_sic_bound = copySICSolver->getObjValue();
    getOrthogonalityStatsForRound(allSICs, minSICOrthogonalityFinal,
        maxSICOrthogonalityFinal, avgSICOrthogonalityFinal);
    if (max_rounds < num_sic_rounds) {
      max_rounds = boundByRoundSIC.size();
    }
  } /* add Gomory cuts in rounds */
      
  // After adding all cuts, count active cuts
//#ifdef TRACE
//  printf("\nCounting number of active cuts.\n");
//#endif
  std::vector<int> active_cut_heur_sicsolver(CutHeuristics::NUM_CUT_HEUR, 0);
//  int num_active_sics_sicsolver = 0;
//  int num_active_closedfpcs_sicsolver = 0;
  int num_active_vpcs_sicsolver = 0;
  int num_active_pha_sicsolver = 0;

  std::vector<int> active_cut_heur_gmisolver(CutHeuristics::NUM_CUT_HEUR, 0);
//  int num_active_gmi_gmisolver = 0;
//  int num_active_closedfpcs_gmisolver = 0;
  int num_active_vpcs_gmisolver = 0;
  int num_active_pha_gmisolver = 0;
  if (!isInfinity(tilted_vpc_pha_opt)) {
    // Count number of active SICs
    for (int cut_ind = 0; cut_ind < structSICs.sizeCuts(); cut_ind++) {
      const double activity = dotProduct(structSICs[cut_ind].row(),
          SICSolver->getColSolution());
      if (isVal(activity, structSICs[cut_ind].rhs())) {
//        num_active_sics_sicsolver++;
        active_cut_heur_sicsolver[CutHeuristics::SIC_CUT_GEN]++;
      }
    }

    // Count number of active GMI cuts
    for (int cut_ind = 0; cut_ind < structGMICuts.sizeCuts(); cut_ind++) {
      const double activity = dotProduct(
          structGMICuts.rowCutPtr(cut_ind)->row(), GMISolver->getColSolution());
      if (isVal(activity, structGMICuts.rowCutPtr(cut_ind)->rhs())) {
//        num_active_gmi_gmisolver++;
        active_cut_heur_gmisolver[CutHeuristics::GMI_CUT_GEN]++;
      }
    }

    // Count number of active tilted cuts
    for (int cut_ind = 0; cut_ind < structTilted.sizeCuts(); cut_ind++) {
      if (numSICs > 0) {
        const double activity = dotProduct(structTilted[cut_ind].row(),
            SICSolver->getColSolution());
        if (isVal(activity, structTilted[cut_ind].rhs())) {
//          num_active_closedfpcs_sicsolver++;
          active_cut_heur_sicsolver[CutHeuristics::TILTED_GEN]++;
        }
      }
      if (numGMICuts > 0) {
        const double activity = dotProduct(structTilted[cut_ind].row(),
            GMISolver->getColSolution());
        if (isVal(activity, structTilted[cut_ind].rhs())) {
//          num_active_closedfpcs_gmisolver++;
          active_cut_heur_gmisolver[CutHeuristics::TILTED_GEN]++;
        }
      }
    }
    
    // Count number of active VPCs
    for (int cut_ind = 0; cut_ind < structVPCs.sizeCuts(); cut_ind++) {
      if (numSICs > 0) {
        const double activity = dotProduct(structVPCs[cut_ind].row(),
            SICSolver->getColSolution());
        if (isVal(activity, structVPCs[cut_ind].rhs())) {
          num_active_vpcs_sicsolver++;
          active_cut_heur_sicsolver[structVPCs[cut_ind].cutHeur]++;
        }
      }
      if (numGMICuts > 0) {
        const double activity = dotProduct(structVPCs[cut_ind].row(),
            GMISolver->getColSolution());
        if (isVal(activity, structVPCs[cut_ind].rhs())) {
          num_active_vpcs_gmisolver++;
          active_cut_heur_gmisolver[structVPCs[cut_ind].cutHeur]++;
        }
      }
    }
    
    // Count number of active PHA
    for (int cut_ind = 0; cut_ind < structPHAs.sizeCuts(); cut_ind++) {
      if (numSICs > 0) {
        const double activity = dotProduct(structPHAs[cut_ind].row(),
            SICSolver->getColSolution());
        if (isVal(activity, structPHAs[cut_ind].rhs())) {
          num_active_pha_sicsolver++;
          active_cut_heur_sicsolver[structPHAs[cut_ind].cutHeur]++;
        }
      }
      if (numGMICuts > 0) {
        const double activity = dotProduct(structPHAs[cut_ind].row(),
            GMISolver->getColSolution());
        if (isVal(activity, structPHAs[cut_ind].rhs())) {
          num_active_pha_gmisolver++;
          active_cut_heur_gmisolver[structPHAs[cut_ind].cutHeur]++;
        }
      }
    }
  }

  char tmpstring[300];
  sprintf(tmpstring, "\n## Results from adding cuts: ##\n");
  output += tmpstring;
  const int NAME_WIDTH = 25;
  const int NUM_DIGITS_BEFORE_DEC = 7;
  const int NUM_DIGITS_AFTER_DEC = 7;
  sprintf(tmpstring, "%-*.*s% -*.*f\n", NAME_WIDTH, NAME_WIDTH, "Original solver: ",
      NUM_DIGITS_BEFORE_DEC, NUM_DIGITS_AFTER_DEC, solver->getObjValue());
  output += tmpstring;
  if (!isInfinity(strongBranchingLB)) {
    sprintf(tmpstring, "%-*.*s% -*.*f\n", NAME_WIDTH, NAME_WIDTH,
        "Strong branching lb: ", NUM_DIGITS_BEFORE_DEC, NUM_DIGITS_AFTER_DEC,
        strongBranchingLB);
    output += tmpstring;
  }
  if (!isNegInfinity(strongBranchingUB)) {
    sprintf(tmpstring, "%-*.*s% -*.*f\n", NAME_WIDTH, NAME_WIDTH,
        "Strong branching ub: ", NUM_DIGITS_BEFORE_DEC, NUM_DIGITS_AFTER_DEC,
        strongBranchingUB);
    output += tmpstring;
  }
  if (numSICs > 0 && !isInfinity(sic_opt)) {
    sprintf(tmpstring, "%-*.*s% -*.*f (%d cuts)\n", NAME_WIDTH, NAME_WIDTH, "SICs: ",
        NUM_DIGITS_BEFORE_DEC, NUM_DIGITS_AFTER_DEC, sic_opt,
        numSICs);
    output += tmpstring;
  }
  if (numGMICuts > 0 && !isInfinity(gmi_opt)) {
    sprintf(tmpstring, "%-*.*s% -*.*f (%d cuts)\n", NAME_WIDTH, NAME_WIDTH, "GMI: ",
        NUM_DIGITS_BEFORE_DEC, NUM_DIGITS_AFTER_DEC, gmi_opt, numGMICuts);
    output += tmpstring;
  }
  if (numLandPCuts > 0 && !isInfinity(landp_opt)) {
    sprintf(tmpstring, "%-*.*s% -*.*f (%d cuts)\n", NAME_WIDTH, NAME_WIDTH, "LandP: ",
        NUM_DIGITS_BEFORE_DEC, NUM_DIGITS_AFTER_DEC, landp_opt, numLandPCuts);
    output += tmpstring;
  }
  if (numTilted > 0 && !isInfinity(tilted_opt)) {
    sprintf(tmpstring, "%-*.*s% -*.*f (%d cuts)\n", NAME_WIDTH, NAME_WIDTH, "Tilted: ",
        NUM_DIGITS_BEFORE_DEC, NUM_DIGITS_AFTER_DEC, tilted_opt,
        numTilted);
    output += tmpstring;
  }
  if (numVPCs > 0 && !isInfinity(vpc_opt)) {
    sprintf(tmpstring, "%-*.*s% -*.*f (%d cuts)\n", NAME_WIDTH, NAME_WIDTH, "VPCs: ",
        NUM_DIGITS_BEFORE_DEC, NUM_DIGITS_AFTER_DEC, vpc_opt,
        numVPCs);
    output += tmpstring;
  }
  if (numPHA > 0 && !isInfinity(pha_opt)) {
    sprintf(tmpstring, "%-*.*s% -*.*f (%d cuts)\n", NAME_WIDTH, NAME_WIDTH, "PHA: ",
        NUM_DIGITS_BEFORE_DEC, NUM_DIGITS_AFTER_DEC, pha_opt,
        numPHA);
    output += tmpstring;
  }
  if (numSICs > 0 && !isInfinity(sic_opt)) {
    if (numTilted > 0 && !isInfinity(tilted_opt)) {
      sprintf(tmpstring, "%-*.*s% -*.*f (%d cuts)\n", NAME_WIDTH, NAME_WIDTH,
          "Tilted+SICs: ", NUM_DIGITS_BEFORE_DEC, NUM_DIGITS_AFTER_DEC,
          sic_tilted_opt, numSICs + numTilted);
      output += tmpstring;
    }
    if (numVPCs > 0 && !isInfinity(vpc_opt)) {
      sprintf(tmpstring, "%-*.*s% -*.*f (%d cuts)\n", NAME_WIDTH, NAME_WIDTH,
          "VPCs+SICs: ", NUM_DIGITS_BEFORE_DEC, NUM_DIGITS_AFTER_DEC,
          sic_vpc_opt, numSICs + numVPCs);
      output += tmpstring;
    }
  }
  if (numGMICuts > 0 && !isInfinity(gmi_opt)) {
    if (numTilted > 0 && !isInfinity(tilted_opt)) {
      sprintf(tmpstring, "%-*.*s% -*.*f (%d cuts)\n", NAME_WIDTH, NAME_WIDTH,
          "Tilted+GMI: ", NUM_DIGITS_BEFORE_DEC, NUM_DIGITS_AFTER_DEC,
          gmi_tilted_opt, numGMICuts + numTilted);
      output += tmpstring;
    }
    if (numVPCs > 0 && !isInfinity(vpc_opt)) {
      sprintf(tmpstring, "%-*.*s% -*.*f (%d cuts)\n", NAME_WIDTH, NAME_WIDTH, "VPCs+GMI: ",
          NUM_DIGITS_BEFORE_DEC, NUM_DIGITS_AFTER_DEC, gmi_vpc_opt,
          numGMICuts + numVPCs);
      output += tmpstring;
    }
  }
  if (isInfinity(tilted_vpc_pha_opt)) {
    sprintf(tmpstring, "%-*.*s+inf (%d cuts)\n", NAME_WIDTH, NAME_WIDTH,
        "*** All but SICs/GMI: ", numTilted + numVPCs + numPHA);
    output += tmpstring;
  } else {
    sprintf(tmpstring, "%-*.*s% -*.*f (%d cuts)\n", NAME_WIDTH, NAME_WIDTH,
        "*** All but SICs/GMI: ", NUM_DIGITS_BEFORE_DEC, NUM_DIGITS_AFTER_DEC,
        tilted_vpc_pha_opt, numTilted + numVPCs + numPHA);
    output += tmpstring;
  }

  if (numSICs > 0) {
    if (isInfinity(sic_tilted_vpc_pha_opt)) {
      sprintf(tmpstring, "%-*.*s+inf (%d cuts", NAME_WIDTH, NAME_WIDTH,
          "*** All cuts with SICs:: ", numSICs + numTilted + numVPCs + numPHA);
      output += tmpstring;
    } else {
      sprintf(tmpstring, "%-*.*s% -*.*f (%d cuts", NAME_WIDTH, NAME_WIDTH,
          "*** All cuts with SICs: ", NUM_DIGITS_BEFORE_DEC,
          NUM_DIGITS_AFTER_DEC, sic_tilted_vpc_pha_opt,
          numSICs + numTilted + numVPCs + numPHA);
      output += tmpstring;
    }
    if (numSICs > 0) {
      sprintf(tmpstring, ", %d / %d active SICs", active_cut_heur_sicsolver[CutHeuristics::SIC_CUT_GEN], numSICs);
      output += tmpstring;
    }
    if (numTilted > 0) {
      sprintf(tmpstring, ", %d / %d active Tilted", active_cut_heur_sicsolver[CutHeuristics::TILTED_GEN],
          numTilted);
      output += tmpstring;
    }
    if (numVPCs > 0) {
      sprintf(tmpstring, ", %d / %d active VPCs", num_active_vpcs_sicsolver, numVPCs);
      output += tmpstring;
    }
    if (numPHA > 0) {
      sprintf(tmpstring, ", %d / %d active PHA", num_active_pha_sicsolver, numPHA);
      output += tmpstring;
    }
    sprintf(tmpstring, ")\n");
    output += tmpstring;
  } /* if numSICs > 0 */

  if (numGMICuts > 0) {
    if (isInfinity(gmi_tilted_vpc_pha_opt)) {
      sprintf(tmpstring, "%-*.*s+inf (%d cuts", NAME_WIDTH, NAME_WIDTH,
          "*** All cuts with GMI:: ", numGMICuts + numTilted + numVPCs + numPHA);
      output += tmpstring;
    } else {
      sprintf(tmpstring, "%-*.*s% -*.*f (%d cuts", NAME_WIDTH, NAME_WIDTH,
          "*** All cuts with GMI: ", NUM_DIGITS_BEFORE_DEC,
          NUM_DIGITS_AFTER_DEC, gmi_tilted_vpc_pha_opt,
          numGMICuts + numTilted + numVPCs + numPHA);
      output += tmpstring;
    }
    if (numGMICuts > 0) {
      sprintf(tmpstring, ", %d / %d active GMI cuts",
          active_cut_heur_gmisolver[CutHeuristics::GMI_CUT_GEN], numGMICuts);
      output += tmpstring;
    }
    if (numTilted > 0) {
      sprintf(tmpstring, ", %d / %d active Tilted",
          active_cut_heur_gmisolver[CutHeuristics::TILTED_GEN],
          numTilted);
      output += tmpstring;
    }
    if (numVPCs > 0) {
      sprintf(tmpstring, ", %d / %d active VPCs", num_active_vpcs_gmisolver, numVPCs);
      output += tmpstring;
    }
    if (numPHA > 0) {
      sprintf(tmpstring, ", %d / %d active PHA", num_active_pha_gmisolver, numPHA);
      output += tmpstring;
    }
    sprintf(tmpstring, ")\n");
    output += tmpstring;
  } /* if numGMICuts > 0 */

  if (!isInfinity(GlobalVariables::bestObjValue)) {
    sprintf(tmpstring, "%-*.*s% -*.*f\n", NAME_WIDTH, NAME_WIDTH, "IP: ",
        NUM_DIGITS_BEFORE_DEC, NUM_DIGITS_AFTER_DEC,
        GlobalVariables::bestObjValue);
    output += tmpstring;
  }

  // BOUND BY ROUND
  if (max_rounds > 0) {
//    const std::vector<int> numCutsPerGenerator { numSICs, numGMICuts,
//        numLandPCuts, numTilted, numVPCs, numPHA };
//    const int max_num_rounds = *std::max_element(numCutsPerGenerator.begin(),
//        numCutsPerGenerator.end()) / numCutsToAddPerRound;
    printf("\n## Bound by round ##\n");
    const int COL_WIDTH = 10
        + sizeof(stringValue(solver->getObjValue()).c_str()) / sizeof(char);
    printf("%-*.*s", 6, 6, "Round");
    if (numSICs > 0) {
      printf("%-*.*s", COL_WIDTH, COL_WIDTH, "SIC");
    }
    if (numGMICuts > 0) {
      printf("%-*.*s", COL_WIDTH, COL_WIDTH, "GMI");
    }
    if (numLandPCuts > 0) {
      printf("%-*.*s", COL_WIDTH, COL_WIDTH, "LandP");
    }
    if (numTilted > 0) {
      printf("%-*.*s", COL_WIDTH, COL_WIDTH, "Tilted");
    }
    if (numTilted > 0 && numSICs > 0) {
      printf("%-*.*s", COL_WIDTH, COL_WIDTH, "Tilted+");
    }
    if (numVPCs > 0) {
      printf("%-*.*s", COL_WIDTH, COL_WIDTH, "VPC");
    }
    if (numVPCs > 0 && numSICs > 0) {
      printf("%-*.*s", COL_WIDTH, COL_WIDTH, "VPC+");
    }
    if (numPHA > 0) {
      printf("%-*.*s", COL_WIDTH, COL_WIDTH, "PHA");
    }
    if (numPHA > 0 && numSICs > 0) {
      printf("%-*.*s", COL_WIDTH, COL_WIDTH, "PHA+");
    }
    printf("\n");
    for (int round_ind = 0; round_ind < max_rounds; round_ind++) {
      printf("%-*.*s", 6, 6, stringValue(round_ind).c_str());
      if (round_ind < (int) boundByRoundSIC.size()) {
        const std::string tmpstring = stringValue(boundByRoundSIC[round_ind],
            "%.2f") + "(" + stringValue(numCutsByRoundSIC[round_ind]) + ")";
        printf("%-*.*s", COL_WIDTH, COL_WIDTH, tmpstring.c_str());
      } else if (numSICs > 0) {
        printf("%-*.*s", COL_WIDTH, COL_WIDTH, "");
      }
      if (round_ind < numGMICuts / numCutsToAddPerRound) {
        printf("%-*.*s", COL_WIDTH, COL_WIDTH, stringValue(gmi_opt, "%.2f").c_str());
      } else if (numGMICuts > 0) {
        printf("%-*.*s", COL_WIDTH, COL_WIDTH, "");
      }
      if (round_ind < numLandPCuts / numCutsToAddPerRound) {
        printf("%-*.*s", COL_WIDTH, COL_WIDTH, stringValue(landp_opt, "%.2f").c_str());
      } else if (numLandPCuts > 0) {
        printf("%-*.*s", COL_WIDTH, COL_WIDTH, "");
      }
      if (round_ind < (int) boundByRoundTilted.size()) {
        printf("%-*.*s", COL_WIDTH, COL_WIDTH,
            stringValue(boundByRoundTilted[round_ind], "%.2f").c_str());
      } else if (numTilted > 0) {
        printf("%-*.*s", COL_WIDTH, COL_WIDTH, "");
      }
      if (round_ind < (int) boundByRoundTiltedPlus.size()) {
        printf("%-*.*s", COL_WIDTH, COL_WIDTH,
            stringValue(boundByRoundTiltedPlus[round_ind], "%.2f").c_str());
      } else if (numTilted > 0 && numSICs > 0) {
        printf("%-*.*s", COL_WIDTH, COL_WIDTH, "");
      }
      if (round_ind < (int) boundByRoundVPC.size()) {
        printf("%-*.*s", COL_WIDTH, COL_WIDTH,
            stringValue(boundByRoundVPC[round_ind], "%.2f").c_str());
      } else if (numVPCs > 0) {
        printf("%-*.*s", COL_WIDTH, COL_WIDTH, "");
      }
      if (round_ind < (int) boundByRoundVPCPlus.size()) {
        printf("%-*.*s", COL_WIDTH, COL_WIDTH,
            stringValue(boundByRoundVPCPlus[round_ind], "%.2f").c_str());
      } else if (numVPCs > 0 && numSICs > 0) {
        printf("%-*.*s", COL_WIDTH, COL_WIDTH, "");
      }
      if (round_ind < (int) boundByRoundPHA.size()) {
        printf("%-*.*s", COL_WIDTH, COL_WIDTH,
            stringValue(boundByRoundPHA[round_ind], "%.2f").c_str());
      } else if (numPHA > 0) {
        printf("%-*.*s", COL_WIDTH, COL_WIDTH, "");
      }
      if (round_ind < (int) boundByRoundPHAPlus.size()) {
        printf("%-*.*s", COL_WIDTH, COL_WIDTH,
            stringValue(boundByRoundPHAPlus[round_ind], "%.2f").c_str());
      } else if (numPHA > 0 && numSICs > 0) {
        printf("%-*.*s", COL_WIDTH, COL_WIDTH, "");
      }
      printf("\n");
    } /* iterate over the rounds */
  } /* print bound by round */

  // Also print if we improved
  // Recall everything has been standardized to be a minimization problem
  // Thus, larger objective means we have closed more gap
  printf("\n## Improvements: ##\n");
  bool PHAOverSICs = greaterThanVal(tilted_vpc_pha_opt, sic_opt);
  bool allOverSICs = greaterThanVal(sic_tilted_vpc_pha_opt, sic_opt);
  if (numSICs == 0) {
    printf("No SICs were generated.\n");
  } else if (PHAOverSICs || allOverSICs) {
    if (PHAOverSICs) {
      printf("New cuts > SICs by %f.\n", tilted_vpc_pha_opt - sic_opt);
    }
    if (allOverSICs) {
      printf("All cuts (with SICs) > SICs by %f.\n",
          sic_tilted_vpc_pha_opt - sic_opt);
    }
  } else {
    printf("No improvements over SICs.\n");
  }

  bool PHAOverGMICuts = greaterThanVal(tilted_vpc_pha_opt, gmi_opt);
  bool allOverGMICuts = greaterThanVal(gmi_tilted_vpc_pha_opt, gmi_opt);
  if (numGMICuts == 0) {
    if (param.getParamVal(ParamIndices::GMI_PARAM_IND) == 1) {
      printf("No GMI cuts were generated.\n");
    }
  } else if (PHAOverGMICuts || allOverGMICuts) {
    if (PHAOverGMICuts) {
      printf("New cuts > GMI cuts by %f.\n", tilted_vpc_pha_opt - gmi_opt);
    }
    if (allOverGMICuts) {
      printf("All cuts (with GMI cuts) > GMI cuts by %f.\n",
          gmi_tilted_vpc_pha_opt - gmi_opt);
    }
  } else {
    printf("No improvements over GMI cuts.\n");
  }

  if ((param.getParamVal(ParamIndices::PHA_PARAM_IND) > 0)
      || (param.getParamVal(ParamIndices::VPC_DEPTH_PARAM_IND) > 0)) {
    printf("\n## Cut summary: ##\n");
    // Report how many cut-generating sets had one facet infeasible
    printf("%d/%d cut-generating sets had one term infeasible.\n",
        GlobalVariables::numCgsOneDisjTermInfeas,
        param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND));
//  // Report how many cut-generating sets were not used (not objectives were tried, due to time or cut limit reached)
//  printf("%d/%d cut-generating sets had zero objectives attempted (cut or time limit reached) or were primal infeasible.\n",
//      param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND)
//          - GlobalVariables::numCgsActuallyUsed,
//      param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND));
    // Report how many cut-generating sets led to cuts
    printf("%d/%d cut-generating sets led to new cuts.\n",
        GlobalVariables::numCgsLeadingToCuts,
        param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND));
    // Report how many objectives were tried
    // Only relevant for VPCs and GICs
    printf(
        "Generated %d cuts (%d VPCs, %d PHA) from %d objectives tried "
            "(on the %d/%d cut-generating sets with feasible point-ray collections and objectives actually tried).\n",
        numVPCs + numPHA, numVPCs, numPHA,
        num_obj_tried
            - GlobalVariables::numCutSolverFails[CutSolverFails::PRIMAL_CUTSOLVER_FAIL_IND],
        GlobalVariables::numCgsActuallyUsed,
        param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND));
  } /* info about the different cut-generating sets used */

  // Also print the which heuristic had active cuts
  printf("\n## Active cuts from which heuristics: ##\n");
  for (int heur_ind = 0; heur_ind < CutHeuristics::NUM_CUT_HEUR; heur_ind++) {
    printf("%s:", CutHeuristicsName[heur_ind].c_str());
    if (numSICs > 0) {
      printf(" [SICSOLVER %d]", active_cut_heur_sicsolver[heur_ind]);
    }
    if (numGMICuts > 0) {
      printf(" [GMISOLVER %d]", active_cut_heur_gmisolver[heur_ind]);
    }
    if (numSICs == 0 && numGMICuts == 0) {
      printf(" 0");
    }
    printf(" (of %d, from %d objectives)\n",
        GlobalVariables::numCutsFromHeur[heur_ind],
        GlobalVariables::numObjFromHeur[heur_ind]);
  } /* get active cut stats */

  // Also save the min/max/avg/stddev of the cut support for SICs
  int minSupportSICs = 0, maxSupportSICs = 0;
  int totalSupportSICs = 0.;
  double stdDevSupportSICs = 0.;
  for (int cut_ind = 0; cut_ind < numSICs; cut_ind++) {
    const int currSupport = structSICs.getCutPointer(cut_ind)->row().getNumElements();
    totalSupportSICs += currSupport;
    stdDevSupportSICs += currSupport * currSupport;
    if (cut_ind == 0) {
      maxSupportSICs = currSupport;
      minSupportSICs = currSupport;
    } else {
      if (minSupportSICs > currSupport) {
        minSupportSICs = currSupport;
      }
      if (maxSupportSICs < currSupport) {
        maxSupportSICs = currSupport;
      }
    }
  } /* get cut support stats SICs */
  const double avgSupportSICs =
      (numSICs > 0) ? (double) totalSupportSICs / numSICs : 0;
  if (numSICs > 0) {
    stdDevSupportSICs /= numSICs;
    stdDevSupportSICs = std::sqrt(stdDevSupportSICs - (avgSupportSICs * avgSupportSICs));
  }

  // Also save the min/max/avg/stddev of the cut support for VPCs
  int minSupportVPCs = 0, maxSupportVPCs = 0;
  int totalSupportVPCs = 0.;
  double stdDevSupportVPCs = 0.;
  for (int cut_ind = 0; cut_ind < numVPCs; cut_ind++) {
    const int currSupport = structVPCs.getCutPointer(cut_ind)->row().getNumElements();
    totalSupportVPCs += currSupport;
    stdDevSupportVPCs += currSupport * currSupport;
    if (cut_ind == 0) {
      minSupportVPCs = currSupport;
      maxSupportVPCs = currSupport;
    } else {
      if (minSupportVPCs > currSupport) {
        minSupportVPCs = currSupport;
      }
      if (maxSupportVPCs < currSupport) {
        maxSupportVPCs = currSupport;
      }
    }
  } /* get cut support stats VPCs */
  const double avgSupportVPCs =
      (numVPCs > 0) ? (double) totalSupportVPCs / numVPCs : 0;
  if (numVPCs > 0) {
    stdDevSupportVPCs /= numVPCs;
    stdDevSupportVPCs = std::sqrt(
        stdDevSupportVPCs - (avgSupportVPCs * avgSupportVPCs));
  }

  // Save all this to the log file
  writeCutInfoToLog(numSICs + numGMICuts + numTilted + numVPCs + numPHA,
      minSupportSICs, maxSupportSICs, avgSupportSICs, stdDevSupportSICs,
      minSupportVPCs, maxSupportVPCs, avgSupportVPCs, stdDevSupportVPCs,
      minOrthoWithObj, maxOrthoWithObj, avgOrthoWithObj,
      num_obj_tried, active_cut_heur_sicsolver, active_cut_heur_gmisolver,
      GlobalVariables::log_file);
  writeRootBoundToLog(solver->getObjValue(), strongBranchingLB,
      strongBranchingUB, numSICs, sic_opt, numGMICuts, gmi_opt, numLandPCuts,
      landp_opt, numTilted, tilted_opt, numVPCs, vpc_opt, numPHA, pha_opt,
      sic_tilted_opt, gmi_tilted_opt, sic_vpc_opt, gmi_vpc_opt, sic_pha_opt,
      gmi_pha_opt, tilted_vpc_pha_opt, sic_tilted_vpc_pha_opt,
      gmi_tilted_vpc_pha_opt,
      active_cut_heur_sicsolver[CutHeuristics::SIC_CUT_GEN],
      active_cut_heur_sicsolver[CutHeuristics::TILTED_GEN],
      num_active_vpcs_sicsolver, num_active_pha_sicsolver,
      active_cut_heur_gmisolver[CutHeuristics::GMI_CUT_GEN],
      active_cut_heur_gmisolver[CutHeuristics::TILTED_GEN],
      num_active_vpcs_gmisolver, num_active_pha_gmisolver,
      GlobalVariables::log_file);

  // Save the round info to ii as well
  writeRoundInfoToLog(sic_vpc_opt, sic_pha_opt, total_num_sics,
      boundByRoundSIC.size(), final_sic_bound, boundByRoundVPCPlus,
      boundByRoundPHAPlus, compute_condition_number_norm2(solver),
      basisCondByRoundSIC, basisCondByRoundVPC, basisCondByRoundVPCPlus,
      basisCondByRoundPHA, basisCondByRoundPHAPlus, minSICOrthogonalityInit,
      maxSICOrthogonalityInit, avgSICOrthogonalityInit, minVPCOrthogonalityInit,
      maxVPCOrthogonalityInit, avgVPCOrthogonalityInit, minPHAOrthogonalityInit,
      maxPHAOrthogonalityInit, avgPHAOrthogonalityInit,
      minSICOrthogonalityFinal, maxSICOrthogonalityFinal,
      avgSICOrthogonalityFinal, minVPCOrthogonalityFinal,
      maxVPCOrthogonalityFinal, avgVPCOrthogonalityFinal,
      minPHAOrthogonalityFinal, maxPHAOrthogonalityFinal,
      avgPHAOrthogonalityFinal, GlobalVariables::log_file);

  // Free
  if (SICSolver) {
    delete SICSolver;
  }
  if (GMISolver) {
    delete GMISolver;
  }
  if (LandPSolver) {
    delete LandPSolver;
  }
  if (TiltedSolver) {
    delete TiltedSolver;
  }
  if (copySICSolver) {
    delete copySICSolver;
  }
} /* compareCuts */

/***********************************************************************/
/** Methods related to adding points and rays */
/***********************************************************************/

/**
 * vec is the packed point
 * @return True if point added; false otherwise.
 */
bool addPoint(std::vector<Point>& point, const Point & tmpPt,
    const bool checkForDuplicate) {
  // Check to make sure we are not adding duplicate
  if (checkForDuplicate && hasPoint(point, tmpPt))
    return false;
  else {
    point.push_back(tmpPt);
    return true;
  }
}

/**
 * vec is the full (non-compressed) point
 * @return True if point added; false otherwise.
 */
bool addPoint(std::vector<Point>& point, const std::vector<double> &vec,
    const bool checkForDuplicate) {
  Point tmpPt((int) vec.size());
  tmpPt.setFullNonZero((int) vec.size(), vec.data());

  // Check to make sure we are not adding duplicate
  if (checkForDuplicate && hasPoint(point, tmpPt))
    return false;
  else {
    point.push_back(tmpPt);
    return true;
  }
}

/**
 * vec is the full (non-compressed) point
 * @return True if point added; false otherwise.
 */
bool addPoint(std::vector<Point>& point, int numElem, const double* vec,
    const bool checkForDuplicate) {
  Point tmpPt(numElem);
  tmpPt.setFullNonZero(numElem, vec);

  // Check to make sure we are not adding duplicate
  if (checkForDuplicate && hasPoint(point, tmpPt))
    return false;
  else {
    point.push_back(tmpPt);
    return true;
  }
}

/**
 * vec is the packed point
 * @return True if point added; false otherwise.
 */
bool addPackedPoint(std::vector<Point>& point, int coBasisSize,
    const std::vector<int> &index, const std::vector<double> &vec,
    const bool checkForDuplicate) {
  Point tmpPt(coBasisSize);
  tmpPt.setVector((int) index.size(), index.data(), vec.data(), false);

  // Check to make sure we are not adding duplicate
  if (checkForDuplicate && hasPoint(point, tmpPt))
    return false;
  else {
    point.push_back(tmpPt);
    return true;
  }
}

/**
 * vec is the packed ray
 * @return True if ray added; false otherwise.
 */
bool addPackedRay(std::vector<Ray>& ray, //int coBasisSize,
    const std::vector<int> &index, const std::vector<double> &vec,
    const bool checkForDuplicate) {
  Ray tmpRay;
//  Ray tmpRay(coBasisSize);
  tmpRay.setVector((int) index.size(), index.data(), vec.data(), false);

  // Check to make sure we are not adding duplicate
  if (checkForDuplicate && hasRay(ray, tmpRay))
    return false;
  else {
    ray.push_back(tmpRay);
    return true;
  }
}

/**
 * @return True if ray added; false otherwise.
 */
bool addRay(std::vector<Ray>& ray, const Ray & tmpRay,
    const bool checkForDuplicate) {
  // Check to make sure we are not adding duplicate
  if (checkForDuplicate && hasRay(ray, tmpRay))
    return false;
  else {
    ray.push_back(tmpRay);
    return true;
  }
}

/**
 * vec is the full (non-compressed) ray
 * @return True if ray added; false otherwise.
 */
bool addRay(std::vector<Ray>& ray, const std::vector<double> &vec,
    const bool checkForDuplicate) {
//  Ray tmpRay((int) vec.size());
  Ray tmpRay;
  tmpRay.setFullNonZero((int) vec.size(), vec.data());

  // Check to make sure we are not adding duplicate
  if (checkForDuplicate && hasRay(ray, tmpRay))
    return false;
  else {
    ray.push_back(tmpRay);
    return true;
  }
}

/**
 * vec is the full (non-compressed) ray
 * @return True if ray added; false otherwise.
 */
bool addRay(std::vector<Ray>& ray, int numElem, const double* vec,
    const bool checkForDuplicate) {
//  Ray tmpRay(numElem);
  Ray tmpRay;
  tmpRay.setFullNonZero(numElem, vec);

  // Check to make sure we are not adding duplicate
  if (checkForDuplicate && hasRay(ray, tmpRay))
    return false;
  else {
    ray.push_back(tmpRay);
    return true;
  }
}

/**
 * Check if point has tmpPoint
 */
bool hasPoint(const std::vector<Point> & point, const Point & tmpPoint) {
  for (int p = 0; p < (int) point.size(); p++) {
    if (tmpPoint == point[p])
      return true;
  }
  return false;
}

/**
 * Check if ray has tmpRay
 */
bool hasRay(const std::vector<Ray> & ray, const Ray & tmpRay) {
  for (int r = 0; r < (int) ray.size(); r++) {
    if (tmpRay == ray[r])
      return true;
  }
  return false;
}

/***********************************************************************/
/***********************************************************************/
/** Methods related to adding an OsiRowCut cut */
/***********************************************************************/
/***********************************************************************/
//int duplicateCutIndex(const OsiRowCut & tmpCut, const OsiCuts& cuts) {
//  for (int i = 0; i < cuts.sizeCuts()(); i++) {
//    if (tmpCut == cuts.)
//      return i;
//  }
//  // If we have reached the end, then this cut was not found
//  return -1;
//}

/***********************************************************************/
/***********************************************************************/
/** Methods to help with generating points and rays */
/***********************************************************************/
/***********************************************************************/
//
///**
// * Returns true if a hyperplane was found, and false otherwise
// * The method depends on the points and rays having their indices sorted
// */
//bool findHplaneToIntersectRay(int& varIn, double& rayDist,
//    const Point & origPoint, const Ray & rayOut,
//    const LiftGICsSolverInterface* const  solver, const SolutionInfo & probData) {
//  rayDist = solver->getInfinity();
//  varIn = -1;
//
//  const int numPointElems = origPoint.getNumElements();
//  const int numRayElems = rayOut.getNumElements();
//  const int* pointIndex = origPoint.getIndices();
//  const int* rayIndex = rayOut.getIndices();
//  const double* pointVal = origPoint.getElements();
//  const double* rayVal = rayOut.getElements();
//
//  int pt_el = 0;
//  for (int el = 0; el < numRayElems; el++) {
//    int curr_ray_var = rayIndex[el]; // THIS COULD BE IN NB SPACE!
//    double rayDirn = rayVal[el];
//
//    // Retrieve the associated point value,
//    // if one exists for this index.
//    // Depends crucially on point and ray being sorted correctly.
//    double ptVal = 0.0;
//    if (pt_el < numPointElems) {
//      int curr_pt_var = pointIndex[pt_el];
//      while (curr_pt_var < curr_ray_var && pt_el < numPointElems) {
//        pt_el++;
//        curr_pt_var = pointIndex[pt_el];
//      }
//      if (curr_pt_var == curr_ray_var)
//        ptVal = pointVal[pt_el];
//    }
//
//    // If this ray is not going anywhere in this direction, skip it
//    if (std::abs(rayDirn) < param.getEPS()) {
//      continue;
//    }
//
//    // Determine how far we can go along this direction
//    // To do this, we check the bounds on this direction
//    // We need to be careful, because we are working in
//    // the full space, but the variables that were in N(v)
//    // have been complemented and shifted
//    // In particular, bounds for j \in N(v) become, in the y-space:
//    //   LB: 0
//    //   UB: g_j - l_j
//    double LB, UB;
//    if (curr_ray_var < probData.numCols) {
//      if (probData.rowOfVar[curr_ray_var] >= 0) {
//        LB = solver->getColLower()[curr_ray_var];
//        UB = solver->getColUpper()[curr_ray_var];
//      } else {
//        LB = 0;
//        UB = solver->getColUpper()[curr_ray_var]
//            - solver->getColLower()[curr_ray_var];
//      }
//    } else {
//      // Again we should be careful, because the slacks were not all complemented:
//      // Only the nb slacks
//      LB = 0;
//      UB = solver->getInfinity();
//      // If it was basic at v and corresponds to a >= row, we adjust for it
//      if ((probData.rowOfVar[curr_ray_var] >= 0)
//          && solver->getRowSense()[curr_ray_var - probData.numRows]
//              == 'G') {
//        LB = -1 * solver->getInfinity();
//        UB = 0;
//      }
//    }
//
//    // We have x_i + t r_i^j = l_j or g_j
//    // depending on the direction of the ray
//    // (or off to infinity)
//    if (rayDirn > param.getEPS()) {
//      if (UB < solver->getInfinity() - param.getEPS()) {
//        double tmpRayDist = (UB - ptVal) / rayDirn;
//        if (tmpRayDist < rayDist) {
//          rayDist = tmpRayDist;
//          varIn = curr_ray_var;
//        }
//      }
//    } else {
//      // Otherwise, the ray direction is negative
//      // (We would have ``continued'' before otherwise)
//      if (LB > -1.0 * solver->getInfinity() + param.getEPS()) {
//        double tmpRayDist = (LB - ptVal) / rayDirn;
//        if (tmpRayDist < rayDist) {
//          rayDist = tmpRayDist;
//          varIn = curr_ray_var;
//        }
//      }
//    }
//  }
//
//  if (varIn < 0)
//    return false;
//  else
//    return true;
//}
//
//void addRaysFromActivation(const int rayOut, const Point & origPoint,
//    const Point & origPointFullSpace, std::vector<Ray>& ray,
//    std::vector<Ray>& rayFullSpace, const LiftGICsSolverInterface* const  tmpSolver,
//    const SolutionInfo & origProbData) {
//  // tmpSolver has the optimal basis on the face of the split
//  // Compute the adjusted rhs that we need to use in order to do the pivot
//  std::vector<double> a0;
//  calculateA0FullInCompNBSpace(a0, origPointFullSpace, rayFullSpace,
//      tmpSolver, origProbData);
//
//  for (int i = 0; i < (int) origPointFullSpace.rayIndices.size(); i++) {
//    int currRay = origPointFullSpace.rayIndices[i];
//    // Make sure this is not the ray we just pivoted out
//    if (currRay == rayOut)
//      continue;
//  }
//
//}
//
//void calculateA0FullInCompNBSpace(std::vector<double>& a0,
//    const Point & pointFullSpace, const std::vector<Ray>& rayFullSpace,
//    const LiftGICsSolverInterface* const  solver,
//    const SolutionInfo & origProbData) {
//  // We want to get the values of a_0
//  // pointFullSpace currently stores the value of x_i, which is
//  //   x_i = a_0 - \sum_{k \in N_L} a_{ik} \ell_k - \sum_{k \in N_U} a_{ik} g_k
//  // We are looking for the full a_0 vector in the complemented non-basic space
//  // The point has the cobasis, and it is in the full space, so we have those values as well
//
//  // 0. Allocate space
//  a0.clear();
//  a0.resize(origProbData.numRows + origProbData.numCols, 0.0);
//
//  // We keep the following vector because we need *quick* access to the
//  // point values, which is not available in a packed vector
//  std::vector<double> xi(origProbData.numRows + origProbData.numCols, 0.0);
//
//  // 1. Iterate through all i, and initialize its value
//  const int numPointElems = pointFullSpace.getNumElements();
//  const int* pointIndex = pointFullSpace.getIndices();
//  const double* pointVal = pointFullSpace.getElements();
//
//  // If the current variable is non-basic, we just store in a0 its value
//  // Otherwise we perform the adjustments (in the next step)
//  for (int el = 0; el < numPointElems; el++) {
//    const int curr_var = pointIndex[el];
//    a0[curr_var] = pointVal[el];
//    xi[curr_var] = pointVal[el];
//  }
//
//  // 2. Now we adjust a0 using the rays
//  // We assume the cobasis is in sorted order, in same order as rayIndices!
//  int cobasis_ind = 0; // Used to track if a variable is basic or not
//  int curr_nbvar = pointFullSpace.cobasis[cobasis_ind];
//  for (int j = 0; j < (int) pointFullSpace.rayIndices.size(); j++) {
//    const int curr_ray = pointFullSpace.rayIndices[j];
//    const int* rayIndex = rayFullSpace[curr_ray].getIndices();
//    const double* rayVal = rayFullSpace[curr_ray].getElements();
//    const int numRayElems = rayFullSpace[curr_ray].getNumElements();
//
//    for (int el = 0; el < numRayElems; el++) {
//      const int curr_row = rayIndex[el];
//
//      // If we have passed the row corresponding to curr_nbvar
//      // get the next curr_nbvar, to check if curr_var is non-basic
//      while (curr_row > curr_nbvar) {
//        cobasis_ind++;
//        if (cobasis_ind < (int) pointFullSpace.cobasis.size())
//          curr_nbvar = pointFullSpace.cobasis[cobasis_ind];
//        else
//          curr_nbvar = origProbData.numRows + origProbData.numCols;
//      }
//
//      // If this row is basic, then we need to adjust a0
//      if (curr_row < curr_nbvar)
//        a0[curr_row] += rayVal[el] * xi[curr_row];
//    }
//  }
//}
