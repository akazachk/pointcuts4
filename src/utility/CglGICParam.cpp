// Name:     CglGICParam.cpp
// Author:   Aleksandr M. Kazachkov 
// Date:     02/02/16
//-----------------------------------------------------------------------------

#include "CglGICParam.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdio>
#include "Utility.hpp"
#include "GlobalConstants.hpp"

/***********************************************************************/
CglGICParam::CglGICParam(const double inf, const double eps,
    const double eps_coeff, const int max_supp_abs) :
  CglParam(inf, eps, eps_coeff, max_supp_abs) {
  setParams();
} /* constructor */

/***********************************************************************/
CglGICParam::CglGICParam(const CglGICParam& source) :
  CglParam(source) {
  setParams(&source);
} /* copy constructor from CglGICParam */

/***********************************************************************/
CglGICParam* CglGICParam::clone() const {
  return new CglGICParam(*this);
} /* clone */

/***********************************************************************/
CglGICParam& CglGICParam::operator=(const CglGICParam &rhs) {
  if(this != &rhs) {
    CglParam::operator=(rhs);
  }
  setParams(&rhs);
  return *this;
} /* assignment operator */

/***********************************************************************/
CglGICParam::~CglGICParam() {
  paramVal.resize(0);
} /* destructor */

/***********************************************************************/
void CglGICParam::setParams(const CglGICParam* const rhs) {
  // Initialize array
  if ((int) paramVal.size() < ParamIndices::NUM_PARAMS) {
    paramVal.resize(ParamIndices::NUM_PARAMS, 0);
  }
  if (rhs) {
    for (int i = 0; i < (int) CglGICParamNamespace::ParamName.size(); i++) {
      paramVal[i] = rhs->getParamVal(i);
    }
    MAX_SUPPORT_REL = rhs->getMAX_SUPPORT_REL();
    RAYEPS = rhs->getRAYEPS();
    DIFFEPS = rhs->getDIFFEPS();
    AWAY = rhs->getAWAY();
    MIN_ORTHOGONALITY = rhs->getMINORTHOGONALITY();
    TIMELIMIT = rhs->getTIMELIMIT();
    CUTSOLVER_TIMELIMIT = rhs->getCUTSOLVER_TIMELIMIT();
    PARTIAL_BB_TIMELIMIT = rhs->getPARTIAL_BB_TIMELIMIT();
    BB_TIMELIMIT = rhs->getBB_TIMELIMIT();
    LUB = rhs->getLUB();
    MAXDYN = rhs->getMAXDYN();
    MAXDYN_LUB = rhs->getMAXDYN_LUB();
    EPS_COEFF_LUB = rhs->getEPS_COEFF_LUB();
    MAXCUTS = rhs->getMAXCUTS();
    CUTSOLVER_MAX_ITER = rhs->getCUTSOLVER_MAX_ITER();
    ITER_PER_CUT_BILINEAR = rhs->getITER_PER_CUT_BILINEAR();
    MAX_PIVOTS = rhs->getMAX_PIVOTS();
    MIN_VIOL_ABS = rhs->getMIN_VIOL_ABS();
    MIN_VIOL_REL = rhs->getMIN_VIOL_REL();
    CHECK_DUPLICATES = rhs->getCHECK_DUPLICATES();
    EXIT_ON_INFEAS = rhs->getEXIT_ON_INFEAS();
    TEMP = rhs->getTEMP();
  } else {
    for (int i = 0; i < (int) CglGICParamNamespace::ParamName.size(); i++) {
      paramVal[i] = ParamDefaults[i];
    }
    MAX_SUPPORT_REL = DEFAULT_MAX_SUPPORT_REL;
    RAYEPS = DEFAULT_RAYEPS;
    DIFFEPS = DEFAULT_DIFFEPS;
    AWAY = DEFAULT_AWAY;
    MIN_ORTHOGONALITY = DEFAULT_MIN_ORTHOGONALITY;
    TIMELIMIT = DEFAULT_TIMELIMIT;
    CUTSOLVER_TIMELIMIT = DEFAULT_CUTSOLVER_TIMELIMIT;
    PARTIAL_BB_TIMELIMIT = DEFAULT_PARTIAL_BB_TIMELIMIT;
    BB_TIMELIMIT = DEFAULT_BB_TIMELIMIT;
    LUB = DEFAULT_LUB;
    MAXDYN = DEFAULT_MAXDYN;
    MAXDYN_LUB = DEFAULT_MAXDYN_LUB;
    EPS_COEFF_LUB = DEFAULT_EPS_COEFF_LUB;
    MAXCUTS = DEFAULT_MAXCUTS;
    CUTSOLVER_MAX_ITER = DEFAULT_CUTSOLVER_MAX_ITER;
    ITER_PER_CUT_BILINEAR = DEFAULT_ITER_PER_CUT_BILINEAR;
    MAX_PIVOTS = DEFAULT_MAX_PIVOTS;
    MIN_VIOL_ABS = DEFAULT_MIN_VIOL_ABS;
    MIN_VIOL_REL = DEFAULT_MIN_VIOL_REL;
    CHECK_DUPLICATES = DEFAULT_CHECK_DUPLICATES;
    EXIT_ON_INFEAS = DEFAULT_EXIT_ON_INFEAS;
    TEMP = DEFAULT_TEMP;
  }
} /* setParams */

/**********************************************************/
void CglGICParam::setParams(const char* filename) {
  if (!filename){
    setParams();
    return;
  }

  std::ifstream infile(filename);
  if (infile.is_open()) {
    std::string line;
    while (std::getline(infile, line)) {
      std::istringstream iss(line);
      if (line.empty()) {
        continue;
      }
      std::string param_name;
      if (!(iss >> param_name)) {
        fprintf(stderr, "*** ERROR: Could not read parameter name. String is %s.\n", line.c_str());
        continue;
      }
      double val;
      if (!(iss >> val)) {
        fprintf(stderr, "*** ERROR: Could not read parameter value. String is %s.\n", line.c_str());
        continue;
      }

      setParamVal(toUpperStringNoUnderscore(param_name), val); // standardize name
    }
    infile.close();
  } else {
    // So far we are exiting disgracefully
    fprintf(stderr, "*** ERROR: Could not open parameter file %s.\n", filename);
    exit(1);
  }
} /* setParams (from file) */

/***********************************************************************/
/**
 * Ensure that parameter settings are within acceptable ranges
 */
bool CglGICParam::checkParam(const int paramIndex, const bool given_value, int value) const {
  // First we check if value is set to a dummy value
  if (!given_value) {
    value = paramVal[paramIndex];
  }
  if (paramIndex >= 0 && paramIndex < ParamIndices::NUM_PARAMS) {
    switch (paramIndex) { 
      case SICS_PARAM_IND:
        if (value < 0 || value > 1) {
          printf(
            "*** ERROR: "
            "SICs param needs to be false (0) or true (1). Provided is: %d.\n",
            value);
          return false;
        }
        break;
      case GMI_PARAM_IND:
        if (value < 0 || value > 1) {
          printf(
              "*** ERROR: "
              "GMI param needs to be false (0) or true (1). Provided is: %d.\n",
              value);
          return false;
        }
        break;
      case LANDP_PARAM_IND:
        if (value < 0 || value > 1) {
          printf("*** ERROR: "
              "LandP param needs to be false (0) or true (1). Provided is: %d.\n",
              value);
          return false;
        }
        break;
      case TILTED_DEPTH_PARAM_IND:
        if (value < -1) {
          printf(
            "*** ERROR: "
            "Depth of tilted cuts param needs to be -1 or higher. Provided is: %d.\n",
            value);
          return false;
        }
        break;
      case VPC_DEPTH_PARAM_IND:
//        if (value < 0) {
//          printf(
//            "*** ERROR: "
//            "Depth of VPCs needs to be non-negative (0 off, 1+ is depth). Provided is: %d.\n",
//            value);
//          return false;
//        }
        if (value == -1) {
          printf(
            "*** ERROR: "
            "Requested value of -1 VPC depth corresponds to using just one B&B node which does not lead to any cuts.\n");
          return false;
        }
        break;
      case PHA_PARAM_IND:
        if (value < 0 || value > 1) {
          printf(
            "*** ERROR: "
            "Use PHA param needs to be false (0) or true (1). Provided is: %d.\n",
            value);
          return false;
        }
        break;
      case NB_SPACE_PARAM_IND: // 0: Use NB space when generating cuts, 1: do not use it
        if (value < 0 || value > 1) {
          printf(
            "*** ERROR: "
            "Use NB space param needs to be false (0) or true (1). Provided is: %d.\n",
            value);
          return false;
        }
        break;
      case STRENGTHEN_PARAM_IND: // 0: Use GMI disjunctions, 1: do not use them
        if (!(value == 0 || value == 1 || value == 2)) {
          printf(
            "*** ERROR: "
            "Strengthen param needs to be false (0) or true (1) or true+ (2), which means generate gomory cuts on disj terms. Provided is: %d.\n",
            value);
          return false;
        }
        break;
      case MIX_PARAM_IND: // 0: Mix inequalities (for tilted ineqs), 1: do not mix them
        if (value < 0 || value > 1) {
          printf(
            "*** ERROR: "
            "Mix param needs to be false (0) or true (1). Provided is: %d.\n",
            value);
          return false;
        }
        break;
      case PIVOT_PARAM_IND: // 0: Pivot to better solutions when generating cuts, 1: do not pivot
        if (value < 0 || value > 1) {
          printf(
            "*** ERROR: "
            "Pivot param needs to be false (0) or true (1). Provided is: %d.\n",
            value);
          return false;
        }
        break;
      case PARTIAL_BB_STRATEGY_PARAM_IND:
//        if (value < 0) { //|| (value >= 10 && value < 20)) {
//          printf(
//              "*** ERROR: "
//                  "Partial BB strategy takes: tens digit: 0: default, 1: dynamic (DISABLED), 2: strong; ones digit: 0: bfs, 1: default, 2: depth, 3: estimate, 4: objective. Provided is: %d.\n",
//              value);
//          return false;
//        }
        break;
      case PARTIAL_BB_NUM_STRONG_PARAM_IND:
        if (value < -2) {
          printf(
              "*** ERROR: "
                  "Partial BB num strong takes: -2: sqrt(n); -1: full sb; 0+: as in Cbc. Provided is %d.\n",
              value);
          return false;
        }
        break;
      case VPC_DEPTH2_HEUR_PARAM_IND: // 0: cut rays, 1: activate hyperplanes
        if (value < 0 || value > 1) {
          printf(
              "*** ERROR: "
                  "VPC depth 2 heuristic needs to be either 0 (cut rays) or 1 (activate hyperplanes). Provided is: %d.\n",
              value);
          return false;
        }
        break;
      case MAX_HPLANES_PARAM_IND:
        if (value < 0) {
          printf(
            "*** ERROR: "
            "Number of hyperplanes to activate for VPCs depth 2 needs to be non-negative (0 off). Provided is: %d.\n",
            value);
          return false;
        }
        break;
      case SPLIT_VAR_DELETED_PARAM_IND:
        if (value < 0 || value > 1) {
          printf(
              "*** ERROR: "
              "Whether split variable is deleted to form facet solvers needs to be boolean. Provided is: %d.\n",
              value);
          return false;
        }
        break;
      case PHA_ACT_OPTION_PARAM_IND: // -1: skip, 0: first, 1: best, 2: best_final
        if (value < -1 || value > 2) {
          printf(
            "*** ERROR: "
            "Hplane heuristic needs to be between -1 and 2. Provided is: %d.\n",
            value);
          return false;
        }
        break;
      case NEXT_HPLANE_FINAL_PARAM_IND: // If generating more than 1 hplane cutting each ray, how to choose
        if (value < 0 || value > 1) {
          printf(
            "*** ERROR: "
            "Next hplane final flag needs to be false (0) or true (1). Provided is: %d.\n",
            value);
          return false;
        }
        break;
      case NUM_EXTRA_ACT_ROUNDS_PARAM_IND: // <= 1 if next_hplane_final = false, else can be more; negative if no tilting round
//        if (value < 0) {
//          printf(
//            "*** ERROR: "
//            "Number of hyperplanes activated per ray needs to be at least 0. Provided is: %d.\n",
//            value);
//          return false;
//        }
        break;
      case NUM_RAYS_CUT_PARAM_IND: // -2 means sqrt(n)/2 rays, -1 means sqrt(n), 0 means as many as possible
        if (value < -2) {
          printf(
            "*** ERROR: "
            "Number of rays cut needs to be -2 or -1 (if we want sqrt(n)/2 or sqrt(n) rays cut, respectively) or non-negative. Provided is %d.\n",
            value);
          return false;
        }
        break;
      case MAX_FRAC_VAR_PARAM_IND: // -2 means sqrt(n)/2 rays, -1 means sqrt(n), 0 means as many as possible
        if (value < -2) {
          printf(
              "*** ERROR: "
                  "Number of variables cut needs to be -2 or -1 (if we want sqrt(n)/2 or sqrt(n) considered, respectively) or non-negative. Provided is %d.\n",
              value);
          return false;
        }
        break;
      case CGS_PARAM_IND: // 0 splits, 1-4 triangles with that corner, 5 all triangles, 6 only triangles not violating shifted diag, 7 cross
        if (value < 0) {
          printf(
              "*** ERROR: "
                  "Cut-generating set option needs to be at least 0. Provided is %d.\n",
              value);
          return false;
        }
        break;
      case NUM_CGS_PARAM_IND: // 0 means as many as possible, -1 means choose sqrt(.) best ones
//        if (value < -1) {
//          printf(
//            "*** ERROR: "
//            "Number of cut-generating sets needs to be at least 0. Provided is %d.\n",
//            value);
//          return false;
//        }
        break;
      case NUM_CUTS_PER_CGS_PARAM_IND:
        if (value < 0) {
          printf(
            "*** ERROR: "
            "Number of cuts per SIC needs to be at least 0. Provided is %d.\n",
            value);
          return false;
        }
        break;
      case OVERLOAD_MAX_CUTS_PARAM_IND: // if > 0, replaces num_cuts_per_cgs parameter for determining max num cuts; < 0 = abs(val) * numSplits
//        if (value < 0) {
//          printf(
//            "*** ERROR: "
//            "Maximum number of cuts needs to be at least 0. Provided is %d.\n",
//            value);
//          return false;
//        }
        break;
      case LIMIT_CUTS_PER_CGS_PARAM_IND: // should number of cuts be limited per split or from total
        if (value < 0 || value > 1) {
          printf(
            "*** ERROR: "
            "Limit cuts per split is a boolean. Provided is %d.\n",
            value);
          return false;
        }
        break;
      case PRLP_STRATEGY_PARAM_IND: // 0: default, 1: try few objectives
        if (value < 0 || value > 2) {
          printf("*** ERROR: "
              "PRLP strategy is either 0 (default) or 1 (try few objectives). Provided is %d.\n", value);
          return false;
        }
        break;
      case FLIP_BETA_PARAM_IND: // 0: no, 1: yes, 2: try even if PRLP is feasible, <0: same but with -1 beta to start
        if (value < -3 || value > 3) {
          printf(
              "*** ERROR: "
                  "Flip beta is either 0 (no), +/- 1 (yes), or +/- 2 (try even if PRLP is feasible). Provided is %d.\n",
              value);
          return false;
        }
        break;
      case USE_SPLIT_SHARE_PARAM_IND: // should the split share be used to generate cuts (may be slow): 0 no, 1 yes first, 2 yes last
        if (value < 0 || value > 2) {
          printf(
            "*** ERROR: "
            "Use split share takes values 0 (no), 1 (yes first), and 2 (yes last). Provided is: %d.\n",
            value);
          return false;
        }
        break;
      case NUM_CUTS_ITER_BILINEAR_PARAM_IND: // 0: do not use the iterative bilinear cut generation, 1+: num cuts to try to generate
        if (value < 0) {
          printf(
            "*** ERROR: "
            "Number of cuts for the iterative bilinear program needs to be non-negative. Provided is: %d.\n",
            value);
          return false;
        }
        break;
      case USE_CUT_VERT_HEUR_PARAM_IND: // 0: do not use the cut vertices cut selection heuristic
        if (value < 0 || value > 1) {
          printf(
            "*** ERROR: "
            "Cut vertices heuristic is either 1 (yes) or 0 (no). Provided is: %d.\n",
            value);
          return false;
        }
        break;
      case MODE_OBJ_PER_POINT_PARAM_IND:
        // acceptable values: -1, 0, 1, 10, 11, 100, 101, 110, 111, 200, 201, 210, 211
        break;
      case NUM_OBJ_PER_POINT_PARAM_IND: // # cuts to try to generate for the strong branching lb point (and others)
        break;
      case USE_ALL_ONES_HEUR_PARAM_IND:
        if (value < 0 && value > 1) {
          printf(
            "*** ERROR: "
            "All ones heuristic is either 1 (yes) or 0 (no). Provided is: %d.\n",
            value);
          return false;
        }
        break;
      case USE_UNIT_VECTORS_HEUR_PARAM_IND: // 0: do not use, 1+: num to try, <0: abs(val) * sqrt(n)
        break;
      case USE_TIGHT_POINTS_HEUR_PARAM_IND: // 0: do not use the tight on points cut heuristic, 1+: num points to try
        if (value < 0) {
          printf(
            "*** ERROR: "
            "Number of points to try for tight on points heuristic must be at least 0. Provided is: %d.\n",
            value);
          return false;
        }
        break;
      case USE_TIGHT_RAYS_HEUR_PARAM_IND: // 0: do not use the tight on rays cut heuristic, 1+: num rays to try, <0: abs(val) * sqrt(# rays)
//        if (value < 0) {
//          printf(
//            "*** ERROR: "
//            "Number of points/rays to try for tight on points/rays heuristic must be at least 0. Provided is: %d.\n",
//            value);
//          return false;
//        }
        break;
      case CUT_PRESOLVE_PARAM_IND: // use presolve in cutSolver? (1: initial, 2: initial+resolve)
        if (value < 0 || value > 2) {
          printf(
            "*** ERROR: "
            "Cut presolve takes values 0, 1, or 2. Provided is: %d.\n",
            value);
          return false;
        }
        break;
      case MAX_POINTS_PER_SPLIT_SPLIT_SHARE_PARAM_IND: // how many points per split when using split share
        if (value < 0) {
          printf(
            "*** ERROR: "
            "The maximum number of points to use in split share needs to be non-negative. Provided is: %d.\n",
            value);
          return false;
        }
        break;
      case SUBSPACE_PARAM_IND: // work in subspace?
        if (value < 0 || value > 1) {
          printf(
            "*** ERROR: "
            "Subspace is binary: either used (1) or not used (0). Provided is: %d.\n",
            value);
          return false;
        }
        break;
      case ROUNDS_PARAM_IND: // 0: no cuts, 1+: number of rounds of cuts
        if (value < 0) {
          printf(
              "*** ERROR: "
                  "Rounds is either 0 (not used) or > 0 (used with that many rounds). Provided is: %d.\n",
              value);
          return false;
        }
        break;
      case BB_RUNS_PARAM_IND: // 0: do not perform branch-and-bound, > 0: this many randomizations of the row or column order
//        if (value < 0) {
//          printf(
//              "*** ERROR: "
//                  "BB is either 0 (not used) or > 0 (used with that many runs). Provided is: %d.\n",
//              value);
//          return false;
//        }
        break;
      case BB_STRATEGY_PARAM_IND: // see enum; negative value means use default for whichever solvers are enabled
        break;
    }
  } else {
    printf("*** WARNING: CglGICParam::checkParam(): index %d out of bounds (needs to be at most %d)\n",
        paramIndex, ParamIndices::NUM_PARAMS);
    return false;
  }
  return true;
} /* checkParam */

/***********************************************************************/
bool CglGICParam::setParamVal(const int index, const int value) {
  if (index >= 0 && index < ParamIndices::NUM_PARAMS) {
    paramVal[index] = value; // We do not check for this being in bounds here
    return true;
  } else {
    printf("*** WARNING: CglGICParam::setParam(): index %d out of bounds (needs to be at most %d)\n",
        index, ParamIndices::NUM_PARAMS);
    return false;
  }
} /* setParamVal (specific index) */

/***********************************************************************/
/*
 * name is assumed to have been standardized already
 */
bool CglGICParam::setParamVal(const std::string name, const double value) {
  const int index = getParamIndex(name);
  if (index >= 0) {
    return setParamVal(index, int(value));
  } else if (name.compare("EPS") == 0) {
    setEPS(value);
    return true;
  } else if (name.compare("RAYEPS") == 0) {
    setRAYEPS(value);
    return true;
  }  else if (name.compare("AWAY") == 0) {
    setAWAY(value);
    return true;
  } else if (name.compare("MINORTHOGONALITY") == 0) {
    setMINORTHOGONALITY(value);
    return true;
  } else if (name.compare("MINVIOLABS") == 0) {
    setMIN_VIOL_ABS(value);
    return true;
  } else if (name.compare("MINVIOLREL") == 0) {
    setMIN_VIOL_REL(value);
    return true;
  } else if (name.compare("TIMELIMIT") == 0) {
    setTIMELIMIT(value);
    return true;
  } else {
    printf("*** WARNING: Could not find parameter %s.\n", name.c_str());
    return false;
  }
} /* setParamVal (by string name) */

/***********************************************************************/
/*
 * name is assumed to have been standardized already
 */
int CglGICParam::getParamIndex(const std::string name) const {
  for (unsigned i = 0; i < CglGICParamNamespace::ParamName.size(); i++) {
    std::string str1 = toUpperStringNoUnderscore(
        CglGICParamNamespace::ParamName[i]);
    if (str1.compare(name) == 0) {
      return i;
    }
  }
  return -1;
} /* getParamIndex */

/***********************************************************************/
bool CglGICParam::setMAX_SUPPORT_REL(const double value) {
  if (value >= 0.0) {
    MAX_SUPPORT_REL = value;
    return true;
  } 
  else {
    printf("*** WARNING: CglGICParam::setMAX_SUPPORT_REL(): value: %f ignored (needs to be at least 0)\n",
        value);
    return false;
  }
} /* setMAX_SUPPORT_REL */

bool CglGICParam::setRAYEPS(const double value) {
  if (value >= 0.0) {
    RAYEPS = value;
    return true;
  }
  else {
    printf("*** WARNING: CglGICParam::setRAYEPS(): value: %f ignored (needs to be at least 0)\n",
        value);
    return false;
  }
} /* setRAYEPS */

bool CglGICParam::setDIFFEPS(const double value) {
  if (value >= 0.0) {
    DIFFEPS = value;
    return true;
  } 
  else {
    printf("*** WARNING: CglGICParam::setDIFFEPS(): value: %f ignored (needs to be at least 0)\n",
        value);
    return false;
  }
} /* setDIFFEPS */

bool CglGICParam::setAWAY(const double value) {
  if (value >= 0.0) {
    AWAY = value;
    return true;
  } else {
    printf(
        "*** WARNING: CglGICParam::setAWAY(): value: %f ignored (needs to be at least 0)\n",
        value);
    return false;
  }
} /* setAWAY */

bool CglGICParam::setMINORTHOGONALITY(const double value) {
  if (value >= 0.0 && value <= 1.0) {
    MIN_ORTHOGONALITY= value;
    return true;
  } else {
    printf(
        "*** WARNING: CglGICParam::setMINORTHOGONALITY(): value: %f ignored (needs to be at least 0 and at most 1)\n",
        value);
    return false;
  }
} /* setMINORTHOGONALITY */

bool CglGICParam::setTIMELIMIT(const double value) {
  if (value >= 0.0) {
    TIMELIMIT = value;
    return true;
  }
  else {
    printf("*** WARNING: CglGICParam::setTIMELIMIT(): value: %f ignored (needs to be at least 0)\n",
        value);
    return false;
  }
} /* setTIMELIMIT */

bool CglGICParam::setCUTSOLVERTIMELIMIT(const double value) {
//  if (value >= 0.0) {
    CUTSOLVER_TIMELIMIT = value;
    return true;
//  }
//  else {
//    printf("*** WARNING: CglGICParam::setCUTSOLVERTIMELIMIT(): value: %f ignored (needs to be at least 0)\n",
//        value);
//    return false;
//  }
} /* setCUTSOLVERTIMELIMIT */

bool CglGICParam::setPARTIALBBTIMELIMIT(const double value) {
  if (value >= 0.0) {
    PARTIAL_BB_TIMELIMIT = value;
    return true;
  }
  else {
    printf("*** WARNING: CglGICParam::setPARTIALBBTIMELIMIT(): value: %f ignored (needs to be at least 0)\n",
        value);
    return false;
  }
} /* setPARTIALBBTIMELIMIT */

bool CglGICParam::setBBTIMELIMIT(const double value) {
  if (value >= 0.0) {
    BB_TIMELIMIT = value;
    return true;
  }
  else {
    printf("*** WARNING: CglGICParam::setBBTIMELIMIT(): value: %f ignored (needs to be at least 0)\n",
        value);
    return false;
  }
} /* setBBTIMELIMIT */

bool CglGICParam::setLUB(const int value) {
  if (value >= 0.0) {
    LUB = value;
    return true;
  } else {
    printf("*** WARNING: CglGICParam::setLUB(): value: %d ignored (needs to be at least 0)\n",
        value);
    return false;
  }
} /* setLUB */

bool CglGICParam::setMAXDYN(const double value) {
  if (value >= 1.0) {
    MAXDYN = value;
    return true;
  } else {
    printf("*** WARNING: CglGICParam::setMAXDYN(): value: %f ignored (needs to be at least 1)\n",
        value);
    return false;
  }
} /* setMAXDYN */

bool CglGICParam::setMAXDYN_LUB(const double value) {
  if (value >= 1.0) {
    MAXDYN_LUB = value;
    return true;
  } else {
    printf("*** WARNING: CglGICParam::setMAXDYN_LUB(): value: %f ignored (needs to be at least 1)\n",
        value);
    return false;
  }
} /* setMAXDYN_LUB */

bool CglGICParam::setEPS_COEFF_LUB(const double value) {
  if (value >= 0.0) {
    EPS_COEFF_LUB = value;
    return true;
  } else {
    printf("*** WARNING: CglGICParam::setMEPS_COEFF_LUB(): value: %f ignored (needs to be at least 0)\n",
        value);
    return false;
  }
} /* setEPS_COEFF_LUB */

bool CglGICParam::setMAXCUTS(const int value) {
  if (value >= 0.0) {
    MAXCUTS = value;
    return true;
  } else {
    printf("*** WARNING: CglGICParam::setMAXCUTS(): value: %d ignored (needs to be at least 0)\n",
        value);
    return false;
  }
} /* setMAXCUTS */

bool CglGICParam::setCUTSOLVER_MAX_ITER(const int value) {
  if (value >= 0.0) {
    CUTSOLVER_MAX_ITER = value;
    return true;
  } else {
    printf("*** WARNING: CglGICParam::setCUTSOLVER_MAX_ITER(): value: %d ignored (needs to be at least 0)\n",
        value);
    return false;
  }
} /* setCUTSOLVER_MAX_ITER */

bool CglGICParam::setITER_PER_CUT_BILINEAR(const int value) {
  if (value >= 0.0) {
    ITER_PER_CUT_BILINEAR = value;
    return true;
  } else {
    printf("*** WARNING: CglGICParam::setITER_PER_CUT_BILINEAR (): value: %d ignored (needs to be at least 0)\n",
        value);
    return false;
  }
} /* setITER_PER_CUT_BILINEAR */

bool CglGICParam::setMAX_PIVOTS(const int value) {
  if (value >= 0.0) {
    MAXCUTS = value;
    return true;
  } else {
    printf("*** WARNING: CglGICParam::setMAXCUTS(): value: %d ignored (needs to be at least 0)\n",
        value);
    return false;
  }
} /* setMAX_PIVOTS */

bool CglGICParam::setMIN_VIOL_ABS(const double value) {
  if (value >= 0.0) {
    MIN_VIOL_ABS = value;
    return true;
  } 
  else {
    printf("*** WARNING: CglGICParam::setMIN_VIOL_ABS(): value: %f ignored (needs to be at least 0)\n",
        value);
    return false;
  }
} /* setMIN_VIOL_ABS */

bool CglGICParam::setMIN_VIOL_REL(const double value) {
  if (value >= 0.0) {
    MIN_VIOL_REL = value;
    return true;
  } 
  else {
    printf("*** WARNING: CglGICParam::setMIN_VIOL_REL(): value: %f ignored (needs to be at least 0)\n",
        value);
    return false;
  }
} /* setMIN_VIOL_REL */

void CglGICParam::setCHECK_DUPLICATES(const bool value) {
  CHECK_DUPLICATES = value;
} /* setCHECK_DUPLICATES */

void CglGICParam::setEXIT_ON_INFEAS(const bool value) {
  EXIT_ON_INFEAS = value;
} /* setEXIT_ON_INFEAS */

void CglGICParam::setTEMP(const int value) {
  TEMP = value;
} /* setTEMP */

/***********************************************************************/
// Check that the parameters are okay given the number of rays and splits
// If returns false, we will be exiting out
bool CglGICParam::finalizeParams(const int numNB, const int numSplits) {
  // If user cuts are being used, then make sure the correct other options are set
  // Namely: turn off presolve, heuristics, and dual reductions
  int strategy = paramVal[ParamIndices::BB_STRATEGY_PARAM_IND];
  if (strategy & BB_Strategy_Options::user_cuts) {
    strategy &= ~(BB_Strategy_Options::presolve_on);
    strategy |= BB_Strategy_Options::presolve_off;
    strategy &= ~(BB_Strategy_Options::heuristics_on);
    strategy |= BB_Strategy_Options::heuristics_off;
    strategy |= BB_Strategy_Options::use_best_bound;
    paramVal[ParamIndices::BB_STRATEGY_PARAM_IND] = strategy;
  }

  // Ensure that the on/off options are not both set at the same time
  bool param_error = false;
  if ((strategy & BB_Strategy_Options::all_cuts_on) 
      && (strategy & BB_Strategy_Options::all_cuts_off)) {
    param_error = true;
  }
  if ((strategy & BB_Strategy_Options::gmics_on) 
      && (strategy & BB_Strategy_Options::gmics_off)) {
    param_error = true;
  }
  if ((strategy & BB_Strategy_Options::presolve_on) 
      && (strategy & BB_Strategy_Options::presolve_off)) {
    param_error = true;
  }
  if ((strategy & BB_Strategy_Options::heuristics_on) 
      && (strategy & BB_Strategy_Options::heuristics_off)) {
    param_error = true;
  }
  if (param_error) {
    error_msg(errorstring, "Some bb_strategy option is both off and on at the same time.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }
 
  // If number of rounds is zero, turn off all of "our" cuts
  if (paramVal[ParamIndices::ROUNDS_PARAM_IND] == 0) {
    paramVal[ParamIndices::TILTED_DEPTH_PARAM_IND] = -1;
    paramVal[ParamIndices::VPC_DEPTH_PARAM_IND] = -1;
    paramVal[ParamIndices::PHA_PARAM_IND] = 0;
  }

  // Ensure parameters given to the problem are feasible
  if (paramVal[ParamIndices::NUM_RAYS_CUT_PARAM_IND] == 0) {
    paramVal[ParamIndices::NUM_RAYS_CUT_PARAM_IND] = numNB;
  }

  if (paramVal[ParamIndices::NUM_RAYS_CUT_PARAM_IND] < 0) {
    paramVal[ParamIndices::NUM_RAYS_CUT_PARAM_IND] = std::ceil(std::sqrt(numNB))
        / (-1 * paramVal[ParamIndices::NUM_RAYS_CUT_PARAM_IND]);
  }

  if (paramVal[ParamIndices::NUM_RAYS_CUT_PARAM_IND] > numNB) {
    warning_msg(warnstring,
        "The maximum number of rays that can be cut is %d. The value specified of %d is too high. Changing to the maximum number of rays.\n",
        numNB,
        paramVal[ParamIndices::NUM_RAYS_CUT_PARAM_IND]);
    paramVal[ParamIndices::NUM_RAYS_CUT_PARAM_IND] = numNB;
  }

  // If number of rays to be cut is STILL 0, there's a problem.
  // Note that there might be some strange things happening if
  // a variable is restricted to be integer, but it has a
  // bound that is fractional. We should clean the problem maybe
  // to ensure that we don't have non-basic integer-restricted
  // variables that are fractional.
  if (paramVal[ParamIndices::NUM_RAYS_CUT_PARAM_IND] == 0) {
    warning_msg(warnstring,
        "The number of non-basic columns is %d. There are no rays to cut.\n",
        numNB);
    return false;
  }

  // Check that the proper number of splits is available and the number of cut-generating sets is set correctly
  if (numSplits < 1) {
    warning_msg(warnstring,
        "The number of splits is %d. No cut-generating sets are possible.\n",
        numSplits);
    return false;
  }

  if ((paramVal[ParamIndices::CGS_PARAM_IND] > 0) && (numSplits < 2)) {
    warning_msg(warnstring,
        "The number of splits is %d. Cannot generate non-splits with fewer than 2 splits.\n", numSplits);
    return false;
  }

  if (paramVal[ParamIndices::MAX_FRAC_VAR_PARAM_IND] == 0) {
    paramVal[ParamIndices::MAX_FRAC_VAR_PARAM_IND] = numSplits;
  }
  if (paramVal[ParamIndices::MAX_FRAC_VAR_PARAM_IND] < 0) {
    paramVal[ParamIndices::MAX_FRAC_VAR_PARAM_IND] = std::ceil(
        std::sqrt(numSplits))
        / (-1 * paramVal[ParamIndices::MAX_FRAC_VAR_PARAM_IND]);
  }

  if (paramVal[ParamIndices::MAX_FRAC_VAR_PARAM_IND] > numSplits) {
    warning_msg(warnstring,
        "Number of requested fractional vars is %d, but the maximum number we can choose is %d. Reducing the requested number of frac vars.\n",
        paramVal[ParamIndices::MAX_FRAC_VAR_PARAM_IND], numSplits);
    paramVal[ParamIndices::MAX_FRAC_VAR_PARAM_IND] = numSplits;
  }

  const int max_num_cgs = getMaxNumCgs(
      paramVal[ParamIndices::MAX_FRAC_VAR_PARAM_IND],
      paramVal[ParamIndices::NUM_CGS_PARAM_IND]);
  if (paramVal[ParamIndices::NUM_CGS_PARAM_IND] <= 0) {
    paramVal[ParamIndices::NUM_CGS_PARAM_IND] = max_num_cgs;
  }

//  if (paramVal[ParamIndices::NUM_CGS_PARAM_IND]
//      < paramVal[ParamIndices::MAX_FRAC_VAR_PARAM_IND]) {
//    paramVal[ParamIndices::MAX_FRAC_VAR_PARAM_IND] =
//        paramVal[ParamIndices::NUM_CGS_PARAM_IND];
//  }

  if (paramVal[ParamIndices::NUM_CGS_PARAM_IND] > max_num_cgs) {
    warning_msg(warnstring,
        "Number of requested cut generating sets is %d, but the maximum number we can choose is %d. Reducing the requested number of splits.\n",
        paramVal[ParamIndices::NUM_CGS_PARAM_IND], max_num_cgs);
    paramVal[ParamIndices::NUM_CGS_PARAM_IND] = max_num_cgs;
  }

  if (!paramVal[ParamIndices::SICS_PARAM_IND]
      && paramVal[ParamIndices::PHA_PARAM_IND]) {
    if ((paramVal[ParamIndices::PHA_ACT_OPTION_PARAM_IND] == 1)
        || (paramVal[ParamIndices::NEXT_HPLANE_FINAL_PARAM_IND] == 0)) {
      error_msg(errorstring, "To use average depth to select hyperplanes, need to generate SICs.\n");
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }
  }

  // Definitely do not delete split variables when generating triangles
  if (paramVal[ParamIndices::CGS_PARAM_IND] > 0) {
    paramVal[ParamIndices::SPLIT_VAR_DELETED_PARAM_IND] = 0;
  }

  return true;
} /* finalizeParams */

/***********************************************************************/
void CglGICParam::printParams(FILE* outfile, const char SEP) const {
  for (int i = 0; i < ParamIndices::NUM_PARAMS; ++i) {
    fprintf(outfile, "%s%c%d\n", toLowerString(ParamName[i].c_str()).c_str(), SEP, paramVal[i]);
  }
  fprintf(outfile, "eps%c%e\n", SEP, EPS);
  fprintf(outfile, "rayeps%c%e\n", SEP, RAYEPS);
  fprintf(outfile, "away%c%e\n", SEP, AWAY);
  fprintf(outfile, "min_orthogonality%c%e\n", SEP, MIN_ORTHOGONALITY);
  fprintf(outfile, "min_viol_abs%c%e\n", SEP, MIN_VIOL_ABS);
  fprintf(outfile, "min_viol_rel%c%e\n", SEP, MIN_VIOL_REL);
  fprintf(outfile, "timelimit%c%e\n", SEP, TIMELIMIT);
  fflush(outfile);
} /* printParams */

/**********************************************************/
int CglGICParam::getMaxNumCgs(const int numSplits, const int limitOnNumCgs) const {
	int max_num_cgs = numSplits;
	if (paramVal[ParamIndices::CGS_PARAM_IND] > 0) {
		max_num_cgs = max_num_cgs * (max_num_cgs - 1) / 2;
	}
	if ((paramVal[ParamIndices::CGS_PARAM_IND] == 5)
			|| (paramVal[ParamIndices::CGS_PARAM_IND] == 6)) {
		max_num_cgs *= 4;
	}
	if (limitOnNumCgs < 0) {
		//max_num_cgs = std::ceil(std::sqrt(max_num_cgs)) / (-1 * limitOnNumCgs);
		max_num_cgs = numSplits / (-1 * limitOnNumCgs);
	}
	if ((limitOnNumCgs > 0) && (limitOnNumCgs < max_num_cgs)) {
		max_num_cgs = limitOnNumCgs;
	}
	return max_num_cgs;
} /* getMaxNumCgs */
