//============================================================================
// Name        : GlobalConstants.cpp
// Author      : akazachk
// Version     : 0.2014.01.10
// Copyright   : Your copyright notice
// Description : Miscellaneous useful definitions and functions used globally.
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#include "GlobalConstants.hpp"

namespace GlobalVariables {
  std::chrono::time_point<std::chrono::system_clock> start_time;
  std::string prob_name, in_f_name, in_f_name_stub, in_file_ext, out_f_name_stub;
  std::string LOG_FILE_NAME;
  std::FILE* log_file = stdout;

  std::vector<int> numCutSolverFails(
      GlobalConstants::CutSolverFails::NUM_CUTSOLVER_FAILS, 0);
  std::vector<int> numObjFromHeur(
      GlobalConstants::CutHeuristics::NUM_CUT_HEUR, 0);
  std::vector<int> numCutsFromHeur(
      GlobalConstants::CutHeuristics::NUM_CUT_HEUR, 0);
  int numCgsActuallyUsed = 0;
  int numCgsLeadingToCuts = 0;
  int numCgsOneDisjTermInfeas = 0;
  int minNodeDepth = std::numeric_limits<int>::max();
  int maxNodeDepth = -1;
  int numDisjTerms = 0;
  double minDensityPRLP = 1.;
  double maxDensityPRLP = 0.;
  int minNumRowsPRLP = std::numeric_limits<int>::max();
  int maxNumRowsPRLP = -1;
  int minNumColsPRLP = std::numeric_limits<int>::max();
  int maxNumColsPRLP = -1;
  int minNumPointsPRLP = std::numeric_limits<int>::max();
  int maxNumPointsPRLP = -1;
  int totalNumPointsPRLP = 0;
  int minNumRaysPRLP = std::numeric_limits<int>::max();
  int maxNumRaysPRLP = -1;
  int totalNumRaysPRLP = 0;
  int numPrunedNodes = 0;
  int numPartialBBNodes = 0;
  double bestObjValue = std::numeric_limits<double>::max();

  Stats timeStats;
  std::vector<std::string> time_name;
  CglGICParam saved_param;
  CglGICParam param;
  int random_seed = 628;

  // Instantiate timers
  void initialize() {
    if (LOG_FILE_NAME.empty()) {
      LOG_FILE_NAME = GlobalConstants::DEFAULT_LOG_FILE;
    }

    time_name.push_back(GlobalConstants::INIT_SOLVE_TIME);
    if (param.paramVal[CglGICParamNamespace::ParamIndices::SUBSPACE_PARAM_IND])
      time_name.push_back(GlobalConstants::GEN_SUB_TIME);
    time_name.push_back(GlobalConstants::GEN_MSICS_TIME);
    time_name.push_back(GlobalConstants::APPLY_MSICS_TIME);
    time_name.push_back(GlobalConstants::CHOOSE_HPLANES_TIME);
    time_name.push_back(GlobalConstants::GEN_INTERSECTION_POINTS_TIME);
    time_name.push_back(GlobalConstants::GEN_VERTICES_TIME);
    time_name.push_back(GlobalConstants::GEN_GMI_TIME);
    time_name.push_back(GlobalConstants::APPLY_GMI_TIME);
    time_name.push_back(GlobalConstants::GEN_TILTED_TIME);
    time_name.push_back(GlobalConstants::APPLY_TILTED_TIME);
    time_name.push_back(GlobalConstants::GEN_VPC_TIME);
    time_name.push_back(GlobalConstants::GEN_TREE_FOR_VPC_TIME);
    time_name.push_back(GlobalConstants::SETUP_PRLP_TIME);
    time_name.push_back(GlobalConstants::SOLVE_PRLP_TIME);
    time_name.push_back(GlobalConstants::APPLY_VPC_TIME);
    time_name.push_back(GlobalConstants::GEN_PHA_TIME);
    time_name.push_back(GlobalConstants::APPLY_PHA_TIME);
    time_name.push_back(GlobalConstants::GEN_MPHA_TIME);
    time_name.push_back(GlobalConstants::APPLY_MPHA_TIME);
    time_name.push_back(GlobalConstants::BB_TIME);
    time_name.push_back(GlobalConstants::COLLECT_STATS_TIME);
    for (int heur_ind = 0;
        heur_ind < GlobalConstants::CutHeuristics::NUM_CUT_HEUR; heur_ind++) {
      time_name.push_back(
          GlobalConstants::CutHeuristicsName[heur_ind] + "_TIME");
    }
    time_name.push_back(GlobalConstants::TOTAL_TIME);
    for (int t = 0; t < (int) time_name.size(); t++) {
      timeStats.register_name(time_name[t]);
    }
  }
}
