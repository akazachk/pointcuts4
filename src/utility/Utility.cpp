//============================================================================
// Name        : Utility.cpp
// Author      : akazachk
// Version     : 0.2013.05.11
// Copyright   : Your copyright notice
// Description : Used for functions that are useful but not particular to any
//               file/class/etc.
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include "GlobalConstants.hpp"

#include "Utility.hpp"
#include <sys/stat.h>
#include <numeric>  // inner_product

#include "CglSIC.hpp"
#include "Output.hpp"
#include "BBHelper.hpp"

#include "OsiChooseStrongCustom.hpp"

#include "CbcBranchStrongDecision.hpp"
#include "CbcBranchDefaultDecision.hpp"
#include "CbcBranchDynamic.hpp"

#include "CbcCompareDefault.hpp"
#include "CbcCompareBFS.hpp"
#include "CbcCompareDepth.hpp"
#include "CbcCompareEstimate.hpp"
#include "CbcCompareObjective.hpp"

/** Overload solve from hot start because of issues */
bool solveFromHotStart(OsiSolverInterface* const solver, const int col,
    const bool isChangedUB, const double origBound, const double newBound) {
  solver->solveFromHotStart();
  if (solver->isIterationLimitReached()) {
    // This sometimes happens, e.g., with arki001
    solver->unmarkHotStart();
    //solver->disableFactorization();
    solver->resolve();
    if (isChangedUB) {
      solver->setColUpper(col, origBound);
    } else {
      solver->setColLower(col, origBound);
    }
    solver->resolve();
    //solver->enableFactorization();
    solver->markHotStart();
    if (isChangedUB) {
      solver->setColUpper(col, newBound);
    } else {
      solver->setColLower(col, newBound);
    }
    solver->solveFromHotStart();
  }
  return (solver->isProvenOptimal());
} /* solveFromHotStart overload */

/**
 * Calculate strong branching lower bound
 */
double calculateStrongBranchingLB(OsiSolverInterface* solver) {
  const double* colSolution = solver->getColSolution();
  double strong_branching_lb = solver->getInfinity();

  try {
    setupClpForStrongBranching(dynamic_cast<OsiClpSolverInterface*>(solver));
  } catch (std::exception& e) {
    // It's okay, we can continue
  }
  solver->enableFactorization();
  solver->markHotStart();
  for (int col = 0; col < solver->getNumCols(); col++) {
  //int cols[2] = {877,924};
  //for ( auto col : cols ) {
    if (solver->isInteger(col)) {
      // Is the variable fractional in the solution?
      // If so, check the two branches; 
      // but do not do fixing if infeasibility detected.
      if (!isVal(colSolution[col], std::floor(colSolution[col]),
          param.getAWAY())
          && !isVal(colSolution[col], std::ceil(colSolution[col]),
              param.getAWAY())) {
        const double origLB = solver->getColLower()[col];
        const double origUB = solver->getColUpper()[col];
        double optValDown, optValUp;
        bool downBranchFeasible = true, upBranchFeasible = true;

        // Check down branch
        solver->setColUpper(col, std::floor(colSolution[col]));
        //solver->solveFromHotStart();
        solveFromHotStart(solver, col, true, origUB, std::floor(colSolution[col]));
        if (solver->isProvenOptimal()) {
          optValDown = solver->getObjValue();
        } else if (solver->isProvenPrimalInfeasible()) {
          downBranchFeasible = false;
        } else {
          // Something strange happened
          error_msg(errorstring,
              "Down branch is neither optimal nor primal infeasible on variable %d (value %e).\n",
              col, colSolution[col]);
          writeErrorToLog(errorstring, GlobalVariables::log_file);
          exit(1);
        }
        solver->setColUpper(col, origUB);

        // Return to previous state
        solver->solveFromHotStart();

        // Check up branch
        solver->setColLower(col, std::ceil(colSolution[col]));
        //solver->solveFromHotStart();
        solveFromHotStart(solver, col, true, origUB, std::ceil(colSolution[col]));
        if (solver->isProvenOptimal()) {
          optValUp = solver->getObjValue();
        } else if (solver->isProvenPrimalInfeasible()) {
          upBranchFeasible = false;
        } else {
          // Something strange happened
          error_msg(errorstring,
              "Up branch is neither optimal nor primal infeasible on variable %d (value %e).\n",
              col, colSolution[col]);
          writeErrorToLog(errorstring, GlobalVariables::log_file);
          exit(1);
        }
        solver->setColLower(col, origLB);

        // Return to original state
        solver->solveFromHotStart();

        // Check if some side of the split is infeasible
        if (!downBranchFeasible || !upBranchFeasible) {
          if (!downBranchFeasible && !upBranchFeasible) {
            // Infeasible problem
            error_msg(errorstring,
                "Infeasible problem due to integer variable %d (value %e).\n",
                col, colSolution[col]);
            writeErrorToLog(errorstring, GlobalVariables::log_file);
            exit(1);
          }
        } else {
          // Update strong branching value, if appropriate
          const double lowerOptVal = (optValDown < optValUp) ? optValDown : optValUp;
          if (lowerOptVal < strong_branching_lb) {
            strong_branching_lb = lowerOptVal;
          }
        }
      }
    } /* is this an integer variable? */
  } /* get strong branching lb */
  solver->unmarkHotStart();
  solver->disableFactorization();

  return strong_branching_lb;
} /* calculateStrongBranchingLB */

/**
 * Perform full cleaning and get statistics
 */
void performCleaning(OsiClpSolverInterface* solver, CglGICParam& param,
    const int CLEANING_MODE_OPTION) {
  const bool DO_BRANCHING_WITH_SICS = false;
  const bool DO_STRONG_BRANCHING = false;
  const bool DO_DEFAULT = true;
  const bool DO_CUTSON = false;
  const bool DO_CUTSOFF = false;
  const int numOrigRows = solver->getNumRows();
  const int numOrigCols = solver->getNumCols();
  int numBoundsChanged = 0;
  int numSBFixed = 0;
  const double origLPOpt = solver->getObjValue();
//  std::vector<double> solution;

  if (DO_STRONG_BRANCHING) {
#ifdef TRACE
    printf("Get strong branching lower bound for original instance.\n");
#endif
  }
  const double origSBLB =
      DO_STRONG_BRANCHING ?
          calculateStrongBranchingLB(solver) :
          -1. * GlobalVariables::param.getInfinity();
  solver->resolve();
  if (!checkSolverOptimality(solver, false)) {
    error_msg(errorstring, "Original solver is not optimal.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

#ifdef TRACE
  printf("Collecting information about original instance.\n");
#endif

  // Get number of integer and binary variables
  // As well as primal and dual degeneracy
  int numNonZero = solver->getMatrixByRow()->getNumElements(); 
  int numInteger = 0, numBinary = 0;
  int origPrimalDegen = 0, origDualDegen = 0;
  for (int col = 0; col < solver->getNumCols(); col++) {
    if (solver->isInteger(col)) {
      numInteger++;
      if (solver->isBinary(col)) {
        numBinary++;
      }
    }

    const bool isBasic = isBasicCol(solver, col);
    const double val = solver->getColSolution()[col];
    const double lb = solver->getColLower()[col];
    const double ub = solver->getColUpper()[col];
    if (isBasic && (isVal(val, lb) || isVal(val, ub))) {
      origPrimalDegen++;
    }
    if (!isBasic && isZero(solver->getReducedCost()[col])) {
      origDualDegen++;
    }
  } /* get col info */
  for (int row = 0; row < solver->getNumRows(); row++) {
    const bool isBasic = isBasicSlack(solver, row);
    const double val = std::abs(solver->getRowActivity()[row] - solver->getRightHandSide()[row]);
    if (isBasic && isZero(val)) {
      origPrimalDegen++;
    }
    if (!isBasic && isZero(solver->getRowPrice()[row])) {
      origDualDegen++;
    }
  } /* get row info */

  // Save the original branch-and-bound strategy
  const int strategy = param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND);
//  const int use_best_bound = (strategy & BB_Strategy_Options::use_best_bound) ? BB_Strategy_Options::use_best_bound : 0;
//  const int default_nousercuts_strategy = 0; //& ~(BB_Strategy_Options::user_cuts);
//  const int cutson_nousercuts_strategy = ((strategy & ~(BB_Strategy_Options::all_cuts_off)) | BB_Strategy_Options::all_cuts_on) & ~(BB_Strategy_Options::user_cuts);
//  const int cutsoff_nousercuts_strategy = ((strategy & ~(BB_Strategy_Options::all_cuts_on) & ~(BB_Strategy_Options::gmics_on)) | BB_Strategy_Options::all_cuts_off) & ~(BB_Strategy_Options::user_cuts);
  const int default_yesusercuts_strategy = strategy; //0 | BB_Strategy_Options::user_cuts | use_best_bound;
  const int cutson_yesusercuts_strategy = ((strategy & ~(BB_Strategy_Options::all_cuts_off)) | BB_Strategy_Options::all_cuts_on) | BB_Strategy_Options::user_cuts;
  const int cutsoff_yesusercuts_strategy = ((strategy & ~(BB_Strategy_Options::all_cuts_on) & ~(BB_Strategy_Options::gmics_on)) | BB_Strategy_Options::all_cuts_off) | BB_Strategy_Options::user_cuts;
  
  // Cbc
  BBInfo origBBInfoCbcDefault, origBBInfoCbcCutsOn, origBBInfoCbcCutsOff;
  if (strategy & BB_Strategy_Options::cbc) {
    if (DO_DEFAULT) {
      // Default
  #ifdef TRACE
    printf("Performing branch-and-bound with Cbc (default) on original instance.\n");
  #endif
      param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, default_yesusercuts_strategy);
      doBranchAndBoundNoCuts(solver, origBBInfoCbcDefault);
    }

    if (DO_CUTSON) {
      // Cuts on
#ifdef TRACE
  printf("Performing branch-and-bound with Cbc (cuts on) on original instance.\n");
#endif
      param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND,
          cutson_yesusercuts_strategy);
      doBranchAndBoundNoCuts(solver, origBBInfoCbcCutsOn);
    }

    if (DO_CUTSOFF) {
      // Cuts off
#ifdef TRACE
  printf("Performing branch-and-bound with Cbc (cuts off) on original instance.\n");
#endif
      param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND,
          cutsoff_yesusercuts_strategy);
      doBranchAndBoundNoCuts(solver, origBBInfoCbcCutsOff);
    }
  } /* cbc */
  
  // Cplex
  BBInfo origBBInfoCplexDefault, origBBInfoCplexCutsOn, origBBInfoCplexCutsOff;
#ifdef VPC_USE_CPLEX
  if (strategy & BB_Strategy_Options::cplex) {
    if (DO_DEFAULT) {
      // Default
  #ifdef TRACE
      printf("Performing branch-and-bound with Cplex (default) on original instance.\n");
  #endif
      param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, default_yesusercuts_strategy);
      doBranchAndBoundWithCplexCallable(GlobalVariables::in_f_name.c_str(), origBBInfoCplexDefault);
    }

    if (DO_CUTSON) {
      // Cuts on
#ifdef TRACE
      printf("Performing branch-and-bound with Cplex (cuts on) on original instance.\n");
#endif
      param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, cutson_yesusercuts_strategy);
      doBranchAndBoundWithCplexCallable(GlobalVariables::in_f_name.c_str(), origBBInfoCplexCutsOn);
    }

    if (DO_CUTSOFF) {
      // Cuts off
#ifdef TRACE
      printf("Performing branch-and-bound with Cplex (cuts off) on original instance.\n");
#endif
      param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, cutsoff_yesusercuts_strategy);
      doBranchAndBoundWithCplexCallable(GlobalVariables::in_f_name.c_str(), origBBInfoCplexCutsOff);
    }
  } /* cplex */
#endif

  // Gurobi
  BBInfo origBBInfoGurobiDefault, origBBInfoGurobiCutsOn, origBBInfoGurobiCutsOff;
#ifdef VPC_USE_GUROBI
  if (strategy & BB_Strategy_Options::gurobi) {
    if (DO_DEFAULT) {
      // Default
  #ifdef TRACE
      printf("Performing branch-and-bound with Gurobi (default) on original instance.\n");
  #endif
      param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, default_yesusercuts_strategy);
      doBranchAndBoundWithGurobi(GlobalVariables::in_f_name.c_str(), origBBInfoGurobiDefault);
    }

    if (DO_CUTSON) {
      // Cuts on
#ifdef TRACE
      printf("Performing branch-and-bound with Gurobi (cuts on) on original instance.\n");
#endif
      param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, cutson_yesusercuts_strategy);
      doBranchAndBoundWithGurobi(GlobalVariables::in_f_name.c_str(), origBBInfoGurobiCutsOn);
    }

    if (DO_CUTSOFF) {
      // Cuts off
#ifdef TRACE
      printf("Performing branch-and-bound with Gurobi (cuts off) on original instance.\n");
#endif
      param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, cutsoff_yesusercuts_strategy);
      doBranchAndBoundWithGurobi(GlobalVariables::in_f_name.c_str(), origBBInfoGurobiCutsOff);
    }
  } /* gurobi */
#endif

  OsiCuts structSICsOriginal, structSICsCleaned;
  OsiCuts structSICsOriginalRd2, structSICsCleanedRd2;
  OsiCuts structSICsOriginalStr, structSICsCleanedStr;
  OsiCuts structSICsOriginalRd2Str, structSICsCleanedRd2Str;
  PointCutsSolverInterface* SICSolver =
      dynamic_cast<PointCutsSolverInterface*>(solver->clone());

  // Unstrengthened Gomory cuts for the original solver
#ifdef TRACE
  printf("Get first round of unstrengthened Gomory cuts for original instance.\n");
#endif
  param.setParamVal(ParamIndices::STRENGTHEN_PARAM_IND, 0);
  CglSIC SICGenOriginal(param);
//  SICGenOriginal.setSolution(&solution);
  SICGenOriginal.generateCuts(*solver, structSICsOriginal);
  //SICSolver->saveBaseModel();
  SICSolver->applyCuts(structSICsOriginal);
  SICSolver->resolve();
  if (!checkSolverOptimality(SICSolver, false)) {
    error_msg(errorstring, "After SICs, original solver is not optimal.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }
  const double origSICOpt = SICSolver->getObjValue();
  
  // Round 2
#ifdef TRACE
  printf("Get second round of unstrengthened Gomory cuts for original instance.\n");
#endif
//  SICGenOriginal.clearCuts();
  SICGenOriginal.generateCuts(*SICSolver, structSICsOriginalRd2);
  SICSolver->applyCuts(structSICsOriginalRd2);
  SICSolver->resolve();
  if (!checkSolverOptimality(SICSolver, false)) {
    error_msg(errorstring, "After SICs rd 2, original solver is not optimal.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }
  const double origSICOptRd2 = SICSolver->getObjValue();
  SICSolver->restoreBaseModel(numOrigRows);
  SICSolver->resolve();

  // Strengthened Gomory cuts for the original solver
#ifdef TRACE
  printf("Get first round of strengthened Gomory cuts for original instance.\n");
#endif
  param.setParamVal(ParamIndices::STRENGTHEN_PARAM_IND, 1);
  CglSIC SICGenOriginalStr(param);
//  SICGenOriginalStr.setSolution(&solution);
  SICGenOriginalStr.generateCuts(*solver, structSICsOriginalStr);
  SICSolver->applyCuts(structSICsOriginalStr);
  SICSolver->resolve();
  if (!checkSolverOptimality(SICSolver, false)) {
    error_msg(errorstring,
        "After strengthened SICs, original solver is not optimal.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }
  const double origSICStrOpt = SICSolver->getObjValue();

  // Cbc
  BBInfo origSICStrBBInfoCbcDefault, origSICStrBBInfoCbcCutsOn, origSICStrBBInfoCbcCutsOff;
  BBInfo origSICStrBBInfoCplexDefault, origSICStrBBInfoCplexCutsOn, origSICStrBBInfoCplexCutsOff;
  BBInfo origSICStrBBInfoGurobiDefault, origSICStrBBInfoGurobiCutsOn, origSICStrBBInfoGurobiCutsOff;
  if (DO_BRANCHING_WITH_SICS) {
    if (strategy & BB_Strategy_Options::cbc) {
      if (DO_DEFAULT) {
        // Default
  #ifdef TRACE
        printf(
            "Performing branch-and-bound with Cbc (default) on original instance with GMICs.\n");
  #endif
        param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, default_yesusercuts_strategy);
        doBranchAndBoundNoCuts(SICSolver, origSICStrBBInfoCbcDefault);
      }

      if (DO_CUTSON) {
        // Cuts on
#ifdef TRACE
        printf(
            "Performing branch-and-bound with Cbc (cuts on) on original instance with GMICs.\n");
#endif
        param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, cutson_yesusercuts_strategy);
        doBranchAndBoundNoCuts(SICSolver, origSICStrBBInfoCbcCutsOn);
      }

      if (DO_CUTSOFF) {
        // Cuts off
#ifdef TRACE
        printf(
            "Performing branch-and-bound with Cbc (cuts off) on original instance with GMICs.\n");
#endif
        param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, cutsoff_yesusercuts_strategy);
        doBranchAndBoundNoCuts(SICSolver, origSICStrBBInfoCbcCutsOff);
      }
    } /* cbc */

    // Cplex
#ifdef VPC_USE_CPLEX
    if (strategy & BB_Strategy_Options::cplex) {
      if (DO_DEFAULT) {
        // Default
  #ifdef TRACE
        printf("Performing branch-and-bound with Cplex (default) on original instance with GMICs.\n");
  #endif
        param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, default_yesusercuts_strategy);
        doBranchAndBoundWithUserCutsCplexCallable(GlobalVariables::in_f_name.c_str(), &structSICsOriginalStr, origSICStrBBInfoCplexDefault);
      }

      if (DO_CUTSON) {
        // Cuts on
#ifdef TRACE
        printf("Performing branch-and-bound with Cplex (cuts on) on original instance with GMICs.\n");
#endif
        param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, cutson_yesusercuts_strategy);
        doBranchAndBoundWithUserCutsCplexCallable(GlobalVariables::in_f_name.c_str(), &structSICsOriginalStr, origSICStrBBInfoCplexCutsOn);
      }

      if (DO_CUTSOFF) {
        // Cuts off
#ifdef TRACE
        printf("Performing branch-and-bound with Cplex (cuts off) on original instance with GMICs.\n");
#endif
        param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, cutsoff_yesusercuts_strategy);
        doBranchAndBoundWithUserCutsCplexCallable(GlobalVariables::in_f_name.c_str(), &structSICsOriginalStr, origSICStrBBInfoCplexCutsOff);
      }
    } /* cplex */
#endif

    // Gurobi
#ifdef VPC_USE_GUROBI
    if (strategy & BB_Strategy_Options::gurobi) {
      if (DO_DEFAULT) {
        // Default
  #ifdef TRACE
        printf(
            "Performing branch-and-bound with Gurobi (default) on original instance with GMICs.\n");
  #endif
        param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, default_yesusercuts_strategy);
        doBranchAndBoundWithUserCutsGurobi(GlobalVariables::in_f_name.c_str(),
            &structSICsOriginalStr, origSICStrBBInfoGurobiDefault);
      }

      if (DO_CUTSON) {
        // Cuts on
#ifdef TRACE
        printf(
            "Performing branch-and-bound with Gurobi (cuts on) on original instance with GMICs.\n");
#endif
        param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, cutson_yesusercuts_strategy);
        doBranchAndBoundWithUserCutsGurobi(GlobalVariables::in_f_name.c_str(),
            &structSICsOriginalStr, origSICStrBBInfoGurobiCutsOn);
      }

      if (DO_CUTSOFF) {
        // Cuts off
#ifdef TRACE
        printf(
            "Performing branch-and-bound with Gurobi (cuts off) on original instance with GMICs.\n");
#endif
        param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, cutsoff_yesusercuts_strategy);
        doBranchAndBoundWithUserCutsGurobi(GlobalVariables::in_f_name.c_str(),
            &structSICsOriginalStr, origSICStrBBInfoGurobiCutsOff);
      }
    } /* gurobi */
#endif
  } /* do branching with sics? */

  // Round 2
#ifdef TRACE
  printf("Get second round of strengthened Gomory cuts for original instance.\n");
#endif
//  SICGenOriginalStr.clearCuts();
  SICGenOriginalStr.generateCuts(*SICSolver, structSICsOriginalRd2Str);
  SICSolver->applyCuts(structSICsOriginalRd2Str);
  SICSolver->resolve();
  if (!checkSolverOptimality(SICSolver, false)) {
    error_msg(errorstring,
        "After strengthened SICs rd 2, original solver is not optimal.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }
  const double origSICStrOptRd2 = SICSolver->getObjValue();
  SICSolver->restoreBaseModel(numOrigRows);
  SICSolver->resolve();

  /********** Now we do the cleaning **********/
  std::string presolved_name_stub =
      (CLEANING_MODE_OPTION <= 1) ?
          GlobalVariables::in_f_name_stub + "_presolved" : "";
  const std::string cleaned_name =
      (CLEANING_MODE_OPTION <= 1) ?
          presolved_name_stub :
          GlobalVariables::in_f_name_stub + "_cleaned.mps";

  // First get presolved opt using CPLEX / Gurobi
  double presolvedLPOptCplex = 0., presolvedLPOptGurobi = 0.;
#ifdef VPC_USE_CPLEX
  if (strategy & BB_Strategy_Options::cplex) {
#ifdef TRACE
    printf("Presolve model with Cplex.\n");
#endif
    presolveModelWithCplexCallable(
        GlobalVariables::in_f_name.c_str(), presolvedLPOptCplex,
        presolved_name_stub); // returns mps.gz
    solver->readMps(presolved_name_stub.c_str());

    // Make sure we are doing a minimization problem; this is just to make later
    // comparisons simpler (i.e., a higher LP obj after adding the cut is better).
    if (solver->getObjSense() < param.getEPS()) {
      printf(
          "\n## Detected maximization problem. Negating objective function to make it minimization. ##\n");
      solver->setObjSense(1.0);
      const double* obj = solver->getObjCoefficients();
      for (int col = 0; col < solver->getNumCols(); col++) {
        solver->setObjCoeff(col, -1. * obj[col]);
      }
      double objOffset = 0.;
      solver->getDblParam(OsiDblParam::OsiObjOffset, objOffset);
      if (objOffset != 0.) {
        solver->setDblParam(OsiDblParam::OsiObjOffset, -1. * objOffset);
      }
    }

    // Perform initial solve
    solver->initialSolve();
    if (!checkSolverOptimality(solver, false)) {
      error_msg(errorstring, "After initial solve, solver is not optimal.\n");
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }

    if (CLEANING_MODE_OPTION > 1) {
      remove(presolved_name_stub.c_str()); // remove the temporary file
    }
  }
#endif
#ifdef VPC_USE_GUROBI
  if (strategy & BB_Strategy_Options::gurobi) {
#ifdef TRACE
    printf("Presolve model with Gurobi.\n");
#endif
    presolveModelWithGurobi(GlobalVariables::in_f_name.c_str(),
        presolvedLPOptGurobi, presolved_name_stub); // returns mps.gz
    solver->readMps(presolved_name_stub.c_str());

    // Make sure we are doing a minimization problem; this is just to make later
    // comparisons simpler (i.e., a higher LP obj after adding the cut is better).
    if (solver->getObjSense() < 1e-3) {
      printf(
          "\n## Detected maximization problem. Negating objective function to make it minimization. ##\n");
      solver->setObjSense(1.0);
      const double* obj = solver->getObjCoefficients();
      for (int col = 0; col < solver->getNumCols(); col++) {
        solver->setObjCoeff(col, -1. * obj[col]);
      }
      double objOffset = 0.;
      solver->getDblParam(OsiDblParam::OsiObjOffset, objOffset);
      if (objOffset != 0.) {
        solver->setDblParam(OsiDblParam::OsiObjOffset, -1. * objOffset);
      }
    }

    // Perform initial solve
    solver->initialSolve();
    if (!checkSolverOptimality(solver, false)) {
      error_msg(errorstring, "After initial solve, solver is not optimal.\n");
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }

    if (CLEANING_MODE_OPTION > 1) {
      remove(presolved_name_stub.c_str()); // remove the temporary file
    }
  }
#endif

  // Now clean using our own methods
  bool is_clean = (CLEANING_MODE_OPTION <= 1);
  while (!is_clean) {
#ifdef TRACE
    printf("Clean model with custom method (remove one-sided split disjunctions and tighten bounds, iteratively).\n");
#endif
    is_clean = cleanProblem(solver, numBoundsChanged, numSBFixed);
  }

  // Get new solver info, including primal and dual degeneracy values
  const int cleanedNumCols = solver->getNumCols();
  const int cleanedNumRows = solver->getNumRows();
  const int cleanedNumNonZero = solver->getMatrixByRow()->getNumElements();
  int cleanedNumInteger = 0, cleanedNumBinary = 0;
  int cleanedPrimalDegen = 0, cleanedDualDegen = 0;
  int cleanedNumSICs = 0, cleanedNumSICsRd2 = 0, cleanedNumSICsStr = 0, cleanedNumSICsStrRd2 = 0;
  double cleanedSICOpt = 0., cleanedSICOptRd2 = 0.;
  double cleanedSICStrOpt = 0., cleanedSICStrOptRd2 = 0.;
//  std::vector<double> cleanedSolution;

  // Resolve
  if (cleanedNumNonZero > 0) {
    solver->resolve();
    if (!checkSolverOptimality(solver, false)) {
      error_msg(errorstring, "After cleaning, solver is not optimal.\n");
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }
  }
  const double cleanedLPOpt = (cleanedNumNonZero > 0) ? solver->getObjValue() : GlobalVariables::bestObjValue;

  if (CLEANING_MODE_OPTION > 1) {
    // Save cleaned LP to in directory
    solver->writeMps(cleaned_name.c_str(), "", solver->getObjSense());
  }

  // Set up new BBInfos
  BBInfo cleanedBBInfoCbcDefault, cleanedSICStrBBInfoCbcDefault;
  BBInfo cleanedBBInfoCbcCutsOn, cleanedSICStrBBInfoCbcCutsOn;
  BBInfo cleanedBBInfoCbcCutsOff, cleanedSICStrBBInfoCbcCutsOff;

  BBInfo cleanedBBInfoCplexDefault, cleanedSICStrBBInfoCplexDefault;
  BBInfo cleanedBBInfoCplexCutsOn, cleanedSICStrBBInfoCplexCutsOn;
  BBInfo cleanedBBInfoCplexCutsOff, cleanedSICStrBBInfoCplexCutsOff;

  BBInfo cleanedBBInfoGurobiDefault, cleanedSICStrBBInfoGurobiDefault;
  BBInfo cleanedBBInfoGurobiCutsOn, cleanedSICStrBBInfoGurobiCutsOn;
  BBInfo cleanedBBInfoGurobiCutsOff, cleanedSICStrBBInfoGurobiCutsOff;

  const bool was_cleaned = (numSBFixed > 0) || (numBoundsChanged > 0)
      || (cleanedNumCols != numOrigCols)
      || (cleanedNumRows != numOrigRows)
      || (cleanedNumNonZero != numNonZero);
  if (cleanedNumNonZero > 0 && was_cleaned) {
#ifdef TRACE
  printf("Collecting information about cleaned instance.\n");
#endif
    for (int col = 0; col < solver->getNumCols(); col++) {
      if (solver->isInteger(col)) {
        cleanedNumInteger++;
        if (solver->isBinary(col)) {
          cleanedNumBinary++;
        }
      }

      const bool isBasic = isBasicCol(solver, col);
      const double val = solver->getColSolution()[col];
      const double lb = solver->getColLower()[col];
      const double ub = solver->getColUpper()[col];
      if (isBasic && (isVal(val, lb) || isVal(val, ub))) {
        cleanedPrimalDegen++;
      }
      if (!isBasic && isZero(solver->getReducedCost()[col])) {
        cleanedDualDegen++;
      }
    }
    for (int row = 0; row < solver->getNumRows(); row++) {
      const bool isBasic = isBasicSlack(solver, row);
      const double val = std::abs(solver->getRowActivity()[row] - solver->getRightHandSide()[row]);
      if (isBasic && isZero(val)) {
        cleanedPrimalDegen++;
      }
      if (!isBasic && isZero(solver->getRowPrice()[row])) {
        cleanedDualDegen++;
      }
    }

    // Cbc
    if (strategy & BB_Strategy_Options::cbc) {
      if (DO_DEFAULT) {
        // Default
  #ifdef TRACE
        printf("Performing branch-and-bound with Cbc (default) on cleaned instance.\n");
  #endif
        param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, default_yesusercuts_strategy);
        doBranchAndBoundNoCuts(solver, cleanedBBInfoCbcDefault);
      }

      if (DO_CUTSON) {
        // Cuts on
#ifdef TRACE
        printf("Performing branch-and-bound with Cbc (cuts on) on cleaned instance.\n");
#endif
        param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, cutson_yesusercuts_strategy);
        doBranchAndBoundNoCuts(solver, cleanedBBInfoCbcCutsOn);
      }

      if (DO_CUTSOFF) {
        // Cuts off
#ifdef TRACE
        printf("Performing branch-and-bound with Cbc (cuts off) on cleaned instance.\n");
#endif
        param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, cutsoff_yesusercuts_strategy);
        doBranchAndBoundNoCuts(solver, cleanedBBInfoCbcCutsOff);
      }
    } /* cbc */

#ifdef VPC_USE_CPLEX
    // Cplex
    if (strategy & BB_Strategy_Options::cplex) {
      if (DO_DEFAULT) {
        // Default
  #ifdef TRACE
        printf("Performing branch-and-bound with Cplex (default) on cleaned instance.\n");
  #endif
        param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, default_yesusercuts_strategy);
        doBranchAndBoundWithCplexCallable(cleaned_name.c_str(), cleanedBBInfoCplexDefault);
      }

      if (DO_CUTSON) {
        // Cuts on
#ifdef TRACE
        printf("Performing branch-and-bound with Cplex (cuts on) on cleaned instance.\n");
#endif
        param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, cutson_yesusercuts_strategy);
        doBranchAndBoundWithCplexCallable(cleaned_name.c_str(), cleanedBBInfoCplexCutsOn);
      }

      if (DO_CUTSOFF) {
        // Cuts off
#ifdef TRACE
        printf("Performing branch-and-bound with Cplex (cuts off) on cleaned instance.\n");
#endif
        param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, cutsoff_yesusercuts_strategy);
        doBranchAndBoundWithCplexCallable(cleaned_name.c_str(), cleanedBBInfoCplexCutsOff);
      }
    } /* cplex */
#endif

#ifdef VPC_USE_GUROBI
    // Gurobi
    if (strategy & BB_Strategy_Options::gurobi) {
      if (DO_DEFAULT) {
        // Default
  #ifdef TRACE
        printf("Performing branch-and-bound with Gurobi (default) on cleaned instance.\n");
  #endif
        param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, default_yesusercuts_strategy);
        doBranchAndBoundWithGurobi(cleaned_name.c_str(), cleanedBBInfoGurobiDefault);
      }

      if (DO_CUTSON) {
        // Cuts on
#ifdef TRACE
        printf("Performing branch-and-bound with Gurobi (cuts on) on cleaned instance.\n");
#endif
        param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, cutson_yesusercuts_strategy);
        doBranchAndBoundWithGurobi(cleaned_name.c_str(), cleanedBBInfoGurobiCutsOn);
      }

      if (DO_CUTSOFF) {
        // Cuts off
#ifdef TRACE
        printf("Performing branch-and-bound with Gurobi (cuts off) on cleaned instance.\n");
#endif
        param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, cutsoff_yesusercuts_strategy);
        doBranchAndBoundWithGurobi(cleaned_name.c_str(), cleanedBBInfoGurobiCutsOff);
      }
    } /* gurobi */
#endif

    // Update SICSolver
    delete SICSolver;
    SICSolver = dynamic_cast<PointCutsSolverInterface*>(solver->clone());

    // Unstrengthened Gomory cuts for the new solver
#ifdef TRACE
    printf("Generate first round of unstrengthened Gomory cuts for cleaned instance.\n");
#endif
    param.setParamVal(ParamIndices::STRENGTHEN_PARAM_IND, 0);
    CglSIC SICGenCleaned(param);
//    SICGenCleaned.setSolution(&cleanedSolution);
    SICGenCleaned.generateCuts(*solver, structSICsCleaned);
    //SICSolver->saveBaseModel();
    SICSolver->applyCuts(structSICsCleaned);
    SICSolver->resolve();
    if (!checkSolverOptimality(SICSolver, false)) {
      error_msg(errorstring, "After SICs, cleaned solver is not optimal.\n");
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }
    cleanedNumSICs = structSICsCleaned.sizeCuts();
    cleanedSICOpt = SICSolver->getObjValue();

    // Round 2
#ifdef TRACE
    printf("Generate second round of unstrengthened Gomory cuts for cleaned instance.\n");
#endif
//    SICGenCleaned.clearCuts();
    SICGenCleaned.generateCuts(*SICSolver, structSICsCleanedRd2);
    SICSolver->applyCuts(structSICsCleanedRd2);
    SICSolver->resolve();
    if (!checkSolverOptimality(SICSolver, false)) {
      error_msg(errorstring,
          "After SICs rd 2, cleaned solver is not optimal.\n");
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }
    cleanedNumSICsRd2 = structSICsCleanedRd2.sizeCuts();
    cleanedSICOptRd2 = SICSolver->getObjValue();
    SICSolver->restoreBaseModel(solver->getNumRows());
    SICSolver->resolve();

    // Strengthened Gomory cuts for the new solver
#ifdef TRACE
    printf("Generate first round of strengthened Gomory cuts for cleaned instance.\n");
#endif
    param.setParamVal(ParamIndices::STRENGTHEN_PARAM_IND, 1);
    CglSIC SICGenCleanedStr(param);
//    SICGenCleanedStr.setSolution(&cleanedSolution);
    SICGenCleanedStr.generateCuts(*solver, structSICsCleanedStr);
    SICSolver->applyCuts(structSICsCleanedStr);
    SICSolver->resolve();
    if (!checkSolverOptimality(SICSolver, false)) {
      error_msg(errorstring,
          "After strengthened SICs, cleaned solver is not optimal.\n");
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }
    cleanedNumSICsStr = structSICsCleanedStr.sizeCuts();
    cleanedSICStrOpt = SICSolver->getObjValue();

    if (DO_BRANCHING_WITH_SICS) {
      // Cbc
      if (strategy & BB_Strategy_Options::cbc) {
        if (DO_DEFAULT) {
          // Default
  #ifdef TRACE
          printf(
              "Performing branch-and-bound with Cbc (default) on cleaned instance with GMICs.\n");
  #endif
          param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND,
              default_yesusercuts_strategy);
          doBranchAndBoundNoCuts(SICSolver, cleanedSICStrBBInfoCbcDefault);
        }

        if (DO_CUTSON) {
          // Cuts on
#ifdef TRACE
          printf(
              "Performing branch-and-bound with Cbc (cuts on) on cleaned instance with GMICs.\n");
#endif
          param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND,
              cutson_yesusercuts_strategy);
          doBranchAndBoundNoCuts(SICSolver, cleanedSICStrBBInfoCbcCutsOn);
        }

        if (DO_CUTSOFF) {
          // Cuts off
#ifdef TRACE
          printf(
              "Performing branch-and-bound with Cbc (cuts off) on cleaned instance with GMICs.\n");
#endif
          param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND,
              cutsoff_yesusercuts_strategy);
          doBranchAndBoundNoCuts(SICSolver, cleanedSICStrBBInfoCbcCutsOff);
        }
      } /* cbc */

#ifdef VPC_USE_CPLEX
      // Cplex
      if (strategy & BB_Strategy_Options::cplex) {
        if (DO_DEFAULT) {
          // Default
  #ifdef TRACE
          printf("Performing branch-and-bound with Cplex (default) on cleaned instance with GMICs.\n");
  #endif
          param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, default_yesusercuts_strategy);
          doBranchAndBoundWithUserCutsCplexCallable(cleaned_name.c_str(), &structSICsCleanedStr, cleanedSICStrBBInfoCplexDefault);
        }

        if (DO_CUTSON) {
          // Cuts on
#ifdef TRACE
          printf("Performing branch-and-bound with Cplex (cuts on) on cleaned instance with GMICs.\n");
#endif
          param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, cutson_yesusercuts_strategy);
          doBranchAndBoundWithUserCutsCplexCallable(cleaned_name.c_str(), &structSICsCleanedStr, cleanedSICStrBBInfoCplexCutsOn);
        }

        if (DO_CUTSOFF) {
          // Cuts off
#ifdef TRACE
          printf("Performing branch-and-bound with Cplex (cuts off) on cleaned instance with GMICs.\n");
#endif
          param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, cutsoff_yesusercuts_strategy);
          doBranchAndBoundWithUserCutsCplexCallable(cleaned_name.c_str(), &structSICsCleanedStr, cleanedSICStrBBInfoCplexCutsOff);
        }
      } /* cplex */
#endif

#ifdef VPC_USE_GUROBI
      // Gurobi
      if (strategy & BB_Strategy_Options::gurobi) {
        if (DO_DEFAULT) {
          // Default
  #ifdef TRACE
          printf(
              "Performing branch-and-bound with Gurobi (default) on cleaned instance with GMICs.\n");
  #endif
          param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND,
              default_yesusercuts_strategy);
          doBranchAndBoundWithUserCutsGurobi(cleaned_name.c_str(),
              &structSICsCleanedStr, cleanedSICStrBBInfoGurobiDefault);
        }

        if (DO_CUTSON) {
          // Cuts on
#ifdef TRACE
          printf(
              "Performing branch-and-bound with Gurobi (cuts on) on cleaned instance with GMICs.\n");
#endif
          param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND,
              cutson_yesusercuts_strategy);
          doBranchAndBoundWithUserCutsGurobi(cleaned_name.c_str(),
              &structSICsCleanedStr, cleanedSICStrBBInfoGurobiCutsOn);
        }

        if (DO_CUTSOFF) {
          // Cuts off
#ifdef TRACE
          printf(
              "Performing branch-and-bound with Gurobi (cuts off) on cleaned instance with GMICs.\n");
#endif
          param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND,
              cutsoff_yesusercuts_strategy);
          doBranchAndBoundWithUserCutsGurobi(cleaned_name.c_str(),
              &structSICsCleanedStr, cleanedSICStrBBInfoGurobiCutsOff);
        }
      } /* gurobi */
#endif
    } /* do branching with sics? */

    // Round 2
#ifdef TRACE
    printf("Generate second round of unstrengthened Gomory cuts for cleaned instance.\n");
#endif
//    SICGenCleanedStr.clearCuts();
    SICGenCleanedStr.generateCuts(*SICSolver, structSICsCleanedRd2Str);
    SICSolver->applyCuts(structSICsCleanedRd2Str);
    SICSolver->resolve();
    if (!checkSolverOptimality(SICSolver, false)) {
      error_msg(errorstring,
          "After strengthened SICs rd 2, cleaned solver is not optimal.\n");
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }
    cleanedNumSICsStrRd2 = structSICsCleanedRd2Str.sizeCuts();
    cleanedSICStrOptRd2 = SICSolver->getObjValue();
  } /* check if any cleaning was performed */
  else if (cleanedNumNonZero == 0) {
    presolvedLPOptCplex = GlobalVariables::bestObjValue;
    presolvedLPOptGurobi = GlobalVariables::bestObjValue;
    cleanedSICOpt = GlobalVariables::bestObjValue;
    cleanedSICOptRd2 = GlobalVariables::bestObjValue;
    cleanedSICStrOpt = GlobalVariables::bestObjValue;
    cleanedSICStrOptRd2 = GlobalVariables::bestObjValue;

    // Cbc
    initializeBBInfo(cleanedBBInfoCbcDefault, GlobalVariables::bestObjValue);
    initializeBBInfo(cleanedBBInfoCbcCutsOn, GlobalVariables::bestObjValue);
    initializeBBInfo(cleanedBBInfoCbcCutsOff, GlobalVariables::bestObjValue);
    initializeBBInfo(cleanedSICStrBBInfoCbcDefault, GlobalVariables::bestObjValue);
    initializeBBInfo(cleanedSICStrBBInfoCbcCutsOn, GlobalVariables::bestObjValue);
    initializeBBInfo(cleanedSICStrBBInfoCbcCutsOff, GlobalVariables::bestObjValue);

    // Cplex
    initializeBBInfo(cleanedBBInfoCplexDefault, GlobalVariables::bestObjValue);
    initializeBBInfo(cleanedBBInfoCplexCutsOn, GlobalVariables::bestObjValue);
    initializeBBInfo(cleanedBBInfoCplexCutsOff, GlobalVariables::bestObjValue);
    initializeBBInfo(cleanedSICStrBBInfoCplexDefault, GlobalVariables::bestObjValue);
    initializeBBInfo(cleanedSICStrBBInfoCplexCutsOn, GlobalVariables::bestObjValue);
    initializeBBInfo(cleanedSICStrBBInfoCplexCutsOff, GlobalVariables::bestObjValue);

    // Gurobi
    initializeBBInfo(cleanedBBInfoGurobiDefault, GlobalVariables::bestObjValue);
    initializeBBInfo(cleanedBBInfoGurobiCutsOn, GlobalVariables::bestObjValue);
    initializeBBInfo(cleanedBBInfoGurobiCutsOff, GlobalVariables::bestObjValue);
    initializeBBInfo(cleanedSICStrBBInfoGurobiDefault, GlobalVariables::bestObjValue);
    initializeBBInfo(cleanedSICStrBBInfoGurobiCutsOn, GlobalVariables::bestObjValue);
    initializeBBInfo(cleanedSICStrBBInfoGurobiCutsOff, GlobalVariables::bestObjValue);
  } /* check if cleaning yielded an empty problem */
  else {
    cleanedNumInteger = numInteger;
    cleanedNumBinary = numBinary;
    cleanedPrimalDegen = origPrimalDegen;
    cleanedDualDegen = origDualDegen;

    // Cbc
    cleanedBBInfoCbcDefault = origBBInfoCbcDefault;
    cleanedBBInfoCbcCutsOn = origBBInfoCbcCutsOn;
    cleanedBBInfoCbcCutsOff= origBBInfoCbcCutsOff;

    cleanedSICStrBBInfoCbcDefault = origSICStrBBInfoCbcDefault;
    cleanedSICStrBBInfoCbcCutsOn = origSICStrBBInfoCbcCutsOn;
    cleanedSICStrBBInfoCbcCutsOff = origSICStrBBInfoCbcCutsOff;

    // Cplex
    cleanedBBInfoCplexDefault = origBBInfoCplexDefault;
    cleanedBBInfoCplexCutsOn = origBBInfoCplexCutsOn;
    cleanedBBInfoCplexCutsOff = origBBInfoCplexCutsOff;

    cleanedSICStrBBInfoCplexDefault = origSICStrBBInfoCplexDefault;
    cleanedSICStrBBInfoCplexCutsOn = origSICStrBBInfoCplexCutsOn;
    cleanedSICStrBBInfoCplexCutsOff = origSICStrBBInfoCplexCutsOff;

    // Gurobi
    cleanedBBInfoGurobiDefault = origBBInfoGurobiDefault;
    cleanedBBInfoGurobiCutsOn = origBBInfoGurobiCutsOn;
    cleanedBBInfoGurobiCutsOff = origBBInfoGurobiCutsOff;

    cleanedSICStrBBInfoGurobiDefault = origSICStrBBInfoGurobiDefault;
    cleanedSICStrBBInfoGurobiCutsOn = origSICStrBBInfoGurobiCutsOn;
    cleanedSICStrBBInfoGurobiCutsOff = origSICStrBBInfoGurobiCutsOff;

    cleanedNumSICs = structSICsOriginal.sizeCuts();
    cleanedNumSICsRd2 = structSICsOriginalRd2.sizeCuts();
    cleanedNumSICsStr = structSICsOriginalStr.sizeCuts();
    cleanedNumSICsStrRd2 = structSICsOriginalRd2Str.sizeCuts();
    cleanedSICOpt = origSICOpt;
    cleanedSICOptRd2 = origSICOptRd2;
    cleanedSICStrOpt = origSICStrOpt;
    cleanedSICStrOptRd2 = origSICStrOptRd2;
  } /* strong branching did nothing so do not repeat the experiments */

  // Finally, get the new strong branching lb
  if (DO_STRONG_BRANCHING) {
#ifdef TRACE
    printf("Get strong branching lower bound for cleaned instance.\n");
#endif
  }
  const double cleanedSBLB =
      DO_STRONG_BRANCHING ?
          calculateStrongBranchingLB(solver) :
          -1 * GlobalVariables::param.getINFINITY();
  
  // Reset B&B strategy
  param.setParamVal(ParamIndices::BB_STRATEGY_PARAM_IND, strategy);

  //// Original problem
  writeEntryToLog(numOrigRows, GlobalVariables::log_file);
  writeEntryToLog(numOrigCols, GlobalVariables::log_file);
  writeEntryToLog(numNonZero, GlobalVariables::log_file);
  writeEntryToLog(numInteger, GlobalVariables::log_file);
  writeEntryToLog(numBinary, GlobalVariables::log_file);
  writeEntryToLog(origLPOpt, GlobalVariables::log_file);
  writeEntryToLog(origSBLB, GlobalVariables::log_file);
  writeEntryToLog(origPrimalDegen, GlobalVariables::log_file);
  writeEntryToLog(origDualDegen, GlobalVariables::log_file);
  writeEntryToLog(structSICsOriginal.sizeCuts(), GlobalVariables::log_file);
  writeEntryToLog(origSICOpt, GlobalVariables::log_file);
  writeEntryToLog(structSICsOriginalRd2.sizeCuts(), GlobalVariables::log_file);
  writeEntryToLog(origSICOptRd2, GlobalVariables::log_file);
  writeEntryToLog(structSICsOriginalStr.sizeCuts(), GlobalVariables::log_file);
  writeEntryToLog(origSICStrOpt, GlobalVariables::log_file);
  writeEntryToLog(structSICsOriginalRd2Str.sizeCuts(), GlobalVariables::log_file);
  writeEntryToLog(origSICStrOptRd2, GlobalVariables::log_file);
  // Cbc
  if (param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND) & BB_Strategy_Options::cbc) {
    printBBInfo(origBBInfoCbcDefault, GlobalVariables::log_file, !DO_DEFAULT);
    printBBInfo(origBBInfoCbcCutsOn, GlobalVariables::log_file, !DO_CUTSON);
    printBBInfo(origBBInfoCbcCutsOff, GlobalVariables::log_file, !DO_CUTSOFF);
    printBBInfo(origSICStrBBInfoCbcDefault, GlobalVariables::log_file,
        !(DO_BRANCHING_WITH_SICS && DO_DEFAULT));
    printBBInfo(origSICStrBBInfoCbcCutsOn, GlobalVariables::log_file,
        !(DO_BRANCHING_WITH_SICS && DO_CUTSON));
    printBBInfo(origSICStrBBInfoCbcCutsOff, GlobalVariables::log_file,
        !(DO_BRANCHING_WITH_SICS && DO_CUTSOFF));
  } /* cbc */
  else {
    for (int i = 0; i < (int) BB_INFO_CONTENTS.size() * 6; i++) {
      writeEntryToLog("", GlobalVariables::log_file);
    }
  }
  // Cplex
  if (param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND) & BB_Strategy_Options::cplex) {
    printBBInfo(origBBInfoCplexDefault, GlobalVariables::log_file, !DO_DEFAULT);
    printBBInfo(origBBInfoCplexCutsOn, GlobalVariables::log_file, !DO_CUTSON);
    printBBInfo(origBBInfoCplexCutsOff, GlobalVariables::log_file, !DO_CUTSOFF);
    printBBInfo(origSICStrBBInfoCplexDefault, GlobalVariables::log_file,
        !(DO_BRANCHING_WITH_SICS && DO_DEFAULT));
    printBBInfo(origSICStrBBInfoCplexCutsOn, GlobalVariables::log_file,
        !(DO_BRANCHING_WITH_SICS && DO_CUTSON));
    printBBInfo(origSICStrBBInfoCplexCutsOff, GlobalVariables::log_file,
        !(DO_BRANCHING_WITH_SICS && DO_CUTSOFF));
  } /* cplex */
  else {
    for (int i = 0; i < (int) BB_INFO_CONTENTS.size() * 6; i++) {
      writeEntryToLog("", GlobalVariables::log_file);
    }
  }
  // Gurobi
  if (param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND) & BB_Strategy_Options::gurobi) {
    printBBInfo(origBBInfoGurobiDefault, GlobalVariables::log_file, !DO_DEFAULT);
    printBBInfo(origBBInfoGurobiCutsOn, GlobalVariables::log_file, !DO_CUTSON);
    printBBInfo(origBBInfoGurobiCutsOff, GlobalVariables::log_file,
        !DO_CUTSOFF);
    printBBInfo(origSICStrBBInfoGurobiDefault, GlobalVariables::log_file,
        !(DO_BRANCHING_WITH_SICS && DO_DEFAULT));
    printBBInfo(origSICStrBBInfoGurobiCutsOn, GlobalVariables::log_file,
        !(DO_BRANCHING_WITH_SICS && DO_CUTSON));
    printBBInfo(origSICStrBBInfoGurobiCutsOff, GlobalVariables::log_file,
        !(DO_BRANCHING_WITH_SICS && DO_CUTSOFF));
  } /* gurobi */
  else {
    for (int i = 0; i < (int) BB_INFO_CONTENTS.size() * 6; i++) {
      writeEntryToLog("", GlobalVariables::log_file);
    }
  }
  //// Cleaned problem
  writeEntryToLog(solver->getNumRows(), GlobalVariables::log_file);
  writeEntryToLog(solver->getNumCols(), GlobalVariables::log_file);
  writeEntryToLog(cleanedNumNonZero, GlobalVariables::log_file);
  writeEntryToLog(cleanedNumInteger, GlobalVariables::log_file);
  writeEntryToLog(cleanedNumBinary, GlobalVariables::log_file);
  writeEntryToLog(numBoundsChanged, GlobalVariables::log_file);
  writeEntryToLog(numSBFixed, GlobalVariables::log_file);
  if (param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND) & BB_Strategy_Options::cplex) {
    writeEntryToLog(presolvedLPOptCplex, GlobalVariables::log_file);
  } else {
    writeEntryToLog("", GlobalVariables::log_file);
  }
  if (param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND) & BB_Strategy_Options::gurobi) {
    writeEntryToLog(presolvedLPOptGurobi, GlobalVariables::log_file);
  } else {
    writeEntryToLog("", GlobalVariables::log_file);
  }
  writeEntryToLog(cleanedLPOpt, GlobalVariables::log_file);
  writeEntryToLog(cleanedSBLB, GlobalVariables::log_file);
  writeEntryToLog(cleanedPrimalDegen, GlobalVariables::log_file);
  writeEntryToLog(cleanedDualDegen, GlobalVariables::log_file);
  writeEntryToLog(cleanedNumSICs, GlobalVariables::log_file);
  writeEntryToLog(cleanedSICOpt, GlobalVariables::log_file);
  writeEntryToLog(cleanedNumSICsRd2, GlobalVariables::log_file);
  writeEntryToLog(cleanedSICOptRd2, GlobalVariables::log_file);
  writeEntryToLog(cleanedNumSICsStr, GlobalVariables::log_file);
  writeEntryToLog(cleanedSICStrOpt, GlobalVariables::log_file);
  writeEntryToLog(cleanedNumSICsStrRd2, GlobalVariables::log_file);
  writeEntryToLog(cleanedSICStrOptRd2, GlobalVariables::log_file);
  // Cbc
  if (param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND) & BB_Strategy_Options::cbc) {
    printBBInfo(cleanedBBInfoCbcDefault, GlobalVariables::log_file, !DO_DEFAULT);
    printBBInfo(cleanedBBInfoCbcCutsOn, GlobalVariables::log_file, !DO_CUTSON);
    printBBInfo(cleanedBBInfoCbcCutsOff, GlobalVariables::log_file,
        !DO_CUTSOFF);
    printBBInfo(cleanedSICStrBBInfoCbcDefault, GlobalVariables::log_file,
        !(DO_BRANCHING_WITH_SICS && DO_DEFAULT));
    printBBInfo(cleanedSICStrBBInfoCbcCutsOn, GlobalVariables::log_file,
        !(DO_BRANCHING_WITH_SICS && DO_CUTSON));
    printBBInfo(cleanedSICStrBBInfoCbcCutsOff, GlobalVariables::log_file,
        !(DO_BRANCHING_WITH_SICS && DO_CUTSOFF));
  } /* cbc */
  else {
    for (int i = 0; i < (int) BB_INFO_CONTENTS.size() * 6; i++) {
      writeEntryToLog("", GlobalVariables::log_file);
    }
  }
  // Cplex
  if (param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND) & BB_Strategy_Options::cplex) {
    printBBInfo(cleanedBBInfoCplexDefault, GlobalVariables::log_file,
        !DO_DEFAULT);
    printBBInfo(cleanedBBInfoCplexCutsOn, GlobalVariables::log_file,
        !DO_CUTSON);
    printBBInfo(cleanedBBInfoCplexCutsOff, GlobalVariables::log_file,
        !DO_CUTSOFF);
    printBBInfo(cleanedSICStrBBInfoCplexDefault, GlobalVariables::log_file,
        !(DO_BRANCHING_WITH_SICS && DO_DEFAULT));
    printBBInfo(cleanedSICStrBBInfoCplexCutsOn, GlobalVariables::log_file,
        !(DO_BRANCHING_WITH_SICS && DO_CUTSON));
    printBBInfo(cleanedSICStrBBInfoCplexCutsOff, GlobalVariables::log_file,
        !(DO_BRANCHING_WITH_SICS && DO_CUTSOFF));
  } /* cplex */
  else {
    for (int i = 0; i < (int) BB_INFO_CONTENTS.size() * 6; i++) {
      writeEntryToLog("", GlobalVariables::log_file);
    }
  }
  // Gurobi
  if (param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND) & BB_Strategy_Options::gurobi) {
    printBBInfo(cleanedBBInfoGurobiDefault, GlobalVariables::log_file,
        !DO_DEFAULT);
    printBBInfo(cleanedBBInfoGurobiCutsOn, GlobalVariables::log_file,
        !DO_CUTSON);
    printBBInfo(cleanedBBInfoGurobiCutsOff, GlobalVariables::log_file,
        !DO_CUTSOFF);
    printBBInfo(cleanedSICStrBBInfoGurobiDefault, GlobalVariables::log_file,
        !(DO_BRANCHING_WITH_SICS && DO_DEFAULT));
    printBBInfo(cleanedSICStrBBInfoGurobiCutsOn, GlobalVariables::log_file,
        !(DO_BRANCHING_WITH_SICS && DO_CUTSON));
    printBBInfo(cleanedSICStrBBInfoGurobiCutsOff, GlobalVariables::log_file,
        !(DO_BRANCHING_WITH_SICS && DO_CUTSOFF));
  } /* gurobi */
  else {
    for (int i = 0; i < (int) BB_INFO_CONTENTS.size() * 6; i++) {
      writeEntryToLog("", GlobalVariables::log_file);
    }
  }
  writeEntryToLog(GlobalVariables::timeStats.get_total_time(INIT_SOLVE_TIME),
      GlobalVariables::log_file);
  if (SICSolver) {
    delete SICSolver;
  }
} /* performCleaning */

/**
 * Makes sure no variable bounds can be tightened,
 * including via strong branching
 */
bool cleanProblem(OsiClpSolverInterface* solver, int& numBoundsChanged,
    int& numSBFixed) {
  bool is_clean = true;

  const int numCols = solver->getNumCols();
//  std::vector<double> origObjCoeff(solver->getObjCoefficients(),
//      solver->getObjCoefficients() + numCols);
  std::vector<double> origColSolution(solver->getColSolution(),
        solver->getColSolution() + numCols);
  std::vector<double> obj(numCols, 0.);

  // Set up solver for checking bounds
  OsiClpSolverInterface* boundSolver =
      dynamic_cast<OsiClpSolverInterface*>(solver->clone());
  boundSolver->setObjective(obj.data());
  boundSolver->setObjSense(solver->getObjSense());
  double objOffset = 0.;
  solver->getDblParam(OsiDblParam::OsiObjOffset, objOffset);
  boundSolver->setDblParam(OsiDblParam::OsiObjOffset, objOffset);

  // For strong branching
  setupClpForStrongBranching(solver);
  solver->enableFactorization();
  solver->markHotStart();
  for (int col = 0; col < numCols; col++) {
    // If col is integral, ensure bounds are integral
    // If the integer variable is fractional, we also strong branch
    if (solver->isInteger(col)) {
      // Is the variable fractional in the solution?
      if (!isVal(origColSolution[col], std::floor(origColSolution[col]),
          param.getAWAY())
          && !isVal(origColSolution[col], std::ceil(origColSolution[col]),
              param.getAWAY())) {
        const double origLB = solver->getColLower()[col];
        const double origUB = solver->getColUpper()[col];
        bool downBranchFeasible = true, upBranchFeasible = true;

        // Check down branch
        solver->setColUpper(col, std::floor(origColSolution[col]));
        //solver->solveFromHotStart();
        solveFromHotStart(solver, col, true, origUB, std::floor(origColSolution[col]));
        if (solver->isProvenPrimalInfeasible()) {
          downBranchFeasible = false;
        }
        solver->setColUpper(col, origUB);

        // Return to original state
        solver->solveFromHotStart();

        // Check up branch
        solver->setColLower(col, std::ceil(origColSolution[col]));
        //solver->solveFromHotStart();
        solveFromHotStart(solver, col, false, origLB, std::ceil(origColSolution[col]));
        if (solver->isProvenPrimalInfeasible()) {
          upBranchFeasible = false;
        }
        solver->setColLower(col, origLB);

        // Return to original state
        solver->solveFromHotStart();

        // Check if some side of the split is infeasible
        if (!downBranchFeasible || !upBranchFeasible) {
          if (!downBranchFeasible && !upBranchFeasible) {
            // Infeasible problem
            error_msg(errorstring,
                "Infeasible problem due to integer variable %d (value %e).\n",
                col, origColSolution[col]);
            writeErrorToLog(errorstring, GlobalVariables::log_file);
            exit(1);
          }
          numSBFixed++;
          is_clean = false;
          if (!downBranchFeasible) {
            boundSolver->setColLower(col, std::ceil(origColSolution[col]));
          } else {
            boundSolver->setColUpper(col, std::floor(origColSolution[col]));
          }
        }
      } /* end strong branching */

      // Check integrality of LB
      if (!isVal(solver->getColLower()[col],
          std::floor(solver->getColLower()[col]))) {
        numBoundsChanged++;
        is_clean = false;
        boundSolver->setColLower(col, std::ceil(solver->getColLower()[col]));
      }

      // Check integrality of UB
      if (!isVal(solver->getColUpper()[col],
          std::ceil(solver->getColUpper()[col]))) {
        numBoundsChanged++;
        is_clean = false;
        boundSolver->setColUpper(col, std::floor(solver->getColUpper()[col]));
      }
    } /* check if integer */

    // Check if LB can be tightened
    boundSolver->setObjCoeff(col, 1.);
    boundSolver->resolve();
    if (checkSolverOptimality(boundSolver, false)) {
      if (greaterThanVal(boundSolver->getColSolution()[col],
          solver->getColLower()[col])) {
        numBoundsChanged++;
        is_clean = false;
        boundSolver->setColLower(col, boundSolver->getColSolution()[col]);
      }
    }

    // Check if UB can be tightened
    boundSolver->setObjCoeff(col, -1.);
    boundSolver->resolve();
    if (checkSolverOptimality(boundSolver, false)) {
      if (lessThanVal(boundSolver->getColSolution()[col],
          solver->getColUpper()[col])) {
        numBoundsChanged++;
        is_clean = false;
        boundSolver->setColUpper(col, boundSolver->getColSolution()[col]);
      }
    }

    // Reset
    boundSolver->setObjCoeff(col, 0.);
  } /* end iterating over columns */
  solver->unmarkHotStart();
  solver->disableFactorization();

  // Change upper and lower bounds wherever needed
  for (int col = 0; col < solver->getNumCols(); col++) {
    solver->setColLower(col, boundSolver->getColLower()[col]);
    solver->setColUpper(col, boundSolver->getColUpper()[col]);
  } /* end iterating over columns to update solver */

  if (boundSolver) {
    delete boundSolver;
  }

  return is_clean;
} /* cleanProblem */

bool checkReducedCosts(OsiSolverInterface* const solver) {
  if (!solver->isProvenOptimal()) {
    return false;
  }

  int num_changed = 0;
  for (int j = 0; j < solver->getNumCols(); j++) {
    const double redCost = solver->getReducedCost()[j];
    const double val = solver->getColSolution()[j];
    const double lb = solver->getColLower()[j];
    const double ub = solver->getColUpper()[j];
    if (isVal(val, lb) && redCost * solver->getObjSense() > 0) {
      num_changed++;
      solver->setColLower(j, lb - 1e20);
    } else if (isVal(val, ub) && redCost * solver->getObjSense() < 0) {
      num_changed++;
      solver->setColUpper(j, ub + 1e20);
    }
  }

  return (num_changed > 0);
} /* checkReducedCosts */

/**
 * @brief Checks whether a solver is optimal and resolves a few times if there are some infeasibilities; doRedCostFixing is disabled no matter what
 */
bool checkSolverOptimality(OsiSolverInterface* const solver,
    const bool exitOnDualInfeas, const double timeLimit,
    const bool doRedCostFixing, const int maxNumResolves) {
  OsiClpSolverInterface* clpsolver = NULL;
  try {
    clpsolver = dynamic_cast<OsiClpSolverInterface*>(solver);
  } catch (std::exception& e) {
    // Disregard
  }

  // If it is supposedly proven primal infeasible, might be good to do a resolve first
  // This is, e.g., something that arises with miplib2003/pp08aCUTS_presolved -32
  int resolve_count = 0;
  if (clpsolver->isProvenPrimalInfeasible()) {
    clpsolver->getModelPtr()->setNumberIterations(0);
    clpsolver->getModelPtr()->setMaximumSeconds(timeLimit);
    clpsolver->resolve();
    resolve_count++;
  }

  // First clean
  if (resolve_count < maxNumResolves && clpsolver) {
    int status = clpsolver->getModelPtr()->secondaryStatus();
    bool is_cleaned = false;
    if (status == 2 || !isZero(clpsolver->getModelPtr()->sumPrimalInfeasibilities())) {
      clpsolver->getModelPtr()->cleanup(1);
      is_cleaned = true;
    } else if (status == 3 || status == 9 || !isZero(clpsolver->getModelPtr()->sumDualInfeasibilities())) {
      clpsolver->getModelPtr()->cleanup(2);
      is_cleaned = true;
    } else if (status == 4) {
      clpsolver->getModelPtr()->cleanup(1);
      is_cleaned = true;
    }
    if (is_cleaned) {
      //      clpsolver->getModelPtr()->setNumberIterations(0);
      //      clpsolver->getModelPtr()->setMaximumSeconds(timeLimit);
      //      clpsolver->resolve();
      resolve_count++;
//      return checkSolverOptimality(solver, exitOnDualInfeas, timeLimit,
//          doRedCostFixing, maxNumResolves - resolve_count);
      return checkSolverOptimality(solver, exitOnDualInfeas, timeLimit,
          doRedCostFixing, 0);
    }
//    if (status == 2 || status == 3 || status == 4) {
//      clpsolver->getModelPtr()->cleanup(1);
//      clpsolver->getModelPtr()->setNumberIterations(0);
//      clpsolver->getModelPtr()->setMaximumSeconds(timeLimit);
//      clpsolver->resolve();
//      if (clpsolver->isProvenPrimalInfeasible()) {
//        // Something has gone wrong, as it was deemed feasible earlier, to get here.
//        // For blend2 str=2 depth-64, we see a case that initially the solver is optimal,
//        // but it becomes primal infeasible after resolving;
//        // this seems bad, but the resolve, on the other hand, has helped clean up solutions,
//        // such as with [example].
//        // Moreover, for the blend2 case at least, if we proceed,
//        // then the cut generation ends up being very time-costly,
//        // due to constantly resolving or hitting the time limit.
//        clpsolver->getModelPtr()->setProblemStatus(4); // say abandoned
//        return false;
//      }
//      return checkSolverOptimality(solver, exitOnDualInfeas, timeLimit, doRedCostFixing,
//          maxNumResolves - 1);
//    }

    // Do some resolves, but first save whether the initial status is dual infeasible
    // The reason is that for neos15 w/str=2, we were getting a dual infeasible problem
    // turn into a primal infeasible problem after resolves 
    // (probably the strengthening causes numerical issues)
    const bool oldStatusDualInfeasible = (clpsolver->isProvenDualInfeasible());
    const bool oldStatusOptimal = (clpsolver->isProvenOptimal());
    bool resolve = true;
    double infeas = std::numeric_limits<double>::max();
    while (resolve && resolve_count < maxNumResolves) {
      const double curr_infeas =
          clpsolver->getModelPtr()->sumPrimalInfeasibilities()
              + clpsolver->getModelPtr()->sumDualInfeasibilities();
      resolve = (resolve_count == 0)
          || (!isZero(curr_infeas) && (curr_infeas < infeas));
      if (resolve) {
        clpsolver->getModelPtr()->setNumberIterations(0);
        clpsolver->getModelPtr()->setMaximumSeconds(timeLimit);
        clpsolver->resolve();
        resolve_count++;
      }
      infeas = curr_infeas;
    }

    // If dual infeas -> primal infeas, do a dual resolve, which should fix the issue
    if (oldStatusDualInfeasible && clpsolver->isProvenPrimalInfeasible()) {
      clpsolver->getModelPtr()->dual(2,0); // just do values pass
      if (!clpsolver->isProvenDualInfeasible() && !clpsolver->isProvenOptimal()) {
        clpsolver->getModelPtr()->setProblemStatus(2);
        resolve_count = maxNumResolves; // stop resolving
      }
    }

    if (oldStatusOptimal && clpsolver->isProvenPrimalInfeasible()) {
      clpsolver->enableFactorization();
      clpsolver->getModelPtr()->setNumberIterations(0);
      clpsolver->getModelPtr()->setMaximumSeconds(timeLimit);
      clpsolver->resolve();
      resolve_count++;
      clpsolver->disableFactorization();
    }

    // Clean once more if needed and possible
    status = clpsolver->getModelPtr()->secondaryStatus();
    is_cleaned = false;
    if (status == 2
        || !isZero(clpsolver->getModelPtr()->sumPrimalInfeasibilities())) {
      clpsolver->getModelPtr()->cleanup(1);
      is_cleaned = true;
    } else if (status == 3 || status == 9
        || !isZero(clpsolver->getModelPtr()->sumDualInfeasibilities())) {
      clpsolver->getModelPtr()->cleanup(2);
      is_cleaned = true;
    } else if (status == 4) {
      clpsolver->getModelPtr()->cleanup(1);
      is_cleaned = true;
    }
    if (is_cleaned) {
      //      clpsolver->getModelPtr()->setNumberIterations(0);
      //      clpsolver->getModelPtr()->setMaximumSeconds(timeLimit);
      //      clpsolver->resolve();
      resolve_count++;
//      return checkSolverOptimality(solver, exitOnDualInfeas, timeLimit,
//          doRedCostFixing, maxNumResolves - resolve_count);
      return checkSolverOptimality(solver, exitOnDualInfeas, timeLimit,
          doRedCostFixing, 0);
    }
  }

  // If it is a PRLP and optimal, check the reduced costs
  // If any have changed, resolve (and recheck optimality)
  if (false && doRedCostFixing && checkReducedCosts(solver)) {
    if (clpsolver) {
      clpsolver->getModelPtr()->setNumberIterations(0);
      clpsolver->getModelPtr()->setMaximumSeconds(timeLimit);
    }
    solver->resolve();
    resolve_count++;
    checkSolverOptimality(solver, exitOnDualInfeas, timeLimit, doRedCostFixing,
        maxNumResolves - resolve_count);
  }

  if (solver->isProvenPrimalInfeasible()) {
    return false;
  } else if (!(solver->isProvenOptimal())) {
    // Sometimes need to resolve once more to get the correct status
    if (resolve_count < maxNumResolves) {
      solver->resolve();
    }
    if (solver->isProvenPrimalInfeasible()) {
      return false;
    } else if (solver->isProvenDualInfeasible()) {
      if (exitOnDualInfeas) {
        error_msg(errstr,
            "Solver is dual infeasible. Check why this happened!\n");
        writeErrorToLog(errstr, GlobalVariables::log_file);
        exit(1);
      } else {
        return false;
      }
    } else if (!(solver->isProvenOptimal())) {
      return false;
    }
  }

  return true;
} /* checkSolverOptimality */

bool enableFactorization(OsiSolverInterface* const solver, const int resolveFlag) {
  solver->enableFactorization();

  if (resolveFlag > 0) {
    try {
      OsiClpSolverInterface* clpsolver =
          dynamic_cast<OsiClpSolverInterface*>(solver);

      // After enabling factorization, things sometimes look different and it is worth resolving
      // This is motivated by seymour-disj-10; after adding one round of GMICs, row 5077 had negative row price
      if ((clpsolver->getModelPtr()->sumPrimalInfeasibilities() > param.getEPS())
          || (clpsolver->getModelPtr()->sumDualInfeasibilities()
              > param.getEPS())) {
        if (resolveFlag == 1) {
          clpsolver->initialSolve(); // hopefully this fixes things rather than breaks things; actually it breaks things, e.g., for rd2 SICs for coral/neos17, the variable basic in row 648 changes from 164 to 23
        } else {
          clpsolver->resolve();
        }
        clpsolver->disableFactorization();
        clpsolver->enableFactorization(); // this will hopefully solve the neos17 problem
      }
    } catch (std::exception& e) {
      // Disregard
    }
  }

  return solver->isProvenOptimal();
} /* enableFactorization */

/**
 * @brief Parses int from string using strtol
 */
bool parseInt(const char *str, int& val) {
  long tmpval;
  bool rc = parseLong(str, tmpval);
  val = static_cast<int>(tmpval);
  return rc;
} /* parseInt */

/**
 * @brief Parses long int from string using strtol
 */
bool parseLong(const char *str, long& val) {
  char *temp;
  bool rc = true;
  errno = 0;
  val = strtol(str, &temp, 10);

  if (temp == str || *temp != '\0' ||
      ((val == std::numeric_limits<long>::min() || val == std::numeric_limits<long>::max()) && errno == ERANGE))
    rc = false;

  return rc;
} /* parseLong */

/**
 * @brief Parses double from string using strtod
 */
bool parseDouble(const char *str, double& val) {
  char *temp;
  bool rc = true;
  errno = 0;
  val = strtod(str, &temp);

  if (temp == str || *temp != '\0' ||
      ((val == std::numeric_limits<double>::min() || val == std::numeric_limits<double>::lowest()
          || val == std::numeric_limits<double>::max()) && errno == ERANGE))
    rc = false;

  return rc;
} /* parseDouble */

/**
 * @brief Convert string of longs to vector of doubles
 */
bool parseStringOfLongs(std::string str, std::vector<long>& vec,
    const std::string& delimiter) {
  size_t pos = 0;
  long val = 0.;
  std::string token;
  while ((pos = str.find(delimiter)) != std::string::npos) {
    token = str.substr(0, pos);
    str.erase(0, pos + delimiter.length());
    if (!parseLong(token.c_str(), val)) {
      return false;
    }
    vec.push_back(val);
  }
  if (!str.empty()) {
    if (!parseLong(str.c_str(), val)) {
      return false;
    }
    vec.push_back(val);
  }
  return true;
} /* parseStringOfLongs */

/**
 * @brief Convert string of doubles to vector of doubles
 */
bool parseStringOfDoubles(std::string str, std::vector<double>& vec,
    const std::string& delimiter) {
  size_t pos = 0;
  double val = 0.;
  std::string token;
  while ((pos = str.find(delimiter)) != std::string::npos) {
    token = str.substr(0, pos);
    str.erase(0, pos + delimiter.length());
    if (!parseDouble(token.c_str(), val)) {
      return false;
    }
    vec.push_back(val);
  }
  if (!str.empty()) {
    if (!parseDouble(str.c_str(), val)) {
      return false;
    }
    vec.push_back(val);
  }
  return true;
} /* parseStringOfDoubles */

/**
 * @brief Way to check the cut limit (useful because of the large number of parameters that determine it)
 */
int getCutLimit() {
  if (param.getParamVal(ParamIndices::OVERLOAD_MAX_CUTS_PARAM_IND) > 0) {
    // Don't forget to convert to double (or multiply by 1.0)
    if (param.getParamVal(ParamIndices::LIMIT_CUTS_PER_CGS_PARAM_IND) != 0) {
      return std::ceil(
          1. * param.getParamVal(ParamIndices::OVERLOAD_MAX_CUTS_PARAM_IND)
              / param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND));
    } else {
      return param.getParamVal(ParamIndices::OVERLOAD_MAX_CUTS_PARAM_IND);
    }
  } else if (param.getParamVal(OVERLOAD_MAX_CUTS_PARAM_IND) < 0) {
    if (param.getParamVal(ParamIndices::LIMIT_CUTS_PER_CGS_PARAM_IND) != 0) {
      return std::ceil(
          -1. * param.getParamVal(ParamIndices::OVERLOAD_MAX_CUTS_PARAM_IND)
              * param.getParamVal(ParamIndices::MAX_FRAC_VAR_PARAM_IND)
              / param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND));
    } else {
      return -1. * param.getParamVal(ParamIndices::OVERLOAD_MAX_CUTS_PARAM_IND)
          * param.getParamVal(ParamIndices::MAX_FRAC_VAR_PARAM_IND);
    }
  } else if (param.getParamVal(ParamIndices::LIMIT_CUTS_PER_CGS_PARAM_IND) == 1) {
    return param.getParamVal(ParamIndices::NUM_CUTS_PER_CGS_PARAM_IND);
  } else {
    return (param.getParamVal(ParamIndices::NUM_CGS_PARAM_IND)
        * param.getParamVal(ParamIndices::NUM_CUTS_PER_CGS_PARAM_IND));
  }
} /* getCutLimit */

/**
 * @brief Universal way to check whether we reached the limit for the number of cuts for each split
 * This allows us to change between restricting number of cuts per split and total number of cuts easily
 */
bool reachedCutLimit(const int num_cuts_this_cgs, const int num_cuts_total) {
  const int one_sided_cuts = GlobalVariables::numCutsFromHeur[CutHeuristics::ONE_SIDED_CUT_HEUR];
  const int num_cuts = -one_sided_cuts
      + ((param.getParamVal(ParamIndices::LIMIT_CUTS_PER_CGS_PARAM_IND) == 0) ?
          num_cuts_total : num_cuts_this_cgs);
  return (num_cuts >= getCutLimit());
} /* reachedCutLimit */

///**
// * @brief Universal way to check whether we reached the limit for the time for some subroutine
// */
//bool reachedTimeLimit(const double time_spent, const double max_time) {
//  return (time_spent > max_time);
//} /* reachedTimeLimit */

/**
 * @brief Universal way to check whether we reached the limit for the time for some subroutine
 */
bool reachedTimeLimit(const Stats& timer, const std::string& timeName,
    const double max_time) {
  return (timer.get_total_time(timeName) > max_time);
} /* reachedTimeLimit */

/**
 * @brief Checks whether too many unsuccessful objective attempts have been made
 *
 * There are four types of checks. The first three have non-decreasing success requirements.
 * 1. Few cuts have been generated:
 *      default is FEW_CUTS = 1 cut, and the success threshold is at least 1 cut every 20 obj (fail ratio = .95).
 * 2. Many cuts have been generated:
 *      default is MANY_CUTS = .25 * CUT_LIMIT, and the success threshold is at least 1 cut every 10 obj (fail ratio = .90).
 * 3. Many obj have been tried, i.e., we have been trying for a long time so we better be successful super often:
 *      MANY_OBJ = max(FEW_CUTS / (1-few_cuts_fail_threshold), MANY_CUTS / (1-many_cuts_fail_threshold));
 *      default = max(20, 2.5 * CUT_LIMIT) and the success threshold is at least 1 cut every 5 obj (fail ratio = .80).
 * 4. Time is too long and we are not too successful:
 *       # obj tried >= MANY_OBJ && time >= 10 && average time / obj >= CUTSOLVER_TIMELIMIT + 1
 *       the success threshold is at least 1 cut every 3 obj
 *
 * Examples:
 * If the failure ratio is .85, then this will return true only if # obj >= MANY_OBJ.
 * This means that, as long as we have not tried too many times unsuccessfully,
 * then even if we have MANY_CUTS cuts, we would like more, and we feel that we are doing pretty well generating them.
 * (Of course, we have not hit the cut limit yet.)
 *
 * If your current failure ratio (# fails / # obj) is .93, then this will return true if # cuts >= MANY_CUTS, as .93 > .90.
 * Note that if # cuts < MANY_CUTS, but # obj >= MANY_OBJ,
 * then we would have failed earlier based on the more stringent limit for the MANY_OBJ case.
 *
 * If the failure ratio is .99, then we got here with # cuts < MANY_CUTS and # obj < MANY_OBJ.
 * Otherwise, if # cuts >= MANY_CUTS, then we would have hit the failure limit earlier (the first time it went above .90).
 * Similarly, if # obj >= MANY_OBJ, then we would have hit the limit even earlier.
 *
 * If the failure ratio is 1 (all failures), then we will reject if # obj > FEW_CUTS / (1.-few_cuts_fail_threshold).
 */
bool reachedFailureLimit(const int num_cuts, const int num_fails, const double time,
    const double few_cuts_fail_threshold, const double many_cuts_fail_threshold,
    const double many_obj_fail_threshold, const double time_fail_threshold) {
  const int num_obj_tried = num_cuts + num_fails;
  if (num_obj_tried == 0) {
    return false;
  }
  const int CUT_LIMIT = getCutLimit();
  const int FEW_CUTS = 1;
  const int NO_CUTS_OBJ_LIMIT = std::ceil(FEW_CUTS / (1. - few_cuts_fail_threshold));
  const int MANY_CUTS = std::ceil(.25 * CUT_LIMIT);
  const int MANY_OBJ = CoinMax(
      std::ceil(FEW_CUTS / (1. - few_cuts_fail_threshold)),
      std::ceil(MANY_CUTS / (1. - many_cuts_fail_threshold)));
  const double fail_ratio = (double) num_fails / num_obj_tried; 
  bool reached_limit = false;
  if (num_obj_tried >= MANY_OBJ && greaterThanVal(fail_ratio, many_obj_fail_threshold)) {
    reached_limit = true;
  } else if (num_cuts >= MANY_CUTS && greaterThanVal(fail_ratio, many_cuts_fail_threshold)) {
    reached_limit = true;
  } else if (num_cuts >= FEW_CUTS && greaterThanVal(fail_ratio, few_cuts_fail_threshold)) {
    reached_limit = true;
  } else if (num_cuts < FEW_CUTS && num_obj_tried >= NO_CUTS_OBJ_LIMIT) {
    reached_limit = true;
  }
  if (!reached_limit && num_obj_tried >= MANY_OBJ && time > 10.
      && time / num_obj_tried >= param.getCUTSOLVER_TIMELIMIT()) {
    reached_limit = true;
  }
  if (reached_limit) {
    GlobalVariables::numCutSolverFails[CutSolverFails::FAIL_LIMIT_NO_OBJ_FAIL_IND]++;
  }
  return reached_limit;
} /* reachedFailureLimit */

/**
 * Get the two norm of a row, using its packed form
 */
double getRowTwoNorm(const int row, const CoinPackedMatrix* const mat) {
  const int start = mat->getVectorFirst(row);
  const int size = mat->getVectorSize(row);
  double prod = 0.;
  for (int i = start; i < start + size; i++) {
    const double elem = mat->getElements()[i];
    prod += elem * elem;
  }
  prod = std::sqrt(prod);
  return prod;
} /* getRowTwoNorm */

/**
 * Compare two double arrays for equality
 * No effort is made to check that both have the same number of elements
 */
bool vectorsEqual(const int n, const double* vec1, const double* vec2) {
  for (int i = 0; i < n; i++) {
    if (!isZero(vec1[i] - vec2[i])) {
      return false;
    }
  }
  return true;
} /* vectorsEqual */

/********************************************************************************/
/**
 * @brief Decide if two rows are the same.
 * @return
 *   0: seem same in coeff and rhs,
 *   +/-1: seem same in coeff, but diff in rhs (-1: cut1 better, +1: cut2 better),
 *   2: seem different in coeff and rhs.
 *
 * The value of eps will be used to determine whether a cut coefficient is zero or not.
 * We are roughly checking that whether cut1 = ratio * cut2 for some ratio.
 * Let ratio = rhs1 / rhs2. (If both are non-zero. Typically both are = 1.)
 * We say the cuts are "truly different" if |coeff^1_j - ratio * coeff^2_j| >= diffeps.
 * However, might be that the cut coefficients are scaled versions, but the rhs values are different.
 * So we also compute ratio_coeff = coeff^1_j / coeff^2_j for the first j where both are non-zero.
 * If |coeff^1_j - ratio * coeff^2_j| >= diffeps for some j,
 * but |coeff^1_j - ratio_coeff * coeff^2_j| < diffeps for all j,
 * then the return code will be -1 or 1 (essentially, one cut will dominate the other).
 *
 * By the way, this is all essentially checking orthogonality...
 */
int isRowDifferent(const CoinPackedVector& cut1Vec, const double cut1rhs,
    const CoinPackedVector& cut2Vec, const double cut2rhs, const double eps) {
//  int ret_val = 2; // start out assuming the cuts are different
//  const double cut1norm = cut1Vec.twoNorm();
//  const double cut2norm = cut2Vec.twoNorm();
//  const double ortho = 1.
//      - (dotProduct(cut1Vec, cut2Vec) / (cut1norm * cut2norm));
//  if (isZero(ortho, eps)) {
//    ret_val--; // they are at least the same in coefficients
//    const double scaled_rhs1 = cut1rhs / cut1norm;
//    const double scaled_rhs2 = cut2rhs / cut2norm;
//    if (isVal(scaled_rhs1, scaled_rhs2, eps)) {
//      ret_val--; // they are also the same in rhs
//    } else if (scaled_rhs1 > scaled_rhs2) {
//      ret_val *= -1; // cut1 dominates
//    }
//  }
//  return ret_val;

  const int numElem1 = cut1Vec.getNumElements();
  const int* index1 = cut1Vec.getIndices();
  const double* value1 = cut1Vec.getElements();

  const int numElem2 = cut2Vec.getNumElements();
  const int* index2 = cut2Vec.getIndices();
  const double* value2 = cut2Vec.getElements();

  double ratio = -1.0;
  double ratio_coeff = -1.0;
  double maxDiff = 0.;
  double maxDiffCoeff = 0.;
  double maxDiffZero = 0.;

  // First, we try to get the ratio from RHS
  if (isZero(cut1rhs, eps) || isZero(cut2rhs, eps)) {
    // If one but not both are 0, then the cuts are clearly different
    // But they may only differ in the rhs
    if (!isZero(cut1rhs, eps)) {
      maxDiff = std::abs(cut1rhs);
    } else if (!isZero(cut2rhs, eps)) {
      maxDiff = std::abs(cut2rhs);
    }
    // Otherwise, they're both near zero,
    // and we're not going to be able to get
    // any kind of ratio from here
  } else {
    ratio = cut1rhs / cut2rhs;
  }

  // Now check if this ratio (if we found one) holds for
  // all the coefficients. In particular, we check that
  // std::abs(cut1.coeff - ratio * cut2.coeff) < eps
  int ind2 = 0;
  for (int ind1 = 0; ind1 < numElem1; ind1++) {
    // If this entry is zero, then we cannot have a violation
    // (unless the corresponding entry of cut2 is non-zero,
    // but this will be detected later)
    //if (isZero(value1[ind1], eps)) {
    //  continue;
    //}

    // Find matching indices
    while (ind2 < numElem2 && index1[ind1] > index2[ind2]) {
      if (!isZero(value2[ind2], eps)) {
        return 2;
      }
      if (value2[ind2] > maxDiffZero) {
        maxDiffZero = std::abs(value2[ind2]);
      }
      ind2++;
    }

    // If we reached end of the second cut,
    // or index1[ind1] does not exist in the second cut,
    // capture maxDiffZero, or exit early if something "really different" detected
    if (ind2 >= numElem2 || index2[ind2] > index1[ind1]) {
      if (!isZero(value1[ind1], eps)) {
        return 2;
      }
      if (value1[ind1] > maxDiffZero) {
        maxDiffZero = std::abs(value1[ind1]);
      }
      continue;
    }

    // Either one or both of the coefficients may be zero,
    // in which case both better be zero.
    if (isZero(value1[ind1], eps) || isZero(value2[ind2], eps)) {
      if (!isZero(value1[ind1], eps) || !isZero(value2[ind2], eps)) {
        return 2; // truly different
      }
      /*
       if (isZero(value1[ind1], eps) && isZero(value2[ind2], eps)) {
       // Both are zero, nothing to see here
       } else if (isZero(value2[ind2], eps)) {
       if (value1[ind1] > maxDiffZero) {
       maxDiffZero = value1[ind1];
       }
       } else {
       if (value2[ind2] > maxDiffZero) {
       maxDiffZero = value2[ind1];
       }
       }
       */
      ind2++;
      continue;
    }

    // Alternatively, we make sure inequality is not scaled version of other
    // Set scale factor if it has not been set before
    if (lessThanVal(ratio, 0.0)) {
      ratio = value1[ind1] / value2[ind2];
      if (lessThanVal(ratio, 0.0)) {
        // The scale is negative
        // Potentially one cut is the negative of the other,
        // but they are thus still distinct
        // (all cuts are ax >= b)
        return 2;
      }
    } else {
      const double diff = std::abs(value1[ind1] - ratio * value2[ind2]);
      if (diff > maxDiff) {
        maxDiff = diff;
      }
    }

    // Also check the ratio_coeff in case one cut dominates the other
    if (lessThanVal(ratio_coeff, 0.0)) {
      ratio_coeff = value1[ind1] / value2[ind2];
      if (lessThanVal(ratio_coeff, 0.0)) {
        // The scale is negative
        // Potentially one cut is the negative of the other,
        // but they are thus still distinct
        // (all cuts are ax >= b)
        return 2;
      }
    } else {
      const double diff = std::abs(value1[ind1] - ratio_coeff * value2[ind2]);
      if (diff > maxDiffCoeff) {
        maxDiffCoeff = diff;
      }
    }

    if (!isZero(maxDiff, eps) && !isZero(maxDiffCoeff, eps)) {
      return 2;
    }

    ind2++;
  } /* iterate over cut1 coefficients */

  // Check any remaining cut2 indices that were not matched
  while (ind2 < numElem2) {
    if (!isZero(value2[ind2], eps)) {
      return 2;
    }
    if (value2[ind2] > maxDiffZero) {
      maxDiffZero = std::abs(value2[ind2]);
    }
    ind2++;
  }

  // Are the cuts scaled versions of one another?
  if (isZero(maxDiff, eps)) {
    return 0; // they seem the same
  }

  // Are the cut coefficients scaled versions of one another?
  if (isZero(maxDiffCoeff, eps)) {
    if (lessThanVal(ratio_coeff, 0., eps)) {
      return 2; // cuts face opposite directions
    }

    // Either one or the other rhs will be better
    const double rhs_diff = cut1rhs - ratio_coeff * cut2rhs;
    if (isZero(rhs_diff, eps)) {
      return 0; // they seem the same, not sure how ratio missed it
    } else if (greaterThanVal(rhs_diff, 0., eps)) {
      return -1;
    } else {
      return 1;
    }
  }

  return 0;
} /* isRowDifferent */

/**
 * Two vectors are parallel iff u.v/|u|*|v| = 1
 */
double getParallelism(const CoinPackedVectorBase& vec1,
    const CoinPackedVectorBase& vec2) {
  const double scalarprod = dotProduct(vec1, vec2);
  return scalarprod / (vec1.twoNorm() * vec2.twoNorm());
} /* getParallelism (packed vectors) */

/**
 * Two vectors are parallel iff u.v/|u|*|v| = 1
 */
double getParallelism(const CoinPackedVectorBase& vec1, const int numElem,
    const double* vec2) {
  const double scalarprod = vec1.dotProduct(vec2);
  const double norm1 = vec1.twoNorm();
  const double norm2 = std::sqrt(std::inner_product(vec2, vec2 + numElem, vec2, 0.0L));
  return scalarprod / (norm1 * norm2);
} /* getParallelism (packed and not packed) */

/**
 * Two vectors are parallel iff u.v/|u|*|v| = 1
 */
double getParallelism(const int numElem, const double* vec1,
    const double* vec2) {
  const double scalarprod = dotProduct(vec1, vec2, numElem);
  const double norm1 = std::sqrt(std::inner_product(vec1, vec1 + numElem, vec1, 0.0L));
  const double norm2 = std::sqrt(std::inner_product(vec2, vec2 + numElem, vec2, 0.0L));
  return scalarprod / (norm1 * norm2);
} /* getParallelism (not packed) */

/**
 * Two vectors are parallel iff u.v/|u|*|v| = 1
 */
double getOrthogonality(const CoinPackedVectorBase& vec1,
    const CoinPackedVectorBase& vec2) {
  return 1. - getParallelism(vec1, vec2);
} /* getOrthogonality (packed vectors) */

/**
 * Two vectors are parallel iff u.v/|u|*|v| = 1
 */
double getOrthogonality(const CoinPackedVectorBase& vec1, const int numElem,
    const double* vec2) {
  return 1. - getParallelism(vec1, numElem, vec2);
} /* getOrthogonality (packed and not packed) */

/**
 * Two vectors are parallel iff u.v/|u|*|v| = 1
 */
double getOrthogonality(const int numElem, const double* vec1,
    const double* vec2) {
  return 1. - getParallelism(numElem, vec1, vec2);
} /* getOrthogonality (not packed) */

void getOrthogonalityStatsForRound(const OsiCuts& cs, double& minOrtho,
    double& maxOrtho, double& avgOrtho,
    const std::vector<int>* const roundWhenCutApplied, const int this_round) {
  const bool check_round = roundWhenCutApplied != NULL && this_round >= 0;
  int num_checked = 0;
  minOrtho = 2.;
  maxOrtho = -1.;
  avgOrtho = 0.;
  for (int pos = 0; pos < cs.sizeCuts(); pos++) {
    if (check_round && (*roundWhenCutApplied)[pos] != this_round) {
      continue;
    }
    for (int pos2 = pos + 1; pos2 < cs.sizeCuts(); pos2++) {
      if (check_round && (*roundWhenCutApplied)[pos2] != this_round) {
        continue;
      }
      const double this_ortho = getOrthogonality(cs.rowCutPtr(pos)->row(),
          cs.rowCutPtr(pos2)->row());
      if (this_ortho < minOrtho) {
        minOrtho = this_ortho;
      }
      if (this_ortho > maxOrtho) {
        maxOrtho = this_ortho;
      }
      avgOrtho += this_ortho;
      num_checked++;
    }
  }
  if (num_checked == 0) { // only one cut in this round
    minOrtho = -1.;
    maxOrtho = -1.;
    avgOrtho = -1.;
  } else {
    avgOrtho /= num_checked;
  }
} /* getOrthogonalityStatsForRound */

double getMinOrthogonality(const CoinPackedVector& vec, const OsiCuts& cs) {
  double min_ortho = 2.;
  for (int pos = 0; pos < cs.sizeCuts(); pos++) {
    const double this_ortho = getOrthogonality(cs.rowCutPtr(pos)->row(), vec);
    if (this_ortho < min_ortho) {
      min_ortho = this_ortho;
    }
  }
  return min_ortho;
} /* getMinOrthogonality */

/**
 * @brief Set full std::vector coordinates from packed vector
 *
 * No effort is made to reset the indices that are not present in packed vector
 * so these should be initialized as desired prior to calling this function
 *
 * No check is performed for indices existing,
 * so this should be ensured before calling this function
 */
void setFullVecFromPackedVec(std::vector<double>& fullVec,
    const CoinPackedVector& vec) {
  const int numElem = vec.getNumElements();
  const int* vertIndex = vec.getIndices();
  const double* vertElem = vec.getElements();
  for (int el = 0; el < numElem; el++) {
    const int curr_ind = vertIndex[el];
    fullVec[curr_ind] = vertElem[el];
  }
} /* setFullVecFromPackedVec */

/**
 * @brief Any indices that appear in vec will be put to zero in fullVec
 *
 * No effort is made to reset the indices that are not present in packed std::vector
 * so these should be initialized as desired prior to calling this function
 *
 * No check is performed for indices existing,
 * so this should be ensured before calling this function
 */
void clearFullVecFromPackedVec(std::vector<double>& fullVec,
    const CoinPackedVector& vec) {
  const int numElem = vec.getNumElements();
  const int* vertIndex = vec.getIndices();
  for (int el = 0; el < numElem; el++) {
    const int curr_ind = vertIndex[el];
    fullVec[curr_ind] = +0.0;
  }
} /* clearFullVecFromPackedVec */

/** From CglLandP: return the coefficients of the intersection cut */
double unstrengthenedIntersectionCutCoeff(double abar, double f0) {
  if (abar > 0) {
    //return alpha_i * (1 - beta);
    return (abar / f0);
  } else {
    //return -alpha_i * beta;
    return (-abar / (1 - f0));
  }
} /* unstrengthenedIntersectionCutCoeff */

/** From CglLandP compute the modularized row coefficient for an integer variable */
double modularizedCoeff(double abar, double f0) {
  double f_i = abar - floor(abar);
  if (f_i <= f0) {
    return f_i;
  } else {
    return f_i - 1;
  }
} /* modularizedCoeff */

/** Adapted from CglLandP: return the coefficients of the strengthened intersection cut */
double strengthenedIntersectionCutCoeff(double abar, double f0,
    const OsiSolverInterface* const solver, const int currFracVar) {
  if ((currFracVar >= 0)
      && ((currFracVar >= solver->getNumCols())
          || !(solver->isInteger(currFracVar)))) {
    return unstrengthenedIntersectionCutCoeff(abar, f0);
  } else {
//    return modularizedCoeff(abar, f0);
    double f_i = abar - std::floor(abar);
    if (f_i < f0) {
//      return f_i * (1 - f0);
      return f_i / f0;
    } else {
//      return (1 - f_i) * f0;
      return (1 - f_i) / (1 - f0);
    }
  }
} /* strengthenedIntersectionCutCoeff */

double intersectionCutCoeff(double abar, double f0,
    const OsiSolverInterface* const solver, const int currFracVar,
    const bool strengthen) {
  if (!strengthen || (currFracVar >= solver->getNumCols())
      || !(solver->isInteger(currFracVar))) {
    return unstrengthenedIntersectionCutCoeff(abar, f0);
  } else {
    return strengthenedIntersectionCutCoeff(abar, f0);
  }
} /* intersectionCutCoeff */

void setClpParameters(OsiClpSolverInterface* const solver,
    const double max_time) {
#ifndef TRACE
  solver->messageHandler()->setLogLevel(0);
  solver->getModelPtr()->messageHandler()->setLogLevel(0);
#endif
  solver->getModelPtr()->setMaximumSeconds(max_time);

  // Try turning on scaling with enableFactorization
//  solver->setSpecialOptions(solver->specialOptions() | 512);
} /* setClpParameters (OsiClp) */

void setMessageHandler(CbcModel* const cbc_model) {
#ifdef TRACE
  cbc_model->setLogLevel(3);
  cbc_model->messagesPointer()->setDetailMessages(10, 10000, (int *) NULL);
#else
  cbc_model->setLogLevel(0);
  cbc_model->messagesPointer()->setDetailMessages(10,5,5000);
#endif
  if (cbc_model->solver()) {
    cbc_model->solver()->setHintParam(OsiDoReducePrint, true, OsiHintTry);
  }
  cbc_model->setPrintFrequency(1);
} /* setMessageHandler (Cbc) */

/**
 * We need to be careful with the strong branching options;
 * sometimes the Clp strong branching fails, such as with arki001, branching down on variable 924
 */
void setupClpForStrongBranching(OsiClpSolverInterface* const solver,
    const int hot_start_iter_limit) {
  solver->setIntParam(OsiMaxNumIterationHotStart, hot_start_iter_limit);
  solver->setSpecialOptions(16); // use standard strong branching rather than clp's
} /* setupClpForStrongBranching */

/**
 * Sets message handler and special options when using solver as part of B&B
 * (in which we want to run full strong branching and enable the fixing of variables)
 */
void setupClpForCbc(OsiClpSolverInterface* const solver, 
    const int hot_start_iter_limit) {
  setClpParameters(solver);
  solver->setHintParam(OsiDoPresolveInInitial, false);
  solver->setHintParam(OsiDoPresolveInResolve, false);
  solver->setIntParam(OsiMaxNumIterationHotStart, hot_start_iter_limit);
  solver->setSpecialOptions(16); // use standard strong branching rather than clp's
  // Do not switch from dual to primal, or something to this effect; 
  // This allows infeasible branches to be fixed during strong branching
  solver->getModelPtr()->setMoreSpecialOptions(solver->getModelPtr()->moreSpecialOptions()+256); 
} /* setupClpForCbc */

/**
 * Set parameters for Cbc used for VPCs, as well as the custom branching decision
 */
void setCbcParametersForPartialBB(CbcModel* const cbc_model,
    CbcEventHandler* eventHandler,
    const int numStrong,
    const int numBeforeTrusted,
    const double max_time) {
  setMessageHandler(cbc_model);
  cbc_model->solver()->setIntParam(OsiMaxNumIterationHotStart, 100);

  // What is the partial strategy?
  const int strategy = std::abs(param.getParamVal(ParamIndices::PARTIAL_BB_STRATEGY_PARAM_IND));
  const int sign = (param.getParamVal(ParamIndices::PARTIAL_BB_STRATEGY_PARAM_IND) < 0) ? -1 : 1;
  const int compare_strategy = strategy % 10; // ones digit
  const int branch_strategy = (strategy % 100 - compare_strategy) / 10; // tens digit
  const int choose_strategy = sign * (strategy % 1000 - compare_strategy - 10 * branch_strategy) / 100; // hundreds digit

  // Branching decision (tens digit)
  // Given a branching variable, which direction to choose?
  // (The choice of branching variable is through OsiChooseVariable)
  // 0: default, 1: dynamic, 2: strong, 3: none
  CbcBranchDecision* branch;
  if (branch_strategy == 1) {
    branch = new CbcBranchDynamicDecision();
  } else if (branch_strategy == 2) {
    branch = new CbcBranchStrongDecision();
  } else if (branch_strategy == 3) {
    branch = NULL;
  } else {
    branch = new CbcBranchDefaultDecision();
  }

  // Set comparison for nodes (ones digit)
  // Given a tree, which node to pick next?
  // 0: default: 1: bfs, 2: depth, 3: estimate, 4: objective, 5: objective_reverse
  CbcCompareBase* compare;
  if (compare_strategy == 1) {
    compare = new CbcCompareBFS();
  } else if (compare_strategy == 2) {
    compare = new CbcCompareDepth();
  } else if (compare_strategy == 3) {
    compare = new CbcCompareEstimate();
  } else if (compare_strategy == 4) {
    compare = new CbcCompareObjective();
  } else {
    compare = new CbcCompareDefault();
  }

  cbc_model->setTypePresolve(0);
  cbc_model->setMaximumSeconds(max_time);
  cbc_model->setMaximumCutPassesAtRoot(0);
  cbc_model->setMaximumCutPasses(0);
  cbc_model->setWhenCuts(0);
  if (numStrong >= 0) {
    // Maximum number of strong branching candidates to consider each time
    cbc_model->setNumberStrong(numStrong);
  }
  if (numBeforeTrusted >= 0) {
    // # before switching to pseudocosts, I think; 0 disables dynamic strong branching, doesn't work well
    cbc_model->setNumberBeforeTrust(numBeforeTrusted);
  }

  if (branch) {
    OsiChooseStrongCustom choose;
    if (numStrong >= 0) {
      choose.setNumberStrong(numStrong);
    }
    if (numBeforeTrusted >= 0) {
      choose.setNumberBeforeTrusted(numBeforeTrusted);
    }
    choose.setMethod(choose_strategy);
    branch->setChooseMethod(choose);
    // From CbcModel::convertToDynamic, we see that the branching decision may be ignored without a choose method
    if ((branch->whichMethod()&1) == 0 && !branch->chooseMethod()) {
      OsiChooseStrong choose;
      choose.setNumberStrong(5);
      choose.setNumberBeforeTrusted(0);
      branch->setChooseMethod(choose);
    }
    cbc_model->setBranchingMethod(*branch);
  } /* check that branch is not NULL */

  if (eventHandler) {
    cbc_model->passInEventHandler(eventHandler);
  }

  cbc_model->setNodeComparison(compare);

  if (branch) {
    delete branch;
  }
  if (compare) {
    delete compare;
  }
} /* setCbcParameters */

int pivot(OsiClpSolverInterface*& pivotSolver, const int varIn,
    const int dirnIn, int& varOut, int& dirnOut, int& pivotRow, double& dist) {
  //pivotSolver->enableSimplexInterface(true);
  ClpSimplex* model = pivotSolver->getModelPtr();

  // Make sure this is not a fixed variable
  if (model->getStatus(varIn) == ClpSimplex::Status::isFixed) {
    varOut = -1;
    dirnOut = 0;
    pivotRow = -1;
    dist = -1;
    return -1;
  }

  const int return_code = pivotSolver->primalPivotResult(varIn, dirnIn, varOut,
      dirnOut, dist,
      NULL);
  if (return_code >= 0) {
    varOut = model->sequenceOut(); // Because Osi uses a negative index for slacks
    pivotRow = model->pivotRow();
//          if (pivotRow >=0) {
//            if (varBasicInRow[pivotRow[ray_ind]] != varOut[ray_ind]) {
//              error_msg(errorstring,
//                  "While processing ray %d (var %d), variable basic in row %d is supposed to be %d but copySolver says it is %d.\n",
//                  ray_ind, varIn, pivotRow[ray_ind], varBasicInRow[ray_ind],
//                  varOut[ray_ind]);
//              writeErrorToII(errorstring, GlobalVariables::inst_info_out);
//              exit(1);
//            }
//          } else {
//            if (pivotRow[ray_ind] != -2 && varIn != varOut[ray_ind]) {
//              error_msg(errorstring,
//                  "While processing ray %d (var %d), pivot says nb variable is closest, so pivotRow should be -2 (is %d) and varIn (%d) == varIn (%d).\n",
//                  ray_ind, varIn, pivotRow[ray_ind], varIn, varOut[ray_ind]);
//              writeErrorToII(errorstring, GlobalVariables::inst_info_out);
//              exit(1);
//            }
//          }
    dist *= dirnIn; // Flip when dirnIn is negative, because then model->theta() will be negative

    // Flip the slacks
    if (varOut >= pivotSolver->getNumCols()) {
      dirnOut = (dirnOut == ClpSimplex::Status::atLowerBound) ? 1 : -1;
    }
  } else {
    varOut = -1;
    dirnOut = 0;
    pivotRow = -1;
    dist = -1;
  }

  //pivotSolver->disableSimplexInterface();
  return return_code;
} /* pivot */

void copySolverBasis(PointCutsSolverInterface* const solverCopy,
    const PointCutsSolverInterface* const solverOld) {
  solverCopy->enableFactorization();
  solverCopy->setBasis(*(solverOld->getConstPointerToWarmStart()));
  // This is to prevent a different basis from being chosen for solverCopy, which sometimes happens.
//  int* cstat = new int[solverOld->getNumCols()];
//  int* rstat = new int[solverOld->getNumRows()];
//  solverOld->getBasisStatus(cstat, rstat);
//  solverCopy->setBasisStatus(cstat, rstat);
//  delete[] cstat;
//  delete[] rstat;
  solverCopy->disableFactorization();
  //  solverCopy->enableSimplexInterface(true); // This caused problems with non-negative variables getting negative values in the solution.
  if (!solverCopy->basisIsAvailable())
    solverCopy->resolve();
//  solverCopy->enableFactorization(); // Seemingly is broken by the resolve or setBasisStatus
} /* copySolverBasis */

/**
 * @brief returns lb of col (recall complementing of slacks)
 */
double getVarLB(const OsiSolverInterface* const solver, const int col) {
  if (col < solver->getNumCols()) {
    return solver->getColLower()[col];
  } else {
    return 0.0;
//    const int row = col - solver->getNumCols();
//    if (in_subspace_or_compl_space || (solver->getRowSense()[row] == 'L')) {
//      return 0.0;
//    } else {
//      return -1 * solver->getInfinity();
//    }
  }
} /* getVarLB */

/**
 * @brief returns ub of col (recall complementing of slacks)
 */
double getVarUB(const OsiSolverInterface* const solver, const int col) {
  if (col < solver->getNumCols()) {
    return solver->getColUpper()[col];
  } else {
    return solver->getInfinity();
//    const int row = col - solver->getNumCols();
//    if (in_subspace_or_compl_space || (solver->getRowSense()[row] == 'L')) {
//      return solver->getInfinity();
//    } else {
//      return 0.0;
//    }
  }
} /* getVarUB */

/**
 * @brief returns value of col (recall complementing of slacks)
 */
double getVarVal(const PointCutsSolverInterface* const solver, const int col) {
  if (col < solver->getNumCols()) {
    return solver->getColSolution()[col];
  } else {
    const int row = col - solver->getNumCols();
    // Absolute value since we assume all slacks are non-negative
    return std::abs(solver->getRowActivity()[row] - solver->getRightHandSide()[row]);
//    return (solver->getRightHandSide()[row] - solver->getRowActivity()[row]);
  }
} /* getVarVal */


/**
 * @brief returns value of col (recall complementing of slacks)
 */
double getCNBVarVal(const PointCutsSolverInterface* const solver, const int col) {
  return getCNBVarVal(solver, col, isNonBasicUBVar(solver, col));
}

/**
 * @brief returns value of col (recall complementing of slacks)
 */
double getCNBVarVal(const PointCutsSolverInterface* const solver, const int col, const bool complement) {
  double value;
  if (col < solver->getNumCols()) {
    value = solver->getColSolution()[col];
    const double LB = getVarLB(solver, col);
    const double UB = getVarUB(solver, col);

    if (complement) {
      value = UB - value;
    } else {
      value = value - LB;
    }
  } else {
    const int row = col - solver->getNumCols();
    // Absolute value since we assume all slacks are non-negative
    value = (solver->getRightHandSide()[row] - solver->getRowActivity()[row]);
    if (complement) {
      value = std::abs(value);
    }
  }
  return value;
} /* getCNBVarVar */

double getNBObjValue(const double* colSolution, const double* slackSolution,
    const OsiSolverInterface* const origSolver,
    const SolutionInfo& origProbData, const int deletedVar) {
  // If a variable was deleted to make tmpSolver, then we will need to adjust the index
  // It is -1 because we need the index in the *new* space
  const int deletedVarAdjustment = (deletedVar >= 0) ? -1 : 0;

  double retval = 0.;
  for (int i = 0; i < origProbData.numNB; i++) {
    const int currVar = origProbData.nonBasicVarIndex[i];
    const int currVarTmpIndex =
        (currVar < deletedVar) ? currVar : currVar + deletedVarAdjustment;
    double tmpVal, origVal;
    if (currVar < origProbData.numCols) {
      tmpVal = colSolution[currVarTmpIndex];
      origVal = origSolver->getColSolution()[currVar];
    } else {
      const int currRow = currVar - origProbData.numCols;
      tmpVal = slackSolution[currRow];
      origVal = 0.0;
    }
    // ***
    // For *struct* var:
    // If the variable is lower-bounded, then we need to shift it up for the comp space
    // That is, xComp = x - lb
    // If the variable is upper-bounded, we need to get the value in the complemented space
    // That is, xComp = ub - x
    // What to do if the variable is fixed NB or free NB?
    // In the former case, thinking of it as a lower-bounded variable, it will simply be zero
    // in any solution, including in the original one at v, in which case we don't need to
    // worry about it for the packed ray.
    // TODO The latter case we could have handled by preprocessing the free variables out
    // ****
    // For *slack* var:
    // In the original NB space at v, all slacks were at zero.
    // We simply look at b[row] - activity[row]
    // For >= rows, slack is non-positive
    // For <= rows, slack is non-negative
    // We take absolute value to deal with this
    const double newVal = std::abs(tmpVal - origVal);
    retval += newVal * origProbData.nonBasicReducedCost[i];
  }
  return retval;
} /* getNBObjValue */

double getNBObjValue(const OsiSolverInterface* const tmpSolver,
    const OsiSolverInterface* const origSolver,
    const SolutionInfo& origProbData, const int deletedVar) {
  // If a variable was deleted to make tmpSolver, then we will need to adjust the index
  // It is -1 because we need the index in the *new* space
  const int deletedVarAdjustment = (deletedVar >= 0) ? -1 : 0;

  double retval = 0.;
  for (int i = 0; i < origProbData.numNB; i++) {
    const int currVar = origProbData.nonBasicVarIndex[i];
    const int currVarTmpIndex =
        (currVar < deletedVar) ? currVar : currVar + deletedVarAdjustment;
    double tmpVal, origVal;
    if (currVar < origProbData.numCols) {
      tmpVal = tmpSolver->getColSolution()[currVarTmpIndex];
      origVal = origSolver->getColSolution()[currVar];
    } else {
      const int currRow = currVar - origProbData.numCols;
      tmpVal = tmpSolver->getRightHandSide()[currRow]
          - tmpSolver->getRowActivity()[currRow];
      origVal = 0.0;
    }
    // ***
    // For *struct* var:
    // If the variable is lower-bounded, then we need to shift it up for the comp space
    // That is, xComp = x - lb
    // If the variable is upper-bounded, we need to get the value in the complemented space
    // That is, xComp = ub - x
    // What to do if the variable is fixed NB or free NB?
    // In the former case, thinking of it as a lower-bounded variable, it will simply be zero
    // in any solution, including in the original one at v, in which case we don't need to
    // worry about it for the packed ray.
    // TODO The latter case we could have handled by preprocessing the free variables out
    // ****
    // For *slack* var:
    // In the original NB space at v, all slacks were at zero.
    // We simply look at b[row] - activity[row]
    // For >= rows, slack is non-positive
    // For <= rows, slack is non-negative
    // We take absolute value to deal with this
    const double newVal = std::abs(tmpVal - origVal);
    retval += newVal * origProbData.nonBasicReducedCost[i];
  }
  return retval;
} /* getNBObjValue */

/**
 * The variables that lead to rays are non-basic, non-fixed structural variables
 * and non-basic slacks not coming from equality constraints.
 */
bool isRayVar(const OsiClpSolverInterface* const solver, const int var) {
  if (var < solver->getNumCols())
    return (!isBasicCol(solver, var)); 
  else
    return (!isBasicSlack(solver, var - solver->getNumCols()));
}

/**
 * Returns the index in raysCutByHplane of the first ray that is cut by this hyperplane in this activation
 */
int getStartIndex(const Hplane& hplane, //const std::vector<Hplane>& hplaneStore, const int hplane_ind,
    const int split_ind, const int act_ind) {
  return (act_ind > 0) ?
      hplane.numRaysCutByHplaneInclPrevAct[split_ind][act_ind - 1] : 0;
}

/**
 * Returns the index in raysCutByHplane of the last ray that is cut by this hyperplane in this activation
 */
int getEndIndex(const Hplane& hplane, //const std::vector<Hplane>& hplaneStore, const int hplane_ind,
    const int split_ind, const int act_ind) {
  const int numRaysCutByHplane =
      hplane.numRaysCutByHplaneInclPrevAct[split_ind][act_ind];
  return numRaysCutByHplane;
//  const int numRaysPrevCut =
//      (act_ind > 0) ?
//          hplane.numRaysCutByHplaneInclPrevAct[split_ind][act_ind - 1] :
//          0;
//  return start_ind + numRaysCutByHplane - numRaysPrevCut;
}

void storeNewRayNBIndices(int numNonBasicCols,
    std::vector<int> &newRayColIndices,
    const std::vector<int> &allRaysCutByHplane,
    std::vector<bool> &raysCutFlag) {
  //Clear array and fill all raysCutFlag elements to false
  newRayColIndices.clear();
  std::fill(raysCutFlag.begin(), raysCutFlag.end(), 0);

  for (unsigned j = 0; j < allRaysCutByHplane.size(); j++)
    raysCutFlag[allRaysCutByHplane[j]] = true;

  for (int j = 0; j < numNonBasicCols; j++)
    if (raysCutFlag[j] == false)
      newRayColIndices.push_back(j);
}

void delElmtsInIntVector(std::vector<int>& intVector,
    const std::vector<int>& delIndices) {
  int size = delIndices.size();
  for (int p = size - 1; p > -1; p--) {
    intVector.erase(intVector.begin() + delIndices[p]);
  }
}

void delElmtsInDoubleVector(std::vector<double>& dblVector,
    const std::vector<int>& delIndices) {
  int size = delIndices.size();
  for (int p = size - 1; p > -1; p--) {
    dblVector.erase(dblVector.begin() + delIndices[p]);
  }
}

//void delElmtsInptOrRayInfoVector(std::vector<ptOrRayInfo*> &intPtOrRayInfo,
//    const std::vector<int> &delIndices) {
//  int size = delIndices.size();
//  for (int p = size - 1; p > -1; p--) {
//    delete intPtOrRayInfo[delIndices[p]];
//    intPtOrRayInfo.erase(intPtOrRayInfo.begin() + delIndices[p]);
//  }
//}

void delElmtsInPartitionVector(std::vector<int>& partitionVector,
    const std::vector<int>& delIndices) {
  const int size = delIndices.size();
  for (int p = size - 1; p > -1; p--) {
    partitionVector.erase(partitionVector.begin() + delIndices[p]);
  }
}

bool duplicatePoint(const CoinPackedMatrix& interPtsAndRays,
    const std::vector<double>& rhs, const Point& newPoint) {
  const int numPtsAndRays = interPtsAndRays.getNumRows();
  for (int r = 0; r < numPtsAndRays; r++) {
    if (isZero(rhs[r]))
      continue;

    const CoinShallowPackedVector vec = interPtsAndRays.getVector(r);
    if (newPoint == vec) // This invokes the operator from Point.hpp
      return true;
  }
  return false;
}

bool duplicateRay(const CoinPackedMatrix& interPtsAndRays,
    const std::vector<double>& rhs, const Ray& newRay) {
  const int numPtsAndRays = interPtsAndRays.getNumRows();
  for (int r = 0; r < numPtsAndRays; r++) {
    if (!isZero(rhs[r]))
      continue;

    const CoinShallowPackedVector vec = interPtsAndRays.getVector(r);
    if (newRay == vec) // This invokes the operator from Ray.hpp
      return true;
  }
  return false;
}

int findDuplicateRay(const CoinPackedMatrix& interPtsAndRays,
    const std::vector<double>& rhs, const Ray& newRay) {
  const int numPtsAndRays = interPtsAndRays.getNumRows();
  for (int r = 0; r < numPtsAndRays; r++) {
    if (!isZero(rhs[r]))
      continue;

    const CoinShallowPackedVector vec = interPtsAndRays.getVector(r);
    if (newRay == vec) // This invokes the operator from Ray.hpp
      return r;
  }
  return -1;
}

/**
 * Determine if ray was previously cut by this hplane
 */
bool rayCutByHplaneForSplit(const int oldHplaneIndex,
    const std::vector<Hplane>& hplaneStore, const int ray_ind,
    const std::vector<Ray>& rayStore, const int split_ind) {
  if (oldHplaneIndex < 0) {
    return false;
  }

  const int numRaysCutForSplit =
      hplaneStore[oldHplaneIndex].rayToBeCutByHplane[split_ind].size();
  for (int r = 0; r < numRaysCutForSplit; r++) {
    if (rayStore[ray_ind]
        == rayStore[hplaneStore[oldHplaneIndex].rayToBeCutByHplane[split_ind][r]]) {
      return true;
    }
  }
  return false;
}

/*
 * Method checks if a hyperplane has been previous activated.
 * Only checks indices specified
 * @return the index if yes and -1 otherwise
 */
int hasHplaneBeenActivated(const std::vector<Hplane>& hplanesPrevAct,
    const std::vector<int> actHplaneIndex, const Hplane& tmpHplane,
    const bool returnIndexWIActivatedVec) {
  for (int i = 0; i < (int) actHplaneIndex.size(); i++) {
    const int hplane_ind = actHplaneIndex[i];
    const int prev_hplane_var = hplanesPrevAct[hplane_ind].var;
    const int prev_hplane_ubflag = hplanesPrevAct[hplane_ind].ubflag;
    if ((prev_hplane_var == tmpHplane.var)
        && (prev_hplane_ubflag == tmpHplane.ubflag)) {
      return returnIndexWIActivatedVec ? i : hplane_ind;
    }
  }
  return -1;
}

/*
 * Method checks if a hyperplane has been previous activated.
 * @return the index if yes and -1 otherwise
 */
int hasHplaneBeenActivated(const std::vector<Hplane>& hplanesPrevAct,
    const Hplane& tmpHplane, const int start_ind) {
  for (int hplane_ind = start_ind; hplane_ind < (int) hplanesPrevAct.size();
      hplane_ind++) {
    if (hplanesPrevAct[hplane_ind] == tmpHplane) {
      return hplane_ind;
    }
  }
  return -1;
}

/**
 * Method checks if a vertex has been previously created
 * @return the index if yes and -1 otherwise
 */
int hasVertexBeenActivated(const std::vector<Vertex>& verticesPrevAct,
    const Vertex& tmpVertex) {
  for (int vert_ind = 0; vert_ind < (int) verticesPrevAct.size();
      vert_ind++) {
    if (tmpVertex == verticesPrevAct[vert_ind])
      return vert_ind;
  }
  return -1;
}

/**
 *
 */
bool calcIntersectionPointWithSplit(Point& intPt, double& dist,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    const Vertex& vertex, const Ray& ray, const int split_var,
    const bool recalc_dist) {
  if (recalc_dist) {
    dist = distanceToSplit(vertex, ray, split_var, solver, solnInfo);
  }
  if (isInfinity(dist, solver->getInfinity())) {
#ifdef TRACE
//    printf("Ray does not intersect boundary of the split.\n");
#endif
    return false;
  }

  calcNewVectorCoordinates(intPt, dist, vertex, ray);
  return true;
}

/**
 *
 */
bool calcIntersectionPointWithSplit(Point& intPt,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    const Vertex& vertex, const Ray& ray, const int split_var) {
  const double dist = distanceToSplit(vertex, ray, split_var, solver,
      solnInfo);
  if (isInfinity(dist, solver->getInfinity())) {
#ifdef TRACE
//    printf("Ray does not intersect boundary of the split.\n");
#endif
    return false;
  }

  calcNewVectorCoordinates(intPt, dist, vertex, ray);
  return true;
}

void calcNewRayCoordinatesRank1(CoinPackedVector& rayIn, //const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo, const int rayOutIndex,
    const int newRayIndex, //const std::vector<Ray>& rayStore,
    const Hplane& hplane, const double RAYEPS) {
//    const int hplane_ind, const std::vector<Hplane>& hplaneStore) {
  std::vector<int> rayInIndex;
  std::vector<double> rayInVal;

  // First component
  rayInIndex.push_back(newRayIndex);
  rayInVal.push_back(1.0);

  // Second component (ignored if activating nb bound)
  // Also ignored if rayOutIndex is -1, meaning no ray was cut, so hplane is some dummy hplane
  if (rayOutIndex != -1 && hplane.row >= 0) {
    const double rayDirn = -1.0 * solnInfo.raysOfC1[newRayIndex][hplane.row]
        / solnInfo.raysOfC1[rayOutIndex][hplane.row];
    if (!isZero(rayDirn, RAYEPS)) {
      rayInIndex.push_back(rayOutIndex);
      rayInVal.push_back(rayDirn);
    }
  }

  // Add and sort
  rayIn.setVector(rayInIndex.size(), rayInIndex.data(), rayInVal.data());
  rayIn.sortIncrIndex();
}

/**
 * Starting at origin, we proceed along ray r^j until we intersect H_h.
 * This leads to vertex v^{jh}.
 * Thus, we want to perform the pivot in which j enters the basis and h leaves it.
 * @param rayOutIndex :: the ray index r^j we want to pivot out (one component, in jth index, of 1.0 value)
 *
 * @return Ray \ell from pivot in which j leaves basis and h enters.
 */
/*void calcNewRayCoordinatesRank1(CoinPackedVector& rayIn, const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo, const int rayOutIndex,
    const int newRayIndex, const std::vector<Ray>& rayStore,
    const std::vector<Hplane>& hplaneStore) {
  error_msg(errstring,
      "This method is unfinished because firstHplaneOverall is not updated correctly.\n");
//  const int rayOutHplane = rayStore[rayOutIndex].firstHplaneOverall;
//  calcNewRayCoordinatesRank1(rayIn, solver, solnInfo, rayOutIndex,
//      newRayIndex, rayStore, hplaneStore[rayOutHplane]);
}*/

/**
 * Starting at origin, we proceed along ray r^j until we intersect H_h.
 * This leads to vertex v^{jh}.
 * Similarly, starting at origin and proceeding along r^k, the first hplane
 * intersected was H_g, leading to vertex v^{kg}.
 *
 * Let r^{jh} be a ray from v^{jh}.
 * Coordinates of r^{jh} in nb space are simple:
 * 1. non-zero corresponding to r^j
 * 2. non-zero corresponding to which of the other n-1 hplanes we depart
 * Note that this leads to a total of n-1 rays, as desired.
 *
 * Now r^{jh} intersects H_g.
 * Specifically, it intersects the corresponding ray emanating from v^{kg}.
 * Call this ray r^{kg}.
 * Then we get the new vertex v^{jhkg}.
 * And now we calculate one of the new rays, which will have three non-basic components.
 */
/*void calcNewRayCoordinatesRank2(Ray& rayIn, const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo, const int rayOutIndex1,
    const int rayOutIndex2, const int newRayIndex,
    const std::vector<Ray>& rayStore,
    const std::vector<Hplane>& hplaneStore) {
  error_msg(errstring,
      "This method is unfinished because firstHplaneOverall is not updated correctly.\n");
   std::vector<int> rayInIndex;
   std::vector<double> rayInVal;

   // First component
   rayInIndex.push_back(newRayIndex);
   rayInVal.push_back(1.0);

   // Look at rayOutIndex2
   // What is the first hyperplane that intersects that ray?
   const int rayOut1Hplane = rayStore[rayOutIndex1].firstHplaneOverall;
   const int rayOut2Hplane = rayStore[rayOutIndex2].firstHplaneOverall;
   const int hplaneRow1 = hplaneStore[rayOut1Hplane].row;
   const int hplaneRow2 = hplaneStore[rayOut2Hplane].row;

   if (hplaneRow1 == -1 && hplaneRow2 == -1) {
   rayIn.setVector(rayInIndex.size(), rayInIndex.data(), rayInVal.data());
   return;
   }

   std::vector<int> rows, cols;
   if (hplaneRow1 != -1) {
   rows.push_back(hplaneRow1);
   cols.push_back(rayOutIndex1);
   }
   if (hplaneRow2 != -1) {
   rows.push_back(hplaneRow2);
   cols.push_back(rayOutIndex2);
   }
   const double denom = determinant(solnInfo.raysOfC1, rows, cols);

   // Calculate second component
   if (hplaneRow1 != -1) {
   cols.clear();
   cols.push_back(newRayIndex);
   if (hplaneRow2 != -1)
   cols.push_back(rayOutIndex2);
   const double num1 = determinant(solnInfo.raysOfC1, rows, cols);

   // Second component, corresponding to rayOutIndex1
   rayInIndex.push_back(rayOutIndex1);
   rayInVal.push_back(-1.0 * num1 / denom);
   }

   // Calculate third component
   if (hplaneRow2 != -1) {
   cols.clear();
   cols.push_back(newRayIndex);
   if (hplaneRow1 != -1)
   cols.push_back(rayOutIndex1);
   const double num2 = determinant(solnInfo.raysOfC1, rows, cols);

   // Third component, corresponding to rayOutIndex2
   rayInIndex.push_back(rayOutIndex2);
   rayInVal.push_back(-1.0 * num2 / denom);
   }

   // Add and sort
   rayIn.setVector(rayInIndex.size(), rayInIndex.data(), rayInVal.data());
   rayIn.sortIncrIndex();
}*/

/**
 * Returns determinant of the 1x1 or 2x2 matrix specified
 */
double determinant(const std::vector<std::vector<double> >& mx,
    const std::vector<int>& rows, const std::vector<int>& cols) {
  const int numRows = rows.size();
  const int numCols = cols.size();
  if (numRows != numCols) {
    error_msg(errorstring,
        "Number of rows and columns for computing determinant needs to be equal. Num rows: %d. Num cols: %d.\n",
        numRows, numCols);
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  if (numRows == 1) {
    const int row = rows[0];
    const int col = cols[0];
    return mx[row][col];
  } else if (numRows == 2) {
    const int row0 = rows[0];
    const int row1 = rows[1];
    const int col0 = cols[0];
    const int col1 = cols[1];
    return mx[row0][col0] * mx[row1][col1] - mx[row0][col1] * mx[row1][col0];
  } else {
    error_msg(errorstring,
        "Currently can only handle 2x2 matrix. Num rows: %d. Num cols: %d.\n",
        numRows, numCols);
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }
}

/**
 * @brief Reimplemented from AdvCuts class because of precision issues with taking inverses
 *
 * @param split_var :: split
 * @param vec :: must be sorted in increasing order (in terms of indices) and be in NB space
 */
double getSICActivity(const int split_var,
    const std::vector<Vertex>& vertexStore,
    const std::vector<Ray>& rayStore, const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo, const CoinPackedVector& vec) {
  const int numVecElem = vec.getNumElements();
  if (numVecElem == 0)
    return +0.0;

  const int* vecIndex = vec.getIndices();
  const double* vecElem = vec.getElements();

  double activity = +0.0;

  for (int vec_el = 0; vec_el < numVecElem; vec_el++) {
    const int vec_ind = vecIndex[vec_el];
    if (!isZero(vecElem[vec_el])) {
      activity += vecElem[vec_el]
          / distanceToSplit(vertexStore[0], rayStore[vec_ind],
              split_var, solver, solnInfo);
    }
  }

  return activity;
}

/**
 * Finds coordinates of new vertex or point obtained by
 * proceeding along rayOut from vertex a distance of dist
 * Depends crucially on both being sorted in increasing order
 */
void calcNewVectorCoordinates(CoinPackedVector& newVector, const double dist,
    const CoinPackedVector& vertex, const CoinPackedVector& rayOut) {
  const int numVertexEls = vertex.getNumElements();
  const int* vertexIndex = vertex.getIndices();
  const double* vertexVal = vertex.getElements();

  const int numRayEls = rayOut.getNumElements();
  const int* rayIndex = rayOut.getIndices();
  const double* rayVal = rayOut.getElements();

  std::vector<int> newVectorIndex;
  std::vector<double> newVectorVal;
  newVectorIndex.reserve(numVertexEls + numRayEls);
  newVectorVal.reserve(numVertexEls + numRayEls);

  int vert_el = 0;
  for (int ray_el = 0; ray_el < numRayEls; ray_el++) {
    const int ray_ind = rayIndex[ray_el];
    const double ray_val = rayVal[ray_el];

    while ((vert_el < numVertexEls) && (vertexIndex[vert_el] < ray_ind)) {
      // The new vertex has not changed along this coordinate
      if (!isZero(vertexVal[vert_el])) {
        newVectorIndex.push_back(vertexIndex[vert_el]);
        newVectorVal.push_back(vertexVal[vert_el]);
      }

      vert_el++;
    }

    double init_vert_val = 0.0;
    if ((vert_el < numVertexEls) && (vertexIndex[vert_el] == ray_ind)) {
      init_vert_val = vertexVal[vert_el];
      vert_el++;
    }

    const double new_val = init_vert_val + dist * ray_val;
    if (!isZero(new_val)) {
      newVectorIndex.push_back(ray_ind);
      newVectorVal.push_back(new_val);
    }
  }

  // Add any remaining vertex elements
  while (vert_el < numVertexEls) {
    // The new vertex has not changed along this coordinate
    newVectorIndex.push_back(vertexIndex[vert_el]);
    newVectorVal.push_back(vertexVal[vert_el]);

    vert_el++;
  }

  newVector.setVector(newVectorIndex.size(), newVectorIndex.data(),
      newVectorVal.data());
}

/**
 * Find distance to the non-basic bounds
 * Crucially assumes that the ray and vertex coordinates are sorted
 */
double findFirstNBBound(const CoinPackedVector& vertex,
    const CoinPackedVector& rayOut, Hplane& hplane,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo) {
  double dist = solver->getInfinity();

  const int numPointElems = vertex.getNumElements();
  const int numRayElems = rayOut.getNumElements();
  const int* pointIndex = vertex.getIndices();
  const int* rayIndex = rayOut.getIndices();
  const double* pointVal = vertex.getElements();
  const double* rayVal = rayOut.getElements();

  int pt_el = 0;
  for (int ray_el = 0; ray_el < numRayElems; ray_el++) {
    const int ray_ind = rayIndex[ray_el];
    const double rayDirn = rayVal[ray_el];
    const int ray_var = solnInfo.nonBasicVarIndex[ray_ind];

//    if (std::abs(rayDirn) <= param.getRAYEPS()) {
    if (isZero(rayDirn, param.getRAYEPS())) {
      continue; // Ray does not actually go in this direction any distance
    }

    double pt_val = 0.0;
    if (pt_el < numPointElems) {
      int pt_ind = pointIndex[pt_el];

      while (pt_ind < ray_ind) {
        pt_el++;
        if (pt_el < numPointElems) {
          pt_ind = pointIndex[pt_el];
        } else {
          break;
        }
      }

      if (pt_ind == ray_ind) {
        pt_val = pointVal[pt_el];
      }
    }

    const double LB = 0.0;
    if (lessThanVal(pt_val, LB)) {
      // This should not happen; it means we violate a nb bound
      error_msg(errorstring,
          "Point violates non-basic bound of ray %d. Pt_val: %f.\n",
          ray_ind, pt_val);
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    }

    double tmpDist = solver->getInfinity();
    if ((greaterThanVal(pt_val, 0.0,  param.getEPS())) && (lessThanVal(rayDirn, 0.0, param.getRAYEPS()))) {
      // If the ray is in the negative direction, and we are not already
      // at the lower-bound, then how far until the lower bound
      tmpDist = (LB - pt_val) / rayDirn;
    } else if (greaterThanVal(rayDirn, 0.0, param.getRAYEPS())) {
      // If the ray is in the positive direction, how far until upper bound
      if (ray_var >= solnInfo.numCols) {
        continue; // No upper bound on the slacks
      }

      const double UB = getVarUB(solver, ray_var);
      if (greaterThanVal(pt_val, UB - LB)) {
        // This should not happen; it means we violate a nb bound
        error_msg(errorstring,
            "Point violates non-basic bound of ray %d. Pt_val: %f.\n",
            ray_ind, pt_val);
        writeErrorToLog(errorstring, GlobalVariables::log_file);
        exit(1);
      }

      // TODO If ub - lb = 0, then this was basically fixed variable... but this could happen
      if (lessThanVal(pt_val, UB - LB)) {
        tmpDist = ((UB - LB) - pt_val) / rayDirn;
      }
    }

    if (lessThanVal(tmpDist, dist)) {
      dist = tmpDist;
      hplane.var = ray_var;
    }
  }

  if (!isInfinity(dist, solver->getInfinity())) {
    hplane.row = static_cast<int>(HplaneRowFlag::NB);
    hplane.ubflag = static_cast<int>(HplaneBoundFlag::toUBNB);
  }

  return dist;
}


///**
// * The bound is going to be in the basic space, because it is easier conceptually
// * @return distance until ray intersects the hplane (infinity if never)
// */
//double distanceToHplane(const double varVal,
//    const CoinPackedVector& rayOut, const Hplane& hplane,
//    const LiftGICsSolverInterface* const solver, const SolutionInfo& solnInfo) {
//  double dist = solver->getInfinity();
//
//  // Which side of the hyperplane are we trying to hit?
//
//  // Compute whether the ray hits the hyperplane
////  const double vDotH = dotProductWithHplane(vertex, hplane, solnInfo, 0);
////  double origVal = 0.0;
////  if (hplane.row >= 0) {
////    origVal = solnInfo.a0[hplane.row];
////  }
////  const double xVal = origVal + vDotH;
//  const double rDotH = dotProductWithHplane(rayOut, hplane, solnInfo, 1);
//
//  // Ensure the vertex is not violating the hyperplane
//  // Will not happen when we intersect each ray with the first hplane
//  if (hplane.ubflag <= 0) {
//    if (lessThanVal(varVal, hplane.bound)) {
//      return dist;
//    }
//  } else {
//    if (greaterThanVal(varVal, hplane.bound)) {
//      return dist;
//    }
//  }
//
//  // Ensure ray is in the proper direction
//  const bool oppFromLB = (hplane.ubflag <= 0) && (rDotH > -param.getRAYEPS());
//  const bool oppFromUB = (hplane.ubflag >= 1) && (rDotH < param.getRAYEPS());
//  if (oppFromLB || oppFromUB) {
//    return dist;
//  }
//
//  dist = (hplane.bound - varVal) / (rDotH);
//
//  if (isInfinity(dist, solver->getInfinity()) || lessThanVal(dist, 0.0)) {
//    return solver->getInfinity(); // ray does not intersect this hyperplane
//  } else if (isZero(dist)) {
//    return 0.0;
//  } else {
//    return dist;
//  }
//} /* distanceToHplane */
//
///**
// * The bound is going to be in the basic space, because it is easier conceptually
// * @return distance until ray intersects the hplane (infinity if never)
// */
//double distanceToHplane(const CoinPackedVector& vertex,
//    const CoinPackedVector& rayOut, const int rayOutVar, const Hplane& hplane,
//    const LiftGICsSolverInterface* const solver, const int origRayMult) {
//  double dist = solver->getInfinity();
//
//  // Which side of the hyperplane are we trying to hit?
//  // Ensure bound is finite
//  if (isInfinity(std::abs(hplane.bound), solver->getInfinity())) {
//    return dist;
//  }
//
//  // Compute whether the ray hits the hyperplane
//  std::vector<double> BInv(solver->getNumRows());
//  solver->getBInvACol(rayOutVar, &BInv[0]);
//  const int rayMult = isNonBasicUBVar(solver, rayOutVar) ? -1 : 1;
//  const double varVal = getColVal(solver, hplane.structvar);
//
//  // Find the distance to the hyperplane, and ensure it is actually intersected
//  if (hplane.row >= 0) {
//    // If it is a basic variable
//    const double rayVal = rayMult * -1 * BInv[hplane.row];
//    dist = (hplane.bound - varVal) / rayVal;
//  } else {
//    const double LB = getColLB(solver, rayOutVar);
//    const double UB = getColUB(solver, rayOutVar);
//    if (!isNegInfinity(LB, solver->getInfinity())
//        && !isInfinity(UB, solver->getInfinity())) {
//      dist = UB - LB;
//    }
//  }
//
//  if (isInfinity(dist, solver->getInfinity()) || lessThanVal(dist, 0.0)) {
//    return solver->getInfinity(); // ray does not intersect this hyperplane
//  } else if (isZero(dist)) {
//    return 0.0;
//  } else {
//    return dist;
//  }
//} /* distanceToHplane */

double distanceToSplit(const CoinPackedVector& vertex,
    const CoinPackedVector& rayOut, const int split_var,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo) {
  double dist = solver->getInfinity();
  const int split_row = solnInfo.rowOfVar[split_var];

  Hplane hplane(split_var, split_row, static_cast<int>(HplaneBoundFlag::none));

//  const double raySplitDirn = hplane.dotProductWithHplane(rayOut, solver, true,
//      true, &(solnInfo.nonBasicVarIndex));
  const double raySplitDirn = hplane.dotProductWithHplane(rayOut, solnInfo,
      true);

  // Which side of the hyperplane are we trying to hit?
  const double splitVal = solnInfo.a0[hplane.row];

  double boundBasicSpace;
  if (isZero(raySplitDirn, param.getRAYEPS())) {
    // Ray does not intersect the bd of the split
    return dist; // infinity
  } else if (greaterThanVal(raySplitDirn, 0.0, param.getRAYEPS())) {
    hplane.ubflag = static_cast<int>(HplaneBoundFlag::toUB);
    boundBasicSpace = std::ceil(splitVal);
  } else {
    hplane.ubflag = static_cast<int>(HplaneBoundFlag::toLB);
    boundBasicSpace = std::floor(splitVal);
  }

  const bool nullVertex = (vertex.getNumElements() == 0);
  if (nullVertex) {
//    printf("Found null vertex.\n");
  }
//  const double vDotH = hplane.dotProductWithHplane(vertex, solver, false, true,
//      &(solnInfo.nonBasicVarIndex));
  const double vDotH = hplane.dotProductWithHplane(vertex, solnInfo, false);
  const double xVal = splitVal + vDotH;

  // Ensure the vertex is not violating the hyperplane
  // Will not happen when we intersect each ray with the first hplane
  if (hplane.ubflag <= 0) {
    if (lessThanVal(xVal, boundBasicSpace)) {
      return dist;
    }
  } else {
    if (greaterThanVal(xVal, boundBasicSpace)) {
      return dist;
    }
  }

  dist = (boundBasicSpace - xVal) / (raySplitDirn);

//  if (isZero(dist)) {
//    dist = 0.0;
//  }

  if (lessThanVal(dist, 0.0)) {
    // Negative distance means the ray is in the opposite direction
    return solver->getInfinity();
  } else {
    return dist;
  }
}

double dotProductWithOptTableau(const CoinPackedVector& vec, const int row_ind,
    const SolutionInfo& solnInfo) {
  const int numElmts = vec.getNumElements();
  const int* elmtIndices = vec.getIndices();
  const double* elmtVals = vec.getElements();

  double NBRowActivity = 0.0;
  for (int k = 0; k < numElmts; k++) {
    //      if (!isZero(solnInfo.raysOfC1[elmtIndices[k]][row_ind], param.getRAYEPS())
    //          && !isZero(elmtVals[k], param.getEPS())) {
    NBRowActivity += solnInfo.raysOfC1[elmtIndices[k]][row_ind]
        * elmtVals[k];
    //      }
  }
  return (solnInfo.a0[row_ind] + NBRowActivity);
}

/** Assume sorted vector **/
void packedSortedVectorSum(CoinPackedVector& sum, const double mult1,
    const CoinPackedVectorBase& vec1, const double mult2,
    const CoinPackedVectorBase& vec2, const double eps) {
  //CoinShallowPackedVector sum;
  if (isZero(mult1, eps) || isZero(mult2, eps)) {
    if (isZero(mult1, eps) && isZero(mult2, eps)) {
      return;
    }
    double mult = isZero(mult1, eps) ? mult2 : mult1;
    if (isZero(mult1, eps)) {
      sum = vec2;
    } else {
      sum = vec1;
    }
    for (int i = 0; i < sum.getNumElements(); i++) {
      sum.setElement(i, mult * sum.getElements()[i]);
    }
    return;
  }

  const int numVec1Elems = vec1.getNumElements();
  const int numVec2Elems = vec2.getNumElements();
  const int* vec1Index = vec1.getIndices();
  const int* vec2Index = vec2.getIndices();
  const double* vec1Val = vec1.getElements();
  const double* vec2Val = vec2.getElements();

  std::vector<int> sumIndex;
  std::vector<double> sumVal;
  sumIndex.reserve(numVec1Elems + numVec2Elems);
  sumVal.reserve(numVec1Elems + numVec2Elems);

  int vec2_el = 0;
  for (int el = 0; el < numVec1Elems; el++) {
    const int vec1_ind = vec1Index[el];

    double new_val = mult1 * vec1Val[el]; 
    while (vec2_el < numVec2Elems) {
      const int vec2_ind = vec2Index[vec2_el];
      if (vec2_ind < vec1_ind) {
        if (!isZero(vec2Val[vec2_el], eps)) {
          sumIndex.push_back(vec2_ind);
          sumVal.push_back(mult2 * vec2Val[vec2_el]);
        }
        vec2_el++;
      } else if (vec2_ind == vec1_ind) {
        new_val += mult2 * vec2Val[vec2_el];
        vec2_el++;
        break;
      } else {
        break;
      }
    }

    if (!isZero(new_val, eps)) {
      sumIndex.push_back(vec1_ind);
      sumVal.push_back(new_val);
    }
  }

  while (vec2_el < numVec2Elems) {
    if (!isZero(vec2Val[vec2_el], eps)) {
      sumIndex.push_back(vec2Index[vec2_el]);
      sumVal.push_back(mult2 * vec2Val[vec2_el]);
    }
    vec2_el++;
  }

  sum.setVector(sumIndex.size(), sumIndex.data(), sumVal.data(), false);
} /* packedSortedVectorSum */

/** Assume sorted vector **/
double packedSum(const CoinPackedVector& vec1,
    const CoinPackedVector& vec2) {
  const int numVec1Elems = vec1.getNumElements();
  const int numVec2Elems = vec2.getNumElements();
  const double* vec1Val = vec1.getElements();
  const double* vec2Val = vec2.getElements();

  double sum = 0.0;

  for (int el = 0; el < numVec1Elems; el++) {
    sum += vec1Val[el];
  }
  for (int el = 0; el < numVec2Elems; el++) {
    sum += vec2Val[el];
  }

  return sum;
} /* packedSum */

//double packedDotProduct(const CoinPackedVector& vec1,
//    const CoinPackedVector& vec2) {
//  const int numVec1Elems = vec1.getNumElements();
//  const int numVec2Elems = vec2.getNumElements();
//  const int* vec1Index = vec1.getIndices();
//  const int* vec2Index = vec2.getIndices();
//  const double* vec1Val = vec1.getElements();
//  const double* vec2Val = vec2.getElements();
//
//  double dot_prod = 0.0;
//
//  int vec2_el = 0;
//  for (int el = 0; el < numVec1Elems; el++) {
//    const int vec1_ind = vec1Index[el];
//    const double vec1_val = vec1Val[el];
//
//    double vec2_val = 0.0;
//    if (vec2_el < numVec2Elems) {
//      int vec2_ind = vec2Index[vec2_el];
//
//      while (vec2_ind < vec1_ind) {
//        vec2_el++;
//        if (vec2_el < numVec2Elems) {
//          vec2_ind = vec2Index[vec2_el];
//        } else {
//          break;
//        }
//      }
//
//      if (vec2_ind == vec1_ind) {
//        vec2_val = vec2Val[vec2_el];
//      }
//    }
//
//    if (!isZero(vec1_val) && !isZero(vec2_val))
//      dot_prod += vec1_val * vec2_val;
//  }
//
//  return dot_prod;
//}

void calculatePointStats(double& min_SIC_depth, double& max_SIC_depth,
    double& sum_SIC_depth, double& sumSquares_SIC_depth,
    double& min_obj_depth, double& max_obj_depth, double& sum_obj_depth,
    double& sumSquares_obj_depth,
//    const int split_ind,
    const int pt_ind,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    IntersectionInfo& interPtsAndRays,
//    const std::vector<Hplane>& hplaneStore,
    const AdvCut* curr_cut,
    const double cutNorm, const AdvCut& objCut, const double objNorm) {
  // Get current point
  const CoinShallowPackedVector tmpVector = interPtsAndRays.getVector(pt_ind);

//  const int hplane_ind = interPtsAndRays.hplane[pt_ind];
//  const int hplane_row_ind =
//      (hplane_ind < 0) ? -1 : hplaneStore[hplane_ind].row;

  if (pointInP(solver, solnInfo, interPtsAndRays, pt_ind)) {
    interPtsAndRays.numFinalPoints++;
    interPtsAndRays.finalFlag[pt_ind] = true;
  }

  // Get depth wrt SIC
  const double SICactivity = (curr_cut) ? curr_cut->getActivity(tmpVector) : 0.0;
  double curr_SIC_depth = (curr_cut) ? (SICactivity - curr_cut->rhs()) / cutNorm : 0.0;
  if (isZero(curr_SIC_depth)) {
    curr_SIC_depth = +0.0;
  }
  interPtsAndRays.SICDepth.push_back(curr_SIC_depth);

  if (lessThanVal(min_SIC_depth, 0.0) || curr_SIC_depth < min_SIC_depth)
    min_SIC_depth = curr_SIC_depth;
  if (lessThanVal(max_SIC_depth, 0.0) || curr_SIC_depth > max_SIC_depth)
    max_SIC_depth = curr_SIC_depth;
  sum_SIC_depth += curr_SIC_depth;
  sumSquares_SIC_depth += curr_SIC_depth * curr_SIC_depth;

  // Get depth wrt objective
  const double objActivity = objCut.getActivity(tmpVector);
  double curr_obj_depth = (objActivity - objCut.rhs()) / objNorm;
  if (isZero(curr_obj_depth)) {
    curr_obj_depth = +0.0;
  }
  interPtsAndRays.objDepth.push_back(curr_obj_depth);

  if (lessThanVal(min_obj_depth, 0.0) || curr_obj_depth < min_obj_depth)
    min_obj_depth = curr_obj_depth;
  if (lessThanVal(max_obj_depth, 0.0) || curr_obj_depth > max_obj_depth)
    max_obj_depth = curr_obj_depth;
  sum_obj_depth += curr_obj_depth;
  sumSquares_obj_depth += curr_obj_depth * curr_obj_depth;
}

/**
 * Checks that the point p does not violate any hyperplane or bound
 */
bool pointInP(const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo, const IntersectionInfo& interPtsAndRays,
    const int pt_ind) {
  if (isZero(interPtsAndRays.RHS[pt_ind]))
    return false;

  const CoinShallowPackedVector tmpVector = interPtsAndRays.getVector(pt_ind);
  const int numElmts = tmpVector.getNumElements();
  const int* elmtIndices = tmpVector.getIndices();
  const double* elmtVals = tmpVector.getElements();

  return pointInP(solver, solnInfo, numElmts, elmtIndices, elmtVals);
}

bool pointInP(const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo, const int numElmts, const int* elmtIndices,
    const double* elmtVals) {
  // We check to make sure each of the rays stays within its bounds
  for (int k = 0; k < numElmts; k++) {
    if (elmtVals[k] < -param.getEPS()) {
      error_msg(errstr,
          "Point has negative component corresponding to index %d: %f.\n",
          elmtIndices[k], elmtVals[k]);
      writeErrorToLog(errstr, GlobalVariables::log_file);
      exit(1);
    }

    // Make sure we have not proceeded past the bound for each ray
    // Actually, this could happen using Selva's PHA method
    double colLB = 0.0, colUB = solver->getInfinity();
    int currVar = solnInfo.nonBasicVarIndex[elmtIndices[k]];
    if (currVar < solnInfo.numCols) {
      colLB = getVarLB(solver, currVar);
      colUB = getVarUB(solver, currVar);
    }
    if (greaterThanVal(elmtVals[k], colUB - colLB, param.getEPS())) {
      // We have a violation
      // This is possible, since a ray stemming from a new vertex may go past its bound
      // However, if it is the cut ray, then this should not happen
      // (unless using Selva's PHA procedure
      // --- might intersect this ray by a hplane activated later and lying "past the bound")
      return false;
    }
  }

  // For each basic variable, compute its new value, given how far
  // traveled for each of the ray directions in comp NB space
  // Check if any bounds are violated
//  const int split_var = solnInfo.feasSplitVar[split_ind];
//  const int split_row = solnInfo.rowOfVar[split_var];

  for (int row_ind = 0; row_ind < solnInfo.numRows; row_ind++) {
    const int varBasicInRow = solnInfo.varBasicInRow[row_ind];
    const double xVal = solnInfo.a0[row_ind];
    double NBRowActivity = 0.0;
    for (int k = 0; k < numElmts; k++) {
//      if (!isZero(solnInfo.raysOfC1[elmtIndices[k]][row_ind], param.getRAYEPS())
//          && !isZero(elmtVals[k], param.getEPS())) {
      NBRowActivity += solnInfo.raysOfC1[elmtIndices[k]][row_ind]
          * elmtVals[k];
//      }
    }
    double newVal = xVal + NBRowActivity;

//    // We check whether the point lies on the boundary of the split
//    if (row_ind == split_row) {
//      // Value should be either floor or ceiling of xVal
//      const double floorVal = std::floor(xVal);
//      const double ceilVal = std::ceil(xVal);
//
//      if (!isVal(newVal, floorVal) && !isVal(newVal, ceilVal)) {
//        error_msg(errstr,
//            "Point %d not on bd of split %d (var %d), created by activating hplane %d. newVal: %s, floor: %s, ceil: %s.\n",
//            pt_ind, split_ind, varBasicInRow,
//            interPtsAndRays.hplane[pt_ind],
//            stringValue(newVal).c_str(),
//            stringValue(floorVal).c_str(),
//            stringValue(ceilVal).c_str());
//        writeErrorToII(errstr, GlobalVariables::inst_info_out);
//        exit(1);
//      }
//    }

    const double colLB = getVarLB(solver, varBasicInRow);
    const double colUB = getVarUB(solver, varBasicInRow);

    // What we really want to ensure is that the variable corresponding
    // to the hplane we activated (if there was one) stays within its bounds
    if (lessThanVal(newVal, colLB, param.getEPS())
        || greaterThanVal(newVal, colUB, param.getEPS())) {
//      if (hplane_row_ind <= -1)
//        return false;
//
//      if (row_ind == hplane_row_ind) {
//        error_msg(errstr,
//            "Basic var %d corresponding to hplane %d activated on split %d yielding pt %d is outside its bounds. newVal: %s, colLB: %s, colUB: %s.\n",
//            varBasicInRow, interPtsAndRays.hplane[pt_ind],
//            split_ind, pt_ind, stringValue(newVal).c_str(),
//            stringValue(colLB).c_str(), stringValue(colUB).c_str());
//        writeErrorToII(errstr, GlobalVariables::inst_info_out);
//        exit(1);
//      }

      return false;
    }
  }

  return true;
}

/*********************/
/* GENERAL FUNCTIONS */
/*********************/

/***********************************************************************/
/**
 * @brief Get environment variable by key.
 * @param key :: Key to look up.
 */
std::string get_env_var(std::string const & key) {
  char * val;
  val = getenv(key.c_str());
  std::string retval = "";
  if (val != NULL) {
    retval = val;
  }
  return retval;
}

/***********************************************************************/
/**
 * @brief Find value in a std::vector and return where it is in the std::vector.
 * @param key :: Key to look up.
 */
int find_val(const int &key, const std::vector<int> &vec) {
  for (int k = 0; k < (int) vec.size(); k++) {
    if (vec[k] == key)
      return k;
  }
  return -1;
}

/***********************************************************************/
/**
 * @brief Find value in a std::vector and return where it is in the std::vector.
 * @param key :: Key to look up.
 */
int find_val(const double &key, const std::vector<double> &vec) {
  for (int k = 0; k < (int) vec.size(); k++) {
    if (vec[k] == key)
      return k;
  }
  return -1;
}

/***********************************************************************/
/**
 * @brief Find value in a std::vector and return where it is in the std::vector.
 * @param key :: Key to look up.
 */
int find_val(const std::string &key, const std::vector<std::string> &vec) {
  for (int k = 0; k < (int) vec.size(); k++) {
    if (vec[k].compare(key) == 0)
      return k;
  }
  return -1;
}

/****************************************************************************/
/**
 * @brief Check if a given file exists in the system.
 *
 * @returns True if exists, False otherwise
 */
bool fexists(const char *filename) {
//  std::ifstream ifile(filename);
//  return ifile; // This worked before g++5
  struct stat buffer;
  return (stat (filename, &buffer) == 0);
}

///********************************************************************************/
///**
// *
// */
//double computeMinCutEucl(AdvCuts &cutinfo) {
//  double min = param.getINFINITY();
//  for (int i = 0; i < (int) cutinfo.size(); i++) {
//    if (cutinfo[i].feas) {
//      if (cutinfo[i].eucl < min - param.getEPS()) {
//        min = cutinfo[i].eucl;
//      }
//    }
//  }
//
//  return min;
//}
//
///********************************************************************************/
///**
// *
// */
//double computeMaxCutEucl(AdvCuts &cutinfo) {
//  double max = -1 * param.getINFINITY();
//  for (int i = 0; i < (int) cutinfo.size(); i++) {
//    if (cutinfo[i].feas) {
//      if (cutinfo[i].eucl > max - param.getEPS()) {
//        max = cutinfo[i].eucl;
//      }
//    }
//  }
//
//  return max;
//}
//
///********************************************************************************/
///**
// *
// */
//double computeAverageCutEucl(AdvCuts &cutinfo) {
//  int n = 0;
//  double sum = 0.0;
//  for (int i = 0; i < (int) cutinfo.size(); i++) {
//    if (cutinfo[i].feas) {
//      n++;
//      sum += cutinfo[i].eucl;
//    }
//  }
//
//  if (n == 0) {
//    return 0.0;
//  }
//
//  return sum / n;
//}
//
///********************************************************************************/
///**
// *
// */
//double computeMinCutObj(AdvCuts &cutinfo) {
//  double min = param.getINFINITY();
//  for (int i = 0; i < (int) cutinfo.size(); i++) {
//    if (cutinfo[i].feas) {
//      if (cutinfo[i].postCutObj < min - param.getEPS()) {
//        min = cutinfo[i].postCutObj;
//      }
//    }
//  }
//
//  return min;
//}
//
///********************************************************************************/
///**
// *
// */
//double computeMaxCutObj(AdvCuts &cutinfo) {
//  double max = -1 * param.getINFINITY();
//  for (int i = 0; i < (int) cutinfo.size(); i++) {
//    if (cutinfo[i].feas) {
//      if (cutinfo[i].postCutObj > max - param.getEPS()) {
//        max = cutinfo[i].postCutObj;
//      }
//    }
//  }
//
//  return max;
//}
//
///********************************************************************************/
///**
// *
// */
//double computeAverageCutObj(AdvCuts &cutinfo) {
//  int n = 0;
//  double sum = 0.0;
//  for (int i = 0; i < (int) cutinfo.size(); i++) {
//    if (cutinfo[i].feas) {
//      n++;
//      sum += cutinfo[i].postCutObj;
//    }
//  }
//
//  if (n == 0) {
//    return 0.0;
//  }
//
//  return sum / n;
//}

double computeAverage(const std::vector<double> &vec) {
  return computeAverage(vec, 0, (int) vec.size() - 1);
}

double computeAverage(const std::vector<int> &vec) {
  return computeAverage(vec, 0, (int) vec.size() - 1);
}

double computeAverage(const std::vector<double> &vec, const int start_index,
    const int end_index) {
//  if (start_index < 0 || start_index > vecsize || end_index < 0
//      || end_index > vecsize || start_index > end_index)
//    return 0.0;

  double sum = 0.0;
  int n = end_index - start_index + 1;
  for (int i = start_index; i <= end_index; i++) {
    sum += vec[i];
  }

  return sum / n;
}

double computeAverage(const std::vector<int> &vec, const int start_index,
    const int end_index) {
//  if (start_index < 0 || start_index > vecsize || end_index < 0
//      || end_index > vecsize || start_index > end_index)
//    return 0.0;

  double sum = 0.0;
  int n = end_index - start_index + 1;
  for (int i = start_index; i <= end_index; i++) {
    sum += vec[i];
  }

  return sum / n;
}

double computeAverage(const std::vector<double> &vec,
    const std::vector<int> &index) {
  int n = (int) index.size();
//  if (n == 0)
//    return 0.0;

  double sum = 0.0;
  for (int i = 0; i < (int) index.size(); i++) {
    sum += vec[index[i]];
  }

  return sum / n;
}

double computeAverage(const std::vector<int> &vec,
    const std::vector<int> &index) {
  int n = (int) index.size();
//  if (n == 0)
//    return 0.0;

  double sum = 0.0;
  for (int i = 0; i < (int) index.size(); i++) {
    sum += vec[index[i]];
  }

  return sum / n;
}

double computeAverageDiff(const std::vector<double> &vec1,
    const std::vector<double> &vec2) {
  return computeAverageDiff(vec1, vec2, 0, (int) vec1.size() - 1);
}

double computeAverageDiff(const std::vector<int> &vec1,
    const std::vector<int> &vec2) {
  return computeAverageDiff(vec1, vec2, 0, (int) vec1.size() - 1);
}

double computeAverageDiff(const std::vector<double> &vec1,
    const std::vector<double> &vec2, const int start_index,
    const int end_index) {
//  if (start_index < 0 || start_index > vecsize || end_index < 0
//      || end_index > vecsize || start_index > end_index)
//    return 0.0;

  double sum = 0.0;
  int n = end_index - start_index + 1;
  for (int i = start_index; i <= end_index; i++) {
    sum += vec1[i] - vec2[i];
  }

  return sum / n;
}

double computeAverageDiff(const std::vector<int> &vec1,
    const std::vector<int> &vec2, const int start_index,
    const int end_index) {
//  if (start_index < 0 || start_index > vecsize || end_index < 0
//      || end_index > vecsize || start_index > end_index)
//    return 0.0;

  double sum = 0.0;
  int n = end_index - start_index + 1;
  for (int i = start_index; i <= end_index; i++) {
    sum += vec1[i] - vec2[i];
  }

  return sum / n;
}

double computeAverageDiff(const std::vector<double> &vec1,
    const std::vector<double> &vec2, const std::vector<int> &index) {
  int n = (int) index.size();
//  if (n == 0)
//    return 0.0;

  double sum = 0.0;
  for (int i = 0; i < (int) index.size(); i++) {
    sum += vec1[index[i]] - vec2[index[i]];
  }

  return sum / n;
}

double computeAverageDiff(const std::vector<int> &vec1,
    const std::vector<int> &vec2, const std::vector<int> &index) {
  int n = (int) index.size();
//  if (n==0)
//    return 0.0;

  double sum = 0.0;
  for (int i = 0; i < (int) index.size(); i++) {
    sum += vec1[index[i]] - vec2[index[i]];
  }

  return sum / n;
}

std::vector<double>::const_iterator computeMin(const std::vector<double> &vec) {
  std::vector<double>::const_iterator minit = vec.begin();
  for (std::vector<double>::const_iterator it = vec.begin(); it != vec.end();
      ++it) {
    if (*it < *minit) {
      minit = it;
    }
  }

  return minit;
}

double computeMin(const int num_el, const double* vec) {
  int min_ind = 0;
  if (num_el == 0) {
    return std::numeric_limits<double>::lowest();
  }
  for (int i = 1; i < num_el; ++i) {
    if (vec[i] < vec[min_ind]) {
      min_ind = i;
    }
  }

  return vec[min_ind];
}

double computeMinAbs(const int num_el, const double* vec) {
  int min_ind = 0;
  if (num_el == 0) {
    return std::numeric_limits<double>::lowest();
  }
  for (int i = 1; i < num_el; ++i) {
    if (std::abs(vec[i]) < std::abs(vec[min_ind])) {
      min_ind = i;
    }
  }

  return std::abs(vec[min_ind]);
}

double computeMaxAbs(const int num_el, const double* vec) {
  int max_ind = 0;
  if (num_el == 0) {
    return std::numeric_limits<double>::max();
  }
  for (int i = 1; i < num_el; ++i) {
    if (std::abs(vec[i]) > std::abs(vec[max_ind])) {
      max_ind = i;
    }
  }

  return std::abs(vec[max_ind]);
}

std::vector<int>::const_iterator computeMin(const std::vector<int> &vec) {
  std::vector<int>::const_iterator minit = vec.begin();
  for (std::vector<int>::const_iterator it = vec.begin(); it != vec.end();
      ++it) {
    if (*it < *minit) {
      minit = it;
    }
  }

  return minit;
}

///********************************************************************************/
///**
// *
// */
//std::vector<int>::iterator computeMinSortedAsc(std::vector<int> &vec) {
//  std::vector<int>::iterator minit = vec.begin();
//  for (std::vector<int>::iterator it = vec.begin(); it != vec.end(); ++it) {
//    if (*it < *minit) {
//      minit = it;
//    }
//  }
//
//  return minit;
//}

std::vector<double>::const_iterator computeMin(const std::vector<double> &vec,
    const std::vector<int> &index) {
  std::vector<double>::const_iterator minit = vec.begin() + index[0];
  for (std::vector<int>::const_iterator it = index.begin(); it != index.end();
      ++it) {
    if (*(vec.begin() + *it) < *minit) {
      minit = vec.begin() + *it;
    }
  }

  return minit;
}

std::vector<int>::const_iterator computeMin(const std::vector<int> &vec,
    const std::vector<int> &index) {
  std::vector<int>::const_iterator minit = vec.begin() + index[0];
  for (std::vector<int>::const_iterator it = index.begin(); it != index.end();
      ++it) {
    if (*(vec.begin() + *it) < *minit) {
      minit = vec.begin() + *it;
    }
  }

  return minit;
}

std::vector<double>::const_iterator computeMax(const std::vector<double> &vec) {
  std::vector<double>::const_iterator minit = vec.begin();
  for (std::vector<double>::const_iterator it = vec.begin(); it != vec.end();
      ++it) {
    if (*it > *minit) {
      minit = it;
    }
  }

  return minit;
}

std::vector<int>::const_iterator computeMax(const std::vector<int> &vec) {
  std::vector<int>::const_iterator minit = vec.begin();
  for (std::vector<int>::const_iterator it = vec.begin(); it != vec.end();
      ++it) {
    if (*it > *minit) {
      minit = it;
    }
  }

  return minit;
}

std::vector<double>::const_iterator computeMax(const std::vector<double> &vec,
    const std::vector<int> &index) {
  std::vector<double>::const_iterator minit = vec.begin() + index[0];
  for (std::vector<int>::const_iterator it = index.begin(); it != index.end();
      ++it) {
    if (*(vec.begin() + *it) > *minit) {
      minit = vec.begin() + *it;
    }
  }

  return minit;
}

std::vector<int>::const_iterator computeMax(const std::vector<int> &vec,
    const std::vector<int> &index) {
  std::vector<int>::const_iterator minit = vec.begin() + index[0];
  for (std::vector<int>::const_iterator it = index.begin(); it != index.end();
      ++it) {
    if (*(vec.begin() + *it) > *minit) {
      minit = vec.begin() + *it;
    }
  }

  return minit;
}

/********************************************************************************/
/**
 * @brief Computes minimum difference between the two std::vectors.
 * This is not commutative. We compute min_{i} (vec1[i] - vec2[i]).
 */
double computeMinDiff(const std::vector<double> &vec1,
    const std::vector<double> &vec2) {
  double currmin = vec1[0] - vec2[0];

  std::vector<double>::const_iterator it1 = vec1.begin();
  std::vector<double>::const_iterator it2 = vec2.begin();
  while (it1 != vec1.end() && it2 != vec2.end()) {
    double tmp = *it1 - *it2;
    if (tmp < currmin) {
      currmin = tmp;
    }
  }

  return currmin;
}

/********************************************************************************/
/**
 * @brief Computes minimum difference between the two std::vectors.
 * This is not commutative. We compute min_{i} (vec1[i] - vec2[i]).
 */
int computeMinDiff(const std::vector<int> &vec1, const std::vector<int> &vec2) {
  int currmin = vec1[0] - vec2[0];

  std::vector<int>::const_iterator it1 = vec1.begin();
  std::vector<int>::const_iterator it2 = vec2.begin();
  while (it1 != vec1.end() && it2 != vec2.end()) {
    int tmp = *it1 - *it2;
    if (tmp < currmin) {
      currmin = tmp;
    }
  }

  return currmin;
}

/********************************************************************************/
/**
 * @brief Computes minimum difference between the two std::vectors, only considering
 * those indices provided.
 * This is not commutative. We compute min_{i \in index} (vec1[i] - vec2[i]).
 */
double computeMinDiff(const std::vector<double> &vec1,
    const std::vector<double> &vec2, const std::vector<int> &index) {
  double currmin = vec1[index[0]] - vec2[index[0]];

  for (int i = 0; i < (int) index.size(); i++) {
    double tmp = vec1[index[i]] - vec2[index[i]];
    if (tmp < currmin)
      currmin = tmp;
  }

  return currmin;
}

/********************************************************************************/
/**
 * @brief Computes minimum difference between the two std::vectors, only considering
 * those indices provided.
 * This is not commutative. We compute min_{i \in index} (vec1[i] - vec2[i]).
 */
int computeMinDiff(const std::vector<int> &vec1, const std::vector<int> &vec2,
    const std::vector<int> &index) {
  int currmin = vec1[index[0]] - vec2[index[0]];

  for (int i = 0; i < (int) index.size(); i++) {
    int tmp = vec1[index[i]] - vec2[index[i]];
    if (tmp < currmin)
      currmin = tmp;
  }

  return currmin;
}

/********************************************************************************/
/**
 * @brief Computes maximum difference between the two std::vectors, only considering
 * those indices provided.
 * This is not commutative. We compute max_{i \in index} (vec1[i] - vec2[i]).
 */
double computeMaxDiff(const std::vector<double> &vec1,
    const std::vector<double> &vec2) {
  double currmax = vec1[0] - vec2[0];

  for (int i = 0; i < (int) vec1.size(); i++) {
    double tmp = vec1[i] - vec2[i];
    if (tmp > currmax)
      currmax = tmp;
  }

  return currmax;
}

/********************************************************************************/
/**
 * @brief Computes maximum difference between the two std::vectors, only considering
 * those indices provided.
 * This is not commutative. We compute max_{i \in index} (vec1[i] - vec2[i]).
 */
int computeMaxDiff(const std::vector<int> &vec1, const std::vector<int> &vec2) {
  int currmax = vec1[0] - vec2[0];

  for (int i = 0; i < (int) vec1.size(); i++) {
    int tmp = vec1[i] - vec2[i];
    if (tmp > currmax)
      currmax = tmp;
  }

  return currmax;
}

/********************************************************************************/
/**
 * @brief Computes maximum difference between the two std::vectors, only considering
 * those indices provided.
 * This is not commutative. We compute max_{i \in index} (vec1[i] - vec2[i]).
 */
double computeMaxDiff(const std::vector<double> &vec1,
    const std::vector<double> &vec2, const std::vector<int> &index) {
  double currmax = vec1[index[0]] - vec2[index[0]];

  for (int i = 0; i < (int) index.size(); i++) {
    double tmp = vec1[index[i]] - vec2[index[i]];
    if (tmp > currmax)
      currmax = tmp;
  }

  return currmax;
}

/********************************************************************************/
/**
 * @brief Computes maximum difference between the two std::vectors, only considering
 * those indices provided.
 * This is not commutative. We compute max_{i \in index} (vec1[i] - vec2[i]).
 */
int computeMaxDiff(const std::vector<int> &vec1, const std::vector<int> &vec2,
    const std::vector<int> &index) {
  int currmax = vec1[index[0]] - vec2[index[0]];

  for (int i = 0; i < (int) index.size(); i++) {
    int tmp = vec1[index[i]] - vec2[index[i]];
    if (tmp > currmax)
      currmax = tmp;
  }

  return currmax;
}

/********************/
/* CONVERSION FUNCS */
/********************/

/***********************************************************************/
/**
 * The solution to the LP relaxation of the problem has all non-basic
 *  variables are at zero, but these are not all zero when we compute
 *  rays that intersect with bd S. Since this computation is done all
 *  in the non-basic space, but we are using the structural space for
 *  computing the lifting, we need to find the coordinates of the
 *  intersection points in the structural space.
 *
 *  @param solver       ::  Solver we are using
 *  @param rowOfVar     ::  Row in which variable is basic (indexed from 0 to numcols)
 *  @param newpt        ::  [point][value]
 *  @param nbinfo       ::  [point][index of nb ray][value]
 */
void compStructCoords(const PointCutsSolverInterface* const solver, std::vector<int> &rowOfVar,
    std::vector<std::vector<double> > &newpt,
    std::vector<std::vector<std::vector<double> > > &nbinfo) {
#ifdef TRACE
  std::cout
  << "\n############### Starting compStructCoords routine to compute structural coordinates of intersection points. ###############"
  << std::endl;
#endif

  // The 2D std::vector passed as an argument holds the indices for the
  // non-basic columns and the non-basic variables' new values.
  int numpoints = (int) nbinfo.size();
#ifdef TRACE
  printf("Number of points is %d.\n", numpoints);
#endif

  int numcols = solver->getNumCols();
  int numrows = solver->getNumRows();
  const double *soln = solver->getColSolution();

  // For each point, update lp relaxed vertex to new coordinates
  for (int pt = 0; pt < numpoints; pt++) {
    // Assign newpt current value of the solution
    std::vector<double> currpt(soln, soln + numcols);

    // Iterate over the nonzero NB coordinates for that point
    for (int coor = 0; coor < (int) nbinfo[pt].size(); coor++) {
      int ptIndex = static_cast<int>(nbinfo[pt][coor][0]);
      double ptVal = nbinfo[pt][coor][1];

      // If this non-basic var has a non-zero component, we need to count it
      if (ptVal != 0) {
        // For \bar a, which we will use multiplied by value of points
        double *ainv = new double[numrows];
        // If slack/artificial variable, use basis inverse.
        // @TODO **** POSSIBLE ISSUE ****
        // 1. What about artificial variables?
        // 2. This assumes coefficient of slack variable is positive...
        if (ptIndex >= numcols) {
//          printf("Here slack. Index is %d.\n", ptIndex);
          // Slack variable has some index >= numcols
          // Its index - numcols is the index of the column
          //   in the basis that it corresponds to
          solver->getBInvCol(ptIndex - numcols, ainv);
        } else { // Else, structural variable, so use inverse of the original column
//          printf("Here structural. Index is %d.\n", ptIndex);
          solver->getBInvACol(ptIndex, ainv);
        }

        // x_k = \bar a_{k0} - \sum_{j \in N} \bar a_{kj} x_j
//        for (int row = 0; row < numrows; row++) {
//          int varIndex = varBasicInRow[row];
//          if (varIndex < (int)currpt.size()) // Make sure the row's basic variable is structural
//            currpt[varIndex] = currpt[varIndex] - ainv[row] * ptVal;
//          printf("Here4. Var: %d. Row: %d.\n", varIndex, row);
//        }
        for (int var = 0; var < numcols; var++) {
          int rowIndex = rowOfVar[var];
          currpt[var] = currpt[var] - ainv[rowIndex] * ptVal;
//          printf("Here4. Var: %d. Row: %d.\n", var, rowIndex);
        }

        delete[] ainv;
      }
    }
    newpt.push_back(currpt);
  }
}

/**
 * Create coordinates of the point from the std::vector structSolution,
 * using the cobasis defined in solnInfo
 */
void compNBCoor(std::vector<double>& NBSolution, const double* structSolution,
    const PointCutsSolverInterface* const tmpSolver,
    const PointCutsSolverInterface* const origSolver,
    const SolutionInfo& origSolnInfo) {
  NBSolution.resize(origSolnInfo.numNB);

  // All coefficients are going to be non-negative, since we work in the complemented NB space
  for (int i = 0; i < origSolnInfo.numNB; i++) {
    int currVar = origSolnInfo.nonBasicVarIndex[i];
    double tmpVal, origVal;
    if (currVar < origSolnInfo.numCols) {
      tmpVal = structSolution[currVar];
      origVal = origSolver->getColSolution()[currVar];
    } else {
      int currRow = currVar - origSolnInfo.numCols;
      tmpVal = tmpSolver->getRightHandSide()[currRow]
          - tmpSolver->getRowActivity()[currRow];
      origVal = 0.0;
    }
    // ***
    // For *struct* var:
    // If the variable is lower-bounded, then we need to shift it up for the comp space
    // That is, xComp = x - lb
    // If the variable is upper-bounded, we need to get the value in the complemented space
    // That is, xComp = ub - x
    // What to do if the variable is fixed NB or free NB?
    // In the former case, thinking of it as a lower-bounded variable, it will simply be zero
    // in any solution, including in the original one at v, in which case we don't need to
    // worry about it for the packed ray.
    // In the latter case, we may need to do something more clever, as in wrt original val
    // of variable in the NB space at v.
    // Actually, value should not change from that in v...?
    // TODO Figure out what to do for free vars
    // ****
    // For *slack* var:
    // In the original NB space at v, all slacks were at zero.
    // We simply look at b[row] - activity[row]
    // For >= rows, slack is non-positive
    // For <= rows, slack is non-negative
    // We take absolute value to deal with this
    const double newVal = std::abs(tmpVal - origVal);
    if (!isZero(newVal))
      NBSolution[i] = newVal;
    else
      NBSolution[i] = 0.0;
  }
//  CoinPackedVector::setVector((int) packedIndex.size(), packedIndex.data(),
//      packedVal.data(), false);
} /* compNBCoor */

/***********************************************************************/
/**
 * @brief Corrects coefficients and right side of cut that are incorrect because of
 * complemented variables from the original problem.
 *
 * @param orderedLiftedCut  ::  [0: split, 1: rhs, 2+: coeffs...]
 * @param deletedCols       ::  Indices of removed columns
 * @param solver            ::  To retrieve original values
 */
//void complementCoeffs(AdvCut &orderedLiftedCut, std::vector<int> &deletedCols,
//    LiftGICsSolverInterface* solver) {
//#ifdef TRACE
//  printf(
//      "\n############### Starting complementCoeffs routine for correcting coefficients of variables that were complemented. ###############\n");
//#endif
//  double tmpRHS = orderedLiftedCut.rhs();
//  for (int col = 0; col < (int) deletedCols.size(); col++) {
//    int currcol = deletedCols[col];
//    double currval = solver->getColSolution()[currcol];
//    double currlb = solver->getColLower()[currcol];
//    double currub = solver->getColUpper()[currcol];
//    double coeff = orderedLiftedCut[currcol];
//
//    // Check if the deleted column was non-zero
//    if (currval == currub) {
//      // We complemented by ub
//#ifdef TRACE
//      printf(
//          "\tComplementing coefficient of col %d which was at upper-bound (= %f).\n",
//          currcol, currub);
//      printf("\tOld rhs: %f. Old coeff: %f.\n", tmpRHS,
//          coeff);
//#endif
//      tmpRHS -= currub * coeff;
//      orderedLiftedCut[currcol] = -1 * coeff;
//#ifdef TRACE
//      printf("\tNew rhs: %f. New coeff: %f.\n", orderedLiftedCut.RHS,
//          orderedLiftedCut.coeff[currcol]);
//#endif
//    } else if (currval == currlb) {
//      // We complemented by lb
//#ifdef TRACE
//      printf(
//          "\tComplementing coefficient of col %d which was at lower-bound (= %f).\n",
//          currcol, currlb);
//      printf("\tOld rhs: %f. Old coeff: %f.\n", orderedLiftedCut.RHS,
//          coeff);
//#endif
////      orderedLiftedCut[currcol] = -1 * coeff;
//      tmpRHS -= currlb * coeff;
//#ifdef TRACE
//      printf("\tNew rhs: %f. New coeff: %f.\n", orderedLiftedCut.RHS,
//          orderedLiftedCut.coeff[currcol]);
//#endif
//    }
//  }
//}
/***********************************************************************/
/**
 * Because Selva's code generates the cut
 * \sum_{s \in N \cap Slack Vars} s_j/\theta_j
 *   + \sum_{j \in N \cap L} (x_j - \ell_j)/\theta_j
 *   + \sum_{j \in N \cap L} (u_j - x_j)/\theta_j
 *   \ge 1
 * This is the so-called y space, if we substitute y_j in there.
 * And we want it in the original x space.
 */
void convertCutFromYSpaceToXSpace(AdvCut& cutinfo, const SolutionInfo& solnInfo,
    const PointCutsSolverInterface* const solver) {
  const CoinPackedVector vec = cutinfo.row();
  const int numElem = vec.getNumElements();
  const int* cutIndex = vec.getIndices();
  std::vector<double> cutCoeff(vec.getElements(),
      vec.getElements() + numElem);

  // Account for upper-bounded variables correctly
  // (They need to be complemented)
  // Also lower-bounded variables adjust the RHS
  double rhs = cutinfo.rhs(); // Recall all cuts are ax >= b
  for (int i = 0; i < numElem; i++) {
    const int curr_ind = cutIndex[i];
    int curr_var = solnInfo.nonBasicVarIndex[curr_ind];
    if (curr_var < solnInfo.numCols) {
      if (isNonBasicUBVar(solver, curr_var)) {
        rhs -= cutCoeff[i] * getVarUB(solver, curr_var);
        cutCoeff[i] *= -1;
      } else if (isNonBasicLBVar(solver, curr_var)) {
        rhs += cutCoeff[i] * getVarLB(solver, curr_var);
      }
    } else {
      if (solver->getRowSense()[curr_var - solnInfo.numCols] == 'G') {
        cutCoeff[i] *= -1;
      }
    }
  }

  // Normalize
  double absrhs = std::abs(rhs);
  if (!isZero(rhs)) {
    if (lessThanVal(rhs, 0.0)) {
      rhs = -1.0;
    } else {
      rhs = 1.0;
    }
    for (int col = 0; col < solnInfo.numNB; col++) {
      cutCoeff[col] = cutCoeff[col] / absrhs;
    }
  } else {
    rhs = 0.0;
  }

  cutinfo.setRow(numElem, cutIndex, cutCoeff.data());
  cutinfo.setLb(rhs);
} /* convertCutFromYSpaceToXSpace */

/***********************************************************************/
/**
 * @brief Converts a cut from the non-basic space to the structural space.
 *
 * @param structCut               ::  OUTPUT: [split var index, rhs, coeffs]
 * @param nonBasicOrigVarIndex    ::  Indices of NB structural variables
 * @param nonBasicSlackVarIndex   ::  Indices of NB slack variables
 * @param solver                  ::  Solver
 * @param JspaceCut               ::  [split var index, rhs, coeffs]
 */
/***********************************************************************/
//void convertCutFromJSpaceToStructSpace(AdvCut &structCut,
//    std::vector<int> &nonBasicOrigVarIndex,
//    std::vector<int> &nonBasicSlackVarIndex, LiftGICsSolverInterface* solver,
//    AdvCut &JspaceCut) {
//  // Useful info
//  int numcols = solver->getNumCols();
//  int numrows = solver->getNumRows();
//  std::vector<int> cstat(numcols), rstat(numrows);
//  solver->getBasisStatus(&cstat[0], &rstat[0]);
//
//  // Allocate space
//  structCut.coeff.clear();
//  structCut.coeff.resize(numcols);
//  structCut.splitVarIndex = JspaceCut.splitVarIndex;
//  structCut.splitVarName = JspaceCut.splitVarName;
//  structCut.splitIndex = JspaceCut.splitIndex;
//  structCut.feas = JspaceCut.feas;
//  structCut.lopsided = JspaceCut.lopsided;
//
//  double StructCutRHS = JspaceCut.RHS;
//  const double* rowRHS = solver->getRightHandSide();
//  const char* rowSense = solver->getRowSense();
////  const double* UB = solver->getColUpper();
////  const double* LB = solver->getColLower();
//
//  int countnonzero = 0;
//  for (int i = 0; i < (int) JspaceCut.coeff.size(); i++) {
//    if (std::abs(JspaceCut.coeff[i]) > param.getEPS()) {
//      //printf("Coeff %d is %E.\n", i, JspaceCut[i]);
//      countnonzero++;
//    } else {
//      JspaceCut.coeff[i] = 0;
//    }
//  }
//
//  for (int i = 0; i < (int) nonBasicOrigVarIndex.size(); i++) {
//    int currvar = nonBasicOrigVarIndex[i];
//    structCut.coeff[currvar] = JspaceCut.coeff[i];
//  }
//
//  const CoinPackedMatrix *mat = solver->getMatrixByRow();
//  int mult = 1.0;
//  for (int i = 0; i < (int) nonBasicSlackVarIndex.size(); i++) {
//    int currvar = nonBasicSlackVarIndex[i];
//    if (rowSense[currvar - numcols] == 'G')
//      mult = 1.0;
//    else if (rowSense[currvar - numcols] == 'L')
//      mult = -1.0;
//    const CoinShallowPackedVector constRow = mat->getVector(
//        currvar - numcols);
//    int constRowNumElements = constRow.getNumElements();
//    const int* constRowIndices = constRow.getIndices();
//    const double* constRowVals = constRow.getElements();
//
//    StructCutRHS += mult * rowRHS[currvar - numcols]
//        * JspaceCut.coeff[(int) nonBasicOrigVarIndex.size() + i];
//    for (int j = 0; j < constRowNumElements; j++) {
//      structCut.coeff[constRowIndices[j]] += mult * constRowVals[j]
//          * JspaceCut.coeff[(int) nonBasicOrigVarIndex.size() + i];
//    }
//
//  }
//
//  if (std::abs(StructCutRHS) > param.getEPS()) {
//    for (int i = 0; i < numcols; i++) {
//      structCut.coeff[i] = structCut.coeff[i] / std::abs(StructCutRHS);
//    }
//  }
//
//  if (StructCutRHS >= param.getEPS()) {
//    StructCutRHS = 1.0;
//  } else {
//    if (StructCutRHS <= -param.getEPS()) {
//      StructCutRHS = -1.0;
//    } else {
//      StructCutRHS = 0.0;
//    }
//  }
//
//  structCut.RHS = StructCutRHS;
//}
/***********************************************************************/
/**
 * We are given a cut in the structural space and we want to convert
 * it to the non-basic space. This means, for all structural variables
 * that are basic, we replace
 */
//void convertStructSpaceCutToJSpace(cut &JCut, LiftGICsSolverInterface* solver,
//    std::vector<int> &rowOfVar, std::vector<int> &nonBasicOrigVarIndex,
//    std::vector<int> &nonBasicSlackVarIndex, cut &structCut) {
//  // Useful info
//  int numcols = solver->getNumCols();
//  int numrows = solver->getNumRows();
//  std::vector<int> cstat(numcols), rstat(numrows);
//  solver->getBasisStatus(&cstat[0], &rstat[0]);
//
//  const double* rowRHS = solver->getRightHandSide();
//  const double* rowActivity = solver->getRowActivity();
//  const char* rowSense = solver->getRowSense();
////  const double* UB = solver->getColUpper();
////  const double* LB = solver->getColLower();
//
//  // Allocate space
//  JCut.coeff.clear();
//  JCut.coeff.resize(numcols, 0.0);
//  JCut.splitVarIndex = structCut.splitVarIndex;
//  JCut.splitIndex = structCut.splitIndex;
//  JCut.splitVarName = structCut.splitVarName;
//  JCut.RHS = structCut.RHS;
//  JCut.feas = structCut.feas;
//  JCut.lopsided = structCut.lopsided;
//
//  for (int currcol = 0; currcol < numcols; currcol++) {
//    double currcoeff = structCut.coeff[currcol];
//    if (cstat[currcol] != 1) { // If it is non-basic, we simply add the same coeff
//      // Note that we are adding because already this coefficient may not be
//      // zero due to times it appeared previously when replacing basic vars.
//      int i = 0;
//      for (i = 0; i < (int) nonBasicOrigVarIndex.size(); i++)
//        if (currcol == nonBasicOrigVarIndex[i])
//          break;
//      JCut.coeff[i] += currcoeff;
//    } else { // If it is basic, we need to get rid of it
//      int currrow = rowOfVar[currcol];
//
//      std::vector<double> structVarCoeff(numcols), slackVarCoeff(numrows);
//      solver->getBInvARow(currrow, &structVarCoeff[0], &slackVarCoeff[0]);
//
//      // Calculate \bar a_{row,0} and also update coeffs
//      int i = 0; // 2 + i will be where we put the updated coeff in JCut
//      double bara0 = solver->getColSolution()[currcol];
//      for (int col = 0; col < numcols; col++) {
//        // Only look at non-basic columns (all other coeffs are zero)
//        if (cstat[col] != 1) {
//          double tmpcoeff = structVarCoeff[col];
//          JCut.coeff[i] -= currcoeff * tmpcoeff;
//          bara0 += tmpcoeff * solver->getColSolution()[col];
//          i++;
//        }
//      }
//
//      // Go through all the slack variables, and if they are corresponding to a
//      // >= row, then we need to negate them.
//      // TODO actually do we need this? This essentially converts those
//      // slack variables into excess variables, but is that we want?
//      // Only if are consistently using them as excess variables throughout
//      for (int row = 0; row < numrows; row++) {
//        if (rstat[row] != 1) {
//          double tmpcoeff = slackVarCoeff[row];
//          double slackVal = rowRHS[row] - rowActivity[row]; // Should be zero, right?
//          if (rowSense[row] == 'G') {
//            tmpcoeff *= -1;
//            slackVal *= -1;
//          }
//          JCut.coeff[i] -= currcoeff * tmpcoeff;
//          bara0 += tmpcoeff * slackVal;
//          i++;
//        }
//      }
//
//      // Change the constant side appropriately
//      JCut.RHS -= currcoeff * bara0;
//    }
//  }
//
//  // Re-normalize
//  double absrhs = std::abs(JCut.RHS);
//  if (absrhs > param.getEPS()) {
//    if (JCut.RHS > param.getEPS()) {
//      JCut.RHS = 1.0;
//    } else
//      JCut.RHS = -1.0;
//
//    for (int coeff = 0; coeff < (int) JCut.coeff.size(); coeff++)
//      JCut.coeff[coeff] = JCut.coeff[coeff] / absrhs;
//  } else {
//    JCut.RHS = 0.0;
//  }
//}
/********************************************************************************/
/**
 * @brief Simply decide if two cuts are the same.
 */
//bool cutsDifferent(cut &cut1, cut &cut2) {
//  double ratio = 0.0;
//  // First, we try to get the ratio from RHS, since
//  // normally we have scaled things.
//  if (std::abs(cut1.RHS) < param.getDIFFEPS() || std::abs(cut2.RHS) < param.getDIFFEPS()) {
//    // If rhs1 = 0, but rhs2 is not near 0
//    // then the cuts are clearly different.
//    // Same goes for if rhs2 = 0, rhs1 not near 0.
//    if (std::abs(cut1.RHS - cut2.RHS) > param.getDIFFEPS()) {
//      return true;
//    }
//    // Otherwise, they're both near zero,
//    // and we're not going to be able to get
//    // any kind of ratio from here.
//  } else {
//    ratio = cut1.RHS / cut2.RHS;
//  }
//
//  // Now check if this ratio (if we found one) holds for
//  // all the coefficients. In particular, we check that
//  // std::abs(cut1.coeff - ratio * cut2.coeff) < param.getDIFFEPS().
//  for (int ind = 0; ind < (int) cut1.coeff.size(); ind++) {
//    if (std::abs(cut1.coeff[ind]) < param.getDIFFEPS()
//        || std::abs(cut2.coeff[ind]) < param.getDIFFEPS()) {
//      if (std::abs(cut1.coeff[ind] - cut2.coeff[ind]) > param.getDIFFEPS())
//        return true;
//    } else {
////      double currRatio = cut1.coeff[ind] / cut2.coeff[ind];
////      if (std::abs(currRatio) < param.getDIFFEPS()
////          && std::abs(cut1.coeff[ind] - cut2.coeff[ind]) > param.getDIFFEPS())
////        return true;
//      if (ratio == 0.0) { // && std::abs(currRatio) > param.getDIFFEPS()) {
//        ratio = cut1.coeff[ind] / cut2.coeff[ind];
//      } else {
//        if (std::abs(
//            cut1.coeff[ind] - ratio * cut2.coeff[ind]) > param.getDIFFEPS()) {
//          return true;
//        }
//      }
//    }
//  }
//  return false;
//}
/********************************************************************************/
/**
 * Check if two solvers have the same everything:
 * 1. colSolution
 * 2. rowPrice
 * 3. slackVals
 * 4. cstat and rstat
 * 5. reducedCosts
 */
bool solversEqual(const PointCutsSolverInterface* const clp1, const PointCutsSolverInterface* const clp2) {
  int numrows1 = clp1->getNumRows();
  int numcols1 = clp1->getNumCols();
  int numrows2 = clp2->getNumRows();
  int numcols2 = clp2->getNumCols();

  // First check that numrows and numcols are the same
  if (numrows1 != numrows2 || numcols1 != numcols2) {
#ifdef TRACE
    printf("*** ERROR: numrows (%d vs %d) or numcols (%d vs %d) not same.",
        numrows1, numrows2, numcols1, numcols2);
#endif
    return false;
  }

  // Set up cstat, rstat
  std::vector<int> cstat1(numcols1), rstat1(numrows1);
  std::vector<int> cstat2(numcols2), rstat2(numrows2);

  clp1->getBasisStatus(&cstat1[0], &rstat1[0]);
  clp2->getBasisStatus(&cstat2[0], &rstat2[0]);

  for (int col = 0; col < numcols1; col++) {
    double val1 = clp1->getColSolution()[col];
    double val2 = clp2->getColSolution()[col];
    double rc1 = clp1->getReducedCost()[col];
    double rc2 = clp2->getReducedCost()[col];
    if ((std::abs(val1 - val2) > param.getEPS()) || (std::abs(rc1 - rc2) > param.getEPS())
        || (cstat1[col] != cstat2[col])) {
      char errorstring[300];
      snprintf(errorstring, sizeof(errorstring) / sizeof(char),
          "*** ERROR: New col values not the same as old values. Variable %d:\n\t"
              "val1: %f \t val2: %f \t Diff: %e\n\t"
              "rc1: %f \t rc2: %f \t Diff: %e\n\t"
              "cstat1: %d \t cstat2: %d\n", col, val1, val2,
          val1 - val2, rc1, rc2, rc1 - rc2, cstat1[col], cstat2[col]);
      std::cerr << errorstring << std::endl;
//      writeErrorToII(errorstring, inst_info_out);
      return false;
    }
  }
  for (int row = 0; row < numrows1; row++) {
    double sv1 = std::abs(
        clp1->getRightHandSide()[row] - clp1->getRowActivity()[row]);
    double sv2 = std::abs(
        clp2->getRightHandSide()[row] - clp2->getRowActivity()[row]);
    double rp1 = clp1->getRowPrice()[row];
    double rp2 = clp2->getRowPrice()[row];
    if ((std::abs(sv1 - sv2) > param.getEPS()) || (std::abs(rp1 - rp2) > param.getEPS())
        || (rstat1[row] != rstat2[row])) {
      char errorstring[300];
      snprintf(errorstring, sizeof(errorstring) / sizeof(char),
          "*** ERROR: New row values not the same as old values. Variable %d:\n\t"
              "sv1: %f \t sv2: %f \t Diff: %e\n\t"
              "rp1: %f \t rp2: %f \t Diff: %e\n\t"
              "rstat1: %d \t rstat2: %d\n", row, sv1, sv2,
          sv1 - sv2, rp1, rp2, rp1 - rp2, rstat1[row], rstat2[row]);
      std::cerr << errorstring << std::endl;
//      writeErrorToII(errorstring, inst_info_out);
      return false;
    }

  }
  return true;
}

/********************/
/* STRING FUNCTIONS */
/********************/

/***********************************************************************/
/**
 * Parses std::string line by the set of delimiters given.
 * (Only works well with one for now.)
 * Parses std::string based on delimiters provided and puts output into std::vector.
 */
void stringparse(std::string &line, std::string delimiters,
    std::vector<std::string> &strings) {
  if (line.empty())
    return;

  std::string curr;
//  cout << "Here." << " Reading: " << line << ".\n";
  // Get all characters to be used as delimiters
//  cout << "Number of delimiters is " << delimiter.length() << std::endl;
  int numdelims = delimiters.length();
//  numdelims = strlen(delims);
  const char *delims = delimiters.c_str();

  // Make istringstream from line
  for (int i = 0; i < numdelims; i++) {
    std::istringstream tmp;
    tmp.str(line);
    while (getline(tmp, curr, delims[i])) {
//      printf("%s\n", curr.c_str());
//      cout << curr << std::endl;
      strings.push_back(curr);
    }
  }
}

/************************************************************************/
// Singular Value Decomposition (from Lapack);
// needed to compute condition number in norm 2
extern "C" int dgesvd_(char *jobu, char *jobvt, int *m, int *n, double *a,
    int *lda, double *s, double *u, int *ldu, double *vt, int *ldvt,
    double *work, int *lwork, int *info);

/**********************************************************/
double compute_condition_number_norm2(const OsiSolverInterface* const solver) {
  double condition = 0.0;
  if (!solver->basisIsAvailable()) {
    return condition;
  }
  int m = solver->getNumRows();
  int n = solver->getNumCols();
  double* basis = new double[m * m];
  memset(basis, 0, m * m * sizeof(double));
  const CoinPackedMatrix* byCol = solver->getMatrixByCol();
  const int * rowPos = byCol->getIndices();
  const CoinBigIndex * columnStart = byCol->getVectorStarts();
  const int * columnLength = byCol->getVectorLengths();
  const double * columnElements = byCol->getElements();

  const CoinWarmStartBasis* coinBasis = NULL;
  int *rstat = NULL, *cstat = NULL;
  try {
    const OsiClpSolverInterface* const clpsolver = dynamic_cast<const OsiClpSolverInterface* const>(solver);
    coinBasis = clpsolver->getConstPointerToWarmStart();
  } catch (std::exception& e) {
    // Need to store the full basis ourselves
    rstat = new int[m];
    cstat = new int[n];
    solver->getBasisStatus(cstat, rstat);
  }

  int bindex = 0;
  // fill the structural columns of the basis
  for (int i = 0; i < n; ++i) {
    if ((coinBasis) ? coinBasis->getStructStatus(i) == CoinWarmStartBasis::Status::basic : cstat[i] == 1) {
      for (int j = columnStart[i]; j < columnStart[i] + columnLength[i];
          ++j) {
        basis[bindex * m + rowPos[j]] = columnElements[j];
      }
      bindex++;
    }
  }
  // fill the artificial columns of the basis
  for (int i = 0; i < m; ++i) {
    if ((coinBasis) ? coinBasis->getArtifStatus(i) == CoinWarmStartBasis::Status::basic : rstat[i] == 1) {
      basis[bindex * m + i] = 1.0;
      bindex++;
    }
  }
  // prepare data for SVD; get the optimal size of the work array
  int info = 0;
  char job = 'N';
  double* sv = new double[m];
  double sizeWork = 0.0;
  // we first call the SVD routine with lwork = -1; this way, the function
  // returns the optimal size of the work array that is needed for the
  // computation (the value is written in sizeWork)
  int lwork = -1;
  dgesvd_(&job, &job, &m, &m, basis, &m, sv, NULL, &m, NULL, &m, &sizeWork,
      &lwork, &info);
  if (info != 0) {
    printf(
        "### WARNING: cannot obtain optimal size of work array for SVD\n");
    if (rstat)
      delete[] rstat;
    if (cstat)
      delete[] cstat;
    delete[] basis;
    delete[] sv;
    return condition;
  }
  // now we know the optimal size, so we allocate work array and do SVD
  lwork = (int) sizeWork;
  double* work = new double[lwork];
  dgesvd_(&job, &job, &m, &m, basis, &m, sv, NULL, &m, NULL, &m, work, &lwork,
      &info);
  if (info != 0) {
    printf("### WARNING: cannot compute condition number\n");
    if (rstat)
      delete[] rstat;
    if (cstat)
      delete[] cstat;
    delete[] basis;
    delete[] sv;
    delete[] work;
    return condition;
  }
  condition = sv[0] / sv[m - 1];
  if (rstat)
    delete[] rstat;
  if (cstat)
    delete[] cstat;
  delete[] basis;
  delete[] sv;
  delete[] work;
  return condition;
} /* compute_condition_number_norm2 */

double compute_condition_number_norm1(OsiSolverInterface* solver) {
  double condition = 0.0;
  if (!solver->basisIsAvailable()) {
    return condition;
  }
  int m = solver->getNumRows();
  int n = solver->getNumCols();
  int* rstat = new int[m];
  int* cstat = new int[n];
  solver->getBasisStatus(cstat, rstat);
  const CoinPackedMatrix* byCol = solver->getMatrixByCol();
  const CoinBigIndex * columnStart = byCol->getVectorStarts();
  const int * columnLength = byCol->getVectorLengths();
  const double * columnElements = byCol->getElements();
  double norm1B = 0.0;
  double currnorm;
  // norm of structural columns
  for (int i = 0; i < n; ++i) {
    if (cstat[i] == 1) {
      currnorm = 0.0;
      for (int j = columnStart[i]; j < columnStart[i] + columnLength[i];
          ++j) {
        currnorm += std::abs(columnElements[j]);
      }
      if (currnorm > norm1B) {
        norm1B = currnorm;
      }
    }
  }
  // norm of artificial columns
  if (norm1B < 1.0) {
    norm1B = 1.0;
  }
  solver->enableSimplexInterface(true);
  solver->enableFactorization();
  double norm1Binv = 0.0;
  double* binvcol = new double[m];
  for (int i = 0; i < m; ++i) {
    solver->getBInvCol(i, binvcol);
    currnorm = 0.0;
    for (int j = 0; j < m; ++j) {
      currnorm += std::abs(binvcol[j]);
    }
    if (currnorm > norm1Binv) {
      norm1Binv = currnorm;
    }
  }
  solver->disableFactorization();
  solver->disableSimplexInterface();
  condition = norm1B * norm1Binv;
  delete[] rstat;
  delete[] cstat;
  delete[] binvcol;
  return condition;
} /* compute_condition_number_norm1 */

// compute the condition number in the given norm (1 or 2)
double compute_condition_number(OsiSolverInterface* solver, const int norm) {
  if (norm < 1 || norm > 2) {
    printf("### WARNING: cannot compute condition number in norm %d\n",
        norm);
    return 0.0;
  }
  if (norm == 1) {
    return compute_condition_number_norm1(solver);
  }
  return compute_condition_number_norm2(solver);
} /* compute_condition_number */

/*************/
/* DEBUGGING */
/*************/

double rayPostPivot(const int component, const int nb_var_in1,
    const int struct_var_out1, const int nb_var_in2,
//    const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo) {
  const int hplane_row = solnInfo.rowOfVar[struct_var_out1];

  if (hplane_row < 0) { // This has to be the same var as nb_var_in1
    if (nb_var_in2 == nb_var_in1) {
      if (component == 1)
        return -1.0;
      else
        return 0.0;
    } else {
      if (component == 1)
        return 0.0;
      else
        return 1.0;
    }
  }

  if (component == 2)
    return 1.0;
  else
    return -1 * solnInfo.raysOfC1[nb_var_in2][hplane_row]
        / solnInfo.raysOfC1[nb_var_in1][hplane_row];
}

double vertexPostSinglePivot(const int component, const double rayDirnToHplane1,
    const double init_val_out1, const double final_val_out1) {
  if (component == 2)
    return 0.0;

  const double numer = final_val_out1 - init_val_out1;
  const double denom = rayDirnToHplane1;

  return numer / denom;
}

double pointPostDoublePivot(const int component, const double rayDirnToHplane1,
    const double init_val_out1, const double final_val_out1,
    const double rayDirnToSplit1, const double rayDirnToSplit2,
    const double rayDirnToHplane2, const double init_val_out2,
    const double final_val_out2) {
  double numer;
  if (component == 1) {
    numer = init_val_out1 * rayDirnToSplit2
        - init_val_out2 * rayDirnToHplane2
        - final_val_out1 * rayDirnToSplit2
        + final_val_out2 * rayDirnToHplane2;
  } else {
    numer = -init_val_out1 * rayDirnToSplit1
        + init_val_out2 * rayDirnToHplane1
        + final_val_out1 * rayDirnToSplit1
        - final_val_out2 * rayDirnToHplane1;
  }
  const double denom = rayDirnToSplit1 * rayDirnToHplane2
      - rayDirnToSplit2 * rayDirnToHplane1;

  return numer / denom;
}

double pointPostPivot(const int component, const int nb_var_in1,
    const int struct_var_out1, const int nb_var_in2,
    const int struct_var_out2, const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo, const bool computeVertexFlag) {
  // If we are only pivoting in one variable, and not computing a vertex,
  // then we need to compute the intersection point of this ray with
  // the boundary of the split (which will be the struct_var_out1)
  if (nb_var_in2 < 0) {
    if (component == 2) {
      return 0.0; // No second nb component
    }

    if (!computeVertexFlag) {
      const int split_row = solnInfo.rowOfVar[struct_var_out1];
      const double split_val = solnInfo.a0[split_row];
      const double rayDirnToSplit =
          solnInfo.raysOfC1[nb_var_in1][split_row];

      double bound = 0.0;
      if (isZero(rayDirnToSplit, param.getRAYEPS())) {
        return solver->getInfinity();
      } else if (greaterThanVal(rayDirnToSplit, 0.0, param.getRAYEPS())) {
        bound = std::ceil(split_val);
      } else {
        bound = std::floor(split_val);
      }

      return (bound - split_val) / rayDirnToSplit;
    }
  }

  const int hplane_row = solnInfo.rowOfVar[struct_var_out1];

  // If the ray direction is positive, we intersect the hyperplane ub,
  // if it is negative, we intersect the hyperplane lb,
  // and otherwise, we do not intersect it at all
  double init_val_out1 = +0.0;
  double final_val_out1 = +0.0;

  const double rayDirnToHplane1 =
      (hplane_row >= 0) ? solnInfo.raysOfC1[nb_var_in1][hplane_row] : 1.0;

  // First we check if a non-basic var is being pivoted out
  // (in which case ray will intersect the
  // "other" bound, since we are assuming all non-basic vars are at lb = 0)
  if (hplane_row < 0) {
    const double UB = getVarUB(solver, struct_var_out1);
    const double LB = getVarLB(solver, struct_var_out1);
    if (!isInfinity(UB, solver->getInfinity())
        && !isNegInfinity(LB, solver->getInfinity())) {
      final_val_out1 = (UB - LB);

      if (component == 1) // Same if nb_var_in2 < 0 or not
        return final_val_out1;
    } else {
      return solver->getInfinity();
    }
  } else {
    // Otherwise it is a basic variable being pivoted out
    init_val_out1 = solnInfo.a0[hplane_row];

    // If it is a slack variable, the ray better have a negative direction
    // (There is only one bound on slacks, which is a lower bound)
    if (struct_var_out1 >= solver->getNumCols()) {
      if (!lessThanVal(rayDirnToHplane1, 0.0, param.getRAYEPS())) {
        return solver->getInfinity();
      }
      final_val_out1 = +0.0;
    } else { // Otherwise it is a structural variable being pivoted out
      if (isZero(rayDirnToHplane1, param.getRAYEPS())) {
        return solver->getInfinity();
      } else if (greaterThanVal(rayDirnToHplane1, 0.0, param.getRAYEPS())) { // Ray intersects ub
        final_val_out1 = getVarUB(solver, struct_var_out1);
      } else { // Ray intersects lb
        final_val_out1 = getVarLB(solver, struct_var_out1);
      }
    }
  }

  // Check if this is after a single pivot and we want the vertex value
  if (nb_var_in2 < 0 || computeVertexFlag) {
    return vertexPostSinglePivot(component, rayDirnToHplane1, init_val_out1,
        final_val_out1);
  }

  // Get values for the second pivot
  const int split_row = solnInfo.rowOfVar[struct_var_out2]; // Note: this HAS to be a non-negative number

  // If the ray direction is positive, we intersect the split ceiling,
  // if it is negative, we intersect the split floor,
  // and otherwise, we do not intersect it at all
  double init_val_out2 = solnInfo.a0[split_row];

  const double rayDirnToSplit1 = solnInfo.raysOfC1[nb_var_in1][split_row];
  const double rayDirnToSplit2 = solnInfo.raysOfC1[nb_var_in2][split_row];

  // If the first ray intersected its own bound, then the next ray,
  // (which will necessarily be a different ray) will not change the first
  // component at all (handled above), and the second
  // component will be as in the "one variable pivoted out" case
  if (hplane_row < 0) {
    const double split_val = solnInfo.a0[split_row]
        + final_val_out1 * rayDirnToSplit1; // Note: must not go past an integer value

    double bound = 0.0;
    if (isZero(rayDirnToSplit2, param.getRAYEPS())) {
      return solver->getInfinity();
    } else if (greaterThanVal(rayDirnToSplit2, 0.0, param.getRAYEPS())) {
      bound = std::ceil(split_val);
    } else {
      bound = std::floor(split_val);
    }

    return (bound - split_val) / rayDirnToSplit2;
  }

  const double rayDirnToHplane2 = solnInfo.raysOfC1[nb_var_in2][hplane_row]; // Note: we know hplane_row >= 0

  const double checkNewRayDirnToSplit = rayDirnToSplit2
      - rayDirnToHplane2 * rayDirnToSplit1 / rayDirnToHplane1;

  double final_val_out2;
  if (isZero(checkNewRayDirnToSplit, param.getRAYEPS())) {
    return solver->getInfinity();
  } else if (greaterThanVal(checkNewRayDirnToSplit, 0.0, param.getRAYEPS())) {
    final_val_out2 = std::ceil(init_val_out2);
  } else {
    final_val_out2 = std::floor(init_val_out2);
  }

  return pointPostDoublePivot(component, rayDirnToHplane1, init_val_out1,
      final_val_out1, rayDirnToSplit1, rayDirnToSplit2, rayDirnToHplane2,
      init_val_out2, final_val_out2);
}

/*
const char* toLower(const char* str) {
  char* tmp = new char[strlen(str) + 1];
  const char* iter = str;
  int i = 0;
  while (*iter != '\0'){
//    if (*iter != '_'){
      tmp[i++] = tolower(*iter);
//    }
    iter++;
  }  
  tmp[i] = '\0';
  return tmp;
}

const char* toUpper(const char* str) {
  char* tmp = new char[strlen(str) + 1];
  const char* iter = str;
  int i = 0;
  while (*iter != '\0'){
//    if (*iter != '_'){
      tmp[i++] = toupper(*iter);
//    }
    iter++;
  }  
  tmp[i] = '\0';
  return tmp;
}
*/

//const char* toLowerNoUnderscore(const char* str) {
//  char* tmp = new char[strlen(str) + 1];
//  const char* iter = str;
//  int i = 0;
//  while (*iter != '\0'){
//    if (*iter != '_'){
//      tmp[i++] = tolower(*iter);
//    }
//    iter++;
//  }
//  tmp[i] = '\0';
//  return tmp;
//}
//
//const char* toUpperNoUnderscore(const char* str) {
//  char* tmp = new char[strlen(str) + 1];
//  const char* iter = str;
//  int i = 0;
//  while (*iter != '\0'){
//    if (*iter != '_'){
//      tmp[i++] = toupper(*iter);
//    }
//    iter++;
//  }
//  tmp[i] = '\0';
//  return tmp;
//}

bool remove_underscore(char c) {
  return (c == '_');
}

std::string toLowerString(const std::string& str) {
//const char* toLowerString(const std::string str) {
  std::string tmp = str; 
  for (int i = 0; i < (int) tmp.length(); i++) {
    tmp[i] = tolower(tmp[i]);
  }
//  std::transform(tmp.begin(), tmp.end(), tmp.begin(), ::tolower);
  return tmp;
}

std::string toUpperString(const std::string& str) {
  std::string tmp = str; 
  for (int i = 0; i < (int) tmp.length(); i++) {
    tmp[i] = toupper(tmp[i]);
  }
  return tmp;
}

std::string toLowerStringNoUnderscore(const std::string& str) {
//  std::string tmp = str;
//  for (int i = 0; i < (int) tmp.length(); i++) {
//    if (tmp[i] != '_') {
//      tmp[i] = tolower(tmp[i]);
//    }
//  }
  std::string tmp = str;
  std::transform(tmp.begin(), tmp.end(), tmp.begin(),
      ::tolower);
  tmp.erase(std::remove_if(tmp.begin(), tmp.end(), remove_underscore),
      tmp.end());
  return tmp;
}

std::string toUpperStringNoUnderscore(const std::string& str) {
  std::string tmp = str;
  std::transform(tmp.begin(), tmp.end(), tmp.begin(),
      ::toupper);
  tmp.erase(std::remove_if(tmp.begin(), tmp.end(), remove_underscore),
      tmp.end());
  return tmp;
}

std::vector<std::string> lowerCaseStringVector(const std::vector<std::string>& strVec) {
  std::vector<std::string> tmp = strVec;
  for (int i = 0; i < (int) strVec.size(); i++) {
    std::transform(tmp[i].begin(), tmp[i].end(), tmp[i].begin(), ::tolower);
  }
  return tmp;
}

//std::vector<std::string> upperCaseStringVector(const std::vector<std::string>& strVec) {
//  std::vector<std::string> tmp = strVec;
//  for (int i = 0; i < (int) strVec.size(); i++) {
//    std::transform(tmp[i].begin(), tmp[i].end(), tmp[i].begin(), ::toupper);
//  }
//  return tmp;
//}

void printSparseVector(const std::vector<int>& vec) {
  printSparseVector(vec.size(), vec.data());
} /* printSparseVector (int vec) */

void printSparseVector(const std::vector<double>& vec) {
  printSparseVector(vec.size(), vec.data());
} /* printSparseVector (double vec) */

void printSparseVector(const int numElem, const int* vec) {
  for (int i = 0; i < numElem; i++) {
    if (!isZero(vec[i])) {
      printf("(%d, %d)", i, vec[i]);
      if (i < numElem - 1) {
        printf(", ");
      }
    }
  }
  printf("\n");
} /* printSparseVector (int*) */

void printSparseVector(const int numElem, const double* vec) {
  for (int i = 0; i < numElem; i++) {
    if (!isZero(vec[i])) {
      printf("(%d, %1.3f)", i, vec[i]);
      if (i < numElem - 1) {
        printf(", ");
      }
    }
  }
  printf("\n");
} /* printSparseVector (double*) */

void printSparseVector(const int numElem, const int* ind, const double* vec, const double eps) {
  for (int i = 0; i < numElem; i++) {
    if (!isZero(vec[i], eps)) {
      printf("(%d, %1.3f)", ind[i], vec[i]);
      if (i < numElem - 1) {
        printf(", ");
      }
    }
  }
  printf("\n");
} /* printSparseVector (index+values) */

/** We assume it is comma separated */
double getObjValueFromFile(const char* filename, const std::string& this_inst_name) {
  if (!filename) {
    return std::numeric_limits<double>::lowest();
  }
  
  std::ifstream infile(filename);
  if (infile.is_open()) {
    std::string line;
    while (std::getline(infile, line)) {
      std::istringstream iss(line);
      if (line.empty()) {
        continue;
      }
      std::string inst_name;
      if (!(std::getline(iss, inst_name, ','))) {
        warning_msg(warnstring,
            "Could not read instance name. String is %s.\n",
            line.c_str());
        continue;
      }
      if (inst_name == this_inst_name) {
        try {
          std::string token;
          if (!(std::getline(iss, token, ','))) {
            throw;
          }
          return std::stod(token);
        } catch (std::exception& e) {
          warning_msg(warnstring,
              "Could not read optimal value. String is %s.\n",
              line.c_str());
          continue;
        }
      }
    }
    infile.close();
  } else {
    // If we were not able to open the file, throw an error
    error_msg(errorstring, "Not able to open obj file.\n");
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }

  return std::numeric_limits<double>::lowest();
} /* getObjValueFromFile */

// Last edit: 03/27/12
//
// Name:     common_definitions.cpp
// Author:   Giacomo Nannicini
//           Singapore University of Technology and Design, Singapore
//           email: nannicini@sutd.edu.sg
// Date:     04/09/11
//-----------------------------------------------------------------------------
// Copyright (C) 2011, Giacomo Nannicini.  All Rights Reserved.

double dotProduct(const CoinPackedVector& vec1, const double* vec2) {
  const int size1 = vec1.getNumElements();
  const int* ind1 = vec1.getIndices();
  const double* el1 = vec1.getElements();
  return dotProduct(size1, ind1, el1, vec2);
}

double dotProduct(const CoinPackedVector& vec1, const CoinPackedVector& vec2) {
  const int size1 = vec1.getNumElements();
  const int* ind1 = vec1.getIndices();
  const double* el1 = vec1.getElements();
  const int size2 = vec2.getNumElements();
  const int* ind2 = vec2.getIndices();
  const double* el2 = vec2.getElements();
  return dotProduct(size1, ind1, el1, size2, ind2, el2);
}

/**********************************************************/
double dotProduct(const double* a, const double* b, int dimension) {
  // Accumulate the dot product here
  double accumulator = 0.0;
  // Compensation term for low-order bits lost
  double compensation = 0.0;
  // Temporary number for storing current term
  double currterm = 0.0;
  // Temporary number for storing new accumulator
  double nextacc = 0.0;
  for (int i = 0; i < dimension; ++i) {
    // Compute current term of the dot product adding compensation
    currterm = (a[i] * b[i]) - compensation;
    // This is the value of the sum
    nextacc = accumulator + currterm;
    // Recover what we just lost adding currterm to accumulator
    compensation = (nextacc - accumulator) - currterm;
    // Now save new value of the accumulator
    accumulator = nextacc;
  }
  return accumulator;
} /* dotProduct (dense x dense) */

/**********************************************************/
double dotProduct(int sizea, const int* indexa, const double* a,
    const double* b) {
  // Accumulate the dot product here
  double accumulator = 0.0;
  // Compensation term for low-order bits lost
  double compensation = 0.0;
  // Temporary number for storing current term
  double currterm = 0.0;
  // Temporary number for storing new accumulator
  double nextacc = 0.0;
  for (int i = 0; i < sizea; ++i) {
    // Compute current term of the dot product adding compensation
    currterm = (a[i] * b[indexa[i]]) - compensation;
    // This is the value of the sum
    nextacc = accumulator + currterm;
    // Recover what we just lost adding currterm to accumulator
    compensation = (nextacc - accumulator) - currterm;
    // Now save new value of the accumulator
    accumulator = nextacc;
  }
  return accumulator;
} /* dotProduct (sparse x dense) */

/**********************************************************/
double dotProduct(int sizea, const int* indexa, const double* a, int sizeb,
    const int* indexb, const double* b) {
  // Accumulate the dot product here
  double accumulator = 0.0;
  // Compensation term for low-order bits lost
  double compensation = 0.0;
  // Temporary number for storing current term
  double currterm = 0.0;
  // Temporary number for storing new accumulator
  double nextacc = 0.0;
  // Current position in vectors a and b
  int posa = 0, posb = 0;
  while (posa < sizea && posb < sizeb) {
    // If it is the same component, compute dot product
    if (indexa[posa] == indexb[posb]) {
      // Compute current term of the dot product adding compensation
      currterm = (a[posa] * b[posb]) - compensation;
      // This is the value of the sum
      nextacc = accumulator + currterm;
      // Recover what we just lost adding currterm to accumulator
      compensation = (nextacc - accumulator) - currterm;
      // Now save new value of the accumulator
      accumulator = nextacc;
      // Increment both position indices
      ++posa;
      ++posb;
    } else if (indexa[posa] < indexb[posb]) {
      // Increment only smaller position index
      ++posa;
    } else if (indexa[posa] > indexb[posb]) {
      // Increment only smaller position index
      ++posb;
    }
  }
  return accumulator;
} /* dotProduct (sparse x sparse) */

/**********************************************************/
/*
void standardizeString(char* string) {
  // Return a "clean" string: everything to upper case and no underscores
  char* tmp = new char[strlen(string) + 1];
  char* iter = string;
  int i = 0;
  while (*iter != '\0') {
    if (*iter != '_') {
      tmp[i++] = toupper(*iter);
    }
    iter++;
  }
  tmp[i] = '\0';
  strcpy(string, tmp);
  delete[] tmp;
}
*/

/**********************************************************/
/*
double randomNumber(unsigned int command, unsigned int* seed) {
  static unsigned int last = 123456;
  if (command == 1 && seed) {
    // Assign the value of the seed
    last = *seed;
  } else if (command == 2 && seed) {
    // Write the value of the current seed
    *seed = last;
  } else if (command == 0) {
    // Generate random number based on previous number
    last = 1664525 * last + 1013904223;
    return ((static_cast<double>(last))
        / std::numeric_limits<unsigned int>::max());
  }
  return (0.0);
}
*/
