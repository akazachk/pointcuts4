//============================================================================
// Name        : Output.hpp
// Author      : akazachk
// Version     : 0.2013.mm.dd
// Copyright   : Your copyright notice
// Description : 
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#pragma once

#include <string>

#include "AdvCut.hpp"
#include "GlobalConstants.hpp"

#include "CoinPackedMatrix.hpp"
#include "typedefs.hpp"
#include "SolutionInfo.hpp"
#include "Stats.hpp"

#include "BBHelper.hpp" // for BBInfo

/***********************************************************************/
/**
 * @brief Adds the header lines to the file
 *
 * ,ORIG PROB,,,,,,,,,,,,,,,,,,,SUB PROB,,,CUT INFO,,,,AFTER LIFT,,,,,,TIME INFO,,,,,
 * INSTANCE,NUM ROWS,NUM COLS,NUM EQ ROWS,NUM INEQ ROWS,NUM BOUND ROWS,NUM ASSIGN ROWS,
 * INTEGERS,BINARIES,CONTINUOUS,NUM BASIC,NUM NON-BASIC,NUM ORIG BASIC,NUM ORIG NON-BASIC,
 * FRAC CORE,A NONZERO,A-DENSITY,INTEGER OBJ,LP OBJ,INIT_SOLVE,
 * SUBROWS,SUBCOLS,GEN_SUB,
 * SUB GIC OBJ,SUB GIC EUCL DIST,SUB SIC OBJ,SUB SIC EUCL DIST,
 * LIFTED GIC OBJ,LIFTED GIC EUCL DIST,LIFTED SIC OBJ,LIFTED SIC EUCL DIST,
 * ORIG SPACE SIC OBJ,ORIG SPACE SIC OBJ,
 * TOTAL,INIT_SOLVE,GEN_SUB,GET_CUT,LIFT,ADD_CUT
 *
 */
void writeHeaderToLog(FILE *myfile);
void writeCleaningHeaderToLog(FILE *myfile);

/***********************************************************************/
/**
 * @brief Writes solution (both primal and dual) to file.
 * @param solver          ::  The solver whose solution we are writing.
 * @param soln_name       ::  The name we are outputting to.
 * @param out_stream      ::  Out stream.
 * @param width_varname   ::  Width of variable name field.
 * @param width_val       ::  Width of variable value field.
 * @param width_stat      ::  Width of cstat/rstat field.
 * @param width_rc        ::  Width of reduced cost field.
 * @param width_activity  ::  Width of row activity field.
 * @param width_rhs       ::  Width of rhs field.
 * @param after_dec       ::  Number of digits after the decimal.
 * @param EXT             ::  Extension added to end of file output.
 */
void writeSoln(const PointCutsSolverInterface* const solver, std::string soln_name,
    int width_varname = 16, int width_val = 16, int width_stat = 16,
    int width_rc = 16, int width_activity = 16, int width_rhs = 16,
    int after_dec = 6, const std::string EXT = "-optSoln.alex");

/***********************************************************************/
/**
 * Prints our the basis corresponding the COIN instance solver
 */
void printSimplexTableauWithNames(FILE* fptr, PointCutsSolverInterface* const solver);

/**
 * Prints our the basis corresponding the COIN instance solver; only non-basic non-artificial columns printed.
 */
void printNBSimplexTableauWithNames(FILE* fptr, PointCutsSolverInterface* const solver);

/**
 * Print points stored in point store
 */
void printPointStore(FILE* fptr, const int fracVar, const int dim,
    const std::vector<Point> &point);

/**
 * Print rays stored in ray store
 */
void printRayStore(FILE* fptr, const int fracVar, const int dim,
    const std::vector<Ray> &ray);

/***********************************************************************/
/**
 * @brief Writes deleted variable names and their values to file.
 * @param deleteCols          ::  Indices of deleted columns (vars).
 * @param deleteColsNames     ::  Names of deleted columns.
 * @param deleteColsVals      ::  Values in orig lp soln of deleted vars.
 * @param deleteColsObjCoeffs ::  Obj coeffs of deleted columns.
 * @param deleteColsLB        ::  LBs of deleted columns.
 * @param deleteColsUB        ::  UBs of deleted columns.
 * @param objSum              ::  Total sum deleted from objective
 * @param fname               ::  File name to write to.
 * @param out_stream          ::  Out stream.
 */
void writeDeletedVars(std::vector<int> &deleteCols,
    std::vector<std::string> &deleteColsNames,
    std::vector<double> &deleteColsVals,
    std::vector<double> &deleteColsObjCoeffs,
    std::vector<double> &deleteColsLB, std::vector<double> &deleteColsUB,
    double objSum, std::string fname);

/***********************************************************************/
/**
 * Writes deleted rows (name of row, coefficients, variables, rhs) to file
 */
/*void writeDeletedRows();*/

/***********************************************************************/
/**
 * @brief Writes problem name to start a new instance info line
 */
void writeProbNameToLog(std::string &prob_name, FILE *myfile);

/***********************************************************************/
/**
 * Writes which settings we are using to instance info
 */
void writeParamInfoToLog(FILE* myfile);

/***********************************************************************/
/**
 * @brief Appends information about solution to prob of given type
 * @param type  :: May be either lpRelax, subprob, or afterCut
 */
void writeSolnInfoToLog(const SolutionInfo& probSolnInfo,
    FILE *myfile);

/***********************************************************************/
/**
 * @brief Appends information about solution to prob of given type
 * @param type  :: May be either lpRelax, subprob, or afterCut
 */
//void writeSolnInfoToLog(const SolutionInfo& probSolnInfo,
//    FILE *myfile);

/***********************************************************************/
/**
 * SUBROWS,SUBCOLS,
 * 0 FEAS SPLIT SIDES,1 FEAS SPLIT SIDE,2 FEAS SPLIT SIDES,
 * NUM FLOOR INFEAS,NUM CEIL INFEAS
 * GEN_SUB
 */
void writeSubSolnInfoToLog(SolutionInfo* solnInfoSub, FILE* myfile);

/***********************************************************************/
void writeCompLSIC_OSICToLog(std::string &prob_name, int numcuts,
    int numfeascuts, double avgNumPivots, int numliftedcuts,
    std::vector<double> &EuclLInit, std::vector<double> &EuclLBest,
    std::vector<double> &EuclOInit, std::vector<double> &EuclOBest,
    std::vector<double> &ObjLInit, std::vector<double> &ObjLBest,
    std::vector<double> &ObjOInit, std::vector<double> &ObjOBest,
    double OSICBoundAll, double OSICBasisCondNum, double LSICBoundInit,
    double LSICBasisCondNumInit, double LSICBoundAll,
    double LSICBasisCondNumAll, double LSICAndOSICBoundInit,
    double LSICAndOSICBasisCondNumInit, double LSICAndOSICBoundAll,
    double LSICAndOSICBasisCondNumAll, FILE* myfile);

/***********************************************************************/
/**
 * Writes which settings we are using to instance info,
 * as well as the number of points and rays generated
 */
void writeGICPointInfoToLog(const int hplaneHeur, const int nextHplaneFinalFlag,
    const int numHplanesPerRay, const int numRaysOfC1,
    const double avgNumParallelRays, const double avg_num_SIC_final_points,
    const int numRaysToBeCut, const int numCutGenSets, const int numRounds,
    std::vector<int> &numPoints, std::vector<int> &numFinalPoints,
    std::vector<int> &numRays, const int totalNumPoints,
    const int totalNumFinalPoints, const int totalNumRays,
    const std::vector<double>& avgSICDepth,
    const std::vector<double>& minSICDepth,
    const std::vector<double>& maxSICDepth,
    const std::vector<double>& avgObjDepth,
    const std::vector<double>& minObjDepth,
    const std::vector<double>& maxObjDepth, FILE* myfile);

/***********************************************************************/
//void writeCompSGIC_SSICToII(AdvCuts &GIC, AdvCuts &SIC,
//    SolutionInfo &solnInfoSub, int numGICs, int num1GICIsSIC, int numRounds,
//    std::vector<int> &GICsOnSplit, std::vector<int> &cutIndexOfSingleGICOnSplit,
//    std::vector<int> &EuclGIC, std::vector<int> &EuclSIC, double minEuclGICSub,
//    double maxEuclGICSub, double avgEuclGICSub, double minEuclSICSub,
//    double maxEuclSICSub, double avgEuclSICSub, double GICBound,
//    double GICBasisCondNumber, double SICBound, double SICBasisCondNumber,
//    double SICAndGICBound, double SICAndGICBasisCondNumber, FILE* myfile);
/**
 fprintf(myfile, "%s%c", "NUM FEAS SSICS", SEP); // 1
 fprintf(myfile, "%s%c", "NUM SGICS", SEP); // 2
 fprintf(myfile, "%s%c", "AVG NUM LSICS PIVOTS AVAILABLE", SEP); // 3
 fprintf(myfile, "%s%c", "AVG NUM LGICS PIVOTS AVAILABLE", SEP); // 4
 fprintf(myfile, "%s%c", "NUM LSICS", SEP); // 5
 fprintf(myfile, "%s%c", "NUM LGICS", SEP); // 6
 fprintf(myfile, "%s%c", "NUM OSICS", SEP); // 7
 fprintf(myfile, "%s%c", "% INIT LGIC EUCL > INIT LSIC EUCL", SEP); // 8
 fprintf(myfile, "%s%c", "% INIT LGIC EUCL = INIT LSIC EUCL", SEP); // 9
 fprintf(myfile, "%s%c", "% INIT LGIC EUCL < INIT LSIC EUCL", SEP); // 10
 fprintf(myfile, "%s%c", "% BEST LGIC EUCL > BEST LSIC EUCL", SEP); // 11
 fprintf(myfile, "%s%c", "% BEST LGIC EUCL = BEST LSIC EUCL", SEP); // 12
 fprintf(myfile, "%s%c", "% BEST LGIC EUCL < BEST LSIC EUCL", SEP); // 13
 fprintf(myfile, "%s%c", "% INIT LGIC EUCL > OSIC EUCL", SEP); // 14
 fprintf(myfile, "%s%c", "% INIT LGIC EUCL = OSIC EUCL", SEP); // 15
 fprintf(myfile, "%s%c", "% INIT LGIC EUCL < OSIC EUCL", SEP); // 16
 fprintf(myfile, "%s%c", "% BEST LGIC EUCL > OSIC EUCL", SEP); // 17
 fprintf(myfile, "%s%c", "% BEST LGIC EUCL = OSIC EUCL", SEP); // 18
 fprintf(myfile, "%s%c", "% BEST LGIC EUCL < OSIC EUCL", SEP); // 19
 */
void writeCompLGIC_LSIC_OSICToLog(std::string &prob_name, int numFeasSSICs,
    int numSGICs, int numLGICs, int numLSICs, double avgNumPivots_SICs,
    double avgNumPivots_GICs, int numOSICs,
    std::vector<double> &Eucl_LGIC_v_LSIC_Init_LG,
    std::vector<double> &Eucl_LGIC_v_LSIC_Best_LG,
    std::vector<double> &Eucl_LGIC_v_LSIC_Init_LS,
    std::vector<double> &Eucl_LGIC_v_LSIC_Best_LS,
    std::vector<double> &Eucl_LGIC_v_OSIC_Init_LG,
    std::vector<double> &Eucl_LGIC_v_OSIC_Best_LG,
    std::vector<double> &Eucl_LGIC_v_OSIC_Init_O,
    std::vector<double> &Eucl_LGIC_v_OSIC_Best_O, FILE* myfile);

/**
 fprintf(myfile, "%s%c", "INIT LGIC BOUND", SEP); // 1
 fprintf(myfile, "%s%c", "ALL LGIC BOUND", SEP); // 2
 fprintf(myfile, "%s%c", "INIT LSIC BOUND", SEP); // 3
 fprintf(myfile, "%s%c", "ALL LSIC BOUND", SEP); // 4
 fprintf(myfile, "%s%c", "OSIC BOUND", SEP); // 5
 fprintf(myfile, "%s%c", "INIT LGIC + INIT LSIC BOUND", SEP); // 6
 fprintf(myfile, "%s%c", "ALL LGIC + ALL LSIC BOUND", SEP); // 7
 fprintf(myfile, "%s%c", "INIT LGIC + OSIC BOUND", SEP); // 8
 fprintf(myfile, "%s%c", "ALL LGIC + OSIC BOUND", SEP); // 9
 fprintf(myfile, "%s%c", "LGIC_All+OSIC > OSIC", SEP); // 10
 fprintf(myfile, "%s%c", "LGIC_All > LGIC_INIT", SEP); // 11
 fprintf(myfile, "%s%c", "LGIC_INIT+LSIC_INIT > LSIC_INIT", SEP); // 12
 fprintf(myfile, "%s%c", "LGIC_ALL+LSIC_ALL > LSIC_ALL", SEP); // 13
 */
//void writeRootBoundToII(double LGICBoundInit, double LGICBoundAll,
//    double LSICBoundInit, double LSICBoundAll, double OSICBound,
//    double LGICAndLSICBoundInit, double LGICAndLSICBoundAll,
//    double LGICAndOSICBoundInit, double LGICAndOSICBoundAll, FILE *myfile);

/*
 fprintf(myfile, "%s%c", "INIT LGIC BASIS COND", SEP); // 1
 fprintf(myfile, "%s%c", "ALL LGIC BASIS COND", SEP); // 2
 fprintf(myfile, "%s%c", "INIT LSIC BASIS COND", SEP); // 3
 fprintf(myfile, "%s%c", "ALL LSIC BASIS COND", SEP); // 4
 fprintf(myfile, "%s%c", "OSIC BASIS COND", SEP); // 5
 fprintf(myfile, "%s%c", "INIT LGIC + INIT LSIC BASIS COND", SEP); // 6
 fprintf(myfile, "%s%c", "ALL LGIC + ALL LSIC BASIS COND", SEP); // 7
 fprintf(myfile, "%s%c", "INIT LGIC + OSIC BASIS COND", SEP); // 8
 fprintf(myfile, "%s%c", "ALL LGIC + OSIC BASIS COND", SEP); // 9
 */
void writeCondNumToLog(double LGICBasisCondNumInit, double LGICBasisCondNumAll,
    double LSICBasisCondNumInit, double LSICBasisCondNumAll,
    double OSICBasisCondNum, double LGICAndLSICBasisCondNumInit,
    double LGICAndLSICBasisCondNumAll, double LGICAndOSICBasisCondNumInit,
    double LGICAndOSICBasisCondNumAll, FILE* myfile);

/***********************************************************************/
/**
 * @brief Writes information about cuts from a single round to instance info file
 *        Info: SUB GIC OBJ, SUB GIC EUCL DIST, SUB SIC OBJ, SUB SIC EUCL DIST
 *
 * @param GICObj      ::  LP obj after adding all GICs
 * @param GICEuclDist ::  Max Euclidean dist from \bar x to any GIC
 * @param SICObj      ::  LP obj after adding all SICs
 * @param SICEuclDist ::  Max Euclidean dist from \bar x to any SIC
 * @param prob_name   ::  Problem name
 * @param myfile      ::  File to write to
 */
void writeAllCutsAddedInfoToLog(double GICObj, double GICEuclDist,
    double SICObj, double SICEuclDist, std::string &prob_name,
    FILE *myfile);

/***********************************************************************/
/**
 * @brief Write information about cuts in original space to instance info file
 * // LIFTED GIC OBJ, LIFTED GIC EUCL DIST, LIFTED SIC OBJ, LIFTED SIC EUCL DIST, ORIG SPACE SIC OBJ, ORIG SPACE SIC EUCL DIST
 */
void writeLiftedInfoToLog(double liftedGICObj,
    double liftedGICEuclDist, double liftedSICObj, double liftedSICEuclDist,
    double origSICObj, double origSICEuclDist, //std::string &prob_name,
    FILE *myfile);

/***********************************************************************/
/**
 * @brief Appends information about GICs (cut heuristics that worked and fails)
 */
void writeCutInfoToLog(const int totalNumCuts, const int minSupportSICs,
    const int maxSupportSICs, const double avgSupportSICs,
    const double stdDevSupportSICs, const int minSupportVPCs,
    const int maxSupportVPCs, const double avgSupportVPCs,
    const double stdDevSupportVPCs, const double minOrthoWithObj,
    const double maxOrthoWithObj, const double avgOrthoWithObj,
    const int totalNumObj, const std::vector<int>& active_cut_heur_sicsolver,
    const std::vector<int>& active_cut_heur_gmisolver, FILE *myfile);

/***********************************************************************/
/**
 * @brief Appends information about time to solve various parts of the prob
 */
void writeTimeInfoToLog(Stats &timeStats, std::vector<std::string> &times,
    //const char* end_time_string,
    FILE *myfile);

/***********************************************************************/
/**
 * @brief Writes entry to II
 */
void writeEntryToLog(const int x, FILE *myfile);
void writeEntryToLog(const long x, FILE *myfile);
void writeEntryToLog(const double x, FILE *myfile);
void writeEntryToLog(const std::string x, FILE *myfile, const char SEPCHAR = SEP);

/***********************************************************************/
/**
 * @brief Writes sets of cuts that are stored in structural space.
 */
void writeCutsInStructSpace(std::string headerText, AdvCuts &structCut,
    SolutionInfo &solnInfo, const PointCutsSolverInterface* const solver, char* filename,
    FILE* inst_info_out);

/***********************************************************************/
/**
 * @brief Writes sets of cuts that are stored in structural space.
 */
void writeCutsInStructSpace(std::string headerText,
    std::vector<AdvCuts> &structCut, SolutionInfo &solnInfo,
    const PointCutsSolverInterface* const solver, char* filename, FILE* inst_info_out);

/***********************************************************************/
/**
 * @brief Writes set of cuts that are stored in NB space.
 */
void writeCutsInNBSpace(std::string headerText, AdvCuts &NBcutinfo,
    SolutionInfo &solnInfo, const PointCutsSolverInterface* const solver, char* filename,
    FILE* inst_info_out);

/***********************************************************************/
/**
 * @brief Writes set of cuts that are stored in NB space.
 */
void writeCutsInNBSpace(std::string headerText, std::vector<AdvCuts> &NBcutinfo,
    SolutionInfo &solnInfo, const PointCutsSolverInterface* const solver, char* filename,
    FILE* inst_info_out);

/***********************************************************************/
/**
 * @brief Writes how far along each NB ray we go for each cut.
 */
void writeRayDistInfo(std::string headerText, const PointCutsSolverInterface* const solver,
    SolutionInfo &solnInfo, AdvCuts &structCut,
    std::vector<std::vector<double> > &distAlongRay, char* filename,
    FILE* inst_info_out);

/***********************************************************************/
/**
 * @brief Writes how far along each NB ray we go for each cut.
 */
void writeRayDistInfo(std::string headerText, const PointCutsSolverInterface* const solver,
    SolutionInfo &solnInfo, std::vector<AdvCuts> &structCut,
    std::vector<std::vector<std::vector<double> > > &distAlongRay,
    char* filename, FILE* inst_info_out);

/***********************************************************************/
void printSplitInfo(const OsiSolverInterface* const solverMain,
    SolutionInfo& solnInfoMain);

/***********************************************************************/
/**
 * @brief Writes euclidean distance by which initial vertex is cut,
 * and the objective function after adding the single cut
 */
void writeCutSummary(std::string headerText,
    //const PointCutsSolverInterface* const solver,
    //SolutionInfo &solnInfo,
    AdvCuts &structCut, char* filename,
    FILE* inst_info_out);

/***********************************************************************/
/**
 * @brief Writes euclidean distance by which initial vertex is cut,
 * and the objective function after adding the single cut
 */
void writeCutSummary(std::string headerText,
    //const PointCutsSolverInterface* const solver,
    //SolutionInfo &solnInfo,
    std::vector<AdvCuts> &structCut, char* filename,
    FILE* inst_info_out);

/***********************************************************************/
/**
 * @brief Writes euclidean distance by which initial vertex is cut,
 * for the sub GICs vs sub SICs
 */
void writeSubGICSummary(std::string headerText, const PointCutsSolverInterface* const solver,
    SolutionInfo &solnInfo, AdvCuts &structCut, AdvCuts &SIC,
    char* filename, FILE* inst_info_out);

/***********************************************************************/
/**
 * @brief Writes euclidean distance by which initial vertex is cut,
 * for the sub GICs vs sub SICs
 */
void writeSubGICSummary(std::string headerText, const PointCutsSolverInterface* const solver,
    SolutionInfo &solnInfo, std::vector<AdvCuts> &structCut, AdvCuts &SIC,
    char* filename, FILE* inst_info_out);

/***********************************************************************/
/**
 * @brief Writes euclidean distance by which initial vertex is cut,
 * for the sub GICs vs sub SICs
 */
void writeLiftedGICSummary(std::string headerText,
    const PointCutsSolverInterface* const solver, SolutionInfo &solnInfo,
    std::vector<AdvCuts> &GICLifted, std::vector<AdvCuts> &SICLifted,
    AdvCuts &SICOrig, char* filename, FILE* inst_info_out);

/***********************************************************************/
/**
 * @brief Writes info about root node bound after all cuts added
 */
void writeRootNodeBoundAllCuts(std::string &prob_name, int hplaneHeur,
    int cutSelHeur, int numRaysToBeCut, int numFeasSplits, int numGICs,
    double SICBound, double SICBasisCondNumber, double GICBound,
    double GICBasisCondNumber, double SICAndGICBound,
    double SICAndGICBasisCondNumber, SolutionInfo &solnInfo,
    const PointCutsSolverInterface* const solver,
    std::vector<std::vector<int> > &hplanesIndxToBeActivated,
    char* filename, FILE* inst_info_out);

void writeRootBoundToLog(const double LPBound, const double strongBranchingLB,
    const double strongBranchingUB, const int numSICs, const double sic_opt,
    const int numGMICuts, const double gmi_opt, const int numLandPCuts,
    const double landp_opt, const int numTilted, const double tilted_opt,
    const int numVPCs, const double fpc_opt, const int numPHA,
    const double pha_opt, const double sic_tilted_opt,
    const double gmi_tilted_opt, const double sic_fpc_opt,
    const double gmi_fpc_opt, const double sic_pha_opt,
    const double gmi_pha_opt, const double tilted_fpc_pha_opt,
    const double sic_tilted_fpc_pha_opt, const double gmi_tilted_fpc_pha_opt,
    const int num_active_sics_sicsolver, const int num_active_tilted_sicsolver,
    const int num_active_fpc_sicsolver, const int num_active_pha_sicsolver,
    const int num_active_gmi_gmisolver, const int num_active_tilted_gmisolver,
    const int num_active_fpc_gmisolver, const int num_active_pha_gmisolver,
    FILE *myfile);

void writeBBInforToLog(const BBInfo& min_bb_info_mycuts, const BBInfo& min_bb_info_allcuts,
    const BBInfo& avg_bb_info_mycuts,
    const BBInfo& avg_bb_info_allcuts,
    const std::vector<BBInfo>& vec_bb_info_mycuts,
    const std::vector<BBInfo>& vec_bb_info_allcuts,
//    const double bb_opt_mycuts, const double bb_opt_allcuts,
//    const long bb_iters_mycuts, const long bb_iters_allcuts,
//    const long bb_nodes_mycuts, const long bb_nodes_allcuts,
//    const long bb_num_root_passes_mycuts, const long bb_num_root_passes_allcuts,
//    const double bb_first_cut_pass_mycuts, const double bb_first_cut_pass_allcuts,
//    const double bb_last_cut_pass_mycuts, const double bb_last_cut_pass_allcuts,
//    const double bb_time_mycuts, const double bb_time_allcuts,
//    const std::string str_bb_opt_mycuts, const std::string str_bb_opt_allcuts,
//    const std::string str_bb_iters_mycuts,
//    const std::string str_bb_iters_allcuts,
//    const std::string str_bb_nodes_mycuts,
//    const std::string str_bb_nodes_allcuts,
//    const std::string str_bb_num_root_passes_mycuts,
//    const std::string str_bb_num_root_passes_allcuts,
//    const std::string str_bb_first_cut_pass_mycuts,
//    const std::string str_bb_first_cut_pass_allcuts,
//    const std::string str_bb_last_cut_pass_mycuts,
//    const std::string str_bb_last_cut_pass_allcuts,
//    const std::string str_bb_time_mycuts,
//    const std::string str_bb_time_allcuts,
    FILE *myfile);

/***********************************************************************/
/**
 * @brief Write bound by round and other round info
 */
void writeRoundInfoToLog(const double sic_fpc_opt, const double sic_pha_opt,
    const int total_num_sics, const int num_sic_rounds,
    const double final_sic_bound,
    const std::vector<double>& boundByRoundVPCPlus,
    const std::vector<double>& boundByRoundPHAPlus, const double LPBasisCond,
    const std::vector<double>& basisCondByRoundSIC,
    const std::vector<double>& basisCondByRoundVPC,
    const std::vector<double>& basisCondByRoundVPCPlus,
    const std::vector<double>& basisCondByRoundPHA,
    const std::vector<double>& basisCondByRoundPHAPlus,
    const double minSICOrthogonalityInit, const double maxSICOrthogonalityInit,
    const double avgSICOrthogonalityInit, const double minVPCOrthogonalityInit,
    const double maxVPCOrthogonalityInit, const double avgVPCOrthogonalityInit,
    const double minPHAOrthogonalityInit, const double maxPHAOrthogonalityInit,
    const double avgPHAOrthogonalityInit, const double minSICOrthogonalityFinal,
    const double maxSICOrthogonalityFinal,
    const double avgSICOrthogonalityFinal,
    const double minVPCOrthogonalityFinal,
    const double maxVPCOrthogonalityFinal,
    const double avgVPCOrthogonalityFinal,
    const double minPHAOrthogonalityFinal,
    const double maxPHAOrthogonalityFinal,
    const double avgPHAOrthogonalityFinal, FILE *myfile);

/***********************************************************************/
/**
 * @brief Writes time info to separate file from instances info.
 */
void writeTimeInfo(Stats &timeStats, std::vector<std::string> &times,
    char* filename, const char* end_time_string);

/***********************************************************************/
/**
 * @brief Writes cut solver errors to separate file from instances info.
 */
void writeCutSolverErrors(const int num_obj_tried, char* filename);
//
//void writeInterPtsAndRaysInJSpace(FILE* fptr, const SolutionInfo &solnInfo,
//    const std::vector<IntersectionInfo>& interPtsAndRays,
//    const std::vector<Hplane>& hplaneStore);

void writeInterPtsAndRays(FILE* fptr, const SolutionInfo &solnInfo,
    const int num_cgs,
    const std::vector<IntersectionInfo>* const interPtsAndRays,
    const std::string* const cgsName, const std::vector<Hplane>* hplaneStore = NULL);

void writeInterPtsAndRaysInJSpace(FILE* fptr, const SolutionInfo &solnInfo,
    const std::vector<IntersectionInfo>& interPtsAndRays,
    const std::vector<Hplane>& hplaneStore,
    const std::vector<int>& num_SIC_points,
    const std::vector<int>& num_SIC_final_points,
    const std::vector<int>& num_SIC_rays);
