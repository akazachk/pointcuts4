#include "optionhelper.hpp"

/**********************************************************
 * Implement arg parsing using getopt and use CglGIC class
 *********************************************************/
// To prevent memory leaks from toLower
const std::vector<std::string> lowerCaseNames = lowerCaseStringVector(optionName);

//const std::string tmp[] = {toLowerString(optionName[optionIndex::UNKNOWN]), toLowerString(optionName[optionIndex::HELP])};
const option::Descriptor usage[] = {
    { optionIndex::UNKNOWN, 0, "", "", Arg::Unknown,
        "USAGE: example [options]\n\n"
            "Options:" },
    { optionIndex::HELP, 0, "h",
        lowerCaseNames[optionIndex::HELP].c_str(),
        Arg::None,
        "  -h, --help \tPrint usage and exit." },
    // REQUIRED OPTIONS
    { optionIndex::UNKNOWN, 0, "", "", Arg::None, "\n  REQUIRED OPTIONS:"},
    { optionIndex::INST_NAME, 0, "i",
        lowerCaseNames[optionIndex::INST_NAME].c_str(),
        Arg::Required,
        "  -i <arg>, --inst_name=<arg>  \tInstance (/dir/name), including extension and directory." },
    { optionIndex::OUT_DIR_NAME, 0, "o",
        lowerCaseNames[optionIndex::OUT_DIR_NAME].c_str(),
        Arg::Required,
        "  -o <arg>, --out_dir_name=<arg> \tDirectory to which output is written." },
    // PARAMETERS AND OUTPUT FILE
    { optionIndex::UNKNOWN, 0, "", "", Arg::None, "\n  OTHER GENERAL OPTIONS:"},
    { optionIndex::LOG_FILE, 0, "",
        lowerCaseNames[optionIndex::LOG_FILE].c_str(),
         Arg::Optional,
         "  --log_file=<arg> \tWhere to compile instance information (usually across instances)." },
    { optionIndex::PARAM_FILE, 0, "p",
        lowerCaseNames[optionIndex::PARAM_FILE].c_str(),
        Arg::Required,
        "  -p <arg>, --param_file=<arg> \tWhere to find the parameter file." },
    { optionIndex::OPT_FILE, 0, "",
        lowerCaseNames[optionIndex::OPT_FILE].c_str(),
        Arg::Optional,
        "  --opt_file=<arg> \tWhere to find the file with optimal values." },
    // WHICH CUTTING PLANE TECHNIQUES TO USE
    { optionIndex::UNKNOWN, 0, "", "", Arg::None, "\n  CUTTING PLANE OPTIONS:"},
    { optionIndex::SICS, 0, "",
        lowerCaseNames[optionIndex::SICS].c_str(),
        Arg::Binary,
        "  --sics=<arg>"
            "\tGenerate standard intersection cuts?" },
    { optionIndex::GMI,
        0, "",
        lowerCaseNames[optionIndex::GMI].c_str(),
        Arg::Binary,
        "  --gmi=<arg>"
            "\tGenerate GMI cuts?" },
    { optionIndex::LANDP,
        0, "",
        lowerCaseNames[optionIndex::LANDP].c_str(),
        Arg::Binary,
        "  --landp=<arg>"
            "\tGenerate lift-and-project cuts?" },
    { optionIndex::TILTED_DEPTH, 0, "",
        lowerCaseNames[optionIndex::TILTED_DEPTH].c_str(),
        Arg::Numeric,
        "  --tilted_depth=<arg>"
            "\tDepth of tilted cuts to generate ("
            "-1: none, "
            "0: objcuts, "
            "1: hyperplanes tight at facet optimum, "
            "2: all hyperplanes possible, "
            "3: option 2 and also lift SICs from subspace, "
            "4: only subspace SICs)." },
    { optionIndex::VPC_DEPTH, 0, "",
        lowerCaseNames[optionIndex::VPC_DEPTH].c_str(),
        Arg::Numeric,
        "  --vpc_depth=<arg>"
            "\tDepth of V-polyhedral cuts to generate (0 = none, negative values indicate max number of nodes that can be used from a B&B tree)." },
    { optionIndex::PHA, 0, "",
        lowerCaseNames[optionIndex::PHA].c_str(),
        Arg::Binary,
        "  --pha=<arg>"
            "\tGenerate GICs by partial hyperplane activation?" },
    // COMMON CUTTING PLANE OPTIONS
    { optionIndex::UNKNOWN, 0, "", "", Arg::None, "\n  COMMON CUTTING PLANE OPTIONS:"},
    { optionIndex::NB_SPACE, 0, "",
        lowerCaseNames[optionIndex::NB_SPACE].c_str(),
        Arg::Binary,
        "  --nb_space=<arg>"
            "\tUse NB space to generate cuts?" },
    { optionIndex::STRENGTHEN, 0, "",
        lowerCaseNames[optionIndex::STRENGTHEN].c_str(),
        Arg::Numeric,
        "  --strengthen=<arg>"
            "\tStrengthen cuts using GMI disjunctions? (0: no, 1: use GMI cuts, 2: also add GMI cuts to disj terms)" },
    { optionIndex::MIX, 0, "",
        lowerCaseNames[optionIndex::MIX].c_str(),
        Arg::Binary,
        "  --mix=<arg>"
            "\tUse mixing inequalities technique?" },
    { optionIndex::PIVOT, 0, "",
        lowerCaseNames[optionIndex::PIVOT].c_str(),
        Arg::Binary,
        "  --pivot=<arg>"
            "\tPivot to get better basis when generating cuts?" },
    { optionIndex::NUM_RAYS_CUT, 0, "",
        lowerCaseNames[optionIndex::NUM_RAYS_CUT].c_str(),
        Arg::Numeric,
        "  --num_rays_cut=<arg>"
            "\tNumber of rays to be cut. A value of "
            "0: cut as many as possible; -1: cut sqrt(n); -2: cut sqrt(n)/2." },
    { optionIndex::MAX_FRAC_VAR, 0, "",
        lowerCaseNames[optionIndex::MAX_FRAC_VAR].c_str(),
        Arg::Numeric,
        "  --max-frac-core=<arg>"
            "\tNumber of fractional variables to consider. Overridden by num_cgs when using only splits anyway. "
            "A value of "
            "0: cut as many as possible; "
            "-1: cut sqrt(n); "
            "-2: cut sqrt(n)/2. " },//; -1: cut sqrt(n); -2: cut sqrt(n)/2." },
    { optionIndex::CGS, 0, "",
        lowerCaseNames[optionIndex::CGS].c_str(),
        Arg::Numeric,
        "  --cgs=<arg>"
            "\tWhich cut-generating sets to use. "
            "0: splits; "
            "1: triangles of type-1 from pairs of intvars, vertex at (0,0); "
            "2: triangles, vertex at (0,1); "
            "3: triangles, vertex at (1,0); "
            "4: triangles, vertex at (1,1); "
            "5: all of the four possible triangles; "
            "6: all triangles such that vertex is in the smaller triangle with diagonal shifted down one; "
            "7: cross as the disjunctive set." },
    { optionIndex::NUM_CGS, 0, "",
        lowerCaseNames[optionIndex::NUM_CGS].c_str(),
        Arg::Numeric,
        "  --num_cgs=<arg>"
            "\tAn explicit bound on the number of cut-generating sets (splits for now). "
            "If 0, try as many splits as possible. If < 0, choose n / abs(param) best ones." },
    { optionIndex::NUM_CUTS_PER_CGS, 0, "",
        lowerCaseNames[optionIndex::NUM_CUTS_PER_CGS].c_str(),
        Arg::Numeric,
        "  --num_cuts_per_cgs=<arg>"
            "\tNumber of cuts per cut-generating set. Ideally, this * number splits >= number cuts generated. "
                "If limit_cuts_per_cgs=1 and overload_max_cuts given, then ignore this value; "
                "simply take number of splits / overload_max_cuts." },
    { optionIndex::OVERLOAD_MAX_CUTS, 0, "",
        lowerCaseNames[optionIndex::OVERLOAD_MAX_CUTS].c_str(),
        Arg::Numeric,
        "  --overload_max_cuts=<arg>"
            "\tWhen not 0, replaces number cuts per cgs parameter as limit on number of cuts. "
            "A negative value means generate at most abs(val) * numSplits cuts." },
    { optionIndex::LIMIT_CUTS_PER_CGS, 0, "",
        lowerCaseNames[optionIndex::LIMIT_CUTS_PER_CGS].c_str(),
        Arg::Binary,
        "  --limit_cuts_per_cgs=<arg>"
            "\tLimit cuts per split? See options with num_cuts_per_cgs and overload_max_cuts." },
    { optionIndex::SUBSPACE, 0, "",
        lowerCaseNames[optionIndex::SUBSPACE].c_str(),
        Arg::Binary,
        "  --subspace=<arg>"
            "\tGenerate subspace?" },
    { optionIndex::ROUNDS, 0, "",
        lowerCaseNames[optionIndex::ROUNDS].c_str(),
        Arg::Numeric,
        "  --rounds=<arg>"
            "\tNumber of cutting plane rounds? "
            "(0: turn off all cuts, "
            "> 0: do this many rounds)" },
    { optionIndex::BB_RUNS, 0, "",
        lowerCaseNames[optionIndex::BB_RUNS].c_str(),
        Arg::Numeric,
        "  --bb_runs=<arg>"
            "\tTest cuts with branch-and-bound? "
            "(0: do not perform branch-and-bound, "
            "> 0: do this many runs with default strategy employed)" },
    { optionIndex::BB_STRATEGY, 0, "",
        lowerCaseNames[optionIndex::BB_STRATEGY].c_str(),
        Arg::Numeric,
        "  --bb_strategy=<arg>"
            "\tIf using B&B, which strategy? "
            "(< 0: default, > 0: cbc, cplex, gurobi, user_cuts, "
            "all_cuts_off, all_cuts_on, gmics_off, gmics_on, "
            "presolve_off, presolve_on, heuristics_off, heuristics_on, "
            "use_best_bound, strong_branching_on)" },
            /*"(0 or 1: Cbc default strategy, 2: Cbc default + strong branching (does not work), "
            "-1: Cbc everything off, -2: Cbc everything off + strong branching, "
            "10 or 11: Cplex default strategy, 12: Cplex default + strong branching, "
            "-11: Cplex everything off, -12: Cplex everything off + strong branching)" },*/
    { optionIndex::BB_CBC, 0, "",
        lowerCaseNames[optionIndex::BB_CBC].c_str(),
        Arg::Binary,
        "  --bb_cbc=<arg>"
            "\tUse Cbc?"},
    { optionIndex::BB_CPLEX, 0, "",
        lowerCaseNames[optionIndex::BB_CPLEX].c_str(),
        Arg::Binary,
        "  --bb_cplex=<arg>"
            "\tUse CPLEX?"},
    { optionIndex::BB_GUROBI, 0, "",
        lowerCaseNames[optionIndex::BB_GUROBI].c_str(),
        Arg::Binary,
        "  --bb_gurobi=<arg>"
            "\tUse Gurobi?"},
    { optionIndex::BB_USER_CUTS, 0, "",
        lowerCaseNames[optionIndex::BB_USER_CUTS].c_str(),
        Arg::Binary,
        "  --bb_user_cuts=<arg>"
            "\tUse user cuts?"},
    { optionIndex::BB_ALL_CUTS, 0, "",
        lowerCaseNames[optionIndex::BB_ALL_CUTS].c_str(),
        Arg::Binary,
        "  --bb_all_cuts=<arg>"
            "\tUse all cuts?"},
    { optionIndex::BB_GMICS, 0, "",
        lowerCaseNames[optionIndex::BB_GMICS].c_str(),
        Arg::Binary,
        "  --bb_gmics=<arg>"
            "\tUse GMICs?"},
    { optionIndex::BB_PRESOLVE, 0, "",
        lowerCaseNames[optionIndex::BB_PRESOLVE].c_str(),
        Arg::Binary,
        "  --bb_presolve=<arg>"
            "\tUse presolve?"},
    { optionIndex::BB_HEURISTICS, 0, "",
        lowerCaseNames[optionIndex::BB_HEURISTICS].c_str(),
        Arg::Binary,
        "  --bb_heuristics=<arg>"
            "\tUse heuristics?"},
    { optionIndex::BB_USE_BEST_BOUND, 0, "",
        lowerCaseNames[optionIndex::BB_USE_BEST_BOUND].c_str(),
        Arg::Binary,
        "  --bb_use_best_bound=<arg>"
            "\tUse best bound?"},
    { optionIndex::BB_SB, 0, "",
        lowerCaseNames[optionIndex::BB_SB].c_str(),
        Arg::Binary,
        "  --bb_sb=<arg>"
            "\tUse strong branching?"},
    //{ optionIndex::BB_RANDOM_SEED, 0, "",
    //    lowerCaseNames[optionIndex::BB_RANDOM_SEED].c_str(),
    //    Arg::Numeric,
    //    "  --bb_random_seed=<arg>"
    //        "\tSpecify random seed."},
    { optionIndex::EPS, 0, "",
        lowerCaseNames[optionIndex::EPS].c_str(),
        Arg::Numeric,
        "  --eps=<arg>"
            "\tValue of EPS." },
    { optionIndex::RAYEPS, 0, "",
        lowerCaseNames[optionIndex::RAYEPS].c_str(),
        Arg::Numeric,
        "  --rayeps=<arg>"
            "\tValue of RAYEPS (zero threshold for ray components)." },
    { optionIndex::MIN_ORTHOGONALITY, 0, "",
        lowerCaseNames[optionIndex::MIN_ORTHOGONALITY].c_str(),
        Arg::Numeric,
        "  --min_orthogonality=<arg>"
            "\tValue of MIN_ORTHOGONALITY, min orthogonality to accept a cut." },
    { optionIndex::MIN_VIOL_ABS, 0, "",
        lowerCaseNames[optionIndex::MIN_VIOL_ABS].c_str(),
        Arg::Numeric,
        "  --min_viol_abs=<arg>"
            "\tValue of MIN_VIOL_ABS, absolute minimum by which cut should remove the interior vertex." },
    { optionIndex::MIN_VIOL_REL, 0, "",
        lowerCaseNames[optionIndex::MIN_VIOL_REL].c_str(),
        Arg::Numeric,
        "  --min_viol_rel=<arg>"
            "\tValue of MIN_VIOL_REL, relative (Euclidean distance) "
            "minimum by which cut should remove the interior vertex." },
    { optionIndex::TIMELIMIT, 0, "",
        lowerCaseNames[optionIndex::TIMELIMIT].c_str(),
        Arg::Numeric,
        "  --timelimit=<arg>"
            "\tSuggestion of maximum of time that can be spent (but more complicated timing stuff happens)." },
    { optionIndex::PARTIAL_BB_TIMELIMIT, 0, "",
        lowerCaseNames[optionIndex::PARTIAL_BB_TIMELIMIT].c_str(),
        Arg::Numeric,
        "  --partial_bb_timelimit=<arg>"
            "\tTime limit for generating a partial b&b tree." },
    { optionIndex::BB_TIMELIMIT, 0, "",
        lowerCaseNames[optionIndex::BB_TIMELIMIT].c_str(),
        Arg::Numeric,
        "  --bb_timelimit=<arg>"
            "\tTime limit for each b&b run." },
    { optionIndex::CUTSOLVER_TIMELIMIT, 0, "",
        lowerCaseNames[optionIndex::CUTSOLVER_TIMELIMIT].c_str(),
        Arg::Numeric,
        "  --cutsolver_timelimit=<arg>"
            "\tTime limit for total time spent in PRLP solving." },
    { optionIndex::TEMP, 0, "",
        lowerCaseNames[optionIndex::TEMP].c_str(),
        Arg::Numeric,
        "  --temp=<arg>"
            "\tTemporary option that can be set for debugging or testing purposes. "
            "0: off; "
            "1: check whether any cuts are violated by the IP optimal solution found by Gurobi (in main); "
            "2: do rounds of SICs in CutHelper; "
            "3: check orthogonality stats per round in CutHelper; "
            "10: generate tikz string from partial tree (>0, print obj, <0, do not print obj), then apply VPCs and run Cbc to completion and print full tree; "
            "11: generate tikz string from partial tree (>0, print obj, <0, do not print obj), then apply SICs and run Cbc to completion and print full tree; "
            "12: generate tikz string from partial tree (>0, print obj, <0, do not print obj), then apply VPCs+SICs and run Cbc to completion and print full tree; "
            "13: generate tikz string from partial tree (>0, print obj, <0, do not print obj), then run Cbc to completion and print full tree; "
            "14: generate tikz string from partial tree (>0, print obj, <0, do not print obj), then return out of CglVPC; "
            "15: generate tikz string from partial tree (>0, print obj, <0, do not print obj), then exit progam entirely; "
            "100+: VPC solve for disjunctive term by explicitly adding inequalities instead of changing bounds on variables." },
    { optionIndex::CLEANING_MODE, 0, "",
        lowerCaseNames[optionIndex::CLEANING_MODE].c_str(),
        Arg::Numeric,
        "  --cleaning_mode=<arg>"
            "\tShould the instance be cleaned? "
            "(0: no; 1: only CPLEX/Gurobi presolve; 2: check strong branching and tighten bounds)" },
    // VPC OPTIONS
    { optionIndex::UNKNOWN, 0, "", "", Arg::None, "\n  VPC OPTIONS:"},
    { optionIndex::PARTIAL_BB_STRATEGY, 0, "",
        lowerCaseNames[optionIndex::PARTIAL_BB_STRATEGY].c_str(),
        Arg::Numeric,
        "  --partial_bb_strategy=<arg>"
            "\tIf using partial BB method of generating VPCs (i.e., vpc_depth < 0), which strategy to generate the partial tree? "
            "The total gives the choose variable decision (hundreds) and branch decision (tens) and node comparison choice (ones)."
            "hundreds digit: 0: default, 1: default+second criterion, 2: max min change+second (max max change), 3: second-best default, 4: second-best max-min change, -x: -1 * (1+x); "
            "tens digit: 0: default, 1: dynamic, 2: strong, 3: none; "
            "ones digit: 0: default: 1: bfs, 2: depth, 3: estimate, 4: objective." },
    { optionIndex::PARTIAL_BB_NUM_STRONG, 0, "",
        lowerCaseNames[optionIndex::PARTIAL_BB_NUM_STRONG].c_str(),
        Arg::Numeric,
        "  --partial_bb_num_strong=<arg>"
            "\tNumber strong candidates to consider during partial bb tree construction (-2: sqrt(n); -1: full; 0+ same as in Cbc)." },
    { optionIndex::VPC_DEPTH2_HEUR, 0, "",
        lowerCaseNames[optionIndex::VPC_DEPTH2_HEUR].c_str(),
        Arg::Numeric,
        "  --vpc_depth2_heur=<arg>"
            "\tIf generating depth-2 VPCs, how to do it? "
            "(0: depth 1 from pivoting to neighbors along each ray, 1: activate hyperplanes)" },
    { optionIndex::MAX_HPLANES, 0, "",
        lowerCaseNames[optionIndex::MAX_HPLANES].c_str(),
        Arg::Numeric,
        "  --max_hplanes=<arg>"
            "\tMaximum number of hyperplanes to be activated (for vpc depth 2)." },
    { optionIndex::SPLIT_VAR_DELETED, 0, "",
        lowerCaseNames[optionIndex::SPLIT_VAR_DELETED].c_str(),
        Arg::Binary,
        "  --split_var_deleted=<arg>"
            "\tShould the split variable be deleted when VPC solver on each facet is constructed?" },
    // PHA OPTIONS
    { optionIndex::UNKNOWN, 0, "", "", Arg::None, "\n  PHA OPTIONS:"},
    { optionIndex::PHA_ACT_OPTION, 0, "",
        lowerCaseNames[optionIndex::PHA_ACT_OPTION].c_str(),
        Arg::Required,
        "  --pha_act_option=<arg>"
            "\tHyperplane heuristic (-1: skip, 0: first, 1: best_avg_depth, 2: best_final_pts)." },
    { optionIndex::NEXT_HPLANE_FINAL, 0, "",
        lowerCaseNames[optionIndex::NEXT_HPLANE_FINAL].c_str(),
        Arg::Binary,
        "  --next_hplane_final=<arg>"
            "\tChoose additional hyperplanes cutting rays by most number of final points created?" },
    { optionIndex::NUM_EXTRA_ACT_ROUNDS, 0, "",
        lowerCaseNames[optionIndex::NUM_EXTRA_ACT_ROUNDS].c_str(),
        Arg::Numeric,
        "  --num_extra_act_rounds=<arg>"
            "\tNumber hyperplanes to activate per ray "
            "(1-2 if next hplane final flag is false, 1+ otherwise; 0 if no activations, negative if no tilting round)." },
    // OBJECTIVE FUNCTION OPTIONS
    { optionIndex::UNKNOWN, 0, "", "", Arg::None, "\n  OBJECTIVE FUNCTION OPTIONS:"},
    { optionIndex::PRLP_STRATEGY, 0, "",
        lowerCaseNames[optionIndex::PRLP_STRATEGY].c_str(),
        Arg::Numeric,
        "  --prlp_strategy=<arg>"
            "\tStrategy for objectives for PRLP: "
            "(0: default, 1: try few objectives)" },
    { optionIndex::FLIP_BETA, 0, "",
            lowerCaseNames[optionIndex::FLIP_BETA].c_str(),
            Arg::Numeric,
            "  --flip_beta=<arg>"
                "\tShould we flip beta if the PRLP is infeasible? "
                "(0: no, 1: yes, 2: try even if feasible, <0: same but with -1 beta to start)" },
    { optionIndex::USE_SPLIT_SHARE, 0, "",
        lowerCaseNames[optionIndex::USE_SPLIT_SHARE].c_str(),
        Arg::Numeric,
        "  --use_split_share=<arg>"
            "\tShould the split share be used to generate cuts (may be slow): "
            "0 no, 1 yes first, 2 yes last." },
    { optionIndex::NUM_CUTS_ITER_BILINEAR, 0, "",
        lowerCaseNames[optionIndex::NUM_CUTS_ITER_BILINEAR].c_str(),
        Arg::Numeric,
        "  --num_cuts_iter_bilinear=<arg>"
            "\tNumber of cuts to try for the iterative bilinear cut generation." },
    { optionIndex::USE_CUT_VERT_HEUR, 0, "",
        lowerCaseNames[optionIndex::USE_CUT_VERT_HEUR].c_str(),
        Arg::Binary,
        "  --use_cut_vert_heur=<arg>"
            "\tUse cut vertices cut heuristic?" },
    { optionIndex::MODE_OBJ_PER_POINT, 0, "",
        lowerCaseNames[optionIndex::MODE_OBJ_PER_POINT].c_str(),
        Arg::Numeric,
        "  --mode_obj_per_point=<arg>"
            "\tMode for generating many cuts on branching lb point (and others): "
            "0: sum up rays, 1: one ray at a time;"
            "1x: use rays as well;"
            "0/2xy: sort by increasing angle with obj / slack; 1/3xy: same but decreasing." },
    { optionIndex::NUM_OBJ_PER_POINT, 0, "",
        lowerCaseNames[optionIndex::NUM_OBJ_PER_POINT].c_str(),
        Arg::Numeric,
        "  --num_obj_per_point=<arg>"
            "\tHow many objectives to try to get per branching lb point (and others)." },
    { optionIndex::USE_ALL_ONES_HEUR, 0, "",
        lowerCaseNames[optionIndex::USE_ALL_ONES_HEUR].c_str(),
        Arg::Binary,
        "  --use_all_ones_heur=<arg>"
            "\tUse the all ones objective?" },
    { optionIndex::USE_UNIT_VECTORS_HEUR, 0, "",
        lowerCaseNames[optionIndex::USE_UNIT_VECTORS_HEUR].c_str(),
        Arg::Numeric,
        "  --use_unit_vectors_heur=<arg>"
            "\tHow many of the axis directions to try (cut heuristic)." },
    { optionIndex::USE_TIGHT_POINTS_HEUR, 0, "",
        lowerCaseNames[optionIndex::USE_TIGHT_POINTS_HEUR].c_str(),
        Arg::Numeric,
        "  --use_tight_points_heur=<arg>"
            "\tHow many points to use in the tight on points cut heuristic." },
    { optionIndex::USE_TIGHT_RAYS_HEUR, 0, "",
        lowerCaseNames[optionIndex::USE_TIGHT_RAYS_HEUR].c_str(),
        Arg::Numeric,
        "  --use_tight_rays_heur=<arg>"
            "\tHow many rays to use in the tight on rays cut heuristic." },
    { optionIndex::CUT_PRESOLVE, 0, "",
        lowerCaseNames[optionIndex::CUT_PRESOLVE].c_str(),
        Arg::Numeric,
        "  --cut_presolve=<arg>"
            "\tUse presolve in cut generation (cutSolver)? "
            "(0: off, 1: in initial, 2: in initial and resolve)" },
    { optionIndex::MAX_POINTS_PER_SPLIT_SPLIT_SHARE, 0, "",
        lowerCaseNames[optionIndex::MAX_POINTS_PER_SPLIT_SPLIT_SHARE].c_str(),
        Arg::Numeric,
        "  --max_points_split_share=<arg>"
            "\tMax number of points per split in split share heuristic." },
    { optionIndex::UNKNOWN, 0, "", "", Arg::None, "\nExamples:\n"
            "  example --unknown -- --this_is_no_option\n"
            "  example -unk --plus -ppp file1 file2\n" }, { 0, 0, 0, 0, 0, 0 }
}; /* usage */

void processArgs(int argc, char** argv, CglGICParam& param,
    std::string& tmppath, std::string& tmpname, std::string& tmpoutdir,
    std::string& opt_file, std::string& instdir, int& CLEANING_MODE_OPTION) {
  // Process parameters
  argc -= (argc > 0);
  argv += (argc > 0); // skip program name argv[0] if present
  option::Stats stats(usage, argc, argv);
  option::Option* options = new option::Option[stats.options_max];
  option::Option* buffer = new option::Option[stats.buffer_max];
  option::Parser parse(usage, argc, argv, options, buffer);

  if (parse.error()) {
    fprintf(stderr, "*** ERROR: Error parsing parameters. Exiting.\n");
    int columns = getenv("COLUMNS") ? atoi(getenv("COLUMNS")) : 82;
    option::printUsage(fwrite, stdout, usage, columns);
    delete[] options;
    delete[] buffer;
    exit(1);
  }

  if (options[optionIndex::HELP] || argc < 0) {
    int columns = getenv("COLUMNS") ? atoi(getenv("COLUMNS")) : 82;
    option::printUsage(fwrite, stdout, usage, columns);
    delete[] options;
    delete[] buffer;
    exit(0);
  }

  if (options[optionIndex::PARAM_FILE]) {
    //option::Option& opt = buffer[options[optionIndex::PARAM_FILE]];
    std::string param_file = options[optionIndex::PARAM_FILE].arg;
    printf("## Setting parameters from file %s. ##\n", param_file.c_str());
    param.setParams(param_file.c_str());
  }

  if (argc < 2) {
    fprintf(stderr,
        "*** ERROR: Not enough arguments provided. Need at least 2 (inst name and out dir). Exiting.\n");
    delete[] options;
    delete[] buffer;
    exit(1);
  }

  for (int i = 0; i < parse.optionsCount(); ++i) {
    option::Option& opt = buffer[i];
    switch (opt.index()) {
      case optionIndex::HELP: // not possible, because handled further above and exits the program
      case optionIndex::PARAM_FILE: { // Handled above
        break;
      }
      case optionIndex::OPT_FILE: { // Decide where to get optimal solution value info
        opt_file = options[optionIndex::OPT_FILE].arg;
        break;
      }
      case optionIndex::INST_NAME: {
        tmppath = opt.arg;
        // Extract the overall directory as well as the instance directory and filename stub
        size_t found_slash = tmppath.find_last_of("/\\");
        if ((found_slash != std::string::npos)
            && (found_slash < tmppath.length())) {
          instdir = tmppath.substr(0, found_slash + 1);
          tmpname = tmppath.substr(found_slash + 1);

//          // We can now look for the second slash, to see where to save the instances information
//          size_t found_slash2 = instdir.find_last_of("/\\",
//              instdir.length() - 2);
//          if ((found_slash2 != std::string::npos)
//              && (found_slash2 < instdir.length())
//              && (found_slash2 != instdir.length() - 1)) {
//            tmpdir = instdir.substr(0, found_slash2 + 1);
//          } else {
//            // Otherwise, only one folder was specified, so the current directory is where to save instance info
//            tmpdir = "./";
//          }
        } else { // Otherwise, no directory was specified, so the parent directory is where to save instance info
          tmpname = tmppath;
          instdir = "./";
//          tmpdir = "../";
        }
        break;
      }
      case optionIndex::OUT_DIR_NAME: {
        tmpoutdir = opt.arg;
        break;
      }
      case optionIndex::LOG_FILE: {
        GlobalVariables::LOG_FILE_NAME = opt.arg;
        break;
      }
      case optionIndex::SICS: {
        const int paramIndex = ParamIndices::SICS_PARAM_IND;
        if (opt.arg) {
          parseInt(opt.arg, param.paramVal[paramIndex]);
        } else {
          param.paramVal[paramIndex] = ParamDefaults[paramIndex];
        }
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::GMI: {
        const int paramIndex = ParamIndices::GMI_PARAM_IND;
        if (opt.arg) {
          parseInt(opt.arg, param.paramVal[paramIndex]);
        } else {
          param.paramVal[paramIndex] = ParamDefaults[paramIndex];
        }
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::LANDP: {
        const int paramIndex = ParamIndices::LANDP_PARAM_IND;
        if (opt.arg) {
          parseInt(opt.arg, param.paramVal[paramIndex]);
        } else {
          param.paramVal[paramIndex] = ParamDefaults[paramIndex];
        }
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::TILTED_DEPTH: {
        const int paramIndex = ParamIndices::TILTED_DEPTH_PARAM_IND;
        if (opt.arg) {
          parseInt(opt.arg, param.paramVal[paramIndex]);
        } else {
          param.paramVal[paramIndex] = ParamDefaults[paramIndex];
        }
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::VPC_DEPTH: {
        const int paramIndex = ParamIndices::VPC_DEPTH_PARAM_IND;
        if (opt.arg) {
          parseInt(opt.arg, param.paramVal[paramIndex]);
        } else {
          param.paramVal[paramIndex] = ParamDefaults[paramIndex];
        }
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::PHA: {
        const int paramIndex = ParamIndices::PHA_PARAM_IND;
        if (opt.arg) {
          parseInt(opt.arg, param.paramVal[paramIndex]);
        } else {
          param.paramVal[paramIndex] = ParamDefaults[paramIndex];
        }
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::NB_SPACE: {
        const int paramIndex = ParamIndices::NB_SPACE_PARAM_IND;
        if (opt.arg) {
          parseInt(opt.arg, param.paramVal[paramIndex]);
        } else {
          param.paramVal[paramIndex] = ParamDefaults[paramIndex];
        }
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::STRENGTHEN: {
        const int paramIndex = ParamIndices::STRENGTHEN_PARAM_IND;
        if (opt.arg) {
          parseInt(opt.arg, param.paramVal[paramIndex]);
        } else {
          param.paramVal[paramIndex] = ParamDefaults[paramIndex];
        }
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::MIX: {
        const int paramIndex = ParamIndices::MIX_PARAM_IND;
        if (opt.arg) {
          parseInt(opt.arg, param.paramVal[paramIndex]);
        } else {
          param.paramVal[paramIndex] = ParamDefaults[paramIndex];
        }
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::PIVOT: {
        const int paramIndex = ParamIndices::PIVOT_PARAM_IND;
        if (opt.arg) {
          parseInt(opt.arg, param.paramVal[paramIndex]);
        } else {
          param.paramVal[paramIndex] = ParamDefaults[paramIndex];
        }
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::PARTIAL_BB_STRATEGY: {
        const int paramIndex = ParamIndices::PARTIAL_BB_STRATEGY_PARAM_IND;
        if (opt.arg) {
          parseInt(opt.arg, param.paramVal[paramIndex]);
        } else {
          param.paramVal[paramIndex] = ParamDefaults[paramIndex];
        }
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::PARTIAL_BB_NUM_STRONG: {
        const int paramIndex = ParamIndices::PARTIAL_BB_NUM_STRONG_PARAM_IND;
        if (opt.arg) {
          parseInt(opt.arg, param.paramVal[paramIndex]);
        } else {
          param.paramVal[paramIndex] = ParamDefaults[paramIndex];
        }
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::VPC_DEPTH2_HEUR: {
        const int paramIndex = ParamIndices::VPC_DEPTH2_HEUR_PARAM_IND;
        if (opt.arg) {
          parseInt(opt.arg, param.paramVal[paramIndex]);
        } else {
          param.paramVal[paramIndex] = ParamDefaults[paramIndex];
        }
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::MAX_HPLANES: {
        const int paramIndex = ParamIndices::MAX_HPLANES_PARAM_IND;
        if (opt.arg) {
          parseInt(opt.arg, param.paramVal[paramIndex]);
        } else {
          param.paramVal[paramIndex] = ParamDefaults[paramIndex];
        }
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::SPLIT_VAR_DELETED: {
        const int paramIndex = ParamIndices::SPLIT_VAR_DELETED_PARAM_IND;
        if (opt.arg) {
          parseInt(opt.arg, param.paramVal[paramIndex]);
        } else {
          param.paramVal[paramIndex] = ParamDefaults[paramIndex];
        }
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::PHA_ACT_OPTION: {
        const int paramIndex = ParamIndices::PHA_ACT_OPTION_PARAM_IND;
        parseInt(opt.arg, param.paramVal[paramIndex]);
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::NEXT_HPLANE_FINAL: {
        const int paramIndex = ParamIndices::NEXT_HPLANE_FINAL_PARAM_IND;
        parseInt(opt.arg, param.paramVal[paramIndex]);
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::NUM_EXTRA_ACT_ROUNDS: {
        const int paramIndex = ParamIndices::NUM_EXTRA_ACT_ROUNDS_PARAM_IND;
        parseInt(opt.arg, param.paramVal[paramIndex]);
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::NUM_RAYS_CUT: {
        const int paramIndex = ParamIndices::NUM_RAYS_CUT_PARAM_IND;
        parseInt(opt.arg, param.paramVal[paramIndex]);
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::MAX_FRAC_VAR: {
        const int paramIndex = ParamIndices::MAX_FRAC_VAR_PARAM_IND;
        parseInt(opt.arg, param.paramVal[paramIndex]);
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::CGS: {
        const int paramIndex = ParamIndices::CGS_PARAM_IND;
        parseInt(opt.arg, param.paramVal[paramIndex]);
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::NUM_CGS: {
        const int paramIndex = ParamIndices::NUM_CGS_PARAM_IND;
        parseInt(opt.arg, param.paramVal[paramIndex]);
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::NUM_CUTS_PER_CGS: {
        const int paramIndex = ParamIndices::NUM_CUTS_PER_CGS_PARAM_IND;
        parseInt(opt.arg, param.paramVal[paramIndex]);
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::OVERLOAD_MAX_CUTS: {
        const int paramIndex = ParamIndices::OVERLOAD_MAX_CUTS_PARAM_IND;
        parseInt(opt.arg, param.paramVal[paramIndex]);
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::LIMIT_CUTS_PER_CGS: {
        const int paramIndex = ParamIndices::LIMIT_CUTS_PER_CGS_PARAM_IND;
        parseInt(opt.arg, param.paramVal[paramIndex]);
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::PRLP_STRATEGY: {
        const int paramIndex = ParamIndices::PRLP_STRATEGY_PARAM_IND;
        parseInt(opt.arg, param.paramVal[paramIndex]);
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::FLIP_BETA: {
        const int paramIndex = ParamIndices::FLIP_BETA_PARAM_IND;
        parseInt(opt.arg, param.paramVal[paramIndex]);
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::USE_SPLIT_SHARE: {
        const int paramIndex = ParamIndices::USE_SPLIT_SHARE_PARAM_IND;
        parseInt(opt.arg, param.paramVal[paramIndex]);
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::NUM_CUTS_ITER_BILINEAR: {
        const int paramIndex = ParamIndices::NUM_CUTS_ITER_BILINEAR_PARAM_IND;
        parseInt(opt.arg, param.paramVal[paramIndex]);
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::USE_CUT_VERT_HEUR: {
        const int paramIndex = ParamIndices::USE_CUT_VERT_HEUR_PARAM_IND;
        parseInt(opt.arg, param.paramVal[paramIndex]);
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::MODE_OBJ_PER_POINT: {
        const int paramIndex = ParamIndices::MODE_OBJ_PER_POINT_PARAM_IND;
        parseInt(opt.arg, param.paramVal[paramIndex]);
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::NUM_OBJ_PER_POINT: {
        const int paramIndex = ParamIndices::NUM_OBJ_PER_POINT_PARAM_IND;
        parseInt(opt.arg, param.paramVal[paramIndex]);
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::USE_ALL_ONES_HEUR: {
        const int paramIndex = ParamIndices::USE_ALL_ONES_HEUR_PARAM_IND;
        parseInt(opt.arg, param.paramVal[paramIndex]);
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::USE_UNIT_VECTORS_HEUR: {
        const int paramIndex = ParamIndices::USE_UNIT_VECTORS_HEUR_PARAM_IND;
        parseInt(opt.arg, param.paramVal[paramIndex]);
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::USE_TIGHT_POINTS_HEUR: {
        const int paramIndex = ParamIndices::USE_TIGHT_POINTS_HEUR_PARAM_IND;
        parseInt(opt.arg, param.paramVal[paramIndex]);
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::USE_TIGHT_RAYS_HEUR: {
        const int paramIndex = ParamIndices::USE_TIGHT_RAYS_HEUR_PARAM_IND;
        parseInt(opt.arg, param.paramVal[paramIndex]);
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::CUT_PRESOLVE: {
        const int paramIndex = ParamIndices::CUT_PRESOLVE_PARAM_IND;
        parseInt(opt.arg, param.paramVal[paramIndex]);
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::MAX_POINTS_PER_SPLIT_SPLIT_SHARE: {
        const int paramIndex =
            ParamIndices::MAX_POINTS_PER_SPLIT_SPLIT_SHARE_PARAM_IND;
        parseInt(opt.arg, param.paramVal[paramIndex]);
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::SUBSPACE: {
        const int paramIndex = ParamIndices::SUBSPACE_PARAM_IND;
        parseInt(opt.arg, param.paramVal[paramIndex]);
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        // Currently we cannot handle subspace = 1
        if (param.paramVal[ParamIndices::SUBSPACE_PARAM_IND] == 1) {
          error_msg(errstr, "Currently, subspace is not supported.\n");
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::ROUNDS: {
        const int paramIndex = ParamIndices::ROUNDS_PARAM_IND;
        parseInt(opt.arg, param.paramVal[paramIndex]);
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::BB_RUNS: {
        const int paramIndex = ParamIndices::BB_RUNS_PARAM_IND;
        parseInt(opt.arg, param.paramVal[paramIndex]);
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::BB_STRATEGY: {
        const int paramIndex = ParamIndices::BB_STRATEGY_PARAM_IND;
        int tmp;
        parseInt(opt.arg, tmp);
        param.paramVal[paramIndex] |= tmp;
        if (!param.checkParam(paramIndex)) {
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        break;
      }
      case optionIndex::BB_CBC: {
        const int paramIndex = ParamIndices::BB_STRATEGY_PARAM_IND;
        int tmp;
        parseInt(opt.arg, tmp);
        if (tmp != 0 && tmp != 1) {
          error_msg(errorstring, "Value should be 0 or 1.\n");
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        if (tmp == 0) {
          param.paramVal[paramIndex] &= ~(BB_Strategy_Options::cbc);
        } else {
          param.paramVal[paramIndex] |= BB_Strategy_Options::cbc;
        }
        break;
      }
      case optionIndex::BB_CPLEX: {
        const int paramIndex = ParamIndices::BB_STRATEGY_PARAM_IND;
        int tmp;
        parseInt(opt.arg, tmp);
        if (tmp != 0 && tmp != 1) {
          error_msg(errorstring, "Value should be 0 or 1.\n");
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        if (tmp == 0) {
          param.paramVal[paramIndex] &= ~(BB_Strategy_Options::cplex);
        } else {
          param.paramVal[paramIndex] |= BB_Strategy_Options::cplex;
        }
        break;
      }
      case optionIndex::BB_GUROBI: {
        const int paramIndex = ParamIndices::BB_STRATEGY_PARAM_IND;
        int tmp;
        parseInt(opt.arg, tmp);
        if (tmp != 0 && tmp != 1) {
          error_msg(errorstring, "Value should be 0 or 1.\n");
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        if (tmp == 0) {
          param.paramVal[paramIndex] &= ~(BB_Strategy_Options::gurobi);
        } else {
          param.paramVal[paramIndex] |= BB_Strategy_Options::gurobi;
        }
        break;
      }
      case optionIndex::BB_USER_CUTS: {
        const int paramIndex = ParamIndices::BB_STRATEGY_PARAM_IND;
        int tmp;
        parseInt(opt.arg, tmp);
        if (tmp != 0 && tmp != 1) {
          error_msg(errorstring, "Value should be 0 or 1.\n");
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        if (tmp == 0) {
          param.paramVal[paramIndex] &= ~(BB_Strategy_Options::user_cuts);
        } else {
          param.paramVal[paramIndex] |= BB_Strategy_Options::user_cuts;
        }
        break;
      }
      case optionIndex::BB_ALL_CUTS: {
        const int paramIndex = ParamIndices::BB_STRATEGY_PARAM_IND;
        int tmp;
        parseInt(opt.arg, tmp);
        if (tmp != 0 && tmp != 1) {
          error_msg(errorstring, "Value should be 0 or 1.\n");
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        if (tmp == 0) {
          param.paramVal[paramIndex] |= BB_Strategy_Options::all_cuts_off;
          param.paramVal[paramIndex] &= ~(BB_Strategy_Options::all_cuts_on);
        } else {
          param.paramVal[paramIndex] |= BB_Strategy_Options::all_cuts_on;
          param.paramVal[paramIndex] &= ~(BB_Strategy_Options::all_cuts_off);
        }
        break;
      }
      case optionIndex::BB_GMICS: {
        const int paramIndex = ParamIndices::BB_STRATEGY_PARAM_IND;
        int tmp;
        parseInt(opt.arg, tmp);
        if (tmp != 0 && tmp != 1) {
          error_msg(errorstring, "Value should be 0 or 1.\n");
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        if (tmp == 0) {
          param.paramVal[paramIndex] |= BB_Strategy_Options::gmics_off;
          param.paramVal[paramIndex] &= ~(BB_Strategy_Options::gmics_on);
        } else {
          param.paramVal[paramIndex] |= BB_Strategy_Options::gmics_on;
          param.paramVal[paramIndex] &= ~(BB_Strategy_Options::gmics_off);
        }
        break;
      }
      case optionIndex::BB_PRESOLVE: {
        const int paramIndex = ParamIndices::BB_STRATEGY_PARAM_IND;
        int tmp;
        parseInt(opt.arg, tmp);
        if (tmp != 0 && tmp != 1) {
          error_msg(errorstring, "Value should be 0 or 1.\n");
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        if (tmp == 0) {
          param.paramVal[paramIndex] |= BB_Strategy_Options::presolve_off;
          param.paramVal[paramIndex] &= ~(BB_Strategy_Options::presolve_on);
        } else {
          param.paramVal[paramIndex] |= BB_Strategy_Options::presolve_on;
          param.paramVal[paramIndex] &= ~(BB_Strategy_Options::presolve_off);
        }
        break;
      }
      case optionIndex::BB_HEURISTICS: {
        const int paramIndex = ParamIndices::BB_STRATEGY_PARAM_IND;
        int tmp;
        parseInt(opt.arg, tmp);
        if (tmp != 0 && tmp != 1) {
          error_msg(errorstring, "Value should be 0 or 1.\n");
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        if (tmp == 0) {
          param.paramVal[paramIndex] |= BB_Strategy_Options::heuristics_off;
          param.paramVal[paramIndex] &= ~(BB_Strategy_Options::heuristics_on);
        } else {
          param.paramVal[paramIndex] |= BB_Strategy_Options::heuristics_on;
          param.paramVal[paramIndex] &= ~(BB_Strategy_Options::heuristics_off);
        }
        break;
      }
      case optionIndex::BB_USE_BEST_BOUND: {
        const int paramIndex = ParamIndices::BB_STRATEGY_PARAM_IND;
        int tmp;
        parseInt(opt.arg, tmp);
        if (tmp != 0 && tmp != 1) {
          error_msg(errorstring, "Value should be 0 or 1.\n");
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        if (tmp == 0) {
          param.paramVal[paramIndex] &= ~(BB_Strategy_Options::use_best_bound);
        } else {
          param.paramVal[paramIndex] |= BB_Strategy_Options::use_best_bound;
        }
        break;
      }
      case optionIndex::BB_SB: {
        const int paramIndex = ParamIndices::BB_STRATEGY_PARAM_IND;
        int tmp;
        parseInt(opt.arg, tmp);
        if (tmp != 0 && tmp != 1) {
          error_msg(errorstring, "Value should be 0 or 1.\n");
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        if (tmp == 0) {
          param.paramVal[paramIndex] &= ~(BB_Strategy_Options::strong_branching_on);
        } else {
          param.paramVal[paramIndex] |= BB_Strategy_Options::strong_branching_on;
        }
        break;
      }
      /*case optionIndex::BB_RANDOM_SEED: {
        int tmp;
        parseInt(opt.arg, tmp);
        if (tmp < 0) {
          error_msg(errorstring, "Value should >= 0.\n");
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        GlobalVariables::random_seed = tmp;
        break;
      }*/
      case optionIndex::EPS: {
        double tmpEps;
        parseDouble(opt.arg, tmpEps);
        if (tmpEps <= 0 || tmpEps > 1) {
          error_msg(errstr,
              "EPS needs to be a positive (small) value. Provided is %e.\n",
              tmpEps);
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        param.setEPS(tmpEps);
        break;
      }
      case optionIndex::RAYEPS: {
        double tmpEps;
        parseDouble(opt.arg, tmpEps);
        if (tmpEps <= 0 || tmpEps > 1) {
          error_msg(errstr,
              "RAYEPS needs to be a positive (small) value. Provided is %e.\n",
              tmpEps);
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        param.setRAYEPS(tmpEps);
        break;
      }
      case optionIndex::TIMELIMIT: {
        double tmpEps;
        parseDouble(opt.arg, tmpEps);
        if (tmpEps <= 0) {
          error_msg(errstr,
              "TIMELIMIT needs to be a positive value. Provided is %e.\n",
              tmpEps);
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        param.setTIMELIMIT(tmpEps);
        break;
      }
      case optionIndex::PARTIAL_BB_TIMELIMIT: {
        double tmpEps;
        parseDouble(opt.arg, tmpEps);
        if (tmpEps < 0) {
          error_msg(errstr,
              "PARTIAL_BB_TIMELIMIT needs to be a non-negative value. Provided is %e.\n",
              tmpEps);
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        param.setPARTIAL_BB_TIMELIMIT(tmpEps);
        break;
      }
      case optionIndex::BB_TIMELIMIT: {
        double tmpEps;
        parseDouble(opt.arg, tmpEps);
        if (tmpEps < 0) {
          error_msg(errstr,
              "BB_TIMELIMIT needs to be a non-negative value. Provided is %e.\n",
              tmpEps);
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        param.setBB_TIMELIMIT(tmpEps);
        break;
      }
      case optionIndex::CUTSOLVER_TIMELIMIT: {
        double tmpEps;
        parseDouble(opt.arg, tmpEps);
//        if (tmpEps <= 0) {
//          error_msg(errstr,
//              "CUTSOLVER_TIMELIMIT needs to be a positive value. Provided is %e.\n",
//              tmpEps);
//          delete[] options;
//          delete[] buffer;
//          exit(1);
//        }
        param.setCUTSOLVER_TIMELIMIT(tmpEps);
        break;
      }
      case optionIndex::MIN_ORTHOGONALITY: {
        double tmpVal;
        parseDouble(opt.arg, tmpVal);
        if (tmpVal < 0 || tmpVal > 1) {
          error_msg(errstr,
              "MIN_ORTHOGONALITY needs to be a value in [0,1]. Provided is %e.\n",
              tmpVal);
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        param.setMINORTHOGONALITY(tmpVal);
        break;
      }
      case optionIndex::MIN_VIOL_ABS: {
        double tmpVal;
        parseDouble(opt.arg, tmpVal);
        if (tmpVal <= 0 || tmpVal > 1) {
          error_msg(errstr,
              "MIN_VIOL_ABS needs to be a positive (small) value. Provided is %e.\n",
              tmpVal);
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        param.setMIN_VIOL_ABS(tmpVal);
        break;
      }
      case optionIndex::MIN_VIOL_REL: {
        double tmpVal;
        parseDouble(opt.arg, tmpVal);
        if (tmpVal <= 0 || tmpVal > 1) {
          error_msg(errstr,
              "MIN_VIOL_REL needs to be a positive (small) value. Provided is %e.\n",
              tmpVal);
          delete[] options;
          delete[] buffer;
          exit(1);
        }
        param.setMIN_VIOL_REL(tmpVal);
        break;
      }
      case optionIndex::TEMP: {
        int tmpVal;
        parseInt(opt.arg, tmpVal);
        param.setTEMP(tmpVal);
        break;
      }
      case optionIndex::CLEANING_MODE: {
        parseInt(opt.arg, CLEANING_MODE_OPTION);
        if (CLEANING_MODE_OPTION < 0 || CLEANING_MODE_OPTION > 2) {
          error_msg(errstr,
              "CLEANING_MODE needs to be 0, 1, or 2. Provided is %d.\n",
              CLEANING_MODE_OPTION);
        }
        break;
      }
      case optionIndex::UNKNOWN: // not possible because Arg::Unknown returns ARG_ILLEGAL// which aborts the parse with an error
        break;
    } /* switch opt.index() */
  } /* loop over options */

  delete[] options;
  delete[] buffer;
} /* processArgs */
