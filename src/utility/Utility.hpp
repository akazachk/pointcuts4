//============================================================================
// Name        : Utility.hpp
// Author      : akazachk
// Version     : 0.2013.05.11
// Copyright   : Your copyright notice
// Description : Header for Utility.hpp, which has miscellaneous useful functions.
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#pragma once
#include <string>
#include <vector>
#include <cstdlib>  // strtol
#include <limits>

#include "typedefs.hpp"

#include "Point.hpp"
#include "Vertex.hpp"
#include "Ray.hpp"
#include "Hplane.hpp"
#include "IntersectionInfo.hpp"
#include "Stats.hpp"
#include "SolutionInfo.hpp"
#include "AdvCut.hpp"

#include "CbcModel.hpp"
#include "OsiChooseVariable.hpp"
#include "CbcBranchDecision.hpp"

/** Overload solve from hot start because of issues */
bool solveFromHotStart(OsiSolverInterface* const solver, const int col,
    const bool isChangedUB, const double origBound, const double newBound);

double calculateStrongBranchingLB(OsiSolverInterface* solver);

void performCleaning(OsiClpSolverInterface* solver, CglGICParam& param,
    const int CLEANING_MODE);

bool cleanProblem(OsiClpSolverInterface* solver, int& numBoundsChanged,
    int& numSBFixed);

/**
 * Checks whether a solver is optimal
 * Something that can go wrong (e.g., bc1 -64 sb5 tmp_ind = 14):
 * Solver is declared optimal for the scaled problem but there are primal or dual infeasibilities in the unscaled problem
 * In this case, secondary status will be 2 or 3
 * Sometimes cleanup helps
 * Sometimes things break after enabling factorization (e.g., secondary status = 0, but sum infeasibilities > 0)
 */
bool checkSolverOptimality(OsiSolverInterface* const solver,
    const bool exitOnDualInfeas,
    const double timeLimit = std::numeric_limits<double>::max(),
    const bool doRedCostFixing = false,
    const int maxNumResolves = 2);

/**
 * Enable factorization, and check whether some cleanup needs to be done
 */
bool enableFactorization(OsiSolverInterface* const solver, const int resolveFlag = 2);

/**
 * @brief Parses int from string using strtol
 */
bool parseInt(const char *str, int &val);

/**
 * @brief Parses long int from string using strtol
 */
bool parseLong(const char *str, long &val);

/**
 * @brief Parses double from string using strtod
 */
bool parseDouble(const char *str, double &val);

/**
 * @brief Convert string of longs to vector of doubles
 */
bool parseStringOfLongs(std::string str, std::vector<long>& vec,
    const std::string& delimiter);

/**
 * @brief Convert string of doubles to vector of doubles
 */
bool parseStringOfDoubles(std::string str, std::vector<double>& vec,
    const std::string& delimiter);

/**
 * Way to check the cut limit given the large number of parameters that determine it
 */
int getCutLimit();

/**
 * @brief Universal way to check whether we reached the limit for the number of cuts for each split
 * This allows us to change between restricting number of cuts per split and total number of cuts easily
 */
bool reachedCutLimit(const int num_cuts_this_cgs, const int num_cuts_total);

/**
 * @brief Universal way to check whether we reached the limit for the time for some subroutine
 */
//bool reachedTimeLimit(const double time_spent, const double max_time);
bool reachedTimeLimit(const Stats& timer, const std::string& timeName,
    const double max_time);

bool reachedFailureLimit(const int num_cuts, const int num_fails,
    const double time, const double few_cuts_fail_threshold = 0.95,
    const double many_cuts_fail_threshold = 0.90,
    const double many_obj_fail_threshold = 0.80,
    const double time_fail_threshold = 0.66);

double getRowTwoNorm(const int row, const CoinPackedMatrix* const mat);

/**
 * Compare two double arrays for equality
 * No effort is made to check that both have the same number of elements
 */
bool vectorsEqual(const int n, const double* vec1, const double* vec2);
int isRowDifferent(const CoinPackedVector& cut1Vec, const double cut1rhs,
    const CoinPackedVector& cut2Vec, const double cut2rhs,
    const double eps = param.getDIFFEPS());

double getParallelism(const CoinPackedVectorBase& vec1,
    const CoinPackedVectorBase& vec2);
double getParallelism(const CoinPackedVectorBase& vec1, const int numElem,
    const double* vec2);
double getParallelism(const int numElem, const double* vec1,
    const double* vec2);
double getOrthogonality(const CoinPackedVectorBase& vec1,
    const CoinPackedVectorBase& vec2);
double getOrthogonality(const CoinPackedVectorBase& vec1, const int numElem,
    const double* vec2);
double getOrthogonality(const int numElem, const double* vec1,
    const double* vec2);

void getOrthogonalityStatsForRound(const OsiCuts& cs,
    double& minOrtho, double& maxOrtho, double& avgOrtho,
    const std::vector<int>* const roundWhenCutApplied = NULL,
    const int this_round = -1);

double getMinOrthogonality(const CoinPackedVector& vec, const OsiCuts& cs);

/**
 * @brief Set full vector coordinates from packed vector
 *
 * No effort is made to reset the indices that are not present in packed vector
 * so these should be initialized as desired prior to calling this function
 *
 * No check is performed for indices existing,
 * so this should be ensured before calling this function
 */
void setFullVecFromPackedVec(std::vector<double>& fullVec,
    const CoinPackedVector& vec);

/**
 * @brief Any indices that appear in vec will be put to zero in fullVec
 *
 * No effort is made to reset the indices that are not present in packed vector
 * so these should be initialized as desired prior to calling this function
 *
 * No check is performed for indices existing,
 * so this should be ensured before calling this function
 */
void clearFullVecFromPackedVec(std::vector<double>& fullVec,
    const CoinPackedVector& vec);

/** From CglLandP: return the coefficients of the intersection cut */
double unstrengthenedIntersectionCutCoeff(double abar, double f0);

/** From CglLandP compute the modularized row coefficient for an integer variable */
double modularizedCoeff(double abar, double f0);

/** Adapted from CglLandP: return the coefficients of the strengthened intersection cut */
double strengthenedIntersectionCutCoeff(double abar, double f0,
    const OsiSolverInterface* const solver = NULL, const int currFracVar = -1);

double intersectionCutCoeff(double abar, double f0,
    const OsiSolverInterface* const solver = NULL, const int currFracVar = -1,
    const bool strengthen = false);

/**
 * @brief returns lb of col (recall complementing of slacks)
 */
double getVarLB(const OsiSolverInterface* const solver, const int col);
//		const bool in_subspace_or_compl_space = param.getParamVal(ParamIndices::SUBSPACE_PARAM_IND)

/**
 * @brief returns ub of col (recall complementing of slacks)
 */
double getVarUB(const OsiSolverInterface* const solver, const int col);
//		const bool in_subspace_or_compl_space = param.getParamVal(ParamIndices::SUBSPACE_PARAM_IND)

/**
 * @brief returns value of col (recall complementing of slacks)
 */
double getVarVal(const PointCutsSolverInterface* const solver, const int col); 
double getCNBVarVal(const PointCutsSolverInterface* const solver, const int col);
double getCNBVarVal(const PointCutsSolverInterface* const solver, const int col, const bool complement);

/**
 * Objective value in non-basic space
 */
double getNBObjValue(const double* colSolution, const double* slackSolution,
    const OsiSolverInterface* const origSolver,
    const SolutionInfo& origProbData, const int deletedVar = -1);
double getNBObjValue(const OsiSolverInterface* const tmpSolver,
    const OsiSolverInterface* const origSolver,
    const SolutionInfo& origProbData, const int deletedVar = -1);

/**
 * Status of structural variables.
 * The only tricky thing is fixed variables.
 * This is decided by reduced cost, as for slacks.
 * This is a minimization problem.
 * The reduced cost, at optimality, is
 *   >= 0 for a variable at its lower bound,
 *     (increasing the variable would increase the objective value)
 *   <= 0 for a variable at its upper bound
 *     (decreasing the variable would increase the objective value).
 * Thus, if a reduced cost is < 0, the variable will be treated as being at its ub.
 */
inline bool isBasicVar(const ClpSimplex::Status stat) {
  return (stat == ClpSimplex::Status::basic);
}

inline bool isNonBasicFreeVar(const ClpSimplex::Status stat) {
  return (stat == ClpSimplex::Status::isFree);
}

// This may be wrong for fixed variables that end up being counted as at ub
inline bool isNonBasicUBCol(const ClpSimplex::Status stat) {
  return (stat == ClpSimplex::Status::atUpperBound);
}

// This may be wrong for fixed variables that end up being counted as at lb
inline bool isNonBasicLBCol(const ClpSimplex::Status stat) {
  return (stat == ClpSimplex::Status::atLowerBound);
}

inline bool isNonBasicFixedVar(const ClpSimplex::Status stat) {
  return (stat == ClpSimplex::Status::isFixed);
}

// Aliases from cstat/rstat
// Flipping we are worried about below has already occurred
inline bool isBasicCol(const std::vector<int> &cstat, const int col) {
  return isBasicVar(static_cast<ClpSimplex::Status>(cstat[col]));
}

inline bool isNonBasicUBCol(const std::vector<int> &cstat, const int col) {
  return isNonBasicUBCol(static_cast<ClpSimplex::Status>(cstat[col]));
}

inline bool isNonBasicLBCol(const std::vector<int> &cstat, const int col) {
  return isNonBasicLBCol(static_cast<ClpSimplex::Status>(cstat[col]));
}

inline bool isBasicSlack(const std::vector<int> &rstat, const int row) {
  return isBasicVar(static_cast<ClpSimplex::Status>(rstat[row]));
}

inline bool isNonBasicUBSlack(const std::vector<int> &rstat, const int row) {
  return isNonBasicUBCol(static_cast<ClpSimplex::Status>(rstat[row]));
}

inline bool isNonBasicLBSlack(const std::vector<int> &rstat, const int row) {
  return isNonBasicLBCol(static_cast<ClpSimplex::Status>(rstat[row]));
}

// Aliases from solver
inline bool isBasicCol(const OsiClpSolverInterface* const solver, const int col) {
  return isBasicVar(solver->getModelPtr()->getColumnStatus(col));
}

inline bool isNonBasicFreeCol(const OsiClpSolverInterface* const solver,
    const int col) {
  return isNonBasicFreeVar(solver->getModelPtr()->getColumnStatus(col));
}

inline bool isNonBasicUBCol(const OsiClpSolverInterface* const solver,
    const int col) {
  const ClpSimplex::Status status = solver->getModelPtr()->getColumnStatus(col);
  return ((status == ClpSimplex::Status::atUpperBound)
      || (status == ClpSimplex::Status::isFixed
          && lessThanVal(solver->getReducedCost()[col], 0.)));
}

inline bool isNonBasicLBCol(const OsiClpSolverInterface* const solver,
    const int col) {
  const ClpSimplex::Status status = solver->getModelPtr()->getColumnStatus(col);
  return ((status == ClpSimplex::Status::atLowerBound)
        || (status == ClpSimplex::Status::isFixed
            && !lessThanVal(solver->getReducedCost()[col], 0.)));
}

inline bool isNonBasicFixedCol(const OsiClpSolverInterface* const solver,
    const int col) {
  return isNonBasicFixedVar(solver->getModelPtr()->getColumnStatus(col));
}

/**
 * Status of slack variables.
 * The only tricky aspect is that the model pointer row status for slack variables
 * at ub is actually that they are at lb, and vice versa.
 * Basically, when Clp says a row is atLowerBound,
 * then it means it is a >= row, so that rowActivity == rowLower.
 * In Osi, the status of the corresponding slack is at its UPPER bound,
 * because Osi keeps all slacks with a +1 coefficient in the row.
 * When it says a row is atUpperBound,
 * then the row is a <= row and rowActivity == rowUpper.
 * In Osi, the status of the corresponding slack is at its LOWER bound,
 * since this is the regular slack we know and love.
 *
 * One more issue: fixed slack (artificial) variables.
 * The status in Osi is decided by the reduced cost.
 * This is a minimization problem; the solution is optimal when the reduced cost is
 * (as above)
 *   >= 0 for variables at their lower bound,
 *   <= 0 for variables at their upper bound.
 * Thus, if a reduced cost is < 0, the variable will be treated as being at its ub.
 * How do we get the reduced cost for slacks?
 * It is actually just the negative of the dual variable value,
 * which is stored in rowPrice.
 */
inline bool isNonBasicFreeSlack(const OsiClpSolverInterface* const solver,
    const int row) {
  return isNonBasicFreeVar(solver->getModelPtr()->getRowStatus(row));
}

inline bool isBasicSlack(const OsiClpSolverInterface* const solver, const int row) {
  return isBasicVar(solver->getModelPtr()->getRowStatus(row));
}

// NOTE: We do at *lower* bound on purpose because of comment above:
// the underlying model flips how it holds these with respect to Clp
inline bool isNonBasicUBSlack(const OsiClpSolverInterface* const solver,
    const int row) {
  const ClpSimplex::Status status = solver->getModelPtr()->getRowStatus(row);
  return ((status == ClpSimplex::Status::atLowerBound)
      || (status == ClpSimplex::Status::isFixed
          && greaterThanVal(solver->getRowPrice()[row], 0.)));
}

// NOTE: We do at *upper* bound on purpose because of comment above:
// the underlying model flips how it holds these with respect to Clp
inline bool isNonBasicLBSlack(const OsiClpSolverInterface* const solver,
    const int row) {
  const ClpSimplex::Status status = solver->getModelPtr()->getRowStatus(row);
  return ((status == ClpSimplex::Status::atUpperBound)
      || (status == ClpSimplex::Status::isFixed
          && !greaterThanVal(solver->getRowPrice()[row], 0.)));
}

inline bool isNonBasicFixedSlack(const OsiClpSolverInterface* const solver,
    const int row) {
  return isNonBasicFixedVar(solver->getModelPtr()->getRowStatus(row));
}

inline bool isBasicVar(const OsiClpSolverInterface* const solver, const int col) {
  return isBasicVar(solver->getModelPtr()->getStatus(col));
}

/**
 * The variables that lead to rays are non-basic structural variables
 * and non-basic slacks not coming from equality constraints.
 */
bool isRayVar(const OsiClpSolverInterface* const solver, const int var);

inline bool isNonBasicLBVar(const OsiClpSolverInterface* const solver,
    const int var) {
  return
      (var < solver->getNumCols()) ?
          isNonBasicLBCol(solver, var) :
          isNonBasicLBSlack(solver, var - solver->getNumCols());
}

inline bool isNonBasicUBVar(const OsiClpSolverInterface* const solver,
    const int var) {
  return
      (var < solver->getNumCols()) ?
          isNonBasicUBCol(solver, var) :
          isNonBasicUBSlack(solver, var - solver->getNumCols());
}

inline bool isNonBasicFreeVar(const OsiClpSolverInterface* const solver,
    const int var) {
  return
      (var < solver->getNumCols()) ?
          isNonBasicFreeCol(solver, var) :
          isNonBasicFreeSlack(solver, var - solver->getNumCols());
}

void setClpParameters(OsiClpSolverInterface* const solver,
    const double max_time = std::numeric_limits<double>::max());

void setMessageHandler(CbcModel* const cbc_model);

/**
 * We need to be careful with the strong branching options;
 * sometimes the Clp strong branching fails, such as with arki001, branching down on variable 924
 */
void setupClpForStrongBranching(OsiClpSolverInterface* const solver,
    const int hot_start_iter_limit = std::numeric_limits<int>::max());

/**
 * Sets message handler and special options when using solver as part of B&B
 * (in which we want to run full strong branching and enable the fixing of variables)
 */
void setupClpForCbc(OsiClpSolverInterface* const solver, 
    const int hot_start_iter_limit = std::numeric_limits<int>::max()); 

/**
 * Set parameters for Cbc used for VPCs, as well as the custom branching decision
 */
void setCbcParametersForPartialBB(CbcModel* const cbc_model,
    CbcEventHandler* eventHandler = NULL, const int numStrong = 5,
    const int numBeforeTrusted = 10, const double max_time =
        std::numeric_limits<double>::max());

int pivot(OsiClpSolverInterface*& pivotSolver, const int varIn,
    const int dirnIn, int& varOut, int& dirnOut, int& pivotRow, double& dist);

void copySolverBasis(PointCutsSolverInterface* const solverCopy,
    const PointCutsSolverInterface* const solverOld);

/**
 * Returns the index in raysCutByHplane of the first ray that is cut by this hyperplane in this activation
 */
int getStartIndex(const Hplane& hplane, //const std::vector<Hplane>& hplaneStore, const int hplane_ind,
    const int split_ind, const int act_ind);

/**
 * Returns the index in raysCutByHplane of the last ray that is cut by this hyperplane in this activation
 */
int getEndIndex(const Hplane& hplane, //const std::vector<Hplane>& hplaneStore, const int hplane_ind,
    const int split_ind, const int act_ind);

void storeNewRayNBIndices(int nNonBasicCols,
    std::vector<int> &newRayColIndices,
    const std::vector<int> &allRaysCutByHplane,
    std::vector<bool> &raysCutFlag);

void delElmtsInIntVector(std::vector<int>& intVector,
    const std::vector<int>& delIndices);

void delElmtsInDoubleVector(std::vector<double>& dblVector,
    const std::vector<int>& delIndices);

void delElmtsInPartitionVector(std::vector<int>& partitionVector,
    const std::vector<int>& delIndices);

bool duplicatePoint(const CoinPackedMatrix& interPtsAndRays,
    const std::vector<double>& rhs, const Point& newPoint);

bool duplicateRay(const CoinPackedMatrix& interPtsAndRays,
    const std::vector<double>& rhs, const Ray& newRay);

int findDuplicateRay(const CoinPackedMatrix& interPtsAndRays,
    const std::vector<double>& rhs, const Ray& newRay);

bool rayCutByHplaneForSplit(const int oldHplaneIndex,
    const std::vector<Hplane>& hplaneStore, const int ray_ind,
    const std::vector<Ray>& rayStore, const int split_ind);

/*
 * Method checks if a hyperplane has been previous activated.
 * Only checks indices specified
 * @return the index if yes and -1 otherwise
 */
int hasHplaneBeenActivated(const std::vector<Hplane>& hplanesPrevAct,
    const std::vector<int> actHplaneIndex, const Hplane& tmpHplane,
    const bool returnIndexWIActivatedVec);

/*
 * Method checks if a hyperplane has been previous activated.
 * @return the index if yes and -1 otherwise
 */
int hasHplaneBeenActivated(const std::vector<Hplane>& hplanesPrevAct,
    const Hplane& tmpHplane, const int start_ind = 0);

/**
 * Method checks if a vertex has been previously created
 * @return the index if yes and -1 otherwise
 */
int hasVertexBeenActivated(const std::vector<Vertex>& verticesPrevAct,
    const Vertex& tmpVertex);

/**
 * Returns determinant of the 1x1 or 2x2 matrix specified
 */
double determinant(const std::vector<std::vector<double> >& mx,
    const std::vector<int>& rows, const std::vector<int>& cols);

/********************/
/* RAY FUNCTIONS */
/********************/
bool calcIntersectionPointWithSplit(Point& intPt, double& dist,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    const Vertex& vertex, const Ray& ray, const int split_var, const bool recalc_dist = true);
bool calcIntersectionPointWithSplit(Point& intPt,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    const Vertex& vertex, const Ray& ray, const int split_var);

/**
 * Starting at origin, we proceed along ray r^j until we intersect H_h.
 * This leads to vertex v^{jh}.
 * Thus, we want to perform the pivot in which j enters the basis and h leaves it.
 * @param rayOutIndex :: the ray index r^j we want to pivot out (one component, in jth index, of 1.0 value)
 *
 * @return Ray \ell from pivot in which j leaves basis and h enters.
 */
void calcNewRayCoordinatesRank1(CoinPackedVector& rayIn, //const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo, const int rayOutIndex,
    const int newRayIndex, //const std::vector<Ray>& rayStore,
    const Hplane& hplane, const double RAYEPS);
//    const int hplane_ind, const std::vector<Hplane>& hplaneStore);

//void calcNewRayCoordinatesRank1(CoinPackedVector& rayIn, const PointCutsSolverInterface* const solver,
//    const SolutionInfo& solnInfo, const int rayOutIndex,
//    const int newRayIndex, const std::vector<Ray>& rayStore,
//    const std::vector<Hplane>& hplaneStore);

/**
 * Starting at origin, we proceed along ray r^j until we intersect H_h.
 * This leads to vertex v^{jh}.
 * Similarly, starting at origin and proceeding along r^k, the first hplane
 * intersected was H_g, leading to vertex v^{kg}.
 *
 * Let r^{jh} be a ray from v^{jh}.
 * Coordinates of r^{jh} in nb space are simple:
 * 1. non-zero corresponding to r^j
 * 2. non-zero corresponding to which of the other n-1 hplanes we depart
 * Note that this leads to a total of n-1 rays, as desired.
 *
 * Now r^{jh} intersects H_g.
 * Specifically, it intersects the corresponding ray emanating from v^{kg}.
 * Call this ray r^{kg}.
 * Then we get the new vertex v^{jhkg}.
 * And now we calculate one of the new rays, which will have three non-basic components.
 */
void calcNewRayCoordinatesRank2(Ray& rayIn, const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo, const int rayOutIndex1,
    const int rayOutIndex2, const int newRayIndex, const Vertex& vertex,
    const std::vector<Ray>& rayStore,
    const std::vector<Hplane>& hplaneStore);

/********************/
/* HPLANE FUNCTIONS */
/********************/

/**
 * @brief Reimplemented from AdvCuts class because of precision issues with taking inverses
 *
 * @param split_var :: split
 * @param vec :: must be sorted in increasing order (in terms of indices) and be in NB space
 */
double getSICActivity(const int split_var,
    const std::vector<Vertex>& vertexStore,
    const std::vector<Ray>& rayStore, const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo, const CoinPackedVector& vec);

/**
 * Finds coordinates of new vertex obtained by proceeding along rayOut from vertex a distance of dist
 */
void calcNewVectorCoordinates(CoinPackedVector& newVector, const double dist,
    const CoinPackedVector& vertex, const CoinPackedVector& rayOut);

double findFirstNBBound(const CoinPackedVector& vertex,
    const CoinPackedVector& rayOut, Hplane& hplane,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo);

double distanceToSplit(const CoinPackedVector& vertex,
    const CoinPackedVector& rayOut, const int split_var,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo);

double dotProductWithOptTableau(const CoinPackedVector& vec, const int row_ind,
    const SolutionInfo& solnInfo);

void packedSortedVectorSum(CoinPackedVector& sum, const double mult1,
    const CoinPackedVectorBase& vec1, const double mult2,
    const CoinPackedVectorBase& vec2, const double eps = param.getEPS());

double packedSum(const CoinPackedVector& vec1, const CoinPackedVector& vec2);

//double packedDotProduct(const CoinPackedVector& vec1,
//    const CoinPackedVector& vec2);

/********************/
/* POINT STATISTICS */
/********************/

void calculatePointStats(double& min_SIC_depth, double& max_SIC_depth,
    double& sum_SIC_depth, double& sumSquares_SIC_depth,
    double& min_obj_depth, double& max_obj_depth, double& sum_obj_depth,
    double& sumSquares_obj_depth,
//    const int split_ind,
    const int pt_ind,
    const PointCutsSolverInterface* const solver, const SolutionInfo& solnInfo,
    IntersectionInfo& interPtsAndRays,
//    const std::vector<Hplane>& hplaneStore,
    const AdvCut* curr_cut,
    const double cutNorm, const AdvCut& objCut, const double objNorm);

bool pointInP(const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo, const IntersectionInfo& interPtsAndRays,
    const int pt_ind);

bool pointInP(const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo, const int numElmts, const int* elmtIndices,
    const double* elmtVals);

inline bool pointInP(const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo, const CoinPackedVector& vec) {
  return pointInP(solver, solnInfo, vec.getNumElements(), vec.getIndices(),
      vec.getElements());
}

/*********************/
/* GENERAL FUNCTIONS */
/*********************/

/***********************************************************************/
/**
 * @brief Get environment variable by key.
 * @param key :: Key to look up.
 */
std::string get_env_var(std::string const & key);

/***********************************************************************/
/**
 * @brief Find value in a std::vector and return where it is in the std::vector.
 * @param key :: Key to look up.
 */
int find_val(const int &key, const std::vector<int> &vec);

/***********************************************************************/
/**
 * @brief Find value in a std::vector and return where it is in the std::vector.
 * @param key :: Key to look up.
 */
int find_val(const double &key, const std::vector<double> &vec);

/***********************************************************************/
/**
 * @brief Find value in a std::vector and return where it is in the std::vector.
 * @param key :: Key to look up.
 */
int find_val(const std::string &key, const std::vector<std::string> &vec);

/****************************************************************************/
/**
 * @brief Check if a given file exists in the system.
 *
 * @returns True if exists, False otherwise
 */
bool fexists(const char *filename);

double computeMinCutEucl(const AdvCuts &cutinfo);
double computeMaxCutEucl(const AdvCuts &cutinfo);
double computeAverageCutEucl(const AdvCuts &cutinfo);
double computeMinCutObj(const AdvCuts &cutinfo);
double computeMaxCutObj(const AdvCuts &cutinfo);
double computeAverageCutObj(const AdvCuts &cutinfo);
//
//inline bool compareAsc(ptSortInfo *tmp1, ptSortInfo *tmp2) {
//  if ((*tmp1).cost <= (*tmp2).cost - param.getEPS())
//    return true;
//  else
//    return false;
//}
//
//inline bool compareDesc(ptSortInfo *tmp1, ptSortInfo *tmp2) {
//  if ((*tmp1).cost >= (*tmp2).cost + param.getEPS())
//    return true;
//  else
//    return false;
//}

/**
 * NOTE: the below sorting functions will only work when the index list given is {0,...,size}
 * (not necessarily in that order)
 * because the index list is used to access elements of arr
 *
 * If you want to, say, sort [10,1,2] by [0.5,20.5,10.0], the below would not work
 * Typically you need to use a struct then combining the data together
 */
template<class T> struct index_cmp_dsc {

  const T arr; // array used to be sorted

  // Constructor
  index_cmp_dsc(const T _arr) :
      arr(_arr) {
  }

  // Comparator
  bool operator()(const size_t a, const size_t b) const {
    //return arr[a] > arr[b] + param.getEPS();
    return arr[a] > arr[b];
  }
};

template<class T> struct index_cmp_asc {

  const T arr; // array used to be sorted

  // Constructor
  index_cmp_asc(const T _arr) :
      arr(_arr) {
  }

  // Comparator
  bool operator()(const size_t a, const size_t b) const {
    //return arr[a] < arr[b] - param.getEPS();
    return arr[a] < arr[b];
  }
};

template<class T1, class T2> struct index_cmp2 {

  const T1 arr1; // array used to be sorted
  const T2 arr2;
  bool flag;
  // Constructor
  index_cmp2(const T1 _arr1, const T2 _arr2) :
      arr1(_arr1), arr2(_arr2) {
    flag = false;
  }

  // Comparator
  bool operator()(const size_t a, const size_t b) {
    flag = false;
    if (arr1[a] - arr1[b] >= param.getEPS())
      flag = true;
    else {
      if ((arr1[a] - arr1[b]) > -param.getEPS() && (arr1[a] - arr1[b]) < param.getEPS()) {
        if (arr2[a] - arr2[b] <= -param.getEPS())
          flag = true;
      }
    }
    return flag;
  }
};

template<class T1, class T2> struct index_cmp3 {

  const T1 arr1; // array used to be sorted
  const T2 arr2;
  bool flag;
  // Constructor
  index_cmp3(const T1 _arr1, const T2 _arr2) :
      arr1(_arr1), arr2(_arr2) {
    flag = false;
  }

  // Comparator
  bool operator()(const size_t a, const size_t b) {
    flag = false;
    if (arr1[a] - arr1[b] <= -param.getEPS())
      flag = true;
    else {
      if ((arr1[a] - arr1[b]) > -param.getEPS() && (arr1[a] - arr1[b]) < param.getEPS()) {
        if (arr2[a] - arr2[b] >= param.getEPS())
          flag = true;
      }
    }
    return flag;
  }
};

template<class T1, class T2> struct index_cmp4 {

  const T1 arr1; // array used to be sorted
  const T2 arr2;
  bool flag;
  // Constructor
  index_cmp4(const T1 _arr1, const T2 _arr2) :
      arr1(_arr1), arr2(_arr2) {
    flag = false;
  }

  // Comparator
  bool operator()(const size_t a, const size_t b) {
    flag = false;
    if (arr1[a] - arr1[b] >= param.getEPS())
      flag = true;
    else {
      if ((arr1[a] - arr1[b]) > -param.getEPS() && (arr1[a] - arr1[b]) < param.getEPS()) {
        if (arr2[a] - arr2[b] >= param.getEPS())
          flag = true;
      }
    }
    return flag;
  }
};

template<class T1, class T2, class T3, class T4> struct index_cmp5 {

  const T1 arr1; // array used to be sorted
  const T2 arr2;
  const T3 arr3;
  const T4 arr4;
  bool flag;
  // Constructor
  index_cmp5(const T1 _arr1, const T2 _arr2, const T3 _arr3, const T4 _arr4) :
      arr1(_arr1), arr2(_arr2), arr3(_arr3), arr4(_arr4) {
    flag = false;
  }

  // Comparator
  bool operator()(const size_t a, const size_t b) {
    flag = false;
    if (arr1[a] - arr1[b] >= param.getEPS())
      flag = true;
    else {
      if ((arr1[a] - arr1[b]) > -param.getEPS() && (arr1[a] - arr1[b]) < param.getEPS()) {
        if (arr2[a] - arr2[b] >= param.getEPS())
          flag = true;
        else {
          if ((arr2[a] - arr2[b]) > -param.getEPS()
              && (arr2[a] - arr2[b]) < param.getEPS()) {
            if (arr3[a] - arr3[b] >= param.getEPS())
              flag = true;
            else {
              if ((arr3[a] - arr3[b]) > -param.getEPS()
                  && (arr3[a] - arr3[b]) < param.getEPS()) {
                if (arr4[a] - arr4[b] >= param.getEPS())
                  flag = true;
              }
            }
          }
        }
      }

    }
    return flag;
  }
};

double computeAverage(const std::vector<double> &vec);

double computeAverage(const std::vector<int> &vec);

double computeAverage(const std::vector<double> &vec, const int start_index,
    const int end_index);

double computeAverage(const std::vector<int> &vec, const int start_index,
    const int end_index);

double computeAverage(const std::vector<double> &vec,
    const std::vector<int> &index);

double computeAverage(const std::vector<int> &vec,
    const std::vector<int> &index);

double computeAverageDiff(const std::vector<double> &vec1,
    const std::vector<double> &vec2);

double computeAverageDiff(const std::vector<int> &vec1,
    const std::vector<int> &vec2);

double computeAverageDiff(const std::vector<double> &vec1,
    const std::vector<double> &vec2, const int start_index,
    const int end_index);

double computeAverageDiff(const std::vector<int> &vec1,
    const std::vector<int> &vec2, const int start_index,
    const int end_index);

double computeAverageDiff(const std::vector<double> &vec1,
    const std::vector<double> &vec2, const std::vector<int> &index);

double computeAverageDiff(const std::vector<int> &vec1,
    const std::vector<int> &vec2, const std::vector<int> &index);

std::vector<double>::const_iterator computeMin(const std::vector<double> &vec);
double computeMin(const int num_el, const double* vec);
double computeMinAbs(const int num_el, const double* vec);
double computeMaxAbs(const int num_el, const double* vec);

std::vector<int>::const_iterator computeMin(const std::vector<int> &vec);

std::vector<double>::const_iterator computeMin(const std::vector<double> &vec,
    const std::vector<int> &index);

std::vector<int>::const_iterator computeMin(const std::vector<int> &vec,
    const std::vector<int> &index);

std::vector<double>::const_iterator computeMax(const std::vector<double> &vec);

std::vector<int>::const_iterator computeMax(const std::vector<int> &vec);

std::vector<double>::const_iterator computeMax(const std::vector<double> &vec,
    const std::vector<int> &index);

std::vector<int>::const_iterator computeMax(const std::vector<int> &vec,
    const std::vector<int> &index);

double computeMinDiff(const std::vector<double> &vec1,
    const std::vector<double> &vec2);

int computeMinDiff(const std::vector<int> &vec1, const std::vector<int> &vec2);

double computeMinDiff(const std::vector<double> &vec1,
    const std::vector<double> &vec2, const std::vector<int> &index);

int computeMinDiff(const std::vector<int> &vec1, const std::vector<int> &vec2,
    const std::vector<int> &index);

double computeMaxDiff(const std::vector<double> &vec1,
    const std::vector<double> &vec2);

int computeMaxDiff(const std::vector<int> &vec1, const std::vector<int> &vec2);

double computeMaxDiff(const std::vector<double> &vec1,
    const std::vector<double> &vec2, const std::vector<int> &index);

int computeMaxDiff(const std::vector<int> &vec1, const std::vector<int> &vec2,
    const std::vector<int> &index);

/********************/
/* CONVERSION FUNCS */
/********************/

/***********************************************************************/
/**
 * The solution to the LP relaxation of the problem has all non-basic
 *  variables are at zero, but these are not all zero when we compute
 *  rays that intersect with bd S. Since this computation is done all
 *  in the non-basic space, but we are using the structural space for
 *  computing the lifting, we need to find the coordinates of the
 *  intersection points in the structural space.
 *
 *  @param solver       ::  Solver we are using
 *  @param rowOfVar     ::  Row in which variable is basic (indexed from 0 to numcols)
 *  @param newpt        ::  [point][value]
 *  @param nbinfo       ::  [point][index of nb ray][value]
 */
void compStructCoords(const PointCutsSolverInterface* const solver, std::vector<int> &rowOfVar,
    std::vector<std::vector<double> > &newpt,
    std::vector<std::vector<std::vector<double> > > &nbinfo, FILE *log_out);

/***********************************************************************/
/**
 * The solution to the LP relaxation of the problem has all non-basic
 *  variables are at zero, but these are not all zero when we compute
 *  rays that intersect with bd S. Since this computation is done all
 *  in the non-basic space, but we are using the structural space for
 *  computing the lifting, we need to find the coordinates of the
 *  intersection points in the structural space.
 *
 *  @param solver       ::  Solver we are using
 *  @param rowOfVar     ::  Row in which variable is basic (indexed from 0 to numcols)
 *  @param newpt        ::  [point][value]
 *  @param nbinfo       ::  [point][index of nb ray][value]
 */
void compNBCoor(std::vector<double>& NBSolution, const double* structSolution,
    const PointCutsSolverInterface* const tmpSolver,
    const PointCutsSolverInterface* const origSolver,
    const SolutionInfo& origSolnInfo);

/***********************************************************************/
/**
 * @brief Corrects coefficients and right side of cut that are incorrect because of
 * complemented variables from the original problem.
 *
 * @param orderedLiftedCut  ::  [0: split, 1: rhs, 2+: coeffs...]
 * @param deletedCols       ::  Indices of removed columns
 * @param solver            ::  To retrieve original values
 */
//void complementCoeffs(AdvCut &orderedLiftedCut, std::vector<int> &deletedCols,
//    LiftGICsSolverInterface* solver);

/***********************************************************************/
/**
 * Because Selva's code generates the cut
 * \sum_{s \in N \cap Slack Vars} s_j/\theta_j
 *   + \sum_{j \in N \cap L} (x_j - \ell_j)/\theta_j
 *   + \sum_{j \in N \cap L} (u_j - x_j)/\theta_j
 *   \ge 1
 * This is the so-called y space, if we substitute y_j in there.
 * And we want it in the original x space.
 */
void convertCutFromYSpaceToXSpace(AdvCut& cutinfo, const SolutionInfo& solnInfo,
    const PointCutsSolverInterface* const solver);

/***********************************************************************/
/**
 * @brief Converts a cut from the non-basic space to the structural space.
 *
 * @param structCut               ::  OUTPUT: [split var index, rhs, coeffs]
 * @param nonBasicOrigVarIndex    ::  Indices of NB structural variables
 * @param nonBasicSlackVarIndex   ::  Indices of NB slack variables
 * @param solver                  ::  Solver
 * @param JspaceCut               ::  [split var index, rhs, coeffs]
 */
/*
void convertCutFromJSpaceToStructSpace(AdvCut &structCut,
    std::vector<int> &nonBasicOrigVarIndex,
    std::vector<int> &nonBasicSlackVarIndex, LiftGICsSolverInterface* solver,
    AdvCut &JspaceCut);
*/

/***********************************************************************/
/**
 * We are given a cut in the structural space and we want to convert
 * it to the non-basic space. This means, for all structural variables
 * that are basic, we replace
 */
/*
void convertStructSpaceCutToJSpace(AdvCut &JCut, LiftGICsSolverInterface* solver,
    std::vector<int> &rowOfVar, std::vector<int> &nonBasicOrigVarIndex,
    std::vector<int> &nonBasicSlackVarIndex, AdvCut &structCut);
*/

/********************************************************************************/
/**
 * Check if two solvers have the same everything:
 * 1. colSolution
 * 2. rowPrice
 * 3. slackVals
 * 4. cstat and rstat
 * 5. reducedCosts
 */
bool solversEqual(const PointCutsSolverInterface* const clp1, const PointCutsSolverInterface* const clp2);

/********************/
/* STRING FUNCTIONS */
/********************/

/***********************************************************************/
/**
 * Parses string line by the set of delimiters given.
 * (Only works well with one for now.)
 * Parses string based on delimiters provided and puts output into vector.
 */
void stringparse(std::string &line, std::string delims,
    std::vector<std::string> &strings);

/****************************************************************************/
double compute_condition_number_norm1(OsiSolverInterface *solver);

/****************************************************************************/
double compute_condition_number_norm2(const OsiSolverInterface* const solver);

/****************************************************************************/
double compute_condition_number(OsiSolverInterface *solver, const int norm);

/*************/
/* DEBUGGING */
/*************/

double rayPostPivot(const int component, const int nb_var_in1,
    const int struct_var_out1, const int nb_var_in2,
//    const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo);

double vertexPostSinglePivot(const int component, const double rayDirnToHplane1,
    const double init_val_out1, const double final_val_out1);

double pointPostDoublePivot(const int component, const double rayDirnToHplane1,
    const double init_val_out1, const double final_val_out1,
    const double rayDirnToSplit1, const double rayDirnToSplit2,
    const double rayDirnToHplane2, const double init_val_out2,
    const double final_val_out2);

double pointPostPivot(const int component, const int nb_var_in1,
    const int struct_var_out1, const int nb_var_in2,
    const int struct_var_out2, const PointCutsSolverInterface* const solver,
    const SolutionInfo& solnInfo, const bool computeVertexFlag = false);

//const char* toLower(const char* str);
//const char* toUpper(const char* str);
//const char* toLowerNoUnderscore(const char* str);
//const char* toUpperNoUnderscore(const char* str);
std::string toLowerString(const std::string& str);
//const char* toLowerString(const std::string str);
std::string toUpperString(const std::string& str);
std::string toLowerStringNoUnderscore(const std::string& str);
std::string toUpperStringNoUnderscore(const std::string& str);
std::vector<std::string> lowerCaseStringVector(const std::vector<std::string>& strVec);

void printSparseVector(const std::vector<int>& vec);
void printSparseVector(const std::vector<double>& vec);
void printSparseVector(const int numElem, const double* vec);
void printSparseVector(const int numElem, const int* vec);
void printSparseVector(const int numElem, const int* ind, const double* vec, const double eps = param.getEPS());

double getObjValueFromFile(const char* filename, const std::string& this_inst_name);

// Last edit: 03/27/12
//
// Name:     common_definitions.hpp
// Author:   Giacomo Nannicini
//           Singapore University of Technology and Design, Singapore
//           email: nannicini@sutd.edu.sg
// Date:     04/09/11
//-----------------------------------------------------------------------------
// Copyright (C) 2012, Giacomo Nannicini.  All Rights Reserved.

// Definitions, macros, and functions which are common throughout the
// project.

double dotProduct(const CoinPackedVector& vec1, const double* vec2);
double dotProduct(const CoinPackedVector& vec1, const CoinPackedVector& vec2);

// Compute dot product using compensated summation to have small
// numerical error. First version: dense vectors
double dotProduct(const double* a, const double* b, int dimension);

// Second version: first vector is sparse, second one is dense
double dotProduct(int sizea, const int* indexa, const double* a,
    const double* b);

// Third version: sparse vectors
double dotProduct(int sizea, const int* indexa, const double* a, int sizeb,
    const int* indexb, const double* b);

/*
// Remove underscores and change to upper case
void standardizeString(char* string);

/// Linear congruential generator. given the seed, the generated numbers are
/// always the same regardless the architecture.
/// Based on the generator in CoinHelperFunctions.hpp.
/// Command can be: 0 - generate random number
///                 1 - set seed (second argument)
///                 2 - get seed (in second argument)
/// Return random number between 0 and 1.
double randomNumber(unsigned int command = 0, unsigned int* seed = NULL);
*/
