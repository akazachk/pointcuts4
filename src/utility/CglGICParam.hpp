// Name:     CglGICParam.hpp
// Author:   Aleksandr M. Kazachkov 
//           based on CglGMIParam.hpp by Giacomo Nannicini
//           and on CglRedSplitParam.hpp by Francois Margot
// Date:     02/02/16
//-----------------------------------------------------------------------------

/**
 * To add a new parameter to CglGICParamNamespace
 * (which we are currently using when setting params from the command line)
 * 1. Add it to ParamIndices
 * 2. In the same position, add its name into ParamName
 * 3. In the same position, add its default value into ParamDefaults
 * 4. Add it to optionparser.h
 *    a. Add it to optionIndex (very preferably, somewhere similar in the order of ParamIndices
 *    b. In same position, put its name in optionName
 *    c. Add it into the list of parameters, with a nice short description, in PointCuts.cpp
 *    d. Process the parameter within init() function in PointCuts.cpp (keep the order)
 * 5. Add the parameter and its allowed values to checkParams() in CglGICParam
 *
 * To add a new parameter to CglGICParams
 * (when the parameter might be something we set once and don't add as a command line parameter because we are lazy)
 * 1.
 */

#pragma once

#include <string>
#include <vector>
#include "CglParam.hpp"
#include <limits>
#include <cmath> // sqrt

namespace CglGICParamNamespace {
  enum ParamIndices {
    SICS_PARAM_IND,
    GMI_PARAM_IND,
    LANDP_PARAM_IND,
    TILTED_DEPTH_PARAM_IND,
    VPC_DEPTH_PARAM_IND,
    PHA_PARAM_IND,
    NB_SPACE_PARAM_IND, // 0: Do not use nb space when generating cuts, 1: use it
    STRENGTHEN_PARAM_IND,
    MIX_PARAM_IND,
    PIVOT_PARAM_IND,
    PARTIAL_BB_STRATEGY_PARAM_IND, // The total gives the choose variable decision (hundreds) and branch decision (tens) and node comparison choice (ones); hundreds digit: 0: default, 1: default+second criterion, 2: max min change+second (max max change), 3: second-best default, 4: second-best max-min change, -x: -1 * (1+x); tens digit: 0: default, 1: dynamic, 2: strong, 3: none; ones digit: 0: default: 1: bfs, 2: depth, 3: estimate, 4: objective
    PARTIAL_BB_NUM_STRONG_PARAM_IND, // Number strong branching candidates. -2: sqrt(n); -1: full; 0+: as in Cbc
    VPC_DEPTH2_HEUR_PARAM_IND, // 0: cut rays, 1: activate hyperplanes
    MAX_HPLANES_PARAM_IND, // Number of hyperplanes to activate (pcuts)
    SPLIT_VAR_DELETED_PARAM_IND, // Should split var be deleted when forming facet solvers
    PHA_ACT_OPTION_PARAM_IND, // -1: skip, 0: first, 1: best, 2: best_final
    NEXT_HPLANE_FINAL_PARAM_IND, // If generating more than 1 hplane cutting each ray, how to choose
    NUM_EXTRA_ACT_ROUNDS_PARAM_IND, // 1 or 2 if next_hplane_final = false, else can be more
    NUM_RAYS_CUT_PARAM_IND, // 0 means as many as possible
    MAX_FRAC_VAR_PARAM_IND,
    CGS_PARAM_IND, // 0: splits, 1: triangles (all)
    NUM_CGS_PARAM_IND, // 0 means as many as possible
    NUM_CUTS_PER_CGS_PARAM_IND,
    OVERLOAD_MAX_CUTS_PARAM_IND, // if > 0, replaces num_cuts_per_cgs parameter for determining max num cuts; < 0 = abs(val) * numSplits
    LIMIT_CUTS_PER_CGS_PARAM_IND, // should number of cuts be limited per split or from total
    PRLP_STRATEGY_PARAM_IND, // 0: default, 1: try few objectives
    FLIP_BETA_PARAM_IND, // 0: no, 1: yes, 2: try even if PRLP is feasible, <0: same but with -1 beta to start
    USE_SPLIT_SHARE_PARAM_IND, // should the split share be used to generate cuts (may be slow): 0 no, 1 yes first, 2 yes last
    NUM_CUTS_ITER_BILINEAR_PARAM_IND, // 0: do not use the iterative bilinear cut generation, 1+: num cuts to try to generate
    USE_CUT_VERT_HEUR_PARAM_IND, // 0: do not use the cut vertices cut selection heuristic
    MODE_OBJ_PER_POINT_PARAM_IND, // 0: sum up rays, 1: one ray at a time; 1x: use rays as well; 0/2xy: sort by increasing angle with obj / slack; 1/3xy: same but decreasing
    NUM_OBJ_PER_POINT_PARAM_IND, // # cuts to try to generate for the strong branching lb point (and others)
    USE_ALL_ONES_HEUR_PARAM_IND, // 0: do not use, 1: use
    USE_UNIT_VECTORS_HEUR_PARAM_IND, // 0: do not use, 1+: num to try, <0: abs(val) * sqrt(n)
    USE_TIGHT_POINTS_HEUR_PARAM_IND, // 0: do not use the tight on points cut heuristic, 1+: num points to try
    USE_TIGHT_RAYS_HEUR_PARAM_IND, // 0: do not use the tight on rays cut heuristic, 1+: num rays to try, -1: try the first sqrt(# rays) rays
    CUT_PRESOLVE_PARAM_IND, // use presolve in cutSolver? (1: initial, 2: initial+resolve)
    MAX_POINTS_PER_SPLIT_SPLIT_SHARE_PARAM_IND, // how many points per split when using split share
    SUBSPACE_PARAM_IND, // 0: Do not generate subspace, 1: generate subspace
    ROUNDS_PARAM_IND, // 0: no cuts, 1+: number of rounds of cuts
    BB_RUNS_PARAM_IND, // 0: do not perform branch-and-bound, > 0: this many seeds (also maybe randomizations of the row or column order)
    BB_STRATEGY_PARAM_IND, // see enum below; negative value means use default for whichever solvers are enabled
    NUM_PARAMS
  };

  const std::vector<std::string> ParamName { "SICS", "GMI", "LANDP",
      "TILTED_DEPTH", "VPC_DEPTH", "PHA", "NB_SPACE", "STRENGTHEN", "MIX",
      "PIVOT", "PARTIAL_BB_STRATEGY", "PARTIAL_BB_NUM_STRONG",
      "VPC_DEPTH2_HEUR", "MAX_HPLANES", "SPLIT_VAR_DELETED", "PHA_ACT_OPTION",
      "NEXT_HPLANE_FINAL", "NUM_EXTRA_ACT_ROUNDS", "NUM_RAYS_CUT",
      "MAX_FRAC_VAR", "CGS", "NUM_CGS", "NUM_CUTS_PER_CGS", "OVERLOAD_MAX_CUTS",
      "LIMIT_CUTS_PER_CGS", "PRLP_STRATEGY", "FLIP_BETA", "USE_SPLIT_SHARE",
      "NUM_CUTS_ITER_BILINEAR", "USE_CUT_VERT_HEUR", "MODE_OBJ_PER_POINT",
      "NUM_OBJ_PER_POINT", "USE_ALL_ONES_HEUR", "USE_UNIT_VECTORS_HEUR",
      "USE_TIGHT_POINTS_HEUR", "USE_TIGHT_RAYS_HEUR", "CUT_PRESOLVE",
      "MAX_POINTS_PER_SPLIT_SPLIT_SHARE", "SUBSPACE", "ROUNDS", "BB_RUNS",
      "BB_STRATEGY" };

  const std::vector<int> ParamDefaults {
      1,     //    SICS_DEFAULT
      0,     //    GMI_DEFAULT
      0,     //    LANDP_DEFAULT
      -1,    //    TILTED_DEPTH_DEFAULT
      0,     //    VPC_DEPTH_DEFAULT
      0,     //    PHA_DEFAULT
      1,     //    NB_SPACE_DEFAULT
      0,     //    STRENGTHEN_DEFAULT
      0,     //    MIX_DEFAULT
      0,     //    PIVOT_DEFAULT
      4,     //    PARTIAL_BB_STRATEGY_DEFAULT
      5,     //    PARTIAL_BB_NUM_STRONG_DEFAULT
      0,     //    VPC_DEPTH2_HEUR_DEFAULT
      1,     //    MAX_HPLANES_DEFAULT
      0,     //    SPLIT_VAR_DELETED_DEFAULT
      2,     //    PHA_ACT_OPTION_DEFAULT
      1,     //    NEXT_HPLANE_FINAL_DEFAULT
      0,     //    NUM_EXTRA_ACT_ROUNDS_DEFAULT
      0,     //    NUM_RAYS_CUT_DEFAULT
      0,     //    MAX_FRAC_VAR_DEFAULT
      0,     //    CGS_DEFAULT
      0,     //    NUM_CGS_DEFAULT
      5,     //    NUM_CUTS_PER_CGS_DEFAULT
      -1,    //    OVERLOAD_MAX_CUTS_DEFAULT
      0,     //    LIMIT_CUTS_PER_CGS_DEFAULT
      1,     //    PRLP_STRATEGY_DEFAULT
      0,     //    FLIP_BETA_DEFAULT
      1,     //    USE_SPLIT_SHARE_DEFAULT
      1,     //    NUM_CUTS_ITER_BILINEAR_DEFAULT
      1,     //    USE_CUT_VERT_HEUR_DEFAULT
      121,   //    MODE_OBJ_PER_POINT_DEFAULT
      -2,    //    NUM_OBJ_PER_POINT_DEFAULT
      1,     //    USE_ALL_ONES_HEUR_DEFAULT
      0,     //    USE_UNIT_VECTORS_HEUR_DEFAULT
      0,     //    USE_TIGHT_POINTS_HEUR_DEFAULT
      0,     //    USE_TIGHT_RAYS_HEUR_DEFAULT
      2,     //    CUT_PRESOLVE_DEFAULT
      1000,  //    MAX_POINTS_PER_SPLIT_SPLIT_SHARE_DEFAULT
      0,     //    SUBSPACE_DEFAULT
      1,     //    ROUNDS_DEFAULT
      0,     //    BB_RUNS_DEFAULT
      0,     //    BB_STRATEGY_DEFAULT
  };

  enum class PHAActivationOption {
    skip = -1, neighbor, depth, final, mostremoved,
  };
  const std::vector<std::string> PHAActivationOptionName {
    "neighbor", "depth", "final", "mostremoved"
  };
  const std::vector<std::string> PHAActivationOptionDescription {
      "activate the first hyperplane intersected for each ray",
      "activate the hyperplane leading to best average depth of added points",
      "activate the hyperplane leading to most final points",
      "activate the hyperplane removing the most points"
  };

  enum BB_Strategy_Options {
    cbc                   = 2,
    cplex                 = 4,
    gurobi                = 8,
    user_cuts             = 16,
    all_cuts_off          = 32,
    all_cuts_on           = 64,
    gmics_off             = 128,
    gmics_on              = 256,
    presolve_off          = 512,
    presolve_on           = 1024,
    heuristics_off        = 2048,
    heuristics_on         = 4096,
    use_best_bound        = 8192,
    strong_branching_on   = 16384
  };
} /* namespace CglGICParamNamespace */

using namespace CglGICParamNamespace;

class CglGICParam : public CglParam {
  public:
    std::vector<int> paramVal; // Make this protected eventually

    /**@name Constructors and destructors */
    //@{
    /// Default constructor
    CglGICParam(
        const double inf = COIN_DBL_MAX,
        const double eps = DEFAULT_EPS,
        const double eps_coeff = DEFAULT_EPS_COEFF,
        const int max_supp_abs = DEFAULT_MAX_SUPPORT_ABS
    );

//    /// Constructor from CglParam
//    CglGICParam(const CglParam &source);

    /// Copy constructor
    CglGICParam(const CglGICParam& source);

    /// Clone
    virtual CglGICParam* clone() const;

    /// Assignment operator
    virtual CglGICParam& operator=(const CglGICParam &rhs);

    /// Destructor 
    virtual ~CglGICParam();
    //@}

    /**@name Other methods */
    //@{
    /** Check whether the parameter has a legal value */
    bool checkParam(const int paramIndex, const bool given_value = false, int value = -1) const;
    //@}

    /**@name Set/get methods */
    //@{
    /** To set parameter values */
    void setParams(const CglGICParam* const rhs = NULL);
    void setParams(const char* filename);
    bool finalizeParams(const int numNB, const int numSplits);

    /** Command line parameter values */
    virtual bool setParamVal(const int index, const int value);
    // name is assumed to have been standardized already
    virtual bool setParamVal(const std::string name, const double value);
    // name is assumed to have been standardized already
    int getParamIndex(const std::string name) const;
    inline std::string getParamName(const int index) const {return ParamName[index];}
    inline int getParamVal(const int index) const {return paramVal[index];}
    
    /** Aliases for parameter get/set method in the base class CglParam */
    /** Value for Infinity. Default: DBL_MAX */
    inline void setInf(const double value) {setINFINIT(value);}
    inline double getInf() const {return INFINIT;}
    inline double getINF() const {return INFINIT;}
    inline void setInfinity(const double value) {setINFINIT(value);}
    inline double getInfinity() const {return INFINIT;}
    inline void setINFINITY(const double value) {setINFINIT(value);}
    inline double getINFINITY() const {return INFINIT;}

    /** Epsilon for comparing numbers. Default: 1.0e-6  */
    inline void setEps(const double value) {setEPS(value);}
    inline double getEps() const {return EPS;}

    /** Epsilon for zeroing out coefficients. Default: 1.0e-5 */
    inline void setEpsCoeff(const double value) {setEPS_COEFF(value);}
    inline double getEpsCoeff() const {return EPS_COEFF;}

    /** Maximum support of the cutting planes (see max_support_rel also below). Default: INT_MAX */
    inline void setMaxSupport(const int value) {setMAX_SUPPORT(value);}
    inline int getMaxSupport() const {return MAX_SUPPORT;}
    /** Alias for consistency with our naming scheme */
    inline void setMaxSupportAbs(const int value) {setMAX_SUPPORT(value);}
    inline void setMAX_SUPPORT_ABS(const int value) {setMAX_SUPPORT(value);}
    inline int getMaxSupportAbs() const {return MAX_SUPPORT;}
    inline int getMAX_SUPPORT_ABS() const {return MAX_SUPPORT;}

    /*
     * Set the value of MAX_SUPPORT_REL, the factor contributing to the maximum support relative to the number of columns.
     * Maximum allowed support is: MAX_SUPPORT_ABS + MAX_SUPPORT_REL*ncols. Default: 0.1
     */
    virtual bool setMAX_SUPPORT_REL(const double value);
    inline double getMaxSupportRel() const {return MAX_SUPPORT_REL;}
    inline double getMAX_SUPPORT_REL() const {return MAX_SUPPORT_REL;}

    /** Set the value of RAYEPS. Default: 1e-7 */
    virtual bool setRAYEPS(const double value);
    inline double getRAYEPS() const {return RAYEPS;}
    /// Aliases
    inline bool set_rayeps(const double value) {return setRAYEPS(value);}
    inline double get_rayeps() const {return RAYEPS;}
    inline bool setRayEps(const double value) {return setRAYEPS(value);}
    inline double getRayEps() const {return RAYEPS;}

    /** Set the value of DIFFEPS. Default: 1e-3 */
    virtual bool setDIFFEPS(const double value);
    inline double getDIFFEPS() const {return DIFFEPS;}
    /// Aliases
    inline bool set_diffeps(const double value) {return setDIFFEPS(value);}
    inline double get_diffeps() const {return DIFFEPS;}
    inline bool setDiffEps(const double value) {return setDIFFEPS(value);}
    inline double getDiffEps() const {return DIFFEPS;}

    /** Set the value of AWAY. Default: 1e-3 */
    virtual bool setAWAY(const double value);
    inline double getAWAY() const {return AWAY;}
    /// Aliases
    inline bool set_away(const double value) {return setAWAY(value);}
    inline double get_away() const {return AWAY;}
    inline bool setAway(const double value) {return setAWAY(value);}
    inline double getAway() const {return AWAY;}

    /** Set the value of MINORTHOGONALITY. Default: 0.0 */
    virtual bool setMINORTHOGONALITY(const double value);
    inline double getMINORTHOGONALITY() const {return MIN_ORTHOGONALITY;}
    /// Aliases
    inline bool set_minorthogonality(const double value) {return setMINORTHOGONALITY(value);}
    inline double get_minorthogonality() const {return MIN_ORTHOGONALITY;}
    inline bool setMinOrthogonality(const double value) {return setMINORTHOGONALITY(value);}
    inline double getMinOrthogonality() const {return MIN_ORTHOGONALITY;}
    inline bool setMin_Orthogonality(const double value) {return setMINORTHOGONALITY(value);}
    inline double getMin_Orthogonality() const {return MIN_ORTHOGONALITY;}
    inline bool setMIN_ORTHOGONALITY(const double value) {return setMINORTHOGONALITY(value);}
    inline double getMIN_ORTHOGONALITY() const {return MIN_ORTHOGONALITY;}

    /** Set the value of TIMELIMT. Default: 3600 */
    virtual bool setTIMELIMIT(const double value);
    inline double getTIMELIMIT() const {return TIMELIMIT;}
    /// Aliases
    inline bool set_timelimit(const double value) {return setTIMELIMIT(value);}
    inline double get_timelimit() const {return TIMELIMIT;}
    inline bool setTimeLimit(const double value) {return setTIMELIMIT(value);}
    inline double getTimeLimit() const {return TIMELIMIT;}

    /** Set the value of CUTSOLVER_TIMELIMIT. Default: -1 */
    virtual bool setCUTSOLVERTIMELIMIT(const double value);
    inline double getCUTSOLVERTIMELIMIT() const {return CUTSOLVER_TIMELIMIT;}
    inline double getMIN_CUTSOLVER_TIMELIMIT() const {return MIN_CUTSOLVER_TIMELIMIT;}
    /// Aliases
    inline bool set_cutsolvertimelimit(const double value) {return setCUTSOLVERTIMELIMIT(value);}
    inline double get_cutsolvertimelimit() const {return CUTSOLVER_TIMELIMIT;}
    inline bool setCutSolverTimeLimit(const double value) {return setCUTSOLVERTIMELIMIT(value);}
    inline double getCutSolverTimeLimit() const {return CUTSOLVER_TIMELIMIT;}
    inline bool setCutSolver_TimeLimit(const double value) {return setCUTSOLVERTIMELIMIT(value);}
    inline double getCutSolver_TimeLimit() const {return CUTSOLVER_TIMELIMIT;}
    inline bool setCUTSOLVER_TIMELIMIT(const double value) {return setCUTSOLVERTIMELIMIT(value);}
    inline double getCUTSOLVER_TIMELIMIT() const {return CUTSOLVER_TIMELIMIT;}

    /** Set the value of PARTIAL_BB_TIMELIMIT. Default: 3600 */
    virtual bool setPARTIALBBTIMELIMIT(const double value);
    inline double getPARTIALBBTIMELIMIT() const {return PARTIAL_BB_TIMELIMIT;}
    /// Aliases
    inline bool set_partialbbtimelimit(const double value) {return setPARTIALBBTIMELIMIT(value);}
    inline double get_partialbbtimelimit() const {return PARTIAL_BB_TIMELIMIT;}
    inline bool setPartialBBTimeLimit(const double value) {return setPARTIALBBTIMELIMIT(value);}
    inline double getPartialBBTimeLimit() const {return PARTIAL_BB_TIMELIMIT;}
    inline bool setPARTIAL_BB_TIMELIMIT(const double value) {return setPARTIALBBTIMELIMIT(value);}
    inline double getPARTIAL_BB_TIMELIMIT() const {return PARTIAL_BB_TIMELIMIT;}

    /** Set the value of BB_TIMELIMIT. Default: 3600 */
    virtual bool setBBTIMELIMIT(const double value);
    inline double getBBTIMELIMIT() const {return BB_TIMELIMIT;}
    /// Aliases
    inline bool set_bbtimelimit(const double value) {return setBBTIMELIMIT(value);}
    inline double get_bbtimelimit() const {return BB_TIMELIMIT;}
    inline bool setBBTimeLimit(const double value) {return setBBTIMELIMIT(value);}
    inline double getBBTimeLimit() const {return BB_TIMELIMIT;}
    inline bool setBB_TIMELIMIT(const double value) {return setBBTIMELIMIT(value);}
    inline double getBB_TIMELIMIT() const {return BB_TIMELIMIT;}

    /**
     * Set what is considered a ``large'' upper bound on a variable's range
     */
    virtual bool setLUB(const int value);
    /** Get the value of LUB */
    inline int getLUB() const { return LUB; }

    /**
     * Set the maximum ratio between largest and smallest non zero 
     * coefficients in a cut. Default: 1e8.
     */
    virtual bool setMAXDYN(const double value);
    /** Get the value of MAXDYN */
    inline double getMAXDYN() const {return MAXDYN;}
    /// Aliases
    inline bool setMaxDyn(const double value) {return setMAXDYN(value);}
    inline double getMaxDyn() const {return MAXDYN;}

    /**
     * Set the maximum ratio between largest and smallest non zero
     * coefficients in a cut when some coeff have large ub
     */
    virtual bool setMAXDYN_LUB(const double value);
    /** Get the value of MAXDYN_LUB */
    inline double getMAXDYN_LUB() const {return MAXDYN_LUB;}

    /**
     * Set what is considered a zero coefficient for variables with large upper bound
     */
    virtual bool setEPS_COEFF_LUB(const double value);
    /** Get the value of EPS_COEFF_LUB */
    inline double getEPS_COEFF_LUB() const { return EPS_COEFF_LUB; }

    /**
     * Set maximum number of cuts we should create 
     * (overrides num_act_rounds * num_cut_gen_sets)
     */
    virtual bool setMAXCUTS(const int value);
    inline double getMAXCUTS() const {return MAXCUTS;}
    inline bool setMaxCuts(const int value) {return setMAXCUTS(value);}
    inline double getMaxCuts() const {return getMAXCUTS();}

    /**
     * Set maximum number of iterations for the cut solver
     */
    virtual bool setCUTSOLVER_MAX_ITER(const int value);
    inline double getCUTSOLVER_MAX_ITER() const {return CUTSOLVER_MAX_ITER;}
    
    /**
     * Set maximum number of iterations of the bilinear program to try
     */
    virtual bool setITER_PER_CUT_BILINEAR(const int value);
    inline int getITER_PER_CUT_BILINEAR() const {return ITER_PER_CUT_BILINEAR;}

    /**
     * Set maximum number of pivots from optimum (for lifting)
     */
    virtual bool setMAX_PIVOTS(const int value);
    inline double getMAX_PIVOTS() const {return MAX_PIVOTS;}
    inline bool setMaxPivots(const int value) {return setMAX_PIVOTS(value);}
    inline double getMaxPivots() const {return getMAX_PIVOTS();}

    /**
     * Set the value of MIN_VIOL_ABS, the minimum violation for the current
     * basic solution in a generated cut. Default: 1e-7 
     **/
    virtual bool setMIN_VIOL_ABS(const double value);
    /** Get the value of MIN_VIOL_ABS */
    inline double getMIN_VIOL_ABS() const {return MIN_VIOL_ABS;}
    /// Aliases
    inline bool setMinViolAbs(const double value) {return setMIN_VIOL_ABS(value);}
    inline double getMinViolAbs() const {return MIN_VIOL_ABS;}

    /**
     * Set the value of MIN_VIOL_REL, the minimum violation for the current
     * basic solution in a generated cut. Default: 1e-7 
     **/
    virtual bool setMIN_VIOL_REL(const double value);
    /** Get the value of MIN_VIOL_REL */
    inline double getMIN_VIOL_REL() const {return MIN_VIOL_REL;}
    /// Aliases
    inline bool setMinViolRel(const double value) {return setMIN_VIOL_REL(value);}
    inline double getMinViolRel() const {return MIN_VIOL_REL;}

    /** Set the value of CHECK_DUPLICATES. Default: 0 */
    virtual void setCHECK_DUPLICATES(const bool value);
    /** Get the value of CHECK_DUPLICATES */
    inline bool getCHECK_DUPLICATES() const {return CHECK_DUPLICATES;}
    /// Aliases
    inline void setCheckDuplicates(bool value) {setCHECK_DUPLICATES(value);}
    inline bool getCheckDuplicates() const {return CHECK_DUPLICATES;}

    /** Set the value of EXIT_ON_INFEAS. Default: 0 */
    virtual void setEXIT_ON_INFEAS(const bool value);
    /** Get the value of EXIT_ON_INFEAS */
    inline bool getEXIT_ON_INFEAS() const {return EXIT_ON_INFEAS;}
    /// Aliases
    inline void setExitOnInfeas(bool value) {setEXIT_ON_INFEAS(value);}
    inline bool getExitOnInfeas() const {return EXIT_ON_INFEAS;}

    /** Set the value of TEMP. Default: 0 */
    virtual void setTEMP(const int value);
    /** Get the value of EXIT_ON_INFEAS */
    inline int getTEMP() const {return TEMP;}
    /// Aliases
    inline void setTemp(int value) {setTEMP(value);}
    inline int getTemp() const {return TEMP;}
    //@}

    void printParams(FILE* outfile = stdout, const char SEP = ' ') const;

    int getMaxNumCgs(const int numSplits, const int limitOnNumCgs) const;

    inline int getMaxNumCgs(const int numSplits) const {
      return getMaxNumCgs(numSplits, 0);
    }

  protected:
    /**@name Parameters */

    //@{
    // Instance parameters

    /**
     * Maximum allowed support is: MAX_SUPPORT_ABS + MAX_SUPPORT_REL*ncols.
     * Default: 0.1
     */
    double MAX_SUPPORT_REL;

    /** 
     * Value to determine whether a ray is parallel to a hyperplane
     * Default: 1e-7 
     */
    double RAYEPS;

    /** 
     * A more conservative value of epsilon, meaning larger epsilon
     * Default: 1e-3
     */
    double DIFFEPS;
    //double PIVOTEPS;
    double AWAY;
    double MIN_ORTHOGONALITY;

    /**
     * Maximum total time
     */
    double TIMELIMIT;
    double PARTIAL_BB_TIMELIMIT;
    double BB_TIMELIMIT;
    double CUTSOLVER_TIMELIMIT;

    /**
     * If the absolute value of the upper bound on a variable is larger than this, it is considered large
     */
    int LUB; 

    /**
     * Maximum ratio between largest and smallest non zero
     * coefficients in a cut. Default: 1e8.
     */
    double MAXDYN;

    /**
     * Same as MAXDYN but when some of the variables involved in the cut have a large upper bound; should be >= MAXDYN logically
     */
    double MAXDYN_LUB; 

    /**
     * Similar to EPS_COEFF for variables having a large upper bound
     */
    double EPS_COEFF_LUB; 

    /**
     * Maximum total number of cuts to create
     */
    int MAXCUTS;

    /**
     * Maximum number of iterations for the cut solver
     */
    int CUTSOLVER_MAX_ITER;

    /**
     * Maximum number of iterations of the bilinear cut generation program
     */
    int ITER_PER_CUT_BILINEAR;

    /**
     * Maximum number of pivots from the optimal solution
     */
    int MAX_PIVOTS;

    /** 
     * Minimum absolute violation for the current basic solution in a generated cut.
     * Default: 1e-7. 
     */
    double MIN_VIOL_ABS;

    /** 
     * Minimum relative violation for the current basic solution in a generated cut.
     * Default: 1e-7. 
     */
    double MIN_VIOL_REL;

    /** Check for duplicates when adding the cut to the collection? */
    bool CHECK_DUPLICATES;

    /** Exit if adding GICs make problem infeasible */
    bool EXIT_ON_INFEAS;

    /**
     * A temporary option to be used for debugging or test
     */
    int TEMP;
    //@}

    // Default values
    // These are defined in CglParam
    static constexpr double  DEFAULT_EPS = 1e-7;
    static constexpr double  DEFAULT_EPS_COEFF = 1e-5;  // Any cut coefficient smaller than that for a variable that does not have a large upper bound is replaced by zero
    static constexpr double  DEFAULT_INFINITY = std::numeric_limits<double>::max(); //1E20;
    static constexpr int     DEFAULT_MAX_SUPPORT_ABS = std::numeric_limits<int>::max();
    static constexpr double  DEFAULT_MAX_SUPPORT_REL = 0.9;

    // The following are specific for point cuts
    static constexpr double  DEFAULT_RAYEPS = 1e-7;
    static constexpr double  DEFAULT_DIFFEPS = 1e-3;
    static constexpr double  DEFAULT_AWAY = 1e-3;
    static constexpr double  DEFAULT_MIN_ORTHOGONALITY = 0.000; //1e-3

    static constexpr double  DEFAULT_TIMELIMIT = 3600; //10800;  // 3 hours in seconds
    static constexpr double  DEFAULT_PARTIAL_BB_TIMELIMIT = 3600;
    static constexpr double  DEFAULT_BB_TIMELIMIT = 3600;
    static constexpr double  DEFAULT_CUTSOLVER_TIMELIMIT = -1.; // -1 = unlimited time for initial solve, and then half that time subsequently; -2 = unlimited time always
    static constexpr double  MIN_CUTSOLVER_TIMELIMIT = 5.;

    //static constexpr double  DEFAULT_EPS_PIVOT = 1E-1;
    // For cut safety
    static constexpr int     DEFAULT_LUB = 1e3;  // If the absolute value of the upper bound on a variable is larger than that, it is considered large
    static constexpr double  DEFAULT_MAXDYN = 1e8;  // |alpha_max| / |alpha_min| upper bound; Maximum ratio between largest and smallest non zero coefficients in a cut
    static constexpr double  DEFAULT_MAXDYN_LUB = 1e13;  // Same as MAXDYN but when some of the variables involved in the cut have a large upper bound; should be >= MAXDYN logically
    static constexpr double  DEFAULT_EPS_COEFF_LUB = 1e-13;  // Similar to EPS_COEFF for variables having a large upper bound

    static constexpr int     DEFAULT_MAXCUTS = 500;
    static constexpr int     DEFAULT_CUTSOLVER_MAX_ITER = std::numeric_limits<int>::max();
    static constexpr int     DEFAULT_ITER_PER_CUT_BILINEAR = 1;
    static constexpr int     DEFAULT_MAX_PIVOTS = 0;

    static constexpr double  DEFAULT_MIN_VIOL_ABS = 1E-7;
    static constexpr double  DEFAULT_MIN_VIOL_REL = 1E-7;
    static constexpr bool    DEFAULT_CHECK_DUPLICATES = true;
    static constexpr bool    DEFAULT_EXIT_ON_INFEAS = false;

    static constexpr double  DEFAULT_TEMP = 0.;
};
