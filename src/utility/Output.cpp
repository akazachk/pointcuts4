//============================================================================
// Name        : Output.cpp
// Author      : akazachk
// Version     : 0.2013.08.15
// Copyright   : Your copyright notice
// Description : 
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

//#include <iostream>
#include <fstream>
#include <cmath> // isnan
#include <algorithm> // For max_element, min_element
#include "Utility.hpp"
#include "Output.hpp"

//class formatted_output
//{
//    private:
//        int width;
//        ofstream& stream_obj;
//
//    public:
//        formatted_output(ofstream& obj, int w): width(w), stream_obj(obj) {}
//
//        template<typename T>
//        formatted_output& operator<<(const T& output)
//        {
//            stream_obj.width(width);
//            stream_obj << output;
//
//            return *this;
//        }
//
//        formatted_output& operator<<(ofstream& (*func)(ofstream&))
//        {
//            func(stream_obj);
//            return *this;
//        }
//};

/*************/
/* VARIABLES */
/*************/
const int countParamInfoEntries = ParamIndices::NUM_PARAMS + 7; // +1 for ortho, +1 for eps, +1 for rayeps, +4 for total time / partial bb time / bb time / cutsolver time
const int countLPRelaxEntries = 24;
const int countSubEntries = 7;
//const int countCompLSIC_OSICEntries = 28;
const int countGICPointInfoEntries = 27;
//const int countCompSGIC_SSICEntries = 21;
//const int countCompLGIC_LSIC_OSICEntries = 19;
const int countRootBoundEntries = 36 + 12;
const int countCgsInfoEntries = 21;
const int countCutInfoEntries = 11 + 3 * CutHeuristics::NUM_CUT_HEUR;
const int countObjInfoEntries = 1 + CutHeuristics::NUM_CUT_HEUR;
const int countFailInfoEntries = 1 + CutSolverFails::NUM_CUTSOLVER_FAILS;
const int countRoundInfoEntries = 46;
const int countBBInfoEntries = (int) BB_INFO_CONTENTS.size() * 4 * 2;
//const int countCondNumEntries = 0;
//const int countTimeEntries = 15;

/***********************************************************************/
/**
 * @brief Adds the header lines to the file
 */
void writeHeaderToLog(FILE *myfile) {
//#ifdef TRACE
//  printf(
//      "\n############### Starting writeHeaderToII routine. ###############\n");
//#endif
  if (myfile == NULL)
    return;
  std::string tmpstring = "";
  fprintf(myfile, "%c", SEP);
  fprintf(myfile, "%s", "PARAM INFO");
  tmpstring.assign(countParamInfoEntries, SEP);
  fprintf(myfile, "%s", tmpstring.c_str());
  fprintf(myfile, "%s", "ORIG PROB");
  tmpstring.assign(countLPRelaxEntries, SEP);
  fprintf(myfile, "%s", tmpstring.c_str());
  if (param.paramVal[SUBSPACE_PARAM_IND]) {
    fprintf(myfile, "%s", "SUB PROB");
    tmpstring.assign(countSubEntries, SEP);
    fprintf(myfile, "%s", tmpstring.c_str());
  }
  /*
   fprintf(myfile, "%s", "COMP LSICS OSICS");
   tmpstring.assign(countCompLSIC_OSICEntries, SEP);
   fprintf(myfile, "%s", tmpstring.c_str());
   */
  if (param.paramVal[ParamIndices::PHA_PARAM_IND]) {
    fprintf(myfile, "%s", "GIC POINT INFO");
    tmpstring.assign(countGICPointInfoEntries, SEP);
    fprintf(myfile, "%s", tmpstring.c_str());
  }
  fprintf(myfile, "%s", "CGS INFO");
  tmpstring.assign(countCgsInfoEntries, SEP);
  fprintf(myfile, "%s", tmpstring.c_str());
  fprintf(myfile, "%s", "CUT INFO");
  tmpstring.assign(countCutInfoEntries, SEP);
  fprintf(myfile, "%s", tmpstring.c_str());
  fprintf(myfile, "%s", "OBJ INFO");
  tmpstring.assign(countObjInfoEntries, SEP);
  fprintf(myfile, "%s", tmpstring.c_str());
  fprintf(myfile, "%s", "FAIL INFO");
  tmpstring.assign(countFailInfoEntries, SEP);
  fprintf(myfile, "%s", tmpstring.c_str());
  /*
   fprintf(myfile, "%s", "COMP SGICS SSICS");
   tmpstring.assign(countCompSGIC_SSICEntries, SEP);
   fprintf(myfile, "%s", tmpstring.c_str());
   fprintf(myfile, "%s", "COMP LGICS LSICS OSICS");
   tmpstring.assign(countCompLGIC_LSIC_OSICEntries, SEP);
   fprintf(myfile, "%s", tmpstring.c_str());
   */
  fprintf(myfile, "%s", "ROOT BOUND");
  tmpstring.assign(countRootBoundEntries, SEP);
  fprintf(myfile, "%s", tmpstring.c_str());
  fprintf(myfile, "%s", "ROUND INFO");
  tmpstring.assign(countRoundInfoEntries, SEP);
  fprintf(myfile, "%s", tmpstring.c_str());
  fprintf(myfile, "%s", "BB INFO");
  tmpstring.assign(countBBInfoEntries, SEP);
  fprintf(myfile, "%s", tmpstring.c_str());
  /*
   fprintf(myfile, "%s", "COND NUM");
   tmpstring.assign(countCondNumEntries, SEP);
   fprintf(myfile, "%s", tmpstring.c_str());
   */
  fprintf(myfile, "%s", "TIME INFO");
  tmpstring.assign(GlobalVariables::time_name.size(), SEP);
  fprintf(myfile, "%s", tmpstring.c_str());
  fprintf(myfile, "%s%c", "END", SEP);
  fprintf(myfile, "\n");

  fprintf(myfile, "%s%c", "INSTANCE", SEP);
  ////////////////////// PARAM INFO
  for (int param_ind = 0; param_ind < ParamIndices::NUM_PARAMS; param_ind++) {
    fprintf(myfile, "%s%c", ParamName[param_ind].c_str(), SEP);
  }
  fprintf(myfile, "%s%c", "MIN_ORTHOGONALITY", SEP);
  fprintf(myfile, "%s%c", "EPS", SEP);
  fprintf(myfile, "%s%c", "RAYEPS", SEP);
  fprintf(myfile, "%s%c", "TIMELIMIT", SEP);
  fprintf(myfile, "%s%c", "PARTIAL_BB_TIMELIMIT", SEP);
  fprintf(myfile, "%s%c", "BB_TIMELIMIT", SEP);
  fprintf(myfile, "%s%c", "CUTSOLVER_TIMELIMIT", SEP);
  ////////////////////// ORIG PROB
  fprintf(myfile, "%s%c", "NUM ROWS", SEP); // 1
  fprintf(myfile, "%s%c", "NUM COLS", SEP); // 2
  fprintf(myfile, "%s%c", "NUM FIXED", SEP); // 21
  fprintf(myfile, "%s%c", "NUM EQ ROWS", SEP); // 3
  fprintf(myfile, "%s%c", "NUM INEQ ROWS", SEP); // 4
  fprintf(myfile, "%s%c", "NUM BOUND ROWS", SEP); // 5
  fprintf(myfile, "%s%c", "NUM ASSIGN ROWS", SEP); // 6
  fprintf(myfile, "%s%c", "INTEGERS NON-BIN", SEP); // 7
  fprintf(myfile, "%s%c", "BINARIES", SEP); // 8
  fprintf(myfile, "%s%c", "CONTINUOUS", SEP); // 9
  fprintf(myfile, "%s%c", "NUM BASIC", SEP); // 10
  fprintf(myfile, "%s%c", "NUM NON-BASIC", SEP); // 11
//  fprintf(myfile, "%s%c", "NUM ORIG BASIC", SEP);
  fprintf(myfile, "%s%c", "NUM ORIG NON-BASIC", SEP); // 13
  fprintf(myfile, "%s%c", "NUM PRIMAL DEGEN", SEP); // 19
  fprintf(myfile, "%s%c", "NUM DUAL DEGEN", SEP); // 20 // 21 above
  fprintf(myfile, "%s%c", "FRAC CORE", SEP); // 14
  fprintf(myfile, "%s%c", "A NONZERO", SEP); // 15
  fprintf(myfile, "%s%c", "A-DENSITY", SEP); // 16
  fprintf(myfile, "%s%c", "MIN A COEFF", SEP); // 22
  fprintf(myfile, "%s%c", "MAX A COEFF", SEP); // 23 // 24 below
  fprintf(myfile, "%s%c", "MIN RAYVAL NO EPS", SEP); // 12
  fprintf(myfile, "%s%c", "MIN RAYVAL", SEP); // 17
  fprintf(myfile, "%s%c", "LP OBJ", SEP); // 18 // 19 above
  fprintf(myfile, "%s%c", "IP OBJ", SEP); // 24
  if (param.paramVal[SUBSPACE_PARAM_IND] > 0) {
    ////////////////////// SUB PROB
    fprintf(myfile, "%s%c", "SUBROWS", SEP); // 1
    fprintf(myfile, "%s%c", "SUBCOLS", SEP); // 2
    fprintf(myfile, "%s%c", "SUB LP OBJ", SEP); // 3
    fprintf(myfile, "%s%c", "SUB FRAC CORE", SEP); // 4
    fprintf(myfile, "%s%c", "% 0 FEAS SPLIT SIDES", SEP); // 5
    fprintf(myfile, "%s%c", "% 1 FEAS SPLIT SIDE", SEP); // 6
    fprintf(myfile, "%s%c", "% 2 FEAS SPLIT SIDES", SEP); // 7
  }
//  ////////////////////// COMP LSICS TO OSICS
//  fprintf(myfile, "%s%c", "NUM TOTAL SUB SICS", SEP); // 1
//  fprintf(myfile, "%s%c", "NUM FEAS SUB SICS", SEP); // 2
//  fprintf(myfile, "%s%c", "AVG NUM PIVOTS AVAILABLE", SEP); // 3
//  fprintf(myfile, "%s%c", "NUM LSICS", SEP); // 4
//  fprintf(myfile, "%s%c", "% INIT LSIC EUCL > OSIC EUCL", SEP); // 5
//  fprintf(myfile, "%s%c", "% INIT LSIC EUCL = OSIC EUCL", SEP); // 6
//  fprintf(myfile, "%s%c", "% INIT LSIC EUCL < OSIC EUCL", SEP); // 7
//  fprintf(myfile, "%s%c", "% BEST LSIC EUCL > OSIC EUCL", SEP); // 8
//  fprintf(myfile, "%s%c", "% BEST LSIC EUCL = OSIC EUCL", SEP); // 9
//  fprintf(myfile, "%s%c", "% BEST LSIC EUCL < OSIC EUCL", SEP); // 10
//  fprintf(myfile, "%s%c", "% INIT LSIC OBJ > ORIG SIC OBJ", SEP); // 11
//  fprintf(myfile, "%s%c", "% INIT LSIC OBJ = ORIG SIC OBJ", SEP); // 12
//  fprintf(myfile, "%s%c", "% INIT LSIC OBJ < ORIG SIC OBJ", SEP); // 13
//  fprintf(myfile, "%s%c", "% BEST LSIC OBJ > ORIG SIC OBJ", SEP); // 14
//  fprintf(myfile, "%s%c", "% BEST LSIC OBJ = ORIG SIC OBJ", SEP); // 15
//  fprintf(myfile, "%s%c", "% BEST LSIC OBJ < ORIG SIC OBJ", SEP); // 16
//  fprintf(myfile, "%s%c", "OSIC BOUND", SEP); // 17
//  fprintf(myfile, "%s%c", "INIT LSIC BOUND", SEP); // 18
//  fprintf(myfile, "%s%c", "ALL LSIC BOUND", SEP); // 19
//  fprintf(myfile, "%s%c", "INIT LSIC + OSIC BOUND", SEP); // 20
//  fprintf(myfile, "%s%c", "ALL LSIC + OSIC BOUND", SEP); // 21
//  fprintf(myfile, "%s%c", "IMPROVE OVER OSICS", SEP); // 22
//  fprintf(myfile, "%s%c", "IMPROVE ALL LSICS", SEP); // 23
//  fprintf(myfile, "%s%c", "OSIC BASIS COND", SEP); // 24
//  fprintf(myfile, "%s%c", "INIT LSIC BASIS COND", SEP); // 25
//  fprintf(myfile, "%s%c", "ALL LSIC BASIS COND", SEP); // 26
//  fprintf(myfile, "%s%c", "INIT LSIC + OSIC BASIS COND", SEP); // 27
//  fprintf(myfile, "%s%c", "ALL LSIC + OSIC BASIS COND", SEP); // 28
  if (param.paramVal[ParamIndices::PHA_PARAM_IND] > 0) {
    ////////////////////// GIC POINT INFO
    fprintf(myfile, "%s%c", "HPLANE HEUR", SEP); // 1
    fprintf(myfile, "%s%c", "NEXT HPLANE FINAL", SEP); // 27
    fprintf(myfile, "%s%c", "NUM HPLANES PER RAY", SEP); // 2
    fprintf(myfile, "%s%c", "NUM RAYS C1", SEP); // 19
    fprintf(myfile, "%s%c", "AVG NUM PARALLEL RAYS C1", SEP); // 26
    fprintf(myfile, "%s%c", "AVG NUM SIC FINAL POINTS", SEP); // 18
    fprintf(myfile, "%s%c", "NUM RAYS TO CUT", SEP); // 3
    fprintf(myfile, "%s%c", "NUM CUT GEN SETS", SEP); // 4
    fprintf(myfile, "%s%c", "NUM CUTS PER SPLIT", SEP); // 5
    fprintf(myfile, "%s%c", "NUM POINTS (TOTAL)", SEP); // 6
    fprintf(myfile, "%s%c", "NUM POINTS (AVG)", SEP); // 7
    fprintf(myfile, "%s%c", "NUM POINTS (MIN)", SEP); // 8
    fprintf(myfile, "%s%c", "NUM POINTS (MAX)", SEP); // 9
    fprintf(myfile, "%s%c", "NUM FINAL POINTS (TOTAL)", SEP); // 10
    fprintf(myfile, "%s%c", "NUM FINAL POINTS (AVG)", SEP); // 11
    fprintf(myfile, "%s%c", "NUM FINAL POINTS (MIN)", SEP); // 12
    fprintf(myfile, "%s%c", "NUM FINAL POINTS (MAX)", SEP); // 13
    fprintf(myfile, "%s%c", "NUM RAYS (TOTAL)", SEP); // 14
    fprintf(myfile, "%s%c", "NUM RAYS (AVG)", SEP); // 15
    fprintf(myfile, "%s%c", "NUM RAYS (MIN)", SEP); // 16
    fprintf(myfile, "%s%c", "NUM RAYS (MAX)", SEP); // 17
    fprintf(myfile, "%s%c", "SIC DEPTH POINTS (AVG)", SEP); // 20
    fprintf(myfile, "%s%c", "SIC DEPTH POINTS (MIN)", SEP); // 21
    fprintf(myfile, "%s%c", "SIC DEPTH POINTS (MAX)", SEP); // 22
    fprintf(myfile, "%s%c", "OBJ DEPTH POINTS (AVG)", SEP); // 23
    fprintf(myfile, "%s%c", "OBJ DEPTH POINTS (MIN)", SEP); // 24
    fprintf(myfile, "%s%c", "OBJ DEPTH POINTS (MAX)", SEP); // 25 // 26 27 above
    //18
  }
  //////////////////// CGS INFO
  fprintf(myfile, "%s%c", "NUM CGS USED", SEP); // 1
  fprintf(myfile, "%s%c", "NUM CGS LEADING TO CUTS", SEP); // 2
  fprintf(myfile, "%s%c", "NUM CGS PR-FEAS", SEP); // 3
  fprintf(myfile, "%s%c", "NUM CGS 1FACET INFEAS", SEP); // 4
  fprintf(myfile, "%s%c", "NUM DISJ TERMS", SEP); // 5
  fprintf(myfile, "%s%c", "MIN DENSITY PRLP", SEP); // 18
  fprintf(myfile, "%s%c", "MAX DENSITY PRLP", SEP); // 19 // 20 below
  fprintf(myfile, "%s%c", "MIN NUM ROWS PRLP", SEP); // 6
  fprintf(myfile, "%s%c", "MAX NUM ROWS PRLP", SEP); // 7
  fprintf(myfile, "%s%c", "MIN NUM COLS PRLP", SEP); // 20
  fprintf(myfile, "%s%c", "MAX NUM COLS PRLP", SEP); // 21 *
  fprintf(myfile, "%s%c", "MIN NUM POINTS PRLP", SEP); // 8
  fprintf(myfile, "%s%c", "MAX NUM POINTS PRLP", SEP); // 9
  fprintf(myfile, "%s%c", "TOTAL NUM POINTS PRLP", SEP); // 10
  fprintf(myfile, "%s%c", "MIN NUM RAYS PRLP", SEP); // 11
  fprintf(myfile, "%s%c", "MAX NUM RAYS PRLP", SEP); // 12
  fprintf(myfile, "%s%c", "TOTAL NUM RAYS PRLP", SEP); // 13
  fprintf(myfile, "%s%c", "NUM PARTIAL BB NODES", SEP); // 14
  fprintf(myfile, "%s%c", "NUM PRUNED NODES", SEP); // 15
  fprintf(myfile, "%s%c", "MIN DEPTH", SEP); // 16
  fprintf(myfile, "%s%c", "MAX DEPTH", SEP); // 17 // 18 above
  //////////////////// CUT INFO
  fprintf(myfile, "%s%c", "TOTAL NUM CUTS", SEP); // 1
  fprintf(myfile, "%s%c", "MIN SUPPORT GOMORY", SEP); // 9
  fprintf(myfile, "%s%c", "MAX SUPPORT GOMORY", SEP); // 10
  fprintf(myfile, "%s%c", "AVG SUPPORT GOMORY", SEP); // 11
  fprintf(myfile, "%s%c", "STDEV SUPPORT GOMORY", SEP); // 12 *
  fprintf(myfile, "%s%c", "MIN SUPPORT NON-GOMORY", SEP); // 2
  fprintf(myfile, "%s%c", "MAX SUPPORT NON-GOMORY", SEP); // 3
  fprintf(myfile, "%s%c", "AVG SUPPORT NON-GOMORY", SEP); // 4
  fprintf(myfile, "%s%c", "STDEV SUPPORT NON-GOMORY", SEP); // 5
  fprintf(myfile, "%s%c", "MIN ORTHO WITH OBJ", SEP); // 6
  fprintf(myfile, "%s%c", "MAX ORTHO WITH OBJ", SEP); // 7
  fprintf(myfile, "%s%c", "AVG ORTHO WITH OBJ", SEP); // 8 // 9 above
  // Cut heuristic stats
  for (int heur_ind = 0; heur_ind < CutHeuristics::NUM_CUT_HEUR; heur_ind++) { // 3 * # cutHeur
    fprintf(myfile, "%s%c", CutHeuristicsName[heur_ind].c_str(), SEP);
    fprintf(myfile, "ACTIVE (SICSOLVER) %s%c", CutHeuristicsName[heur_ind].c_str(),
        SEP);
    fprintf(myfile, "ACTIVE (GMISOLVER) %s%c", CutHeuristicsName[heur_ind].c_str(),
            SEP);
  }
  //  fprintf(myfile, "%s%c", "CUT VERT HEUR", SEP);
  //  fprintf(myfile, "%s%c", "DUMMY OBJ HEUR", SEP);
  //  fprintf(myfile, "%s%c", "ITER BIL HEUR", SEP);
  //  fprintf(myfile, "%s%c", "NB RAY DIRN HEUR", SEP);
  //  fprintf(myfile, "%s%c", "SPLIT SHARE HEUR", SEP);
  // OBJ INFO
  fprintf(myfile, "%s%c", "NUM OBJ", SEP); // 6
  for (int heur_ind = 0; heur_ind < CutHeuristics::NUM_CUT_HEUR; heur_ind++) { // 1 * # cutHeur
    fprintf(myfile, "OBJ %s%c", CutHeuristicsName[heur_ind].c_str(), SEP);
  }
  // FAIL INFO
  fprintf(myfile, "%s%c", "NUM FAILS", SEP); // 7
  for (int fail_ind = 0; fail_ind < CutSolverFails::NUM_CUTSOLVER_FAILS;
      fail_ind++) {
    fprintf(myfile, "%s%c", CutSolverFailName[fail_ind].c_str(), SEP);
  }
  //  fprintf(myfile, "%s%c", "DUAL CUT SOLVER FAILS", SEP);
  //  fprintf(myfile, "%s%c", "DUP SIC FAILS", SEP);
  //  fprintf(myfile, "%s%c", "ITERATION FAILS", SEP);
  //  fprintf(myfile, "%s%c", "NON CUTTING FAILS", SEP);
  //  fprintf(myfile, "%s%c", "PRIMAL CUT SOLVER FAILS", SEP);
  //  fprintf(myfile, "%s%c", "UNSPEC FAILS", SEP);
  /*
  //////////////////// COMP SGICS SSICS
  fprintf(myfile, "%s%c", "NUM FEAS SUB SICS", SEP); // 1
  fprintf(myfile, "%s%c", "NUM SPLITS - (SPLITS: 1 GIC = SIC)", SEP); // 2 This is the number of `successful' sub GIC splits
  fprintf(myfile, "%s%c", "NUM SGICS", SEP); // 3
  fprintf(myfile, "%s%c", "NUM ROUNDS", SEP); // 4
   // fprintf(myfile, "%s%c", "NUM TIMES SUB SIC GENERATED", SEP); // 5
   fprintf(myfile, "%s%c", "AVG GICS PER SPLIT", SEP); // 6
   fprintf(myfile, "%s%c", "AVG GICS PER SPLIT (EXCEPT 1 GIC = SIC)", SEP); // 7
   fprintf(myfile, "%s%c", "% SUB GIC EUCL > SUB SIC EUCL", SEP); // 8
   fprintf(myfile, "%s%c", "% SUB GIC EUCL = SUB SIC EUCL", SEP); // 9
   fprintf(myfile, "%s%c", "% SUB GIC EUCL < SUB SIC EUCL", SEP); // 10
   fprintf(myfile, "%s%c", "(>) GIC - SIC EUCL (MIN)", SEP); // 11
   fprintf(myfile, "%s%c", "(>) GIC - SIC EUCL (MAX)", SEP); // 12
   fprintf(myfile, "%s%c", "(>) GIC - SIC EUCL (AVG)", SEP); // 13
   fprintf(myfile, "%s%c", "(<) GIC - SIC EUCL (MIN)", SEP); // 14
   fprintf(myfile, "%s%c", "(<) GIC - SIC EUCL (MAX)", SEP); // 15
   fprintf(myfile, "%s%c", "(<) GIC - SIC EUCL (AVG)", SEP); // 16
   fprintf(myfile, "%s%c", "BOUND POST ALL SUB GICS", SEP); // 17
   fprintf(myfile, "%s%c", "BASIS COND NUM POST ALL SUB GICS", SEP); // 18
   fprintf(myfile, "%s%c", "BOUND POST ALL SUB SICS", SEP); // 19
   fprintf(myfile, "%s%c", "BASIS COND NUM POST ALL SUB SICS", SEP); // 20
   fprintf(myfile, "%s%c", "BOUND POST ALL SUB SICS AND SUB GICS", SEP); // 21
   fprintf(myfile, "%s%c", "BASIS COND NUM POST ALL SUB SICS AND SUB GICS",
   SEP); // 22
   ////////////////////// COMP LGICS LSICS OSICS
   fprintf(myfile, "%s%c", "NUM FEAS SSICS", SEP); // 1
   fprintf(myfile, "%s%c", "NUM SGICS", SEP); // 2
   fprintf(myfile, "%s%c", "AVG NUM LSICS PIVOTS AVAILABLE", SEP); // 3
   fprintf(myfile, "%s%c", "AVG NUM LGICS PIVOTS AVAILABLE", SEP); // 4
   fprintf(myfile, "%s%c", "NUM LSICS", SEP); // 5
   fprintf(myfile, "%s%c", "NUM LGICS", SEP); // 6
   fprintf(myfile, "%s%c", "NUM OSICS", SEP); // 7
   fprintf(myfile, "%s%c", "% INIT LGIC EUCL > INIT LSIC EUCL", SEP); // 8
   fprintf(myfile, "%s%c", "% INIT LGIC EUCL = INIT LSIC EUCL", SEP); // 9
   fprintf(myfile, "%s%c", "% INIT LGIC EUCL < INIT LSIC EUCL", SEP); // 10
   fprintf(myfile, "%s%c", "% BEST LGIC EUCL > BEST LSIC EUCL", SEP); // 11
   fprintf(myfile, "%s%c", "% BEST LGIC EUCL = BEST LSIC EUCL", SEP); // 12
   fprintf(myfile, "%s%c", "% BEST LGIC EUCL < BEST LSIC EUCL", SEP); // 13
   fprintf(myfile, "%s%c", "% INIT LGIC EUCL > OSIC EUCL", SEP); // 14
   fprintf(myfile, "%s%c", "% INIT LGIC EUCL = OSIC EUCL", SEP); // 15
   fprintf(myfile, "%s%c", "% INIT LGIC EUCL < OSIC EUCL", SEP); // 16
   fprintf(myfile, "%s%c", "% BEST LGIC EUCL > OSIC EUCL", SEP); // 17
   fprintf(myfile, "%s%c", "% BEST LGIC EUCL = OSIC EUCL", SEP); // 18
   fprintf(myfile, "%s%c", "% BEST LGIC EUCL < OSIC EUCL", SEP); // 19
   */
  ////////////////////// ROOT NODE BOUNDS
  fprintf(myfile, "%s%c", "INIT BOUND", SEP); // 1
  fprintf(myfile, "%s%c", "STRONG BRANCHING LB", SEP); // 35
  fprintf(myfile, "%s%c", "STRONG BRANCHING UB", SEP); // 36
  fprintf(myfile, "%s%c", "NUM SIC", SEP); // 2
  fprintf(myfile, "%s%c", "SIC BOUND", SEP); // 3
  fprintf(myfile, "%s%c", "NUM GMI", SEP); // 18
  fprintf(myfile, "%s%c", "GMI BOUND", SEP); // 19
  fprintf(myfile, "%s%c", "NUM LANDP", SEP); // 31
  fprintf(myfile, "%s%c", "LANDP BOUND", SEP); // 32
  fprintf(myfile, "%s%c", "NUM TILTED", SEP); // 4
  fprintf(myfile, "%s%c", "TILTED BOUND", SEP); // 5
  fprintf(myfile, "%s%c", "NUM VPC", SEP); // 6
  fprintf(myfile, "%s%c", "VPC BOUND", SEP); // 7
  fprintf(myfile, "%s%c", "NUM PHA", SEP); // 8
  fprintf(myfile, "%s%c", "PHA BOUND", SEP); // 9
  fprintf(myfile, "%s%c", "SIC+TILTED BOUND", SEP); // 27
  fprintf(myfile, "%s%c", "GMI+TILTED BOUND", SEP); // 28
  fprintf(myfile, "%s%c", "SIC+VPC BOUND", SEP); // 29
  fprintf(myfile, "%s%c", "GMI+VPC BOUND", SEP); // 30 // 31 above
  fprintf(myfile, "%s%c", "SIC+PHA BOUND", SEP); // 33 // 32 above
  fprintf(myfile, "%s%c", "GMI+PHA BOUND", SEP); // 34 // 35 above
  fprintf(myfile, "%s%c", "ALL BUT GOMORY", SEP); // 10
  fprintf(myfile, "%s%c", "ALL CUTS (WITH SIC)", SEP); // 11
  fprintf(myfile, "%s%c", "ALL CUTS (WITH GMI)", SEP); // 20
  fprintf(myfile, "%s%c", "GIC OVER SIC", SEP); // 12
  fprintf(myfile, "%s%c", "ALL OVER SIC", SEP); // 13
  fprintf(myfile, "%s%c", "GIC OVER GMI", SEP); // 21
  fprintf(myfile, "%s%c", "ALL OVER GMI", SEP); // 22
  fprintf(myfile, "%s%c", "ACTIVE SIC SICSOLVER", SEP); // 14
  fprintf(myfile, "%s%c", "ACTIVE TILTED SICSOLVER", SEP); // 15
  fprintf(myfile, "%s%c", "ACTIVE VPC SICSOLVER", SEP); // 16
  fprintf(myfile, "%s%c", "ACTIVE PHA SICSOLVER", SEP); // 17
  fprintf(myfile, "%s%c", "ACTIVE GMI GMISOLVER", SEP); // 23
  fprintf(myfile, "%s%c", "ACTIVE TILTED GMISOLVER", SEP); // 24
  fprintf(myfile, "%s%c", "ACTIVE VPC GMISOLVER", SEP); // 25
  fprintf(myfile, "%s%c", "ACTIVE PHA GMISOLVER", SEP); // 26 // 27 above
  fprintf(myfile, "%s%c", "SIC % GAP CLOSED", SEP); // 37
  fprintf(myfile, "%s%c", "GMI % GAP CLOSED", SEP); // 38
  fprintf(myfile, "%s%c", "LANDP % GAP CLOSED", SEP); // 39
  fprintf(myfile, "%s%c", "TILTED % GAP CLOSED", SEP); // 40
  fprintf(myfile, "%s%c", "VPC % GAP CLOSED", SEP); // 41
  fprintf(myfile, "%s%c", "PHA % GAP CLOSED", SEP); // 42
  fprintf(myfile, "%s%c", "SIC+TILTED % GAP CLOSED", SEP); // 43
  fprintf(myfile, "%s%c", "GMI+TILTED % GAP CLOSED", SEP); // 44
  fprintf(myfile, "%s%c", "SIC+VPC % GAP CLOSED", SEP); // 45
  fprintf(myfile, "%s%c", "GMI+VPC % GAP CLOSED", SEP); // 46
  fprintf(myfile, "%s%c", "SIC+PHA % GAP CLOSED", SEP); // 47
  fprintf(myfile, "%s%c", "GMI+PHA % GAP CLOSED", SEP); // 48

  //////////////////// ROUND INFO
  fprintf(myfile, "%s%c", "TOTAL NUM SICS", SEP); // 1
  fprintf(myfile, "%s%c", "NUM SIC ROUNDS", SEP); // 2
  fprintf(myfile, "%s%c", "FINAL SIC BOUND", SEP); // 3
  fprintf(myfile, "%s%c", "VPC+ BOUND RD 1", SEP); // 4
  fprintf(myfile, "%s%c", "VPC+ BOUND RD 2", SEP); // 5
  fprintf(myfile, "%s%c", "VPC+ BOUND RD 5", SEP); // 6
  fprintf(myfile, "%s%c", "PHA+ BOUND RD 1", SEP); // 7
  fprintf(myfile, "%s%c", "PHA+ BOUND RD 2", SEP); // 8
  fprintf(myfile, "%s%c", "PHA+ BOUND RD 5", SEP); // 9
  fprintf(myfile, "%s%c", "LP BASIS COND", SEP); // 10
  fprintf(myfile, "%s%c", "SIC BASIS COND INIT", SEP); // 11
  fprintf(myfile, "%s%c", "SIC BASIS COND FINAL", SEP); // 12
  fprintf(myfile, "%s%c", "VPC BASIS COND RD 1", SEP); // 13
  fprintf(myfile, "%s%c", "VPC BASIS COND RD 2", SEP); // 14
  fprintf(myfile, "%s%c", "VPC BASIS COND RD 5", SEP); // 15
  fprintf(myfile, "%s%c", "VPC BASIS COND FINAL", SEP); // 16
  fprintf(myfile, "%s%c", "VPC+ BASIS COND RD 1", SEP); // 17
  fprintf(myfile, "%s%c", "VPC+ BASIS COND RD 2", SEP); // 18
  fprintf(myfile, "%s%c", "VPC+ BASIS COND RD 5", SEP); // 19
  fprintf(myfile, "%s%c", "VPC+ BASIS COND FINAL", SEP); // 20
  fprintf(myfile, "%s%c", "PHA BASIS COND RD 1", SEP); // 21
  fprintf(myfile, "%s%c", "PHA BASIS COND RD 2", SEP); // 22
  fprintf(myfile, "%s%c", "PHA BASIS COND RD 5", SEP); // 23
  fprintf(myfile, "%s%c", "PHA BASIS COND FINAL", SEP); // 24
  fprintf(myfile, "%s%c", "PHA+ BASIS COND RD 1", SEP); // 25
  fprintf(myfile, "%s%c", "PHA+ BASIS COND RD 2", SEP); // 26
  fprintf(myfile, "%s%c", "PHA+ BASIS COND RD 5", SEP); // 27
  fprintf(myfile, "%s%c", "PHA+ BASIS COND FINAL", SEP); // 28
  fprintf(myfile, "%s%c", "MIN SIC ORTHOGONALITY INIT", SEP); // 29
  fprintf(myfile, "%s%c", "MAX SIC ORTHOGONALITY INIT", SEP); // 30
  fprintf(myfile, "%s%c", "AVG SIC ORTHOGONALITY INIT", SEP); // 31
  fprintf(myfile, "%s%c", "MIN VPC ORTHOGONALITY INIT", SEP); // 32
  fprintf(myfile, "%s%c", "MAX VPC ORTHOGONALITY INIT", SEP); // 33
  fprintf(myfile, "%s%c", "AVG VPC ORTHOGONALITY INIT", SEP); // 34
  fprintf(myfile, "%s%c", "MIN PHA ORTHOGONALITY INIT", SEP); // 35
  fprintf(myfile, "%s%c", "MAX PHA ORTHOGONALITY INIT", SEP); // 36
  fprintf(myfile, "%s%c", "AVG PHA ORTHOGONALITY INIT", SEP); // 37
  fprintf(myfile, "%s%c", "MIN SIC ORTHOGONALITY FINAL", SEP); // 38
  fprintf(myfile, "%s%c", "MAX SIC ORTHOGONALITY FINAL", SEP); // 39
  fprintf(myfile, "%s%c", "AVG SIC ORTHOGONALITY FINAL", SEP); // 40
  fprintf(myfile, "%s%c", "MIN VPC ORTHOGONALITY FINAL", SEP); // 41
  fprintf(myfile, "%s%c", "MAX VPC ORTHOGONALITY FINAL", SEP); // 42
  fprintf(myfile, "%s%c", "AVG VPC ORTHOGONALITY FINAL", SEP); // 43
  fprintf(myfile, "%s%c", "MIN PHA ORTHOGONALITY FINAL", SEP); // 44
  fprintf(myfile, "%s%c", "MAX PHA ORTHOGONALITY FINAL", SEP); // 45
  fprintf(myfile, "%s%c", "AVG PHA ORTHOGONALITY FINAL", SEP); // 46

  //////////////////// BB INFO
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("FIRST BB " + name + " VPC", myfile);
    writeEntryToLog("FIRST BB " + name + " VPC+GOMORY", myfile);
  }
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("BEST BB " + name + " VPC", myfile);
    writeEntryToLog("BEST BB " + name + " VPC+GOMORY", myfile);
  }
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("AVG BB " + name + " VPC", myfile);
    writeEntryToLog("AVG BB " + name + " VPC+GOMORY", myfile);
  }
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("ALL BB " + name + " VPC", myfile);
    writeEntryToLog("ALL BB " + name + " VPC+GOMORY", myfile);
  }
  /*
   ////////////////////// CONDITION NUMBERS
   fprintf(myfile, "%s%c", "INIT LGIC BASIS COND", SEP); // 1
   fprintf(myfile, "%s%c", "ALL LGIC BASIS COND", SEP); // 2
   fprintf(myfile, "%s%c", "INIT LSIC BASIS COND", SEP); // 3
   fprintf(myfile, "%s%c", "ALL LSIC BASIS COND", SEP); // 4
   fprintf(myfile, "%s%c", "OSIC BASIS COND", SEP); // 5
   fprintf(myfile, "%s%c", "INIT LGIC + INIT LSIC BASIS COND", SEP); // 6
   fprintf(myfile, "%s%c", "ALL LGIC + ALL LSIC BASIS COND", SEP); // 7
   fprintf(myfile, "%s%c", "INIT LGIC + OSIC BASIS COND", SEP); // 8
   fprintf(myfile, "%s%c", "ALL LGIC + OSIC BASIS COND", SEP); // 9
   */
  ////////////////////// TIME
  for (int t = 0; t < (int) GlobalVariables::time_name.size(); t++) {
    fprintf(myfile, "%s%c", GlobalVariables::time_name[t].c_str(), SEP);
  }
  ////////////////////// newline
  fprintf(myfile, "\n");
  fflush(myfile);
} /* writeHeaderToII */

/***********************************************************************/
/**
 * @brief Adds the header lines to the file
 */
void writeCleaningHeaderToLog(FILE *myfile) {
  if (myfile == NULL)
    return;
  writeEntryToLog("INSTANCE", myfile);
  //// Original problem
  writeEntryToLog("ROWS", myfile);
  writeEntryToLog("COLS", myfile);
  writeEntryToLog("NNZ", myfile);
  writeEntryToLog("# INTEGER", myfile);
  writeEntryToLog("# BINARY", myfile);
  writeEntryToLog("ORIG LP OPT", myfile);
  writeEntryToLog("ORIG SB LB", myfile);
  writeEntryToLog("ORIG PRIMAL DEGENERATE", myfile);
  writeEntryToLog("ORIG DUAL DEGENERATE", myfile);
  writeEntryToLog("ORIG NUM SICS", myfile);
  writeEntryToLog("ORIG SIC OPT", myfile);
  writeEntryToLog("ORIG NUM SICS RD 2", myfile);
  writeEntryToLog("ORIG SIC OPT RD 2", myfile);
  writeEntryToLog("ORIG NUM SICS STR", myfile);
  writeEntryToLog("ORIG SIC OPT STR", myfile);
  writeEntryToLog("ORIG NUM SICS RD 2 STR", myfile);
  writeEntryToLog("ORIG SIC OPT RD 2 STR", myfile);
  // Cbc
  //if (param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND) & BB_Strategy_Options::cbc) {
  // Default
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("CBC_DEFAULT ORIG BB " + name, myfile);
  }
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("CBC_DEFAULT ORIG SICS STR BB " + name,
        myfile);
  }
  // Cuts on
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("CBC_CUTSON ORIG BB " + name, myfile);
  }
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("CBC_CUTSON ORIG SICS STR BB " + name,
        myfile);
  }
  // Cuts on
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("CBC_CUTSOFF ORIG BB " + name, myfile);
  }
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("CBC_CUTSOFF ORIG SICS STR BB " + name,
        myfile);
  }
  //} /* cbc */
  // CPLEX
  //if (param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND) & BB_Strategy_Options::cplex) {
  // Default
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("CPLEX_DEFAULT ORIG BB " + name, myfile);
  }
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("CPLEX_DEFAULT ORIG SICS STR BB " + name,
        myfile);
  }
  // Cuts on
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("CPLEX_CUTSON ORIG BB " + name, myfile);
  }
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("CPLEX_CUTSON ORIG SICS STR BB " + name,
        myfile);
  }
  // Cuts on
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("CPLEX_CUTSOFF ORIG BB " + name, myfile);
  }
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("CPLEX_CUTSOFF ORIG SICS STR BB " + name,
        myfile);
  }
  //} /* cplex */
  // Gurobi
  //if (param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND) & BB_Strategy_Options::gurobi) {
  // Default
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("GUROBI_DEFAULT ORIG BB " + name,
        myfile);
  }
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("GUROBI_DEFAULT ORIG SICS STR BB " + name,
        myfile);
  }
  // Cuts on
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("GUROBI_CUTSON ORIG BB " + name, myfile);
  }
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("GUROBI_CUTSON ORIG SICS STR BB " + name,
        myfile);
  }
  // Cuts on
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("GUROBI_CUTSOFF ORIG BB " + name,
        myfile);
  }
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("GUROBI_CUTSOFF ORIG SICS STR BB " + name,
        myfile);
  }
  //} /* gurobi */
  //// Cleaned problem
  writeEntryToLog("CLEANED ROWS", myfile);
  writeEntryToLog("CLEANED COLS", myfile);
  writeEntryToLog("CLEANED NNZ", myfile);
  writeEntryToLog("CLEANED # INTEGER", myfile);
  writeEntryToLog("CLEANED # BINARY", myfile);
  writeEntryToLog("# BOUNDS CHANGED", myfile);
  writeEntryToLog("# SB FIXED", myfile);
  //if (param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND) & BB_Strategy_Options::cplex) {
  writeEntryToLog("CPLEX PRESOLVED LP OPT", myfile);
  //}
  //if (param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND) & BB_Strategy_Options::gurobi) {
  writeEntryToLog("GUROBI PRESOLVED LP OPT", myfile);
  //}
  writeEntryToLog("CLEANED LP OPT", myfile);
  writeEntryToLog("CLEANED SB LB", myfile);
  writeEntryToLog("CLEANED PRIMAL DEGENERATE", myfile);
  writeEntryToLog("CLEANED DUAL DEGENERATE", myfile);
  writeEntryToLog("CLEANED NUM SICS", myfile);
  writeEntryToLog("CLEANED SIC OPT", myfile);
  writeEntryToLog("CLEANED NUM SICS RD 2", myfile);
  writeEntryToLog("CLEANED SIC OPT RD 2", myfile);
  writeEntryToLog("CLEANED NUM SICS STR", myfile);
  writeEntryToLog("CLEANED SIC OPT STR", myfile);
  writeEntryToLog("CLEANED NUM SICS RD 2 STR", myfile);
  writeEntryToLog("CLEANED SIC OPT RD 2 STR", myfile);
  // Cbc
  //if (param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND) & BB_Strategy_Options::cbc) {
  // Default
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("CBC_DEFAULT CLEANED BB " + name,
        myfile);
  }
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("CBC_DEFAULT CLEANED SICS STR BB " + name,
        myfile);
  }
  // Cuts on
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("CBC_CUTSON CLEANED BB " + name, myfile);
  }
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("CBC_CUTSON CLEANED SICS STR BB " + name,
        myfile);
  }
  // Cuts on
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("CBC_CUTSOFF CLEANED BB " + name,
        myfile);
  }
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("CBC_CUTSOFF CLEANED SICS STR BB " + name,
        myfile);
  }
  //} /* cbc */
  // CPLEX
  //if (param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND) & BB_Strategy_Options::cplex) {
  // Default
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("CPLEX_DEFAULT CLEANED BB " + name,
        myfile);
  }
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("CPLEX_DEFAULT CLEANED SICS STR BB " + name,
        myfile);
  }
  // Cuts on
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("CPLEX_CUTSON CLEANED BB " + name,
        myfile);
  }
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("CPLEX_CUTSON CLEANED SICS STR BB " + name,
        myfile);
  }
  // Cuts on
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("CPLEX_CUTSOFF CLEANED BB " + name,
        myfile);
  }
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("CPLEX_CUTSOFF CLEANED SICS STR BB " + name,
        myfile);
  }
  //} /* cplex */
  // Gurobi
  //if (param.getParamVal(ParamIndices::BB_STRATEGY_PARAM_IND) & BB_Strategy_Options::gurobi) {
  // Default
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("GUROBI_DEFAULT CLEANED BB " + name,
        myfile);
  }
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("GUROBI_DEFAULT CLEANED SICS STR BB " + name,
        myfile);
  }
  // Cuts on
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("GUROBI_CUTSON CLEANED BB " + name,
        myfile);
  }
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("GUROBI_CUTSON CLEANED SICS STR BB " + name,
        myfile);
  }
  // Cuts on
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("GUROBI_CUTSOFF CLEANED BB " + name,
        myfile);
  }
  for (std::string name : BB_INFO_CONTENTS) {
    writeEntryToLog("GUROBI_CUTSOFF CLEANED SICS STR BB " + name,
        myfile);
  }
  //} /* gurobi */
  //// Overall time
  writeEntryToLog("INIT SOLVE TIME", myfile);
  writeEntryToLog("TOTAL TIME", myfile);
  fprintf(myfile, "\n");
  fflush(myfile);
} /* writeCleaningHeaderToII */

/***********************************************************************/
/**
 * @brief Writes solution (both primal and dual) to file.
 * @param solver          ::  The solver whose solution we are writing.
 * @param soln_name       ::  The name we are outputting to.
 * @param out_stream      ::  Out stream.
 * @param width_varname   ::  Width of variable name field.
 * @param width_val       ::  Width of variable value field.
 * @param width_activity  ::  Width of row activity field.
 * @param after_dec       ::  Number of digits after the decimal.
 * @param EXT             ::  Extension added to end of file output.
 */
//void writeSoln(LiftGICsSolverInterface *solver, std::string soln_name, FILE *log_out,
//    int width_varname = 16, int width_val = 12, int width_activity = 12,
//    int after_dec = 6, const std::string EXT = "-optSoln.alex") {
/***********************************************************************/
/**
 * @brief Writes solution (both primal and dual) to file.
 * @param solver          ::  The solver whose solution we are writing.
 * @param soln_name       ::  The name we are outputting to.
 * @param out_stream      ::  Out stream.
 * @param width_varname   ::  Width of variable name field.
 * @param width_val       ::  Width of variable value field.
 * @param width_activity  ::  Width of row activity field.
 * @param after_dec       ::  Number of digits after the decimal.
 * @param EXT             ::  Extension added to end of file output.
 */
void writeSoln(const PointCutsSolverInterface* const solver, std::string soln_name,
    int width_varname, int width_val, int width_stat, int width_rc,
    int width_activity, int width_rhs, int after_dec,
    const std::string EXT) {
#ifdef TRACE
  printf("\n## Writing solution for %s. ##\n", soln_name.c_str());
#endif
  FILE *myfile;
  myfile = fopen((soln_name + EXT).c_str(), "w");
  if (myfile != NULL) {
    std::vector<int> cstat(solver->getNumCols()), rstat(
        solver->getNumRows());
    solver->getBasisStatus(&cstat[0], &rstat[0]);

    fprintf(myfile, "Obj: %s with value %f.\n",
        solver->getObjName().c_str(), solver->getObjValue());
    fprintf(myfile, "\n");

    fprintf(myfile, "%-*s\t%-*s\t%-*s\t%-*s\t\n", width_varname,
        "Variable name", width_val, "Value", width_stat, "cstat",
        width_rc, "Reduced Cost");
    for (int col = 0; col < solver->getNumCols(); col++) {
      fprintf(myfile, "%-*.*s\t% -*.*f\t%-*d\t%-*f\t\n", width_varname,
          width_varname, solver->getColName(col).c_str(), width_val,
          after_dec, solver->getColSolution()[col], width_stat,
          cstat[col], width_rc, solver->getReducedCost()[col]);
    }
    fprintf(myfile, "\n");

    fprintf(myfile, "%-*s\t%-*s\t%-*s\t%-*s\t%-*s\t%-*s\t\n", width_varname,
        "Row name", width_val, "Dual Value", width_activity, "Activity",
        width_rhs, ">=", width_rhs, "<=", width_stat, "rstat");
    for (int row = 0; row < solver->getNumRows(); row++) {
      char rowsense = solver->getRowSense()[row];
      if (rowsense == 'E' || rowsense == 'R')
        fprintf(myfile,
            "%-*.*s\t% -*.*f\t% -*.*f\t% -*.*f\t% -*.*f\t%-*d\t\n",
            width_varname, width_varname,
            solver->getRowName(row).c_str(), width_val, after_dec,
            solver->getRowPrice()[row], width_activity, after_dec,
            solver->getRowActivity()[row], width_rhs, after_dec,
            solver->getRowLower()[row], width_rhs, after_dec,
            solver->getRowUpper()[row], width_stat, rstat[row]);
      else if (rowsense == 'G')
        fprintf(myfile,
            "%-*.*s\t% -*.*f\t% -*.*f\t% -*.*f\t%-*s\t%-*d\t\n",
            width_varname, width_varname,
            solver->getRowName(row).c_str(), width_val, after_dec,
            solver->getRowPrice()[row], width_activity, after_dec,
            solver->getRowActivity()[row], width_rhs, after_dec,
            solver->getRowLower()[row], width_rhs, "", width_stat,
            rstat[row]);
      else if (rowsense == 'L')
        fprintf(myfile,
            "%-*.*s\t% -*.*f\t% -*.*f\t%-*s\t% -*.*f\t%-*d\t\n",
            width_varname, width_varname,
            solver->getRowName(row).c_str(), width_val, after_dec,
            solver->getRowPrice()[row], width_activity, after_dec,
            solver->getRowActivity()[row], width_rhs, "", width_rhs,
            after_dec, solver->getRowUpper()[row], width_stat,
            rstat[row]);
      else
        fprintf(myfile, 
            "%-*.*s\t% -*.*f\t% -*.*f\t%-*s\t%-*s\t%-*d\t\n",
            width_varname, width_varname,
            solver->getRowName(row).c_str(), width_val, after_dec,
            solver->getRowPrice()[row], width_activity, after_dec,
            solver->getRowActivity()[row], width_rhs, "?",
            width_rhs, "?", width_stat, rstat[row]);
    }
    fclose(myfile);
  }
} /* writeSoln */

/***********************************************************************/
/**
 * Prints our the basis corresponding the COIN instance solver
 */
void printSimplexTableauWithNames(FILE* fptr, PointCutsSolverInterface* const solver) {
  //Print optimal basis
//  const int solverFactorizationStatus = solver->canDoSimplexInterface();
//  if (solverFactorizationStatus == 0) {
  solver->enableFactorization();
//  }
  fprintf(fptr, "Optimal,simplex,tableau,of,P:\n");
  int numCols = solver->getNumCols();
  int numRows = solver->getNumRows();
  std::vector<double> a0(numRows);
  std::vector<int> tableauRowToVarIndex(numRows);
  solver->getBasics(&tableauRowToVarIndex[0]);
  const double* rowActivity = solver->getRowActivity();
  const double* rowRHS = solver->getRightHandSide();
  const double* colSoln = solver->getColSolution();

  std::vector<int> rstat(numRows), cstat(numCols);

  for (int i = 0; i < numRows; i++) {
    if (tableauRowToVarIndex[i] < numCols) {
      a0[i] = colSoln[tableauRowToVarIndex[i]];
    } else {
      a0[i] = -rowActivity[tableauRowToVarIndex[i] - numCols]
          + rowRHS[tableauRowToVarIndex[i] - numCols];
    }
  }

  int i, j;
  double* basisBasicRow = new double[numCols];
  double* basisSlackRow = new double[numRows];

  const char* rowSense = solver->getRowSense();

  fprintf(fptr, ",,");
  for (j = 0; j < numCols; j++)
    fprintf(fptr, "%d,", j);
  for (j = 0; j < numRows; j++)
    fprintf(fptr, "%d,", numCols + j);
  fprintf(fptr, "\n");
  fprintf(fptr, ",,");
  for (j = 0; j < numCols; j++)
    fprintf(fptr, "%s,", solver->getColName(j).c_str());
  for (j = 0; j < numRows; j++)
    fprintf(fptr, "%s,", solver->getRowName(j).c_str());
  fprintf(fptr, "LP solution (not RHS)\n");
  for (i = 0; i < numRows; i++) {
    fprintf(fptr, "%d,", i);
    if (tableauRowToVarIndex[i] < numCols)
      fprintf(fptr, "%s,",
          solver->getColName(tableauRowToVarIndex[i]).c_str());
    else
      fprintf(fptr, "%s,",
          solver->getRowName(tableauRowToVarIndex[i] - numCols).c_str());
    solver->getBInvARow(i, basisBasicRow, basisSlackRow);
    for (j = 0; j < numCols; j++)
      fprintf(fptr, "%2.4lf,", basisBasicRow[j]); //cout << basisBasicRow[j] << " ";
    for (j = 0; j < numRows; j++) {
      if (rowSense[j] == 'G')
        fprintf(fptr, "%2.4lf,", -basisSlackRow[j]); //cout << basisSlackRow[j] << " ";
      else {
        fprintf(fptr, "%2.4lf,", basisSlackRow[j]); //cout << basisSlackRow[j] << " ";
      }
    }
    //fprintf(fptr,"%1.2f ",primalSoln[i]);
    fprintf(fptr, "%2.4lf,", a0[i]);
    fprintf(fptr, "\n");

  }
  fprintf(fptr, ",Row0,");
  const double* redCostStruct = solver->getReducedCost();
  const double* redCostSlacks = solver->getRowPrice();
  solver->getBasisStatus(&cstat[0], &rstat[0]);
  for (j = 0; j < numCols; j++) {
    fprintf(fptr, "%2.4lf,", redCostStruct[j]);
    //    printf("%2.4lf\n", redCostStruct[j]);
  } //cout << basisBasicRow[j] << " ";
  for (j = 0; j < numRows; j++) {

    if (rowSense[j] == 'G')
      fprintf(fptr, "%2.4lf,", redCostSlacks[j]);
    else
      fprintf(fptr, "%2.4lf,", -redCostSlacks[j]);
  }
  fprintf(fptr, "%2.4lf\n", solver->getObjValue());
  // cout << "\n";
//  if (solverFactorizationStatus == 0) {
  solver->disableFactorization();
//  }
  delete[] basisBasicRow;
  delete[] basisSlackRow;
}

/***********************************************************************/
/**
 * Prints our the basis corresponding the COIN instance solver; only non-basic non-artificial columns printed.
 */
void printNBSimplexTableauWithNames(FILE* fptr, PointCutsSolverInterface* const solver) {
  //Print optimal basis
//  const int solverFactorizationStatus = solver->canDoSimplexInterface();
//  if (solverFactorizationStatus == 0) {
  solver->enableFactorization();
//  }
  fprintf(fptr, "NB, optimal,simplex,tableau,of,P:\n");
  int numCols = solver->getNumCols();
  int numRows = solver->getNumRows();
  std::vector<double> a0(numRows);
  std::vector<int> tableauRowToVarIndex(numRows);
  solver->getBasics(&tableauRowToVarIndex[0]);
  const double* rowActivity = solver->getRowActivity();
  const double* rowRHS = solver->getRightHandSide();
  const double* colSoln = solver->getColSolution();

  std::vector<int> rstat(numRows), cstat(numCols);
  solver->getBasisStatus(&cstat[0], &rstat[0]);

  for (int i = 0; i < numRows; i++) {
    if (tableauRowToVarIndex[i] < numCols) {
      a0[i] = colSoln[tableauRowToVarIndex[i]];
    } else {
      a0[i] = -rowActivity[tableauRowToVarIndex[i] - numCols]
          + rowRHS[tableauRowToVarIndex[i] - numCols];
    }
  }

  int i, j;
  double* basisBasicRow = new double[numCols];
  double* basisSlackRow = new double[numRows];
  fprintf(fptr, ",,");
  for (j = 0; j < numCols; j++) {
    if (cstat[j] == 2 || cstat[j] == 3)
      fprintf(fptr, "%d,", j);
  }
  for (j = 0; j < numRows; j++) {
    if ((rstat[j] == 2 || rstat[j] == 3) && solver->getRowSense()[j] != 'E')
      fprintf(fptr, "%d,", numCols + j);
  }
  fprintf(fptr, "\n");
  fprintf(fptr, ",,");
  for (j = 0; j < numCols; j++)
    if (cstat[j] == 2 || cstat[j] == 3)
      fprintf(fptr, "%s,", solver->getColName(j).c_str());
  for (j = 0; j < numRows; j++)
    if ((rstat[j] == 2 || rstat[j] == 3) && solver->getRowSense()[j] != 'E')
      fprintf(fptr, "%s,", solver->getRowName(j).c_str());
  fprintf(fptr, "RHS\n");
  for (i = 0; i < numRows; i++) {
    fprintf(fptr, "%d,", i);
    if (tableauRowToVarIndex[i] < numCols)
      fprintf(fptr, "%s,",
          solver->getColName(tableauRowToVarIndex[i]).c_str());
    else
      fprintf(fptr, "%s,",
          solver->getRowName(tableauRowToVarIndex[i] - numCols).c_str());
    solver->getBInvARow(i, basisBasicRow, basisSlackRow);
    for (j = 0; j < numCols; j++)
      if (cstat[j] == 2 || cstat[j] == 3)
        fprintf(fptr, "%2.7lf,", basisBasicRow[j]); //cout << basisBasicRow[j] << " ";
    for (j = 0; j < numRows; j++)
      if ((rstat[j] == 2 || rstat[j] == 3)
          && solver->getRowSense()[j] != 'E')
        fprintf(fptr, "%2.7lf,", basisSlackRow[j]); //cout << basisSlackRow[j] << " ";
    //fprintf(fptr,"%1.2f ",primalSoln[i]);
    fprintf(fptr, "%2.7lf,", a0[i]);
    fprintf(fptr, "\n");

  }
  fprintf(fptr, ",Row0,");
  const double* redCostStruct = solver->getReducedCost();
  const double* redCostSlacks = solver->getRowPrice();
  const char* rowSense = solver->getRowSense();
  for (j = 0; j < numCols; j++) {
    if (cstat[j] == 2 || cstat[j] == 3)
      fprintf(fptr, "%2.7lf,", redCostStruct[j]);
    //    printf("%2.4lf\n", redCostStruct[j]);
  } //cout << basisBasicRow[j] << " ";
  for (j = 0; j < numRows; j++) {
    if ((rstat[j] == 2 || rstat[j] == 3)
        && solver->getRowSense()[j] != 'E') {
      if (rowSense[j] == 'G')
        fprintf(fptr, "%2.7lf,", redCostSlacks[j]);
      else
        fprintf(fptr, "%2.7lf,", -redCostSlacks[j]);
    }
  }
  fprintf(fptr, "%2.7lf\n", solver->getObjValue());
  // cout << "\n";
//  if (solverFactorizationStatus == 0) {
  solver->disableFactorization();
//  }
  delete[] basisBasicRow;
  delete[] basisSlackRow;
} /* printNBSimplexTableauWithNames */

/**
 * Print points stored in point store
 */
void printPointStore(FILE* fptr, const int fracVar, const int dim,
    const std::vector<Point> &point) {
  fprintf(fptr,
      "## Printing point store for split %d. Number points: %d. ##\n",
      fracVar, (int) point.size());

  // First print indices of the points
  fprintf(fptr, "Split var%cPoint index%c", SEP, SEP);
  for (int p = 0; p < (int) point.size(); p++) {
    fprintf(fptr, "%d%c", p, SEP);
  }
  fprintf(fptr, "\n");

  // Now we print the obj violations
  fprintf(fptr, "%c%c", SEP, SEP);
  for (int p = 0; p < (int) point.size(); p++) {
    fprintf(fptr, "%f%c", point[p].getObjViolation(), SEP);
  }
  fprintf(fptr, "\n");

  // Now the actual points
  for (int i = 0; i < dim; i++) {
    fprintf(fptr, "%d%c", fracVar, SEP);
    fprintf(fptr, "%d%c", i, SEP);
    for (int p = 0; p < (int) point.size(); p++) {
      fprintf(fptr, "%2.7f%c", point[p][i], SEP);
    }
    fprintf(fptr, "\n");
  }
  fflush(fptr);
} /* printPointStore */

/**
 * Print rays stored in ray store
 */
void printRayStore(FILE* fptr, const int fracVar, const int dim,
    const std::vector<Ray> &ray) {
  fprintf(fptr, "## Printing ray store for split %d. Number rays: %d. ##\n",
      fracVar, (int) ray.size());

  // First print indices of the rays
  fprintf(fptr, "Split var%cRay index%c", SEP, SEP);
  for (int r = 0; r < (int) ray.size(); r++) {
    fprintf(fptr, "%d%c", r, SEP);
  }
  fprintf(fptr, "\n");

  // Now we print the extreme flags
  fprintf(fptr, "%c%c", SEP, SEP);
  for (int r = 0; r < (int) ray.size(); r++) {
    if (ray[r].isExtreme())
      fprintf(fptr, "E%c", SEP);
    else
      fprintf(fptr, "%c", SEP);
  }
  fprintf(fptr, "\n");

  // Now the actual rays
  for (int i = 0; i < dim; i++) {
    fprintf(fptr, "%d%c", fracVar, SEP);
    fprintf(fptr, "%d%c", i, SEP);
    for (int r = 0; r < (int) ray.size(); r++) {
      fprintf(fptr, "%2.7f%c", ray[r][i], SEP);
    }
    fprintf(fptr, "\n");
  }
  fflush(fptr);
} /* printRayStore */

/***********************************************************************/
/**
 * @brief Writes deleted variable names and their values to file.
 * @param deleteCols          ::  Indices of deleted columns (vars).
 * @param deleteColsNames     ::  Names of deleted columns.
 * @param deleteColsVals      ::  Values in orig lp soln of deleted vars.
 * @param deleteColsObjCoeffs ::  Obj coeffs of deleted columns.
 * @param deleteColsLB        ::  LBs of deleted columns.
 * @param deleteColsUB        ::  UBs of deleted columns.
 * @param objSum              ::  Total sum deleted from objective
 * @param fname               ::  File name to write to.
 */
void writeDeletedVars(std::vector<int> &deleteCols,
    std::vector<std::string> &deleteColsNames,
    std::vector<double> &deleteColsVals,
    std::vector<double> &deleteColsObjCoeffs,
    std::vector<double> &deleteColsLB, std::vector<double> &deleteColsUB,
    double objSum, std::string fname) {
  int width_varname = 16;
  int width_var = 12;
  int width_val = 12;
  int width_obj = 16;
  int width_lb = 12;
  int width_ub = 12;
  const std::string EXT = "-deletedVars.alex";
//#ifdef TRACE
//  std::cout << "\n############### Starting writeDeletedVars routine to generate "
//  << fname + EXT << ". ###############" << std::endl;
//#endif
  FILE *myfile;
  myfile = fopen((fname + EXT).c_str(), "w");
  if (myfile != NULL) {
    fprintf(myfile, "Problem_Name:\t%s\n", fname.c_str());
    fprintf(myfile, "Total_Removed:\t%f\n", objSum);
    fprintf(myfile, "%-*s\t%-*s\t%-*s\t%-*s\t%-*s\t%-*s\n", width_varname,
        "Variable name", width_var, "Column index", width_val, "Value",
        width_obj, "Objective coeff", width_lb, "LB curr", width_ub,
        "UB curr");
    for (unsigned col = 0; col < deleteCols.size(); col++) {
//      double lbcurr = deleteColsLB[col];
//      double ubcurr = deleteColsUB[col];

      fprintf(myfile, "%-*s\t%-*d\t%-*f\t%-*f\t%-*s\t%-*s\n",
          width_varname, deleteColsNames[col].c_str(), width_var,
          deleteCols[col], width_val, deleteColsVals[col], width_obj,
          deleteColsObjCoeffs[col], width_lb,
          stringValue(deleteColsLB[col], "%f").c_str(), width_ub,
          stringValue(deleteColsUB[col], "%f").c_str());

//      if (lbcurr > -1 * param.getINFINITY() + param.getEPS()) {
//        if (ubcurr < param.getINFINITY() - param.getEPS()) {
//          fprintf(myfile, "%-*s\t%-*d\t%-*f\t%-*f\t%-*f\t%-*f\n",
//              width_varname, deleteColsNames[col].c_str(),
//              width_var, deleteCols[col], width_val,
//              deleteColsVals[col], width_obj,
//              deleteColsObjCoeffs[col], width_lb,
//              deleteColsLB[col], width_ub, deleteColsUB[col]);
//        } else {
//          fprintf(myfile, "%-*s\t%-*d\t%-*f\t%-*f\t%-*f\t%-*s\n",
//              width_varname, deleteColsNames[col].c_str(),
//              width_var, deleteCols[col], width_val,
//              deleteColsVals[col], width_obj,
//              deleteColsObjCoeffs[col], width_lb,
//              deleteColsLB[col], width_ub, "+inf");
//        }
//      } else if (ubcurr < param.getINFINITY() - param.getEPS()) {
//        fprintf(myfile, "%-*s\t%-*d\t%-*f\t%-*f\t%-*s\t%-*f\n",
//            width_varname, deleteColsNames[col].c_str(), width_var,
//            deleteCols[col], width_val, deleteColsVals[col],
//            width_obj, deleteColsObjCoeffs[col], width_lb, "-inf",
//            width_ub, deleteColsUB[col]);
//      } else {
//        fprintf(myfile, "%-*s\t%-*d\t%-*f\t%-*f\t%-*s\t%-*s\n",
//            width_varname, deleteColsNames[col].c_str(), width_var,
//            deleteCols[col], width_val, deleteColsVals[col],
//            width_obj, deleteColsObjCoeffs[col], width_lb, "-inf",
//            width_ub, "+inf");
//      }
    }
    fclose(myfile);
  }
} /* writeDeletedVars */

/***********************************************************************/
/**
 * Writes deleted rows to file, where each line corresponds to a row.
 */
/* void writeDeletedRows(PointCutsSolverInterface *solver, std::string fname) {
  const std::string EXT = "deletedRows.alex";
#ifdef TRACE
  std::cout << "\n############### (NOT TRUE YET) Starting writeDeletedRows routine to generate "
  << fname + EXT << ". ###############" << std::endl;
#endif
}*/ /* writeDeletedRows */

/***********************************************************************/
/**
 * @brief Writes problem name to start a new instance info line
 */
void writeProbNameToLog(std::string &prob_name, FILE *myfile) {
//#ifdef TRACE
//  printf(
//      "\n############### Starting writeProbNameToInstanceInfo routine for %s. ###############\n",
//      prob_name.c_str());
//#endif
  if (myfile == NULL)
    return;
  // Instance name
  fprintf(myfile, "%s%c", prob_name.c_str(), SEP);
  fflush(myfile);
} /* writeProbNameToInstanceInfo */

/***********************************************************************/
/**
 * Writes which settings we are using to instance info
 */
void writeParamInfoToLog(FILE* myfile) {
  if (myfile == NULL)
    return;
  for (int param_ind = 0; param_ind < ParamIndices::NUM_PARAMS; param_ind++) {
    if (param_ind == ParamIndices::VPC_DEPTH_PARAM_IND) {
      fprintf(myfile, "%05d%c", param.paramVal[param_ind], SEP);
    } else {
      fprintf(myfile, "%d%c", param.paramVal[param_ind], SEP);
    }
  }
  fprintf(myfile, "%e%c", param.getMINORTHOGONALITY(), SEP);
  fprintf(myfile, "%e%c", param.getEPS(), SEP);
  fprintf(myfile, "%e%c", param.getRAYEPS(), SEP);
  fprintf(myfile, "%e%c", param.getTIMELIMIT(), SEP);
  fprintf(myfile, "%e%c", param.getPARTIAL_BB_TIMELIMIT(), SEP);
  fprintf(myfile, "%e%c", param.getBB_TIMELIMIT(), SEP);
  fprintf(myfile, "%e%c", param.getCUTSOLVER_TIMELIMIT(), SEP);
  fflush(myfile);
} /* writeParamInfoToII */

/***********************************************************************/
/**
 * @brief Appends information about LP relaxation solution to prob
 */
void writeSolnInfoToLog(const SolutionInfo& probSolnInfo,
    FILE *myfile) {
//#ifdef TRACE
//  printf(
//      "\n############### Starting writeSolnInfoToInstanceInfo routine for problem %s. ###############\n",
//      prob_name.c_str());
//#endif
  if (myfile == NULL)
    return;
  // Print original problem info:
  //  NUM ROWS, NUM COLS, NUM EQ ROWS, NUM INEQ ROWS, NUM BOUND ROWS, NUM ASSIGN ROWS,
  //  INTEGERS NON-BIN, BINARIES, CONTINUOUS, NUM BASIC, NUM NON-BASIC,
  //Deleted //NUM ORIG BASIC,
  //  NUM ORIG NON-BASIC, FRAC CORE, A NONZERO, A-DENSITY, INTEGER OBJ, LP OBJ
//  if (probSolnInfo != NULL) {
  fprintf(myfile, "%d%c", probSolnInfo.numRows, SEP);
  fprintf(myfile, "%d%c", probSolnInfo.numCols, SEP);
  fprintf(myfile, "%d%c", probSolnInfo.numFixed, SEP);
  fprintf(myfile, "%d%c", probSolnInfo.numEqRows, SEP);
  fprintf(myfile, "%d%c", probSolnInfo.numIneqRows, SEP);
  fprintf(myfile, "%d%c", probSolnInfo.numBoundRows, SEP);
  fprintf(myfile, "%d%c", probSolnInfo.numAssignRows, SEP);
  fprintf(myfile, "%d%c", probSolnInfo.numIntegerNonBinary, SEP);
  fprintf(myfile, "%d%c", probSolnInfo.numBinary, SEP);
  fprintf(myfile, "%d%c", probSolnInfo.numContinuous, SEP);
  fprintf(myfile, "%d%c", probSolnInfo.numBasicCols, SEP);
  fprintf(myfile, "%d%c", probSolnInfo.numNB, SEP);
//    fprintf(myfile, "%d%c", (int) probSolnInfo.basicOrigVarIndex.size(),
//        SEP);
  fprintf(myfile, "%d%c", probSolnInfo.numNBOrig, SEP);
  fprintf(myfile, "%d%c", probSolnInfo.numPrimalDegeneratePivots, SEP);
  fprintf(myfile, "%d%c", probSolnInfo.numDualDegeneratePivots, SEP);
  fprintf(myfile, "%d%c", (int) probSolnInfo.fractionalCore.size(), SEP);
  fprintf(myfile, "%d%c", probSolnInfo.numANonzero, SEP);
  fprintf(myfile, "%2.3f%c",
      (double) probSolnInfo.numANonzero
          / (probSolnInfo.numCols * probSolnInfo.numRows), SEP);
  fprintf(myfile, "%1.10f%c", probSolnInfo.minAbsCoeff, SEP);
  fprintf(myfile, "%1.10f%c", probSolnInfo.maxAbsCoeff, SEP);
  fprintf(myfile, "%1.10f%c", probSolnInfo.minAbsRayValNoEPS, SEP);
  fprintf(myfile, "%1.10f%c", probSolnInfo.minAbsRayVal, SEP);
  fprintf(myfile, "%.20f%c", probSolnInfo.LPopt, SEP);
  fprintf(myfile, "%s%c", stringValue(GlobalVariables::bestObjValue, "%.20f").c_str(), SEP);
  fflush(myfile);
} /* writeSolnInfoToInstanceInfo */

/***********************************************************************/
/**
 * SUBROWS,SUBCOLS,FRAC CORE
 * % 0 FEAS SPLIT SIDES,% 1 FEAS SPLIT SIDE,% 2 FEAS SPLIT SIDES,
 * GEN_SUB
 */
void writeSubSolnInfoToLog(SolutionInfo* solnInfoSub,
    FILE* myfile) {
//#ifdef TRACE
//  printf(
//      "\n############### Starting writeSubSolnInfoToII routine for problem %s. ###############\n",
//      prob_name.c_str());
//#endif
  if (myfile == NULL)
    return;
  if (solnInfoSub == NULL) {
    std::string tmpstring(countSubEntries, SEP);
    fprintf(myfile, "%s", tmpstring.c_str());
  }

  int numsplits = solnInfoSub->fractionalCore.size();
  fprintf(myfile, "%d%c", solnInfoSub->numRows, SEP);
  fprintf(myfile, "%d%c", solnInfoSub->numCols, SEP);
  fprintf(myfile, "%2.3f%c", solnInfoSub->LPopt, SEP);
  fprintf(myfile, "%d%c", numsplits, SEP);
  fprintf(myfile, "%2.3f%c", ((double) solnInfoSub->num0feas) / numsplits,
      SEP);
  fprintf(myfile, "%2.3f%c", ((double) solnInfoSub->num1feas) / numsplits,
      SEP);
  fprintf(myfile, "%2.3f%c", ((double) solnInfoSub->num2feas) / numsplits,
      SEP);
  fflush(myfile);
//  fprintf(myfile, "%d%c", solnInfoSub->numFloorInfeas, SEP);
//  fprintf(myfile, "%d%c", solnInfoSub->numCeilInfeas, SEP);
//  fprintf(myfile, "%f%c", time, SEP);
}

/***********************************************************************/
/**
 fprintf(myfile, "%s%c", "NUM TOTAL SUB SICS", SEP); // 1
 fprintf(myfile, "%s%c", "NUM FEAS SUB SICS", SEP); // 2
 fprintf(myfile, "%s%c", "AVG NUM PIVOTS AVAILABLE", SEP); // 3
 fprintf(myfile, "%s%c", "NUM LSICS", SEP); // 4
 fprintf(myfile, "%s%c", "% INIT LSIC EUCL > OSIC EUCL", SEP); // 5
 fprintf(myfile, "%s%c", "% INIT LSIC EUCL = OSIC EUCL", SEP); // 6
 fprintf(myfile, "%s%c", "% INIT LSIC EUCL < OSIC EUCL", SEP); // 7
 fprintf(myfile, "%s%c", "% BEST LSIC EUCL > OSIC EUCL", SEP); // 8
 fprintf(myfile, "%s%c", "% BEST LSIC EUCL = OSIC EUCL", SEP); // 9
 fprintf(myfile, "%s%c", "% BEST LSIC EUCL < OSIC EUCL", SEP); // 10
 fprintf(myfile, "%s%c", "% INIT LSIC OBJ > ORIG SIC OBJ", SEP); // 11
 fprintf(myfile, "%s%c", "% INIT LSIC OBJ = ORIG SIC OBJ", SEP); // 12
 fprintf(myfile, "%s%c", "% INIT LSIC OBJ < ORIG SIC OBJ", SEP); // 13
 fprintf(myfile, "%s%c", "% BEST LSIC OBJ > ORIG SIC OBJ", SEP); // 14
 fprintf(myfile, "%s%c", "% BEST LSIC OBJ = ORIG SIC OBJ", SEP); // 15
 fprintf(myfile, "%s%c", "% BEST LSIC OBJ < ORIG SIC OBJ", SEP); // 16
 fprintf(myfile, "%s%c", "OSIC BOUND", SEP); // 17
 fprintf(myfile, "%s%c", "INIT LSIC BOUND", SEP); // 18
 fprintf(myfile, "%s%c", "ALL LSIC BOUND", SEP); // 19
 fprintf(myfile, "%s%c", "INIT LSIC + OSIC BOUND", SEP); // 20
 fprintf(myfile, "%s%c", "ALL LSIC + OSIC BOUND", SEP); // 21
 fprintf(myfile, "%s%c", "IMPROVE OVER OSICS", SEP); // 22
 fprintf(myfile, "%s%c", "IMPROVE ALL LSICS", SEP); // 23
 fprintf(myfile, "%s%c", "OSIC BASIS COND", SEP); // 24
 fprintf(myfile, "%s%c", "INIT LSIC BASIS COND", SEP); // 25
 fprintf(myfile, "%s%c", "ALL LSIC BASIS COND", SEP); // 26
 fprintf(myfile, "%s%c", "INIT LSIC + OSIC BASIS COND", SEP); // 27
 fprintf(myfile, "%s%c", "ALL LSIC + OSIC BASIS COND", SEP); // 28
 */
/*void writeCompLSIC_OSICToLog(std::string &prob_name, int numcuts,
    int numfeascuts, double avgNumPivots, int numliftedcuts,
    std::vector<double> &EuclLInit, std::vector<double> &EuclLBest,
    std::vector<double> &EuclOInit, std::vector<double> &EuclOBest,
    std::vector<double> &ObjLInit, std::vector<double> &ObjLBest,
    std::vector<double> &ObjOInit, std::vector<double> &ObjOBest,
    double OSICBoundAll, double OSICBasisCondNum, double LSICBoundInit,
    double LSICBasisCondNumInit, double LSICBoundAll,
    double LSICBasisCondNumAll, double LSICAndOSICBoundInit,
    double LSICAndOSICBasisCondNumInit, double LSICAndOSICBoundAll,
    double LSICAndOSICBasisCondNumAll, FILE* myfile) {
  if (myfile == NULL)
    return;

  //////////
  // Sub
  //////////
  fprintf(myfile, "%d%c", numcuts, SEP); // 1
  fprintf(myfile, "%d%c", numfeascuts, SEP); // 2
  fprintf(myfile, "%f%c", avgNumPivots, SEP); // 2
  fprintf(myfile, "%d%c", numliftedcuts, SEP); // 3
  //////////
  // Eucl
  //////////
  // Compute desired quantities:
  // 1. % splits LSIC Init eucl > OSIC eucl
  // 2. % splits LSIC Init eucl = OSIC eucl
  // 3. % splits LSIC Init eucl < OSIC eucl
  // 4. % splits LSIC Best eucl > OSIC eucl
  // 5. % splits LSIC Best eucl = OSIC eucl
  // 6. % splits LSIC Best eucl < OSIC eucl
  fprintf(myfile, "%2.3f%c", (double) EuclLInit.size() / numfeascuts, SEP); // 3
  fprintf(myfile, "%2.3f%c",
      1.0 - (double) (EuclLInit.size() + EuclOInit.size()) / numfeascuts,
      SEP); // 4
  fprintf(myfile, "%2.3f%c", (double) EuclOInit.size() / numfeascuts, SEP); // 5
  fprintf(myfile, "%2.3f%c", (double) EuclLBest.size() / numfeascuts, SEP); // 6
  fprintf(myfile, "%2.3f%c",
      1.0 - (double) (EuclLBest.size() + EuclOBest.size()) / numfeascuts,
      SEP); // 7
  fprintf(myfile, "%2.3f%c", (double) EuclOBest.size() / numfeascuts, SEP); // 8
  //////////
  // Obj
  //////////
  // 7. % splits LSIC Init obj > OSIC obj
  // 8. % splits LSIC Init obj = OSIC obj
  // 9. % splits LSIC Init obj < OSIC obj
  // 10. % splits LSIC Best obj > OSIC obj
  // 11. % splits LSIC Best obj = OSIC obj
  // 12. % splits LSIC Best obj < OSIC obj
  fprintf(myfile, "%2.3f%c", (double) ObjLInit.size() / numfeascuts, SEP); // 9
  fprintf(myfile, "%2.3f%c",
      1.0 - (double) (ObjLInit.size() + ObjOInit.size()) / numfeascuts,
      SEP); // 10
  fprintf(myfile, "%2.3f%c", (double) ObjOInit.size() / numfeascuts, SEP); // 11
  fprintf(myfile, "%2.3f%c", (double) ObjLBest.size() / numfeascuts, SEP); // 12
  fprintf(myfile, "%2.3f%c",
      1.0 - (double) (ObjLBest.size() + ObjOBest.size()) / numfeascuts,
      SEP); // 13
  fprintf(myfile, "%2.3f%c", (double) ObjOBest.size() / numfeascuts, SEP); // 14
  //////////
  // Root node bound
  //////////
  fprintf(myfile, "%2.6f%c", OSICBoundAll, SEP); // 15
  fprintf(myfile, "%2.6f%c", LSICBoundInit, SEP); // 16
  fprintf(myfile, "%2.6f%c", LSICBoundAll, SEP); // 17
  fprintf(myfile, "%2.6f%c", LSICAndOSICBoundInit, SEP); // 18
  fprintf(myfile, "%2.6f%c", LSICAndOSICBoundAll, SEP); // 19
  if (greaterThanVal(LSICAndOSICBoundAll, OSICBoundAll, param.getDIFFEPS())) {
    fprintf(myfile, "%d%c", 1, SEP); // 20
  } else {
    fprintf(myfile, "%d%c", 0, SEP); // 20
  }
  if (greaterThanVal(LSICBoundAll, LSICBoundInit, param.getDIFFEPS())) {
    fprintf(myfile, "%d%c", 1, SEP); // 21
  } else {
    fprintf(myfile, "%d%c", 0, SEP); // 21
  }
  //////////
  // Basis cond num
  //////////
  fprintf(myfile, "%2.3f%c", OSICBasisCondNum, SEP); // 22
  fprintf(myfile, "%2.3f%c", LSICBasisCondNumInit, SEP); // 23
  fprintf(myfile, "%2.3f%c", LSICBasisCondNumAll, SEP); // 24
  fprintf(myfile, "%2.3f%c", LSICAndOSICBasisCondNumInit, SEP); // 25
  fprintf(myfile, "%2.3f%c", LSICAndOSICBasisCondNumAll, SEP); // 26
  fflush(myfile);
}*/

/***********************************************************************/
/**
 * Writes which settings we are using to instance info,
 * as well as the number of points and rays generated
 */
void writeGICPointInfoToLog(const int hplaneHeur, const int nextHplaneFinalFlag,
    const int numHplanesPerRay, const int numRaysOfC1,
    const double avgNumParallelRays, const double avg_num_SIC_final_points,
    const int numRaysToBeCut, const int numCutGenSets, const int numRounds,
    std::vector<int> &numPoints, std::vector<int> &numFinalPoints,
    std::vector<int> &numRays, const int totalNumPoints,
    const int totalNumFinalPoints, const int totalNumRays,
    const std::vector<double>& avgSICDepth,
    const std::vector<double>& minSICDepth,
    const std::vector<double>& maxSICDepth,
    const std::vector<double>& avgObjDepth,
    const std::vector<double>& minObjDepth,
    const std::vector<double>& maxObjDepth, FILE* myfile) {
  if (myfile == NULL)
    return;
  fprintf(myfile, "%d%c", hplaneHeur, SEP); // 1
  fprintf(myfile, "%d%c", nextHplaneFinalFlag, SEP); // 27
  fprintf(myfile, "%d%c", numHplanesPerRay, SEP); // 2
  fprintf(myfile, "%d%c", numRaysOfC1, SEP); // 19
  fprintf(myfile, "%2.3f%c", avgNumParallelRays, SEP); // 26
  fprintf(myfile, "%2.3f%c", avg_num_SIC_final_points, SEP); // 18
  fprintf(myfile, "%d%c", numRaysToBeCut, SEP); // 3
  fprintf(myfile, "%d%c", numCutGenSets, SEP); // 4
  fprintf(myfile, "%d%c", numRounds, SEP); // 5
  fprintf(myfile, "%d%c", totalNumPoints, SEP); // 6
  fprintf(myfile, "%2.3f%c", computeAverage(numPoints), SEP); // 7
  fprintf(myfile, "%d%c", *computeMin(numPoints), SEP); // 8
  fprintf(myfile, "%d%c", *computeMax(numPoints), SEP); // 9
  fprintf(myfile, "%d%c", totalNumFinalPoints, SEP); // 10
  fprintf(myfile, "%2.3f%c", computeAverage(numFinalPoints), SEP); // 11
  fprintf(myfile, "%d%c", *computeMin(numFinalPoints), SEP); // 12
  fprintf(myfile, "%d%c", *computeMax(numFinalPoints), SEP); // 13
  fprintf(myfile, "%d%c", totalNumRays, SEP); // 14
  fprintf(myfile, "%2.3f%c", computeAverage(numRays), SEP); // 15
  fprintf(myfile, "%d%c", *computeMin(numRays), SEP); // 16
  fprintf(myfile, "%d%c", *computeMax(numRays), SEP); // 17
  fprintf(myfile, "%2.3f%c", computeAverage(avgSICDepth), SEP); // 20
  fprintf(myfile, "%2.3f%c", computeAverage(minSICDepth), SEP); // 21
  fprintf(myfile, "%2.3f%c", computeAverage(maxSICDepth), SEP); // 22
  fprintf(myfile, "%2.3f%c", computeAverage(avgObjDepth), SEP); // 23
  fprintf(myfile, "%2.3f%c", computeAverage(minObjDepth), SEP); // 24
  fprintf(myfile, "%2.3f%c", computeAverage(maxObjDepth), SEP); // 25 // 26 27 above
  // 18
  fflush(myfile);
}

/***********************************************************************/
/**
 fprintf(myfile, "%s%c", "NUM FEAS SUB SICS", SEP); // 1
 fprintf(myfile, "%s%c", "NUM SPLITS - (SPLITS: 1 GIC = SIC)", SEP); // 2 This is the number of `successful' sub GIC splits
 fprintf(myfile, "%s%c", "NUM GICS GENERATED", SEP); // 3
 // fprintf(myfile, "%s%c", "NUM TIMES SUB SIC GENERATED", SEP); // 4
 fprintf(myfile, "%s%c", "NUM ROUNDS", SEP); // 5
 fprintf(myfile, "%s%c", "AVG GICS PER SPLIT", SEP); // 6
 fprintf(myfile, "%s%c", "AVG GICS PER SPLIT (EXCEPT 1 GIC = SIC)", SEP); // 7
 fprintf(myfile, "%s%c", "%% SUB GIC EUCL > SUB SIC EUCL", SEP); // 8
 fprintf(myfile, "%s%c", "%% SUB GIC EUCL = SUB SIC EUCL", SEP); // 9
 fprintf(myfile, "%s%c", "%% SUB GIC EUCL < SUB SIC EUCL", SEP); // 10
 fprintf(myfile, "%s%c", "(>) GIC - SIC EUCL (MIN)", SEP); // 11
 fprintf(myfile, "%s%c", "(>) GIC - SIC EUCL (MAX)", SEP); // 12
 fprintf(myfile, "%s%c", "(>) GIC - SIC EUCL (AVG)", SEP); // 13
 fprintf(myfile, "%s%c", "(<) GIC - SIC EUCL (MIN)", SEP); // 14
 fprintf(myfile, "%s%c", "(<) GIC - SIC EUCL (MAX)", SEP); // 15
 fprintf(myfile, "%s%c", "(<) GIC - SIC EUCL (AVG)", SEP); // 16
 fprintf(myfile, "%s%c", "OBJ POST ALL SUB GICS", SEP); // 17
 fprintf(myfile, "%s%c", "OBJ POST ALL SUB SICS", SEP); // 18
 */
/*void writeCompSGIC_SSICToII(AdvCuts &GIC, //AdvCuts &SIC,
    SolutionInfo &solnInfoSub, int numGICs, int num1GICIsSIC, int numRounds,
    std::vector<int> &GICsOnSplit,
    std::vector<int> &cutIndexOfSingleGICOnSplit, std::vector<int> &EuclGIC,
    std::vector<int> &EuclSIC, double minEuclGICSub, double maxEuclGICSub,
    double avgEuclGICSub, double minEuclSICSub, double maxEuclSICSub,
    double avgEuclSICSub, double GICBound, double GICBasisCondNumber,
    double SICBound, double SICBasisCondNumber, double SICAndGICBound,
    double SICAndGICBasisCondNumber, FILE* myfile) {
  if (myfile == NULL)
    return;
  fprintf(myfile, "%d%c", (int) solnInfoSub.numFeasSplits, SEP); // 1
  fprintf(myfile, "%d%c", (int) solnInfoSub.numFeasSplits - num1GICIsSIC,
      SEP); // 2 This is the number of `successful' sub GIC splits
  fprintf(myfile, "%d%c", numGICs, SEP); // 3
  // fprintf(myfile, "%s%c", "NUM TIMES SUB SIC GENERATED", SEP); // 4
  fprintf(myfile, "%d%c", numRounds, SEP); // 5
  fprintf(myfile, "%f%c", computeAverage(GICsOnSplit), SEP); // 6

  int sum = 0;
  int n = 0;
  for (int split = 0; split < (int) GICsOnSplit.size(); split++) {
    if (cutIndexOfSingleGICOnSplit[split] == -1) {
      sum += GICsOnSplit[split];
      n++;
    }
  }
  if (n != 0) {
    fprintf(myfile, "%f%c", (double) sum / n, SEP); // 7
  } else {
    fprintf(myfile, "%f%c", (double) 0.0, SEP); // 7
  }
  fprintf(myfile, "%f%c", (double) EuclGIC.size() / numGICs, SEP); // 8
  fprintf(myfile, "%f%c",
      ((double) (numGICs - EuclGIC.size() - EuclSIC.size())) / numGICs,
      SEP); // 9
  fprintf(myfile, "%f%c", (double) EuclSIC.size() / numGICs, SEP); // 10
  fprintf(myfile, "%f%c", minEuclGICSub, SEP); // 11
  fprintf(myfile, "%f%c", maxEuclGICSub, SEP); // 12
  fprintf(myfile, "%f%c", avgEuclGICSub, SEP); // 13
  fprintf(myfile, "%f%c", minEuclSICSub, SEP); // 14
  fprintf(myfile, "%f%c", maxEuclSICSub, SEP); // 15
  fprintf(myfile, "%f%c", avgEuclSICSub, SEP); // 16
  if (!isInfinity(GICBound, param.getINFINITY(), param.getEPS())) {
    fprintf(myfile, "%f%c", GICBound, SEP); // 17
  } else {
    fprintf(myfile, "infeas%c", SEP); // 17
  }
  fprintf(myfile, "%f%c", GICBasisCondNumber, SEP); // 18
  if (!isInfinity(SICBound, param.getINFINITY(), param.getEPS())) {
    fprintf(myfile, "%f%c", SICBound, SEP); // 19
  } else {
    fprintf(myfile, "infeas%c", SEP); // 19
  }
  fprintf(myfile, "%f%c", SICBasisCondNumber, SEP); // 20
  if (!isInfinity(SICAndGICBound, param.getINFINITY(), param.getEPS())) {
    fprintf(myfile, "%f%c", SICAndGICBound, SEP); // 21
  } else {
    fprintf(myfile, "infeas%c", SEP); // 21
  }
  fprintf(myfile, "%f%c", SICAndGICBasisCondNumber, SEP); // 22
  fflush(myfile);
}*/

/**
 fprintf(myfile, "%s%c", "NUM FEAS SSICS", SEP); // 1
 fprintf(myfile, "%s%c", "NUM SGICS", SEP); // 2
 fprintf(myfile, "%s%c", "AVG NUM LSICS PIVOTS AVAILABLE", SEP); // 3
 fprintf(myfile, "%s%c", "AVG NUM LGICS PIVOTS AVAILABLE", SEP); // 4
 fprintf(myfile, "%s%c", "NUM LSICS", SEP); // 5
 fprintf(myfile, "%s%c", "NUM LGICS", SEP); // 6
 fprintf(myfile, "%s%c", "NUM OSICS", SEP); // 7
 fprintf(myfile, "%s%c", "% INIT LGIC EUCL > INIT LSIC EUCL", SEP); // 8
 fprintf(myfile, "%s%c", "% INIT LGIC EUCL = INIT LSIC EUCL", SEP); // 9
 fprintf(myfile, "%s%c", "% INIT LGIC EUCL < INIT LSIC EUCL", SEP); // 10
 fprintf(myfile, "%s%c", "% BEST LGIC EUCL > BEST LSIC EUCL", SEP); // 11
 fprintf(myfile, "%s%c", "% BEST LGIC EUCL = BEST LSIC EUCL", SEP); // 12
 fprintf(myfile, "%s%c", "% BEST LGIC EUCL < BEST LSIC EUCL", SEP); // 13
 fprintf(myfile, "%s%c", "% INIT LGIC EUCL > OSIC EUCL", SEP); // 14
 fprintf(myfile, "%s%c", "% INIT LGIC EUCL = OSIC EUCL", SEP); // 15
 fprintf(myfile, "%s%c", "% INIT LGIC EUCL < OSIC EUCL", SEP); // 16
 fprintf(myfile, "%s%c", "% BEST LGIC EUCL > OSIC EUCL", SEP); // 17
 fprintf(myfile, "%s%c", "% BEST LGIC EUCL = OSIC EUCL", SEP); // 18
 fprintf(myfile, "%s%c", "% BEST LGIC EUCL < OSIC EUCL", SEP); // 19
 */
/*void writeCompLGIC_LSIC_OSICToLog(std::string &prob_name, int numFeasSSICs,
    int numSGICs, int numLGICs, int numLSICs, double avgNumPivots_SICs,
    double avgNumPivots_GICs, int numOSICs,
    std::vector<double> &Eucl_LGIC_v_LSIC_Init_LG,
    std::vector<double> &Eucl_LGIC_v_LSIC_Best_LG,
    std::vector<double> &Eucl_LGIC_v_LSIC_Init_LS,
    std::vector<double> &Eucl_LGIC_v_LSIC_Best_LS,
    std::vector<double> &Eucl_LGIC_v_OSIC_Init_LG,
    std::vector<double> &Eucl_LGIC_v_OSIC_Best_LG,
    std::vector<double> &Eucl_LGIC_v_OSIC_Init_O,
    std::vector<double> &Eucl_LGIC_v_OSIC_Best_O, FILE* myfile) {
  if (myfile == NULL)
    return;

  //////////
  // Info
  //////////
  fprintf(myfile, "%d%c", numFeasSSICs, SEP); // 1
  fprintf(myfile, "%d%c", numSGICs, SEP); // 1
  fprintf(myfile, "%2.3f%c", avgNumPivots_SICs, SEP); // 1
  fprintf(myfile, "%2.3f%c", avgNumPivots_GICs, SEP); // 1
  fprintf(myfile, "%d%c", numLSICs, SEP); // 1
  fprintf(myfile, "%d%c", numLGICs, SEP); // 2
  fprintf(myfile, "%d%c", numOSICs, SEP); // 2

  //////////
  // Eucl
  //////////
  // We compare to numSGICs, because there is one init LGIC per each SGIC TODO This might not be true in a later version

  // Init LGIC v LSIC
  fprintf(myfile, "%2.3f%c",
      (double) Eucl_LGIC_v_LSIC_Init_LG.size() / numSGICs, SEP); // 3
  fprintf(myfile, "%2.3f%c",
      1.0
          - (double) (Eucl_LGIC_v_LSIC_Init_LG.size()
              + Eucl_LGIC_v_LSIC_Init_LS.size()) / numSGICs, SEP); // 4
  fprintf(myfile, "%2.3f%c",
      (double) Eucl_LGIC_v_LSIC_Init_LS.size() / numSGICs, SEP); // 5

  // Best LGIC v LSIC
  fprintf(myfile, "%2.3f%c",
      (double) Eucl_LGIC_v_LSIC_Best_LG.size() / numSGICs, SEP); // 6
  fprintf(myfile, "%2.3f%c",
      1.0
          - (double) (Eucl_LGIC_v_LSIC_Best_LG.size()
              + Eucl_LGIC_v_LSIC_Best_LS.size()) / numSGICs, SEP); // 7
  fprintf(myfile, "%2.3f%c",
      (double) Eucl_LGIC_v_LSIC_Best_LS.size() / numSGICs, SEP); // 8

  // Here we are again using numSGICs, which should really equal numOSICs

  // Init LGIC v OSIC
  fprintf(myfile, "%2.3f%c",
      (double) Eucl_LGIC_v_OSIC_Init_LG.size() / numSGICs, SEP); // 3
  fprintf(myfile, "%2.3f%c",
      1.0
          - (double) (Eucl_LGIC_v_OSIC_Init_LG.size()
              + Eucl_LGIC_v_OSIC_Init_O.size()) / numSGICs, SEP); // 4
  fprintf(myfile, "%2.3f%c",
      (double) Eucl_LGIC_v_OSIC_Init_O.size() / numSGICs, SEP); // 5

  // Best LGIC v OSIC
  fprintf(myfile, "%2.3f%c",
      (double) Eucl_LGIC_v_OSIC_Best_LG.size() / numSGICs, SEP); // 6
  fprintf(myfile, "%2.3f%c",
      1.0
          - (double) (Eucl_LGIC_v_OSIC_Best_LG.size()
              + Eucl_LGIC_v_OSIC_Best_O.size()) / numSGICs, SEP); // 7
  fprintf(myfile, "%2.3f%c",
      (double) Eucl_LGIC_v_OSIC_Best_O.size() / numSGICs, SEP); // 8

  fflush(myfile);
}*/

/**
 fprintf(myfile, "%s%c", "INIT LGIC BOUND", SEP); // 1
 fprintf(myfile, "%s%c", "ALL LGIC BOUND", SEP); // 2
 fprintf(myfile, "%s%c", "INIT LSIC BOUND", SEP); // 3
 fprintf(myfile, "%s%c", "ALL LSIC BOUND", SEP); // 4
 fprintf(myfile, "%s%c", "OSIC BOUND", SEP); // 5
 fprintf(myfile, "%s%c", "INIT LGIC + INIT LSIC BOUND", SEP); // 6
 fprintf(myfile, "%s%c", "ALL LGIC + ALL LSIC BOUND", SEP); // 7
 fprintf(myfile, "%s%c", "INIT LGIC + OSIC BOUND", SEP); // 8
 fprintf(myfile, "%s%c", "ALL LGIC + OSIC BOUND", SEP); // 9
 fprintf(myfile, "%s%c", "LGIC_All+OSIC > OSIC", SEP); // 10
 fprintf(myfile, "%s%c", "LGIC_All > LGIC_INIT", SEP); // 11
 fprintf(myfile, "%s%c", "LGIC_INIT+LSIC_INIT > LSIC_INIT", SEP); // 12
 fprintf(myfile, "%s%c", "LGIC_ALL+LSIC_ALL > LSIC_ALL", SEP); // 13
 */
void writeRootBoundToLog(const double lp_opt, const double strongBranchingLB,
    const double strongBranchingUB, const int numSICs, const double sic_opt,
    const int numGMICuts, const double gmi_opt, const int numLandPCuts,
    const double landp_opt, const int numTilted, const double tilted_opt,
    const int numVPCs, const double vpc_opt, const int numPHA,
    const double pha_opt, const double sic_tilted_opt,
    const double gmi_tilted_opt, const double sic_vpc_opt,
    const double gmi_vpc_opt, const double sic_pha_opt,
    const double gmi_pha_opt, const double tilted_vpc_pha_opt,
    const double sic_tilted_vpc_pha_opt, const double gmi_tilted_vpc_pha_opt,
    const int num_active_sics_sicsolver, const int num_active_tilted_sicsolver,
    const int num_active_vpc_sicsolver, const int num_active_pha_sicsolver,
    const int num_active_gmi_gmisolver, const int num_active_tilted_gmisolver,
    const int num_active_vpc_gmisolver, const int num_active_pha_gmisolver,
    FILE *myfile) {
  if (myfile == NULL)
    return;

  const bool GICsOverSICs = greaterThanVal(tilted_vpc_pha_opt, sic_opt);
  const bool allOverSICs = greaterThanVal(sic_tilted_vpc_pha_opt, sic_opt);
  const bool GICsOverGMI = greaterThanVal(tilted_vpc_pha_opt, gmi_opt);
  const bool allOverGMI = greaterThanVal(gmi_tilted_vpc_pha_opt, gmi_opt);

  //////////
  // Root node bound
  //////////
  fprintf(myfile, "%.20f%c", lp_opt, SEP);
  fprintf(myfile, "%s%c", stringValue(strongBranchingLB, "%2.20f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(strongBranchingUB, "%2.20f").c_str(), SEP);
  fprintf(myfile, "%d%c", numSICs, SEP);
  fprintf(myfile, "%s%c", stringValue(sic_opt, "%2.20f").c_str(), SEP);
  fprintf(myfile, "%d%c", numGMICuts, SEP);
  fprintf(myfile, "%s%c", stringValue(gmi_opt, "%2.20f").c_str(), SEP);
  fprintf(myfile, "%d%c", numLandPCuts, SEP);
  fprintf(myfile, "%s%c", stringValue(landp_opt, "%2.20f").c_str(), SEP);
  fprintf(myfile, "%d%c", numTilted, SEP);
  fprintf(myfile, "%s%c", stringValue(tilted_opt, "%2.20f").c_str(), SEP);
  fprintf(myfile, "%d%c", numVPCs, SEP);
  fprintf(myfile, "%s%c", stringValue(vpc_opt, "%2.20f").c_str(), SEP);
  fprintf(myfile, "%d%c", numPHA, SEP);
  fprintf(myfile, "%s%c", stringValue(pha_opt, "%2.20f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(sic_tilted_opt, "%2.20f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(gmi_tilted_opt, "%2.20f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(sic_vpc_opt, "%2.20f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(gmi_vpc_opt, "%2.20f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(sic_pha_opt, "%2.20f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(gmi_pha_opt, "%2.20f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(tilted_vpc_pha_opt, "%2.20f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(sic_tilted_vpc_pha_opt, "%2.20f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(gmi_tilted_vpc_pha_opt, "%2.20f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(
      GICsOverSICs * (tilted_vpc_pha_opt - sic_opt), "%2.6f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(
      allOverSICs * (sic_tilted_vpc_pha_opt - sic_opt), "%2.6f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(
        GICsOverGMI * (tilted_vpc_pha_opt - gmi_opt), "%2.6f").c_str(), SEP);
    fprintf(myfile, "%s%c", stringValue(
        allOverGMI * (gmi_tilted_vpc_pha_opt - gmi_opt), "%2.6f").c_str(), SEP);
  fprintf(myfile, "%d%c", num_active_sics_sicsolver, SEP);
  fprintf(myfile, "%d%c", num_active_tilted_sicsolver, SEP);
  fprintf(myfile, "%d%c", num_active_vpc_sicsolver, SEP);
  fprintf(myfile, "%d%c", num_active_pha_sicsolver, SEP);
  fprintf(myfile, "%d%c", num_active_gmi_gmisolver, SEP);
  fprintf(myfile, "%d%c", num_active_tilted_gmisolver, SEP);
  fprintf(myfile, "%d%c", num_active_vpc_gmisolver, SEP);
  fprintf(myfile, "%d%c", num_active_pha_gmisolver, SEP);
  if (!isInfinity(std::abs(GlobalVariables::bestObjValue))) {
    const double ip_opt = GlobalVariables::bestObjValue;
    fprintf(myfile, "%2.6f%c", 100. * (sic_opt - lp_opt) / (ip_opt - lp_opt), SEP);  // 11
    fprintf(myfile, "%2.6f%c", 100. * (gmi_opt - lp_opt) / (ip_opt - lp_opt), SEP);  // 12
    fprintf(myfile, "%2.6f%c", 100. * (landp_opt - lp_opt) / (ip_opt - lp_opt), SEP);  // 12
    fprintf(myfile, "%2.6f%c", 100. * (tilted_opt - lp_opt) / (ip_opt - lp_opt), SEP);  // 12
    fprintf(myfile, "%2.6f%c", 100. * (vpc_opt - lp_opt) / (ip_opt - lp_opt), SEP);  // 12
    fprintf(myfile, "%2.6f%c", 100. * (pha_opt - lp_opt) / (ip_opt - lp_opt), SEP);  // 12
    fprintf(myfile, "%2.6f%c", 100. * (sic_tilted_opt - lp_opt) / (ip_opt - lp_opt), SEP);  // 13
    fprintf(myfile, "%2.6f%c", 100. * (gmi_tilted_opt - lp_opt) / (ip_opt - lp_opt), SEP);  // 13
    fprintf(myfile, "%2.6f%c", 100. * (sic_vpc_opt - lp_opt) / (ip_opt - lp_opt), SEP);  // 13
    fprintf(myfile, "%2.6f%c", 100. * (gmi_vpc_opt - lp_opt) / (ip_opt - lp_opt), SEP);  // 13
    fprintf(myfile, "%2.6f%c", 100. * (sic_pha_opt - lp_opt) / (ip_opt - lp_opt), SEP);  // 13
    fprintf(myfile, "%2.6f%c", 100. * (gmi_pha_opt - lp_opt) / (ip_opt - lp_opt), SEP);  // 13
  } else {
    fprintf(myfile, "%c",SEP);
    fprintf(myfile, "%c",SEP);
    fprintf(myfile, "%c",SEP);
    fprintf(myfile, "%c",SEP);
    fprintf(myfile, "%c",SEP);
    fprintf(myfile, "%c",SEP);
    fprintf(myfile, "%c",SEP);
    fprintf(myfile, "%c",SEP);
    fprintf(myfile, "%c",SEP);
    fprintf(myfile, "%c",SEP);
    fprintf(myfile, "%c",SEP);
    fprintf(myfile, "%c",SEP);
  }
  fflush(myfile);
} /* writeRootBoundToII */

void writeRoundInfoToLog(const double sic_vpc_opt, const double sic_pha_opt,
    const int total_num_sics, const int num_sic_rounds,
    const double final_sic_bound,
    const std::vector<double>& boundByRoundVPCPlus,
    const std::vector<double>& boundByRoundPHAPlus, const double LPBasisCond,
    const std::vector<double>& basisCondByRoundSIC,
    const std::vector<double>& basisCondByRoundVPC,
    const std::vector<double>& basisCondByRoundVPCPlus,
    const std::vector<double>& basisCondByRoundPHA,
    const std::vector<double>& basisCondByRoundPHAPlus,
    const double minSICOrthogonalityInit, const double maxSICOrthogonalityInit,
    const double avgSICOrthogonalityInit, const double minVPCOrthogonalityInit,
    const double maxVPCOrthogonalityInit, const double avgVPCOrthogonalityInit,
    const double minPHAOrthogonalityInit, const double maxPHAOrthogonalityInit,
    const double avgPHAOrthogonalityInit, const double minSICOrthogonalityFinal,
    const double maxSICOrthogonalityFinal,
    const double avgSICOrthogonalityFinal,
    const double minVPCOrthogonalityFinal,
    const double maxVPCOrthogonalityFinal,
    const double avgVPCOrthogonalityFinal,
    const double minPHAOrthogonalityFinal,
    const double maxPHAOrthogonalityFinal,
    const double avgPHAOrthogonalityFinal, FILE *myfile) {
  if (myfile == NULL)
    return;
  const int numRoundsVPC = boundByRoundVPCPlus.size();
  const int numRoundsPHA = boundByRoundPHAPlus.size();
  // SICs
  const double SICBasisCondInit =
      (num_sic_rounds > 0) ? basisCondByRoundSIC[0] : 0.;
  const double SICBasisCondFinal =
      (num_sic_rounds > 0) ? basisCondByRoundSIC[num_sic_rounds - 1] : 0.;
  // VPCs
  const double VPCPlusBoundRd1 =
      (numRoundsVPC > 0) ? boundByRoundVPCPlus[0] : sic_vpc_opt;
  const double VPCPlusBoundRd2 =
      (numRoundsVPC > 1) ? boundByRoundVPCPlus[1] : sic_vpc_opt;
  const double VPCPlusBoundRd5 =
      (numRoundsVPC > 4) ? boundByRoundVPCPlus[4] : sic_vpc_opt;
  const double VPCBasisCondRd1 =
      (numRoundsVPC > 0) ? basisCondByRoundVPC[0] : 0.;
  const double VPCBasisCondRd2 =
      (numRoundsVPC > 1) ? basisCondByRoundVPC[1] :
      (numRoundsVPC > 0) ? basisCondByRoundVPC[numRoundsVPC - 1] : 0.;
  const double VPCBasisCondRd5 =
      (numRoundsVPC > 4) ? basisCondByRoundVPC[4] :
      (numRoundsVPC > 0) ? basisCondByRoundVPC[numRoundsVPC - 1] : 0.;
  const double VPCBasisCondFinal =
      (numRoundsVPC > 0) ? basisCondByRoundVPC[numRoundsVPC - 1] : 0.;
  const double VPCPlusBasisCondRd1 =
      (numRoundsVPC > 0) ? basisCondByRoundVPCPlus[0] : 0.;
  const double VPCPlusBasisCondRd2 =
      (numRoundsVPC > 1) ? basisCondByRoundVPCPlus[1] :
      (numRoundsVPC > 0) ? basisCondByRoundVPCPlus[numRoundsVPC - 1] : 0.;
  const double VPCPlusBasisCondRd5 =
      (numRoundsVPC > 4) ? basisCondByRoundVPCPlus[4] :
      (numRoundsVPC > 0) ? basisCondByRoundVPCPlus[numRoundsVPC - 1] : 0.;
  const double VPCPlusBasisCondFinal =
      (numRoundsVPC > 0) ? basisCondByRoundVPCPlus[numRoundsVPC - 1] : 0.;
  // PHA
  const double PHAPlusBoundRd1 =
      (numRoundsPHA > 0) ? boundByRoundPHAPlus[0] : sic_pha_opt;
  const double PHAPlusBoundRd2 =
      (numRoundsPHA > 1) ? boundByRoundPHAPlus[1] : sic_pha_opt;
  const double PHAPlusBoundRd5 =
      (numRoundsPHA > 4) ? boundByRoundPHAPlus[4] : sic_pha_opt;
  const double PHABasisCondRd1 =
      (numRoundsPHA > 0) ? basisCondByRoundPHA[0] : 0.;
  const double PHABasisCondRd2 =
      (numRoundsPHA > 1) ? basisCondByRoundPHA[1] :
      (numRoundsPHA > 0) ? basisCondByRoundPHA[numRoundsPHA - 1] : 0.;
  const double PHABasisCondRd5 =
      (numRoundsPHA > 4) ? basisCondByRoundPHA[4] :
      (numRoundsPHA > 0) ? basisCondByRoundPHA[numRoundsPHA - 1] : 0.;
  const double PHABasisCondFinal =
      (numRoundsPHA > 0) ? basisCondByRoundPHA[numRoundsPHA - 1] : 0.;
  const double PHAPlusBasisCondRd1 =
      (numRoundsPHA > 0) ? basisCondByRoundPHAPlus[0] : 0.;
  const double PHAPlusBasisCondRd2 =
      (numRoundsPHA > 1) ? basisCondByRoundPHAPlus[1] :
      (numRoundsPHA > 0) ? basisCondByRoundPHAPlus[numRoundsPHA - 1] : 0.;
  const double PHAPlusBasisCondRd5 =
      (numRoundsPHA > 4) ? basisCondByRoundPHAPlus[4] :
      (numRoundsPHA > 0) ? basisCondByRoundPHAPlus[numRoundsPHA - 1] : 0.;
  const double PHAPlusBasisCondFinal =
      (numRoundsPHA > 0) ? basisCondByRoundPHAPlus[numRoundsPHA - 1] : 0.;
  fprintf(myfile, "%d%c", total_num_sics, SEP);
  fprintf(myfile, "%d%c", num_sic_rounds, SEP);
  fprintf(myfile, "%s%c", stringValue(final_sic_bound, "%2.6f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(VPCPlusBoundRd1, "%2.6f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(VPCPlusBoundRd2, "%2.6f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(VPCPlusBoundRd5, "%2.6f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(PHAPlusBoundRd1, "%2.6f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(PHAPlusBoundRd2, "%2.6f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(PHAPlusBoundRd5, "%2.6f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(LPBasisCond, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(SICBasisCondInit, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(SICBasisCondFinal, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(VPCBasisCondRd1, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(VPCBasisCondRd2, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(VPCBasisCondRd5, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(VPCBasisCondFinal, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(VPCPlusBasisCondRd1, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(VPCPlusBasisCondRd2, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(VPCPlusBasisCondRd5, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(VPCPlusBasisCondFinal, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(PHABasisCondRd1, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(PHABasisCondRd2, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(PHABasisCondRd5, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(PHABasisCondFinal, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(PHAPlusBasisCondRd1, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(PHAPlusBasisCondRd2, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(PHAPlusBasisCondRd5, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(PHAPlusBasisCondFinal, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(minSICOrthogonalityInit, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(maxSICOrthogonalityInit, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(avgSICOrthogonalityInit, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(minVPCOrthogonalityInit, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(maxVPCOrthogonalityInit, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(avgVPCOrthogonalityInit, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(minPHAOrthogonalityInit, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(maxPHAOrthogonalityInit, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(avgPHAOrthogonalityInit, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(minSICOrthogonalityFinal, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(maxSICOrthogonalityFinal, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(avgSICOrthogonalityFinal, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(minVPCOrthogonalityFinal, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(maxVPCOrthogonalityFinal, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(avgVPCOrthogonalityFinal, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(minPHAOrthogonalityFinal, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(maxPHAOrthogonalityFinal, "%2.3f").c_str(), SEP);
  fprintf(myfile, "%s%c", stringValue(avgPHAOrthogonalityFinal, "%2.3f").c_str(), SEP);
  fflush(myfile);
} /* writeRoundInfoToII */

void writeBBInforToLog(const BBInfo& best_bb_info_mycuts, const BBInfo& best_bb_info_allcuts,
    const BBInfo& avg_bb_info_mycuts, const BBInfo& avg_bb_info_allcuts,
    const std::vector<BBInfo>& vec_bb_info_mycuts, const std::vector<BBInfo>& vec_bb_info_allcuts,
//    const double avg_bb_opt_mycuts, const double avg_bb_opt_allcuts,
//    const long avg_bb_iters_mycuts, const long avg_bb_iters_allcuts,
//    const long avg_bb_nodes_mycuts, const long avg_bb_nodes_allcuts,
//    const long avg_bb_num_root_passes_mycuts, const long avg_bb_num_root_passes_allcuts,
//    const double avg_bb_first_cut_pass_mycuts, const double avg_bb_first_cut_pass_allcuts,
//    const double avg_bb_last_cut_pass_mycuts, const double avg_bb_last_cut_pass_allcuts,
//    const double avg_bb_time_mycuts, const double avg_bb_time_allcuts,
//    const std::string str_bb_opt_mycuts, const std::string str_bb_opt_allcuts,
//    const std::string str_bb_iters_mycuts,
//    const std::string str_bb_iters_allcuts,
//    const std::string str_bb_nodes_mycuts,
//    const std::string str_bb_nodes_allcuts,
//    const std::string str_bb_num_root_passes_mycuts,
//    const std::string str_bb_num_root_passes_allcuts,
//    const std::string str_bb_first_cut_pass_mycuts,
//    const std::string str_bb_first_cut_pass_allcuts,
//    const std::string str_bb_last_cut_pass_mycuts,
//    const std::string str_bb_last_cut_pass_allcuts,
//    const std::string str_bb_time_mycuts,
//    const std::string str_bb_time_allcuts,
    FILE *myfile) {
  if (myfile == NULL) {
    return;
  }

  if (vec_bb_info_mycuts.size() == 0) {
    for (int i = 0; i < countBBInfoEntries; i++) {
      fprintf(myfile, "%c", SEP);
    }
    fflush(myfile);
    return;
  }

  // First
  printBBInfo(vec_bb_info_mycuts[0], vec_bb_info_allcuts[0], myfile, false, SEP);

  // Min
  printBBInfo(best_bb_info_mycuts, best_bb_info_allcuts, myfile, false, SEP);

  // Average
  printBBInfo(avg_bb_info_mycuts, avg_bb_info_allcuts, myfile, false, SEP);

  // All
  std::vector<std::string> vec_str_mycuts, vec_str_allcuts;
  createStringFromBBInfoVec(vec_bb_info_mycuts, vec_str_mycuts);
  createStringFromBBInfoVec(vec_bb_info_allcuts, vec_str_allcuts);
  for (int i = 0; i < (int) vec_str_mycuts.size(); i++) {
    fprintf(myfile, "%s%c", vec_str_mycuts[i].c_str(), SEP);
    fprintf(myfile, "%s%c", vec_str_allcuts[i].c_str(), SEP);
  }
  fflush(myfile);
} /* writeBBInforToII */

/*
 fprintf(myfile, "%s%c", "INIT LGIC BASIS COND", SEP); // 1
 fprintf(myfile, "%s%c", "ALL LGIC BASIS COND", SEP); // 2
 fprintf(myfile, "%s%c", "INIT LSIC BASIS COND", SEP); // 3
 fprintf(myfile, "%s%c", "ALL LSIC BASIS COND", SEP); // 4
 fprintf(myfile, "%s%c", "OSIC BASIS COND", SEP); // 5
 fprintf(myfile, "%s%c", "INIT LGIC + INIT LSIC BASIS COND", SEP); // 6
 fprintf(myfile, "%s%c", "ALL LGIC + ALL LSIC BASIS COND", SEP); // 7
 fprintf(myfile, "%s%c", "INIT LGIC + OSIC BASIS COND", SEP); // 8
 fprintf(myfile, "%s%c", "ALL LGIC + OSIC BASIS COND", SEP); // 9
 */
void writeCondNumToLog(double LGICBasisCondNumInit, double LGICBasisCondNumAll,
    double LSICBasisCondNumInit, double LSICBasisCondNumAll,
    double OSICBasisCondNum, double LGICAndLSICBasisCondNumInit,
    double LGICAndLSICBasisCondNumAll, double LGICAndOSICBasisCondNumInit,
    double LGICAndOSICBasisCondNumAll, FILE* myfile) {
  if (myfile == NULL)
    return;
  //////////
  // Basis cond num
  //////////
  fprintf(myfile, "%2.3f%c", LGICBasisCondNumInit, SEP); // 1
  fprintf(myfile, "%2.3f%c", LGICBasisCondNumAll, SEP); // 2
  fprintf(myfile, "%2.3f%c", LSICBasisCondNumInit, SEP); // 3
  fprintf(myfile, "%2.3f%c", LSICBasisCondNumAll, SEP); // 4
  fprintf(myfile, "%2.3f%c", OSICBasisCondNum, SEP); // 5
  fprintf(myfile, "%2.3f%c", LGICAndLSICBasisCondNumInit, SEP); // 6
  fprintf(myfile, "%2.3f%c", LGICAndLSICBasisCondNumAll, SEP); // 7
  fprintf(myfile, "%2.3f%c", LGICAndOSICBasisCondNumInit, SEP); // 7
  fprintf(myfile, "%2.3f%c", LGICAndOSICBasisCondNumAll, SEP); // 7
} /* writeCondNumToII */

/***********************************************************************/
/**
 * @brief Write information about cuts in original space to instance info file
 * // LIFTED GIC OBJ, LIFTED GIC EUCL DIST, LIFTED SIC OBJ, LIFTED SIC EUCL DIST, ORIG SPACE SIC OBJ, ORIG SPACE SIC EUCL DIST
 */
void writeLiftedInfoToLog(double liftedGICObj,
    double liftedGICEuclDist, double liftedSICObj, double liftedSICEuclDist,
    double origSICObj, double origSICEuclDist, //std::string &prob_name,
    FILE *myfile) {
//#ifdef TRACE
//  printf(
//      "\n############### Starting writeLiftedInfoToInstanceInfo routine for problem %s. ###############\n",
//      prob_name.c_str());
//#endif
  if (myfile == NULL)
    return;

  fprintf(myfile, "%2.3f%c", liftedGICObj, SEP);
  fprintf(myfile, "%2.3f%c", liftedGICEuclDist, SEP);
  fprintf(myfile, "%2.3f%c", liftedSICObj, SEP);
  fprintf(myfile, "%2.3f%c", liftedSICEuclDist, SEP);
  fprintf(myfile, "%2.3f%c", origSICObj, SEP);
  fprintf(myfile, "%2.3f%c", origSICEuclDist, SEP);

#ifdef TRACE
  printf("\n## Lifted cut info (all cuts added) written for problem %s. ##\n",
      prob_name.c_str());
#endif
}

/***********************************************************************/
/**
 * @brief Appends information about VPCs (cut heuristics that worked and fails)
 */
void writeCutInfoToLog(const int totalNumCuts, const int minSupportSICs,
    const int maxSupportSICs, const double avgSupportSICs,
    const double stdDevSupportSICs, const int minSupportVPCs,
    const int maxSupportVPCs, const double avgSupportVPCs,
    const double stdDevSupportVPCs, const double minOrthoWithObj,
    const double maxOrthoWithObj, const double avgOrthoWithObj,
    const int totalNumObj, const std::vector<int>& active_cut_heur_sicsolver,
    const std::vector<int>& active_cut_heur_gmisolver, FILE *myfile) {
  if (myfile == NULL)
    return;
  // Number cgs actually used
  fprintf(myfile, "%d%c", GlobalVariables::numCgsActuallyUsed, SEP);
  // Number cgs leading to cuts
  fprintf(myfile, "%d%c", GlobalVariables::numCgsLeadingToCuts, SEP);
  // Number cgs - num times PR collection was primal infeasible
  fprintf(myfile, "%d%c",
      GlobalVariables::numCgsActuallyUsed
          - GlobalVariables::numCutSolverFails[CutSolverFails::PRIMAL_CUTSOLVER_FAIL_IND],
      SEP);
  // Number of cgs with 1 facet (exactly) infeasible
  fprintf(myfile, "%d%c", GlobalVariables::numCgsOneDisjTermInfeas, SEP);

  // Number of terms
  fprintf(myfile, "%d%c", GlobalVariables::numDisjTerms, SEP);
  // Size of PRLP
  fprintf(myfile, "%.8f%c", GlobalVariables::minDensityPRLP, SEP);
  fprintf(myfile, "%.8f%c", GlobalVariables::maxDensityPRLP, SEP);
  fprintf(myfile, "%s%c", stringValue(GlobalVariables::minNumRowsPRLP).c_str(), SEP);
  fprintf(myfile, "%d%c", GlobalVariables::maxNumRowsPRLP, SEP);
  fprintf(myfile, "%s%c", stringValue(GlobalVariables::minNumColsPRLP).c_str(), SEP);
  fprintf(myfile, "%d%c", GlobalVariables::maxNumColsPRLP, SEP);
  fprintf(myfile, "%s%c", stringValue(GlobalVariables::minNumPointsPRLP).c_str(), SEP);
  fprintf(myfile, "%d%c", GlobalVariables::maxNumPointsPRLP, SEP);
  fprintf(myfile, "%d%c", GlobalVariables::totalNumPointsPRLP, SEP);
  fprintf(myfile, "%s%c", stringValue(GlobalVariables::minNumRaysPRLP).c_str(), SEP);
  fprintf(myfile, "%d%c", GlobalVariables::maxNumRaysPRLP, SEP);
  fprintf(myfile, "%d%c", GlobalVariables::totalNumRaysPRLP, SEP);
  // Number of b&b nodes taken to get partial tree
  fprintf(myfile, "%d%c", GlobalVariables::numPartialBBNodes, SEP);
  // Number of pruned nodes (subtracts out feasible integer ones)
  fprintf(myfile, "%d%c", GlobalVariables::numPrunedNodes, SEP);
  // Min depth
  fprintf(myfile, "%s%c", stringValue(GlobalVariables::minNodeDepth).c_str(), SEP);
  // Max depth
  fprintf(myfile, "%d%c", GlobalVariables::maxNodeDepth, SEP);

  // How many cuts total
  fprintf(myfile, "%d%c", totalNumCuts, SEP);
  // Support stats
  fprintf(myfile, "%s%c", stringValue(minSupportSICs).c_str(), SEP);
  fprintf(myfile, "%d%c", maxSupportSICs, SEP);
  fprintf(myfile, "%f%c", avgSupportSICs, SEP);
  fprintf(myfile, "%f%c", stdDevSupportSICs, SEP);
  fprintf(myfile, "%s%c", stringValue(minSupportVPCs).c_str(), SEP);
  fprintf(myfile, "%d%c", maxSupportVPCs, SEP);
  fprintf(myfile, "%f%c", avgSupportVPCs, SEP);
  fprintf(myfile, "%f%c", stdDevSupportVPCs, SEP);
  // Ortho with oj stats
  fprintf(myfile, "%f%c", minOrthoWithObj, SEP);
  fprintf(myfile, "%f%c", maxOrthoWithObj, SEP);
  fprintf(myfile, "%f%c", avgOrthoWithObj, SEP);
  // Cut heuristic stats
  for (int heur_ind = 0; heur_ind < CutHeuristics::NUM_CUT_HEUR; heur_ind++) {
    fprintf(myfile, "%d%c", GlobalVariables::numCutsFromHeur[heur_ind], SEP);
    fprintf(myfile, "%d%c", active_cut_heur_sicsolver[heur_ind], SEP);
    fprintf(myfile, "%d%c", active_cut_heur_gmisolver[heur_ind], SEP);
  }
  // How many objectives tried
  fprintf(myfile, "%d%c", totalNumObj, SEP);
  for (int heur_ind = 0; heur_ind < CutHeuristics::NUM_CUT_HEUR; heur_ind++) {
    fprintf(myfile, "%d%c", GlobalVariables::numObjFromHeur[heur_ind], SEP);
  }
  // Cut solver fails
  int totalNumFails = 0;
  for (int cut_fails_ind = 0;
      cut_fails_ind < CutSolverFails::NUM_CUTSOLVER_FAILS; cut_fails_ind++) {
    if ((GlobalConstants::CutSolverFailName[cut_fails_ind].find("NO_OBJ")
        == std::string::npos)
        && (GlobalConstants::CutSolverFailName[cut_fails_ind].find("SIC")
            == std::string::npos)) {
      totalNumFails += GlobalVariables::numCutSolverFails[cut_fails_ind];
    }
  }
  fprintf(myfile, "%d%c", totalNumFails, SEP);
  for (int cut_fails_ind = 0;
      cut_fails_ind < CutSolverFails::NUM_CUTSOLVER_FAILS; cut_fails_ind++) {
    fprintf(myfile, "%d%c", GlobalVariables::numCutSolverFails[cut_fails_ind],
        SEP);
  }
  fflush(myfile);
}

/***********************************************************************/
/**
 * @brief Appends information about time to solve various parts of the prob
 */
void writeTimeInfoToLog(Stats &timeStats, std::vector<std::string> &times,
    //const char* end_time_string,
    FILE *myfile) {
  if (myfile == NULL)
    return;
// Print time info:
//  TOTAL, INIT_SOLVE, GEN_SUB, GET_CUT, LIFT, ADD_CUT
  for (int t = 0; t < (int) times.size(); t++)
    fprintf(myfile, "%2.3f%c", timeStats.get_time(times[t]), SEP);
  fflush(myfile);
}

/***********************************************************************/
/**
 * @brief Writes int entry to II
 */
void writeEntryToLog(const int x, FILE *myfile) {
  if (myfile == NULL)
    return;
  fprintf(myfile, "%s%c", stringValue(x).c_str(), SEP);
  fflush(myfile);
}

/**
 * @brief Writes long int entry to II
 */
void writeEntryToLog(const long x, FILE *myfile) {
  if (myfile == NULL)
    return;
  fprintf(myfile, "%s%c", stringValue(x).c_str(), SEP);
  fflush(myfile);
}

/**
 * @brief Writes double entry to II
 */
void writeEntryToLog(const double x, FILE *myfile) {
  if (myfile == NULL)
    return;
  fprintf(myfile, "%s%c", stringValue(x).c_str(), SEP);
  fflush(myfile);
}

/**
 * @brief Writes string entry to II
 */
void writeEntryToLog(const std::string x, FILE *myfile, const char SEPCHAR) {
  if (myfile == NULL)
    return;
  fprintf(myfile, "%s%c", x.c_str(), SEPCHAR);
  fflush(myfile);
}

/***********************************************************************/
/**
 * @brief Writes sets of cuts that are stored in structural space.
 */
void writeCutsInStructSpace(std::string headerText, AdvCuts &structCut,
    SolutionInfo &solnInfo, const PointCutsSolverInterface* const solver, char* filename,
    FILE* inst_info_out) {
  FILE* StructCutOut = fopen(filename, "w");
  if (StructCutOut == NULL) {
    error_msg(errorstring,
        "Unable to open file to write cuts in structural space for filename %s.\n",
        filename);
    writeErrorToLog(errorstring, inst_info_out);
    exit(1);
  }
  fprintf(StructCutOut, "%s", headerText.c_str());
  fprintf(StructCutOut, "%s,%s,%s,%s,%s\n", "Cut #", "Facet", "Facet name", "Vars"
      "Rhs", "Coeff");
  fprintf(StructCutOut, ",,,,,");
  for (int col = 0; col < solnInfo.numCols; col++) {
    fprintf(StructCutOut, "%d,", col);
  }
  fprintf(StructCutOut, "\n");
  fprintf(StructCutOut, ",,,,,");
  for (int col = 0; col < solnInfo.numCols; col++) {
    fprintf(StructCutOut, "%s,", solver->getColName(col).c_str());
  }
  fprintf(StructCutOut, "\n");
  for (int currcut = 0; currcut < (int) structCut.size(); currcut++) {
    fprintf(StructCutOut, "%d,", currcut);
    fprintf(StructCutOut, "%d,", structCut[currcut].cgsIndex);
    fprintf(StructCutOut, "%s,", structCut[currcut].cgsName.c_str());
    fprintf(StructCutOut, "%f,", structCut[currcut].rhs());
    for (int coeff = 0; coeff < (int) structCut[currcut].num_coeff;
        coeff++) {
//      if (isZero(structCut[currcut][coeff])) {
//        structCut[currcut][coeff] = 0.0;
//      }
      fprintf(StructCutOut, "%f,", structCut[currcut][coeff]);
      if (std::isnan(structCut[currcut][coeff])) {
        error_msg(errorstring, "Found NAN! Coeff %d of cut %d is %f.\n",
            coeff, currcut, structCut[currcut][coeff]);
        writeErrorToLog(errorstring, inst_info_out);
        exit(1);
      }
    }
    fprintf(StructCutOut, "\n");
  }
  fprintf(StructCutOut, "\n");

  fclose(StructCutOut);
}

/***********************************************************************/
/**
 * @brief Writes sets of cuts that are stored in structural space.
 */
void writeCutsInStructSpace(std::string headerText,
    std::vector<AdvCuts> &structCut, SolutionInfo &solnInfo,
    const PointCutsSolverInterface* const solver, char* filename, FILE* inst_info_out) {
  FILE* StructCutOut = fopen(filename, "w");
  if (StructCutOut == NULL) {
    error_msg(errorstring,
        "Unable to open file to write cuts in structural space for filename %s.\n",
        filename);
    writeErrorToLog(errorstring, inst_info_out);
    exit(1);
  }
  fprintf(StructCutOut, "%s", headerText.c_str());
  fprintf(StructCutOut, "%s,%s,%s,%s,%s\n", "Cut #", "Split", "Split name",
      "Rhs", "Coeff");
  fprintf(StructCutOut, ",,,,");
  for (int col = 0; col < solnInfo.numCols; col++) {
    fprintf(StructCutOut, "%d,", col);
  }
  fprintf(StructCutOut, "\n");
  fprintf(StructCutOut, ",,,,");
  for (int col = 0; col < solnInfo.numCols; col++) {
    fprintf(StructCutOut, "%s,", solver->getColName(col).c_str());
  }
  fprintf(StructCutOut, "\n");
  for (int cgs_ind = 0; cgs_ind < (int) structCut.size(); cgs_ind++) {
    for (int currcut = 0; currcut < (int) structCut[cgs_ind].size();
        currcut++) {
      fprintf(StructCutOut, "%d-%d,", cgs_ind, currcut);
      fprintf(StructCutOut, "%d,", structCut[cgs_ind][currcut].cgsIndex);
      fprintf(StructCutOut, "%s,", structCut[cgs_ind][currcut].cgsName.c_str());
      fprintf(StructCutOut, "%f,", structCut[cgs_ind][currcut].rhs());
      for (int coeff = 0;
          coeff < (int) structCut[cgs_ind][currcut].num_coeff;
          coeff++) {
//        if (std::abs(structCut[cgs_ind][currcut][coeff]) < param.getEPS()) {
//          structCut[cgs_ind][currcut][coeff] = 0.0;
//        }
        fprintf(StructCutOut, "%f,",
            structCut[cgs_ind][currcut][coeff]);
        if (std::isnan(structCut[cgs_ind][currcut][coeff])) {
          error_msg(errorstring,
              "Found NAN! Coeff %d of cut %d is %f.\n", coeff,
              currcut, structCut[cgs_ind][currcut][coeff]);
          writeErrorToLog(errorstring, inst_info_out);
          exit(1);
        }
      }
      fprintf(StructCutOut, "\n");
    }
  }
  fprintf(StructCutOut, "\n");

  fclose(StructCutOut);
}

/***********************************************************************/
/**
 * @brief Writes sets of cuts that are stored in NB space.
 */
void writeCutsInNBSpace(std::string headerText, AdvCuts &NBCut,
    SolutionInfo &solnInfo, const PointCutsSolverInterface* const solver, char* filename,
    FILE* inst_info_out) {
  FILE* NBCutOut = fopen(filename, "w");
  if (NBCutOut == NULL) {
    error_msg(errorstring,
        "Unable to open file to write cuts in non-basic space for filename %s.\n",
        filename);
    writeErrorToLog(errorstring, inst_info_out);
    exit(1);
  }
  fprintf(NBCutOut, "%s", headerText.c_str());
  fprintf(NBCutOut, "%s,%s,%s,%s,%s\n", "Cut #", "Split", "Split name", "Rhs",
      "Coeff");
  fprintf(NBCutOut, ",,,,");
  for (int nbcol = 0; nbcol < solnInfo.numNB; nbcol++) {
    int currvar = solnInfo.nonBasicVarIndex[nbcol];
    fprintf(NBCutOut, "%d,", currvar);
  }
  fprintf(NBCutOut, "\n");
  fprintf(NBCutOut, ",,,,");
  for (int nbcol = 0; nbcol < solnInfo.numNB; nbcol++) {
    int currvar = solnInfo.nonBasicVarIndex[nbcol];
    if (currvar < solnInfo.numCols) {
      fprintf(NBCutOut, "%s,", solver->getColName(currvar).c_str());
    } else {
      fprintf(NBCutOut, "%s,",
          solver->getRowName(currvar - solnInfo.numCols).c_str());
    }
  }
  fprintf(NBCutOut, "\n");
  for (int currcut = 0; currcut < (int) NBCut.size(); currcut++) {
    fprintf(NBCutOut, "%d,", currcut);
    fprintf(NBCutOut, "%d,", NBCut[currcut].cgsIndex);
    fprintf(NBCutOut, "%s,", NBCut[currcut].cgsName.c_str());
    fprintf(NBCutOut, "%f,", NBCut[currcut].rhs());
    for (int coeff = 0; coeff < (int) NBCut[currcut].num_coeff; coeff++) {
//      if (std::abs(NBCut[currcut].coeff[coeff]) < param.getEPS()) {
//        NBCut[currcut].coeff[coeff] = 0.0;
//      }
      fprintf(NBCutOut, "%f,", NBCut[currcut][coeff]);
      if (std::isnan(NBCut[currcut][coeff])) {
        error_msg(errorstring, "Found NAN! Coeff %d of cut %d is %f.\n",
            coeff, currcut, NBCut[currcut][coeff]);
        writeErrorToLog(errorstring, inst_info_out);
        exit(1);
      }
    }
    fprintf(NBCutOut, "\n");
  }
  fprintf(NBCutOut, "\n");

  fclose(NBCutOut);

}

/***********************************************************************/
/**
 * @brief Writes sets of cuts that are stored in NB space.
 */
void writeCutsInNBSpace(std::string headerText, std::vector<AdvCuts> &NBCut,
    SolutionInfo &solnInfo, const PointCutsSolverInterface* const solver, char* filename,
    FILE* inst_info_out) {
  FILE* NBCutOut = fopen(filename, "w");
  if (NBCutOut == NULL) {
    error_msg(errorstring,
        "Unable to open file to write cuts in non-basic space for filename %s.\n",
        filename);
    writeErrorToLog(errorstring, inst_info_out);
    exit(1);
  }
  fprintf(NBCutOut, "%s", headerText.c_str());
  fprintf(NBCutOut, "%s,%s,%s,%s,%s\n", "Cut #", "Split", "Split name", "Rhs",
      "Coeff");
  fprintf(NBCutOut, ",,,,");
  for (int nbcol = 0; nbcol < solnInfo.numNB; nbcol++) {
    int currvar = solnInfo.nonBasicVarIndex[nbcol];
    fprintf(NBCutOut, "%d,", currvar);
  }
  fprintf(NBCutOut, "\n");
  fprintf(NBCutOut, ",,,,");
  for (int nbcol = 0; nbcol < solnInfo.numNB; nbcol++) {
    int currvar = solnInfo.nonBasicVarIndex[nbcol];
    if (currvar < solnInfo.numCols) {
      fprintf(NBCutOut, "%s,", solver->getColName(currvar).c_str());
    } else {
      fprintf(NBCutOut, "%s,",
          solver->getRowName(currvar - solnInfo.numCols).c_str());
    }
  }
  fprintf(NBCutOut, "\n");
  for (int cgs_ind = 0; cgs_ind < (int) NBCut.size(); cgs_ind++) {
    for (int currcut = 0; currcut < (int) NBCut[cgs_ind].size();
        currcut++) {
      fprintf(NBCutOut, "%d,", currcut);
      fprintf(NBCutOut, "%d,", NBCut[cgs_ind][currcut].cgsIndex);
      fprintf(NBCutOut, "%s,", NBCut[cgs_ind][currcut].cgsName.c_str());
      fprintf(NBCutOut, "%f,", NBCut[cgs_ind][currcut].rhs());
      for (int coeff = 0;
          coeff < (int) NBCut[cgs_ind][currcut].num_coeff; coeff++) {
//        if (std::abs(NBCut[splitind][currcut].coeff[coeff]) < param.getEPS()) {
//          NBCut[splitind][currcut].coeff[coeff] = 0.0;
//        }
        fprintf(NBCutOut, "%f,", NBCut[cgs_ind][currcut][coeff]);
        if (std::isnan(NBCut[cgs_ind][currcut][coeff])) {
          error_msg(errorstring,
              "Found NAN! Coeff %d of cut %d is %f.\n", coeff,
              currcut, NBCut[cgs_ind][currcut][coeff]);
          writeErrorToLog(errorstring, inst_info_out);
          exit(1);
        }
      }
      fprintf(NBCutOut, "\n");
    }
  }
  fprintf(NBCutOut, "\n");

  fclose(NBCutOut);

}

/***********************************************************************/
/**
 * @brief Writes how far along each NB ray we go for each cut.
 */
void writeRayDistInfo(std::string headerText, const PointCutsSolverInterface* const solver,
    SolutionInfo &solnInfo, AdvCuts &structCut,
    std::vector<std::vector<double> > &distAlongRay, char* filename,
    FILE* inst_info_out) {
  FILE* NBDist = fopen(filename, "w");
  if (NBDist == NULL) {
    error_msg(errorstring,
        "Unable to open file to write ray distance info for filename %s.\n",
        filename);
    writeErrorToLog(errorstring, inst_info_out);
    exit(1);
  }
  fprintf(NBDist, "%s", headerText.c_str());
  fprintf(NBDist, "Ray,Ray name,Cut\n");
  fprintf(NBDist, ",,");
  for (int currcut = 0; currcut < (int) structCut.size(); currcut++) {
    fprintf(NBDist, "%d,", structCut[currcut].cgsIndex);
  }
  fprintf(NBDist, "\n");
  fprintf(NBDist, ",,");
  for (int currcut = 0; currcut < (int) structCut.size(); currcut++) {
    fprintf(NBDist, "%s,", structCut[currcut].cgsName.c_str());
  }
  fprintf(NBDist, "\n");
  for (int ray = 0; ray < solnInfo.numNB; ray++) {
    std::string rayname;
    int currvar = solnInfo.nonBasicVarIndex[ray];
    if (ray < solnInfo.numNBOrig) {
      rayname = solver->getColName(currvar);
    } else {
      rayname = solver->getRowName(currvar - solnInfo.numCols);
    }
    if (isNonBasicUBVar(solver, currvar)) {
      // Then we are going in the negative direction
      rayname = "-" + rayname;
    }

    fprintf(NBDist, "%d,%s,", ray, rayname.c_str());
    for (int currcut = 0; currcut < (int) structCut.size(); currcut++) {
      double currdist = distAlongRay[currcut][ray];
      if (isInfinity(currdist, solver->getInfinity(), param.getEPS())) {
        fprintf(NBDist, "inf,");
      } else {
        fprintf(NBDist, "%2.3f,", currdist);
      }
    }
    fprintf(NBDist, "\n");
  }

  fprintf(NBDist, ",SICEuclDist,");
  for (int currcut = 0; currcut < (int) structCut.size(); currcut++) {
//    fprintf(NBDist, "%2.3f,", structCut[currcut].eucl);
    fprintf(NBDist, "not_impl,");
  }
  fprintf(NBDist, "\n");

  fprintf(NBDist, ",SICObj,");
  for (int currcut = 0; currcut < (int) structCut.size(); currcut++) {
//    int indexOfSplitInFeasSplits = structCut[currcut].splitIndex;
////    printf("Split index is %d.\n", indexOfSplitInFeasSplits);
//    int indexOfSplitInFracCore = solnInfo.feasSplit[indexOfSplitInFeasSplits];
////    printf("Split index in frac core is %d.\n", indexOfSplitInFracCore);
//    if (solnInfo.splitFeas[indexOfSplitInFracCore]) {
//    if (structCut[currcut].feas) {
    double currobj = structCut[currcut].postCutObj;
    if (!isInfinity(currobj, solver->getInfinity(), param.getEPS()))
      fprintf(NBDist, "%2.3f,", currobj);
    else
      fprintf(NBDist, "error,");
//    } else {
//      fprintf(NBDist, "infeas,");
//    }
  }
  fprintf(NBDist, "\n");

  fclose(NBDist);
} /* writeRayDistInfo */

/***********************************************************************/
/**
 * @brief Writes how far along each NB ray we go for each cut.
 */
void writeRayDistInfo(std::string headerText, const PointCutsSolverInterface* const solver,
    SolutionInfo &solnInfo, std::vector<AdvCuts> &structCut,
    std::vector<std::vector<std::vector<double> > > &distAlongRay,
    char* filename, FILE* inst_info_out) {
  FILE* NBDist = fopen(filename, "w");
  if (NBDist == NULL) {
    error_msg(errorstring,
        "Unable to open file to write ray distance info for filename %s.\n",
        filename);
    writeErrorToLog(errorstring, inst_info_out);
    exit(1);
  }
  fprintf(NBDist, "%s", headerText.c_str());
  fprintf(NBDist, "Ray,Ray name,Cut\n");
  fprintf(NBDist, ",,");
  for (int splitind = 0; splitind < (int) structCut.size(); splitind++) {
    for (int currcut = 0; currcut < (int) structCut[splitind].size();
        currcut++) {
      fprintf(NBDist, "%d,", structCut[splitind][currcut].cgsIndex);
    }
  }
  fprintf(NBDist, "\n");
  fprintf(NBDist, ",,");
  for (int splitind = 0; splitind < (int) structCut.size(); splitind++) {
    for (int currcut = 0; currcut < (int) structCut[splitind].size();
        currcut++) {
      fprintf(NBDist, "%s,", structCut[splitind][currcut].cgsName.c_str());
    }
  }
  fprintf(NBDist, "\n");
  for (int ray = 0; ray < solnInfo.numNB; ray++) {
    std::string rayname;
    int currvar = solnInfo.nonBasicVarIndex[ray];
    if (ray < solnInfo.numNB) {
      rayname = solver->getColName(currvar);
    } else {
      rayname = solver->getRowName(currvar - solnInfo.numCols);
    }
    if (isNonBasicUBVar(solver, currvar)) {
      // Then we are going in the negative direction
      rayname = "-" + rayname;
    }

    fprintf(NBDist, "%d,%s,", ray, rayname.c_str());
    for (int splitind = 0; splitind < (int) structCut.size(); splitind++) {
      for (int currcut = 0; currcut < (int) structCut[splitind].size();
          currcut++) {
        double currdist = distAlongRay[splitind][currcut][ray];
        if (isInfinity(currdist, solver->getInfinity(), param.getEPS())) {
          fprintf(NBDist, "inf,");
        } else {
          fprintf(NBDist, "%2.3f,", currdist);
        }
      }
    }
    fprintf(NBDist, "\n");
  }

  fprintf(NBDist, ",SICEuclDist,");
  for (int splitind = 0; splitind < (int) structCut.size(); splitind++) {
    for (int currcut = 0; currcut < (int) structCut[splitind].size();
        currcut++) {
//      fprintf(NBDist, "%2.3f,", structCut[splitind][currcut].eucl);
      fprintf(NBDist, "not_impl,");
    }
  }
  fprintf(NBDist, "\n");

  fprintf(NBDist, ",SICObj,");
  for (int splitind = 0; splitind < (int) structCut.size(); splitind++) {
    for (int currcut = 0; currcut < (int) structCut[splitind].size();
        currcut++) {
//    int indexOfSplitInFeasSplits = structCut[currcut].splitIndex;
////    printf("Split index is %d.\n", indexOfSplitInFeasSplits);
//    int indexOfSplitInFracCore = solnInfo.feasSplit[indexOfSplitInFeasSplits];
////    printf("Split index in frac core is %d.\n", indexOfSplitInFracCore);
//    if (solnInfo.splitFeas[indexOfSplitInFracCore]) {
//      if (structCut[splitind][currcut].feas) {
      double currobj = structCut[splitind][currcut].postCutObj;
      if (!isInfinity(currobj, solver->getInfinity(), param.getEPS()))
        fprintf(NBDist, "%2.3f,", currobj);
      else
        fprintf(NBDist, "error,");
//      } else {
//        fprintf(NBDist, "infeas,");
//      }
    }
  }
  fprintf(NBDist, "\n");

  fclose(NBDist);
} /* writeRayDistInfo */

/***********************************************************************/
void printSplitInfo(const OsiSolverInterface* const solverMain,
    SolutionInfo& solnInfoMain) {
//#ifdef TRACE
//  printf(
//      "\n## Store split feasibility info as well as num cuts generated per each feasible split. ##\n");
//#endif
  char filename[300];
  int tmpindex1 = 0;
  snprintf(filename, sizeof(filename) / sizeof(char),
      "%sMain-splitFeasInfo.csv", GlobalVariables::out_f_name_stub.c_str());
  FILE* splitInfoOuttmp = fopen(filename, "w");
  fprintf(splitInfoOuttmp,
      "Split Feasibility Info (Main, subspace option: %d, num splits: %d, num feasible: %d)\n",
      param.paramVal[ParamIndices::SUBSPACE_PARAM_IND], solnInfoMain.numSplits,
      solnInfoMain.numFeasSplits);
  fprintf(splitInfoOuttmp,
      "Split,Split var index,Split name,Feas sides > 0 && post-SIC feas && chosen?,"
          "Num feas sides,Floor feas?,Ceil feas?,Num intersecting rays,Num rays int floor,Num rays int ceil,"
          "Percent intersecting rays,Num GICs generated\n");
  for (int split = 0; split < solnInfoMain.numSplits; split++) {
    int currsplit = solnInfoMain.fractionalCore[split];
    fprintf(splitInfoOuttmp, "%d,", split);
    fprintf(splitInfoOuttmp, "%d,", currsplit);
    fprintf(splitInfoOuttmp, "%s,",
        solverMain->getColName(currsplit).c_str());
    fprintf(splitInfoOuttmp, "%d,",
        (int) solnInfoMain.splitFeasFlag[split]);
    fprintf(splitInfoOuttmp, "%d,",
        (int) (solnInfoMain.fracCoreFloorFeasFlag[split]
            + solnInfoMain.fracCoreCeilFeasFlag[split]));
    fprintf(splitInfoOuttmp, "%d,",
        (int) solnInfoMain.fracCoreFloorFeasFlag[split]);
    fprintf(splitInfoOuttmp, "%d,",
        (int) solnInfoMain.fracCoreCeilFeasFlag[split]);
    if (solnInfoMain.splitFeasFlag[split]) {
      fprintf(splitInfoOuttmp, "%d,",
          solnInfoMain.count_intersecting_rays[tmpindex1]);
      fprintf(splitInfoOuttmp, "%d,",
          solnInfoMain.count_intersecting_rays_floor[tmpindex1]);
      fprintf(splitInfoOuttmp, "%d,",
          solnInfoMain.count_intersecting_rays_ceil[tmpindex1]);
      fprintf(splitInfoOuttmp, "%2.3f,",
          (double) solnInfoMain.count_intersecting_rays[tmpindex1]
              / solnInfoMain.numNB);
      tmpindex1++;
    }
    fprintf(splitInfoOuttmp, "\n");
  }
  fclose(splitInfoOuttmp);
} /* printSplitInfo */

/***********************************************************************/
/**
 * @brief Writes euclidean distance by which initial vertex is cut,
 * and the objective function after adding the single cut
 */
void writeCutSummary(std::string headerText,
    //const PointCutsSolverInterface* const solver,
//    SolutionInfo &solnInfo,
    AdvCuts &structCut, char* filename,
    FILE* inst_info_out) {
  FILE* CutOut = fopen(filename, "w");
  if (CutOut == NULL) {
    error_msg(errorstring,
        "Unable to open file to write cut summary info for filename %s.\n",
        filename);
    writeErrorToLog(errorstring, inst_info_out);
    exit(1);
  }
  fprintf(CutOut, "%s", headerText.c_str());
  fprintf(CutOut, "Cut#,Facet,Facetname,Euclideandist,LPobj\n");
  for (int currcut = 0; currcut < (int) structCut.size(); currcut++) {
//    fprintf(CutOut, "%d,%d,%s,%2.3f,%2.3f\n", currcut, currsplit,
//        solver->getColName(currsplit).c_str(), structCut[currcut].eucl,
//        structCut[currcut].obj);
    fprintf(CutOut, "%d,%d,%s,%2.3f,%2.3f\n", currcut,
        structCut[currcut].cgsIndex, structCut[currcut].cgsName.c_str(),
        -1.0, structCut[currcut].postCutObj);
  }
  fclose(CutOut);
} /* writeCutSummary */

/***********************************************************************/
/**
 * @brief Writes euclidean distance by which initial vertex is cut,
 * and the objective function after adding the single cut
 */
void writeCutSummary(std::string headerText,
    //const PointCutsSolverInterface* const solver,
    //SolutionInfo &solnInfo,
    std::vector<AdvCuts> &structCut, char* filename,
    FILE* inst_info_out) {
  FILE* CutOut = fopen(filename, "w");
  if (CutOut == NULL) {
    error_msg(errorstring,
        "Unable to open file to write cut summary info for filename %s.\n",
        filename);
    writeErrorToLog(errorstring, inst_info_out);
    exit(1);
  }
  fprintf(CutOut, "%s", headerText.c_str());
  fprintf(CutOut, "Cut#,Cgs,Facet,Facetname,Euclideandist,LPobj\n");
  for (int cgs_ind = 0; cgs_ind < (int) structCut.size(); cgs_ind++) {
    for (int currcut = 0; currcut < (int) structCut[cgs_ind].size();
        currcut++) {
//      fprintf(CutOut, "%d,%d,%s,%2.3f,%2.3f\n", currcut, currsplit,
//          solver->getColName(currsplit).c_str(),
//          structCut[splitind][currcut].eucl,
//          structCut[splitind][currcut].obj);
      fprintf(CutOut, "%d,%d,%d,%s,%2.3f,%2.3f\n", currcut, cgs_ind,
          structCut[cgs_ind][currcut].cgsIndex, structCut[cgs_ind][currcut].cgsName.c_str(),
          -1.0, structCut[cgs_ind][currcut].postCutObj);
    }
  }
  fclose(CutOut);
} /* writeCutSummary */

/***********************************************************************/
/**
 * @brief Writes euclidean distance by which initial vertex is cut,
 * for the sub GICs vs sub SICs
 */
//void writeSubGICSummary(std::string headerText, LiftGICsSolverInterface* solver,
//    SolutionInfo &solnInfo, AdvCuts &structCut, AdvCuts &SIC,
//    char* filename, FILE* inst_info_out) {
//  FILE* CutOut = fopen(filename, "w");
//  if (CutOut == NULL) {
//    char errorstring[300];
//    snprintf(errorstring, sizeof(errorstring) / sizeof(char),
//        "*** ERROR: Unable to open file to write cut summary info for filename %s.\n",
//        filename);
//    std::cerr << errorstring << std::endl;
//    writeErrorToII(errorstring, inst_info_out);
//    exit(1);
//  }
//  fprintf(CutOut, "%s", headerText.c_str());
//  fprintf(CutOut,
//      "Cut no,Split var,Split name,GIC Euclidean dist,SIC Euclidean dist\n");
//  for (int currcut = 0; currcut < (int) structCut.size(); currcut++) {
//    int currsplit = structCut[currcut].splitVarIndex;
//
//    // Since SIC contains cuts for all splits, not just the feasible ones,
//    // we need to find where in that std::vector the cut corresponding to this split is.
//    // structCut.splitIndex gives which of the splits in feasSplitVar this is.
//    // But then feasSplit[splitIndex] gives index in fractionalCore of this split,
//    // which will also be its index in SIC.
//    int splitindex = structCut[currcut].splitIndex;
//    int index = solnInfo.feasSplitFracCoreIndex[splitindex];
//
//    fprintf(CutOut, "%d,%d,%s,%2.3f,%2.3f,", currcut, currsplit,
//        solver->getColName(currsplit).c_str(), structCut[currcut].eucl,
//        SIC[index].eucl);
//    fprintf(CutOut, "\n");
//  }
//  fclose(CutOut);
//}
/***********************************************************************/
/**
 * @brief Writes euclidean distance by which initial vertex is cut,
 * for the sub GICs vs sub SICs
 */
//void writeSubGICSummary(std::string headerText, LiftGICsSolverInterface* solver,
//    SolutionInfo &solnInfo, std::vector<AdvCuts> &structCut, AdvCuts &SIC,
//    char* filename, FILE* inst_info_out) {
//  FILE* CutOut = fopen(filename, "w");
//  if (CutOut == NULL) {
//    error_msg(errorstring,
//        "Unable to open file to write cut summary info for filename %s.\n",
//        filename);
//    writeErrorToII(errorstring, inst_info_out);
//    exit(1);
//  }
//  fprintf(CutOut, "%s", headerText.c_str());
//  fprintf(CutOut,
//      "Cut no,Split var,Split name,GIC Euclidean dist,SIC Euclidean dist\n");
//  for (int splitind = 0; splitind < (int) structCut.size(); splitind++) {
//    for (int currcut = 0; currcut < (int) structCut[splitind].size();
//        currcut++) {
//      int currsplit = structCut[splitind][currcut].splitVarIndex;
//
//      // Since SIC contains cuts for all splits, not just the feasible ones,
//      // we need to find where in that std::vector the cut corresponding to this split is.
//      // structCut.splitIndex gives which of the splits in feasSplitVar this is.
//      // But then feasSplit[splitIndex] gives index in fractionalCore of this split,
//      // which will also be its index in SIC.
//      int splitindex = structCut[splitind][currcut].splitIndex;
//      int index = solnInfo.feasSplitFracCoreIndex[splitindex];
//
//      fprintf(CutOut, "%d,%d,%s,%2.3f,%2.3f,", currcut, currsplit,
//          solver->getColName(currsplit).c_str(),
//          structCut[splitind][currcut].eucl, SIC[index].eucl);
//      fprintf(CutOut, "\n");
//    }
//  }
//  fclose(CutOut);
//}
/***********************************************************************/
/**
 * @brief Writes euclidean distance by which initial vertex is cut,
 * for the sub GICs vs sub SICs
 */
/*
void writeLiftedGICSummary(std::string headerText,
    LiftGICsSolverInterface* solver, SolutionInfo &solnInfo,
    std::vector<AdvCuts> &GICLifted, std::vector<AdvCuts> &SICLifted,
    AdvCuts &SICOrig, char* filename, FILE* inst_info_out) {
//  FILE* CutOut = fopen(filename, "w");
//  if (CutOut == NULL) {
//    char errorstring[300];
//    snprintf(errorstring, sizeof(errorstring) / sizeof(char),
//        "*** ERROR: Unable to open file to write cut summary info for filename %s.\n",
//        filename);
//    std::cerr << errorstring << std::endl;
//    writeErrorToII(errorstring, inst_info_out);
//    exit(1);
//  }
//  fprintf(CutOut, "%s", headerText.c_str());
//  fprintf(CutOut,
//      "Cut no,Split var,Split name,lifted GIC Euclidean dist,lifted SIC Euclidean dist,orig SIC Euclidean dist,\n");
//  for (int currcut = 0; currcut < (int) GICLifted.size(); currcut++) {
//    int currsplit = GICLifted[currcut].splitVarIndex;
//
//    // Since SIC contains cuts for all splits, not just the feasible ones,
//    // we need to find where in that std::vector the cut corresponding to this split is.
//    // structCut.splitIndex gives which of the splits in feasSplitVar this is.
//    // But then feasSplit[splitIndex] gives index in fractionalCore of this split,
//    // which will also be its index in SIC.
//    int splitindex = GICLifted[currcut].splitIndex;
////    int index = solnInfo.feasSplit[splitindex];
//
//    fprintf(CutOut, "%d,%d,%s,%2.3f,%2.3f,%2.3f", currcut, currsplit,
//        solver->getColName(currsplit).c_str(), GICLifted[currcut].eucl,
//        SICLifted[splitindex].eucl, SICOrig[splitindex].eucl);
//    fprintf(CutOut, "\n");
//  }
//  fclose(CutOut);
}
*/

/***********************************************************************/
/**
 * @brief Writes info about root node bound after all cuts added
 */
void writeRootNodeBoundAllCuts(std::string &prob_name, int hplaneHeur,
    int cutSelHeur, int numRaysToBeCut, int numFeasSplits, int numGICs,
    double SICBound, double SICBasisCondNumber, double GICBound,
    double GICBasisCondNumber, double SICAndGICBound,
    double SICAndGICBasisCondNumber, SolutionInfo &solnInfo,
    const PointCutsSolverInterface* const solver,
    std::vector<std::vector<int> > &hplanesIndxToBeActivated,
    char* filename, FILE* inst_info_out) {
  FILE* cutSum = fopen(filename, "w");
  if (cutSum == NULL) {
    error_msg(errorstring,
        "Failed to open file %s to write comparison of cut root node bounds, all cuts.\n",
        filename);
    writeErrorToLog(errorstring, inst_info_out);
    exit(1);
  }

  int numHplanes = -1;
  for (int i = 0; i < numFeasSplits; i++) {
    if ((int) hplanesIndxToBeActivated[i].size() > numHplanes)
      numHplanes = hplanesIndxToBeActivated[i].size();
  }

  fprintf(cutSum, "\n## Printing all SICs added versus all GICs added.\n");
  fprintf(cutSum,
      "Instance name,Hplane heur,Cut sel heur,Effective number of splits,Num rays to be cut,Max num hplanes activated over all splits,LP opt,");
  fprintf(cutSum, "Num SICs,SIC bound,SIC cond number,");
  fprintf(cutSum, "Num GICs,GIC bound,GIC cond number,%% GIC better,");
  fprintf(cutSum, "SICAndGIC Bound, SICAndGIC cond number");
  fprintf(cutSum, "\n");
  fprintf(cutSum, "%s,%d,%d,%d,%d,%d,%f,", prob_name.c_str(), hplaneHeur,
      cutSelHeur, numFeasSplits, numRaysToBeCut, numHplanes,
      solnInfo.LPopt);
  if (!isInfinity(SICBound, solver->getInfinity(), param.getEPS())) {
    fprintf(cutSum, "%d,%f,%f,", numFeasSplits, SICBound,
        SICBasisCondNumber);
  } else {
    fprintf(cutSum, "%d,infeas,%f,", numFeasSplits, SICBasisCondNumber);
  }
  if (!isInfinity(GICBound, solver->getInfinity(), param.getEPS())) {
    fprintf(cutSum, "%d,%f,%f,", numGICs, GICBound, GICBasisCondNumber);
  } else {
    fprintf(cutSum, "%d,infeas,%f,", numGICs, GICBasisCondNumber);
  }
  if (isVal(solver->getObjSense(), 1.0)) {
    fprintf(cutSum, "%f,", ((GICBound - SICBound) / SICBound) * 100.0);
  } else {
    fprintf(cutSum, "%f,", ((SICBound - GICBound) / GICBound) * 100.0);
  }

  if (!isInfinity(SICAndGICBound, solver->getInfinity(), param.getEPS())) {
    fprintf(cutSum, "%f,%f,", SICAndGICBound, SICAndGICBasisCondNumber);
  }

  fprintf(cutSum, "\n");
}

/***********************************************************************/
/**
 * @brief Writes time info to separate file from instances info.
 */
void writeTimeInfo(Stats &timeStats, std::vector<std::string> &times,
    char* filename, const char* end_time_string) {
  FILE* TimeOut = fopen(filename, "w");
  if (TimeOut == NULL) {
    error_msg(errorstring,
        "Unable to open file to write time info for filename %s.\n",
        filename);
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }
  fprintf(TimeOut, "\n## Writing time info. ##\n");
  for (int t = 0; t < (int) times.size(); t++)
    fprintf(TimeOut, "%s%c", times[t].c_str(), SEP);
  fprintf(TimeOut, "\n");
  for (int t = 0; t < (int) times.size(); t++)
    fprintf(TimeOut, "%2.3f%c", timeStats.get_time(times[t]), SEP);
  fprintf(TimeOut, "%s", end_time_string);
  fclose(TimeOut);
}

/***********************************************************************/
/**
 * @brief Writes cut solver errors to separate file from instances info.
 */
void writeCutSolverErrors(const int num_obj_tried, char* filename) {
  const bool fileExists = fexists(filename);
  FILE* CutErrorsOut = fopen(filename, "a");
  if (CutErrorsOut == NULL) {
    error_msg(errorstring,
        "Unable to open file to write time info for filename %s.\n",
        filename);
    writeErrorToLog(errorstring, GlobalVariables::log_file);
    exit(1);
  }
  if (!fileExists) {
    fprintf(CutErrorsOut, "## Writing cut solver errors. ##\n");
    fprintf(CutErrorsOut, "%s%c", "OBJ", SEP);
    for (int fail_ind = 0; fail_ind < CutSolverFails::NUM_CUTSOLVER_FAILS;
        fail_ind++) {
      fprintf(CutErrorsOut, "%s%c", CutSolverFailName[fail_ind].c_str(), SEP);
    }
    fprintf(CutErrorsOut, "\n");
  }

  fprintf(CutErrorsOut, "%d%c", num_obj_tried, SEP);
  printf("OBJ TRIED: %d\n", num_obj_tried);
  for (int fail_ind = 0; fail_ind < CutSolverFails::NUM_CUTSOLVER_FAILS;
      fail_ind++) {
    fprintf(CutErrorsOut, "%d%c",
        GlobalVariables::numCutSolverFails[fail_ind], SEP);
    printf("%s: %d\n", CutSolverFailName[fail_ind].c_str(),
        GlobalVariables::numCutSolverFails[fail_ind]);
  }
  fprintf(CutErrorsOut, "\n");
  fclose(CutErrorsOut);
}

void writeInterPtsAndRaysInJSpace(FILE* fptr, const SolutionInfo &solnInfo,
    const std::vector<IntersectionInfo>& interPtsAndRays,
    const std::vector<Hplane>& hplaneStore) {
  int numSplits = interPtsAndRays.size();

  fprintf(fptr,
      "Contains information about the intersection points and rays created during VPC generation\n\n");

  fprintf(fptr,
      "splitIndx,numPoints,numFinalPoints,numRays,Total\n");
  for (int s = 0; s < numSplits; s++) {
    fprintf(fptr, "%d,%d,%d,%d,%d,\n",
        solnInfo.feasSplitVar[s], interPtsAndRays[s].numPoints,
        interPtsAndRays[s].numFinalPoints, interPtsAndRays[s].numRays,
        interPtsAndRays[s].numPoints + interPtsAndRays[s].numRays);
  }
  fprintf(fptr, "\n\n");

//  int numElmts;
  //, count;
  fprintf(fptr,
      "splitIndx,No,cutRayNBColIndx,cutRayVarIndx,hplaneRowIndx,newRayNBColIndx,newRayVarIndx,PtFlag,indx1,val1,indx2,val2,finalFlag\n");
  //  fprintf(fptr, ",,,");
  //  for (int j = 0; j < numCols; j++)
  //    fprintf(fptr, "%d,", j);
  //  fprintf(fptr, "\n");
  std::vector<bool> flag(solnInfo.numNB);
  std::vector<int> index(solnInfo.numNB);

  for (int s = 0; s < numSplits; s++) {
    const int numPointsOrRays = interPtsAndRays[s].getNumRows();
    for (int r = 0; r < numPointsOrRays; r++) {
      bool pointOrRayFlag = !isZero(interPtsAndRays[s].RHS[r]);
      const int hplane_ind = interPtsAndRays[s].hplane[r];
      const int cut_ray_ind = interPtsAndRays[s].cutRay[r];
      const int new_ray_ind = interPtsAndRays[s].newRay[r];

      const CoinShallowPackedVector tmpVector =
          interPtsAndRays[s].getVector(r);
//      numElmts = tmpVector.getNumElements();
//      const int* elmtIndices = tmpVector.getIndices();
//      const double* elmtVals = tmpVector.getElements();

      if (cut_ray_ind >= 0) { // Then necessarily new_ray_ind >= 0
        fprintf(fptr, "%d,%d,%d,%d,%d,%d,%d,%d,",
            solnInfo.feasSplitVar[s], r,
            cut_ray_ind,
            solnInfo.nonBasicVarIndex[cut_ray_ind],
            hplaneStore[hplane_ind].row,
            new_ray_ind,
            solnInfo.nonBasicVarIndex[new_ray_ind],
            pointOrRayFlag);
      } else { 
        if (new_ray_ind >= 0) {
          fprintf(fptr, "%d,%d,-1,-1,-1,%d,%d,%d,",
              solnInfo.feasSplitVar[s], r,
              interPtsAndRays[s].newRay[r],
              solnInfo.nonBasicVarIndex[interPtsAndRays[s].newRay[r]],
              pointOrRayFlag);
        } else {
          fprintf(fptr, "%d,%d,-1,-1,-1,-2,-2,%d,",
              solnInfo.feasSplitVar[s], r,
            pointOrRayFlag);
        }
      }

      if (cut_ray_ind >= 0) {
        fprintf(fptr, "%d,%f,", cut_ray_ind, tmpVector[cut_ray_ind]);
      } else {
        fprintf(fptr, "%d,0.0,", cut_ray_ind);
      }
      if (new_ray_ind >= 0) {
        fprintf(fptr, "%d,%f,", new_ray_ind, tmpVector[new_ray_ind]);
      } else {
        fprintf(fptr, "%d,0.0,", new_ray_ind);
      }

//      for (int k = 0; k < numElmts; k++) {
//        fprintf(fptr, "%d,%lf,", elmtIndices[k], elmtVals[k]);
//      }

      const int final_flag = interPtsAndRays[s].finalFlag[r];
      fprintf(fptr, "%d,", final_flag);
      fprintf(fptr, "\n");
    }
  }
} /* writeInterPtsAndRaysInJSpace (VPC version, no cgsName given) */

void writeInterPtsAndRays(FILE* fptr, const SolutionInfo &solnInfo,
		const int num_cgs,
    const std::vector<IntersectionInfo>* const interPtsAndRays,
    const std::string* const cgsName,
    const std::vector<Hplane>* hplaneStore) {
  fprintf(fptr,
      "Contains information about the intersection points and rays created\n\n");

  fprintf(fptr, "cgsName,numPoints,numFinalPoints,numRays,Total\n");
  for (int cgs_ind = 0; cgs_ind < num_cgs; cgs_ind++) {
    int numPoints = 0, numFinalPoints = 0, numRays = 0;
    for (int f = 0; f < (int) interPtsAndRays[cgs_ind].size(); f++) {
      numPoints += interPtsAndRays[cgs_ind][f].numPoints;
      numFinalPoints += interPtsAndRays[cgs_ind][f].numFinalPoints;
      numRays += interPtsAndRays[cgs_ind][f].numRays;
    }
    fprintf(fptr, "%s,%d,%d,%d,%d,\n", cgsName[cgs_ind].c_str(), numPoints,
        numFinalPoints, numRays, numPoints + numRays);
  }
  fprintf(fptr, "\n\n");

  fprintf(fptr,
      "cgsName,No,cutRayNBColIndx,cutRayVarIndx,hplaneRowIndx,newRayNBColIndx,newRayVarIndx,finalFlag,RHS,index,val\n");
  for (int cgs_ind = 0; cgs_ind < num_cgs; cgs_ind++) {
    for (int f = 0; f < (int) interPtsAndRays[cgs_ind].size(); f++) {
      for (int r = 0; r < interPtsAndRays[cgs_ind][f].getNumRows(); r++) {
        const double rhs = interPtsAndRays[cgs_ind][f].RHS[r];
        const int hplane_ind = interPtsAndRays[cgs_ind][f].hplane[r];
        const int cut_ray_ind = interPtsAndRays[cgs_ind][f].cutRay[r];
        const int new_ray_ind = interPtsAndRays[cgs_ind][f].newRay[r];
        const int final_flag = interPtsAndRays[cgs_ind][f].finalFlag[r];

        const int hplane_row_ind =
            (hplaneStore == NULL) ? -1 : (*hplaneStore)[hplane_ind].row;
        if (cut_ray_ind >= 0) { // Then necessarily new_ray_ind >= 0
          fprintf(fptr, "%s,%d,%d,%d,%d,%d,%d,%d,%d,%.1f,",
              cgsName[cgs_ind].c_str(), r, cut_ray_ind,
              solnInfo.nonBasicVarIndex[cut_ray_ind], hplane_row_ind,
              new_ray_ind, solnInfo.nonBasicVarIndex[new_ray_ind], final_flag,
              final_flag, rhs);
        } else {
          if (new_ray_ind >= 0) {
            fprintf(fptr, "%s,%d,-1,-1,-1,%d,%d,%d,%.1f,",
                cgsName[cgs_ind].c_str(), r,
                interPtsAndRays[cgs_ind][f].newRay[r],
                solnInfo.nonBasicVarIndex[interPtsAndRays[cgs_ind][f].newRay[r]],
                final_flag, rhs);
          } else {
            fprintf(fptr, "%s,%d,-1,-1,-1,-2,-2,%d,%.1f,",
                cgsName[cgs_ind].c_str(), r, final_flag, rhs);
          }
        }

        const CoinShallowPackedVector vec =
            interPtsAndRays[cgs_ind][f].getVector(r);
        for (int k = 0; k < vec.getNumElements(); k++) {
          fprintf(fptr, "%d,%f,", vec.getIndices()[k], vec.getElements()[k]);
        }
        fprintf(fptr, "\n");
      }
    }
  }
} /* writeInterPtsAndRays (general version, cgsNames given) */

void writeInterPtsAndRaysInJSpace(FILE* fptr, const SolutionInfo &solnInfo,
    const std::vector<IntersectionInfo>& interPtsAndRays,
    const std::vector<Hplane>& hplaneStore,
    const std::vector<int>& num_SIC_points,
    const std::vector<int>& num_SIC_final_points,
    const std::vector<int>& num_SIC_rays) {
  const int numSplits = solnInfo.feasSplitVar.size();

  fprintf(fptr,
      "Contains information about the intersection points and rays created from partial hyperplane activation\n\n");

  fprintf(fptr,
      "splitIndx,numPoints,numFinalPoints,numRays,Total,,SICPoints,SICFinalPoints,SICRays\n");
//  int numPoints, numRays;
  for (int s = 0; s < numSplits; s++) {
//    numPoints = 0;
//    numRays = 0;
//    const int numPointsOrRays = interPtsAndRays[s].getNumRows();
//    for (int r = 0; r < numPointsOrRays; r++) {
//      if (std::abs(interPtsAndRays[s].RHS[r] - 1.0) < param.getEPS())
//        numPoints++;
//      else
//        numRays++;
//    }
    fprintf(fptr, "%d,%d,%d,%d,%d,,%d,%d,%d\n",
        solnInfo.feasSplitVar[s], interPtsAndRays[s].numPoints,
        interPtsAndRays[s].numFinalPoints, interPtsAndRays[s].numRays,
        interPtsAndRays[s].numPoints + interPtsAndRays[s].numRays,
        num_SIC_points[s], num_SIC_final_points[s], num_SIC_rays[s]);
  }
  fprintf(fptr, "\n\n");

//  int numElmts;
  //, count;
  fprintf(fptr,
      "splitIndx,No,cutRayNBColIndx,cutRayVarIndx,hplaneRowIndx,newRayNBColIndx,newRayVarIndx,PtFlag,indx1,val1,indx2,val2,finalFlag\n");
  //  fprintf(fptr, ",,,");
  //  for (int j = 0; j < numCols; j++)
  //    fprintf(fptr, "%d,", j);
  //  fprintf(fptr, "\n");
  std::vector<bool> flag(solnInfo.numNB);
  std::vector<int> index(solnInfo.numNB);

  for (int s = 0; s < numSplits; s++) {
    const int numPointsOrRays = interPtsAndRays[s].getNumRows();
    for (int r = 0; r < numPointsOrRays; r++) {
      bool pointOrRayFlag = !isZero(interPtsAndRays[s].RHS[r]);
      const int hplane_ind = interPtsAndRays[s].hplane[r];

      const CoinShallowPackedVector tmpVector =
          interPtsAndRays[s].getVector(r);
//      numElmts = tmpVector.getNumElements();
//      const int* elmtIndices = tmpVector.getIndices();
//      const double* elmtVals = tmpVector.getElements();

      if (interPtsAndRays[s].cutRay[r] >= 0) {
        fprintf(fptr, "%d,%d,%d,%d,%d,%d,%d,%d,",
            solnInfo.feasSplitVar[s], r,
            interPtsAndRays[s].cutRay[r],
            solnInfo.nonBasicVarIndex[interPtsAndRays[s].cutRay[r]],
            hplaneStore[hplane_ind].row,
            interPtsAndRays[s].newRay[r],
            solnInfo.nonBasicVarIndex[interPtsAndRays[s].newRay[r]],
            pointOrRayFlag);
      } else {
        fprintf(fptr, "%d,%d,-1,-1,-1,%d,%d,%d,",
            solnInfo.feasSplitVar[s], r,
            interPtsAndRays[s].newRay[r],
            solnInfo.nonBasicVarIndex[interPtsAndRays[s].newRay[r]],
            pointOrRayFlag);
      }

      fprintf(fptr, "%d,%f,", interPtsAndRays[s].cutRay[r],
          tmpVector[interPtsAndRays[s].cutRay[r]]);
      fprintf(fptr, "%d,%f,", interPtsAndRays[s].newRay[r],
          tmpVector[interPtsAndRays[s].newRay[r]]);

//      for (int k = 0; k < numElmts; k++) {
//        fprintf(fptr, "%d,%lf,", elmtIndices[k], elmtVals[k]);
//      }

      const int final_flag = interPtsAndRays[s].finalFlag[r];
      fprintf(fptr, "%d,", final_flag);
      fprintf(fptr, "\n");
    }
  }
} /* writeInterPtsAndRaysInJSpace (PHA version) */
