//============================================================================
// Name        : Vertex.hpp
// Author      : Aleksandr M. Kazachkov
// Version     : 0.2014.05.23
// Copyright   : Your copyright notice
// Description : Class Point and related methods
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#pragma once

#include "CoinPackedVector.hpp"
#include <string>
#include <vector>

class Vertex: public CoinPackedVector {
public:
  // Indices of hyperplanes used to get vector (not currently being used anywhere)
//  std::vector<int> hplaneIndices;

// These are the indices, in the vector of rays, of all rays emanating from this point
  std::vector<int> rayIndices;

  // Stores whether each ray should still be considered
  std::vector<bool> rayActiveFlag;

  std::vector<int> cobasis; // Keep this sorted! Should be lexmin

  Vertex();
  ~Vertex();

  Vertex(const std::vector<double>& coeff);
  Vertex(int num_coeff, const double* coeff);

  void setPackedVector(int num_coeff, const double* coeff);

//  void setFullCoorFromPointRay(const Point & point, const Ray & ray,
//      const double rayDist);

//  void setFullCoorWithCompNBSpace(const LiftGICsSolverInterface* const  tmpSolver,
//      const LiftGICsSolverInterface* const  origSolver,
//      const ProblemData & origProbData);

  /**
   * Create coordinates of the point from the point in the full space,
   * using the cobasis defined in probData
   */
//  void setCompNBCoor(const Point & pointFullSpace,
//      const ProblemData & origProbData);
  /**
   * Create coordinates of the point from the vector structSolution,
   * using the cobasis defined in probData
   */
//  void setCompNBCoor(const double* structSolution,
//      const LiftGICsSolverInterface* const  tmpSolver,
//      const LiftGICsSolverInterface* const  origSolver,
//      const ProblemData & origProbData);
//  void setObjViolation(const AdvCut & obj, const int maxIndex = -1);
//  void setSICViolation(const AdvCut & SIC); // Make sure it is in the same space
  void printSparseVector() const;
  void printSparseVector(FILE* fptr) const;
  const std::string vectorString() const;

  /**
   * Overloading the standard CoinPackedVectorBase method since that one
   * uses std::equal, which is fine for the indices, but for the double vec
   * it is checking exact equality, and we are okay with eps difference.
   */
  inline bool operator==(const Vertex& pt2) const {
    return !verticesDifferent(*this, pt2);
  }

  inline bool operator!=(const Vertex& pt2) const {
    return !((*this) == pt2);
  }

  static bool verticesDifferent(const Vertex& pt1, const Vertex& pt2);

//  double getObjViolation() const {
//    return objViolation;
//  }

  //  const std::vector<double>& getCoor() const {
  //    return coor;
  //  }
  //
  //  void setCoor(const std::vector<double>& coor) {
  //    this->coor = coor;
  //  }
  //
  //  const std::vector<long>& getNum() const {
  //    return num;
  //  }

  //  void setNum(const std::vector<long>& num) {
  //    this->num = num;
  //  }

  //  void setSparseVector();
  //
  //  long getDen() const {
  //    return den;
  //  }
  //
  //  void setDen(long den) {
  //    this->den = den;
  //  }

  //  void setNumDenCoorFromOutput(const std::vector<long>& output);

  const std::vector<int>& getCobasis() const {
    return cobasis;
  }

  void setCobasis(const std::vector<int>& cobasis) {
    this->cobasis = cobasis;
  }

  void clearCobasis() {
    this->cobasis.clear();
  }

  size_t getCobasisSize() {
    return cobasis.size();
  }

  const std::vector<int>& getBasis() const {
    return basis;
  }

  void setBasis(const std::vector<int>& basis) {
    this->basis = basis;
  }

  void clearBasis() {
    this->basis.clear();
  }

protected:
//  std::vector<long> num;
//  long den;
//  std::vector<double> coor;
  std::vector<int> basis;
  double objViolation, SICViolation;
};
