//============================================================================
// Name        : Point.hpp
// Author      : Aleksandr M. Kazachkov
// Version     : 0.2014.03.08
// Copyright   : Your copyright notice
// Description : Class Point and related methods
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#pragma once

#include "OsiSolverInterface.hpp"
#include "CoinPackedVector.hpp"
#include "SolutionInfo.hpp"

//#include "AdvCut.hpp"

//#include <gmpxx.h>
#include <string>
#include <vector>

class Point: public CoinPackedVector {
public:
  // These are the indices, in the vector of rays, of all rays emanating from this point
  std::vector<int> rayIndices;
  std::vector<int> cobasis; // Keep this sorted! Should be lexmin
//  int splitIndex;
  int num_coeff;

//  Point(int N, const std::vector<int>& cobasis);
  Point(int N);
  Point() :
      Point(-1) {
  }
  ;
  ~Point();

  const std::vector<int>& getCobasis() const {
    return cobasis;
  }

  void setCobasis(const std::vector<int>& cobasis) {
    this->cobasis = cobasis;
  }

  void clearCobasis() {
    this->cobasis.clear();
  }

  size_t getCobasisSize() {
    return cobasis.size();
  }

  const std::vector<int>& getBasis() const {
    return basis;
  }

  void setBasis(const std::vector<int>& basis) {
    this->basis = basis;
  }

  void clearBasis() {
    this->basis.clear();
  }

//  void setFullCoorFromPointRay(const Point & point, const Ray & ray,
//      const double rayDist);

//  void setFullCoorWithCompNBSpace(const LiftGICsSolverInterface* const  tmpSolver,
//      const LiftGICsSolverInterface* const  origSolver,
//      const ProblemData & origProbData);

  /**
   * Create coordinates of the point from the point in the full space,
   * using the cobasis defined in probData
   */
//  void setCompNBCoor(const Point & pointFullSpace,
//      const ProblemData & origProbData);
  /**
   * Create coordinates of the point from the vector structSolution,
   * using the cobasis defined in probData
   */
  void setCompNBCoor(const OsiSolverInterface* const tmpSolver,
      const OsiSolverInterface* const origSolver,
      const SolutionInfo& origProbData, const int deletedVar = -1);
  void setCompNBCoor(const double* const currColValue,
      const double* const currSlackValue, const OsiSolverInterface* const origSolver,
      const SolutionInfo& origProbData, const int deletedVar = -1);
#ifdef TRACE
  void printSparseVector() const;
  void printSparseVector(FILE* fptr) const;
#endif
  const std::string vectorString() const;
  /**
   * Overloading the standard CoinPackedVectorBase method since that one
   * uses std::equal, which is fine for the indices, but for the double vec
   * it is checking exact equality, and we are okay with eps difference.
   */
  inline bool operator==(const CoinPackedVector& pt2) const {
    return !pointsDifferent(*this, pt2);
  }

  inline bool operator!=(const Point& pt2) const {
    return !((*this) == pt2);
  }

  static bool pointsDifferent(const Point& pt1, const CoinPackedVector& pt2);

  inline double getObjViolation() const {
    return objViolation;
  }

  inline void setObjViolation(const double viol) {
    this->objViolation = viol;
  }

protected:
//  std::vector<long> num;
//  long den;
//  std::vector<double> coor;
  std::vector<int> basis;
  double objViolation, SICViolation;
};
