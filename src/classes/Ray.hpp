//============================================================================
// Name        : Ray.hpp
// Author      : Aleksandr M. Kazachkov
// Version     : 0.2014.03.08
// Copyright   : Your copyright notice
// Description : Class Ray and related methods
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#pragma once

#include "OsiSolverInterface.hpp"
#include "CoinPackedVector.hpp"
#include "SolutionInfo.hpp"

#include "CoinPackedVector.hpp"
//#include <gmpxx.h>
#include <string>
#include <vector>

class Ray: public CoinPackedVector {
public:
  // Stores the index in hplanesToAct of the first hplane to intersect the ray
  // And the corresponding vertex index in vertexStore
//  int firstHplaneOverall;
//  int firstVertexOverall;

//  std::vector<std::vector<int> > partitionOfPtsAndRaysByRaysOfC1;

  // For each split, a quick way to check whether we have cut the ray or not
  std::vector<int> cutFlag;

//  // For each split, a quick way to check whether we have added the intersection point/ray with bd S
//  std::vector<int> addedPointFlag;

  int num_coeff;
 
  Ray(int N);
  Ray() : Ray(-1) {};
  ~Ray();

  const std::vector<int>& getCobasis() const {
    return cobasis;
  }

  void setCobasis(const std::vector<int>& cobasis) {
    this->cobasis = cobasis;
  }

  void clearCobasis() {
    this->cobasis.clear();
  }

//  void setFullCoorWithCompNBSpace(const double* structBinvA,
//      const LiftGICsSolverInterface* const  tmpSolver,
//      const LiftGICsSolverInterface* const  origSolver,
//      const ProblemData & origProbData, const int tmpNBVar);

  /**
   * Create coordinates of the ray from the vector structSolution,
   * using the cobasis defined in probData.
   * This ray will be the one corresponding to tmpNBVar in the
   * *current* cobasis in tmpSolver.
   */
  void setCompNBCoor(const double* ray,
      const PointCutsSolverInterface* const tmpSolver,
      const std::vector<int>& rowOfOrigNBVar,
//      const OsiSolverInterface* const origSolver,
      const SolutionInfo& origProbData, const int tmpNBVar, 
      const int deletedVar, const bool rayNeedsCalculation);
//  const std::vector<double>& getCoor() const {
//    return coor;
//  }
//  void setCoor(const std::vector<double>& coor) {
//    this->coor = coor;
//  }
//
//  const std::vector<long>& getNum() const {
//    return num;
//  }
//
//  void setNum(const std::vector<long>& num) {
//    this->num = num;
//  }
//
//  long getDen() const {
//    return den;
//  }
//
//  void setDen(long den) {
//    this->den = den;
//  }
//  void setNumDenCoorFromOutput(const std::vector<long>& output);
//  void setNumDenCoorFromOutput(const std::vector<mpz_class>& output);
  void printSparseVector() const;
  void printSparseVector(FILE* fptr) const;
  const std::string vectorString() const;

  /**
   * Overloading the standard CoinPackedVectorBase method since that one
   * uses std::equal, which is fine for the indices, but for the double vec
   * it is checking exact equality, and we are okay with eps difference.
   */
  inline bool operator==(const CoinPackedVector& ray2) const {
    return !raysDifferent(*this, ray2);
  }

  inline bool operator!=(const Ray& ray2) const {
    return !((*this) == ray2);
  }

  static bool raysDifferent(const Ray& ray1, const CoinPackedVector& ray2);

  inline bool isExtreme() const {
    return extreme;
  }

  inline void setExtreme(bool extreme) {
    this->extreme = extreme;
  }

  inline double getObjViolation() const {
    return objViolation;
  }

  inline void setObjViolation(const double viol) {
    this->objViolation = viol;
  }

  inline double getScale() const { return scale; }

protected:
//  std::vector<long> num;
//  long den;
//  std::vector<double> coor;
  std::vector<int> cobasis; // Keep this sorted! Should be lexmin
  bool extreme;
  double objViolation;
  double scale;
};
