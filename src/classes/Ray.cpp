//============================================================================
// Name        : Ray.cpp
// Author      : Aleksandr M. Kazachkov
// Version     : 0.2014.03.08
// Copyright   : Your copyright notice
// Description : Class Ray and related methods
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#include "GlobalConstants.hpp"
#include <cmath>
#include "Ray.hpp"
#include "Utility.hpp"

//Ray::Ray(int N, const std::vector<int>& cobasis) {
////  num.resize(n);
////  den = -1;
////  coor.resize(n);
//  this->cobasis = cobasis;
//  extreme = false;
//}
//
//Ray::Ray(int N) {
////  num.resize(n);
////  den = 0; // This is a ray
////  coor.resize(n);
//  cobasis.resize(N);
//  extreme = false;
//}

Ray::Ray(int N) {
  num_coeff = N;
  extreme = false;
  objViolation = -1.;
  scale = 1.; // currently an int
//  firstHplaneOverall = -1;
//  firstVertexOverall = -1;
}

Ray::~Ray() {
}

//void Ray::setFullCoorWithCompNBSpace(const double* structBinvA,
//    const LiftGICsSolverInterface* const  tmpSolver,
//    const LiftGICsSolverInterface* const  origSolver,
//    const ProblemData & origProbData, const int tmpNBVar) {
//  std::vector<double> NBRayElemFull(
//      origProbData.numRows + origProbData.numCols, 0.0);
//
//  std::vector<int> varBasicInRow(origProbData.numRows);
//  tmpSolver.getBasics(&varBasicInRow[0]);
//
//  // If the variable corresponding to this ray is upper-bounded, its ray direction is negative
//  double rayDirn = 1.0;
//  if (isNonBasicUBVar(tmpSolver, tmpNBVar))
//    rayDirn = -1.0;
//
//  for (int row = 0; row < origProbData.numRows; row++) {
//    int var = varBasicInRow[row];
//    if (std::abs(structBinvA[row]) > PTCONST::param.getEPS())
//      NBRayElemFull[var] = rayDirn * -1.0 * structBinvA[row];
//
//    // Since we are in the complemented NB space,
//    // the variables that were NB in the original solver
//    // and were at their upper bound
//    // must be negated, since their rays are flipped
//    if (isNonBasicUBVar(origSolver, var)) {
//      NBRayElemFull[var] *= -1.0;
//    }
//  }
//
//  // We also add the rayDirn in the component corresponding to tmpNBVar
//  NBRayElemFull[tmpNBVar] = rayDirn;
//
//  // If the non-basic variable corresponding to this ray
//  // was non-basic at ub in the original solver,
//  // recall that we are in the complemented NB space,
//  // so the ray direction will always be negated.
//  if (isNonBasicUBVar(origSolver, tmpNBVar)) {
//    NBRayElemFull[tmpNBVar] *= -1.0;
//  }
//
//  CoinPackedVector::setFullNonZero(
//      origProbData.numRows + origProbData.numCols, NBRayElemFull.data());
//}

/**
 * Note that we may have deleted a variable to get to tmpSolver
 * In that case, tmpNBVar is in the space of tmpSolver
 */
void Ray::setCompNBCoor(const double* ray,
    const PointCutsSolverInterface* const tmpSolver,
    const std::vector<int>& rowOfOrigNBVar,
//    const OsiSolverInterface* const origSolver,
    const SolutionInfo& origProbData, const int tmpNBVar, const int deletedVar,
    const bool rayNeedsCalculation) {
  std::vector<int> NBRayIndex;
  std::vector<double> NBRayElem;

  NBRayIndex.reserve(origProbData.numNB);
  NBRayElem.reserve(origProbData.numNB);

  // Adjustments for the deletedVar (if there is one)
  const int deletedVarAdjustment = (deletedVar >= 0) ? 1 : 0;
  const int tmpNBVarOrigIndex = (tmpNBVar < deletedVar) ? tmpNBVar : tmpNBVar + deletedVarAdjustment;

  // If the variable corresponding to this ray is upper-bounded, its ray direction is negative
  const int tmpRayMult =
      (isNonBasicUBVar(tmpSolver, tmpNBVar)) ? -1 : 1;

  // Get objective dot product
  if (tmpNBVar < tmpSolver->getNumCols()) {
    objViolation = (double) tmpRayMult * tmpSolver->getReducedCost()[tmpNBVar];
  } else {
    const int tempIndex = tmpNBVar - tmpSolver->getNumCols();
    objViolation = (double) -1 * tmpRayMult * tmpSolver->getRowPrice()[tempIndex];
  }

  // Perhaps we will scale the rays to get better numerics
  int minExponent = 1000, maxExponent = 1;
  int minExponentTiny = 0;

  // Sometimes rays have tiny values
  // These may be "necessary" for validity
  // E.g., if the ray is (r1,r2), and c is the nb obj, we need c1 * r1 + c2 * r2 >= 0 (actually -1e-7)
  // It may be that r1 is tiny, but c1 is very large, and c2 * r2 < 0 by that amount, roughly
  // Other times, if the non-tiny values satisfy the objective cut on their own, let's ignore the tiny ones
  // We will not add the tiny indices + values until the end, when we check if they are necessary
  std::vector<int> tinyIndex;
  std::vector<double> tinyElem;
  double tinyObjOffset = 0.; // how much c . r changes when tiny values are ignored
  double nonTinyObj = 0.;

  // We loop through the *originally* non-basic variables (at v) to get their ray components
  // Some of these may now be basic...
  for (int c = 0; c < origProbData.numNB; c++) {
    const int origNBVar = origProbData.nonBasicVarIndex[c];
    const int origNBVarRow = rowOfOrigNBVar[c];
    const int rayMult = origProbData.isNBUBVar(origNBVar) ? -1 : 1;
    const int newRayMult = tmpRayMult * rayMult;

    // If the current ray, from NBVar, is the same variable as origNBVar,
    // then we should insert at this point the ray direction and index.
    // Though we are working in the complemented non-basic space, the ray direction may
    // actually be -1.0 here, if the NBVar has switched to being tight at its other bound
    if (origNBVar == tmpNBVarOrigIndex) {
      NBRayIndex.push_back(c);
      NBRayElem.push_back(newRayMult);
      nonTinyObj += newRayMult * origProbData.nonBasicReducedCost[c];
      if (0 < minExponent) {
        minExponent = 0;
      }
    } else if (origNBVarRow >= 0) {
      // If we are here, then the NB var is basic currently.
      // Thus, we need to get its component and store it.
      // For the variables that were upper-bounded, we actually go in the negative direction.
      const int flip = rayNeedsCalculation ? -1 : tmpRayMult; // Negate the use of tmpRayMult previously, if ray already calculated
//      const int rowMult =
//          (rayNeedsCalculation && (origNBVar >= origSolver->getNumCols())
//              && (origSolver->getRowSense()[origNBVarRow] == 'G')) ? -1 : 1;
      const double rayVal = flip * newRayMult * ray[origNBVarRow];
      if (!isZero(rayVal, param.getRAYEPS())) {
        NBRayIndex.push_back(c);
        NBRayElem.push_back(rayVal);
        nonTinyObj += rayVal * origProbData.nonBasicReducedCost[c];

        // Get exponent
        const double exp = std::log10(std::abs(rayVal));
        const int intExponent = static_cast<int>(((exp >= 0) ? 0.046 : -0.046) + exp);
        if (intExponent > maxExponent) {
          maxExponent = intExponent;
        }
        if (intExponent < minExponent) {
          minExponent = intExponent;
        }
      } // non-tiny
      else if (!isZero(rayVal * origProbData.nonBasicReducedCost[c], param.getRAYEPS())) {
        tinyIndex.push_back(c);
        tinyElem.push_back(rayVal);
        tinyObjOffset += rayVal * origProbData.nonBasicReducedCost[c];
      } // tiny
      /*
      else {
        if (rayVal != 0)
        printf("\nDEBUG: rejecting ray coeff on nb_var %d (variable %d) with value %.10e\n", c, origNBVar, rayVal);
      }
      */
    }
  } /* end iterating over non-basic elements from original basis */

  // Check whether tiny elements are needed
  bool useTinyElements = lessThanVal(nonTinyObj, 0., param.getEPS())
          || !isVal(nonTinyObj, objViolation, param.getEPS());
  const int numNonTiny = NBRayIndex.size();
  if (useTinyElements) {
    // We may not need all of the tiny elements
    // Sort the tiny elements in decreasing order and keep adding them until the criterion is reached
    const int numTinyTotal = tinyIndex.size();
    CoinPackedVector tinyVec(numTinyTotal, tinyIndex.data(), tinyElem.data());
    tinyVec.sortDecrElement();
    int numTiny = 0;
    for (int i = 0; i < numTinyTotal && useTinyElements; i++) {
      const double val = tinyVec.getElements()[i];
      numTiny++;
      nonTinyObj += val * origProbData.nonBasicReducedCost[tinyVec.getIndices()[i]];
      useTinyElements = lessThanVal(nonTinyObj, 0., param.getEPS())
          || !isVal(nonTinyObj, objViolation, param.getEPS());

      // Get exponent
      const double exp = std::log10(std::abs(val));
      const int intExponent =
          static_cast<int>(((exp >= 0) ? 0.046 : -0.046) + exp);
      if (intExponent < minExponentTiny) {
        minExponentTiny = intExponent;
      }
    }

    int scale_exp = 0;
    const double avgExponentsTiny = 0.5 * (minExponentTiny + maxExponent);
    if (avgExponentsTiny < -0.25) {
      scale_exp = 1 - static_cast<int>(avgExponentsTiny + 0.25);
    }
    scale = std::pow(10., static_cast<double>(scale_exp));

    std::vector<int> newNBRayIndex(numNonTiny + numTiny, 0);
    std::vector<double> newNBRayElem(numNonTiny + numTiny, 0);
    int ind = 0, ind1 = 0, ind2 = 0;
    while (ind1 < numNonTiny) {
      while (ind2 < numTiny && tinyIndex[ind2] < NBRayIndex[ind1]) {
        newNBRayIndex[ind] = tinyVec.getIndices()[ind2];
        newNBRayElem[ind] = tinyVec.getElements()[ind2] * scale;
        ind++;
        ind2++;
      }
      newNBRayIndex[ind] = NBRayIndex[ind1];
      newNBRayElem[ind] = NBRayElem[ind1] * scale;
      ind++;
      ind1++;
    }
    while (ind2 < numTiny) {
      newNBRayIndex[ind] = tinyVec.getIndices()[ind2];
      newNBRayElem[ind] = tinyVec.getElements()[ind2] * scale;
      ind++;
      ind2++;
    }

    CoinPackedVector::setVector((int) newNBRayIndex.size(),
        newNBRayIndex.data(), newNBRayElem.data(), false);
  } else {
    // Get exponent for scaling
    // We will only scale if it is up, not down, by the heuristic that small values are bad
    int scale_exp = 0;
    const double avgExponents = 0.5 * (minExponent + maxExponent);
    if (avgExponents < -0.25) {
      scale_exp = 1 - static_cast<int>(avgExponents + 0.25);
    } else if (minExponent > 0) {
      scale_exp = -1 * minExponent;
    }
    scale = std::pow(10., static_cast<double>(scale_exp));

    if (!isVal(scale, 1.)) {
      for (int i = 0; i < numNonTiny; i++) {
        NBRayElem[i] *= scale;
      }
    }

    CoinPackedVector::setVector(numNonTiny, NBRayIndex.data(), NBRayElem.data(),
        false);
  }

  if (lessThanVal(nonTinyObj, 0., param.getEPS())) {
    if (lessThanVal(nonTinyObj, 0., 1e-3)) {
      error_msg(errorstring,
          "Ray %d: dot product with obj < 0. Obj viol from solver: %.8f. Calculated: %.8f.\n",
          tmpNBVar, objViolation, nonTinyObj);
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    } else {
      warning_msg(warnstring,
          "Ray %d: dot product with obj < 0. Obj viol from solver: %.8f. Calculated: %.8f.\n",
          tmpNBVar, objViolation, nonTinyObj);
    }
  }

  if (!isVal(nonTinyObj, objViolation, param.getEPS())) {
    if (!isVal(nonTinyObj, objViolation, 1e-3)) {
      error_msg(errorstring,
          "Ray %d: Calculated dot product with obj differs from solver's. Obj viol from solver: %.8f. Calculated: %.8f.\n",
          tmpNBVar, objViolation, nonTinyObj);
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    } else {
      warning_msg(warnstring,
          "Ray %d: Calculated dot product with obj differs from solver's. Obj viol from solver: %.8f. Calculated: %.8f.\n",
          tmpNBVar, objViolation, nonTinyObj);
    }
  }
} /* setCompNBCoor */

//void Ray::setNumDenCoorFromOutput(const std::vector<long>& output) {
////  den = output[0].get_si();
////  for (int i = 1; i < (int) output.size(); i++) {
////    num[i-1] = output[i].get_si();
////    coor[i-1] = output[i].get_si() / den;
////  }
//  this->den = output[0];
//  for (int i = 1; i < (int) output.size(); i++) {
//    this->num[i - 1] = output[i];
//    this->coor[i - 1] = (double) output[i] / this->den;
//  }
//}

//void Ray::setNumDenCoorFromOutput(const std::vector<mpz_class>& output) {
//  std::vector<int> indices;
//  std::vector<double> sparse_coeff;
//  indices.reserve((int) output.size() - 1); // -1 b/c first index is denominator
//  sparse_coeff.reserve((int) output.size() - 1);
//
//  double tmpDen = output[0].get_si();
//  if (tmpDen != 0) {
//    error_msg(errstr, "GCD of ray should be zero. Instead it is %f.\n",
//        tmpDen);
//    writeErrorToII(errstr, PointCutsParam::inst_info_out);
//    std::exit(1);
//  }
//  for (int i = 1; i < (int) output.size(); i++) {
////    this->num[i - 1] = output[i].get_si();
////    this->coor[i - 1] = output[i].get_d();
//    double tmpCoor = output[i].get_d();
//    if (std::abs(tmpCoor) > PTCONST::param.getEPS()) {
//      indices.push_back(i - 1);
//      sparse_coeff.push_back(tmpCoor);
//    }
//  }
//
//  CoinPackedVector::setVector((int) indices.size(), indices.data(),
//      sparse_coeff.data());
//}

void Ray::printSparseVector() const {
  printSparseVector(stdout);
}

void Ray::printSparseVector(FILE* fptr) const {
  int numElems = CoinPackedVector::getNumElements();
  const int* index = CoinPackedVector::getIndices();
  const double* element = CoinPackedVector::getElements();
  printf("Num elements is %d.\n", numElems);
  for (int i = 0; i < numElems; i++) {
    fprintf(fptr, "\t%d: %f\n", index[i], element[i]);
  }
}

const std::string Ray::vectorString() const {
  const int numElems = this->getNumElements();
  const int* vecIndex = this->getIndices();
  const double* vecVal = this->getElements();
  char vec_string[200];
  if (numElems == 0) {
    snprintf(vec_string, sizeof(vec_string) / sizeof(char),
        "(zero vector)");
  } else if (numElems == 1) {
    snprintf(vec_string, sizeof(vec_string) / sizeof(char), "(%d: %s)",
        vecIndex[0], stringValue(vecVal[0]).c_str());
  } else if (numElems == 2) {
    snprintf(vec_string, sizeof(vec_string) / sizeof(char),
        "(%d: %s, %d: %s)", vecIndex[0], stringValue(vecVal[0]).c_str(),
        vecIndex[1], stringValue(vecVal[1]).c_str());
  }
  std::string tmp(vec_string);
  return tmp;
}

/**
 * Input has to have the indices sorted in the same order (ideally increasing)
 * Also all zeroes should have been eliminated
 * Returns true if one point is same as the other (up to scaling for the ray)
 */
bool Ray::raysDifferent(const Ray& ray1, const CoinPackedVector& ray2) {
  return isRowDifferent(ray1, 0., ray2, 0., param.getEPS()) != 0;
//  int numElemPt1 = ray1.getNumElements();
//  int numElemPt2 = ray2.getNumElements();
//
//  const int* indicesPt1 = ray1.getIndices();
//  const int* indicesPt2 = ray2.getIndices();
//  const double* valsPt1 = ray1.getElements();
//  const double* valsPt2 = ray2.getElements();
//
//  double scaleFactor = -1.0;
//
//  int ind2 = 0;
//  for (int ind = 0; ind < numElemPt1; ind++) {
//    // If this entry is zero, then we cannot have a violation
//    // (unless the corresponding entry of ray2 is non-zero,
//    // but this will be detected later)
//    if (isZero(valsPt1[ind]))
//      continue;
//
//    // If ind2 has reached numElemPt2, ensure all subsequent values zero
//    if (ind2 >= numElemPt2) {
//      return true; // Already checked current element is non-zero
//    }
//
//    // Find matching indices
//    while (indicesPt1[ind] != indicesPt2[ind2]) {
//      // Ensure the value for ray2 is zero
//      // (No matching ray1 index, since sorted)
//      if (!isZero(valsPt2[ind2])) {
//        return true;
//      } else {
//        ind2++;
//        if (ind2 >= numElemPt2) {
//          return true; // Already checked that valsPt1[ind] is non-zero
//        }
//      }
//    }
//
//    if (isZero(valsPt2[ind2]))
//      return true; // Since valsPt1[ind] is non-zero
//
//    // Alternatively, we make sure ray is not scaled version of other
//    // Set scale factor if it has not been set before
//    if (lessThanVal(scaleFactor, 0.0)) {
//      scaleFactor = valsPt1[ind] / valsPt2[ind2];
//      if (lessThanVal(scaleFactor, 0.0)) {
//        // The scale is negative
//        // Potentially one ray is the negative of the other,
//        // but they are thus still distinct
//        return true;
//
//      }
//    } else if (!isZero(valsPt1[ind] - valsPt2[ind2] * scaleFactor)) {
//      return true;
//    }
//    ind2++;
//  }
//
//  // Check any remaining ray2 indices that were not matched
//  for (int i = ind2; i < numElemPt2; i++) {
//    if (!isZero(valsPt2[i]))
//      return true;
//  }
//
//  return false;
} /* raysDifferent */
