//============================================================================
// Name        : Vertex.cpp
// Author      : Aleksandr M. Kazachkov
// Version     : 0.2014.03.08
// Copyright   : Your copyright notice
// Description : Class Point and related methods
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#include "GlobalConstants.hpp"
#include <cmath>
//#include <gmpxx.h>
#include "GlobalConstants.hpp"
#include "Vertex.hpp"
#include "Utility.hpp"

Vertex::Vertex() {
  objViolation = -1.0;
  SICViolation = -1.0;
}

Vertex::Vertex(const std::vector<double>& coeff) {
  objViolation = -1.0;
  SICViolation = -1.0;

  setPackedVector(coeff.size(), coeff.data());
}

Vertex::Vertex(int num_coeff, const double* coeff) {
  objViolation = -1.0;
  SICViolation = -1.0;

  setPackedVector(num_coeff, coeff);
}

Vertex::~Vertex() {
}

void Vertex::setPackedVector(int num_coeff, const double* coeff) {
  std::vector<int> index;
  std::vector<double> val;
  for (int i = 0; i < num_coeff; i++) {
    if (!isZero(coeff[i])) {
      index.push_back(i);
      val.push_back(coeff[i]);
    }
  }

  setVector((int) index.size(), index.data(), val.data());
}

void Vertex::printSparseVector() const {
  printSparseVector(stdout);
}

void Vertex::printSparseVector(FILE* fptr) const {
  int numElems = CoinPackedVector::getNumElements();
  const int* index = CoinPackedVector::getIndices();
  const double* element = CoinPackedVector::getElements();
  printf("Num elements is %d.\n", numElems);
  for (int i = 0; i < numElems; i++) {
    fprintf(fptr, "\t%d: %f\n", index[i], element[i]);
  }
}

const std::string Vertex::vectorString() const {
  const int numElems = this->getNumElements();
  const int* vecIndex = this->getIndices();
  const double* vecVal = this->getElements();
  char vec_string[200];
  if (numElems == 0) {
    snprintf(vec_string, sizeof(vec_string) / sizeof(char),
        "(zero vector)");
  } else if (numElems == 1) {
    snprintf(vec_string, sizeof(vec_string) / sizeof(char), "(%d: %s)",
        vecIndex[0], stringValue(vecVal[0]).c_str());
  } else if (numElems == 2) {
    snprintf(vec_string, sizeof(vec_string) / sizeof(char),
        "(%d: %s, %d: %s)", vecIndex[0], stringValue(vecVal[0]).c_str(),
        vecIndex[1], stringValue(vecVal[1]).c_str());
  }
  std::string tmp(vec_string);
  return tmp;
}

/**
 * Input has to have the indices sorted in the same order (ideally increasing)
 * Returns true if one point is exactly same as the other (no scaling for the point)
 */
bool Vertex::verticesDifferent(const Vertex& pt1, const Vertex& pt2) {
  return isRowDifferent(pt1, 1., pt2, 1., param.getEPS()) != 0;
/*
  int numElemPt1 = pt1.getNumElements();
  int numElemPt2 = pt2.getNumElements();

//  // If number elements not same, then clearly we are different
//  if (numElemPt1 != numElemPt2)
//    return true;

  const int* indicesPt1 = pt1.getIndices();
  const int* indicesPt2 = pt2.getIndices();
  const double* valsPt1 = pt1.getElements();
  const double* valsPt2 = pt2.getElements();

  int ind2 = 0;
  for (int ind = 0; ind < numElemPt1; ind++) {
    // If this entry is zero, then we cannot have a violation
    // (unless the corresponding entry of pt2 is non-zero,
    // but this will be detected later)
    if (isZero(valsPt1[ind]))
      continue;

    // If ind2 has reached numElemPt2, ensure all subsequent values zero
    if (ind2 >= numElemPt2) {
      return true; // Already checked current element is non-zero
    }

    // Find matching indices
    while (indicesPt1[ind] != indicesPt2[ind2]) {
      // Ensure the value for pt2 is zero
      // (No matching pt1 index, since sorted)
      if (!isZero(valsPt2[ind2])) {
        return true;
      } else {
        ind2++;
        if (ind2 >= numElemPt2) {
          return true; // Already checked that valsPt1[ind] is non-zero
        }
      }
    }

    if (isZero(valsPt2[ind2]))
      return true; // Since valsPt1[ind] is non-zero

    // Alternatively, if any values are different, we are different
    if (!isZero(valsPt1[ind] - valsPt2[ind2]))
      return true;
    ind2++;
  }

  // Check any remaining pt2 indices that were not matched
  for (int i = ind2; i < numElemPt2; i++) {
    if (!isZero(valsPt2[i]))
      return true;
  }

  return false;
*/
} /* verticesDifferent */
