//============================================================================
// Name        : Point.cpp
// Author      : Aleksandr M. Kazachkov
// Version     : 0.2014.03.08
// Copyright   : Your copyright notice
// Description : Class Point and related methods
//
// This code is licensed under the terms of the Eclipse Public License (EPL).
//============================================================================

#include "GlobalConstants.hpp"
#include <cmath>
#include "Point.hpp"
#include "Utility.hpp"

//Point::Point(int N, const std::vector<int>& cobasis) {
////  num.resize(n);
////  den = -1;
////  coor.resize(n);
//  this->cobasis = cobasis;
//  rayIndices.reserve(N);
//  objViolation = -1.0;
//  SICViolation = -1.0;
//}
//
Point::Point(int N) {
  num_coeff = N;
  objViolation = -1.0;
  SICViolation = -1.0;
}

Point::~Point() {
}
//
///**
// * This method hinges on the indices being in sorted order
// */
//void Point::setFullCoorFromPointRay(const Point & point, const Ray & ray,
//    const double rayDist) {
//  std::vector<int> packedIndex;
//  std::vector<double> packedElem;
//
//  const int* pointIndex = point.getIndices();
//  const int* rayIndex = ray.getIndices();
//  const double* pointVal = point.getElements();
//  const double* rayVal = ray.getElements();
//  const int numPointElems = point.getNumElements();
//  const int numRayElems = ray.getNumElements();
//  int pt_el = 0, ray_el = 0;
//
//  while (pt_el < numPointElems && ray_el < numRayElems) {
//    int curr_pt_ind = pointIndex[pt_el];
//    int curr_ray_ind = rayIndex[ray_el];
//    double val = 0.0;
//
//    if (curr_pt_ind == curr_ray_ind) {
//      val = pointVal[pt_el] + rayDist * rayVal[ray_el];
//      pt_el++;
//      ray_el++;
//    } else if (curr_pt_ind < curr_ray_ind) {
//      val = pointVal[pt_el];
//      pt_el++;
//    } else {
//      val = rayDist * rayVal[ray_el];
//      ray_el++;
//    }
//
//    if (std::abs(val) > PTCONST::param.getEPS()) {
//      packedIndex.push_back(curr_pt_ind);
//      packedElem.push_back(val);
//    }
//  }
//
//  // Now we go through the remaining elements
//  // We know at least one of pt_el == numPointElems or ray_el == numRayElems
//  if (pt_el < numPointElems) {
//    for (int el = pt_el; el < numPointElems; el++) {
//      double val = pointVal[el];
//      if (std::abs(val) > PTCONST::param.getEPS()) {
//        packedIndex.push_back(pointIndex[el]);
//        packedElem.push_back(val);
//      }
//    }
//  } else if (ray_el < numRayElems) {
//    for (int el = ray_el; el < numRayElems; el++) {
//      double val = rayDist * rayVal[el];
//      if (std::abs(val) > PTCONST::param.getEPS()) {
//        packedIndex.push_back(rayIndex[ray_el]);
//        packedElem.push_back(val);
//      }
//    }
//  }
//
//  CoinPackedVector::setVector((int) packedIndex.size(), packedIndex.data(),
//      packedElem.data(), false);
//}
//
//void Point::setFullCoorWithCompNBSpace(const LiftGICsSolverInterface* const  tmpSolver,
//    const LiftGICsSolverInterface* const  origSolver,
//    const ProblemData & origProbData) {
//  // For keeping the packed coordinates to input as a new point
//  std::vector<int> packedIndex;
//  std::vector<double> packedElem;
//
//  packedIndex.reserve(origProbData.numRows + origProbData.numCols);
//  packedElem.reserve(origProbData.numRows + origProbData.numCols);
//
//  for (int var = 0; var < origProbData.numCols + origProbData.numRows;
//      var++) {
//    double tmpVal, origVal;
//    double newVal;
//    if (var < origProbData.numCols) {
//      if (isBasicCol(origSolver, var)) {
//        newVal = tmpSolver.getColSolution()[var];
//      } else {
//        tmpVal = tmpSolver.getColSolution()[var];
//        origVal = origSolver.getColSolution()[var];
//        newVal = std::abs(tmpVal - origVal); // always non-negative
//      }
//    } else {
//      int currRow = var - origProbData.numCols;
//
//      newVal = tmpSolver.getRightHandSide()[currRow]
//          - tmpSolver.getRowActivity()[currRow];
//
//      if (!isBasicSlack(origSolver, currRow)) {
//        newVal = std::abs(newVal); // Should we be complementing the slacks that were basic as well?
//      }
//    }
//
//    if (std::abs(newVal) > PTCONST::param.getEPS()) {
//      packedIndex.push_back(var);
//      packedElem.push_back(newVal);
//    }
//  }
//
//  CoinPackedVector::setVector((int) packedIndex.size(), packedIndex.data(),
//      packedElem.data(), false);
//}
//
///**
// * Create coordinates of the point from the point in the full space,
// * using the cobasis defined in probData
// */
//void Point::setCompNBCoor(const Point & pointFullSpace,
//    const ProblemData & origProbData) {
//  std::vector<int> packedIndex;
//  std::vector<double> packedElem;
//
//  packedIndex.reserve(origProbData.numNB);
//  packedElem.reserve(origProbData.numNB);
//
//  const int* ind = pointFullSpace.getIndices();
//  const double* el = pointFullSpace.getElements();
//  const int numElem = pointFullSpace.getNumElements();
//
//  // Since the full vector has indices sorted in order
//  // with first structural then slack
//  // and that is also the order in nonBasicVarIndex,
//  // adding indices in this order will also keep everything
//  // in the resulting packed vector still in order
//  // wrt the non-basic variable indices.
//  for (int i = 0; i < numElem; i++) {
//    int curr_var = ind[i];
//    int nb_ind = 0;
//    if (curr_var < origProbData.numCols) {
//      nb_ind = origProbData.rowOfVar[curr_var];
//    } else {
//      nb_ind = origProbData.rowOfSlack[curr_var - origProbData.numRows];
//    }
//    // Recall that nb_ind will either be positive,
//    // in which case that variable was originally basic,
//    // or it will be negative, in which case nb_ind will be
//    //   -1 - cobasic index
//    if (nb_ind >= 0)
//      continue;
//
//    nb_ind = -1 - nb_ind;
//    double val = el[i];
//
//    // We do not bother checking that this value is non-zero
//    // Should already be non-zero, since it is being taken from a packed vector
//    packedIndex.push_back(nb_ind);
//    packedElem.push_back(val);
//  }
//  CoinPackedVector::setVector((int) packedIndex.size(), packedIndex.data(),
//      packedElem.data(), false);
//}
//

/**
 * Assumed to be on the same set of variables
 */
void Point::setCompNBCoor(const OsiSolverInterface* const tmpSolver,
    const OsiSolverInterface* const origSolver,
    const SolutionInfo& origProbData, const int deletedVar) {
  // If a variable was deleted to make tmpSolver, then we will need to adjust the index
  // It is -1 because we need the index in the *new* space
  const int deletedVarAdjustment = (deletedVar >= 0) ? -1 : 0;

  // For keeping the packed coordinates to input as a new point
  std::vector<int> packedIndex;
  std::vector<double> packedElem;

  packedIndex.reserve(origProbData.numNB);
  packedElem.reserve(origProbData.numNB);

  objViolation = tmpSolver->getObjValue() - origSolver->getObjValue();

  // Sometimes points have tiny values
  // These may be "necessary" for validity
  // E.g., if the point is (p1,p2), and c is the nb obj, we need c1 * p1 + c2 * p2 >= 1 (actually 1 - 1e-7)
  // It may be that p1 is tiny, but c1 is very large, and c2 * p2 < 0 by that amount, roughly
  // Other times, if the non-tiny values satisfy the objective cut on their own, let's ignore the tiny ones
  // We will not add the tiny indices + values until the end, when we check if they are necessary
  std::vector<int> tinyIndex;
  std::vector<double> tinyElem;
  double tinyObjOffset = 0.; // how much c . p changes when tiny values are ignored
  double nonTinyObj = 0.;

  // All coefficients are going to be non-negative, since we work in the complemented NB space
  for (int i = 0; i < origProbData.numNB; i++) {
    const int currVar = origProbData.nonBasicVarIndex[i];
    const int currVarTmpIndex = (currVar < deletedVar) ? currVar : currVar + deletedVarAdjustment;
    double tmpVal = 0., origVal = 0.;
    if (currVar < origProbData.numCols) {
      tmpVal = tmpSolver->getColSolution()[currVarTmpIndex];
      origVal = origSolver->getColSolution()[currVar];
    } else {
      const int currRow = currVar - origProbData.numCols;
      tmpVal = tmpSolver->getRightHandSide()[currRow]
          - tmpSolver->getRowActivity()[currRow];
      origVal = 0.0;
    }
    // ***
    // For *struct* var:
    // If the variable is lower-bounded, then we need to shift it up for the comp space
    // That is, xComp = x - lb
    // If the variable is upper-bounded, we need to get the value in the complemented space
    // That is, xComp = ub - x
    // What to do if the variable is fixed NB or free NB?
    // In the former case, thinking of it as a lower-bounded variable, it will simply be zero
    // in any solution, including in the original one at v, in which case we don't need to
    // worry about it for the packed ray.
    // I think the latter case we could have handled by preprocessing the free variables out
    // ****
    // For *slack* var:
    // In the original NB space at v, all slacks were at zero.
    // We simply look at b[row] - activity[row]
    // For >= rows, slack is non-positive
    // For <= rows, slack is non-negative
    // We take absolute value to deal with this
    const double newVal = std::abs(tmpVal - origVal);

    if (!isZero(newVal)) {
      packedIndex.push_back(i);
      packedElem.push_back(newVal);
      nonTinyObj += newVal * origProbData.nonBasicReducedCost[i];
    } // non-tiny
    else if (!isZero(newVal * origProbData.nonBasicReducedCost[i])) {
      tinyIndex.push_back(i);
      tinyElem.push_back(newVal);
      tinyObjOffset += newVal * origProbData.nonBasicReducedCost[i];
    } // tiny
  } /* end iterating over non-basic elements from original basis */

  // Check whether tiny elements are needed
  bool useTinyElements = //lessThanVal(nonTinyObj, 0., param.getEPS())
          !isVal(nonTinyObj, objViolation, param.getEPS());
  const int numNonTiny = packedIndex.size();
  if (useTinyElements) {
    // We may not need all of the tiny elements
    // Sort the tiny elements in decreasing order and keep adding them until the criterion is reached
    const int numTinyTotal = tinyIndex.size();
    CoinPackedVector tinyVec(numTinyTotal, tinyIndex.data(), tinyElem.data());
    tinyVec.sortDecrElement();
    int numTiny = 0;
    for (int i = 0; i < numTinyTotal && useTinyElements; i++) {
      const double val = tinyVec.getElements()[i];
      numTiny++;
      nonTinyObj += val * origProbData.nonBasicReducedCost[tinyVec.getIndices()[i]];
      useTinyElements = //lessThanVal(nonTinyObj, 0., param.getEPS())
          !isVal(nonTinyObj, objViolation, param.getEPS());
    }

    std::vector<int> newPackedIndex(numNonTiny + numTiny, 0);
    std::vector<double> newPackedElem(numNonTiny + numTiny, 0);
    int ind = 0, ind1 = 0, ind2 = 0;
    const int scale = 1.; // perhaps in a future version we will scale things
    while (ind1 < numNonTiny) {
      while (ind2 < numTiny && tinyIndex[ind2] < packedIndex[ind1]) {
        newPackedIndex[ind] = tinyVec.getIndices()[ind2];
        newPackedElem[ind] = tinyVec.getElements()[ind2] * scale;
        ind++;
        ind2++;
      }
      newPackedIndex[ind] = packedIndex[ind1];
      newPackedElem[ind] = packedElem[ind1] * scale;
      ind++;
      ind1++;
    }
    while (ind2 < numTiny) {
      newPackedIndex[ind] = tinyVec.getIndices()[ind2];
      newPackedElem[ind] = tinyVec.getElements()[ind2] * scale;
      ind++;
      ind2++;
    }

    CoinPackedVector::setVector((int) newPackedIndex.size(),
        newPackedIndex.data(), newPackedElem.data(), false);
  } else {
    CoinPackedVector::setVector((int) packedIndex.size(), packedIndex.data(),
        packedElem.data(), false);
  }

  if (!isVal(nonTinyObj, objViolation, param.getEPS())) {
    if (!isVal(nonTinyObj, objViolation, 1e-3)) {
      error_msg(errorstring,
          "Point: Calculated dot product with obj differs from solver's. Obj viol from solver: %.8f. Calculated: %.8f.\n",
          objViolation, nonTinyObj);
      writeErrorToLog(errorstring, GlobalVariables::log_file);
      exit(1);
    } else {
      warning_msg(warnstring,
          "Point: Calculated dot product with obj differs from solver's. Obj viol from solver: %.8f. Calculated: %.8f.\n",
          objViolation, nonTinyObj);
    }
  }
} /* setCompNBCoor */

void Point::setCompNBCoor(const double* const currColValue,
    const double* const currSlackValue, const OsiSolverInterface* const origSolver,
    const SolutionInfo& origProbData, const int deletedVar) {
  // If a variable was deleted to make tmpSolver, then we will need to adjust the index
  // It is -1 because we need the index in the *new* space
  const int deletedVarAdjustment = (deletedVar >= 0) ? -1 : 0;

  // For keeping the packed coordinates to input as a new point
  std::vector<int> packedIndex;
  std::vector<double> packedElem;

  packedIndex.reserve(origProbData.numNB);
  packedElem.reserve(origProbData.numNB);

  objViolation = 0.;

  // All coefficients are going to be non-negative, since we work in the complemented NB space
  for (int i = 0; i < (int) origProbData.nonBasicVarIndex.size(); i++) {
    const int currVar = origProbData.nonBasicVarIndex[i];
    const int currVarTmpIndex = (currVar < deletedVar) ? currVar : currVar + deletedVarAdjustment;
    double tmpVal = 0., origVal = 0.;
    double objCoeff = 0.;
    if (currVar < origProbData.numCols) {
      tmpVal = currColValue[currVarTmpIndex];
      origVal = origSolver->getColSolution()[currVar];
      objCoeff = origSolver->getObjCoefficients()[currVar];
    } else {
      const int currRow = currVar - origProbData.numCols;
      tmpVal = currSlackValue[currRow];
      origVal = 0.0;
    }
    // ***
    // For *struct* var:
    // If the variable is lower-bounded, then we need to shift it up for the comp space
    // That is, xComp = x - lb
    // If the variable is upper-bounded, we need to get the value in the complemented space
    // That is, xComp = ub - x
    // What to do if the variable is fixed NB or free NB?
    // In the former case, thinking of it as a lower-bounded variable, it will simply be zero
    // in any solution, including in the original one at v, in which case we don't need to
    // worry about it for the packed ray.
    // TODO The latter case we have handled by preprocessing the free variables out
    // ****
    // For *slack* var:
    // In the original NB space at v, all slacks were at zero.
    // We simply look at b[row] - activity[row]
    // For >= rows, slack is non-positive
    // For <= rows, slack is non-negative
    // We take absolute value to deal with this
    const double newVal = std::abs(tmpVal - origVal);
    objViolation += newVal * origProbData.nonBasicReducedCost[i];

    if (!isZero(newVal) || !isZero(newVal * objCoeff)) {
      packedIndex.push_back(i);
      packedElem.push_back(newVal);
    }
  }
  CoinPackedVector::setVector((int) packedIndex.size(), packedIndex.data(),
      packedElem.data(), false);
}

/**
 * Input has to have the indices sorted in the same order (ideally increasing)
 * Returns true if one point is exactly same as the other (no scaling for the point)
 */
bool Point::pointsDifferent(const Point& pt1, const CoinPackedVector& pt2) {
  return isRowDifferent(pt1, 1., pt2, 1., param.getEPS()) != 0;
} /* pointsDifferent */

#ifdef TRACE
void Point::printSparseVector() const {
  printSparseVector(stdout);
}

void Point::printSparseVector(FILE* fptr) const {
  int numElems = CoinPackedVector::getNumElements();
  const int* index = CoinPackedVector::getIndices();
  const double* element = CoinPackedVector::getElements();
  printf("Num elements is %d.\n", numElems);
  for (int i = 0; i < numElems; i++) {
    fprintf(fptr, "\t%d: %f\n", index[i], element[i]);
  }
}
#endif

const std::string Point::vectorString() const {
  const int numElems = this->getNumElements();
  const int* vecIndex = this->getIndices();
  const double* vecVal = this->getElements();
  char vec_string[200];
  if (numElems == 0) {
    snprintf(vec_string, sizeof(vec_string) / sizeof(char),
        "(zero vector)");
  } else if (numElems == 1) {
    snprintf(vec_string, sizeof(vec_string) / sizeof(char), "(%d: %s)",
        vecIndex[0], stringValue(vecVal[0]).c_str());
  } else if (numElems == 2) {
    snprintf(vec_string, sizeof(vec_string) / sizeof(char),
        "(%d: %s, %d: %s)", vecIndex[0], stringValue(vecVal[0]).c_str(),
        vecIndex[1], stringValue(vecVal[1]).c_str());
  }
  std::string tmp(vec_string);
  return tmp;
}
