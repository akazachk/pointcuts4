## Arguments
# First argument: recompute, test or best

## Set up proper path variables
from os import environ
PROJ_DIR = environ['PROJ_DIR']
CUT_TYPE = environ['CUT_TYPE']
#PROJ_DIR = '../..'
ADD_TO_PATH = PROJ_DIR + '/scripts/python_scripts'

## Ensure CUT_TYPE is set
if (CUT_TYPE not in ['pha', 'vpc', 'tilted']):
  raise ValueError( "CUT_TYPE needs to be 'pha' or 'vpc' or 'tilted'." )

from sys import path
path.append(ADD_TO_PATH)

## Now import all the relevant code
from utility import *
from lgreader import LGReader
from matrix2latex import matrix2latex
import csv

## Get arguments
from sys import argv
is_test = False
is_best = False
recompute = False
if (len(argv) > 1):
  try:
    recompute = int(argv[1])
  except:
    raise TypeError( "First argument should be 0 or 1." )

  if (recompute not in [0,1]):
    raise ValueError( "recompute needs to be 0 or 1, but given is: %d." % recompute )
  
  if (len(argv) > 2):
    is_test = True if argv[2].lower() in ['test'] else False
    is_best = True if argv[2].lower() in ['best'] else False

is_full = not is_test and not is_best

def chain_elements_or_slices(*elements_or_slices):
    new_list = []
    for i in elements_or_slices:
        if isinstance(i, list):
            new_list.extend(i)
        else:
            new_list.append(i)
    return new_list

## In and out directories
in_dir = PROJ_DIR + '/results/instances'
out_dir = PROJ_DIR + '/results/tables'
f_name = in_dir + '/' + CUT_TYPE + '-full.csv'
if is_test:
  f_name = in_dir + '/' + CUT_TYPE + '-test.csv'
if is_best:
  f_name = in_dir + '/' + CUT_TYPE + '-best.csv'

## Options 'test' and 'full' only make sense with pha
if (is_test or is_full) and (CUT_TYPE != "pha"):
  raise ValueError( "CUT_TYPE needs to be 'pha' if test or full is being used." )

## Uncomment any tables you wish to ignore
ignore_table_list = [
    #0,  # inst-info
    #1,  # gap-closed
    #2,  # obj-fails
    #3,  # point-analysis
    #4,  # hplane-analysis
    #5,  # cut-heur-analysis
    #6,  # active-cuts
    7   # param-analysis
  ]

if is_best:
  ignore_table_list.extend([2,3,4,5,7])

instance = [
  'bell3a',
  'bell3b',
  'bell4',
  'bell5',
  'blend2',
  'bm23',
  'egout',
  'flugpl',
  'glass4',
  'gt2',
  'k16x240',
  'lseu',
  'mas74',
  'mas76',
  'mas284',
  'misc01',
  'misc02',
  'misc03',
  'misc05',
  'misc07',
  'mod008',
  'mod013',
  'modglob',
  'p0033',
  'p0040',
  'p0201',
  'p0282',
  'p0291',
  'pipex',
  'pp08a',
  'probportfolio',
  'rgn',
  'sample2',
  'sentoy',
  'stein15_nosym',
  'stein27_nosym',
  'stein45_nosym',
  'timtab1',
  'vpm1',
  'vpm2'
]

instance_shortlist = [
  'bell3a',
  'bell3b',
  'bell4',
  'bell5',
  'blend2',
  'bm23',
  'egout',
  'flugpl',
  'gt2',
  'k16x240',
  'lseu',
  'mas74',
  'mas76',
  'mas284',
  'misc05',
  'mod008',
  'mod013',
  'modglob',
  'p0033',
  'p0040',
  'p0282',
  'p0291',
  'pipex',
  'pp08a',
  'probportfolio',
  'sample2',
  'sentoy',
  'stein15_nosym',
  'stein27_nosym',
  'stein45_nosym',
  'timtab1',
  'vpm1',
  'vpm2'
]

f_name_stub_list = [
        "inst-info",  # 0
        "gap-closed",  # 1
        "obj-fails",  # 2 
        "point-analysis",  # 3
        "hplane-analysis",  # 4
        "cut-heur-analysis",  # 5
        "active-cuts",  # 6
        "param-analysis"  # 7
    ]
num_tables = len(f_name_stub_list)

caption_list = [
        "Set of instances used in experiments, along with the number of rows and columns for each instance, and the average amount of time.",  # inst-info
        "Best gap closed and number of cuts.",  # gap-closed
        "Number of cuts generated.",  # obj-fails
        "Number of points and final points generated.",  # point-analysis
        "Best gap closed based on hyperplane heuristic used.",  # hplane-analysis
        "Best gap closed based on objective function used.",  # cut-heur-analysis
        "Active cuts by heuristic used to generate them.",  # active-cuts
        "Parameters."  # param-analysis
    ]

align_list = [
        r"lccc",  # inst-info
        r"l*{2}{c}*{4}{H}cc>{\bfseries}c*{5}{c}",  # gap-closed
        r"lHcccccHHccccHccHcH",  # obj-fails
        r"lccc",  # point-analysis
        r"lHHc|*{3}{c}|*{5}{c}|*{4}{c}|*{3}{H}|*{4}{H}|*{4}{H}",  # hplane-analysis"
        r"lcc*{4}{c}*{6}{c}",  # cut-heur-analysis
        r"lcccccccccccccc",  # active-cuts
        r"lccccccccc"  # param-analysis
    ]

label_list = [
        "tab:"+f_name_stub_list[i]
        for i in range(num_tables)
    ]

float_style0 = "%.0f"
float_style1 = "%.1f"
float_style2 = "%.2f"
int_style = "%d"
gen_style = "%g"
format_list = [
        [gen_style, int_style, int_style, float_style1],  # inst-info
        [gen_style] + 2*[int_style] + 6*[float_style2] + [float_style2] + 4*[int_style] + [float_style0],  #  gap-closed
        int_style,  # obj-fails 
        [gen_style] + 8*[int_style] + [float_style0],  # point-analysis
        float_style2,  # hplane-analyis
        float_style2,  # cut-heur-analysis
        int_style,  # active-cuts
        float_style2  # param-analysis
    ]

header_length_list = [
        1,  # inst-info
        2,  # gap-closed
        1,  # obj-fails
        1, #2,  # point-analysis
        2,  # hplane-analysis
        1,  # cut-heur-analysis
        1,  # active-cuts
        1  # param-analysis
    ]

summaryrows_list = [
        0,  # inst-info
        0,  # gap-closed
        0,  # obj-fails
        0,  # point-analysis
        1,  # hplane-analyis
        1,  # cut-heur-analysis
        0,  # active-cuts
        1  # param-analysis
    ]

midruleIndex_list = [
        [],  # inst-info
        [len(instance_shortlist), len(instance_shortlist)+1],  # gap-closed
        [],  # obj-fails
        [],  # point-analyis
        [],  # hplane-analysis
        [],  # cut-heu-analysis
        [],  # active-cuts
        []  # param-analysis
    ]

tab = []

if __name__ == "__main__":
    if (recompute):
        reader = LGReader(f_name, inst_name = instance)
        # Save the best run data
        #print( "## Saving best runs to file ##" )
        #reader.write_best_params(PROJ_DIR + "/data/best_params")
        #with open(out_dir + '/' + 'best_runs.csv','wb') as myfile:
        #  wr = csv.writer(myfile)
        #  wr.writerow(reader._header[0])
        #  wr.writerow(reader._header[1])
        #  for inst in range(len(instance)):
        #    print( "\tBest run: instance %s." % instance[inst] )
        #    tmp = reader.get_row(reader._bestrow[inst])
        #    wr.writerow(tmp)
        if (0 in ignore_table_list):
          tab.append([])
        else:
          tab.append( reader.inst_info(num_rays_cut=range(4,500)) )
        if (1 in ignore_table_list):
          tab.append([])
        else:
          tab.append( reader.gap_closed_table(num_rays_cut=range(4,500)) )
        if (2 in ignore_table_list):
          tab.append([])
        else:
          tab.append( reader.obj_fails_table() )
        if (3 in ignore_table_list):
          tab.append([])
        else:
          tab.append( reader.point_analysis() )
        if (4 in ignore_table_list):
          tab.append([])
        else:
          tab.append( reader.hplane_analysis() )
        if (5 in ignore_table_list):
          tab.append([])
        else:
          tab.append( reader.cut_heur_analysis() )
        if (6 in ignore_table_list):
          tab.append([])
        else:
          tab.append( reader.active_cuts_table() )
        if (7 in ignore_table_list):
          tab.append([])
        else:
          tab.append( reader.param_analysis() )
    else:
        import re
        for i in range(num_tables):
          if (i in ignore_table_list):
            continue
          tab.append( csv2table(out_dir + '/' + f_name_stub_list[i] + '.csv') )

    for i in range(num_tables):
        if (i in ignore_table_list):
          continue
        f_name_stub = f_name_stub_list[i]
        start_table = header_length_list[i]
        out_stub = out_dir + '/' + f_name_stub 
        out_name = out_stub + '.csv'
        if (recompute):
            tabcsv = print_table(tab[i], sep=',', outfile=out_name, overwrite=True)
        out_name = out_stub + '.tex'
        with open(out_name, 'wb') as out_f:
            out_f.write(
                matrix2latex(tab[i][start_table:], None,
                "singlespace", "adjustbox", "tabular",
                headerRow=tab[i][0:start_table], 
                alignment=align_list[i], 
                label=label_list[i], 
                formatColumn=format_list[i],
                summaryrows = summaryrows_list[i],
                midruleIndex = midruleIndex_list[i],
                caption=caption_list[i])
            )

    # Also compute the same tables for the short list
    # Need to compute averages when required for only the shortlist instances
    #"inst-info",
    #"gap-closed",
    #"cut-analysis",
    #"point-analysis",
    #"hplane-analysis",
    #"cut-heur-analysis",
    #"active-cuts"
    caption_list_short = [
            "Set of instances with non-zero gap closed, along with the number of rows and columns for each instance, and the average amount of time.",  # inst-info
            "Best gap closed and maximum number of active cuts after reoptimization on instances with any improvement.",  # gap-closed
            "Number of cuts generated for instances with any gap closed.",  # cut-analysis
            "Number of points and final points generated for instances with any gap closed.",  # point-analysis
            "Best gap closed by hyperplane heuristic used, for instances with any improvement across all settings.",  # hplane-analysis
            "Best gap closed by objective function used, for instances with any improvement over SICs across all settings.",  # cut-heur-analysis
            "Active cuts for instances with improvements by heuristic used to generate them.",  # active-cuts
            "Parameters."  # param-analysis
        ]
    avg_col_indices = [
            [],  # inst-info
            [7,8,9,14],  # gap-closed
            [],  # cut-analysis
            [],  #[8],  # point-analysis
            range(1,15),  # hplane-analysis
            range(1,13),  # cut-heur-analysis
            [],  # active-cuts
            range(1,10)  # param-analysis
        ]
    shortlist_indices = [
            i for i in range(len(instance)) 
            if instance[i] in instance_shortlist
        ]
    len_shortlist = len(shortlist_indices)
    summaryrows_list_shortlist = [
            0,  # inst-info
            1,  # gap-closed
            0,  # cut-analysis
            0,  # point-analysis
            1,  # hplane-analysis
            1,  # cut-heur-analysis
            0,  # active-cuts
            1  # param-analysis
        ]
    for i in range(num_tables):
        if (i in ignore_table_list):
          continue
        f_name_stub = f_name_stub_list[i]
        start_table = header_length_list[i]
        tmp_tab = chain_elements_or_slices(
                tab[i][0:start_table], 
                #[tab[i][j+start_table] for j in shortlist_indices]
                [tab[i][j]
                for j in range(len(tab[i])) if tab[i][j][0] in instance_shortlist]
            )
        if (len(avg_col_indices[i]) > 0):
            # Add new average row
            avg = [
                    sum([float(row[j]) for row in tmp_tab[header_length_list[i]:]]) / len_shortlist
                    if j in avg_col_indices[i]
                    else ''
                    for j in range(len(tmp_tab[0]))
                ]
            avg[0] = "Average"
            tmp_tab.append(avg)
        out_stub = out_dir + '/' + f_name_stub + '_short'
        out_name = out_stub + ".csv"
        if (recompute):
            tabcsv = print_table(tmp_tab, sep=',', outfile=out_name, overwrite=True)
        out_name = out_stub + ".tex"
        with open(out_name, 'wb') as out_f:
            out_f.write(
                matrix2latex(tmp_tab[start_table:], None,
                "singlespace", "adjustbox", "tabular",
                headerRow=tmp_tab[0:start_table], 
                alignment=align_list[i], 
                label=label_list[i]+"-short", 
                formatColumn=format_list[i],
                summaryrows=summaryrows_list_shortlist[i],
                caption=caption_list_short[i])
            )
