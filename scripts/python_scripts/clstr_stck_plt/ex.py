import pandas as pd
import matplotlib.cm as cm
import numpy as np
import matplotlib.pyplot as plt
from copy import deepcopy
from clstr_stck_plt import *

# create fake dataframes
#import pdb; pdb.set_trace()
#df1 = pd.DataFrame(np.random.rand(4, 5),
#                   index=["A", "B", "C", "D"],
#                   columns=["I", "J", "K", "L", "M"])
#df2 = pd.DataFrame(np.random.rand(4, 5),
#                   index=["A", "B", "C", "D"],
#                   columns=["I", "J", "K", "L", "M"])
#df3 = pd.DataFrame(np.random.rand(4, 5),
#                   index=["A", "B", "C", "D"], 
#                   columns=["I", "J", "K", "L", "M"])


#df01 = pd.read_csv('presolve0-numcuts-1.csv', index_col=0)
#df05 = pd.read_csv('presolve0-numcuts-5.csv', index_col=0)
#df010K = pd.read_csv('presolve0-numcuts10K.csv', index_col=0)
#df15 = pd.read_csv('presolve1-numcuts-5.csv', index_col=0)

#dfall = [df01, df05, df010K]
df = pd.read_csv('gap_5K.csv', index_col=0)

## The data is sb, space, table, space, etc
num_threshold_cols = 1
df_threshold = df.iloc[0:,0:num_threshold_cols]
num_offset_cols = num_threshold_cols+1
num_cols_per_table = 2
num_intertable_cols = 1

## Num tables is x, where x * (num_cols_per_table + num_intertable_cols) - num_intertable_cols + num_offset_cols = num_df_cols
num_tables = (len(df.columns) - num_offset_cols + num_intertable_cols) / (num_cols_per_table + num_intertable_cols)
dfall = []
for i in range(num_tables):
  start_table = (num_cols_per_table + num_intertable_cols) * i + num_offset_cols
  end_table = start_table + num_cols_per_table
  dfall.append(df.iloc[0:, start_table:end_table])
#import pdb; pdb.set_trace()

#plt.rc('text', usetex=True)
#plt.rc('font',**{'family':'sans-serif','sans-serif':['Computer Modern Sans serif']})
#plt.rcParams.update({'font.size': 10})

#labels = ["01", "05", "15"]
labels = []
#title = "Average \% Gap Closed"
title = "Effect of varying number leaf nodes"

# Then, just call:
fig = plot_clustered_stacked(dfall, df_threshold, labels, title, 3*'/')
fig.savefig('fig.pdf', format='pdf', bbox_inches='tight')
#fig.set_gray(True)
#fig.savefig('fig-bw.pdf', format='pdf', bbox_inches='tight')
