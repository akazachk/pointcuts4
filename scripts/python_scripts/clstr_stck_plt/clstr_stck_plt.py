import pandas as pd
import matplotlib.cm as cm
import numpy as np
import matplotlib.pyplot as plt

def plot_clustered_stacked(dfall, threshold, labels=None, title="multiple stacked bar plot",  H='/', **kwargs):
    """Given a list of dataframes, with identical columns and index, create a clustered stacked bar plot. 
       labels is a list of the names of the dataframe, used for the legend.
       title is a string for the title of the plot.
       H is the hatch used for identification of the different dataframe."""
    ## Setup
    fig = plt.figure()
    is_bw = False
    n_df = len(dfall)
    n_col = len(dfall[0].columns) 
    n_ind = len(dfall[0].index)
    col_width = 1 / float(n_df + 1)
    inter_width = col_width
    margin_width = col_width
    cluster_width = n_df * col_width
    start_x = -0.25
    end_x = start_x + n_ind * cluster_width + (n_ind - 1) * inter_width
     
    plt.rc('text', usetex=True)
    plt.rc('font',**{'family':'sans-serif','sans-serif':['Computer Modern Sans serif']})
    plt.rcParams.update({'font.size': 10})

    axe = plt.subplot(111)
    for df in dfall : # for each data frame
      axe = df.plot(
          kind="bar",
          linewidth=0,
          stacked=True,
          ax=axe,
          legend=False,
          grid=False,
          **kwargs)  # make bar plots

    #axe.axvline(x=0)
    #axe.axvline(x=1)
    #dash_width = 12 
    #inter_dash_width = .5 
    #axe.plot([0,1], [40,40], "r--", linewidth=2, dashes=(dash_width, inter_dash_width))
    #fig.savefig('fig.png')
    #quit()

    ## Now the figure is all of the bars one on top another; we need to shift things about
    h,l = axe.get_legend_handles_labels() # get the handles we want to modify
    from matplotlib import colors as mcolors
    colors = [u'#00843d', u'#f2a900', u'#002d72', u'#a6192e']
    if (is_bw):
      #colors = ['1', '0.75', '0.5', '0.25']
      colors = ['0', '0.5', '0.75', '0.9']
      #plt.set_cmap('gray')
    #colors = [[0, 132, 61], [242, 169, 0], [0, 45, 114], [166, 25, 46]]
    #import pdb; pdb.set_trace()
    for i in range(0, n_df * n_col, n_col): # len(h) = n_col * n_df
        for j, pa in enumerate(h[i:i+n_col]):
            is_top = j > 0 #(rect.get_y() > 0)
            for rect in pa.patches: # for each index
                #print(rect.get_x())
                rect.set_x(rect.get_x() + col_width * i / float(n_col))
                rect.set_width(col_width)

                # transparency
                alpha = 1 - 0.5 * is_top
                rect.set_alpha(alpha)
                #alpha = '7F' if is_top else 'FF'
                #color.append(alpha)

                # color
                #if (i / n_col % n_df) >= len(colors):
                #  continue
                #color = list(colors[i/n_col %  n_df]) 
                #color = [color[c] / 255. for c in range(3)]
                color_index = (i / n_col % n_df) % len(colors)

                # hatching
                if (is_bw):
                  rect.set_color(colors[color_index])
                  rect.set_edgecolor('0')
                  set_ind = int((i / n_col % n_df) / 4)
                  set_ind = (set_ind + 1) % 3
                  rect.set_hatch(H * set_ind) #edited part     

                  #rect.set_label(str(i))
                  #print(rect.get_x())
                else: 
                  color = mcolors.to_rgb(colors[color_index])
                  rect.set_color(color)
                  rect.set_edgecolor('0')

    axe.set_title(title)
    axe.set_ylabel("Average \% gap closed")
    axe.set_xticks((np.arange(start_x + cluster_width / 2., end_x, cluster_width + inter_width)))
    axe.set_xticklabels(df.index, rotation = 0) #weight='bold')
    #axe.tick_params(axis=u'both', which=u'both',length=0)
    axe.tick_params(
        axis='both',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom='off',      # ticks along the bottom edge are off
        top='off',
        left='off',
        right='off')         # ticks along the top edge are off
    #axe.tick_params(axis='y', which='both', labelsize=10)
    axe.set_xlim(start_x - margin_width, end_x + margin_width)
    axe.set_frame_on(False)
    #axe.patch.set_visible(False)
    #fig.patch.set_visible(False)
    #plt.autoscale(enable=True, axis='both', tight=False)

    ## Add upper bound line
    scale_points_to_units = 45. #50. #49.
    line_width = 1
    num_dashes_per_column = 2.5
    num_inter_dashes_per_column = 2
    dash_to_inter_dash_ratio = 2
    dash_scale = (2. / 7)
    inter_dash_scale = (1. / 7)
    dash_width = (scale_points_to_units / line_width) * col_width * dash_scale #3 * cluster_width
    inter_dash_width = (scale_points_to_units / line_width) * col_width * inter_dash_scale #2 * cluster_width
    #axe.plot([0,1], [10,10], "r--", linewidth=2, dashes=(dash_width, inter_dash_width))
    strengthen_flag = 0
    gurobi_flag = 1
    threshold_width = cluster_width - strengthen_flag * cluster_width / 2. - gurobi_flag * cluster_width / 2.
    start_thr_ind = 0 + (len(threshold.index) > 6)
    end_thr_ind = len(threshold.index) 
    for i in range(start_thr_ind, end_thr_ind):
      curr_threshold = threshold.iloc[i,0]
      curr_start_x = start_x + i * (cluster_width + inter_width) 
      curr_end_x = curr_start_x + threshold_width
      axe.plot([curr_start_x, curr_end_x], [curr_threshold, curr_threshold], "r--", linewidth=line_width, dashes=(dash_width, inter_dash_width), label=("VPC upper bound" if i == start_thr_ind else None))
      if (strengthen_flag == 1):
          curr_threshold = threshold.iloc[i,1]
          curr_start_x = curr_end_x
          curr_end_x = curr_start_x + threshold_width
          axe.plot([curr_start_x, curr_end_x], [curr_threshold, curr_threshold], "r--", linewidth=line_width, dashes=(dash_width, inter_dash_width), label=None)
          #axe.plot([curr_start_x, curr_end_x], [curr_threshold, curr_threshold], "r--", linewidth=line_width, dashes=(dash_width, inter_dash_width), label=("VPC\\textsuperscript{+} upper bound" if i == 0 else None))

    ## Process legend
    legend_loc = 'upper left'
    #n=[]        
    #for i in range(n_df):
    #    n.append(axe.bar(0, 0, color="gray", hatch=H * i))
    #l1 = axe.legend(h[:n_col], l[:n_col], loc=legend_loc, frameon=False, fontsize='small')
    #if labels is not None:
    #    l2 = plt.legend(n, labels, loc=legend_loc, frameon=False, fontsize='small') 
    #axe.add_artist(l1)
    handles,labels = axe.get_legend_handles_labels() # get the handles we want to modify
    num_handles = len(handles)
    htmp = [handles[(i+1)%num_handles] for i in range(num_handles)]
    ltmp = [labels[(i+1)%num_handles] for i in range(num_handles)]
    plt.legend(htmp, ltmp, loc=legend_loc,frameon=False,fontsize=6,ncol=2)
    
    ## Show and save figure
    #fig.show()
    #with open('fig.png', 'w') as outfile:
    #    fig.canvas.print_png(outfile)
    #fig.savefig('fig.png')

    return fig

if __name__ == '__main__':
  # create fake dataframes
  df1 = pd.DataFrame(np.random.rand(4, 5),
                   index=["A", "B", "C", "D"],
                   columns=["I", "J", "K", "L", "M"])
  df2 = pd.DataFrame(np.random.rand(4, 5),
                   index=["A", "B", "C", "D"],
                   columns=["I", "J", "K", "L", "M"])
  df3 = pd.DataFrame(np.random.rand(4, 5),
                   index=["A", "B", "C", "D"], 
                   columns=["I", "J", "K", "L", "M"])

  # Then, just call :
  plot_clustered_stacked([df1, df2, df3],["df1", "df2", "df3"])
