#!/bin/bash
# PROJ_DIR should be defined for these scripts
#
# Three arguments: 
# 1. 'pha', 'vpc', 'vpc_bb', 'vpc_best', or 'tilted' [required, no default]
# 2. full path to instance list, which should be located in the same directory as the instances [optional; default: ${PROJ_DIR}/data/instances/500x500.instances]
# 3. '1' or a '0', referrring to whether batch mode is used [optional; default: 0]
# 4. 'best', 'test', or 'full' [optional; default: best]
#
# The third argument is only relevant for pha
# In batch mode, give the first argument as 1 and the second (optional) is where the set of batches is
# In non-batch mode, which is the default, the first (optional) argument is the directory containing the instances

# Defaults
DEFAULT_INSTANCE_DIR="${PROJ_DIR}/data/instances"
DEFAULT_INSTANCE_LIST="${PROJ_DIR}/data/instances/500x500.instances"
INSTANCE_LIST="${DEFAULT_INSTANCE_LIST}"
DEFAULT_BATCH_LIST="${PROJ_DIR}/data/instances/500x500.batch"
BATCH_MODE=0
#INSTANCE_DIR="${DEFAULT_INSTANCE_DIR}"

SCRIPT_DIR="${PROJ_DIR}/scripts/run_scripts"
OUT_DIR="${PROJ_DIR}/results/instances"

RUN_TYPE_STUB="best"

DEBUG="echo"
DEBUG=""

# Process cut type
if [ -z "$1" ]
then 
  echo "*** ERROR: Need to specify cut type."
  exit 1
#elif [ "$1" = "pha" -o "$1" = "vpc" -o "$1" = "vpc_best" -o "$1" = "tilted" ]
#then
else
  export CUT_TYPE="$1"
#else
#  echo "*** ERROR: Specified cut type is invalid."
#  exit 1
fi

# Process run type
if [ "$2" = "test" -o "$3" = "test" -o "$4" = "test" ]
then 
  RUN_TYPE_STUB="test"
elif [ "$2" = "full" -o "$3" = "full" -o "$4" = "full" ]
then 
  RUN_TYPE_STUB="full"
fi

# Process other arguments
if [ -z "$2" ]
then 
  INSTANCE_LIST="${DEFAULT_INSTANCE_LIST}"
elif [ "$2" = 1 ]
then 
  BATCH_MODE=1
  #INSTANCE_DIR="${PROJ_DIR}/data/instances/batches"
  INSTANCE_LIST="${DEFAULT_BATCH_LIST}"
elif [ "$2" = 0 ]
then 
  BATCH_MODE=0
elif [ "$2" != "best" -a "$2" != "test" -a "$2" != "full" ]
then 
  INSTANCE_LIST="$2"
  if [ "$3" = 1 ]
    then 
      BATCH_MODE=1
  elif [ "$3" = 0 ]
    then 
      BATCH_MODE=0
  fi
fi

TMPNAME="run_${CUT_TYPE}"
TMPNAME_EXT=".py"

# If pha, need to append to TMPNAME
if [ "${CUT_TYPE}" = "pha" ]
then
  TMPNAME="${TMPNAME}_${RUN_TYPE_STUB}"
elif [ "${RUN_TYPE_STUB}" = "test" -o "${RUN_TYPE_STUB}" = "full" ]
then
  echo "*** ERROR: Test and full runs only make sense for pha."
  exit 1
fi

echo "Options selected: run ${CUT_TYPE}/${TMPNAME} using instances given in ${INSTANCE_LIST}. In batch mode? ${BATCH_MODE}."
#exit 1

# Proceed depending on whether run is in batches or not
if [ $BATCH_MODE == 0 ] 
then
  if [ "${RUN_TYPE_STUB}" = "full" ]
    then
      echo "Running ${CUT_TYPE}/${TMPNAME}0${TMPNAME_EXT} from ${INSTANCE_LIST}"
      nohup python ${SCRIPT_DIR}/${CUT_TYPE}/${TMPNAME}0${TMPNAME_EXT} 0 ${INSTANCE_LIST} >& ${OUT_DIR}/nohup.out &
      echo "Running ${CUT_TYPE}/${TMPNAME}1${TMPNAME_EXT} from ${INSTANCE_LIST}"
      nohup python ${SCRIPT_DIR}/${CUT_TYPE}/${TMPNAME}1${TMPNAME_EXT} 0 ${INSTANCE_LIST} >& ${OUT_DIR}/nohup.out &
      echo "Running ${CUT_TYPE}/${TMPNAME}2${TMPNAME_EXT} from ${INSTANCE_LIST}"
      nohup python ${SCRIPT_DIR}/${CUT_TYPE}/${TMPNAME}2${TMPNAME_EXT} 0 ${INSTANCE_LIST} >& ${OUT_DIR}/nohup.out &
  else
    echo "Running ${CUT_TYPE}/${TMPNAME}${TMPNAME_EXT} from ${INSTANCE_LIST}"
    nohup python ${SCRIPT_DIR}/${CUT_TYPE}/${TMPNAME}${TMPNAME_EXT} 0 ${INSTANCE_LIST} >& ${OUT_DIR}/nohup.out &
  fi
else
  FSTUB=`head -n 1 ${INSTANCE_LIST}`
  tmpfilename=""
  for line in `tail -n +2 ${INSTANCE_LIST}`; do
    # Skip empty lines
    if [ -z "$line" ]
    then
      continue
    fi
    
    #if [ ! -z $line ]
    #then
    #  echo "Current line: $line"
    #  echo "Current batch: $batch"
    #fi

    # If line ends with '/', then we start a new batch
    len="$((${#line}-1))"
    if [ "${line:len:1}" == "/" ]
    then
      # If there was an old batch, then run it
      if [ ! -z "${tmpfilename}" ]
      then
        if [ "${RUN_TYPE_STUB}" = "full" ]
          then
            echo "Running ${CUT_TYPE}/${TMPNAME}0${TMPNAME_EXT} from ${tmpfilename}"
            nohup python ${SCRIPT_DIR}/${CUT_TYPE}/${TMPNAME}0${TMPNAME_EXT} 1 ${tmpfilename} >& ${OUT_DIR}/nohup.out &
            echo "Running ${CUT_TYPE}/${TMPNAME}1${TMPNAME_EXT} from ${tmpfilename}"
            nohup python ${SCRIPT_DIR}/${CUT_TYPE}/${TMPNAME}1${TMPNAME_EXT} 1 ${tmpfilename} >& ${OUT_DIR}/nohup.out &
            echo "Running ${CUT_TYPE}/${TMPNAME}2${TMPNAME_EXT} from ${tmpfilename}"
            nohup python ${SCRIPT_DIR}/${CUT_TYPE}/${TMPNAME}2${TMPNAME_EXT} 1 ${tmpfilename} >& ${OUT_DIR}/nohup.out &
        else
          echo "Running ${CUT_TYPE}/${TMPNAME}${TMPNAME_EXT} from ${tmpfilename}"
          nohup python ${SCRIPT_DIR}/${CUT_TYPE}/${TMPNAME}${TMPNAME_EXT} 1 ${tmpfilename} >& ${OUT_DIR}/nohup.out &
        fi
      fi  

      # Now we create the new batch
      batchstub="${line:0:len}"
      tmpfilename="/tmp/${FSTUB}.batch${batchstub}"
      tmpfile=$(mktemp -q ${tmpfilename})
      echo "${FSTUB}" > "${tmpfilename}"
      echo "${line}" >> "${tmpfilename}"
    else
      # Else, we add the current line to the current batch
      echo "${line}" >> "${tmpfilename}"
    fi  
  done

  if [ ! -z "${tmpfilename}" ]
  then
    if [ "${RUN_TYPE_STUB}" = "full" ]
      then
        echo "Running ${CUT_TYPE}/${TMPNAME}0${TMPNAME_EXT} from ${tmpfilename}"
        nohup python ${SCRIPT_DIR}/${CUT_TYPE}/${TMPNAME}0${TMPNAME_EXT} 1 ${tmpfilename} >& ${OUT_DIR}/nohup.out &
        echo "Running ${CUT_TYPE}/${TMPNAME}1${TMPNAME_EXT} from ${tmpfilename}"
        nohup python ${SCRIPT_DIR}/${CUT_TYPE}/${TMPNAME}1${TMPNAME_EXT} 1 ${tmpfilename} >& ${OUT_DIR}/nohup.out &
        echo "Running ${CUT_TYPE}/${TMPNAME}2${TMPNAME_EXT} from ${tmpfilename}"
        nohup python ${SCRIPT_DIR}/${CUT_TYPE}/${TMPNAME}2${TMPNAME_EXT} 1 ${tmpfilename} >& ${OUT_DIR}/nohup.out &
    else
      echo "Running ${CUT_TYPE}/${TMPNAME}${TMPNAME_EXT} from ${tmpfilename}"
      nohup python ${SCRIPT_DIR}/${CUT_TYPE}/${TMPNAME}${TMPNAME_EXT} 1 ${tmpfilename} >& ${OUT_DIR}/nohup.out &
    fi
  fi  
fi
