#!/bin/bash
# The first argument is the results directory
# The second argument takes values: "best", "test", or "special"; the first two mean we are merging ${CUT_TYPE}-best.csv (${CUT_TYPE}-test.csv) from the batches; "special" means we specify the name to be merged here

MASTER_RESULTS_DIR="${PROJ_DIR}/results/instances"
SCRIPT_DIR="${PROJ_DIR}/scripts/run_scripts"
EXECUTABLE="merge_results.sh"

if [ -z "$1" ]
  then RESULTS_DIR="${MASTER_RESULTS_DIR}/batches/500x500"
else
  RESULTS_DIR="$1"
fi

if [ "$2" = "special" ]
then
  TMPNAME="cleaning_log.csv"
else
  if [ -z "${CUT_TYPE}" ]
  then 
    echo "*** ERROR: Need to specify cut type."
    exit 1
  fi

  TMPNAME="${CUT_TYPE}-full.csv"
  if [ "$2" = "best" ]
    then 
      TMPNAME="${CUT_TYPE}-best.csv"
      #TMPNAME="${CUT_TYPE}-best-nobb-allparams.csv"
      #TMPNAME="${CUT_TYPE}-best-bb-1.csv"
      #TMPNAME="${CUT_TYPE}-best-bb1.csv"
  elif [ "$2" = "test" ]
    then TMPNAME="${CUT_TYPE}-test.csv"
  fi
  TMPNAME="vpc_bb-best-bb10776.csv"
fi

OUT_DIR="${RESULTS_DIR}"
OUTNAME="${OUT_DIR}/${TMPNAME}"

i=0
for batchname in `ls -d ${RESULTS_DIR}/*/`; do
  i=$(( $i + 1 ))  # maintain line count
  if [ "$2" != "best" -a "$2" != "test" -a "$2" != "special" ]
    then ${SCRIPT_DIR}/${EXECUTABLE} ${batchname}
  fi
  if [ $i = 1 ]
    then 
      if [ "$2" = "special" ]
      then
        head -n 1 ${batchname}${TMPNAME} > ${OUTNAME}
      else
        head -n 2 ${batchname}${TMPNAME} > ${OUTNAME}
      fi
  fi
  echo "Copying ${TMPNAME} from ${batchname}"
  if [ "$2" = "special" ]
  then
    tail -n +2 ${batchname}${TMPNAME} | grep DONE >> ${OUTNAME}
  else
    tail -n +3 ${batchname}${TMPNAME} | grep DONE >> ${OUTNAME}
  fi
done

echo "Copying and sorting merged batch result from ${OUTNAME} to ${MASTER_RESULTS_DIR}/${TMPNAME}"
if [ "${RUN_TYPE_STUB}" = "-special" ]
then
  (head -n 1 ${OUTNAME} && (tail -n +2 ${OUTNAME} | sort)) > ${MASTER_RESULTS_DIR}/${TMPNAME}
  #head -n 2 ${OUTNAME} > ${MASTER_RESULTS_DIR}/${TMPNAME}
  #(tail -n +3 ${OUTNAME} | sort) >> ${MASTER_RESULTS_DIR}/${TMPNAME}
  cp ${MASTER_RESULTS_DIR}/${TMPNAME} ${OUTNAME}
else
  (head -n 2 ${OUTNAME} && (tail -n +3 ${OUTNAME} | sort)) > ${MASTER_RESULTS_DIR}/${TMPNAME}
  #head -n 2 ${OUTNAME} > ${MASTER_RESULTS_DIR}/${TMPNAME}
  #(tail -n +3 ${OUTNAME} | sort) >> ${MASTER_RESULTS_DIR}/${TMPNAME}
  cp ${MASTER_RESULTS_DIR}/${TMPNAME} ${OUTNAME}
fi
