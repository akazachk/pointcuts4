#!/bin/bash
# PROJ_DIR should be defined for these scripts
#
# Three arguments: 
# 1. full path to instance list, which should be located in the same directory as the instances [optional; default: ${PROJ_DIR}/data/instances/500x500.instances]
# 2. '1' or a '0', referrring to whether batch mode is used [optional; default: 0]
# 3. 'special' [optional; default: special]
#
# The third argument is only relevant for pha
# In batch mode, give the first argument as 1 and the second (optional) is where the set of batches is
# In non-batch mode, which is the default, the first (optional) argument is the directory containing the instances

# Defaults
DEFAULT_INSTANCE_DIR="${PROJ_DIR}/data/instances"
DEFAULT_INSTANCE_LIST="${PROJ_DIR}/data/instances/500x500.instances"
INSTANCE_LIST="${DEFAULT_INSTANCE_LIST}"
DEFAULT_BATCH_LIST="${PROJ_DIR}/data/instances/500x500.batch"
BATCH_MODE=0
#INSTANCE_DIR="${DEFAULT_INSTANCE_DIR}"

SCRIPT_DIR="${PROJ_DIR}/scripts/run_scripts/special"
OUT_DIR="${PROJ_DIR}/results/instances"

RUN_TYPE_STUB="special"

DEBUG="echo"
DEBUG=""

# Process run type
if [ "$1" = "special" -o "$2" = "special" -o "$3" = "special" ]
then 
  RUN_TYPE_STUB="special"
fi

# Process other arguments
if [ -z "$1" ]
then 
  INSTANCE_LIST="${DEFAULT_INSTANCE_LIST}"
elif [ "$1" = 1 ]
then 
  BATCH_MODE=1
  #INSTANCE_DIR="${PROJ_DIR}/data/instances/batches"
  INSTANCE_LIST="${DEFAULT_BATCH_LIST}"
elif [ "$1" = 0 ]
then 
  BATCH_MODE=0
elif [ "$1" != "special" ] #-a "$1" != "special" -a "$1" != "special" ]
then 
  INSTANCE_LIST="$1"
  if [ "$2" = 1 ]
    then 
      BATCH_MODE=1
  elif [ "$2" = 0 ]
    then 
      BATCH_MODE=0
  fi
fi

TMPNAME="run_${RUN_TYPE_STUB}"
TMPNAME_EXT=".py"

echo "Options selected: run ${SCRIPT_DIR}/${TMPNAME}${TMPNAME_EXT} using instances given in ${INSTANCE_LIST}. In batch mode? ${BATCH_MODE}. Output sent to ${OUT_DIR}/nohup.out."
#exit 1

# Proceed depending on whether run is in batches or not
if [ $BATCH_MODE == 0 ] 
then
  nohup python ${SCRIPT_DIR}/${TMPNAME}${TMPNAME_EXT} 0 ${INSTANCE_LIST} >> ${OUT_DIR}/nohup.out 2>&1 &
else
  FSTUB=`head -n 1 ${INSTANCE_LIST}`
  tmpfilename=""
  for line in `tail -n +2 ${INSTANCE_LIST}`; do
    # Skip empty lines
    if [ -z "$line" ]
    then
      continue
    fi
    
    #if [ ! -z $line ]
    #then
    #  echo "Current line: $line"
    #  echo "Current batch: $batch"
    #fi

    # If line ends with '/', then we start a new batch
    len="$((${#line}-1))"
    if [ "${line:len:1}" == "/" ]
    then
      # If there was an old batch, then run it
      if [ ! -z "${tmpfilename}" ]
      then
        echo "Running ${TMPNAME}${TMPNAME_EXT} from ${tmpfilename}"
        nohup python ${SCRIPT_DIR}/${TMPNAME}${TMPNAME_EXT} 1 ${tmpfilename} >> ${OUT_DIR}/nohup.out 2>&1 &
      fi  

      # Now we create the new batch
      batchstub="${line:0:len}"
      tmpfilename="/tmp/${FSTUB}.batch${batchstub}"
      tmpfile=$(mktemp -q ${tmpfilename})
      echo "${FSTUB}" > "${tmpfilename}"
      echo "${line}" >> "${tmpfilename}"
    else
      # Else, we add the current line to the current batch
      echo "${line}" >> "${tmpfilename}"
    fi  
  done

  if [ ! -z "${tmpfilename}" ]
  then
    echo "Running ${TMPNAME}${TMPNAME_EXT} from ${tmpfilename}"
    nohup python ${SCRIPT_DIR}/${TMPNAME}${TMPNAME_EXT} 1 ${tmpfilename} >> ${OUT_DIR}/nohup.out 2>&1 &
  fi  
fi
