####
## The inputted file is either [filename].instances or [file].batch.
## (The extension is not important.)
## The first line of this file will be the directory ``stub'',
## and output will be sent to ${PROJ_DIR}/results/instances/stub if batch mode is off,
## and to ${PROJ_DIR}/results/instances/batches/stub/batchname if it is on.
## Each line contains either a relative input path to an instance (without the extension) or a batch name.
## The path is relative to ${PROJ_DIR}/data/instances, e.g., the line will be miplib2/bm23.
## A batch name is distinguished by having the batch end with a '/', e.g., '2/' or 'batch2/'.

## Set up proper path variables
import os
PROJ_DIR = os.path.abspath(os.environ['PROJ_DIR'])
#CUT_TYPE = os.environ['CUT_TYPE']
#PROJ_DIR = os.path.abspath("..")
CUT_TYPE = 'pha'  # pha or vpc or vpc_bb or tilted

## Solver options
hplaneHeurs = [0,1,2]
newHplaneFinalFlag = 1
numExtraActRounds = [0,1,-1]
numRaysToBeCut = [5]
numCutGenSets = [0]
numCutsPerSplit = [5]
overloadMaxCuts = [0]
limitCutsPerSplit = [1]
useSplitShare = [0]
numCutsIterBilinear = [0]
useCutVertHeur = [0,1]
useTightPointsHeur = [0,1000]
usePresolve = [0]
maxPointsInCutShare = 1000

## Set up output and input folders
results_path = PROJ_DIR + '/results/instances'
paramfile = PROJ_DIR + '/data/params/pha_params.txt'
#instances_path = os.getcwd()
instances_path = PROJ_DIR + '/data/instances'
instances_file = instances_path + '/' + "500x500.instances"

outinfo_stub = CUT_TYPE + '-test'
outinfo_dir = results_path

## Get arguments
from sys import argv
use_folders = False  # set to true/false depending on if mps files are all in one folder or divided up into subfolders
if (len(argv) > 1):
  use_folders = True if argv[1] in ['true', 'True', '1', 't'] else False
  if (use_folders and len(argv) < 2):
    raise ValueError('When using batches, specifying the folder is required')

if (len(argv) > 2):
  instances_file = os.path.abspath(argv[2])

## Where are the instances?
with open(instances_file) as f_in:
  list_to_use = list(filter(None, (line.rstrip() for line in f_in)))

## The first line will be the name of the directory we should use
dir_stub = list_to_use[0]
list_to_use = list_to_use[1:]

if use_batches:
  dir_stub = "batches/" + dir_stub

## Finalize outinfo
outinfo_dir = outinfo_dir + '/' + dir_stub 
os.system("mkdir -p " + outinfo_dir)  # make the dir if it does not exist

## Choose order so that deepest for loop are the results you want to see first, fixing all others
batch_name = ''
for usecutvert in useCutVertHeur:
  for numrays in numRaysToBeCut:
    for numiterblp in numCutsIterBilinear:
      for splitshareopt in useSplitShare:
        for limitcuts in limitCutsPerSplit:
          for numcuts in numCutsPerSplit:
            for usepresolve in usePresolve:
              for numextra in numExtraActRounds:
                for hplaneheur in hplaneHeurs:
                  for inst in list_to_use:
#                    if (not inst_name.startswith('.') and not inst_name.startswith('IGNORE') and
#                        (use_folders or (inst_name.endswith('mps') or inst_name.endswith('mps.gz') or inst_name.endswith('lp')))):
                    ## Check if batch name
                    if (inst[-1] == '/'):
                      batch_name = inst
                      continue

                    ## Run on instances_path/inst.mps
                    inst_name = inst
                    infile = instances_path + '/' + inst_name + '.mps'
                    curr_out_dir = outinfo_dir + '/' + batch_name
                    outinfo = curr_out_dir + outinfo_stub
          
                    ## Arguments
                    extraparams = " --pha_act_option=" + str(hplaneheur) + " --num_extra_act_rounds=" + str(numextra) + " --num_rays_cut=" + str(numrays) + " --use_split_share=" + str(splitshareopt) + " --use_cut_vert_heur=" + str(usecutvert) + " --num_cuts_iter_bilinear=" + str(numiterblp) + " --cut_presolve=" + str(usepresolve) + " --limit_cuts_per_cgs=" + str(limitcuts) + " --num_cuts_per_cgs=" + str(numcuts) + " --use_tight_points_heur=" + str(useTightPointsHeur[0])
                      
                    print('\t' + infile + extraparams)
                      
                    os.system(PROJ_DIR + "/Release/PointCuts" + " -i " + infile + " -o " + curr_out_dir + " --log_file=" + outinfo + " -p " + paramfile + extraparams + " > /dev/null 2>&1")
