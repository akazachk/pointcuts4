####
## The inputted file is either [filename].instances or [file].batch.
## (The extension is not important.)
## The first line of this file will be the directory ``stub'',
## and output will be sent to ${PROJ_DIR}/results/instances/stub if batch mode is off,
## and to ${PROJ_DIR}/results/instances/batches/stub/batchname if it is on.
## Each line contains either a relative input path to an instance (without the extension) or a batch name.
## The path is relative to ${PROJ_DIR}/data/instances, e.g., the line will be miplib2/bm23.
## A batch name is distinguished by having the batch end with a '/', e.g., '2/' or 'batch2/'.

## Set up proper path variables
import os
PROJ_DIR = os.path.abspath(os.environ['PROJ_DIR'])
#CUT_TYPE = os.environ['CUT_TYPE']
#PROJ_DIR = os.path.abspath("..")
CUT_TYPE = 'vpc_bb'  # pha or vpc or vpc_bb or tilted

## Solver options
#vpcDepthList = [0]
#overloadMaxCutsList = [0]
strengthenList = [1]
prlpStrategyList = [1]
# bb_strategy = sum of 2^x, where x is whichever option you want: 
# 1 = cbc, 2 = cplex, 3 = gurobi, 
# 4 = user_cuts, 5 = all_cuts_off, 6 = all_cuts_on, 7 = gmics_off, 8 = gmics_on, 
# 9 = presolve_off, 10 = presolve_on, 11 = heuristics_off, 12 = heuristics_on, 
# 13 = use_best_bound, 14 = strong_branching_on
bbStrategy = 10776  # 3, 4, 9, 11, 13
cutPresolveList = [0]

## Set up output and input folders
results_path = PROJ_DIR + '/results/instances'
paramfile_dir = PROJ_DIR + '/data/params'
paramfile = paramfile_dir + "/" + CUT_TYPE + "_params.txt"
#instances_path = os.getcwd()
instances_path = PROJ_DIR + "/data/instances"
instances_file = instances_path + '/' + "500x500.instances"

outinfo_stub = CUT_TYPE + '-best' + '-bb' + str(bbStrategy)
outinfo_dir = results_path

## Get arguments
from sys import argv
use_batches = False  # set to true/false depending on if mps files are all in one folder or divided up into subfolders
if (len(argv) > 1):
  use_batches = True if argv[1] in ['true', 'True', '1', 't'] else False
  if (use_batches and len(argv) < 2):
    raise ValueError('When using batches, specifying the folder is required')

if (len(argv) > 2):
  instances_file = os.path.abspath(argv[2])

## Where are the instances?
with open(instances_file) as f_in:
  list_to_use = list(filter(None, (line.rstrip() for line in f_in)))

## The first line will be the name of the directory we should use
dir_stub = list_to_use[0]
list_to_use = list_to_use[1:]

#instances_file_name = instances_file.split('/')[-1]
#instances_file_name_split_by_dot = instances_file_name.split('.')
#dir_stub = '.'.join(instances_file_name_split_by_dot[0:len(instances_file_name_split_by_dot)-1])
if use_batches:
  dir_stub = "batches/" + dir_stub

## Finalize outinfo
outinfo_dir = outinfo_dir + '/' + dir_stub 
os.system("mkdir -p " + outinfo_dir)  # make the dir if it does not exist

## Choose order so that deepest for loop are the results you want to see first, fixing all others
batch_name = ''
for inst in list_to_use:
    ## Check if batch name
    if (inst[-1] == '/'):
      batch_name = inst
      continue

    ## Parse from batch name
    depth = 0 # no cuts
    if (depth == 0):
      maxcuts = -1
      cut_presolve = 2
    else:
      maxcuts = batch_name.split('_')[0] 
      if (maxcuts == "1"):
        maxcuts = "-1"
      elif (maxcuts == "5"):
        maxcuts = "-5"
      else:
        maxcuts = "10000"
      depth = '-' + batch_name.split('_')[1]
      cut_presolve = batch_name.split('_')[2]
      cut_presolve = cut_presolve.split('/')[0]

    ## Run on instances_path/inst.mps
    inst_name = inst
    infile = instances_path + '/' + inst_name + '.mps'
    curr_out_dir = outinfo_dir + '/' + batch_name
    outinfo = curr_out_dir + outinfo_stub

    ## In case the out directory does not exist
    os.system("mkdir -p " + curr_out_dir)

    for prlp_strategy in prlpStrategyList:
      for strengthen in strengthenList:
        ## Arguments
        extraparams = " -p " + paramfile + " --vpc_depth=" + str(depth) + " --strengthen=" + str(strengthen) + " --overload_max_cuts=" + str(maxcuts) + " --bb_runs=-7 --bb_strategy=" + str(bbStrategy) + " --prlp_strategy=" + str(prlp_strategy) + " --partial_bb_strategy=04" + " --bb_use_best_bound=1" + " --opt_file=" + PROJ_DIR + '/data/ip_opt.csv' + " --cut_presolve=" + str(cut_presolve)
        #extraparams = " --bb_strategy=10776" + " --cleaning_mode=1" + " --opt_file=" + PROJ_DIR + '/data/ip_opt.csv'

        print(PROJ_DIR + "/Release/PointCuts" + " -i " + infile + " -o " + curr_out_dir + " --log_file=" + outinfo + extraparams)
        os.system(PROJ_DIR + "/Release/PointCuts" + " -i " + infile + " -o " + curr_out_dir + " --log_file=" + outinfo + extraparams + " > /dev/null 2>&1") 
        #os.system(PROJ_DIR + "/Release/PointCuts" + " -i " + infile + " -o " + curr_out_dir + extraparams + " > /dev/null 2>&1") 

        ## Rerun with depth = 0
        #extraparams = " -p " + paramfile + " --vpc_depth=" + str(depth) + " --strengthen=" + str(strengthen) + " --overload_max_cuts=" + str(maxcuts) + " --bb_runs=-5 --bb_strategy=" + str(bbStrategy) + " --prlp_strategy=" + str(prlp_strategy) + " --partial_bb_strategy=04" + " --bb_use_best_bound=1" + " --opt_file=" + PROJ_DIR + '/data/ip_opt.csv' + " --cut_presolve=" + str(cut_presolve)

        #print(PROJ_DIR + "/Release/PointCuts" + " -i " + infile + " -o " + curr_out_dir + " --log_file=" + outinfo +  extraparams)
        #os.system(PROJ_DIR + "/Release/PointCuts" + " -i " + infile + " -o " + curr_out_dir + " --log_file=" + outinfo + extraparams + " > /dev/null 2>&1") 
