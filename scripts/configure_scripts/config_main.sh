#!/bin/bash

DEBUG="echo"
DEBUG=""
ENV_VARS_DIR="saved_settings"
ENV_VARS_FNAME="env_vars.sh"

read -p "Do environment variables need to be set up? [y/n] " -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
  read -p "  Have you put in the proper variables in ${ENV_VARS_FNAME}? Currently, all files are going to be installed in the directory ${PROJ_DIR}. [y/n] " -r
  if [[ ! $REPLY =~ ^[Yy]$ ]]
  then
    echo "Please do so before continuing."
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 # handle exits from shell or function but don't exit interactive shell
  fi

  source "${ENV_VARS_DIR}/${ENV_VARS_FNAME}"
fi

echo
echo "All files are going to be installed in the directory ${PROJ_DIR}."
SCRIPT_DIR="${PROJ_DIR}/scripts/configure_scripts"

echo
read -p "Do you want to locally install LAPACK and BLAS in ${PROJ_DIR}/lib? [y/n] " -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
  echo "  Setting up ${PROJ_DIR}/lib with LAPACK and BLAS."
  ${DEBUG} "${SCRIPT_DIR}/setup_lapack_and_blas.sh"
fi

echo
read -p "Do you want to locally install Bcp in ${PROJ_DIR}/coin-or? [y/n] " -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
  echo "  Setting up ${PROJ_DIR}/coin-or with Bcp."
  ${DEBUG} "${SCRIPT_DIR}/setup_coin.sh"
fi

echo
read -p "Do you want to locally install matplotlib in ${PROJ_DIR}/scripts/python_scripts? [y/n] " -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
  echo "  Setting up ${PROJ_DIR}/scripts/python_scripts with matplotlib."
  ${DEBUG} "${SCRIPT_DIR}/install_matplotlib.sh"
fi
