#!/bin/bash

NAME=lapack
DESTINATION="${PROJ_DIR}/lib"
INSTALL_DIR="${PROJ_DIR}/install"
SCRIPT_DIR="${PROJ_DIR}/scripts/configure_scripts/saved_settings/lapack"
SAVED_DIR="${PWD}"
DEBUG="echo -e \t"

${DEBUG} mkdir -p "${INSTALL_DIR}"
${DEBUG} mkdir -p "${DESTINATION}"

echo "    Installing ${NAME} into ${INSTALL_DIR}"
${DEBUG} git clone https://github.com/Reference-LAPACK/lapack.git "${INSTALL_DIR}/lapack"

${DEBUG} cd "${INSTALL_DIR}/lapack"
${DEBUG} tar zxf lapack.tar
${DEBUG} cp "${SCRIPT_DIR}/make.inc" ${INSTALL_DIR}/lapack/make.inc
${DEBUG} cp "${SCRIPT_DIR}/SRC/Makefile" ${INSTALL_DIR}/lapack/SRC/Makefile
${DEBUG} cp "${SCRIPT_DIR}/BLAS/SRC/Makefile" ${INSTALL_DIR}/lapack/BLAS/SRC/Makefile

${DEBUG} cd "${SAVED_DIR}"

echo "Makefiles have been copied to ${INSTALL_DIR}/lapack. To proceed with installation, go to ${INSTALL_DIR} and call 'make blaslib' and then 'make lapacklib'. Then copy the files 'liblapack.so' and 'libblas.so' to ${DESTINATION}."
