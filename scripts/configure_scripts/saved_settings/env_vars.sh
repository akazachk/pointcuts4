#!/bin/sh
export PROJ_DIR=${REPOS_DIR}/pointcuts4
#${HOME}/Desktop/Box/repos/pointcuts4
export BCP_DEBUG="${PROJ_DIR}/coin-or/Bcp-1.4.3/buildg"
export BCP_RELEASE="${PROJ_DIR}/coin-or/Bcp-1.4.3/build"
export CBC_DEBUG="${PROJ_DIR}/coin-or/Cbc-2.9.8/buildg"
export CBC_RELEASE="${PROJ_DIR}/coin-or/Cbc-2.9.8/build"
# CUT_TYPE can be vpc_bb or vpc or pha or tilted
export CUT_TYPE="vpc_bb" 
export ENV_LAPACK_LIB=""
export ENV_LAPACK_LIB_NAME=""
export ENV_BLAS_LIB=""
export ENV_BLAS_LIB_NAME=""
#export ENV_LAPACK_LIB="${PROJ_DIR}/lib"
#export ENV_LAPACK_LIB_NAME="liblapack.so"
#export ENV_BLAS_LIB="${PROJ_DIR}/lib"
#export ENV_BLAS_LIB_NAME="libblas.so"
export LD_LIBRARY_PATH="${PROJ_DIR}/lib"
export PYTHONPATH="${PROJ_DIR}/scripts/python_scripts:${PROJ_DIR}/lib64/python:${PROJ_DIR}/lib/python"
