#!/bin/bash

INSTALL_DIR="${PROJ_DIR}/install"
DESTINATION="${PROJ_DIR}"
SAVED_DIR="${PWD}"
DEBUG="echo -e \t"

${DEBUG} mkdir -p "${INSTALL_DIR}"
${DEBUG} mkdir -p "${DESTINATION}/lib"
${DEBUG} mkdir -p "${DESTINATION}/lib64"

echo "    Installing six into ${INSTALL_DIR}"
${DEBUG} git clone https://github.com/benjaminp/six.git ${INSTALL_DIR}/six
${DEBUG} cd "${INSTALL_DIR}/six"
${DEBUG} python setup.py install --home="${DESTINATION}"

echo "    Installing matplotlib into ${INSTALL_DIR}"
${DEBUG} git clone git@github.com:matplotlib/matplotlib.git ${INSTALL_DIR}/matplotlib
${DEBUG} cd "${INSTALL_DIR}/matplotlib"
${DEBUG} python setup.py install --home="${DESTINATION}"

${DEBUG} cd "${SAVED_DIR}"
