#!/bin/bash

NAME=Bcp
DESTINATION="${PROJ_DIR}/coin-or"
SCRIPT_DIR="${PROJ_DIR}/scripts/configure_scripts/saved_settings/lapack"
VERSION=1.4.3
SYSTEM=mac
DEBUG="echo -e \t"

echo "    Installing ${NAME} into ${DESTINATION}"
${DEBUG} mkdir -p "${DESTINATION}"
${DEBUG} svn checkout https://projects.coin-or.org/svn/Bcp/releases/${VERSION} "${DESTINATION}/Bcp"

${DEBUG} mkdir -p "${DESTINATION}/Bcp/build/share"
${DEBUG} cp "${SCRIPT_DIR}/${SYSTEM}_release_config.site" "${DESTINATION}/Bcp/build/share"

echo "Configuration file has been copied into ${DESTINATION}/Bcp/build/share. To proceed with installation, go to ${DESTINATION}/Bcp/build, call '../configure -C', check that blas and lapack are correctly detected, then run 'make', and finally 'make install'."
