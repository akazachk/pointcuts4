* ENCODING=ISO-8859-1
NAME          temp3.lp
ROWS
 N  obj     
 L  cons1   
 L  cons2   
 L  cons3   
 L  cons4   
COLUMNS
    MARK0000  'MARKER'                 'INTORG'
    x1        obj                            -2
    x1        cons1                           1
    x1        cons2                           1
    x1        cons3                           1
    x1        cons4                          -1
    x2        obj                            -4
    x2        cons1                           1
    x2        cons2                           1
    x2        cons3                          -1
    x3        obj                            -8
    x3        cons1                           1
    x3        cons2                          -1
    x3        cons3                           1
    x3        cons4                           1
    x4        obj                            10
    x4        cons1                          -1
    x4        cons2                           1
    MARK0001  'MARKER'                 'INTEND'
RHS
    rhs       cons1                         2.5
    rhs       cons2                           2
    rhs       cons3                         1.5
    rhs       cons4                         0.5
BOUNDS
 UP bnd       x1                              1
 UP bnd       x2                              1
 UP bnd       x3                              1
 UP bnd       x4                              1
ENDATA
